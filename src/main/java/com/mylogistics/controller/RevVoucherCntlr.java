package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.FaBPromDAO;
import com.mylogistics.DAO.FaTravDivisionDAO;
import com.mylogistics.DAO.RevVoucherDAO;
import com.mylogistics.DAO.SubElectricityMstrDAO;
import com.mylogistics.DAO.SubTelephoneMstrDAO;
import com.mylogistics.DAO.SubVehicleMstrDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.FaBProm;
import com.mylogistics.model.SubElectricityMstr;
import com.mylogistics.model.SubTelephoneMstr;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.TravDivService;
import com.mylogistics.services.VM_SubVMService;

@Controller
public class RevVoucherCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Autowired
	private RevVoucherDAO revVoucherDAO;
	
	@Autowired
	private SubTelephoneMstrDAO subTelephoneMstrDAO;
	
	@Autowired
	private SubElectricityMstrDAO subElectricityMstrDAO;
	
	@Autowired
	private FaBPromDAO faBPromDAO;
	
	@Autowired
	private SubVehicleMstrDAO subVehicleMstrDAO;
	
	@Autowired
	private FaTravDivisionDAO faTravDivisionDAO;
	
	@RequestMapping(value = "/getVDetFrRV" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrRV() {  
		System.out.println("Enter into getVDetFrRV---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				List<CashStmt> csList = cashStmtStatusDAO.getAllCashStmt(cashStmtStatus.getCssId());
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put("csList",csList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	

	@RequestMapping(value = "/getTelVSubFile" , method = RequestMethod.POST)  
	public @ResponseBody Object getTelVSubFile(@RequestBody List<CashStmt> reqCsList) {
		System.out.println("enter into submitRevVouch function");
		Map<String,Object> map = new HashMap<String,Object>();
		if(!reqCsList.isEmpty()){
			List<SubTelephoneMstr> stmList = subTelephoneMstrDAO.getSubFileDetails(reqCsList);
			if(!stmList.isEmpty()){
				map.put("list",stmList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	@RequestMapping(value = "/getEleVSubFile" , method = RequestMethod.POST)  
	public @ResponseBody Object getEleVSubFile(@RequestBody List<CashStmt> reqCsList) {
		System.out.println("enter into getEleVSubFile function");
		Map<String,Object> map = new HashMap<String,Object>();
		if(!reqCsList.isEmpty()){
			List<SubElectricityMstr> semList = subElectricityMstrDAO.getSubFileDetails(reqCsList);
			if(!semList.isEmpty()){
				map.put("list",semList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	@RequestMapping(value = "/getBPVSubFile" , method = RequestMethod.POST)  
	public @ResponseBody Object getBPVSubFile(@RequestBody List<CashStmt> reqCsList) {
		System.out.println("enter into getBPVSubFile function");
		Map<String,Object> map = new HashMap<String,Object>();
		if(!reqCsList.isEmpty()){
			List<FaBProm> fbpList = faBPromDAO.getSubFileDetails(reqCsList);
			if(!fbpList.isEmpty()){
				map.put("list",fbpList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	@RequestMapping(value = "/getVehVSubFile" , method = RequestMethod.POST)  
	public @ResponseBody Object getVehVSubFile(@RequestBody List<CashStmt> reqCsList) {
		System.out.println("enter into getVehVSubFile function");
		Map<String,Object> map = new HashMap<String,Object>();
		if(!reqCsList.isEmpty()){
			List<VM_SubVMService> vDetList = subVehicleMstrDAO.getSubFileDetails(reqCsList);
			if(!vDetList.isEmpty()){
				map.put("list",vDetList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getTrvSubFile" , method = RequestMethod.POST)  
	public @ResponseBody Object getTrvSubFile(@RequestBody List<CashStmt> reqCsList) {
		System.out.println("enter into getTrvSubFile function");
		Map<String,Object> map = new HashMap<String,Object>();
		if(!reqCsList.isEmpty()){
			List<TravDivService> tDetList = faTravDivisionDAO.getSubFileDetails(reqCsList);
			if(!tDetList.isEmpty()){
				map.put("list",tDetList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	

	@RequestMapping(value = "/submitRevVouch" , method = RequestMethod.POST)  
	public @ResponseBody Object submitRevVouch(@RequestBody List<CashStmt> reqCsList) {
		System.out.println("enter into submitRevVouch function");
		Map<String,Object> map = new HashMap<String,Object>();
		int result = 0;
		if(!reqCsList.isEmpty()){
			if(reqCsList.get(0).getCsType().equalsIgnoreCase(ConstantsValues.TEL_VOUCHER)){
				result = revVoucherDAO.revTelVoucher(reqCsList);
			}else if(reqCsList.get(0).getCsType().equalsIgnoreCase(ConstantsValues.ELE_VOUCHER)){
				result = revVoucherDAO.revEleVoucher(reqCsList);
			}else if(reqCsList.get(0).getCsType().equalsIgnoreCase(ConstantsValues.BP_VOUCHER)){
				result = revVoucherDAO.revBPVoucher(reqCsList);
			}else if(reqCsList.get(0).getCsType().equalsIgnoreCase(ConstantsValues.VEH_VOUCHER)){
				result = revVoucherDAO.revVehVoucher(reqCsList);
			}else if(reqCsList.get(0).getCsType().equalsIgnoreCase(ConstantsValues.VEH_TRAVEL)){
				result = revVoucherDAO.revTrvVoucher(reqCsList);
			}else{
				result = revVoucherDAO.revVoucher(reqCsList);
			}
			
			
			if(result > 0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}

	
	//TODO back data
	@RequestMapping(value = "/getVDetFrRVBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrRVBack() {  
		System.out.println("Enter into getVDetFrRVBack---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				List<CashStmt> csList = cashStmtStatusDAO.getAllCashStmt(cashStmtStatus.getCssId());
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put("csList",csList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
}

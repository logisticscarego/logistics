package com.mylogistics.controller;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Date;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.jboss.logging.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itextpdf.text.FontFactory;
import com.mylogistics.DAO.AllImgDao;
import com.mylogistics.DAO.ArrivalReportDAO;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BrhWiseMunsDAO;
import com.mylogistics.DAO.BrokerDAO;
import com.mylogistics.DAO.ChallanDAO;
import com.mylogistics.DAO.LhpvStatusDAO;
import com.mylogistics.DAO.MunsianaDAO;
import com.mylogistics.DAO.OwnerDAO;
import com.mylogistics.DAO.RemarksDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.DAOImpl.LhpvStatusDAOImpl;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Broker;
import com.mylogistics.model.Challan;
import com.mylogistics.model.Owner;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.model.lhpv.BrhWiseMuns;
import com.mylogistics.model.lhpv.LhpvBal;
import com.mylogistics.model.lhpv.LhpvStatus;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;

@Controller
public class LhpvBalCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private LhpvStatusDAO lhpvStatusDAO;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Autowired
	private BrokerDAO brokerDAO;
	
	@Autowired
	private OwnerDAO ownerDAO;
	
	@Autowired
	private ChallanDAO challanDAO;
	
	@Autowired
	private MunsianaDAO munsianaDAO;
	
	@Autowired
	private ArrivalReportDAO arrivalReportDAO;
	
	@Autowired
	private BrhWiseMunsDAO brhWiseMunsDAO;
	
	@Autowired
	private RemarksDAO remarksDAO;
	
	@Autowired
	private AllImgDao allImgDao;
	
	Blob blob=null;
	
    private	List<LhpvBal> gLhpvBalList;
	
	public static Logger logger = Logger.getLogger(LhpvBalCntlr.class);
	
	private SessionFactory sessionFactory;
//	private Session session;
//	private Transaction transaction;
	
	@Autowired
	public LhpvBalCntlr(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	@RequestMapping(value = "/getLhpvBalDet" , method = RequestMethod.POST)  
	public @ResponseBody Object getLhpvBalDet() {  
		System.out.println("Enter into getLhpvBalDet---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
//		Date date=Date.valueOf("2018-04-01");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				//CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				List<Map<String,Object>> bnkList = new ArrayList<>();
				bnkList = bankMstrDAO.getBnkNameAndCode(branchCode);
				LhpvStatus lhpvStatus = lhpvStatusDAO.getLastLhpvStatus(branchCode);
				if(lhpvStatus != null){
					System.out.println("lhpvStatus: "+lhpvStatus.getbCode());
//					if(!lhpvStatus.getbCode().equalsIgnoreCase("1") && lhpvStatus.getLsDt().compareTo(date)>=0){
//						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
//						System.out.println("lsDate= "+lhpvStatus.getLsDt()+" branchCode"+lhpvStatus.getbCode());
//					}else{
						map.put(ConstantsValues.BRANCH, branchList.get(0));
						map.put("lhpvStatus",lhpvStatus);
						map.put("bnkList",bnkList);
						map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				//	}
					
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}
	
	
	@RequestMapping(value = "/getChlnAndBrOwnLB" , method = RequestMethod.POST)  
	public @ResponseBody Object getChlnAndBrOwnLB() {  
		System.out.println("Enter into getChlnAndBrOwnLB---->");
		//User currentUser = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> map = new HashMap<>();	
		
		/*List<String> chlnList = challanDAO.getChCodeByBrh(currentUser.getUserBranchCode());*/
		List<String> chlnList = challanDAO.getChlnCodeFrLHB();
		
		if(!chlnList.isEmpty()){
			for(int i=0;i<chlnList.size();i++){
				for(int j=i+1;j<chlnList.size();j++){
					if(chlnList.get(i).equalsIgnoreCase(chlnList.get(j))){
						chlnList.remove(j);
						j--;
					}
				}
			}
		}
		
		List<Map<String,String>> brkList = brokerDAO.getBrkNCF();
		List<Map<String,String>> ownList = ownerDAO.getOwnNCF();
		
		List<Map<String,String>> actBOList = new ArrayList<>();
		
		if(!brkList.isEmpty()){
			/*for(int i=0;i<brkList.size();i++){
				if(Integer.parseInt(brkList.get(i).get("panImgId")) <= 0){
					brkList.remove(i);
					i = i-1;
				}
			}*/
			actBOList = brkList;
		}
		
		if(!ownList.isEmpty()){
			for(int i=0;i<ownList.size();i++){
				//if(Integer.parseInt(ownList.get(i).get("panImgId")) > 0){
					actBOList.add(ownList.get(i));
				//}
			}
		}
		
		if(!chlnList.isEmpty() && !actBOList.isEmpty()){
			map.put("chlnList", chlnList);
			map.put("actBOList",actBOList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}	
	
	
	
	@RequestMapping(value = "/getChequeNoFrLHB" , method = RequestMethod.POST)  
	public @ResponseBody Object getChequeNoFrLHB(@RequestBody Map<String,String> clientMap) {  
		System.out.println("Enter into getChequeNoFrLHB---->");
		Map<String, Object> map = new HashMap<>();	
		String bankCode = clientMap.get("bankCode");
		String cType = clientMap.get("CType");
		char CType = cType.charAt(0);
		System.out.println("value of CType = "+CType);
		ChequeLeaves chequeLeaves= bankMstrDAO.getChqListByBnkC(bankCode,CType);
		List<ChequeLeaves> chqList = bankMstrDAO.getChqLByBnk(bankCode,CType);
		if(!chqList.isEmpty()){
			System.out.println("***************** 1");
			map.put("list",chqList);
			map.put(ConstantsValues.CHEQUE_LEAVES,chequeLeaves);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			System.out.println("***************** 2");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/getChlnFrLB" , method = RequestMethod.POST)  
	public @ResponseBody Object getChlnFrLB(@RequestBody Map<String,String> clientMap) {  
		System.out.println("Enter into getChlnFrLB---->");	
		Map<String, Object> map = new HashMap<>();	
		String chlnCode = clientMap.get("chlnCode");
		String brkOwnFaCode = clientMap.get("brkOwnFaCode");
		float tdsPer = (float) 0.0;
		
		if(brkOwnFaCode != null){
			if(brkOwnFaCode.substring(0,2).equals("08")){
				Owner own = ownerDAO.getOwnByFaCode(brkOwnFaCode);
				if(own != null){
					tdsPer = own.getOwnPanIntRt();
				}
			}else if(brkOwnFaCode.substring(0,2).equals("09")){
				Broker brk = brokerDAO.getBrkByFaCode(brkOwnFaCode);
				if(brk != null){
					tdsPer = brk.getBrkPanIntRt();
				}
			}else{
				System.out.println("invalid owner/broker code");
			}
		}
		
		System.out.println("value of tdsPer = "+tdsPer);
		
		if(chlnCode != null){
			List<Challan> chlnList = challanDAO.getChallanList(chlnCode);
			if(!chlnList.isEmpty()){
				
				List list=remarksDAO.verifyFrBal(chlnCode);
				System.out.println("list size=="+list.size());
				if(!list.isEmpty()){
					map.put("result", "hold");
					return map;
				}
				int res = challanDAO.checkLB(chlnList.get(0).getChlnId());
				
				double chlnBal = chlnList.get(0).getChlnRemBal();
				double munsAmt = 0.0;
				double tdsAmt = 0.0;
				double csDis  = 0.0;
				System.out.println("chlnBal = "+chlnBal);
				
				String brhCode = chlnList.get(0).getBranchCode();
				BrhWiseMuns brhWiseMuns = brhWiseMunsDAO.getBrhMuns(Integer.parseInt(brhCode));
				
				if(chlnBal > 0){
					if(brhWiseMuns != null){
						munsAmt = brhWiseMuns.getBwmMuns();
						float csDisPer = brhWiseMuns.getBwmCsDis();
						
						System.out.println("csDisPer = "+csDisPer);
						csDis  = (chlnBal * csDisPer) / 100;
						DecimalFormat df = new DecimalFormat("#.##");
						String angleFormated = df.format(csDis);
						csDis = Double.parseDouble(angleFormated);
						
						tdsAmt = (chlnBal * tdsPer) / 100;
					}else{
						munsAmt = munsianaDAO.getMunsAmt(chlnBal);
						System.out.println("munsAmt === > "+munsAmt);
						csDis = (chlnBal * 0.01);
						tdsAmt = (chlnBal * tdsPer) / 100;
					}
				}
				
				System.out.println("value of tdsAmt = "+tdsAmt);
				map.put("arHoAlw",false);
				if(chlnList.get(0).getChlnArId() > 0){
					ArrivalReport arrivalReport = arrivalReportDAO.getArById(chlnList.get(0).getChlnArId());
					if(arrivalReport.isArIsHOAlw() == true){
						map.put("arHoAlw",true);
						

						// allow only extra amount
						if (brhCode.equalsIgnoreCase("24")) {
							java.util.Date d = new java.util.Date();
							Long dt = d.getTime();
							Date date = new Date(dt);
							Date date2 = arrivalReport.getArIssueDt();
							Long dif = (date.getTime() - date2.getTime())
									/ (24 * 60 * 60 * 1000);
							System.out.println("date="+ dif);
							if (dif <= 7) {
								chlnBal = 0;
							}

						}
						
						System.out.println("arrivalReport.getArWtShrtg() = "+arrivalReport.getArWtShrtg());
						/*map.put("wtShrtg",arrivalReport.getArWtShrtg());
                        map.put("drRcvrWt",arrivalReport.getArDrRcvrWt());
                        map.put("ltDel",arrivalReport.getArLateDelivery());
                        map.put("ltAck",arrivalReport.getArLateAck());
                        map.put("exKm",arrivalReport.getArExtKm());
                        map.put("ovrHgt",arrivalReport.getArOvrHgt());
                        map.put("penalty",arrivalReport.getArPenalty());
                        map.put("oth",arrivalReport.getArOther());
                        map.put("det",arrivalReport.getArDetention());
                        map.put("unLdg",arrivalReport.getArUnloading());*/
						map.put("wtShrtg",arrivalReport.getArRemWtShrtg());
						map.put("drRcvrWt",arrivalReport.getArRemDrRcvrWt());
						map.put("ltDel",arrivalReport.getArRemLateDelivery());
						map.put("ltAck",arrivalReport.getArRemLateAck());
						map.put("exKm",arrivalReport.getArRemExtKm());
						map.put("ovrHgt",arrivalReport.getArRemOvrHgt());
						map.put("penalty",arrivalReport.getArPenalty());
						map.put("oth",arrivalReport.getArOther());
						map.put("det",arrivalReport.getArRemDetention());
						map.put("unLdg",arrivalReport.getArRemUnloading());
					}else{
						map.put("arHoAlw",false);
					}
					
					if(arrivalReport.getArSrtgDmg()!=null){
						if(arrivalReport.getArSrtgDmg().equalsIgnoreCase("OK") || arrivalReport.isSrtgDmgAlw()==true){
							map.put("arStgDmg", "OK");
						}else{
							map.put("arStgDmg", "DMGSRTG");
							map.put("result", "SRTG");
							return map;
						}
					
					}else{
						map.put("arStgDmg", "OK");
					}
				}else{
					map.put("wtShrtg",0);
					map.put("drRcvrWt",0);
					map.put("ltDel",0);
					map.put("ltAck",0);
					map.put("exKm",0);
					map.put("ovrHgt",0);
					map.put("penalty",0);
					map.put("oth",0);
					map.put("det",0);
					map.put("unLdg",0);
				}
				
				//deduct tds once 
				/*if(res > 0){
					map.put("isCal",false);
				}else{
					map.put("isCal",true);
				}*/
				
				//deduct tds multiple time
				map.put("isCal",true);
				
				map.put("tdsPer",tdsPer);
				map.put("chln",chlnList.get(0));
				map.put("munsAmt",munsAmt);
				map.put("tdsAmt",tdsAmt);
				map.put("csDis",csDis);
				map.put("chlnBal",chlnBal);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	@RequestMapping(value = "/submitLhpvBal" , method = RequestMethod.POST)  
	public @ResponseBody Object submitLhpvBal(@RequestBody VoucherService voucherService) {  
		System.out.println("Enter into submitLhpvBal---->");
		
		Map<String, Object> map = new HashMap<>();	
		
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try{
			
			int i = arrivalReportDAO.saveArRemBal(voucherService,session);
			int lbNo = lhpvStatusDAO.saveLhpvBal(voucherService,session);

			if(lbNo > 0 && i>0){
				map.put("lbNo",lbNo);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}	
			
			session.flush();
			session.clear();
			transaction.commit();
		}catch(Exception e){
			e.printStackTrace();
			transaction.rollback();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			logger.info("Exception in submitlhpvbal save "+e);
		}finally{
			session.close();
		}
		
		return map;
	}	
	
	@RequestMapping(value = "/verifyChlnForLhpvBal" , method = RequestMethod.POST)
	public @ResponseBody Object verifyChlnForLhpvBal(@RequestBody String chlnCode){
		System.out.println("LhpvAdvCntlr.verifyChlnForLhpvBal()");
		Map<String, Object> map = new HashMap<>();
		map = challanDAO.verifyChlnForLhpvBalOld(chlnCode);
		return map;
	}
	
	@RequestMapping(value = "/verifyChlnForLhpvBalCash" , method = RequestMethod.POST)
	public @ResponseBody Object verifyChlnForLhpvBalCash(@RequestBody String chlnCode){
		System.out.println("LhpvAdvCntlr.verifyChlnForLhpvBalCash()");
		Map<String, Object> map = new HashMap<>();
		map = challanDAO.verifyChlnForLhpvBalCash(chlnCode);
		return map;
	}
	
	
	@RequestMapping(value = "/verifyChlnForLhpvBalRvrs" , method = RequestMethod.POST)
	public @ResponseBody Object verifyChlnForLhpvBalRvrs(@RequestBody Map<String,String> chlnData){
		System.out.println("LhpvAdvCntlr.verifyChlnForLhpvBalRvrs()");
		Map<String, Object> map = new HashMap<>();
		map = challanDAO.verifyChlnForLhpvBalRvrs(chlnData);
		return map;
	}
	
	//TODO Back Data
	@RequestMapping(value = "/getLhpvBalDetBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getLhpvBalDetBack() {  
		System.out.println("Enter into getLhpvBalDetBack---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				//CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				List<Map<String,Object>> bnkList = new ArrayList<>();
				bnkList = bankMstrDAO.getBnkNameAndCode(branchCode);
				LhpvStatus lhpvStatus = lhpvStatusDAO.getLastLhpvStatusBack(branchCode);
				if(lhpvStatus != null){
					System.out.println("lhpvStatus: "+lhpvStatus.getbCode());
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put("lhpvStatus",lhpvStatus);
					map.put("bnkList",bnkList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}
	
	
	@RequestMapping(value = "/submitLhpvBalBrh" , method = RequestMethod.POST)  
	public @ResponseBody Object submitLhpvBalBrh(@RequestBody VoucherService voucherService) {  
		System.out.println("Enter into submitLhpvBal---->");
		
		Map<String, Object> map = new HashMap<>();	
		
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try{
			
			int lbNo = lhpvStatusDAO.saveLhpvBalByBrh(voucherService,session);

			session.flush();
			session.clear();
			transaction.commit();
			
			if(lbNo==1)
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			else
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			
		}catch(Exception e){
			e.printStackTrace();
			transaction.rollback();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			logger.info("Exception in submitlhpvbal save "+e);
		}finally{
			session.close();
		}
		
		return map;
	}	


	@RequestMapping(value = "/verifyChlnForLhpvBalN" , method = RequestMethod.POST)
	public @ResponseBody Object verifyChlnForLhpvBalN(@RequestBody Map clientMap){
		System.out.println("LhpvAdvCntlr.verifyChlnForLhpvBalN()");
		Map<String, Object> map = new HashMap<>();
		map = challanDAO.verifyChlnForLhpvBal(clientMap);
		return map;
	}

	
	@RequestMapping(value = "/getChlnFrLBHO" , method = RequestMethod.POST)  
	public @ResponseBody Object getChlnFrLBHO(@RequestBody Map<String,String> clientMap) {
		System.out.println("Enter into getChlnFrLBHO---->");
		Map<String, Object> map = new HashMap<>();
		String chlnCode = clientMap.get("chlnCode");
		String brkOwnFaCode = clientMap.get("brkOwnFaCode");
		float tdsPer = (float) 0.0;
		
		if(brkOwnFaCode != null){
			if(brkOwnFaCode.substring(0,2).equals("08")){
				Owner own = ownerDAO.getOwnByFaCode(brkOwnFaCode);
				if(own != null){
					tdsPer = own.getOwnPanIntRt();
				}
			}else if(brkOwnFaCode.substring(0,2).equals("09")){
				Broker brk = brokerDAO.getBrkByFaCode(brkOwnFaCode);
				if(brk != null){
					tdsPer = brk.getBrkPanIntRt();
				}
			}else{
				System.out.println("invalid owner/broker code");
			}
		}
		
		System.out.println("value of tdsPer = "+tdsPer);
		
		if(chlnCode != null){
			List<Challan> chlnList = challanDAO.getChallanList(chlnCode);
			if(!chlnList.isEmpty()){
				
				List list=remarksDAO.verifyFrBal(chlnCode);
				System.out.println("list size=="+list.size());
				if(!list.isEmpty()){
					map.put("result", "hold");
					return map;
				}
				int res = challanDAO.checkLB(chlnList.get(0).getChlnId());
				
				double chlnBal = chlnList.get(0).getChlnRemBal();
				double munsAmt = 0.0;
				double tdsAmt = 0.0;
				double csDis  = 0.0;
				System.out.println("chlnBal = "+chlnBal);
				
				String brhCode = chlnList.get(0).getBranchCode();
				BrhWiseMuns brhWiseMuns = brhWiseMunsDAO.getBrhMuns(Integer.parseInt(brhCode));
				
				if(chlnBal > 0){
					if(brhWiseMuns != null){
						munsAmt = brhWiseMuns.getBwmMuns();
						float csDisPer = brhWiseMuns.getBwmCsDis();
						
						System.out.println("csDisPer = "+csDisPer);
						csDis  = (chlnBal * csDisPer) / 100;
						DecimalFormat df = new DecimalFormat("#.##");
						String angleFormated = df.format(csDis);
						csDis = Double.parseDouble(angleFormated);
						
						tdsAmt = (chlnBal * tdsPer) / 100;
					}else{
						munsAmt = munsianaDAO.getMunsAmt(chlnBal);
						System.out.println("munsAmt === > "+munsAmt);
						csDis = (chlnBal * 0.01);
						tdsAmt = (chlnBal * tdsPer) / 100;
					}
				}
				
				System.out.println("value of tdsAmt = "+tdsAmt);
				map.put("arHoAlw",false);
				if(chlnList.get(0).getChlnArId() > 0){
					ArrivalReport arrivalReport = arrivalReportDAO.getArById(chlnList.get(0).getChlnArId());
					if(arrivalReport.isArIsHOAlw() == true){
						map.put("arHoAlw",true);

						// allow only extra amount
						if (brhCode.equalsIgnoreCase("24")) {
							java.util.Date d = new java.util.Date();
							Long dt = d.getTime();
							Date date = new Date(dt);
							Date date2 = arrivalReport.getArIssueDt();
							Long dif = (date.getTime() - date2.getTime())
									/ (24 * 60 * 60 * 1000);
							System.out.println("date="+dif);
							if (dif <= 7) {
								chlnBal = 0;
							}

						}

						System.out.println("arrivalReport.getArWtShrtg() = "+arrivalReport.getArWtShrtg());
						map.put("wtShrtg",arrivalReport.getArRemWtShrtg());
						map.put("drRcvrWt",arrivalReport.getArRemDrRcvrWt());
						map.put("ltDel",arrivalReport.getArRemLateDelivery());
						map.put("ltAck",arrivalReport.getArRemLateAck());
						map.put("exKm",arrivalReport.getArRemExtKm());
						map.put("ovrHgt",arrivalReport.getArRemOvrHgt());
						map.put("penalty",arrivalReport.getArPenalty());
						map.put("oth",arrivalReport.getArOther());
						map.put("det",arrivalReport.getArRemDetention());
						map.put("unLdg",arrivalReport.getArRemUnloading());
					}else{
						map.put("arHoAlw",false);
					}
					
					if(arrivalReport.getArSrtgDmg()!=null){
						if(arrivalReport.getArSrtgDmg().equalsIgnoreCase("OK") || arrivalReport.isSrtgDmgAlw()==true){
							map.put("arStgDmg", "OK");
						}else{
							map.put("arStgDmg", "DMGSRTG");
							map.put("result", "SRTG");
							return map;
						}
					
					}else{
						map.put("arStgDmg", "OK");
					}
				}else{
					map.put("wtShrtg",0);
					map.put("drRcvrWt",0);
					map.put("ltDel",0);
					map.put("ltAck",0);
					map.put("exKm",0);
					map.put("ovrHgt",0);
					map.put("penalty",0);
					map.put("oth",0);
					map.put("det",0);
					map.put("unLdg",0);
				}
				
				//deduct tds once 
				/*if(res > 0){
					map.put("isCal",false);
				}else{
					map.put("isCal",true);
				}*/
				
				//deduct tds multiple time
				map.put("isCal",true);
				
				map.put("tdsPer",tdsPer);
				map.put("chln",chlnList.get(0));
				map.put("munsAmt",munsAmt);
				map.put("tdsAmt",tdsAmt);
				map.put("csDis",csDis);
				map.put("chlnBal",chlnBal);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	

	
	
	@RequestMapping(value = "/getChallanForHoLbPay" , method = RequestMethod.POST)
	public @ResponseBody Object getChallanForHoLbPay(@RequestBody String bankCode){
		System.out.println("LhpvAdvCntlr.getChallanForHoLbPay()");
		Map<String, Object> map = new HashMap<>();
		map = challanDAO.getChallanForHoLbPay(bankCode);
		return map;
	}

	
	@RequestMapping(value = "/verifyChlnForLhpvBalHo" , method = RequestMethod.POST)
	public @ResponseBody Object verifyChlnForLhpvBalHo(@RequestBody String chlnCode){
		System.out.println("LhpvAdvCntlr.verifyChlnForLhpvBalHo()");
		Map<String, Object> map = new HashMap<>();
		map = challanDAO.verifyChlnForLhpvBalHO(chlnCode);
		return map;
	}
	

	
	@RequestMapping(value = "/submitLhpvBalHo" , method = RequestMethod.POST)  
	public @ResponseBody Object submitLhpvBalHo(@RequestBody VoucherService voucherService) {  
		System.out.println("Enter into submitLhpvBalHo---->");
		
		Map<String, Object> map = new HashMap<>();	
		
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try{
			
			int i = arrivalReportDAO.saveArRemBal(voucherService,session);
			int lbNo = lhpvStatusDAO.saveLhpvBalHo(voucherService,session);

			if(lbNo > 0 && i>0){
				map.put("lbNo",lbNo);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}	
			
			session.flush();
			session.clear();
			transaction.commit();
		}catch(Exception e){
			e.printStackTrace();
			transaction.rollback();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			logger.info("Exception in submitlhpvbal save "+e);
		}finally{
			session.close();
		}
		
		return map;
	}	

	
	
	//By Manoj/Kamal
	//data fetch for excel file
	@RequestMapping(value="/fetchLhpvBalData", method=RequestMethod.POST)
	public @ResponseBody Object getLhpvBalData(@RequestBody Map<String,Object> map)
	{
		Map<String, Object> responseMap=new HashMap<>();
	String excelType=map.get("excelData").toString();
	List<LhpvBal> lhpvBalList=new ArrayList<>();
	if(excelType.equalsIgnoreCase("P")){
		lhpvBalList = lhpvStatusDAO.getLhpvBalFrPtro(Date.valueOf((map.get("date").toString())));
	}else if(excelType.equalsIgnoreCase("B")){
		lhpvBalList = lhpvStatusDAO.getLhpvBalFrBank(Date.valueOf((map.get("date").toString())));
	}
   	if(!lhpvBalList.isEmpty())
	{
		this.gLhpvBalList=lhpvBalList;
		responseMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		responseMap.put("excelType", excelType);
	}
	else
	{
		responseMap.put(ConstantsValues.RESULT, "No Data Found");	
	}
    return responseMap;
	}
	
	
	//By Manoj
	// generate excel file and update lhpvBal table to avoid to regenerate same same excel file
	@RequestMapping(value="/getLhpvBalRptBankXLSX",method=RequestMethod.POST)
	public void downloadLhpvBalBank(HttpServletResponse response)
	{
		
		List<Branch> branchList=branchDAO.getAllActiveBranches();
		Map<String, Branch> branchMap=new HashMap<>();
		for (Branch branch : branchList) {
			branchMap.put(branch.getBranchCode(), branch);
			
		}
		List<LhpvBal> lhpvBalList=null;
		if(!this.gLhpvBalList.isEmpty())
		{
		    lhpvBalList=this.gLhpvBalList;
		}
		
		
		System.out.println("Here........................................");
		if(lhpvBalList!=null)
		{
			
		
		XSSFWorkbook workbook=new XSSFWorkbook();
		
	    //Header font
  		XSSFFont fontHeader = workbook.createFont();
  		fontHeader.setFontHeightInPoints((short)11);
  		fontHeader.setColor(IndexedColors.WHITE.getIndex());
  		fontHeader.setFontName(FontFactory.COURIER_BOLD);
  		fontHeader.setBold(true);
		
  		//Header style
  		XSSFCellStyle styleHeader = workbook.createCellStyle();
  		styleHeader.setFont(fontHeader);
  		styleHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
  		styleHeader.setFillForegroundColor(IndexedColors.BLUE_GREY.getIndex());
  		styleHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
  		styleHeader.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
  		styleHeader.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
  		styleHeader.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
  		styleHeader.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
		
  		
		XSSFCellStyle styleData = workbook.createCellStyle();
		styleData.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		
	    XSSFSheet sheet=workbook.createSheet("Lhpv Bal");
	    sheet.setColumnWidth(0, 20*256);
	    sheet.setColumnWidth(1, 25*256);
	    sheet.setColumnWidth(2, 35*256);
	    sheet.setColumnWidth(3, 30*256);
	    sheet.setColumnWidth(4, 30*256);
	    sheet.setColumnWidth(5, 30*256);
	    sheet.setColumnWidth(6, 30*256);
	    sheet.setColumnWidth(7, 30*256);
	    sheet.setColumnWidth(8, 30*256);
	    sheet.setColumnWidth(9, 30*256);
	    sheet.setColumnWidth(10, 30*256);
	    sheet.setColumnWidth(11, 30*256);
	    sheet.setColumnWidth(12, 40*256);
	    sheet.setColumnWidth(13, 40*256);
	    sheet.setColumnWidth(14, 30*256);
	    sheet.setColumnWidth(15, 30*256);
	    sheet.setColumnWidth(16, 30*256);
	    sheet.setColumnWidth(17, 30*256);
	    sheet.setColumnWidth(18, 30*256);
	    sheet.setColumnWidth(19, 30*256);
	    sheet.setColumnWidth(20, 30*256);
	    sheet.setColumnWidth(21, 30*256);
	    sheet.setColumnWidth(22, 30*256);
	    sheet.setColumnWidth(23, 30*256);
	    sheet.setColumnWidth(24, 30*256);
	    sheet.setColumnWidth(25, 30*256);
	    sheet.setColumnWidth(26, 30*256);
	    sheet.setColumnWidth(27, 30*256);
	    sheet.setColumnWidth(28, 30*256);
	    sheet.setColumnWidth(29, 30*256);
	    XSSFRow row=sheet.createRow(0);
	    
	    XSSFCell cell=null;
	    cell=row.createCell(0);
	    cell.setCellValue("Transaction Type");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(1);
	    cell.setCellValue("Beneficiary Code");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(2);
	    cell.setCellValue("Beneficiary Account Number");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(3);
	    cell.setCellValue("Instrument Amount");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(4);
	    cell.setCellValue("Beneficiary Name");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(5);
	    cell.setCellValue("Drawee Location");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(6);
	    cell.setCellValue("Print Location");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(7);
	    cell.setCellValue("Bene Address 1");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(8);
	    cell.setCellValue("Bene Address 2");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(9);
	    cell.setCellValue("Bene Address 3");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(10);
	    cell.setCellValue("Bene Address 4");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(11);
	    cell.setCellValue("Bene Address 5");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(12);
	    cell.setCellValue("Instruction Reference Number");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(13);
	    cell.setCellValue("Customer Reference Number");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(14);
	    cell.setCellValue("Payment details 1");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(15);
	    cell.setCellValue("Payment details 2");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(16);
	    cell.setCellValue("Payment details 3");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(17);
	    cell.setCellValue("Payment details 4");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(18);
	    cell.setCellValue("Payment details 5");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(19);
	    cell.setCellValue("Payment details 6");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(20);
	    cell.setCellValue("Payment details 7");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(21);
	    cell.setCellValue("Cheque Number");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(22);
	    cell.setCellValue("Chq/Tm Date");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(23);
	    cell.setCellValue("MICR Number");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(24);
	    cell.setCellValue("IFSC Code");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(25);
	    cell.setCellValue("Bene Bank Name");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(26);
	    cell.setCellValue("Bene Bank Branch Name ");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(27);
	    cell.setCellValue("Beneficiary email id");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(28);
	    cell.setCellValue("Challan No.");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(29);
	    cell.setCellValue("Challan Branch");
	    cell.setCellStyle(styleHeader);
	    
	    int rowToCreate=1;
	    XSSFRow dataRow=null;
	    XSSFCell dataCell=null;
	    
	    SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
	    for (int i=0;i<lhpvBalList.size();i++) {
	    		
			String ifscCode = lhpvBalList.get(i).getLbIfscCode();

	    	dataRow=sheet.createRow(rowToCreate);
	    	dataCell=dataRow.createCell(0);

	    	if (ifscCode.substring(0, 4).equalsIgnoreCase("HDFC")) {
				dataCell.setCellValue("I");
			} else {
				if (lhpvBalList.get(i).getLbFinalTot() > 200000) {
					dataCell.setCellValue("R");
				} else {
					dataCell.setCellValue("N");
				}
			}
	    	
	    	
	    	dataCell=dataRow.createCell(1);
	    	dataCell.setCellValue(branchMap.get(lhpvBalList.get(i).getbCode()).getBranchName());
	    	
	    	dataCell=dataRow.createCell(2);
	    	dataCell.setCellValue(lhpvBalList.get(i).getLbAccountNo());
	    	
	    	dataCell=dataRow.createCell(3);
	    	dataCell.setCellValue(lhpvBalList.get(i).getLbFinalTot());
	    	
	    	dataCell=dataRow.createCell(4);
	    	dataCell.setCellValue(lhpvBalList.get(i).getLbPayeeName());
	    	
	    	dataCell=dataRow.createCell(5);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(6);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(7);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(8);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(9);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(10);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(11);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(12);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(13);
	    	if(lhpvBalList.get(i).getLbPayeeName().length()>12){
	    		dataCell.setCellValue(lhpvBalList.get(i).getLbPayeeName().substring(0, 12));
	    	}else{
	    		dataCell.setCellValue(lhpvBalList.get(i).getLbPayeeName());
	    	}
	    	
	    	dataCell=dataRow.createCell(14);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(15);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(16);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(17);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(18);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(19);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(20);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(21);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(22);
	    	dataCell.setCellValue(dateFormat.format(lhpvBalList.get(i).getLbDt()));
	    	
	    	dataCell=dataRow.createCell(23);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(24);
	    	dataCell.setCellValue(lhpvBalList.get(i).getLbIfscCode());
	    	
	    	dataCell=dataRow.createCell(25);
	    	dataCell.setCellValue(lhpvBalList.get(i).getLbBankName());
	    	
	    	dataCell=dataRow.createCell(26);
	    	dataCell.setCellValue(branchMap.get(lhpvBalList.get(i).getbCode()).getBranchName());
	    	
	    	dataCell=dataRow.createCell(27);
	    	dataCell.setCellValue(branchMap.get(lhpvBalList.get(i).getChallan().getbCode()).getBranchEmailId());
	    	
	    	
	    	dataCell=dataRow.createCell(28);
	    	dataCell.setCellValue(lhpvBalList.get(i).getChallan().getChlnCode());
	    	
	    	dataCell=dataRow.createCell(29);
	    	dataCell.setCellValue(branchMap.get(lhpvBalList.get(i).getChallan().getbCode()).getBranchName());
	    	
	    	rowToCreate++;
	    	
	    	}
	    
	 	response.setContentType("application/vnd.ms-excel");
    		response.setHeader("Content-Disposition", "attachment; filename=LhpvBalInfo.xlsx");
    		ServletOutputStream out;
			try {
				out = response.getOutputStream();
				workbook.write(out);
	    		out.flush();
	    		out.close();
	    		
	    	int val=lhpvStatusDAO.updateLhpvBal(this.gLhpvBalList);
	    	System.out.println("Value of Val is  "+val);
	    		
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				this.gLhpvBalList.clear();
			}
			
		}
	
	}	
	
	//By Manoj
	// generate excel file and update lhpvBal table to avoid to regenerate same same excel file
	@RequestMapping(value="/getLhpvBalPtroXLSX",method=RequestMethod.POST)
	public void downloadLhpvBalPetro(HttpServletResponse response)
	{
		
		
		List<Branch> branchList=branchDAO.getAllActiveBranches();
		Map<String, Object> branchMap=new HashMap<>();
		for (Branch branch : branchList) {
			branchMap.put(branch.getBranchCode(), branch);
		}
		
		// creating the workbook to create a excel sheet
		XSSFWorkbook workbook=new XSSFWorkbook();
		
		
		// creating a excel sheet 
	    XSSFSheet sheet=workbook.createSheet("LhpvBal Data");
	 
	    // creating the first row in the sheet
	    XSSFRow row=sheet.createRow(0);

	    

	    // getting the XSSFFont instance from the workbook
	    XSSFFont font=workbook.createFont();
	    font.setBold(true);
	    
	    // getting the XSSFFont instance from the workbook
	    XSSFFont font2=workbook.createFont();
	    font2.setBold(true);
	    font2.setFontHeightInPoints((short) 15);
	   // font2.setColor(HSSFColor.BLACK.index);

	    
	    // getting the cell style instance from the workbook
	    XSSFCellStyle cellStyle= workbook.createCellStyle();
	    cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	    cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
	    cellStyle .setFont(font);
	    
	    
	    XSSFCellStyle cellStyle3= workbook.createCellStyle();
	    cellStyle3.setAlignment(CellStyle.ALIGN_RIGHT);
	    cellStyle3.setVerticalAlignment(VerticalAlignment.CENTER);
	    cellStyle3 .setFont(font2);

	    
	// getting the cell style instance from the workbook
	    XSSFCellStyle cellStyle2= workbook.createCellStyle();
	    cellStyle2.setAlignment(CellStyle.ALIGN_CENTER);
	    cellStyle2.setWrapText(true);
	    cellStyle2 .setFont(font);
	    // to format data as Numeric
	  //  cellStyle2.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
	    
	    // creating a date format
	    
	   SimpleDateFormat dateFormat=new SimpleDateFormat("dd.MM.yyyy");
	  

	    XSSFCell cell;
	    
	    cell=  row.createCell(0);
	    cell.setCellStyle(cellStyle);
	    cell.setCellValue("Branch");
	    
	    cell=  row.createCell(1);
	    cell.setCellStyle(cellStyle);
	    cell.setCellValue("Email Id");
	    
	    cell=  row.createCell(2);
	    cell.setCellStyle(cellStyle);
	    cell.setCellValue("ChallanNo.");
	    
	    cell=  row.createCell(3);
	    cell.setCellStyle(cellStyle);
	    cell.setCellValue("Challan Date");
	    
	    cell=  row.createCell(4);
	    cell.setCellStyle(cellStyle);
	    cell.setCellValue("Lb Date");
	    
	    cell=  row.createCell(5);
	    cell.setCellStyle(cellStyle);
	    cell.setCellValue("Lorry No.");
	    
	    cell=  row.createCell(6);
	    cell.setCellStyle(cellStyle);
	    cell.setCellValue("Card Type");
	    
	    cell=  row.createCell(7);
	    cell.setCellStyle(cellStyle2);
	    cell.setCellValue("Card No.");
	    
	    cell=  row.createCell(8);
	    cell.setCellStyle(cellStyle);
	    cell.setCellValue("Final Total");
	    
	    
	    if(this.gLhpvBalList!=null)
	    {
	    	 int rowstocreate=1;
	    	  XSSFRow xssfRow=null;
	    	  double totalFinalTOT=0.0;
	    	for(LhpvBal lhpvBal:gLhpvBalList)
	    	{

	    		 xssfRow= sheet.createRow(rowstocreate);
	    		Branch brh=(Branch) branchMap.get(lhpvBal.getChallan().getbCode());
	    		cell= xssfRow.createCell(0);
		    	cell.setCellValue(brh.getBranchName());
		    	
		    	cell= xssfRow.createCell(1);
		    	cell.setCellValue(brh.getBranchEmailId());
		    	/*if(lhpvBal.getLbBankCode().equalsIgnoreCase("0700044"))
		    		cell.setCellValue("H.P.");
		    	if(lhpvBal.getLbBankCode().equalsIgnoreCase("0700046"))
		    		cell.setCellValue("B.P.");
		    	if(lhpvBal.getLbBankCode().equalsIgnoreCase("0700003"))
		    		cell.setCellValue("HDFC");
		    	if(lhpvBal.getLbBankCode().equalsIgnoreCase("0700010"))
		    		cell.setCellValue("KOTAK");
		    	if(lhpvBal.getLbBankCode().equalsIgnoreCase("0700047"))
		    		cell.setCellValue("I.O.");*/
	    		
	    		cell= xssfRow.createCell(2);
	    		cell.setCellValue(lhpvBal.getChallan().getChlnCode());
	    		
	    		cell= xssfRow.createCell(3);
	    		cell.setCellValue(dateFormat.format(lhpvBal.getChallan().getChlnDt()));
	    		
	    		cell= xssfRow.createCell(4);
	    		cell.setCellValue(dateFormat.format(lhpvBal.getLbDt()));
	    		
	    		cell= xssfRow.createCell(5);
	    		cell.setCellValue(lhpvBal.getChallan().getChlnLryNo());
	    		
	    		cell= xssfRow.createCell(6);
	    		cell.setCellValue(lhpvBal.getPayToStf());
	    		
	    		cell= xssfRow.createCell(7);
	    		cell.setCellValue(lhpvBal.getLbAccountNo());

	    		
	    		cell= xssfRow.createCell(8);
	    		cell.setCellValue(lhpvBal.getLbFinalTot());
	    		
	    		totalFinalTOT=totalFinalTOT+lhpvBal.getLbFinalTot();
                   
	    		rowstocreate++;
	    	}
	    	XSSFRow xssfRow2=sheet.createRow(rowstocreate+1);
	    	XSSFCell xssfCell=xssfRow2.createCell(0);
	    	xssfCell.setCellStyle(cellStyle3);
	    	xssfCell.setCellValue("Total  ");
	    	
	    	XSSFCell xssfCell2=xssfRow2.createCell(8);
	    	xssfCell2.setCellStyle(cellStyle3);
	    	xssfCell2.setCellValue(Math.round(totalFinalTOT));
	    	
	    	sheet.addMergedRegion(new CellRangeAddress(rowstocreate+1, rowstocreate+1, 0, 4));
	    	  // Defining the size to a cell automatically
	    	 for (int i = 0; i < 20; i++) {
	    				sheet.autoSizeColumn(i);
	    			}
	    	 	response.setContentType("application/vnd.ms-excel");
	    		response.setHeader("Content-Disposition", "attachment; filename=LhpvBalInfo.xlsx");
	    		ServletOutputStream out;
				try {
					out = response.getOutputStream();
					workbook.write(out);
		    		out.flush();
		    		out.close();
		    		
		    	int val=lhpvStatusDAO.updateLhpvBal(this.gLhpvBalList);
		    	System.out.println("Value of Val is  "+val);
		    		
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					this.gLhpvBalList.clear();
				}
	    
	    }
	    
		
	}
	
	@RequestMapping(value = "/getLhpvBalDetFrHoBalPay" , method = RequestMethod.POST)  
	public @ResponseBody Object getLhpvBalDetFrHoBalPay(@RequestBody String branchCode) {  
		System.out.println("Enter into getLhpvBalDetFrHoBalPay---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
//		Date date=Date.valueOf("2018-04-01");
		if(user != null){
			//String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				//CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				List<Map<String,Object>> bnkList = new ArrayList<>();
				
			 	Map map1=new HashMap<String, Object>();
				map1.put("bnkFaCode","0700003");
				map1.put("bnkName", "HDFC BANK LTD.");
				bnkList.add(map1);
				map1=new HashMap<String, Object>();
				map1.put("bnkFaCode","0700010");
				map1.put("bnkName", "KOTAK MAHINDRA BANK LTD.");
				bnkList.add(map1);
				map1=new HashMap<String, Object>();
				map1.put("bnkFaCode","0700044");
				map1.put("bnkName", "HINDUSTAN PETROLIUM");
				bnkList.add(map1);
				map1=new HashMap<String, Object>();
				map1.put("bnkFaCode","0700046");
				map1.put("bnkName", "BHARAT PETROLIUM");
				bnkList.add(map1);
				map1=new HashMap<String, Object>();
				map1.put("bnkFaCode","0700047");
				map1.put("bnkName", "INDIAN OIL");
				bnkList.add(map1);
				
				//bnkList = bankMstrDAO.getBnkNameAndCode(branchCode);
				LhpvStatus lhpvStatus = lhpvStatusDAO.getLastLhpvStatus(branchCode);
				if(lhpvStatus != null){
					System.out.println("lhpvStatus: "+lhpvStatus.getbCode());
//					if(!lhpvStatus.getbCode().equalsIgnoreCase("1") && lhpvStatus.getLsDt().compareTo(date)>=0){
//						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
//						System.out.println("lsDate= "+lhpvStatus.getLsDt()+" branchCode"+lhpvStatus.getbCode());
//					}else{
						map.put(ConstantsValues.BRANCH, branchList.get(0));
						map.put("lhpvStatus",lhpvStatus);
						map.put("bnkList",bnkList);
						map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				//	}
					
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}
	

	
	
	@RequestMapping(value = "/getChlnFrLBOld" , method = RequestMethod.POST)  
	public @ResponseBody Object getChlnFrLBOld(@RequestBody Map<String,String> clientMap) {  
		System.out.println("Enter into getChlnFrLB---->");	
		Map<String, Object> map = new HashMap<>();	
		String chlnCode = clientMap.get("chlnCode");
		String brkOwnFaCode = clientMap.get("brkOwnFaCode");
		float tdsPer = (float) 0.0;
		
		if(brkOwnFaCode != null){
			if(brkOwnFaCode.substring(0,2).equals("08")){
				Owner own = ownerDAO.getOwnByFaCode(brkOwnFaCode);
				if(own != null){
					tdsPer = own.getOwnPanIntRt();
				}
			}else if(brkOwnFaCode.substring(0,2).equals("09")){
				Broker brk = brokerDAO.getBrkByFaCode(brkOwnFaCode);
				if(brk != null){
					tdsPer = brk.getBrkPanIntRt();
				}
			}else{
				System.out.println("invalid owner/broker code");
			}
		}
		
		System.out.println("value of tdsPer = "+tdsPer);
		
		if(chlnCode != null){
			List<Challan> chlnList = challanDAO.getChallanList(chlnCode);
			if(!chlnList.isEmpty()){
				
				List list=remarksDAO.verifyFrBal(chlnCode);
				System.out.println("list size=="+list.size());
				if(!list.isEmpty()){
					map.put("result", "hold");
					return map;
				}
				int res = challanDAO.checkLB(chlnList.get(0).getChlnId());
				
				double chlnBal = chlnList.get(0).getChlnRemBal();
				double munsAmt = 0.0;
				double tdsAmt = 0.0;
				double csDis  = 0.0;
				System.out.println("chlnBal = "+chlnBal);
				
				String brhCode = chlnList.get(0).getBranchCode();
				BrhWiseMuns brhWiseMuns = brhWiseMunsDAO.getBrhMuns(Integer.parseInt(brhCode));
				
				if(chlnBal > 0){
					if(brhWiseMuns != null){
						munsAmt = brhWiseMuns.getBwmMuns();
						float csDisPer = brhWiseMuns.getBwmCsDis();
						
						System.out.println("csDisPer = "+csDisPer);
						csDis  = (chlnBal * csDisPer) / 100;
						DecimalFormat df = new DecimalFormat("#.##");
						String angleFormated = df.format(csDis);
						csDis = Double.parseDouble(angleFormated);
						
						tdsAmt = (chlnBal * tdsPer) / 100;
					}else{
						munsAmt = munsianaDAO.getMunsAmt(chlnBal);
						System.out.println("munsAmt === > "+munsAmt);
						csDis = (chlnBal * 0.01);
						tdsAmt = (chlnBal * tdsPer) / 100;
					}
				}
				
				System.out.println("value of tdsAmt = "+tdsAmt);
				map.put("arHoAlw",false);
				if(chlnList.get(0).getChlnArId() > 0){
					ArrivalReport arrivalReport = arrivalReportDAO.getArById(chlnList.get(0).getChlnArId());
					if(arrivalReport.isArIsHOAlw() == true){
						map.put("arHoAlw",true);
						

						// allow only extra amount
						/*if (brhCode.equalsIgnoreCase("24")) {
							java.util.Date d = new java.util.Date();
							Long dt = d.getTime();
							Date date = new Date(dt);
							Date date2 = arrivalReport.getArIssueDt();
							Long dif = (date.getTime() - date2.getTime())
									/ (24 * 60 * 60 * 1000);
							System.out.println("date="+ dif);
							if (dif <= 15) {
								chlnBal = 0;
							}

						}*/
						
						System.out.println("arrivalReport.getArWtShrtg() = "+arrivalReport.getArWtShrtg());
						/*map.put("wtShrtg",arrivalReport.getArWtShrtg());
                        map.put("drRcvrWt",arrivalReport.getArDrRcvrWt());
                        map.put("ltDel",arrivalReport.getArLateDelivery());
                        map.put("ltAck",arrivalReport.getArLateAck());
                        map.put("exKm",arrivalReport.getArExtKm());
                        map.put("ovrHgt",arrivalReport.getArOvrHgt());
                        map.put("penalty",arrivalReport.getArPenalty());
                        map.put("oth",arrivalReport.getArOther());
                        map.put("det",arrivalReport.getArDetention());
                        map.put("unLdg",arrivalReport.getArUnloading());*/
						map.put("wtShrtg",arrivalReport.getArRemWtShrtg());
						map.put("drRcvrWt",arrivalReport.getArRemDrRcvrWt());
						map.put("ltDel",arrivalReport.getArRemLateDelivery());
						map.put("ltAck",arrivalReport.getArRemLateAck());
						map.put("exKm",arrivalReport.getArRemExtKm());
						map.put("ovrHgt",arrivalReport.getArRemOvrHgt());
						map.put("penalty",arrivalReport.getArPenalty());
						map.put("oth",arrivalReport.getArOther());
						map.put("det",arrivalReport.getArRemDetention());
						map.put("unLdg",arrivalReport.getArRemUnloading());
					}else{
						map.put("arHoAlw",false);
					}
					
					if(arrivalReport.getArSrtgDmg()!=null){
						if(arrivalReport.getArSrtgDmg().equalsIgnoreCase("OK") || arrivalReport.isSrtgDmgAlw()==true){
							map.put("arStgDmg", "OK");
						}else{
							map.put("arStgDmg", "DMGSRTG");
							map.put("result", "SRTG");
							return map;
						}
					
					}else{
						map.put("arStgDmg", "OK");
					}
				}else{
					map.put("wtShrtg",0);
					map.put("drRcvrWt",0);
					map.put("ltDel",0);
					map.put("ltAck",0);
					map.put("exKm",0);
					map.put("ovrHgt",0);
					map.put("penalty",0);
					map.put("oth",0);
					map.put("det",0);
					map.put("unLdg",0);
				}
				
				//deduct tds once 
				/*if(res > 0){
					map.put("isCal",false);
				}else{
					map.put("isCal",true);
				}*/
				
				//deduct tds multiple time
				map.put("isCal",true);
				
				map.put("tdsPer",tdsPer);
				map.put("chln",chlnList.get(0));
				map.put("munsAmt",munsAmt);
				map.put("tdsAmt",tdsAmt);
				map.put("csDis",csDis);
				map.put("chlnBal",chlnBal);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	
	
	@RequestMapping(value = "/downloadBnkImgByCode", method = RequestMethod.POST)
	public void downloadBnkImgByCode(HttpServletRequest request, HttpServletResponse response){	
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if(user != null){
			logger.info("UserID = "+user.getUserId()+" : Enter into /downloadImg()");		
			try{
				if(blob != null){
					ServletOutputStream out = response.getOutputStream();
					out.write(blob.getBytes(1, (int) blob.length()));
					out.close();
					blob = null;
				}					
			}catch(Exception e){
				logger.error("Exception : "+e);
			}
		}		
		logger.info("UserID = "+user.getUserId()+" : Exit from /downloadImg()");
	}
	
	@RequestMapping(value = "/isDownloadImgByCode", method = RequestMethod.POST)
	public @ResponseBody Object isDownloadImgByCode(@RequestBody Map<String, String> initParam){	
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if(user != null){
			logger.info("UserID = "+user.getUserId()+" : Enter into /isDownloadImg() : InitParam = "+initParam);		
			Session session = null;
			try{
				session = this.sessionFactory.openSession();
				blob = allImgDao.downloadImgByCode(session, user, initParam);				
				if(blob == null){	
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Image not found !");					
				}else
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}catch(Exception e){
				logger.error("Exception : "+e);
			}finally{
				session.clear();
				session.close();
			}
		}		
		logger.info("UserID = "+user.getUserId()+" : Exit from /isDownloadImg()");
		return resultMap;
	}
	
	
	
}

package com.mylogistics.controller.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CustomerGroupDAO;
import com.mylogistics.DAO.report.EnquiryDAO;
import com.mylogistics.model.Bill;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CustomerGroup;
import com.mylogistics.model.MoneyReceipt;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class EnquiryCntlr {
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private EnquiryDAO enquiryDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private CustomerGroupDAO customerGroupDAO;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@RequestMapping(value="/getUserBrNBrList", method=RequestMethod.POST)
	public @ResponseBody Object getActiveBrNCI(){
		
		Map<String, Object> map = new HashMap<>();
		Map<String, Object> userBranchMap = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		
		List<Map<String, Object>> branchNCIList = branchDAO.getActiveBrNCI();
		for (Map<String, Object> branchMap : branchNCIList) {
			if (String.valueOf(branchMap.get("branchId")).equalsIgnoreCase(currentUser.getUserBranchCode())) {
				userBranchMap = branchMap;
				break;
			}
		}
		
		if (!branchNCIList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("branchList", branchNCIList);
			map.put("userBranch", userBranchMap);
		} else {
			map.put(ConstantsValues.RESULT,  ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value="/getUserBrCustNBrList", method=RequestMethod.POST)
	public @ResponseBody Object getUserBrCustNBrList(){
		
		Map<String, Object> map = new HashMap<>();
		Map<String, Object> userBranchMap = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		
		Map<String, Object> brNCustNCIList = branchDAO.getActiveBrNCustNCI();
		List<Map<String, Object>> branchList = (List<Map<String, Object>>) brNCustNCIList.get("branchList");
		
		for (Map<String, Object> branchMap : branchList) {
			if (String.valueOf(branchMap.get("branchId")).equalsIgnoreCase(currentUser.getUserBranchCode())) {
				userBranchMap = branchMap;
				break;
			}
		}
		
		List<CustomerGroup> allCustomerGroupList= customerGroupDAO.getAllCustGroupList(sessionFactory);
		
		if (!branchList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("branchList", branchList);
			map.put("userBranch", userBranchMap);
			map.put("custList", brNCustNCIList.get("custList"));
			map.put("allCustGroupList",allCustomerGroupList);
		} else {
			map.put(ConstantsValues.RESULT,  ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value="/getTypeCode", method=RequestMethod.POST)
	public @ResponseBody Object getTypeCode(@RequestBody Map<String, Object> brNTypeMap){ 
		System.out.println("getTypeCode");
		Map<String, Object> map  = new HashMap<>();
		
		System.out.println("branchId: "+brNTypeMap.get("branchId"));
		System.out.println("type: "+brNTypeMap.get("type"));
		
		List<String> typeCodeList = enquiryDAO.getCodeListByType(String.valueOf(brNTypeMap.get("branchId")), String.valueOf(brNTypeMap.get("type")));
		
		if (!typeCodeList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("typeCodeList", typeCodeList);
			map.put("msg", "Success");
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("msg", String.valueOf(brNTypeMap.get("type"))+" not found");
		}
		
		return map;
	}
	
	@RequestMapping(value="/getEnquiry", method=RequestMethod.POST)
	public @ResponseBody Object getEnquiry(@RequestBody Map<String, Object> brTypeNCodeMap){
		System.out.println("getEnquiry()");
		Map<String, Object> map = new HashMap<>();
		List<Map<String, Object>> enquiryList = new ArrayList<Map<String, Object>>();
		
		String type = String.valueOf(brTypeNCodeMap.get("type"));
		String code = String.valueOf(brTypeNCodeMap.get("code"));
		String faCodeFrom = String.valueOf(brTypeNCodeMap.get("faCodeFrom"));
		String faCodeTo = String.valueOf(brTypeNCodeMap.get("faCodeTo"));
		String fromDate = String.valueOf(brTypeNCodeMap.get("fromDate"));
		String toDate = String.valueOf(brTypeNCodeMap.get("toDate"));
				
		System.out.println("Type : "+type);
		System.out.println("Code : "+code);
		System.out.println("FaCodeFrom : "+faCodeFrom);
		System.out.println("FaCodeTo : "+faCodeTo);
		System.out.println("From Date : "+fromDate);
		System.out.println("To Date : "+toDate);
		
		enquiryList = enquiryDAO.getEnquiry(type, code, faCodeFrom, faCodeTo, fromDate, toDate);
		System.out.println("Enquiry List Size : "+enquiryList.size());
		if (!enquiryList.isEmpty()) {			
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("enquiryList", enquiryList);
		}else if(enquiryList.size() == 0){
			map.put(ConstantsValues.RESULT, ConstantsValues.NOT_FOUND);
		}else {			
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}	
	
	@RequestMapping(value="/getBillDetByMr", method=RequestMethod.POST)
	public @ResponseBody Object getBillDetByMr(@RequestBody Map<String, Object> brTypeNCodeMap){
		System.out.println("getBillDetByMr()");
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> map = new HashMap<>();
		if(user != null){			
			List enquiryList = enquiryDAO.getBillDetByMr(String.valueOf(brTypeNCodeMap.get("branchId")), String.valueOf(brTypeNCodeMap.get("code")));
			System.out.println("MR BILL SIZE : "+enquiryList.size());
			if(!enquiryList.isEmpty()){
				map.put("billDetList", enquiryList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else
				map.put(ConstantsValues.RESULT, ConstantsValues.NOT_FOUND);			
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}	
	
	@RequestMapping(value="/getFaCodeByCode", method=RequestMethod.POST)
	public @ResponseBody Object getFaCodeByCode(@RequestBody Map<String, Object> faObj){
		System.out.println("getFaCodeByCode()");
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> map = new HashMap<>();
		if(user != null){			
			List enquiryList = enquiryDAO.getFaCodeByCode(String.valueOf(faObj.get("bCode")), String.valueOf(faObj.get("faCode")));					
			System.out.println("FaCode List Size : "+enquiryList.size());
			if(!enquiryList.isEmpty()){
				map.put("faMasterList", enquiryList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else
				map.put(ConstantsValues.RESULT, ConstantsValues.NOT_FOUND);			
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}	
	
}
package com.mylogistics.controller.report;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DaoSupport;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.mylogistics.DAO.CashStmtTempDAO;
import com.mylogistics.DAO.report.ReportDAO;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.CsTempNCsListService;
import com.mylogistics.services.ReconRptService;

@Controller
public class BrReconRptCntlr {

	@Autowired
	private ReportDAO reportDAO;
	
	@Autowired
	private CashStmtTempDAO cashStmtTempDAO;
	
	@RequestMapping(value="/getReconRpt", method=RequestMethod.POST)
	public @ResponseBody Object getReconRpt(@RequestBody ReconRptService reconRptService){
		System.out.println("getReconRpt()");
		Map<String, Object> reconRptMap = reportDAO.getReconReport(reconRptService);
 		
		if ((int)reconRptMap.get("temp") > 0) {
			reconRptMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			reconRptMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return reconRptMap;
	}
	
	@RequestMapping(value = "/updateCSTemp", method = RequestMethod.POST)
	public @ResponseBody Object updateCSTemp(@RequestBody CsTempNCsListService csTempNcsListService){
		System.out.println("updateCSTemp()");
		Map<String, Object> map = new HashMap<>();
		
		int temp = cashStmtTempDAO.updateCashStmtTemp(csTempNcsListService);
		if (temp > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value="/getCsFrmCsTemp", method = RequestMethod.POST)
	public @ResponseBody Object getCsFrmCsTemp(@RequestBody CashStmtTemp cashStmtTemp){
		System.out.println("BrReconRptCntlr.getCsFrmCsTemp()");
		Map<String, Object> map = new HashMap<>();
		List<CashStmt> cashStmtList = reportDAO.getCsListFrmCsTemp(cashStmtTemp);
		if (!cashStmtList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("cashStmtList", cashStmtList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
}

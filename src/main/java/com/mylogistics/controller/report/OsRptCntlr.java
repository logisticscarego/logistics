package com.mylogistics.controller.report;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import javassist.expr.Instanceof;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itextpdf.text.FontFactory;
import com.mylogistics.DAO.report.ReportDAO;
import com.mylogistics.constants.ArrivalReportCNTS;
import com.mylogistics.constants.BillCNTS;
import com.mylogistics.constants.BillDetailCNTS;
import com.mylogistics.constants.CnmtCNTS;
import com.mylogistics.model.Customer;
import com.mylogistics.model.MoneyReceipt;
import com.mylogistics.model.User;
import com.mylogistics.services.CodePatternService;
import com.mylogistics.services.ConstantsValues;

@Controller
public class OsRptCntlr {

	public static final Logger logger = Logger.getLogger(OsRptCntlr.class);
	@Autowired
	private ReportDAO reportDAO;
	@Autowired
	private HttpSession httpSession;
	@Autowired
	private ServletContext servletContext;
	
	private String osFilePath = "/resources/report/";
	
	@RequestMapping(value="/getOsRpt", method=RequestMethod.POST)
	public @ResponseBody Object getOsRpt(@RequestBody Map<String, Object> osRptService) throws IOException {
		System.out.println("OsRptCntlr.getOsRpt()");
		Map<String, Object> map = new HashMap<>();
		User user = (User) httpSession.getAttribute("currentUser");
		
		System.out.println("branchId: "+osRptService.get("branchId"));
		System.out.println("custId: "+osRptService.get("custId"));
		System.out.println("upToDt: "+osRptService.get("upToDt"));
		System.out.println("selectCust:"+osRptService.get("custGroupId"));
		
		String uptoDate = String.valueOf(osRptService.get("upToDt"));
		httpSession.setAttribute("uptoDate", uptoDate);
		Map<String, Object> osRpt = reportDAO.getOsRpt(osRptService);
		if ((int)osRpt.get("temp") > 0) {
			String fileName = servletContext.getRealPath("/");
			fileName = fileName + osFilePath +user.getUserCode()+"OSReport.xlsx";
			printOsRptXLSX(osRpt, fileName);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value="/getOsRptN", method=RequestMethod.POST)
	public @ResponseBody Object getOsRptN(@RequestBody Map<String, Object> osRptService, HttpServletResponse response, HttpServletRequest request) 
			throws IOException {
		System.out.println("OsRptCntlr.getOsRpt()");
		Map<String, Object> map = new HashMap<>();
		User user = (User) httpSession.getAttribute("currentUser");  
		System.out.println("branchId: "+osRptService.get("branchId"));
		System.out.println("custId: "+osRptService.get("custId"));
		System.out.println("upToDt: "+osRptService.get("upToDt"));
		System.out.println("selectCust:"+osRptService.get("custGroupId"));
		
		String uptoDate = String.valueOf(osRptService.get("upToDt"));
		httpSession.setAttribute("uptoDate", uptoDate);
		Map<String, Object> osRpt = reportDAO.getOsRptN(osRptService);
		if ((int)osRpt.get("temp") > 0) {
			String fileName = servletContext.getRealPath("/");
			fileName = fileName + osFilePath +user.getUserCode()+"OSReport.xlsx";
			printOsRptXLSX(osRpt, fileName);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	public void printOsRptXLSX(Map<String, Object> osRpt, String fileName) throws IOException {

		System.out.println("OsRptCntlr.getOsRptXLSX()");
		
		List<Map<String, Object>> osBillRptList = (List<Map<String, Object>>) osRpt.get("osBillRptList");
		Map<Integer, String> branchMap = (Map<Integer, String>) osRpt.get("branchMap");
		List<Customer> custListTemp = (List<Customer>) osRpt.get("custList");
		List<Customer> custList = new ArrayList<>();
		List<MoneyReceipt> mrList = (List<MoneyReceipt>) osRpt.get("mrList");
		System.out.println("mrList Size="+mrList.size());
		
		for(MoneyReceipt mr:mrList){
			System.out.println(mr.getMrNo()+" "+mr.getMrRemAmt());
		}
		//get only those customer whose bill has been prepared
		Set<Integer> custIdSet = new TreeSet<>();
		for (Map<String, Object> osBillRpt : osBillRptList) {
			custIdSet.add(Integer.parseInt(String.valueOf(osBillRpt.get(BillCNTS.BILL_CUST_ID))));
		}
		
		boolean checkCust=true;
		
		for (Customer customer : custListTemp) {
			for (Integer custId : custIdSet) {
				if (custId == customer.getCustId()) {
					checkCust=false;                         //mk
					custList.add(customer);
					break;
				} 
			}
		}
		
		for(Customer customer:custListTemp){            //mk
		if(checkCust){
			custList.add(customer);
			break;
			}
		}
		
		XSSFWorkbook workbook = new XSSFWorkbook();
		
		//create blank sheet
		XSSFSheet spreadsheet = workbook.createSheet();
		
		//set column width accordingly
		spreadsheet.setColumnWidth(0, 10*256);
		spreadsheet.setColumnWidth(1, 5*256);
		spreadsheet.setColumnWidth(2, 10*256); //date
		spreadsheet.setColumnWidth(3, 7*256); //date
		spreadsheet.setColumnWidth(4, 10*256);
		spreadsheet.setColumnWidth(5, 7*256);
		spreadsheet.setColumnWidth(6, 12*256);
		spreadsheet.setColumnWidth(7, 12*256);
		spreadsheet.setColumnWidth(8, 12*256);
		spreadsheet.setColumnWidth(9, 7*256); //date
		spreadsheet.setColumnWidth(10, 13*256);
		spreadsheet.setColumnWidth(11, 13*256);
		spreadsheet.setColumnWidth(12, 13*256);
		spreadsheet.setColumnWidth(13, 13*256);
		spreadsheet.setColumnWidth(14, 13*256);
		spreadsheet.setColumnWidth(15, 13*256);
		spreadsheet.setColumnWidth(16, 13*256);
		spreadsheet.setColumnWidth(17, 13*256);
		spreadsheet.setColumnWidth(18, 13*256);
		
		//Create row object
		XSSFRow row;
		
		//Heading font
		XSSFFont fontHeading = workbook.createFont();
		fontHeading.setFontHeightInPoints((short)18);
		fontHeading.setFontName(FontFactory.HELVETICA_BOLD);
		fontHeading.setBold(true);
		fontHeading.setUnderline(XSSFFont.U_SINGLE);
		
		//Heading Style
		XSSFCellStyle styleHeading = workbook.createCellStyle();
	    styleHeading.setFont(fontHeading);
	    styleHeading.setAlignment(XSSFCellStyle.ALIGN_CENTER);
	    
	    //Heading row
	    int rowId = 0;
	    String uptoDate = (String) httpSession.getAttribute("uptoDate");
		httpSession.getAttribute("uptoDate");
	    row = spreadsheet.createRow(rowId++);
	    XSSFCell cellHeading = row.createCell(0);
    	cellHeading.setCellStyle(styleHeading);
    	cellHeading.setCellValue("OUTSTANDING REPORT"+
    			"     (Up to Date: "+CodePatternService.getFormatedDateString(uptoDate)+")");
    	
    	spreadsheet.addMergedRegion(new CellRangeAddress(0,0,0,18));
		
	    //blank row
	    rowId++;
		
	    //Header font
  		XSSFFont fontHeader = workbook.createFont();
  		fontHeader.setFontHeightInPoints((short)11);
  		fontHeader.setColor(IndexedColors.WHITE.getIndex());
  		fontHeader.setFontName(FontFactory.COURIER_BOLD);
  		fontHeader.setBold(true);
		
  		//Header style
  		XSSFCellStyle styleHeader = workbook.createCellStyle();
  		styleHeader.setFont(fontHeader);
  		styleHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
  		styleHeader.setFillForegroundColor(IndexedColors.BLUE_GREY.getIndex());
  		styleHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
  		styleHeader.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
  		styleHeader.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
  		styleHeader.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
  		styleHeader.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
  		
  		//subHeader font
  		XSSFFont fontSubHeader = workbook.createFont();
  		fontSubHeader.setFontHeightInPoints((short)10);
  		fontSubHeader.setColor(IndexedColors.BLUE.getIndex());
  		fontSubHeader.setFontName(FontFactory.TIMES);
  		fontSubHeader.setBold(true);
		
  		//subHeader style
  		XSSFCellStyle styleSubHeader = workbook.createCellStyle();
  		styleSubHeader.setFont(fontSubHeader);
  		styleSubHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
  		styleSubHeader.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
  		styleSubHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
  		styleSubHeader.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeader.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeader.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeader.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
  		
  		//subHeaderNumber style
  		XSSFCellStyle styleSubHeaderNumber = workbook.createCellStyle();
  		styleSubHeaderNumber.setFont(fontSubHeader);
  		styleSubHeaderNumber.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
  		styleSubHeaderNumber.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
  		styleSubHeaderNumber.setFillPattern(CellStyle.SOLID_FOREGROUND);
  		styleSubHeaderNumber.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeaderNumber.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeaderNumber.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeaderNumber.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
  		styleSubHeaderNumber.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
  		
  		//table font
  		XSSFFont font = workbook.createFont();
  		font.setFontHeightInPoints((short)10);
  		font.setColor(IndexedColors.BLACK.getIndex());
  		font.setFontName(FontFactory.TIMES);
  		
  		//table style
  		XSSFCellStyle style = workbook.createCellStyle();
  		style.setFont(font);
  		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
  		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
  		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
  		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
  		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
  		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
  		style.setWrapText(true);
  		
  		//table style number
  		XSSFCellStyle styleNumber = workbook.createCellStyle();
  		styleNumber.setFont(font);
  		styleNumber.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
  		styleNumber.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
  		styleNumber.setBorderTop(XSSFCellStyle.BORDER_THIN);
  		styleNumber.setBorderRight(XSSFCellStyle.BORDER_THIN);
  		styleNumber.setBorderBottom(XSSFCellStyle.BORDER_THIN);
  		styleNumber.setBorderLeft(XSSFCellStyle.BORDER_THIN);
  		styleNumber.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
  		//styleNumber.setDataFormat((short)8);
	    
  		System.out.println("sizeof="+custList.size());
  		
         List<Map<String,Object>> listOSTotal=new ArrayList<Map<String,Object>>();
  		
  		for (Customer customer : custList) {
			
			row = spreadsheet.createRow(rowId++);
			Map<String,Object> mapOs=new HashMap<String,Object>();
			 mapOs.put("custName",customer.getCustName());
			 mapOs.put("custFacode",customer.getCustFaCode());
			for (int i = 0; i <= 18; i++) {
				XSSFCell cust = row.createCell(i);
				cust.setCellStyle(styleHeader);
		        if (i == 0) {
		        	cust.setCellValue("A/C CODE: "+customer.getCustFaCode()+
		        			"     NAME: "+customer.getCustName()+"   ("+branchMap.get(Integer.parseInt(customer.getBranchCode())).toUpperCase()+")"+
		        			"     PAYMENT TERMS: "+customer.getCustCrPeriod()+" Days From "+customer.getCustCrBase()+" Date");
		        } 
		    }
			spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,0,17));
			
			//cnmt wise detail
			row = spreadsheet.createRow(rowId++);
  			
			XSSFCell cellBlNo = row.createCell(0);
			cellBlNo.setCellStyle(styleSubHeader);
			cellBlNo.setCellValue("BlNo");
			
	       	XSSFCell cellBlType = row.createCell(1);
	       	cellBlType.setCellStyle(styleSubHeader);
	       	cellBlType.setCellValue("Typ");
	       	
	       	XSSFCell cellBlDt = row.createCell(2);
	       	cellBlDt.setCellStyle(styleSubHeader);
	       	cellBlDt.setCellValue("BlDt");
	       	
	     	XSSFCell cellBlSubDt = row.createCell(3);
	       	cellBlSubDt.setCellStyle(styleSubHeader);
	       	cellBlSubDt.setCellValue("BlSubDt");
	       	
	       	XSSFCell cellCnNo = row.createCell(4);
	       	cellCnNo.setCellStyle(styleSubHeader);
	       	cellCnNo.setCellValue("CnNo");
	       	
	       	XSSFCell cellCnDt = row.createCell(5);
	       	cellCnDt.setCellStyle(styleSubHeader);
	       	cellCnDt.setCellValue("CnDt");
	       	
	       	XSSFCell cellFrmSt = row.createCell(6);
	       	cellFrmSt.setCellStyle(styleSubHeader);
	       	cellFrmSt.setCellValue("FrmStn");
	       	
	       	XSSFCell cellToStn = row.createCell(7);
	       	cellToStn.setCellStyle(styleSubHeader);
	       	cellToStn.setCellValue("ToStn");
	       	
	       	XSSFCell cellInvcNo = row.createCell(8);
	       	cellInvcNo.setCellStyle(styleSubHeader);
	       	cellInvcNo.setCellValue("InvcNo");
	       	
	       	XSSFCell cellDlyDt = row.createCell(9);
	       	cellDlyDt.setCellStyle(styleSubHeader);
	       	cellDlyDt.setCellValue("DlyDt");
	       	
	       	XSSFCell cellCnFrt = row.createCell(10);
	       	cellCnFrt.setCellStyle(styleSubHeader);
	       	cellCnFrt.setCellValue("CnFrt");
	       	
	       	XSSFCell cellOthrs = row.createCell(11);
	       	cellOthrs.setCellStyle(styleSubHeader);
	       	cellOthrs.setCellValue("Othrs");
	       	
	       	XSSFCell cellBlFrt = row.createCell(12);
	       	cellBlFrt.setCellStyle(styleSubHeader);
	       	cellBlFrt.setCellValue("BlFrt");
	       	
	       	XSSFCell cellBlTot = row.createCell(13);
	       	cellBlTot.setCellStyle(styleSubHeader);
	       	cellBlTot.setCellValue("BlTot");
	       	
	   /*   	XSSFCell cellBlSubDt = row.createCell(13);
	       	cellBlTot.setCellStyle(styleSubHeader);
	       	cellBlTot.setCellValue("BlTot");*/
	       	
	       	XSSFCell cellOS0_30 = row.createCell(14);
	       	cellOS0_30.setCellStyle(styleSubHeader);
	       	cellOS0_30.setCellValue("0-30 days");
	       	
	       	XSSFCell cellOS31_60 = row.createCell(15);
	       	cellOS31_60.setCellStyle(styleSubHeader);
	       	cellOS31_60.setCellValue("31-60 days");
	       	
	       	XSSFCell cellOS61_90 = row.createCell(16);
	       	cellOS61_90.setCellStyle(styleSubHeader);
	       	cellOS61_90.setCellValue("61-90 days");
	       	
	       	XSSFCell cellOS91_120 = row.createCell(17);
	       	cellOS91_120.setCellStyle(styleSubHeader);
	       	cellOS91_120.setCellValue("91-120 days");
	       	
	       	XSSFCell cellOS121_more = row.createCell(18);
	       	cellOS121_more.setCellStyle(styleSubHeader);
	       	cellOS121_more.setCellValue("121 Above days");
	       	
	       	String billNoPrevious = "";
	       	double totalCnFrt = 0.0;
	       	double totalOthChg = 0.0;
	       	double totalBlFrt = 0.0;
	       	double totalBlTot = 0.0;
	       	double totalOS0_30 = 0.0;
	       	double totalOS31_60 = 0.0;
	       	double totalOS61_90 = 0.0;
	       	double totalOS91_120 = 0.0;
	       	double totalOS121_more = 0.0;
	       	
	       	for (Map<String, Object> osBillRpt : osBillRptList) {
				if (Integer.parseInt(String.valueOf(osBillRpt.get(BillCNTS.BILL_CUST_ID))) == customer.getCustId()) {
					
					row = spreadsheet.createRow(rowId++);
					
					if (osBillRpt.get(BillCNTS.BILL_TYPE) != null) {
	       				XSSFCell cellBlTypeValue = row.createCell(1);
	       				cellBlTypeValue.setCellStyle(style);
	       				cellBlTypeValue.setCellValue(String.valueOf(osBillRpt.get(BillCNTS.BILL_TYPE)));
					} else {
						XSSFCell cellBlTypeValue = row.createCell(1);
						cellBlTypeValue.setCellStyle(style);
					}
	       			
	    	       	if (osBillRpt.get(BillCNTS.BILL_DT) != null) {
	    	       		XSSFCell cellBlDtValue = row.createCell(2);
	    	       		cellBlDtValue.setCellStyle(style);
	    	       		cellBlDtValue.setCellValue(CodePatternService.getFormatedDateString(String.valueOf(osBillRpt.get(BillCNTS.BILL_DT))));
					} else {
						XSSFCell cellBlDtValue = row.createCell(2);
						cellBlDtValue.setCellStyle(style);
		    	    }
	    	       	
	    	       	if (osBillRpt.get(BillCNTS.BILL_SUB_DT) != null) {
	    	       		XSSFCell cellBlDtValue = row.createCell(3);
	    	       		cellBlDtValue.setCellStyle(style);
	    	       		cellBlDtValue.setCellValue(CodePatternService.getFormatedDateString(String.valueOf(osBillRpt.get(BillCNTS.BILL_SUB_DT))));
					} else {
						XSSFCell cellBlDtValue = row.createCell(3);
						cellBlDtValue.setCellStyle(style);
		    	    }
	    	       	
	    	       	
	    	       	if (osBillRpt.get(CnmtCNTS.CNMT_CODE) != null) {
	    	       		XSSFCell cellCnNoValue = row.createCell(4);
	    	       		cellCnNoValue.setCellStyle(style);
	    	       		cellCnNoValue.setCellValue(String.valueOf(osBillRpt.get(CnmtCNTS.CNMT_CODE)).toLowerCase());
					} else {
						XSSFCell cellCnNoValue = row.createCell(4);
						cellCnNoValue.setCellStyle(style);
		    	    }
	    	       	
	    	       	if (osBillRpt.get(CnmtCNTS.CNMT_DT) != null) {
	    	       		XSSFCell cellCnDtValue = row.createCell(5);
	    	       		cellCnDtValue.setCellStyle(style);
	    	       		cellCnDtValue.setCellValue(CodePatternService.getFormatedDateString(String.valueOf(osBillRpt.get(CnmtCNTS.CNMT_DT))));
					} else {
						XSSFCell cellCnDtValue = row.createCell(5);
						cellCnDtValue.setCellStyle(style);
					}
	    	       	
	    	       	if (osBillRpt.get("frmStnName") != null) {
	    	       		XSSFCell cellFrmStValue = row.createCell(6);
	    	       		cellFrmStValue.setCellStyle(style);
	    	       		cellFrmStValue.setCellValue(String.valueOf(osBillRpt.get("frmStnName")).toLowerCase());
					} else {
						XSSFCell cellFrmStValue = row.createCell(6);
						cellFrmStValue.setCellStyle(style);
					}
	    	       	
	    	       	if (osBillRpt.get("toStnName") != null) {
	    	       		XSSFCell cellToStnValue = row.createCell(7);
	    	       		cellToStnValue.setCellStyle(style);
	    	       		cellToStnValue.setCellValue(String.valueOf(osBillRpt.get("toStnName")).toLowerCase());
					} else {
						XSSFCell cellGWtValue = row.createCell(7);
		    	       	cellGWtValue.setCellStyle(style);
					}
	    	       	
	    	       	if (osBillRpt.get(CnmtCNTS.CNMT_INVOICE_NO) != null) {
	    	       		XSSFCell cellInvcNoValue = row.createCell(8);
	    	       		cellInvcNoValue.setCellStyle(style);
	    	       		cellInvcNoValue.setCellValue(String.valueOf(osBillRpt.get(CnmtCNTS.CNMT_INVOICE_NO)));
					} else {
						XSSFCell cellInvcNoValue = row.createCell(8);
						cellInvcNoValue.setCellStyle(style);
					}
	    	       	
	    	       	if (osBillRpt.get(ArrivalReportCNTS.AR_DATE) != null) {
	    	       		XSSFCell cellDlyDtValue = row.createCell(9);
	    	       		cellDlyDtValue.setCellStyle(style);
	    	       		cellDlyDtValue.setCellValue(CodePatternService.getFormatedDateString(String.valueOf(osBillRpt.get(ArrivalReportCNTS.AR_DATE))));
					} else {
						XSSFCell cellDlyDtValue = row.createCell(9);
						cellDlyDtValue.setCellStyle(style);
					}
	    	       	
	    	       	if (osBillRpt.get(CnmtCNTS.CNMT_FREIGHT) != null) {
	    	       		XSSFCell cellCnFrtValue = row.createCell(10);
	    	       		cellCnFrtValue.setCellStyle(styleNumber);
	    	       		cellCnFrtValue.setCellValue(Double.parseDouble(String.valueOf(osBillRpt.get(CnmtCNTS.CNMT_FREIGHT))));
	    	       		totalCnFrt += Double.parseDouble(String.valueOf(osBillRpt.get(CnmtCNTS.CNMT_FREIGHT)));
					} else {
						XSSFCell cellCnFrtValue = row.createCell(10);
						cellCnFrtValue.setCellStyle(styleNumber);
					}
	    	       	
	    	       	if (osBillRpt.get("othChg") != null) {
	    	       		XSSFCell cellOthChgValue = row.createCell(11);
	    	       		cellOthChgValue.setCellStyle(styleNumber);
	    	       		cellOthChgValue.setCellValue(Double.parseDouble(String.valueOf(osBillRpt.get("othChg"))));
	    	       		totalOthChg += Double.parseDouble(String.valueOf(osBillRpt.get("othChg")));
					} else {
						XSSFCell cellOthChgValue = row.createCell(11);
						cellOthChgValue.setCellStyle(styleNumber);
					}
	    	       	
	    	       	if (osBillRpt.get(BillDetailCNTS.BD_TOT_AMT) != null) {
	    	       		XSSFCell cellBlFrtValue = row.createCell(12);
	    	       		cellBlFrtValue.setCellStyle(styleNumber);
	    	       		cellBlFrtValue.setCellValue(Double.parseDouble(String.valueOf(osBillRpt.get(BillDetailCNTS.BD_TOT_AMT))));
	    	       		totalBlFrt += Double.parseDouble(String.valueOf(osBillRpt.get(BillDetailCNTS.BD_TOT_AMT)));
					} else {
						XSSFCell cellBlFrtValue = row.createCell(12);
						cellBlFrtValue.setCellStyle(styleNumber);
					}
	    	       	
	    	       	if (!billNoPrevious.equalsIgnoreCase(String.valueOf(osBillRpt.get(BillCNTS.Bill_NO)))) {
	    	       		
	    	       		if (osBillRpt.get(BillCNTS.Bill_NO) != null) {
							XSSFCell cellBlNoValue = row.createCell(0);
							cellBlNoValue.setCellStyle(style);
							cellBlNoValue.setCellValue(String.valueOf(osBillRpt.get(BillCNTS.Bill_NO)));
						} else {
							XSSFCell cellBlNoVal = row.createCell(0);
							cellBlNoVal.setCellStyle(style);
						}
	    	       		
	    	       		if (osBillRpt.get(BillCNTS.BILL_FINAL_TOT) != null) {
		    	       		XSSFCell cellBlTotValue = row.createCell(13);
		    	       		cellBlTotValue.setCellStyle(styleNumber);
		    	       		cellBlTotValue.setCellValue(Double.parseDouble(String.valueOf(osBillRpt.get(BillCNTS.BILL_FINAL_TOT))));
		    	       		totalBlTot += Double.parseDouble(String.valueOf(osBillRpt.get(BillCNTS.BILL_FINAL_TOT)));
		    	       	} else {
							XSSFCell cellBlTotValue = row.createCell(13);
							cellBlTotValue.setCellStyle(styleNumber);
						}
		    	       	
		    	       	if (osBillRpt.get(BillCNTS.BILL_FINAL_TOT+"os0-30") != null) {
		    	       		XSSFCell cellOS0_30Value = row.createCell(14);
		    	       		cellOS0_30Value.setCellStyle(styleNumber);
		    	       		cellOS0_30Value.setCellValue(Double.parseDouble(String.valueOf(osBillRpt.get(BillCNTS.BILL_FINAL_TOT+"os0-30"))));
		    	       		totalOS0_30 += Double.parseDouble(String.valueOf(osBillRpt.get(BillCNTS.BILL_FINAL_TOT+"os0-30")));
						} else {
							XSSFCell cellOS0_30Value = row.createCell(14);
							cellOS0_30Value.setCellStyle(styleNumber);
						}
		    	       	
		    	       	if (osBillRpt.get(BillCNTS.BILL_FINAL_TOT+"os31-60") != null) {
		    	       		XSSFCell cellOS31_60Value = row.createCell(15);
		    	       		cellOS31_60Value.setCellStyle(styleNumber);
		    	       		cellOS31_60Value.setCellValue(Double.parseDouble(String.valueOf(osBillRpt.get(BillCNTS.BILL_FINAL_TOT+"os31-60"))));
		    	       		totalOS31_60 += Double.parseDouble(String.valueOf(osBillRpt.get(BillCNTS.BILL_FINAL_TOT+"os31-60")));
						} else {
							XSSFCell cellOS31_60Value = row.createCell(15);
							cellOS31_60Value.setCellStyle(styleNumber);
						}
		    	       	
		    	       	if (osBillRpt.get(BillCNTS.BILL_FINAL_TOT+"os61-90") != null) {
		    	       		XSSFCell cellOS61_90Value = row.createCell(16);
		    	       		cellOS61_90Value.setCellStyle(styleNumber);
		    	       		cellOS61_90Value.setCellValue(Double.parseDouble(String.valueOf(osBillRpt.get(BillCNTS.BILL_FINAL_TOT+"os61-90"))));
		    	       		totalOS61_90 += Double.parseDouble(String.valueOf(osBillRpt.get(BillCNTS.BILL_FINAL_TOT+"os61-90")));
						} else {
							XSSFCell cellOS61_90Value = row.createCell(16);
							cellOS61_90Value.setCellStyle(styleNumber);
						}
		    	       	
		    	       	if (osBillRpt.get(BillCNTS.BILL_FINAL_TOT+"os91-120") != null) {
		    	       		XSSFCell cellOS91_120Value = row.createCell(17);
		    	       		cellOS91_120Value.setCellStyle(styleNumber);
		    	       		cellOS91_120Value.setCellValue(Double.parseDouble(String.valueOf(osBillRpt.get(BillCNTS.BILL_FINAL_TOT+"os91-120"))));
		    	       		totalOS91_120 += Double.parseDouble(String.valueOf(osBillRpt.get(BillCNTS.BILL_FINAL_TOT+"os91-120")));
						} else {
							XSSFCell cellOS91_120Value = row.createCell(17);
							cellOS91_120Value.setCellStyle(styleNumber);
						}
		    	       	
		    	       	if (osBillRpt.get(BillCNTS.BILL_FINAL_TOT+"os121-more") != null) {
		    	       		XSSFCell cellOS121_moreValue = row.createCell(18);
		    	       		cellOS121_moreValue.setCellStyle(styleNumber);
		    	       		cellOS121_moreValue.setCellValue(Double.parseDouble(String.valueOf(osBillRpt.get(BillCNTS.BILL_FINAL_TOT+"os121-more"))));
		    	       		totalOS121_more += Double.parseDouble(String.valueOf(osBillRpt.get(BillCNTS.BILL_FINAL_TOT+"os121-more")));
						} else {
							XSSFCell cellOS121_moreValue = row.createCell(18);
							cellOS121_moreValue.setCellStyle(styleNumber);
						}
		    	       	
		    	       	billNoPrevious = String.valueOf(osBillRpt.get(BillCNTS.Bill_NO));
					} else {
						XSSFCell cellBlNoVal = row.createCell(0);
						cellBlNoVal.setCellStyle(style);
						
						XSSFCell cellBlTotValue = row.createCell(12);
						cellBlTotValue.setCellStyle(styleNumber);
						
						XSSFCell cellOS0_30Value = row.createCell(13);
						cellOS0_30Value.setCellStyle(styleNumber);
						
						XSSFCell cellOS31_60Value = row.createCell(14);
						cellOS31_60Value.setCellStyle(styleNumber);
						
						XSSFCell cellOS61_90Value = row.createCell(15);
						cellOS61_90Value.setCellStyle(styleNumber);
						
						XSSFCell cellOS91_120Value = row.createCell(16);
						cellOS91_120Value.setCellStyle(styleNumber);
						
						XSSFCell cellOS121_moreValue = row.createCell(17);
						cellOS121_moreValue.setCellStyle(styleNumber);
					}
				} 
			}
			
			row = spreadsheet.createRow(rowId++);
			
			for (int i = 0; i <= 9; i++) {
				XSSFCell cust = row.createCell(i);
				cust.setCellStyle(styleSubHeaderNumber);
		        if (i == 0) {
		        	cust.setCellValue("TOTAL    ");
		        } 
		    }
			spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,0,9));
			
			XSSFCell cellTotalCnFrt = row.createCell(10);
			cellTotalCnFrt.setCellStyle(styleSubHeaderNumber);
			cellTotalCnFrt.setCellValue(totalCnFrt);
		    
			XSSFCell cellTotalOthChg = row.createCell(11);
			cellTotalOthChg.setCellStyle(styleSubHeaderNumber);
			cellTotalOthChg.setCellValue(totalOthChg);
			
			XSSFCell cellTotalBlFrt = row.createCell(12);
			cellTotalBlFrt.setCellStyle(styleSubHeaderNumber);
			cellTotalBlFrt.setCellValue(totalBlFrt);
			
			XSSFCell cellTotalBlTot = row.createCell(13);
			cellTotalBlTot.setCellStyle(styleSubHeaderNumber);
			cellTotalBlTot.setCellValue(totalBlTot);
			
			XSSFCell cellTotalOS0_30 = row.createCell(14);
			cellTotalOS0_30.setCellStyle(styleSubHeaderNumber);
			cellTotalOS0_30.setCellValue(totalOS0_30);
			
			XSSFCell cellTotalOS31_60 = row.createCell(15);
			cellTotalOS31_60.setCellStyle(styleSubHeaderNumber);
			cellTotalOS31_60.setCellValue(totalOS31_60);
			
			XSSFCell cellTotalOS61_90 = row.createCell(16);
			cellTotalOS61_90.setCellStyle(styleSubHeaderNumber);
			cellTotalOS61_90.setCellValue(totalOS61_90);
			
			XSSFCell cellTotalOS91_120 = row.createCell(17);
			cellTotalOS91_120.setCellStyle(styleSubHeaderNumber);
			cellTotalOS91_120.setCellValue(totalOS91_120);
			
			XSSFCell cellTotalOS121_more = row.createCell(18);
			cellTotalOS121_more.setCellStyle(styleSubHeaderNumber);
			cellTotalOS121_more.setCellValue(totalOS121_more);
			
			//blank row
			rowId++;
			
			//TODO
			//On Account detail
			
			//add header
			row = spreadsheet.createRow(rowId++);
  			
			XSSFCell cellMrNo = row.createCell(0);
			cellMrNo.setCellStyle(styleSubHeader);
			cellMrNo.setCellValue("MrNo");
			
			XSSFCell cellMrPayBy = row.createCell(1);
			cellMrPayBy.setCellStyle(styleSubHeader);
			cellMrPayBy.setCellValue("PTyp");
			
			XSSFCell cellMrDt = row.createCell(2);
			cellMrDt.setCellStyle(styleSubHeader);
			cellMrDt.setCellValue("MrDt");
			
			for (int i = 3; i <= 6; i++) {
				XSSFCell cellMrCustBankName = row.createCell(i);
				cellMrCustBankName.setCellStyle(styleSubHeader);
		        if (i == 3) {
		        	cellMrCustBankName.setCellValue("Bank Name");
		        }
		    }
			spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,3,6));
			
			for (int i = 7; i <= 12; i++) {
				XSSFCell cellMrRtgsRefNo = row.createCell(i);
				cellMrRtgsRefNo.setCellStyle(styleSubHeader);
		        if (i == 7) {
		        	cellMrRtgsRefNo.setCellValue("RefNo");
		        } 
		    }
			spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,7,12));
			
			XSSFCell cellMrNetAmt = row.createCell(13);
			cellMrNetAmt.setCellStyle(styleSubHeader);
			cellMrNetAmt.setCellValue("MrAmt");
			
			XSSFCell cellCumAmt = row.createCell(14);
			cellCumAmt.setCellStyle(styleSubHeader);
			cellCumAmt.setCellValue("MrCumAmt");
			
			XSSFCell cellEmpty = row.createCell(15);
			cellEmpty.setCellStyle(styleSubHeader);
			
			cellEmpty = row.createCell(16);
			cellEmpty.setCellStyle(styleSubHeader);
			
			cellEmpty = row.createCell(17);
			cellEmpty.setCellStyle(styleSubHeader);
			
			cellEmpty = row.createCell(18);
			cellEmpty.setCellStyle(styleSubHeader);
			
			//add detail
			double mrRemAmtTot = 0.0;
			for (MoneyReceipt mr : mrList) {
				if (customer.getCustId() == mr.getMrCustId()) {
					row = spreadsheet.createRow(rowId++);
					
					if (mr.getMrNo() != null && !mr.getMrNo().equalsIgnoreCase("")) {
						XSSFCell cellMrNoVal = row.createCell(0);
						cellMrNoVal.setCellStyle(style);
						cellMrNoVal.setCellValue(mr.getMrNo());
					} else {
						XSSFCell cellMrNoVal = row.createCell(0);
						cellMrNoVal.setCellStyle(style);
					}
					
					XSSFCell cellMrPayByVal = row.createCell(1);
					cellMrPayByVal.setCellStyle(style);
					cellMrPayByVal.setCellValue(String.valueOf(mr.getMrPayBy()));
					
					if (mr.getMrDate() != null) {
						XSSFCell cellMrDtVal = row.createCell(2);
						cellMrDtVal.setCellStyle(style);
						cellMrDtVal.setCellValue(CodePatternService.getFormatedDateString(String.valueOf(mr.getMrDate())));
					} else {
						XSSFCell cellMrDtVal = row.createCell(2);
						cellMrDtVal.setCellStyle(style);
					}
					
					
					for (int i = 3; i <= 6; i++) {
						XSSFCell cellMrCustBankNameVal = row.createCell(i);
						cellMrCustBankNameVal.setCellStyle(style);
				        if (i == 3) {
				        	if (mr.getMrCustBankName() != null && !mr.getMrCustBankName().equalsIgnoreCase("")) {
				        		cellMrCustBankNameVal.setCellValue(mr.getMrCustBankName());
							}
				        }
				    }
					spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,3,6));
					
					for (int i = 7; i <= 12; i++) {
						XSSFCell cellMrRtgsRefNoVal = row.createCell(i);
						cellMrRtgsRefNoVal.setCellStyle(style);
				        if (i == 7) {
				        	if (mr.getMrRtgsRefNo() != null && !mr.getMrRtgsRefNo().equalsIgnoreCase("")) {
				        		cellMrRtgsRefNoVal.setCellValue(mr.getMrRtgsRefNo());
							}
				        	
				        } 
				    }
					spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,7,12));
					
					XSSFCell cellMrNetAmtVal = row.createCell(13);
					cellMrNetAmtVal.setCellStyle(style);
					cellMrNetAmtVal.setCellValue(mr.getMrNetAmtForRpt());
					
					mrRemAmtTot += mr.getMrNetAmtForRpt();
					
					XSSFCell cellMrNetAmtTot = row.createCell(14);
					cellMrNetAmtTot.setCellStyle(style);
					cellMrNetAmtTot.setCellValue(mrRemAmtTot);
					
					XSSFCell cellEmptyVal = row.createCell(15);
					cellEmptyVal.setCellStyle(style);
					
					cellEmptyVal = row.createCell(16);
					cellEmptyVal.setCellStyle(style);
					
					cellEmptyVal = row.createCell(17);
					cellEmptyVal.setCellStyle(style);
					
					cellEmptyVal = row.createCell(18);
					cellEmptyVal.setCellStyle(style);
				}
			}
			if(mrRemAmtTot<1){
				
				row = spreadsheet.createRow(rowId++);
				
				spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,7,11));
				
				XSSFCell cellMrNetAmtVal = row.createCell(12);
				cellMrNetAmtVal.setCellStyle(style);
				cellMrNetAmtVal.setCellValue("0.0");
				
				XSSFCell cellMrNetAmtTot = row.createCell(13);
				cellMrNetAmtTot.setCellStyle(style);
				cellMrNetAmtTot.setCellValue("0.0");
				
				XSSFCell cellEmptyVal = row.createCell(14);
				cellEmptyVal.setCellStyle(style);
				
				cellEmptyVal = row.createCell(15);
				cellEmptyVal.setCellStyle(style);
				
				cellEmptyVal = row.createCell(16);
				cellEmptyVal.setCellStyle(style);
				
				cellEmptyVal = row.createCell(17);
				cellEmptyVal.setCellStyle(style);
				
			}
			
			row = spreadsheet.createRow(rowId++);
			
			//add footer
			for (int i = 0; i <= 12; i++) {
				XSSFCell cellFinalTot = row.createCell(i);
				cellFinalTot.setCellStyle(styleSubHeaderNumber);
		        if (i == 0) {
		        	cellFinalTot.setCellValue("O/S TOTAL (BlTotRem - MrAmt)    ");
		        } 
		    }
			spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,0,12));
			
			XSSFCell cellFinalTotVal = row.createCell(13);
			cellFinalTotVal.setCellStyle(styleSubHeaderNumber);
			 
			cellFinalTotVal.setCellValue((totalOS0_30+totalOS31_60+totalOS61_90+totalOS91_120+totalOS121_more) - mrRemAmtTot);
			  
			XSSFCell cellFooterEmpty = row.createCell(14);
			cellFooterEmpty.setCellStyle(styleSubHeaderNumber);
			
			cellFooterEmpty = row.createCell(15);
			cellFooterEmpty.setCellStyle(styleSubHeaderNumber);
			
			cellFooterEmpty = row.createCell(16);
			cellFooterEmpty.setCellStyle(styleSubHeaderNumber);
			
			cellFooterEmpty = row.createCell(17);
			cellFooterEmpty.setCellStyle(styleSubHeaderNumber);
			
			cellFooterEmpty = row.createCell(18);
			cellFooterEmpty.setCellStyle(styleSubHeaderNumber);
			
			double totalOs=((totalOS0_30+totalOS31_60+totalOS61_90+totalOS91_120+totalOS121_more) - mrRemAmtTot);
			mapOs.put("totalOs",totalOs);
			//blank row
			rowId++;
			
			row = spreadsheet.createRow(rowId++);
			
			//add footer
			for (int i = 0; i <= 18; i++) {
				XSSFCell cellFinalTot = row.createCell(i);
				cellFinalTot.setCellStyle(style);
		        if (i == 0) {
		        	cellFinalTot.setCellValue("###########################################################################");
		        } 
		    }
			spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,0,17));
			
			//blank row
			rowId++;
			row = spreadsheet.createRow(rowId++);
			listOSTotal.add(mapOs);

		}
  		
  		row = spreadsheet.createRow(rowId++);
  		//spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,4,6));
  		  
  		for (int i = 4; i <=10; i++) {
			XSSFCell cellFinalTot = row.createCell(i);
			cellFinalTot.setCellStyle(styleSubHeader);
			if(i==4){
				spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,4,5));
			        cellFinalTot.setCellValue("CustomerFACode");
			}
			if(i==6){
				spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,6,8));
		        cellFinalTot.setCellValue("CustomerName");
			}if(i==9){
				spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,9,10));
		        cellFinalTot.setCellValue("totalOs");
				
			}
	         
	    }
		
  		double sumTotal=0;
  		Object obj[]=new Object[3];
		   for(int i=0;i<listOSTotal.size();i++){
			   row = spreadsheet.createRow(rowId++);
			   	int m=0;
  			     Map<String,Object> map=listOSTotal.get(i);
  			     for(Map.Entry<String,Object> me:map.entrySet()){
  				  obj[m++]=me.getValue();
  			     }
  				 for(int j=4;j<=10;j++){
  				XSSFCell cellFinalTot = row.createCell(j);
  				cellFinalTot.setCellStyle(styleSubHeaderNumber);
  				 if(j==9){
  					spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,9,10));
  					System.out.println("String.valueOf(obj[0])"+String.valueOf(obj[0]));
  					System.out.println("String.valueOf(obj[1])"+String.valueOf(obj[1]));
  					System.out.println("String.valueOf(obj[2])"+String.valueOf(obj[2]));
  					  sumTotal+=Double.parseDouble(String.valueOf(obj[0]));//2
  					  cellFinalTot.setCellValue(Double.parseDouble(String.valueOf(obj[0])));//2
  				 }
  				 else if(j==4){
  					spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,4,5));
  					cellFinalTot.setCellValue(String.valueOf(obj[1])); //0
  				 }else if(j==6){
  					spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,6,8));
  					cellFinalTot.setCellValue(String.valueOf(obj[2])); //1
  				 }
  				
			 }
	         
	    } 
		   row = spreadsheet.createRow(rowId++);
			for(int j=7;j<=10;j++){
  				XSSFCell cellSumFinalTot=row.createCell(j);
  				   cellSumFinalTot.setCellStyle(styleSubHeaderNumber);
  				 if(j==7){
  					spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,7,8));
  					cellSumFinalTot.setCellValue("total="); 
  				 }
  				 if(j==9){
  					spreadsheet.addMergedRegion(new CellRangeAddress(rowId-1,rowId-1,9,10));
					  cellSumFinalTot.setCellValue(sumTotal);
  				 }
  			}
	System.out.println("File path = "+fileName);
			File osReportFile = new File(fileName);
			if (osReportFile.exists())
				osReportFile.delete();
			else
				osReportFile.createNewFile();
			FileOutputStream out = new FileOutputStream(osReportFile);
			workbook.write(out);
			out.flush();
			out.close();
			System.out.println("osRtp.xlsx written successfully");
	}
	
	@RequestMapping(value="/getOsRptXLSX", method=RequestMethod.POST)
	public void getOsRptXLSX(HttpServletRequest request, HttpServletResponse response) throws Exception{
		User user = (User) httpSession.getAttribute("currentUser");
		String fileName = servletContext.getRealPath("/");
		fileName = fileName + osFilePath +user.getUserCode()+"OSReport.xlsx";
		
		File osReport = new File(fileName);
		if (osReport.exists()) {
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=osRpt.xlsx");
			response.setContentLength((int)osReport.length());
			InputStream inputStream = new BufferedInputStream(new FileInputStream(osReport));
			FileCopyUtils.copy(inputStream, response.getOutputStream());
		}
		//osReport.
	}
	
}
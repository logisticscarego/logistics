package com.mylogistics.controller.report;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.CustomerGroupDAO;
import com.mylogistics.DAO.report.RelAnnextureRptDAO;
import com.mylogistics.model.CustomerGroup;
import com.mylogistics.services.ConstantsValues;

@Controller
public class RelAnnextureRptCntlr {

	@Autowired
	private CustomerGroupDAO customerGroupDAO;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private RelAnnextureRptDAO relAnnextureRptDAO;
	
	@RequestMapping(value="/getcustGroup",method=RequestMethod.POST)
	public @ResponseBody Object getCustGroup(){
		System.out.println("enter in to get Customer group...");
	 Map<String,Object> map=new HashMap<String,Object>();
	      List<CustomerGroup> custGroupList=customerGroupDAO.getAllCustGroupList(sessionFactory);
	 if(!custGroupList.isEmpty()){
		 map.put("custGroupList",custGroupList);
		 map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
	 }else{
		 map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
	 }
		return map;
	}
	
	@RequestMapping(value="/relAnnexPrintSubmit", method=RequestMethod.POST)
	public @ResponseBody Object getAnnextureRpt(@RequestBody Map<String,Object> clientMap){
		System.out.println(clientMap);
		Date fromDt = Date.valueOf((String) clientMap.get("fromDate"));
		Date toDt = Date.valueOf((String) clientMap.get("toDate"));
		String custGroupId=(String)clientMap.get("custGroupId");
		Map<String,Object> map=new HashMap<String,Object>();
		List<Map<String,Object>> blList = new ArrayList<Map<String,Object>>();
		 blList=relAnnextureRptDAO.submitRelAnnextureRpt(fromDt,toDt,custGroupId);
		 if(!blList.isEmpty()){
				System.out.println("size of blList = "+blList.size());
				map.put("list",blList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put("msg","Branch Does not have any bill");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		 return map;
	}
}

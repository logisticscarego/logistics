package com.mylogistics.controller.report;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.report.ReportDAO;
import com.mylogistics.services.ConstantsValues;

@Controller
public class CNMisReportCntlr {

	@Autowired
	private ReportDAO reportDAO;
	
	@RequestMapping(value = "/getMisCnmt", method = RequestMethod.POST)
	public @ResponseBody Object getMisCnmt(@RequestBody Map<String, String> misCnmtMap) {
		System.out.println("enter into getMisCnmt funciton");
		
		System.out.println("from cnmt no: "+misCnmtMap.get("fromCnmtNo"));
		System.out.println("to cnmt no: "+misCnmtMap.get("toCnmtNo"));
		System.out.println("branchId: "+misCnmtMap.get("branchId"));
		
		List<String> misCnmtList = reportDAO.getMisCnmt(misCnmtMap.get("branchId"), misCnmtMap.get("fromCnmtNo"), misCnmtMap.get("toCnmtNo"), ConstantsValues.STATUS_CNMT);
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		if(!misCnmtList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("misCnmtList",misCnmtList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getMisChln", method = RequestMethod.POST)
	public @ResponseBody Object getMisChln(@RequestBody Map<String, String> misChlnMap) {
		System.out.println("enter into getMisChln funciton");

		System.out.println("branchId: "+misChlnMap.get("branchId"));
		
		List<String> misChlnList = reportDAO.getMisChln(misChlnMap.get("branchId"), ConstantsValues.STATUS_CHLN);
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		if(!misChlnList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("misChlnList",misChlnList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	@RequestMapping(value = "/getMisSedr", method = RequestMethod.POST)
	public @ResponseBody Object getMisSedr(@RequestBody Map<String, String> missedrMap) {
		System.out.println("enter into getMisSedr funciton");

		System.out.println("branchId: "+missedrMap.get("branchId"));
		
		List<String> missedrList = reportDAO.getMisSedr(missedrMap.get("branchId"), ConstantsValues.STATUS_SEDR);
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		if(!missedrList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("missedrList",missedrList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}

	
	
	
}

package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchRightsDAO;
import com.mylogistics.DAO.CustomerRightsDAO;
import com.mylogistics.DAO.StationRightsDAO;
import com.mylogistics.model.BranchRights;
import com.mylogistics.model.CustomerRights;
import com.mylogistics.model.StationRights;
import com.mylogistics.services.ConstantsValues;

@Controller
public class DisallowAdvanceCntlr {
	
	@Autowired
	private CustomerRightsDAO customerRightsDAO;
	
	@Autowired
	private BranchRightsDAO branchRightsDAO;
	
	@Autowired
	private StationRightsDAO stationRightsDAO;


	@RequestMapping(value="/getCustRightsData",method = RequestMethod.POST)
	public @ResponseBody Object getCustRightsData(){
		System.out.println("Entered into getCustRightsData function in controller--");
		
		List<CustomerRights> list = customerRightsDAO.getCustRightsIsAllow();
		
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(list.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",list);	
  		}else {
  			map.put("list",list);
  			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	@RequestMapping(value="/getBranchRightsData",method = RequestMethod.POST)
	public @ResponseBody Object getBranchRightsData(){
		System.out.println("Entered into getBranchRightsData function in controller--");
		
		List<BranchRights> list = branchRightsDAO.getBranchRightsIsAllow();
		
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(list.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",list);	
  		}else {
  			map.put("list",list);
  			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	@RequestMapping(value="/getStationRightsData",method = RequestMethod.POST)
	public @ResponseBody Object getStationRightsData(){
		System.out.println("Entered into getStationRightsData function in controller--");
		
		List<StationRights> list = stationRightsDAO.getStationRightsIsAllow();
		
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(list.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",list);	
  		}else {
  			map.put("list",list);
  			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	@RequestMapping(value = "/submitCustIsAllowNo", method = RequestMethod.POST)
    public @ResponseBody Object submitCustIsAllowNo(@RequestBody String selection) {
		System.out.println("Entered into submitCustIsAllowNo function");
        String contids[] = selection.split(",");
        
        int[] contIdsInt = new int[contids.length];
        for (int i = 0; i < contids.length; i++) {
        	contIdsInt[i] = Integer.parseInt(contids[i]);
        }
        
        int temp=customerRightsDAO.updateCustRightsIsAllowNo(contIdsInt);
        Map<String, Object> map = new HashMap<String, Object>();
        if(temp>0){
            map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
        }
        else{
            map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
        }
        return map;
    }
	
	@RequestMapping(value = "/submitBranchIsAllowNo", method = RequestMethod.POST)
    public @ResponseBody Object submitBranchIsAllowNo(@RequestBody String selection) {
		System.out.println("Entered into submitBranchIsAllowNo function");
        String contids[] = selection.split(",");
        
        int[] contIdsInt = new int[contids.length];
        for (int i = 0; i < contids.length; i++) {
        	contIdsInt[i] = Integer.parseInt(contids[i]);
        }
        
        int temp=branchRightsDAO.updateBrRightsIsAllowNo(contIdsInt);
        Map<String, Object> map = new HashMap<String, Object>();
        if(temp>0){
            map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
        }
        else{
            map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
        }
        return map;
    }
	
	@RequestMapping(value = "/submitStationIsAllowNo", method = RequestMethod.POST)
    public @ResponseBody Object submitStationIsAllowNo(@RequestBody String selection) {
		System.out.println("Entered into submitStationIsAllowNo function");
        String contids[] = selection.split(",");
        
        int[] contIdsInt = new int[contids.length];
        for (int i = 0; i < contids.length; i++) {
        	contIdsInt[i] = Integer.parseInt(contids[i]);
        }
        
        int temp=stationRightsDAO.updateStnRightsIsAllowNo(contIdsInt);
        Map<String, Object> map = new HashMap<String, Object>();
        if(temp>0){
            map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
        }
        else{
            map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
        }
        return map;
    }
}

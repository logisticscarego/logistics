package com.mylogistics.controller;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itextpdf.text.FontFactory;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.RentMstrDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.RentDetail;
import com.mylogistics.model.RentMstr;
import com.mylogistics.model.User;
import com.mylogistics.model.lhpv.LhpvBal;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.RentService;

@Controller
public class RentCntlr {

	@Autowired
	private RentMstrDAO rentMstrDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;

	@RequestMapping(value = "/saveRent", method = RequestMethod.POST)
	public @ResponseBody Object saveRent(@RequestBody RentService rentService) {
		System.out.println("Enter into dissBankFromBranch---->");
		Map<String, Object> map = new HashMap<>();

		System.out.println("Adv rent: "+rentService.getRentMstr().getRentMAdv());
		System.out.println("BranchId: "+rentService.getBranchId());
		System.out.println("EmpId: "+rentService.getEmpId());
		System.out.println("Add pin: "+rentService.getAddress().getAddPin());
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		int temp = rentMstrDAO.saveRentMstr(rentService, currentUser);
		
		if (temp > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value="/getRentByBranchId", method=RequestMethod.POST)
	public @ResponseBody Map<String, Object> getRentByBranchId(@RequestBody String branchId){
		Map<String, Object> responseMap=new HashMap<>();
		List<RentMstr> rentMstrList=rentMstrDAO.getAllRentMstrByBranchId(Integer.parseInt(branchId));
		if(!rentMstrList.isEmpty()) {
			responseMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			responseMap.put("rentList", rentMstrList);
		}else {
			responseMap.put(ConstantsValues.RESULT, "Data not found for "+branchId);
		}
		return responseMap;
	}
	
	
	@RequestMapping(value="/getRentMstrList", method=RequestMethod.POST)
	public @ResponseBody Map<String, Object> getRentMstrList(){
		Map<String, Object> responseMap=new HashMap<>();
		List<Map<String, Object>> rentMstrList=rentMstrDAO.getRentMstrForN();
		if(!rentMstrList.isEmpty()) {
			responseMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			responseMap.put("rentList", rentMstrList);
		}else {
			responseMap.put(ConstantsValues.RESULT, "No record found");
		}
		return responseMap;
	}
	
	
	@RequestMapping(value="/updateRentMstr", method=RequestMethod.POST)
	public @ResponseBody Map<String, String> updateRentMstr(@RequestBody RentMstr mstr){
		Map<String, String> responseMap=new HashMap<>();
		
		System.out.println(mstr.getRentMLandLordName());
		System.out.println(mstr.getAddress().getAddCity());
		System.out.println(mstr.getBranch().getBranchAdd());
		System.out.println(mstr.getEmployee());
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		mstr.getAddress().setUserCode(currentUser.getUserCode());
		mstr.setUserCode(currentUser.getUserCode());
		boolean b=rentMstrDAO.updateRentMstr(mstr);
		System.out.println(b);
		if(b) {
			responseMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}
		else {
			responseMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		
		return responseMap;
	}
	
	@RequestMapping(value="/submitRentDetails", method=RequestMethod.POST)
	public @ResponseBody Map<String, String> submitRentDetails(@RequestBody RentDetail rentDetail){
		Map<String, String> responseMap=new HashMap<>();
		if(rentDetail.getTdsAmt()==null) {
			rentDetail.setTdsAmt(0.0);
		}
		rentDetail.setNetAmt(rentDetail.getRentAmt()-rentDetail.getTdsAmt());
	    Calendar calendar=Calendar.getInstance();
	    Date date=new Date(calendar.getTimeInMillis());
		rentDetail.setRdDate(date);
		rentDetail.setUploaded(false);
		if(rentDetail.getRentDesc()==null){
			rentDetail.setRentDesc("Rent Entry");
		}else if(rentDetail.getRentDesc().trim().equalsIgnoreCase("")){
			rentDetail.setRentDesc("Rent Entry");
		}
		
		int i=rentMstrDAO.saveRentDetail(rentDetail);
		if(i>0) {
			responseMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else {
			responseMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return responseMap;
	}
	
	
	
	@RequestMapping(value="/downloadRentXLSX",method=RequestMethod.POST)
	public void downloadRentXLSX(HttpServletResponse response)
	{
		
		List<Branch> branchList=branchDAO.getAllActiveBranches();
		Map<String, String> branchMap=new HashMap<>();
		List<Map<Object, Object>> rentMap=new ArrayList<>();
		for (Branch branch : branchList) {
			branchMap.put(branch.getBranchCode(), branch.getBranchName());
			
		}
		List<RentDetail> rentDetailList=rentMstrDAO.getRentDetailFrExl();
		List<RentMstr> rentMstrList=rentMstrDAO.getRentMstr();
		if(!rentDetailList.isEmpty() && !rentMstrList.isEmpty())
		{
		
		for(int j=0;j<rentDetailList.size();j++){
			Map<Object,Object> map=new HashMap<>();
			map.put("rentDetail", rentDetailList.get(j));
			for(int i=0;i<rentMstrList.size();i++){
				if(rentDetailList.get(j).getRentMstrId()==rentMstrList.get(i).getRentMId())
					map.put("rentMstr", rentMstrList.get(i));
			}
			
			rentMap.add(map);
		}
			
		XSSFWorkbook workbook=new XSSFWorkbook();
		
	    //Header font
  		XSSFFont fontHeader = workbook.createFont();
  		fontHeader.setFontHeightInPoints((short)11);
  		fontHeader.setColor(IndexedColors.WHITE.getIndex());
  		fontHeader.setFontName(FontFactory.COURIER_BOLD);
  		fontHeader.setBold(true);
		
  		//Header style
  		XSSFCellStyle styleHeader = workbook.createCellStyle();
  		styleHeader.setFont(fontHeader);
  		styleHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
  		styleHeader.setFillForegroundColor(IndexedColors.BLUE_GREY.getIndex());
  		styleHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
  		styleHeader.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
  		styleHeader.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
  		styleHeader.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
  		styleHeader.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
		
  		
  		//
		XSSFCellStyle styleData = workbook.createCellStyle();
		styleData.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		
	    XSSFSheet sheet=workbook.createSheet("Lhpv Bal");
	    sheet.setColumnWidth(0, 20*256);
	    sheet.setColumnWidth(1, 25*256);
	    sheet.setColumnWidth(2, 35*256);
	    sheet.setColumnWidth(3, 30*256);
	    sheet.setColumnWidth(4, 30*256);
	    sheet.setColumnWidth(5, 30*256);
	    sheet.setColumnWidth(6, 30*256);
	    sheet.setColumnWidth(7, 30*256);
	    sheet.setColumnWidth(8, 30*256);
	    sheet.setColumnWidth(9, 30*256);
	    sheet.setColumnWidth(10, 30*256);
	    sheet.setColumnWidth(11, 30*256);
	    sheet.setColumnWidth(12, 40*256);
	    sheet.setColumnWidth(13, 40*256);
	    sheet.setColumnWidth(14, 30*256);
	    sheet.setColumnWidth(15, 30*256);
	    sheet.setColumnWidth(16, 30*256);
	    sheet.setColumnWidth(17, 30*256);
	    sheet.setColumnWidth(18, 30*256);
	    sheet.setColumnWidth(19, 30*256);
	    sheet.setColumnWidth(20, 30*256);
	    sheet.setColumnWidth(21, 30*256);
	    sheet.setColumnWidth(22, 30*256);
	    sheet.setColumnWidth(23, 30*256);
	    sheet.setColumnWidth(24, 30*256);
	    sheet.setColumnWidth(25, 30*256);
	    sheet.setColumnWidth(26, 30*256);
	    sheet.setColumnWidth(27, 30*256);
	    XSSFRow row=sheet.createRow(0);
	    
	    XSSFCell cell=null;
	    cell=row.createCell(0);
	    cell.setCellValue("Transaction Type");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(1);
	    cell.setCellValue("Beneficiary Code");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(2);
	    cell.setCellValue("Beneficiary Account Number");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(3);
	    cell.setCellValue("Instrument Amount");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(4);
	    cell.setCellValue("Beneficiary Name");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(5);
	    cell.setCellValue("Drawee Location");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(6);
	    cell.setCellValue("Print Location");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(7);
	    cell.setCellValue("Bene Address 1");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(8);
	    cell.setCellValue("Bene Address 2");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(9);
	    cell.setCellValue("Bene Address 3");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(10);
	    cell.setCellValue("Bene Address 4");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(11);
	    cell.setCellValue("Bene Address 5");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(12);
	    cell.setCellValue("Instruction Reference Number");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(13);
	    cell.setCellValue("Customer Reference Number");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(14);
	    cell.setCellValue("Payment details 1");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(15);
	    cell.setCellValue("Payment details 2");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(16);
	    cell.setCellValue("Payment details 3");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(17);
	    cell.setCellValue("Payment details 4");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(18);
	    cell.setCellValue("Payment details 5");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(19);
	    cell.setCellValue("Payment details 6");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(20);
	    cell.setCellValue("Payment details 7");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(21);
	    cell.setCellValue("Cheque Number");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(22);
	    cell.setCellValue("Chq/Tm Date");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(23);
	    cell.setCellValue("MICR Number");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(24);
	    cell.setCellValue("IFSC Code");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(25);
	    cell.setCellValue("Bene Bank Name");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(26);
	    cell.setCellValue("Bene Bank Branch Name ");
	    cell.setCellStyle(styleHeader);
	    
	    cell=row.createCell(27);
	    cell.setCellValue("Beneficiary email id");
	    cell.setCellStyle(styleHeader);
	    
	    int rowToCreate=1;
	    XSSFRow dataRow=null;
	    XSSFCell dataCell=null;
	    
	    SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
	    for (int i=0;i<rentMap.size();i++) {
	    		
			//String ifscCode = rentMap.get(i).get("");
	    	RentDetail rd=(RentDetail) rentMap.get(i).get("rentDetail");
	    	RentMstr rm=(RentMstr) rentMap.get(i).get("rentMstr");
	    	
	    	String ifscCode = rm.getRentMIFSC();

	    	dataRow=sheet.createRow(rowToCreate);
	    	dataCell=dataRow.createCell(0);
	    	

	    	if (ifscCode.substring(0, 4).equalsIgnoreCase("HDFC")) {
				dataCell.setCellValue("I");
			} else {
				if (rd.getNetAmt() > 200000) {
					dataCell.setCellValue("R");
				} else {
					dataCell.setCellValue("N");
				}
			}
	    	
	    	
	    	dataCell=dataRow.createCell(1);
	    	dataCell.setCellValue(branchMap.get(rm.getBranch().getBranchCode()));
	    	
	    	dataCell=dataRow.createCell(2);
	    	dataCell.setCellValue(rm.getRentAcNo());
	    	
	    	dataCell=dataRow.createCell(3);
	    	dataCell.setCellValue(rd.getNetAmt());
	    	
	    	dataCell=dataRow.createCell(4);
	    	dataCell.setCellValue(rm.getRentMLandLordName());
	    	
	    	dataCell=dataRow.createCell(5);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(6);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(7);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(8);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(9);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(10);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(11);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(12);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(13);
	    	if(rm.getRentMLandLordName().length()>12){
	    		dataCell.setCellValue(rm.getRentMLandLordName().substring(0, 12));
	    	}else{
	    		dataCell.setCellValue(rm.getRentMLandLordName());
	    	}
	    	
	    	dataCell=dataRow.createCell(14);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(15);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(16);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(17);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(18);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(19);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(20);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(21);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(22);
	    	dataCell.setCellValue(dateFormat.format(rd.getRdDate()));
	    	
	    	dataCell=dataRow.createCell(23);
	    	dataCell.setCellValue("");
	    	
	    	dataCell=dataRow.createCell(24);
	    	dataCell.setCellValue(ifscCode);
	    	
	    	dataCell=dataRow.createCell(25);
	    	dataCell.setCellValue(rm.getRentMBankName());
	    	
	    	dataCell=dataRow.createCell(26);
	    	dataCell.setCellValue(branchMap.get(rm.getBranch().getBranchCode()));
	    	
	    	dataCell=dataRow.createCell(27);
	    	dataCell.setCellValue("");
	    	
	    	rowToCreate++;
	    	
	    	}
	    
	 	response.setContentType("application/vnd.ms-excel");
    		response.setHeader("Content-Disposition", "attachment; filename=RentPay.xlsx");
    		ServletOutputStream out;
			try {
				out = response.getOutputStream();
				workbook.write(out);
	    		out.flush();
	    		out.close();
	    		
	    	int val=rentMstrDAO.updateRentDetail(rentDetailList);
	    	System.out.println("Value of Val is  "+val);
	    		
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				//this.gLhpvBalList.clear();
			}
			
		}
	
	}	

	
	
	
	
}

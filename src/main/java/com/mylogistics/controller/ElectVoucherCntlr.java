package com.mylogistics.controller;

//import java.util.Date;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.ElectricityMstrDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.DAO.bank.ChequeLeavesDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.model.ElectricityMstr;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.SubElectricityMstr;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.EM_SEMService;
import com.mylogistics.services.ElectVoucherService;
import com.mylogistics.services.ElectVoucherServiceImpl;
import com.mylogistics.services.VoucherService;

@Controller
public class ElectVoucherCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Autowired
	private ElectricityMstrDAO electricityMstrDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@Autowired
	private ChequeLeavesDAO chequeLeavesDAO;
	
	private ElectVoucherService electVoucherService = new ElectVoucherServiceImpl();
	
	@RequestMapping(value = "/getVDetFrEV" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrTelV() {  
		System.out.println("Enter into getVDetFrTelV---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}
	
	
	@RequestMapping(value = "/getChequeNoFrEV" , method = RequestMethod.POST)  
	public @ResponseBody Object getChequeNoFrEV(@RequestBody Map<String,String> clientMap) {  
		System.out.println("Enter into getChequeNoFrEV---->");
		Map<String, Object> map = new HashMap<>();	
		String bankCode = clientMap.get("bankCode");
		String cType = clientMap.get("CType");
		char CType = cType.charAt(0);
		System.out.println("value of CType = "+CType);
		//ChequeLeaves chequeLeaves= bankMstrDAO.getChqListByBnkC(bankCode,CType);
		ChequeLeaves chequeLeaves = bankMstrDAO.getAccPayChqByBnkC(bankCode,CType);
		List<ChequeLeaves> chqList = bankMstrDAO.getAPChqLByBnkC(bankCode,CType);
		if(chequeLeaves != null){
			System.out.println("***************** 1");
			map.put(ConstantsValues.CHEQUE_LEAVES,chequeLeaves);
			map.put("list",chqList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			System.out.println("***************** 2");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	@RequestMapping(value = "/getElectList" , method = RequestMethod.POST)  
	public @ResponseBody Object getElectList() {  
		System.out.println("enter into getCrnNoList function");
		Map<String,Object> map = new HashMap<String,Object>();
		List<ElectricityMstr> emList = electricityMstrDAO.getAllElectMstr();
		if(!emList.isEmpty()){
			map.put("emList",emList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/submitEMVoucher" , method = RequestMethod.POST)  
	public @ResponseBody Object submitEMVoucher(@RequestBody EM_SEMService em_sem) {  
		System.out.println("enter into submitEMVoucher function");
		Map<String,Object> map = new HashMap<String,Object>();
		if(em_sem != null){
			electVoucherService.addEMAndSEM(em_sem);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}			
		return map;
	}
	
	
	@RequestMapping(value = "/fetchEmAndSem" , method = RequestMethod.POST)  
	public @ResponseBody Object fetchEmAndSem() {  
		System.out.println("enter into fetchEmAndSem function");
		Map<String,Object> map = new HashMap<String,Object>();
		List<EM_SEMService>  emsemList = electVoucherService.getAllEMAndSEM();
		System.out.println("size of tmstmList ="+emsemList.size());
		map.put("list",emsemList);
		return map;
	}
	
	
	@RequestMapping(value = "/removeSemV" , method = RequestMethod.POST)  
	public @ResponseBody Object removeSemV(@RequestBody Map<String,Object> clientMap) {  
		System.out.println("enter into removeSemV function ");
		int index = (int) clientMap.get("index");
		Map<String,Object> map = new HashMap<String,Object>();
		electVoucherService.remove(index);
		return map;
	}
	
	
	@RequestMapping(value = "/submitElectVoucher" , method = RequestMethod.POST)
	public @ResponseBody Object submitElectVoucher(@RequestBody VoucherService voucherService) {  
		System.out.println("enter into submitElectVoucher function");
		Map<String,Object> map = new HashMap<String,Object>();
		if(voucherService.getVoucherType().equalsIgnoreCase("Electricity")){
			 map = saveElectVoucher(voucherService);
			 int vhNo = (int) map.get("vhNo");
			if(vhNo>0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}
		return map;
	}
	
	
	public Map<String,Object> saveElectVoucher(VoucherService voucherService){
		System.out.println("Enter into saveElectVoucher----->>>");
		User currentUser =	(User)httpSession.getAttribute("currentUser");
		Map<String,Object> map = new HashMap<String,Object>();
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		List<FAMaster> famList = faMasterDAO.getFAMByFaName(ConstantsValues.ELECT_FANAME);
		String eleFaCode = "";
		if(!famList.isEmpty()){
			eleFaCode = famList.get(0).getFaMfaCode();
		}
		
		char payBy = voucherService.getPayBy();
		System.out.println("value of payBy = "+payBy);
		String payMode = "";
		if(payBy == 'C'){
			payMode = "C";
			CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
			List<EM_SEMService> emsemList = electVoucherService.getAllEMAndSEM();
			int voucherNo = 0;
			if(!emsemList.isEmpty()){
				
				for(int i=0;i<emsemList.size();i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					
					EM_SEMService em_sem = emsemList.get(i);
					ElectricityMstr eleMstr = em_sem.getElctMstr();
					
					SubElectricityMstr sEleMstr = em_sem.getSubElectMstr();
					
					//String tvNo = 
					
					String eleBrFaCode = eleMstr.getBranch().getBranchFaCode(); 
					
					if(brFaCode.equalsIgnoreCase(eleBrFaCode)){
						
						CashStmt cashStmt = new CashStmt();
						cashStmt.setbCode(currentUser.getUserBranchCode());
						cashStmt.setUserCode(currentUser.getUserCode());
						cashStmt.setCsDescription(voucherService.getDesc());
						cashStmt.setCsDrCr('D');
						cashStmt.setCsAmt(sEleMstr.getSemPayAmt());
						cashStmt.setCsType(voucherService.getVoucherType());
						cashStmt.setCsVouchType("cash");
						cashStmt.setCsFaCode(eleFaCode);
						cashStmt.setCsTvNo(eleMstr.getEmCrnNo());						
						cashStmt.setCsVouchNo(voucherNo);						
						java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
						cashStmt.setCsDt(sqlDate);
						cashStmt.setPayMode(payMode);
						
						newMap = cashStmtStatusDAO.updateCSSForEleV(cashStmtStatus.getCssId(), cashStmt);
						voucherNo = (int) newMap.get("vhNo");
						
						System.out.println("cashStmt id = "+cashStmt.getCsId());
						sEleMstr.setSemCsId(cashStmt.getCsId());
						sEleMstr.setbCode(currentUser.getUserBranchCode());
						sEleMstr.setUserCode(currentUser.getUserCode());
						electricityMstrDAO.updateEMBySEM(eleMstr.getEmId() , sEleMstr);
						
						cashStmtStatusDAO.updateCSSBySFId(cashStmt.getCsId() , sEleMstr.getSemId());
						
					}else{
						
						CashStmt cashStmt1 = new CashStmt();
						cashStmt1.setbCode(currentUser.getUserBranchCode());
						cashStmt1.setUserCode(currentUser.getUserCode());
						cashStmt1.setCsDescription(voucherService.getDesc());
						cashStmt1.setCsDrCr('D');
						cashStmt1.setCsAmt(sEleMstr.getSemPayAmt());
						cashStmt1.setCsType(voucherService.getVoucherType());
						cashStmt1.setCsVouchType("cash");
						cashStmt1.setCsFaCode(eleMstr.getBranch().getBranchFaCode());
						cashStmt1.setCsTvNo(eleMstr.getEmCrnNo());						
						cashStmt1.setCsVouchNo(voucherNo);						
						java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
						cashStmt1.setCsDt(sqlDate);
						cashStmt1.setPayMode(payMode);
						
						newMap = cashStmtStatusDAO.updateCSSForEleV(cashStmtStatus.getCssId(), cashStmt1);
						voucherNo = (int) newMap.get("vhNo");
						
						
						System.out.println("cashStmt id = "+cashStmt1.getCsId());
						sEleMstr.setSemCsId(cashStmt1.getCsId());
						sEleMstr.setbCode(currentUser.getUserBranchCode());
						sEleMstr.setUserCode(currentUser.getUserCode());
						electricityMstrDAO.updateEMBySEM(eleMstr.getEmId() , sEleMstr);
						
						
						cashStmtStatusDAO.updateCSSBySFId(cashStmt1.getCsId() , sEleMstr.getSemId());
						
						CashStmtTemp cashStmt2 = new CashStmtTemp();
						cashStmt2.setbCode(currentUser.getUserBranchCode());
						cashStmt2.setUserCode(currentUser.getUserCode());
						cashStmt2.setCsDescription(voucherService.getDesc());
						cashStmt2.setCsDrCr('C');
						cashStmt2.setCsAmt(sEleMstr.getSemPayAmt());
						cashStmt2.setCsType(voucherService.getVoucherType());
						cashStmt2.setCsVouchType("contra");
						cashStmt2.setCsFaCode(voucherService.getBranch().getBranchFaCode());
						cashStmt2.setCsBrhFaCode(eleMstr.getBranch().getBranchFaCode());
						cashStmt2.setCsTvNo(eleMstr.getEmCrnNo());						
						//cashStmt2.setCsVouchNo(voucherNo);
						cashStmt2.setCsDt(sqlDate);
						cashStmt2.setPayMode(payMode);
						
						cashStmtStatusDAO.saveCsTemp(cashStmt2);
						/*newMap = cashStmtStatusDAO.updateCSSForEleV(cashStmtStatus.getCssId(), cashStmt2);
						voucherNo = (int) newMap.get("vhNo");*/
						
						CashStmtTemp cashStmt3 = new CashStmtTemp();
						cashStmt3.setbCode(currentUser.getUserBranchCode());
						cashStmt3.setUserCode(currentUser.getUserCode());
						cashStmt3.setCsDescription(voucherService.getDesc());
						cashStmt3.setCsDrCr('D');
						cashStmt3.setCsAmt(sEleMstr.getSemPayAmt());
						cashStmt3.setCsType(voucherService.getVoucherType());
						cashStmt3.setCsVouchType("contra");
						cashStmt3.setCsFaCode(eleFaCode);
						cashStmt3.setCsBrhFaCode(eleMstr.getBranch().getBranchFaCode());
						cashStmt3.setCsTvNo(eleMstr.getEmCrnNo());						
						//cashStmt3.setCsVouchNo(voucherNo);						
						cashStmt3.setCsDt(sqlDate);
						cashStmt3.setPayMode(payMode);
						
						cashStmtStatusDAO.saveCsTemp(cashStmt3);
						/*newMap = cashStmtStatusDAO.updateCSSForEleV(cashStmtStatus.getCssId(), cashStmt3);
						voucherNo = (int) newMap.get("vhNo");*/
					}
				}
				
				electVoucherService.deleteAllEMAndSEM();
				map.put("vhNo",voucherNo);
			  }
		}else if(payBy == 'Q'){
				payMode = "Q";
				String consumerNo = "";
				char chequeType = voucherService.getChequeType();
				System.out.println("chequeType = "+chequeType);
				CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
				String bankCode = voucherService.getBankCode();
				List<BankMstr> bankMList = bankMstrDAO.getBankByBankCode(bankCode);
				if(!bankMList.isEmpty()){
					BankMstr bankMstr = bankMList.get(0);
					List<EM_SEMService> emsemList = electVoucherService.getAllEMAndSEM();
					int voucherNo = 0;
					double totAmt = 0.0;
					if(!emsemList.isEmpty()){
						for(int i=0;i<emsemList.size();i++){
							totAmt = totAmt + emsemList.get(i).getSubElectMstr().getSemPayAmt();
						}	
					}
					
					
					ChequeLeaves chequeLeaves = voucherService.getChequeLeaves();
					chequeLeaves.setBankMstr(bankMstr);
					chequeLeaves.setChqLChqAmt(totAmt);
					chequeLeaves.setChqLUsedDt(new Date(new java.util.Date().getTime()));
					chequeLeaves.setChqLUsed(true);
					String tvNo = chequeLeaves.getChqLChqNo();
					int temp = chequeLeavesDAO.updateChqLeaves(chequeLeaves);
					
					// InterBranch Case Main Cash Stmt Start
					double amount = 0.0;
					int count = 0;
					ElectricityMstr eleMstr = null;
					for(int i=0;i<emsemList.size();i++){
		
						EM_SEMService em_sem = emsemList.get(i);
						//ElectricityMstr eleMstr = em_sem.getElctMstr();
						eleMstr = em_sem.getElctMstr();
						SubElectricityMstr sEleMstr = em_sem.getSubElectMstr();
							
						String eleBrFaCode = eleMstr.getBranch().getBranchFaCode();
						amount = amount + sEleMstr.getSemPayAmt();
						consumerNo = consumerNo+eleMstr.getEmCrnNo()+",";
						/*if(brFaCode.equalsIgnoreCase(eleBrFaCode)){
							System.out.println("************** "+i);
						}else{
							amount = amount + sEleMstr.getSemPayAmt();
							count = count + 1;
						}	*/
					}
					consumerNo = consumerNo.substring(0, consumerNo.length()-1);
					System.out.println(count+" crn no are from other branches--");
					CashStmt mainCashStmt = new CashStmt();
					
					//if(count > 0){
						Map<String,Object> newMap = new HashMap<String,Object>();
						
						mainCashStmt.setbCode(currentUser.getUserBranchCode());
						mainCashStmt.setUserCode(currentUser.getUserCode());
						mainCashStmt.setCsDescription(voucherService.getDesc());
						mainCashStmt.setCsDrCr('C');
						mainCashStmt.setCsAmt(amount);
						mainCashStmt.setCsType(voucherService.getVoucherType());
						mainCashStmt.setCsVouchType("bank");
						mainCashStmt.setCsPayTo(voucherService.getPayTo());
						mainCashStmt.setCsFaCode(voucherService.getBankCode());						
						mainCashStmt.setCsTvNo(consumerNo+" ("+tvNo+")");						
						java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
						mainCashStmt.setCsDt(sqlDate);
						mainCashStmt.setPayMode(payMode);
						
						newMap = cashStmtStatusDAO.updateCSSForEleV(cashStmtStatus.getCssId(), mainCashStmt);
						voucherNo = (int) newMap.get("vhNo");
					//}
					
					// InterBranch Case Main Cash Stmt End
					
					if(temp > 0){
						if(!emsemList.isEmpty()){
							for(int i=0;i<emsemList.size();i++){
								Map<String,Object> newMap11 = new HashMap<String,Object>();
								
								EM_SEMService em_sem = emsemList.get(i);
								eleMstr = em_sem.getElctMstr();
								SubElectricityMstr sEleMstr = em_sem.getSubElectMstr();
								
								/*String tvNo = telMstr.getTmPhNo();
								tvNo = "000" + tvNo;*/
								
								String eleBrFaCode = eleMstr.getBranch().getBranchFaCode(); 
								
								if(brFaCode.equalsIgnoreCase(eleBrFaCode)){
									
									CashStmt cashStmt = new CashStmt();
									cashStmt.setbCode(currentUser.getUserBranchCode());
									cashStmt.setUserCode(currentUser.getUserCode());
									cashStmt.setCsDescription(voucherService.getDesc());
									cashStmt.setCsDrCr('D');
									cashStmt.setCsAmt(sEleMstr.getSemPayAmt());
									cashStmt.setCsType(voucherService.getVoucherType());
									cashStmt.setCsVouchType("bank");
									cashStmt.setCsPayTo(voucherService.getPayTo());
									cashStmt.setCsFaCode(eleFaCode);
									cashStmt.setCsVouchNo(voucherNo);
									cashStmt.setCsTvNo(eleMstr.getEmCrnNo()+" ("+tvNo+")");									
									sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
									cashStmt.setCsDt(sqlDate);
									cashStmt.setPayMode(payMode);
									
									newMap11 = cashStmtStatusDAO.updateCSSForEleV(cashStmtStatus.getCssId(), cashStmt);
									voucherNo = (int) newMap11.get("vhNo");
									System.out.println("cashStmt id = "+cashStmt.getCsId());
									
									sEleMstr.setSemCsId(cashStmt.getCsId());
									sEleMstr.setbCode(currentUser.getUserBranchCode());
									sEleMstr.setUserCode(currentUser.getUserCode());
									electricityMstrDAO.updateEMBySEM(eleMstr.getEmId() , sEleMstr);
									
									cashStmtStatusDAO.updateCSSBySFId(cashStmt.getCsId() , sEleMstr.getSemId());
									
								/*	CashStmt cStmt = new CashStmt();
									cStmt.setbCode(currentUser.getUserBranchCode());
									cStmt.setUserCode(currentUser.getUserCode());
									cStmt.setCsDescription(voucherService.getDesc());
									cStmt.setCsDrCr('C');
									cStmt.setCsAmt(sEleMstr.getSemPayAmt());
									cStmt.setCsType(voucherService.getVoucherType());
									cStmt.setCsVouchType("bank");
									cStmt.setCsPayTo(voucherService.getPayTo());
									cStmt.setCsVouchNo(voucherNo);
									//cStmt.setCsTvNo(tvNo);
									cStmt.setCsFaCode(voucherService.getBankCode());
									cStmt.setCsDt(sqlDate);
									
									System.out.println("cStmt is going to save in DB");
									cashStmtStatusDAO.updateCSSForEleV(cashStmtStatus.getCssId(), cStmt);
									
									System.out.println("cStmt id = "+cStmt.getCsId());*/
									
								}else{
									
									CashStmt cashStmt1 = new CashStmt();
									cashStmt1.setbCode(currentUser.getUserBranchCode());
									cashStmt1.setUserCode(currentUser.getUserCode());
									cashStmt1.setCsDescription(voucherService.getDesc());
									cashStmt1.setCsDrCr('D');
									cashStmt1.setCsAmt(sEleMstr.getSemPayAmt());
									cashStmt1.setCsType(voucherService.getVoucherType());
									cashStmt1.setCsVouchType("bank");
									cashStmt1.setCsPayTo(voucherService.getPayTo());
									cashStmt1.setCsFaCode(eleMstr.getBranch().getBranchFaCode());
									cashStmt1.setCsVouchNo(voucherNo);
									cashStmt1.setCsTvNo(eleMstr.getEmCrnNo()+ " ("+tvNo+")");									
									sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
									cashStmt1.setCsDt(sqlDate);
									cashStmt1.setPayMode(payMode);
									
									System.out.println("cashStmt1 is going to save in DB");
									newMap11 = cashStmtStatusDAO.updateCSSForEleV(cashStmtStatus.getCssId(), cashStmt1);
									voucherNo = (int) newMap11.get("vhNo");
									System.out.println("cashStmt id = "+cashStmt1.getCsId());
									
									sEleMstr.setSemCsId(cashStmt1.getCsId());
									sEleMstr.setbCode(currentUser.getUserBranchCode());
									sEleMstr.setUserCode(currentUser.getUserCode());
									electricityMstrDAO.updateEMBySEM(eleMstr.getEmId() , sEleMstr);
									
									cashStmtStatusDAO.updateCSSBySFId(cashStmt1.getCsId() , sEleMstr.getSemId());
									
									CashStmtTemp cashStmt2 = new CashStmtTemp();
									cashStmt2.setbCode(currentUser.getUserBranchCode());
									cashStmt2.setUserCode(currentUser.getUserCode());
									cashStmt2.setCsDescription(voucherService.getDesc());
									cashStmt2.setCsDrCr('C');
									cashStmt2.setCsAmt(sEleMstr.getSemPayAmt());
									cashStmt2.setCsType(voucherService.getVoucherType());
									cashStmt2.setCsVouchType("contra");
									cashStmt2.setCsPayTo(voucherService.getPayTo());
									//cashStmt2.setCsVouchNo(voucherNo);
									cashStmt2.setCsTvNo(eleMstr.getEmCrnNo()+" ("+tvNo+")");
									cashStmt2.setCsFaCode(voucherService.getBranch().getBranchFaCode());
									cashStmt2.setCsBrhFaCode(eleMstr.getBranch().getBranchFaCode());
									cashStmt2.setCsDt(sqlDate);
									cashStmt2.setPayMode(payMode);
									
									cashStmtStatusDAO.saveCsTemp(cashStmt2);
									/*System.out.println("cashStmt2 is going to save in DB");
									newMap = cashStmtStatusDAO.updateCSSForEleV(cashStmtStatus.getCssId(), cashStmt2);
									voucherNo = (int) newMap.get("vhNo");
									System.out.println("cashStmt2 id = "+cashStmt2.getCsId());*/
									
									CashStmtTemp cashStmt3 = new CashStmtTemp();
									cashStmt3.setbCode(currentUser.getUserBranchCode());
									cashStmt3.setUserCode(currentUser.getUserCode());
									cashStmt3.setCsDescription(voucherService.getDesc());
									cashStmt3.setCsDrCr('D');
									cashStmt3.setCsAmt(sEleMstr.getSemPayAmt());
									cashStmt3.setCsType(voucherService.getVoucherType());
									cashStmt3.setCsVouchType("contra");
									cashStmt3.setCsPayTo(voucherService.getPayTo());
									cashStmt3.setCsFaCode(eleFaCode);
									cashStmt3.setCsBrhFaCode(eleMstr.getBranch().getBranchFaCode());
									cashStmt3.setCsTvNo(eleMstr.getEmCrnNo()+" ("+tvNo+")");									
									//cashStmt3.setCsVouchNo(voucherNo);
									cashStmt3.setCsDt(sqlDate);
									cashStmt3.setPayMode(payMode);
									
									cashStmtStatusDAO.saveCsTemp(cashStmt3);
									/*System.out.println("cashStmt3 is going to save in DB");
									newMap = cashStmtStatusDAO.updateCSSForEleV(cashStmtStatus.getCssId(), cashStmt3);
									voucherNo = (int) newMap.get("vhNo");
									System.out.println("cashStmt3 id = "+cashStmt3.getCsId());*/
								}
								
							}
							
							electVoucherService.deleteAllEMAndSEM();
							map.put("vhNo",voucherNo);
						  }else{
							  System.out.println("emsemList list is empty");
							  map.put("vhNo",0);
						  }
					}else{
						System.out.println("chequeLeaves not updated");
						map.put("vhNo",0);
					}
				}else{
					System.out.println("invalid bank code");
					map.put("vhNo",0);
				}
		}else if(payBy == 'O'){
			payMode = "O";
			CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
			List<EM_SEMService> emsemList = electVoucherService.getAllEMAndSEM();
			int voucherNo = 0;
			if(!emsemList.isEmpty()){
				
				// InterBranch Case Main Cash Stmt Start
				double amount = 0.0;
				int count = 0;
				ElectricityMstr eleMstr = null;
				for(int i=0;i<emsemList.size();i++){	
					EM_SEMService em_sem = emsemList.get(i);
					eleMstr = em_sem.getElctMstr();
					SubElectricityMstr sEleMstr = em_sem.getSubElectMstr();
					
					String eleBrFaCode = eleMstr.getBranch().getBranchFaCode();
					amount = amount + sEleMstr.getSemPayAmt();
					/*if(brFaCode.equalsIgnoreCase(eleBrFaCode)){
						System.out.println("************** "+i);
					}else{
						amount = amount + sEleMstr.getSemPayAmt();
						count = count + 1;
					}	*/
				}
				
				System.out.println(count+" Crn no are from other branches--");
				CashStmt mainCashStmt = new CashStmt();
				
				//if(count > 0){
					Map<String,Object> newMap = new HashMap<String,Object>();
					
					mainCashStmt.setbCode(currentUser.getUserBranchCode());
					mainCashStmt.setUserCode(currentUser.getUserCode());
					mainCashStmt.setCsDescription(voucherService.getDesc());
					mainCashStmt.setCsDrCr('C');
					mainCashStmt.setCsAmt(amount);
					mainCashStmt.setCsType(voucherService.getVoucherType());
					mainCashStmt.setCsVouchType("contra");
					mainCashStmt.setCsPayTo(voucherService.getPayTo());
					mainCashStmt.setCsFaCode(voucherService.getBankCode());					
					mainCashStmt.setCsTvNo(eleMstr.getEmCrnNo());					
					java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
					mainCashStmt.setCsDt(sqlDate);
					mainCashStmt.setPayMode(payMode);
					
					newMap = cashStmtStatusDAO.updateCSSForEleV(cashStmtStatus.getCssId(), mainCashStmt);
					voucherNo = (int) newMap.get("vhNo");
					List<BankMstr> bnkList= bankMstrDAO.getBankByBankCode(voucherService.getBankCode());
					if(!bnkList.isEmpty()){
						bankMstrDAO.decBankBal(bnkList.get(0).getBnkId(), amount);
					}
				//}
				
				// InterBranch Case Main Cash Stmt End
				
				
				for(int i=0;i<emsemList.size();i++){
					Map<String,Object> newMap11 = new HashMap<String,Object>();
					
					EM_SEMService em_sem = emsemList.get(i);
					eleMstr = em_sem.getElctMstr();
					SubElectricityMstr sEleMstr = em_sem.getSubElectMstr();
					
					/*String tvNo = telMstr.getTmPhNo();
					tvNo = "000" + tvNo;*/
					
					String eleBrFaCode = eleMstr.getBranch().getBranchFaCode(); 
					
					if(brFaCode.equalsIgnoreCase(eleBrFaCode)){
						
						CashStmt cashStmt = new CashStmt();
						cashStmt.setbCode(currentUser.getUserBranchCode());
						cashStmt.setUserCode(currentUser.getUserCode());
						cashStmt.setCsDescription(voucherService.getDesc());
						cashStmt.setCsDrCr('D');
						cashStmt.setCsAmt(sEleMstr.getSemPayAmt());
						cashStmt.setCsType(voucherService.getVoucherType());
						cashStmt.setCsVouchType("bank");
						cashStmt.setCsPayTo(voucherService.getPayTo());
						cashStmt.setCsFaCode(eleFaCode);
						cashStmt.setCsVouchNo(voucherNo);
						cashStmt.setCsTvNo(eleMstr.getEmCrnNo());						
						sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
						cashStmt.setCsDt(sqlDate);
						cashStmt.setPayMode(payMode);
						
						newMap11 = cashStmtStatusDAO.updateCSSForEleV(cashStmtStatus.getCssId(), cashStmt);
						voucherNo = (int) newMap11.get("vhNo");
						System.out.println("cashStmt id = "+cashStmt.getCsId());
						
						sEleMstr.setSemCsId(cashStmt.getCsId());
						sEleMstr.setbCode(currentUser.getUserBranchCode());
						sEleMstr.setUserCode(currentUser.getUserCode());
						electricityMstrDAO.updateEMBySEM(eleMstr.getEmId() , sEleMstr);
						
						cashStmtStatusDAO.updateCSSBySFId(cashStmt.getCsId() , sEleMstr.getSemId());
						
						/*CashStmt cStmt = new CashStmt();
						cStmt.setbCode(currentUser.getUserBranchCode());
						cStmt.setUserCode(currentUser.getUserCode());
						cStmt.setCsDescription(voucherService.getDesc());
						cStmt.setCsDrCr('C');
						cStmt.setCsAmt(sEleMstr.getSemPayAmt());
						cStmt.setCsType(voucherService.getVoucherType());
						cStmt.setCsVouchType("bank");
						cStmt.setCsPayTo(voucherService.getPayTo());
						cStmt.setCsVouchNo(voucherNo);
						//cStmt.setCsTvNo(tvNo);
						cStmt.setCsFaCode(voucherService.getBankCode());
						cStmt.setCsDt(sqlDate);
						
						System.out.println("cStmt is going to save in DB");
						cashStmtStatusDAO.updateCSSForEleV(cashStmtStatus.getCssId(), cStmt);
						
						System.out.println("cStmt id = "+cStmt.getCsId());*/
						
					}else{
						
						CashStmt cashStmt1 = new CashStmt();
						cashStmt1.setbCode(currentUser.getUserBranchCode());
						cashStmt1.setUserCode(currentUser.getUserCode());
						cashStmt1.setCsDescription(voucherService.getDesc());
						cashStmt1.setCsDrCr('D');
						cashStmt1.setCsAmt(sEleMstr.getSemPayAmt());
						cashStmt1.setCsType(voucherService.getVoucherType());
						cashStmt1.setCsVouchType("bank");
						cashStmt1.setCsPayTo(voucherService.getPayTo());
						cashStmt1.setCsFaCode(eleMstr.getBranch().getBranchFaCode());
						cashStmt1.setCsVouchNo(voucherNo);
						cashStmt1.setCsTvNo(eleMstr.getEmCrnNo());						
						sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
						cashStmt1.setCsDt(sqlDate);
						cashStmt1.setPayMode(payMode);						
						
						System.out.println("cashStmt1 is going to save in DB");
						newMap11 = cashStmtStatusDAO.updateCSSForEleV(cashStmtStatus.getCssId(), cashStmt1);
						voucherNo = (int) newMap11.get("vhNo");
						System.out.println("cashStmt id = "+cashStmt1.getCsId());
						
						sEleMstr.setSemCsId(cashStmt1.getCsId());
						sEleMstr.setbCode(currentUser.getUserBranchCode());
						sEleMstr.setUserCode(currentUser.getUserCode());
						electricityMstrDAO.updateEMBySEM(eleMstr.getEmId() , sEleMstr);
						
						cashStmtStatusDAO.updateCSSBySFId(cashStmt1.getCsId() , sEleMstr.getSemId());
						
						CashStmtTemp cashStmt2 = new CashStmtTemp();
						cashStmt2.setbCode(currentUser.getUserBranchCode());
						cashStmt2.setUserCode(currentUser.getUserCode());
						cashStmt2.setCsDescription(voucherService.getDesc());
						cashStmt2.setCsDrCr('C');
						cashStmt2.setCsAmt(sEleMstr.getSemPayAmt());
						cashStmt2.setCsType(voucherService.getVoucherType());
						cashStmt2.setCsVouchType("contra");
						cashStmt2.setCsPayTo(voucherService.getPayTo());
						cashStmt2.setCsVouchNo(voucherNo);
						cashStmt2.setCsTvNo(eleMstr.getEmCrnNo());
						cashStmt2.setCsFaCode(voucherService.getBranch().getBranchFaCode());
						cashStmt2.setCsBrhFaCode(eleMstr.getBranch().getBranchFaCode());
						cashStmt2.setCsDt(sqlDate);
						cashStmt2.setPayMode(payMode);
						
						cashStmtStatusDAO.saveCsTemp(cashStmt2);
						/*System.out.println("cashStmt2 is going to save in DB");
						newMap = cashStmtStatusDAO.updateCSSForEleV(cashStmtStatus.getCssId(), cashStmt2);
						voucherNo = (int) newMap.get("vhNo");
						System.out.println("cashStmt2 id = "+cashStmt2.getCsId());*/
						
						CashStmtTemp cashStmt3 = new CashStmtTemp();
						cashStmt3.setbCode(currentUser.getUserBranchCode());
						cashStmt3.setUserCode(currentUser.getUserCode());
						cashStmt3.setCsDescription(voucherService.getDesc());
						cashStmt3.setCsDrCr('D');
						cashStmt3.setCsAmt(sEleMstr.getSemPayAmt());
						cashStmt3.setCsType(voucherService.getVoucherType());
						cashStmt3.setCsVouchType("contra");
						cashStmt3.setCsFaCode(eleFaCode);
						cashStmt3.setCsPayTo(voucherService.getPayTo());
						cashStmt3.setCsBrhFaCode(eleMstr.getBranch().getBranchFaCode());
						cashStmt3.setCsTvNo(eleMstr.getEmCrnNo());						
						//cashStmt3.setCsVouchNo(voucherNo);
						cashStmt3.setCsDt(sqlDate);
						cashStmt3.setPayMode(payMode);
						
						cashStmtStatusDAO.saveCsTemp(cashStmt3);
						/*System.out.println("cashStmt3 is going to save in DB");
						newMap = cashStmtStatusDAO.updateCSSForEleV(cashStmtStatus.getCssId(), cashStmt3);
						voucherNo = (int) newMap.get("vhNo");
						System.out.println("cashStmt3 id = "+cashStmt3.getCsId());*/
						
					}
					
				}
				
				electVoucherService.deleteAllEMAndSEM();
				map.put("vhNo",voucherNo);
			  }
		}else{
			map.put("vhNo",0);
			map.put("tvNo","0");
			System.out.println("invalid payment method");
		}
		
		return map;
	}
	
	//TODO Back data
	@RequestMapping(value = "/getVDetFrEVBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrTelVBack() {  
		System.out.println("Enter into getVDetFrTelVBack---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}
	

	
	
}

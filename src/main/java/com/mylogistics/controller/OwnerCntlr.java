package com.mylogistics.controller;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.mylogistics.DAO.AddressDAO;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.ContPersonDAO;
import com.mylogistics.DAO.FAParticularDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.MultipleStateDAO;
import com.mylogistics.DAO.MultipleStationDAO;
import com.mylogistics.DAO.OwnerDAO;
import com.mylogistics.DAO.StateDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.DAO.VehicleTypeDAO;
import com.mylogistics.DAO.VehicleVendorDAO;
import com.mylogistics.constants.OwnerCNTS;
import com.mylogistics.model.Address;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Broker;
import com.mylogistics.model.ContPerson;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.MultipleState;
import com.mylogistics.model.MultipleStation;
import com.mylogistics.model.Owner;
import com.mylogistics.model.OwnerImg;
import com.mylogistics.model.State;
import com.mylogistics.model.Station;
import com.mylogistics.model.TransferVehicle;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleType;
import com.mylogistics.model.VehicleVendorMstr;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.ImageService;
import com.mylogistics.services.ModelService;
import com.mylogistics.services.MultipleStateService;
import com.mylogistics.services.MultipleStateServiceImpl;
import com.mylogistics.services.MultipleStationService;
import com.mylogistics.services.MultipleStationServiceImpl;
import com.mylogistics.services.VoucherService;

@Controller
public class OwnerCntlr {
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private StationDAO stationDAO;
	
	@Autowired
	private StateDAO stateDAO;
	
	@Autowired
	private VehicleTypeDAO vehicleTypeDAO;
	
	@Autowired
	private VehicleVendorDAO vehicleVendorDAO;
	
	@Autowired
	private ContPersonDAO contPersonDAO;
	
	@Autowired
	private AddressDAO addressDAO;
	
	@Autowired
	private OwnerDAO ownerDAO;
	
	@Autowired 
	private MultipleStateDAO multipleStateDAO;
	
	@Autowired
	private MultipleStationDAO multipleStationDAO;
	
	@Autowired
    private HttpSession httpSession;
	
	@Autowired
	private FAParticularDAO faParticularDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	/*private String ownerBankImgPath = "/var/www/html/Erp_Image/Owner/BankDet";
	private String ownerPanImgPath = "/var/www/html/Erp_Image/Owner/Pan";
	private String ownerDecImgPath = "/var/www/html/Erp_Image/Owner/Dec";
	private String rcImgPath = "/var/www/html/Erp_Image/Vehicle/RC";
	private String psImgPath = "/var/www/html/Erp_Image/Vehicle/PerSlp";
	private String policyImgPath = "/var/www/html/Erp_Image/Vehicle/VehPolicy";*/
	
	private MultipleStateService multipleStateService = new MultipleStateServiceImpl();
	
	private MultipleStationService multipleStationService = new MultipleStationServiceImpl();
	
	private ImageService imageService = new ImageService();
	
	private static Logger logger = Logger.getLogger(OwnerCntlr.class);
	
	
	@RequestMapping(value = "/getBranchDataForOwner", method = RequestMethod.POST)
	public @ResponseBody Object getBranchData() {

		List<Branch> branchList = new ArrayList<Branch>();
		Map<String, Object> map = new HashMap<String, Object>();
		branchList = branchDAO.getAllActiveBranches();
		if(!branchList.isEmpty()){
			map.put("list", branchList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getStateDataForOwner", method = RequestMethod.POST)
	public @ResponseBody Object getStateData(@RequestBody String staNameCode) {
		List<State> listState = stateDAO.getStateDataByStaNameCode(staNameCode);
		Map<String, Object> map = new HashMap<String, Object>();
		if(!listState.isEmpty()){
			map.put("list", listState);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getStationDataForOwner", method = RequestMethod.POST)
	public @ResponseBody Object getStationData(@RequestBody String staNameCode) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Station> stationList = new ArrayList<Station>();
		stationList = stationDAO.getStationDataByNameCode(staNameCode);
		if(!stationList.isEmpty()){
			map.put("list", stationList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getVehicleTypeCodeForOwnerByVNameCode", method = RequestMethod.POST)
	public @ResponseBody Object getVehicleTypeCode(@RequestBody String vNameCode) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<VehicleType> vtList = new ArrayList<VehicleType>();
		 vtList = vehicleTypeDAO.getVehicleTypeByVNameCode(vNameCode);
		 if (!vtList.isEmpty()) {	 
			 map.put("list", vtList);
			 map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		 }else{
			 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		 }
		return map;
	}
	
	@RequestMapping(value = "/getVehicleTypeCodeForOwner", method = RequestMethod.POST)
	public @ResponseBody Object getVehicleTypeCode() {
		Map<String, Object> map = new HashMap<String, Object>();
		List<VehicleType> vtList = new ArrayList<VehicleType>();
		 vtList = vehicleTypeDAO.getVehicleType();
		 if (!vtList.isEmpty()) {	 
			 map.put("list", vtList);
			 map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		 }else{
			 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		 }
		return map;
	}

	@RequestMapping(value = "/SubmitOwner", method = RequestMethod.POST)
	public @ResponseBody Object SubmitOwner(@RequestBody ModelService modelService) throws Exception{
		logger.info("Enter into /SubmitOwner().....");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			
			Address currentAddress = modelService.getCurrentAddress();
			Address registerAddress = modelService.getRegisterAddress();
			Address otherAddress = modelService.getOtherAddress();
			Owner owner = modelService.getOwner();
			
			User currentUser = (User)httpSession.getAttribute("currentUser");
			
			String ownerCode=null;
			String faCode=null;
			
			Boolean available = ownerDAO.isOwnerAvailable(modelService.getOwner().getOwnPanNo());
			
			if(available){
				
				List<FAParticular> faParticulars = faParticularDAO.getFAParticular(ConstantsValues.FAP_OWN);
				FAParticular fAParticular = new FAParticular();
				fAParticular=faParticulars.get(0);
				int fapId=fAParticular.getFaPerId();
				String fapIdStr = String.valueOf(fapId); 
				if(fapId<10)
					fapIdStr="0"+fapIdStr;
				owner.setfAParticular(fAParticular);
				owner.setView(false);
				owner.setUserCode(currentUser.getUserCode());
				owner.setbCode(currentUser.getUserBranchCode());
				
				OwnerImg ownImg = new OwnerImg();
				
				if (imageService.getImage() != null && imageService.getDecImg() != null) {
					owner.setOwnPanIntRt(0);
				} else if (imageService.getImage() != null ) {
					if (owner.getOwnPanNo().charAt(3) == 'P' ||	owner.getOwnPanNo().charAt(3) == 'H' ||	owner.getOwnPanNo().charAt(3) == 'J') {
						owner.setOwnPanIntRt(1);
					}else {
						owner.setOwnPanIntRt(2);
					}
				}
				
				Integer ownId = ownerDAO.saveOwnAndImg(owner,ownImg, imageService.getImage(), imageService.getDecImg());
				
				imageService.setImage(null);
				imageService.setDecImg(null);
				
				if(ownId > 0){
					
					ownerCode = "own"+String.valueOf(ownId); 
					ownId=ownId+100000;
					String ownIdStr = String.valueOf(ownId).substring(1,6);
					faCode=fapIdStr+ownIdStr;
					owner.setOwnFaCode(faCode);
					
					int updateOwner= ownerDAO.updateOwner(owner);
					
					FAMaster faMaster = new FAMaster();
					faMaster.setFaMfaCode(faCode);
					faMaster.setFaMfaType("owner");
					faMaster.setFaMfaName(owner.getOwnName());
					faMaster.setbCode(currentUser.getUserBranchCode());
					faMaster.setUserCode(currentUser.getUserCode());
					
					faMasterDAO.saveFaMaster(faMaster);
					
					if (currentAddress.getAddType() != null) {
						currentAddress.setUserCode(currentUser.getUserCode());
						currentAddress.setbCode(currentUser.getUserBranchCode());
						currentAddress.setAddRefCode(ownerCode);
						
						addressDAO.saveAddress(currentAddress);
					}
					
					if(registerAddress.getAddType() != null && !(registerAddress.getAddType().equalsIgnoreCase("no"))){
						registerAddress.setUserCode(currentUser.getUserCode());
						registerAddress.setbCode(currentUser.getUserBranchCode());
						registerAddress.setAddRefCode(ownerCode);
						
						addressDAO.saveAddress(registerAddress);
					}
					
					if(otherAddress.getAddType() != null && !(otherAddress.getAddType().equalsIgnoreCase("no"))){
						otherAddress.setUserCode(currentUser.getUserCode());
						otherAddress.setbCode(currentUser.getUserBranchCode());
						otherAddress.setAddRefCode(ownerCode);
						
						addressDAO.saveAddress(otherAddress);
					}
					
					List<MultipleState> mStates = multipleStateService.getAllState();
					List<MultipleStation> mStations = multipleStationService.getAllStation();
					
					Iterator it = mStates.iterator();
					while(it.hasNext()){
						MultipleState state = (MultipleState) it.next();
						state.setbCode(currentUser.getUserBranchCode());
						state.setUserCode(currentUser.getUserCode());
						state.setMulStateRefCode(ownerCode);
						
						multipleStateDAO.saveMultipleState(state);
					}
					
					it = mStations.iterator();
					while(it.hasNext()){
						MultipleStation stn = (MultipleStation) it.next();
						stn.setMulStnRefCode(ownerCode);
						stn.setbCode(currentUser.getUserBranchCode());
						stn.setUserCode(currentUser.getUserCode());
						
						multipleStationDAO.saveMultipleStation(stn);
					}
					
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					resultMap.put("ownCode", ownerCode);
					resultMap.put("msg", "Owner is saved !");
					
					logger.info("Owner is successfully saved !");
					
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Please try again !");
				}
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Owner already exists !");
			}
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "ERROR !");
		}
		logger.info("Exit from /submitOwner()");
		return resultMap;
	}
	
	
	
	
	
	
	@RequestMapping(value = "/fetchStateList", method = RequestMethod.GET)
	public @ResponseBody Object fetchStateList() {
		System.out.println("enter into fetchStateList function");
		List<MultipleState> multipleStates = multipleStateService.getAllState();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", multipleStates);
		return map;
	}

	
	@RequestMapping(value = "/addState", method = RequestMethod.POST)
	public @ResponseBody Object addState(@RequestBody MultipleState multipleState) {

		System.out.println("enter into addState function");
		multipleStateService.addState(multipleState);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeState", method = RequestMethod.POST)
	public @ResponseBody Object removeState(@RequestBody MultipleState multipleState) {

		System.out.println("enter into removeState function");
		multipleStateService.deleteState(multipleState);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeAllState", method = RequestMethod.POST)
	public @ResponseBody Object removeAllState() {
		System.out.println("enter into removeAllState function");
		multipleStateService.deleteAllState();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/fetchStationList", method = RequestMethod.GET)
	public @ResponseBody Object fetchStationList() {
		System.out.println("enter into fetchStationList function");
		List<MultipleStation> multipleStations = multipleStationService.getAllStation();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", multipleStations);
		return map;
	}
	
	@RequestMapping(value = "/addStation", method = RequestMethod.POST)
	public @ResponseBody Object addStation(@RequestBody MultipleStation multipleStation) {

		System.out.println("enter into addStation function");
		multipleStationService.addStation(multipleStation);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeStation", method = RequestMethod.POST)
	public @ResponseBody Object removeStation(@RequestBody MultipleStation multipleStation) {

		System.out.println("enter into removeStation function");
		
		multipleStationService.deleteStation(multipleStation);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeAllStation", method = RequestMethod.POST)
	public @ResponseBody Object removeAllStation() {
		System.out.println("enter into removeAllStation function");
		multipleStationService.deleteAllStation();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/getOwnerCodes", method = RequestMethod.POST)
	public @ResponseBody Object getOwnerCodes() {
		
		List<String> oList = ownerDAO.getOwnerCode();
		Map<String, Object> map = new HashMap<String, Object>();
		if(!oList.isEmpty()){
			map.put("list", oList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/saveNewState", method = RequestMethod.POST)
	public @ResponseBody Object saveNewState(@RequestBody State state) {
		System.out.println("Controller called of saveNewStateToDB");
		Map<String, String> map = new HashMap<String, String>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		state.setUserCode(currentUser.getUserCode());
		state.setbCode(currentUser.getUserBranchCode());
		int temp = stateDAO.saveStateToDB(state);
		if (temp >= 0) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/saveNewStation", method = RequestMethod.POST)
	public @ResponseBody Object saveNewStation(@RequestBody Station station) {

		System.out.println("Controller called of saveNewStation");

		Map<String, String> map = new HashMap<String, String>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		station.setUserCode(currentUser.getUserCode());
		station.setbCode(currentUser.getUserBranchCode());
		int temp = stationDAO.saveStationToDB(station);
		if (temp >= 0) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/upldOwnPanImg", method = RequestMethod.POST)
	public @ResponseBody Object upldOwnPanImg(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into upldOwnPanImg funciton");
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			imageService.setImage(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			imageService.setImage(blob) ;
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	@RequestMapping(value = "/upldOwnDecImg", method = RequestMethod.POST)
	public @ResponseBody Object upldOwnDecImg(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into upldOwnDecImg funciton");
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			imageService.setDecImg(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			imageService.setDecImg(blob);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getOwnCodeList", method=RequestMethod.POST)
	public @ResponseBody Object getOwnCodeList(){
		System.out.println("Entered into getOwnCodeList");
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> ownCodeList = ownerDAO.getOwnNameCodeIdFa();
		
		if (!ownCodeList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("ownCodeList", ownCodeList);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}		
		return map;
	}
	
	@RequestMapping(value = "/getOwnerByNameCode", method = RequestMethod.POST)
	public @ResponseBody Object getOwnerByNameCode(@RequestBody String ownNameCode){
		User user = (User) httpSession.getAttribute("currentUser");
		logger.info("UserID = "+user.getUserId()+" : Enter into /getOwnerByNameCode() : OwnNameCode = "+ownNameCode);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		try{
			List<String> proList = new ArrayList<String>();
			
			proList.add(OwnerCNTS.OWN_ID);
			proList.add(OwnerCNTS.OWN_NAME);
			proList.add(OwnerCNTS.OWN_CODE);
			proList.add(OwnerCNTS.OWN_PAN_NO);			
			
			List<Owner> ownList = ownerDAO.getOwnerByNameCode(session, user, ownNameCode, proList);
			
			if(ownList.isEmpty()){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "No such owner found !");
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				resultMap.put("ownerList", ownList);
			}
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}finally{
			session.clear();
			session.close();
		}
		logger.info("UserID = "+user.getUserId()+" : Exit from /getOwnerByNameCode() : OwnNameCode = "+ownNameCode);
		return resultMap;
	}
	
	
	@RequestMapping(value = "/getOwnerView", method=RequestMethod.POST)
	public @ResponseBody Object getOwnerView(){
		System.out.println("Entered into getOwnerView");
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> ownCodeList = ownerDAO.getOwnNamePhCodeIdFa();
		
		if (!ownCodeList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("list", ownCodeList);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}		
		return map;
	}
	
	
	@RequestMapping(value = "/checkOwnerPan", method=RequestMethod.POST)
	public @ResponseBody Object checkOwnerPan(@RequestBody String ownPanNo){
		System.out.println("Entered into getOwnerView");
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> ownCodeList = ownerDAO.getOwnNameCodeByPanNo(ownPanNo);
		
		if (!ownCodeList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("list", ownCodeList);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}		
		return map;
	}
	
	
	
	@RequestMapping(value = "/submitVehilceOwner", method = RequestMethod.POST)
	public @ResponseBody Object submitVehilceOwner(@RequestBody ModelService modelService) {
		logger.info("Enter into /SubmitVehilceOwner().....");
		Session session=this.sessionFactory.openSession();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		Transaction transaction=null;
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			System.out.println(modelService.getOwner().getOwnName());
			System.out.println(modelService.getVehVenMstr().getVvRcNo());
			System.out.println(modelService.getRegisterAddress());
			
			boolean v=vehicleVendorDAO.checkVehicleExist(modelService.getVehVenMstr().getVvRcNo(),session);
			if(v) {
				resultMap.put("result", "error");
				resultMap.put("msg", "Vehicle already exist");
				
				return resultMap;
			}
			
			
			boolean b=ownerDAO.isOwnerAvailable(session,modelService.getOwner().getOwnPanNo());
			
				
			transaction=session.beginTransaction();
			if(b) {
				int ownId=ownerDAO.saveOwner(session,modelService.getOwner(),imageService.getImage(),imageService.getDecImg(),imageService.getChqImg());
				

				
				int vvId=vehicleVendorDAO.saveVehVenMstr(session,modelService.getVehVenMstr(),ownId,imageService.getRcImg(),imageService.getInsImg(),imageService.getPsImg());
				int fa=800000+ownId;
				String faCode="0"+fa;
				FAMaster fAMaster=new FAMaster();
				fAMaster.setbCode(currentUser.getUserBranchCode());
				fAMaster.setFaMfaCode(faCode);
				fAMaster.setFaMfaName(modelService.getOwner().getOwnName());
				fAMaster.setFaMfaType("owner");
				fAMaster.setUserCode(currentUser.getUserCode());
				int faId=faMasterDAO.saveFaMaster(fAMaster,session);
				
				Address address=new Address();
				address.setAddCity(modelService.getRegisterAddress().getAddCity());
				address.setAddPin(modelService.getRegisterAddress().getAddPin());
				address.setAddDist(modelService.getRegisterAddress().getAddDist());
				address.setAddPost(modelService.getRegisterAddress().getAddPost());
				address.setAddRefCode("own"+ownId);
				address.setAddState(modelService.getRegisterAddress().getAddState());
				address.setAddType("Current Address");
				address.setCompleteAdd(modelService.getRegisterAddress().getCompleteAdd());
				address.setbCode(currentUser.getUserBranchCode());
				address.setUserCode(currentUser.getUserCode());
				addressDAO.saveAddress(address,session);
				
				
				
				
				resultMap.put("ownId", ownId);
				resultMap.put("ownCode", "own"+ownId);
				resultMap.put("vvId", vvId);
				resultMap.put("result", "success");
				
				
			}else {
				
				if(modelService.getOwner().getOwnCode()==null) {
					resultMap.put("result", "exist");
					resultMap.put("msg", "Owner already exist");
				}else {
					
					Owner owner=modelService.getOwner();
					
					int ownId=ownerDAO.updateOwner(session,modelService.getOwner(),imageService.getImage(),imageService.getDecImg(),imageService.getChqImg());
					
					
					int vvId=vehicleVendorDAO.saveVehVenMstr(session,modelService.getVehVenMstr(),owner.getOwnId(),imageService.getRcImg(),imageService.getInsImg(),imageService.getPsImg());
					
					
					resultMap.put("ownId", ownId);
					resultMap.put("ownCode", "own"+ownId);
					resultMap.put("vvId", vvId);
					
					String addType="Current Address";
					String addRef=owner.getOwnCode();
					List<Address> crntAddList=addressDAO.getAddByTypeRef(session,addType,addRef);
					
					if(crntAddList.isEmpty()) {
						Address address=new Address();
						address.setAddCity(modelService.getRegisterAddress().getAddCity());
						address.setAddPin(modelService.getRegisterAddress().getAddPin());
						address.setAddDist(modelService.getRegisterAddress().getAddDist());
						address.setAddPost(modelService.getRegisterAddress().getAddPost());
						address.setAddRefCode(addRef);
						address.setAddState(modelService.getRegisterAddress().getAddState());
						address.setAddType("Current Address");
						address.setCompleteAdd(modelService.getRegisterAddress().getCompleteAdd());
						address.setbCode(currentUser.getUserBranchCode());
						address.setUserCode(currentUser.getUserCode());
						addressDAO.saveAddress(address,session);
						
					}else {
						
						Address address=crntAddList.get(0);
						address.setAddCity(modelService.getRegisterAddress().getAddCity());
						address.setAddPin(modelService.getRegisterAddress().getAddPin());
						address.setAddDist(modelService.getRegisterAddress().getAddDist());
						address.setAddPost(modelService.getRegisterAddress().getAddPost());
						address.setAddRefCode(addRef);
						address.setAddState(modelService.getRegisterAddress().getAddState());
						address.setAddType("Current Address");
						address.setCompleteAdd(modelService.getRegisterAddress().getCompleteAdd());
						address.setbCode(currentUser.getUserBranchCode());
						address.setUserCode(currentUser.getUserCode());
						addressDAO.updateAddress(address,session);
						
						
					}
					
				}
				
					
				
			}
			
			session.flush();
			session.clear();
			transaction.commit();
			resultMap.put("result", "success");
			
		}catch(Exception e) {
			resultMap.put("result", "error");
			resultMap.put("msg", "Retry");
			e.printStackTrace();
			transaction.rollback();
		}finally {
			imageService.setImage(null);
			imageService.setDecImg(null);
			imageService.setChqImg(null);
			imageService.setRcImg(null);
			imageService.setInsImg(null);
			imageService.setPsImg(null);
			session.close();
		}
	return resultMap;
	}
	
	
	
	
	@RequestMapping(value = "/getOwnerDetail", method=RequestMethod.POST)
	public @ResponseBody Object getOwnerDetail(@RequestBody int ownId){
		System.out.println("Entered into getOwnerDetail");
		Map<String, Object> map = new HashMap<>();
		map = ownerDAO.getOwnerDetail(ownId);
		return map;
	}
	
	
	
	@RequestMapping(value = "/upldOwnPanImgN", method = RequestMethod.POST)
	public @ResponseBody Object upldOwnPanImgN(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into upldOwnPanImg funciton");
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			imageService.setImage(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			imageService.setImage(blob) ;
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	@RequestMapping(value = "/upldOwnDecImgN", method = RequestMethod.POST)
	public @ResponseBody Object upldOwnDecImgN(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into upldOwnDecImg funciton");
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			imageService.setDecImg(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			imageService.setDecImg(blob);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/upldOwnChqImgN", method = RequestMethod.POST)
	public @ResponseBody Object upldOwnChqImgN(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into upldOwnDecImg funciton");
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			imageService.setChqImg(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			imageService.setChqImg(blob);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/upldOwnRcImgN", method = RequestMethod.POST)
	public @ResponseBody Object upldOwnRcImgN(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into upldOwnDecImg funciton");
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			imageService.setRcImg(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			imageService.setRcImg(blob);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	/*@RequestMapping(value = "/upldOwnTcImgN", method = RequestMethod.POST)
	public @ResponseBody Object upldOwnTcImgN(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into upldOwnDecImg funciton");
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			imageService.setTcImg(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			imageService.setTcImg(blob);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}*/
	
	@RequestMapping(value = "/upldOwnPsImgN", method = RequestMethod.POST)
	public @ResponseBody Object upldOwnPsImgN(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into upldOwnDecImg funciton");
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			imageService.setPsImg(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			imageService.setPsImg(blob);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/upldOwnInsImgN", method = RequestMethod.POST)
	public @ResponseBody Object upldOwnInsImgN(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into upldOwnDecImg funciton");
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			imageService.setInsImg(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			imageService.setInsImg(blob);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	
	@RequestMapping(value = "/checkVehicleExist", method=RequestMethod.POST)
	public @ResponseBody Object checkVehicleExist(@RequestBody Map<String,String> vehicleMap){
		System.out.println("Entered into checkVehicleExist");
		Map<String, String> map = new HashMap<>();

		map=vehicleVendorDAO.checkVehicleExist(vehicleMap);
		return map;
	}
	
	
	
	
	
	@RequestMapping(value = "/submitVehilceTrfOwner", method = RequestMethod.POST)
	public @ResponseBody Object submitVehilceTrfOwner(@RequestBody ModelService modelService) {
		logger.info("Enter into /SubmitVehilceTrfOwner().....");
		Session session=this.sessionFactory.openSession();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		Transaction transaction=null;
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			System.out.println(modelService.getOwner().getOwnName());
			System.out.println(modelService.getVehVenMstr().getVvRcNo());
			System.out.println(modelService.getRegisterAddress());
			
			VehicleVendorMstr vv=vehicleVendorDAO.getVehicleByRc(modelService.getVehVenMstr().getVvRcNo(),session);
			if(vv==null) {
				resultMap.put("result", "error");
				resultMap.put("msg", "Vehicle not found");
				
				return resultMap;
			}
			
			
			boolean b=ownerDAO.isOwnerAvailable(session,modelService.getOwner().getOwnPanNo());
			
				
			transaction=session.beginTransaction();
			if(b) {
				int ownId=ownerDAO.saveOwner(session,modelService.getOwner(),imageService.getImage(),imageService.getDecImg(),imageService.getChqImg());
				
				TransferVehicle tv=new TransferVehicle();
				tv.setOldOwnId(vv.getOwner().getOwnId());
				tv.setVvId(vv.getVvId());
				tv.setOwnId(ownId);
				tv.setTransferDate(modelService.getVehVenMstr().getVvTransferDt());
				tv.setTransferId(modelService.getVehVenMstr().getVvTransferId());
				tv.setUserCode(currentUser.getUserCode());
				vehicleVendorDAO.saveTransferVehicle(session, tv);
				
				vv.setVvTransferDt(tv.getTransferDate());
				vv.setVvTransferId(tv.getTransferId());
				vehicleVendorDAO.updateVehVenMstr(session,vv,ownId,imageService.getRcImg(),imageService.getInsImg(),imageService.getPsImg());
				int fa=800000+ownId;
				String faCode="0"+fa;
				FAMaster fAMaster=new FAMaster();
				fAMaster.setbCode(currentUser.getUserBranchCode());
				fAMaster.setFaMfaCode(faCode);
				fAMaster.setFaMfaName(modelService.getOwner().getOwnName());
				fAMaster.setFaMfaType("owner");
				fAMaster.setUserCode(currentUser.getUserCode());
				faMasterDAO.saveFaMaster(fAMaster,session);
				
				Address address=new Address();
				address.setAddCity(modelService.getRegisterAddress().getAddCity());
				address.setAddPin(modelService.getRegisterAddress().getAddPin());
				address.setAddDist(modelService.getRegisterAddress().getAddDist());
				address.setAddPost(modelService.getRegisterAddress().getAddPost());
				address.setAddRefCode("own"+ownId);
				address.setAddState(modelService.getRegisterAddress().getAddState());
				address.setAddType("Current Address");
				address.setCompleteAdd(modelService.getRegisterAddress().getCompleteAdd());
				address.setbCode(currentUser.getUserBranchCode());
				address.setUserCode(currentUser.getUserCode());
				addressDAO.saveAddress(address,session);
				
				
				
				resultMap.put("ownId", ownId);
				resultMap.put("ownCode", "own"+ownId);
				resultMap.put("vvId", vv.getVvId());
				resultMap.put("result", "success");
				
				
			}else {
				
				if(modelService.getOwner().getOwnCode()==null) {
					resultMap.put("result", "exist");
					resultMap.put("msg", "Owner already exist");
				}else {
					
					Owner owner=modelService.getOwner();
					
					int ownId=ownerDAO.updateOwner(session,modelService.getOwner(),imageService.getImage(),imageService.getDecImg(),imageService.getChqImg());
					
					
					TransferVehicle tv=new TransferVehicle();
					tv.setOldOwnId(vv.getOwner().getOwnId());
					tv.setOwnId(ownId);
					tv.setVvId(vv.getVvId());
					tv.setTransferDate(modelService.getVehVenMstr().getVvTransferDt());
					tv.setTransferId(modelService.getVehVenMstr().getVvTransferId());
					tv.setUserCode(currentUser.getUserCode());
					vehicleVendorDAO.saveTransferVehicle(session, tv);
					
					vv.setVvTransferDt(tv.getTransferDate());
					vv.setVvTransferId(tv.getTransferId());
					vehicleVendorDAO.updateVehVenMstr(session,vv,owner.getOwnId(),imageService.getRcImg(),imageService.getInsImg(),imageService.getPsImg());
					
					
					resultMap.put("ownId", ownId);
					resultMap.put("ownCode", "own"+ownId);
					resultMap.put("vvId", vv.getVvId());
					
					String addType="Current Address";
					String addRef=owner.getOwnCode();
					List<Address> crntAddList=addressDAO.getAddByTypeRef(session,addType,addRef);
					
					if(crntAddList.isEmpty()) {
						Address address=new Address();
						address.setAddCity(modelService.getRegisterAddress().getAddCity());
						address.setAddPin(modelService.getRegisterAddress().getAddPin());
						address.setAddDist(modelService.getRegisterAddress().getAddDist());
						address.setAddPost(modelService.getRegisterAddress().getAddPost());
						address.setAddRefCode(addRef);
						address.setAddState(modelService.getRegisterAddress().getAddState());
						address.setAddType("Current Address");
						address.setCompleteAdd(modelService.getRegisterAddress().getCompleteAdd());
						address.setbCode(currentUser.getUserBranchCode());
						address.setUserCode(currentUser.getUserCode());
						addressDAO.saveAddress(address,session);
						
					}else {
						
						Address address=crntAddList.get(0);
						address.setAddCity(modelService.getRegisterAddress().getAddCity());
						address.setAddPin(modelService.getRegisterAddress().getAddPin());
						address.setAddDist(modelService.getRegisterAddress().getAddDist());
						address.setAddPost(modelService.getRegisterAddress().getAddPost());
						address.setAddRefCode(addRef);
						address.setAddState(modelService.getRegisterAddress().getAddState());
						address.setAddType("Current Address");
						address.setCompleteAdd(modelService.getRegisterAddress().getCompleteAdd());
						address.setbCode(currentUser.getUserBranchCode());
						address.setUserCode(currentUser.getUserCode());
						addressDAO.updateAddress(address,session);
						
						
					}
					
					
				}
				
				
			}
			
			session.flush();
			session.clear();
			transaction.commit();
			resultMap.put("result", "success");
			
		}catch(Exception e) {
			resultMap.put("result", "error");
			resultMap.put("msg", "Retry");
			e.printStackTrace();
			transaction.rollback();
		}finally {
			imageService.setImage(null);
			imageService.setDecImg(null);
			imageService.setChqImg(null);
			imageService.setRcImg(null);
			imageService.setInsImg(null);
			imageService.setPsImg(null);
			//imageService.setTcImg(null);
			session.close();
		}
	return resultMap;
	}

	
	
	
	@RequestMapping(value = "/getOwnerFrBnkVerify", method=RequestMethod.POST)
	public @ResponseBody Object getOwnerFrBnkVerify(){
		System.out.println("Entered into getOwnerFrBnkVerify");
		Map<String, Object> map = new HashMap<>();
		
		List<Owner> ownList = ownerDAO.getOwnFrBnkVerify();
		
		if (!ownList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("ownList", ownList);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("msg", "No record found");
		}		
		return map;
	}
	
	
	@RequestMapping(value = "/getOwnerInvalidAcc", method=RequestMethod.POST)
	public @ResponseBody Object getOwnerInvalidAcc(){
		System.out.println("Entered into getOwnerFrBnkVerify");
		Map<String, Object> map = new HashMap<>();
		
		List<Owner> ownList = ownerDAO.getOwnerInvalidAcc();
		
		if (!ownList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("ownList", ownList);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("msg", "No record found");
		}		
		return map;
	}
	
	
	@RequestMapping(value = "/validOwnBnkDet", method=RequestMethod.POST)
	public @ResponseBody Object validOwnBnkDet(@RequestBody int ownId){
		System.out.println("Entered into validBnkDet");
		Map<String, String> map = new HashMap<>();
		
			map=ownerDAO.bnkInfoValid(ownId);
				
		return map;
	}
	
	
	@RequestMapping(value = "/invalidOwnBnkDet", method=RequestMethod.POST)
	public @ResponseBody Object invalidOwnBnkDet(@RequestBody int ownId){
		System.out.println("Entered into validBnkDet");
		Map<String, String> map = new HashMap<>();
		
			map=ownerDAO.bnkInfoInValid(ownId);
		
		return map;
	}
	
	
	@RequestMapping(value = "/updateOwnBnkDet", method=RequestMethod.POST)
	public @ResponseBody Object updateOwnBnkDet(@RequestBody Owner own){
		System.out.println("Entered into validBnkDet");
		Map<String, String> map = new HashMap<>();
		
			map=ownerDAO.bnkInfoUpdate(own);
		
		return map;
	}
	
	
	@RequestMapping(value = "/uploadOwnChqImage", method = RequestMethod.POST)
	public @ResponseBody Object uploadOwnChqImage(@RequestParam("file") MultipartFile file,@RequestParam("id") Integer ownId){	
		logger.info("Enter into /uploadOwnChqImage() : FileName = "+file.getName());
		System.out.println("Enter into /upldExcelFile() : File = "+file.getName());
		System.out.println("Enter into /upldExcelFile() : File = "+file.getOriginalFilename());
		
		Map<String, String> resultMap = new HashMap<String, String>();
		try{
			byte [] fileInBytes = file.getBytes();
			resultMap = ownerDAO.uploadChqImg(fileInBytes, ownId);
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put("result", "error");
			resultMap.put("msg", "There is some problem to upload file");
			resultMap.put("exp", "Exception="+e);
		}
		logger.info("Exit from /upldExcelFile()");
		return resultMap;
	}
	
	@RequestMapping(value = "/uploadOwnPanImage", method = RequestMethod.POST)
	public @ResponseBody Object uploadOwnPanImage(@RequestParam("file") MultipartFile file,@RequestParam("id") Integer ownId){	
		logger.info("Enter into /uploadOwnChqImage() : FileName = "+file.getName());
		System.out.println("Enter into /upldExcelFile() : File = "+file.getName());
		System.out.println("Enter into /upldExcelFile() : File = "+file.getOriginalFilename());
		
		Map<String, String> resultMap = new HashMap<String, String>();
		try{
			byte [] fileInBytes = file.getBytes();
			resultMap = ownerDAO.uploadPanImg(fileInBytes, ownId);
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put("result", "error");
			resultMap.put("msg", "There is some problem to upload file");
			resultMap.put("exp", "Exception="+e);
		}
		logger.info("Exit from /upldExcelFile()");
		return resultMap;
	}
	
	@RequestMapping(value = "/uploadOwnDecImage", method = RequestMethod.POST)
	public @ResponseBody Object uploadOwnDecImage(@RequestParam("file") MultipartFile file,@RequestParam("id") Integer ownId){	
		logger.info("Enter into /uploadOwnDecImage() : FileName = "+file.getName());
		System.out.println("Enter into /upldExcelFile() : File = "+file.getName());
		System.out.println("Enter into /upldExcelFile() : File = "+file.getOriginalFilename());
		
		Map<String, String> resultMap = new HashMap<String, String>();
		try{
			byte [] fileInBytes = file.getBytes();
			resultMap = ownerDAO.uploadDecImg(fileInBytes, ownId);
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put("result", "error");
			resultMap.put("msg", "There is some problem to upload file");
			resultMap.put("exp", "Exception="+e);
		}
		logger.info("Exit from /upldExcelFile()");
		return resultMap;
	}
	
	
}

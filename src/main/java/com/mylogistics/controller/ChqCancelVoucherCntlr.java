package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.ChqCancelVoucherDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.services.CPVoucherService;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;


@Controller
public class ChqCancelVoucherCntlr {

	@Autowired
	HttpSession httpSession;
	
	@Autowired
	BranchDAO branchDAO;
	
	@Autowired
	CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	BankMstrDAO bankMstrDAO;
	
	@Autowired
	ChqCancelVoucherDAO chqCancelVoucherDAO;
	
	
	@RequestMapping(value = "/getVDetFrCCV" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrCCV() {  
		System.out.println("Enter into getVDetFrCCV---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				List<Map<String, Object>> bankACNList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for (BankMstr bankMstr : bnkList) {
						Map<String, Object> bankMstrMap = new HashMap<>();
						bankMstrMap.put(BankMstrCNTS.BNK_FA_CODE, bankMstr.getBnkFaCode());
						bankMstrMap.put(BankMstrCNTS.BNK_NAME, bankMstr.getBnkName());
						bankMstrMap.put(BankMstrCNTS.BNK_AC_NO, bankMstr.getBnkAcNo());
						bankMstrMap.put(BankMstrCNTS.BNK_ID, bankMstr.getBnkId());
						bankACNList.add(bankMstrMap);
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankACNList.size());
				if(cashStmtStatus != null && !bankACNList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put("bankACNList", bankACNList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getChqFrCCV" , method = RequestMethod.POST)
	public @ResponseBody Object getChqFrCCV(@RequestBody Map<String, Object> bankACNMap){
		
		Map<String, Object> map = new HashMap<>();
		
		List<ChequeLeaves> chequeLeaveList = bankMstrDAO.getChqLByBnkId((int) bankACNMap.get("bnkId"));
		
		List<ChequeLeaves> usedChqList = new ArrayList<>();
		List<ChequeLeaves> unUsedChqList = new ArrayList<>();
		
		if (!chequeLeaveList.isEmpty()) {
			for (ChequeLeaves chequeLeaves : chequeLeaveList) {
				if (!chequeLeaves.isChqLUsed() && !chequeLeaves.isChqLCancel()) {
					unUsedChqList.add(chequeLeaves);
				} else if(chequeLeaves.isChqLUsed() && !chequeLeaves.isChqLCancel()) {
					usedChqList.add(chequeLeaves);
				}
			}
		}
		
		System.out.println("unUsedChqList: "+unUsedChqList.size());
		System.out.println("usedChqList: "+usedChqList.size());
		
		if (!usedChqList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("usedChqList", usedChqList);
			map.put("unUsedChqList", unUsedChqList);
		}else {
			map.put(ConstantsValues.RESULT, "No used cheque for this bank");
		}

		
		return map;
	} 
	
	@RequestMapping(value = "/submitCCVoucher" , method = RequestMethod.POST)  
	public @ResponseBody Object submitCPVoucher(@RequestBody VoucherService voucherService) {  
		System.out.println("Enter into submitCPVoucher---->");
		Map<String,Object> map = new HashMap<String,Object>();
		
		int temp = chqCancelVoucherDAO.saveChqCancelVoucher(voucherService);
		
		/*VoucherService voucherService = cpVoucherService.getVoucherService();
		List<Map<String,Object>> faList = cpVoucherService.getFaList();
		System.out.println("size of faList = "+faList.size());
		
		for(int i=0;i<faList.size();i++){
			Map<String,Object> clientMap = faList.get(i);
			System.out.println(i+" record faCode = "+clientMap.get("faCode") + "class = "+clientMap.get("faCode").getClass());
			System.out.println(i+" record amt = "+clientMap.get("amt") + "class = "+clientMap.get("amt").getClass());
		}
		
		if(voucherService.getVoucherType().equalsIgnoreCase("Cheque Payment")){
			 map = chqPayVoucherDAO.saveChqPayVoucher(voucherService , faList);
			 int vhNo = (int) map.get("vhNo");
			if(vhNo>0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}*/
		
		if (temp > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}	
	
	//TODO Back Data
	@RequestMapping(value = "/getVDetFrCCVBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrCCVBack() {  
		System.out.println("Enter into getVDetFrCCVBack---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				List<Map<String, Object>> bankACNList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for (BankMstr bankMstr : bnkList) {
						Map<String, Object> bankMstrMap = new HashMap<>();
						bankMstrMap.put(BankMstrCNTS.BNK_FA_CODE, bankMstr.getBnkFaCode());
						bankMstrMap.put(BankMstrCNTS.BNK_NAME, bankMstr.getBnkName());
						bankMstrMap.put(BankMstrCNTS.BNK_AC_NO, bankMstr.getBnkAcNo());
						bankMstrMap.put(BankMstrCNTS.BNK_ID, bankMstr.getBnkId());
						bankACNList.add(bankMstrMap);
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankACNList.size());
				if(cashStmtStatus != null && !bankACNList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put("bankACNList", bankACNList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
}

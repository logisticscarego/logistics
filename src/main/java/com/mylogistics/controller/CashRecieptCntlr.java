package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;

@Controller
public class CashRecieptCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;

	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@RequestMapping(value = "/getCashRecDetail" , method = RequestMethod.POST)  
	public @ResponseBody Object getCashRecDetail() {  
		System.out.println("Enter into getCashRecDetail---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				if(cashStmtStatus != null){
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}
	
	
	
	@RequestMapping(value = "/getFaCodeFrCR" , method = RequestMethod.POST)  
	public @ResponseBody Object getFaCodeFrCR() {  
		System.out.println("Enter into getFaCodeFrCR---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user!=null){
			List<FAMaster> faMList = new ArrayList<FAMaster>();
			
			List<FAMaster> fmEmpList = faMasterDAO.getMultiFaM("employee");
			if(!fmEmpList.isEmpty()){
				for(int i=0;i<fmEmpList.size();i++){
					faMList.add(fmEmpList.get(i));
				}
			}
			
			List<FAMaster> fmOwnList = faMasterDAO.getMultiFaM("owner");
			if(!fmOwnList.isEmpty()){
				for(int i=0;i<fmOwnList.size();i++){
					faMList.add(fmOwnList.get(i));
				}
			}
			
			List<FAMaster> fmBrkList = faMasterDAO.getMultiFaM("broker");
			if(!fmBrkList.isEmpty()){
				for(int i=0;i<fmBrkList.size();i++){
					faMList.add(fmBrkList.get(i));
				}
			}
			
			List<FAMaster> fmMisList = faMasterDAO.getMultiFaM("miscfa");
			if(!fmMisList.isEmpty()){
				for(int i=0;i<fmMisList.size();i++){
					faMList.add(fmMisList.get(i));
				}
			}
			
			List<FAMaster> fmPALList = faMasterDAO.getMultiFaM("profitnloss");
			if(!fmPALList.isEmpty()){
				for(int i=0;i<fmPALList.size();i++){
					faMList.add(fmPALList.get(i));
				}
			}
			
			List<FAMaster> fmAALList = faMasterDAO.getMultiFaM("assestsnliab");
			if(!fmAALList.isEmpty()){
				for(int i=0;i<fmAALList.size();i++){
					faMList.add(fmAALList.get(i));
				}
			}
			
			System.out.println("Fa Master List Size : "+faMList.size());
			
			if(!faMList.isEmpty()){
				map.put("faMList",faMList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/submitCashR" , method = RequestMethod.POST)  
	public @ResponseBody Object submitCashR(@RequestBody VoucherService voucherService) {  
		System.out.println("Enter into submitCashR---->");
		Map<String, Object> map = new HashMap<>();	
		if(voucherService.getVoucherType().equalsIgnoreCase("CashReciept")){
			 map = saveCashReciept(voucherService);
			 int vhNo = (int) map.get("vhNo");
			if(vhNo>0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}
		return map;
	}
	
	
	public Map<String,Object> saveCashReciept(VoucherService voucherService){
		System.out.println("Enter into saveCashReciept----->>>");
		User currentUser =	(User)httpSession.getAttribute("currentUser");
		Map<String,Object> map = new HashMap<String,Object>();		
		CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
		String faCode = voucherService.getFaCode();
		String payMode = "C";
		if(cashStmtStatus.getCssId() > 0){
			CashStmt cashStmt = new CashStmt();
			cashStmt.setbCode(currentUser.getUserBranchCode());
			cashStmt.setUserCode(currentUser.getUserCode());
			cashStmt.setCsDescription(voucherService.getDesc());
			cashStmt.setCsDrCr('C');
			cashStmt.setCsAmt(voucherService.getAmount());
			cashStmt.setCsType(voucherService.getVoucherType());
			cashStmt.setCsMrNo(voucherService.getMrNo());
			cashStmt.setCsTvNo(faCode);
			cashStmt.setCsVouchType("cash");
			cashStmt.setCsFaCode(faCode);
			cashStmt.setPayMode(payMode);
			
			java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
			cashStmt.setCsDt(sqlDate);
			map = cashStmtStatusDAO.updateCSSByCS(cashStmtStatus.getCssId() , cashStmt);
			return map;
		}else{
			return map;
		}
	}	
	
	@RequestMapping(value = "/getCashRecDetailBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getCashRecDetailBack() {  
		System.out.println("Enter into getCashRecDetailBack---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(branchCode);
				if(cashStmtStatus != null){
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}
	

	
	
}

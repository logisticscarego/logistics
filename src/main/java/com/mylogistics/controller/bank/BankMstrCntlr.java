package com.mylogistics.controller.bank;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.model.Address;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.BankName;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.bank.BankBranchService;
import com.mylogistics.services.bank.BankMstrService;

@Controller
public class BankMstrCntlr {

	@Autowired
	private BankMstrDAO bankMstrDAO;

	@Autowired
	private HttpSession httpSession;

	@Autowired
	private EmployeeDAO employeeDAO;

	@RequestMapping(value = "/getBankNameNEmp", method = RequestMethod.POST)
	public @ResponseBody
	Object getBankNameNEmp() {
		System.out.println("Enter into getBankNameNEmp---->");
		Map<String, Object> map = new HashMap<>();

		List<BankName> bankNames = bankMstrDAO.getBankNameList();
		List<Employee> employees = employeeDAO.getAllActiveEmployees();

		System.out.println("no of bank in BankName: " + bankNames.isEmpty());
		System.out.println("no of Employee in employees: "
				+ employees.isEmpty());

		if (!bankNames.isEmpty() && !employees.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("bankNames", bankNames);
			map.put("employees", employees);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}

	@RequestMapping(value = "/submitBankMster", method = RequestMethod.POST)
	public @ResponseBody
	Object submitBankMster(@RequestBody BankMstrService bankMstrService) {

		System.out.println("Enter into submitBankMster()");
		Map<String, Object> map = new HashMap<>();

		Address address = bankMstrService.getBnkAdd();
		BankMstr bankMstr = bankMstrService.getBankMstr();

		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		bankMstr.setUserCode(currentUser.getUserCode());
		bankMstr.setbCode(currentUser.getUserBranchCode());
		
		address.setUserCode(currentUser.getUserCode());
		address.setbCode(currentUser.getUserBranchCode());
		address.setAddType(BankMstrCNTS.ADDRESS_TYPE);
		
		bankMstr.setAddress(address);

		String bankFaCode = bankMstrDAO.saveBankMstr(bankMstr);
		if (bankFaCode != null && !bankFaCode.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("bankFaCode", bankFaCode);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		/*
		 * ArrayList<Map<String, Object>> singleSList =
		 * bankMstr.getBnkSingleSignatoryList(); ArrayList<Map<String, Object>>
		 * jointSList = bankMstr.getBnkJointSignatoryList();
		 * 
		 * for (Map<String, Object> singleSListMap : singleSList) {
		 * System.out.println
		 * ("SingleSignatory: "+singleSListMap.get("empName")); }
		 * 
		 * for (Map<String, Object> jointSListMap : jointSList) {
		 * System.out.println("JointSignatory: "+jointSListMap.get("empName"));
		 * }
		 * 
		 * System.out.println("Address: "+address.getAddCity());
		 * System.out.println("BankMster: "+bankMstr.getBnkAcNo());
		 */

		return map;
	}
	
	@RequestMapping(value = "/getBankMstr", method = RequestMethod.POST)
	public @ResponseBody
	Object getBankMstr() {
		System.out.println("Enter into getBankMstr---->");
		Map<String, Object> map = new HashMap<>();

		List<BankMstr> bankMstrList = bankMstrDAO.getUnAssignBank();
		
		if (!bankMstrList.isEmpty()) {
			/*for (BankMstr bankMstr : bankMstrList) {
				System.out.println("bankName: "+bankMstr.getBnkName());
			}*/
			
			map.put("bankMstrList", bankMstrList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/assignBankToBranch", method = RequestMethod.POST)
	public @ResponseBody
	Object assignBankToBranch(@RequestBody BankBranchService bankBranchService) {
		System.out.println("Enter into assignBankToBranch---->");
		Map<String, Object> map = new HashMap<>();

		BankMstr bankMstr = bankBranchService.getBankMstr();
		Branch branch = bankBranchService.getBranch();
		
		System.out.println("bank Name: "+bankMstr.getBnkName());
		System.out.println("bank FaCode: "+bankMstr.getBnkFaCode());
		
		System.out.println("branch name: "+branch.getBranchName());
		System.out.println("barnch FaCode: "+branch.getBranchFaCode());
		
		int temp = bankMstrDAO.saveBankMstr(bankMstr, branch);
		if (temp > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/viewBankMstr", method = RequestMethod.POST)
	public @ResponseBody
	Object viewBankMstr() {
		System.out.println("Enter into viewBankMstr---->");
		Map<String, Object> map = new HashMap<>();

		List<BankMstr> bankMstrList = bankMstrDAO.getBankMstr();
		
		if (!bankMstrList.isEmpty()) {
			map.put("bankMstrList", bankMstrList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/dissBankFromBranch", method = RequestMethod.POST)
	public @ResponseBody
	Object dissBankFromBranch(@RequestBody BankBranchService bankBranchService) {
		System.out.println("Enter into dissBankFromBranch---->");
		Map<String, Object> map = new HashMap<>();

		BankMstr bankMstr = bankBranchService.getBankMstr();
		Branch branch = bankBranchService.getBranch();
		
		System.out.println("bank Name: "+bankMstr.getBnkName());
		System.out.println("bank FaCode: "+bankMstr.getBnkFaCode());
		
		System.out.println("branch name: "+branch.getBranchName());
		System.out.println("barnch FaCode: "+branch.getBranchFaCode());
		
		int temp = bankMstrDAO.dissBMFromBranch(bankMstr, branch);
		if (temp > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	//get the list of assigned bank
	@RequestMapping(value = "/getAssignedBank", method = RequestMethod.POST)
	public @ResponseBody Object getAssignedBank() {
		System.out.println("Enter into getAssignedBank---->");
		Map<String, Object> map = new HashMap<>();

		List<BankMstr> assignedBankList = bankMstrDAO.getAssignedBank();
		
		if (!assignedBankList.isEmpty()) {
			for (BankMstr bankMstr : assignedBankList) {
				System.out.println("bankName: "+bankMstr.getBnkName());
			}
			
			map.put("assignedBankList", assignedBankList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/dissAssBank", method = RequestMethod.POST)
	public @ResponseBody Object dissAssBank(@RequestBody BankMstr bankMstr) {
		System.out.println("Enter into dissBankFromBranch---->");
		Map<String, Object> map = new HashMap<>();

		System.out.println("bank Name: "+bankMstr.getBnkName());
		System.out.println("bank FaCode: "+bankMstr.getBnkFaCode());
		
		int temp = bankMstrDAO.dissBankMstr(bankMstr);
		if (temp > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value="/getBankNCAI" , method = RequestMethod.POST)
	public @ResponseBody Object getBankNCAI(@RequestBody String branchId){
		
		System.out.println("BranchId: "+branchId);
		
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> bankNCAIList = bankMstrDAO.getBankNCAIList(branchId);
		
		if (!bankNCAIList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("bankNCAIList", bankNCAIList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}

}

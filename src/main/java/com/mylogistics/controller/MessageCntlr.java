package com.mylogistics.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MessageCntlr {
	
	@RequestMapping(value = "/sendMsgToAllBranches", method = RequestMethod.POST)
	public void sendMsgToAllBranchs(@RequestBody Map<String, String> initParam){
		try{
			System.err.println("InitParam = "+initParam);
			
			String msg = initParam.get("msg");
			Integer repCount = Integer.parseInt(initParam.get("repCount"));
			
			for(int i=0; i<repCount; i++){
				System.err.println("Msg = "+msg);
				
				Thread.sleep(1000);
			}
			System.out.println("End.....");
		}catch(Exception e){
			System.err.println("Exception = "+e);
		}
	}
	
}
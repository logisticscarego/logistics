package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.RentMstrDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;

@Controller
public class RentVoucherCntlr {

	@Autowired
	private HttpSession httpSession;
	@Autowired
	private BranchDAO branchDAO;
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	@Autowired
	private RentMstrDAO rentMstrDAO;
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	public static final Logger logger = Logger.getLogger(RentVoucherCntlr.class);
	
	@RequestMapping(value = "/getVDetFrRent" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrRent() {  
		logger.info("Enter into getVDetFrRent---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				logger.info("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				logger.info("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					logger.info("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getRentMstrData" , method = RequestMethod.POST)  
	public @ResponseBody Object getRentMstrData() {  
		logger.info("Enter into getRentMstrData---->");
		Map<String, Object> map = new HashMap<>();	
		List<Map<String,Object>> rentInfoList = rentMstrDAO.getRentMstrInfo();
		List<FAMaster> faMList = faMasterDAO.getAllTdsFAM();
		if(!rentInfoList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",rentInfoList);
			map.put("faMList",faMList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	@RequestMapping(value = "/submitRentVouch" , method = RequestMethod.POST)  
	public @ResponseBody Object submitRentVouch(@RequestBody VoucherService voucherService) {  
		logger.info("Enter into submitRentVouch---->");
		Map<String, Object> map = new HashMap<>();	
		int vhNo = cashStmtStatusDAO.saveRentVoucher(voucherService);
		if(vhNo > 0){
			map.put("vhNo",vhNo);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}	
		return map;
	}	
	
	@RequestMapping(value = "/submitRentVouchN" , method = RequestMethod.POST)  
	public @ResponseBody Object submitRentVouchN(@RequestBody VoucherService voucherService) {  
		logger.info("Enter into submitRentVouchN---->");
		return cashStmtStatusDAO.saveRentVoucherN(voucherService);
	}	
	
	@RequestMapping(value = "/getVDetFrRentBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrRentBack() {  
		logger.info("Enter into getVDetFrRent---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				logger.info("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				logger.info("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					logger.info("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
}
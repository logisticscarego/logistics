package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BillDAO;
import com.mylogistics.DAO.BillForwardingDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.model.Bill;
import com.mylogistics.model.BillForwarding;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

	@Controller
	public class BillFrwdCntlr {
	
	@Autowired
	private HttpSession httpSession;

	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private BillDAO billDAO;
	
	@Autowired
	private BillForwardingDAO billForwardingDAO;
	
	@RequestMapping(value = "/getCustFrBillF", method = RequestMethod.POST)
	public @ResponseBody Object getCustFrBillF() {
		System.out.println("enter into getCustFrBillF function");
		Map<String,Object> map = new HashMap<>();
		List<Map<String,Object>> custList = new ArrayList<>();
		custList = customerDAO.getCustNCI();
		if(!custList.isEmpty()){
			map.put("list",custList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getBillFrBF", method = RequestMethod.POST)
	public @ResponseBody Object getBillFrBF(@RequestBody Map<String,Object> clientMap) {
		System.out.println("enter into getBillFrBF function");
		Map<String,Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		System.out.println("userBranchCode="+currentUser.getUserBranchCode());
		
		int custId = Integer.parseInt(String.valueOf(clientMap.get("custId")));
		System.out.println("from client custId = "+custId);
		if(custId > 0){
			
			BillForwarding bf=billForwardingDAO.getPreviousBFbyCustBr(custId, currentUser.getUserBranchCode());
			
			if(bf!=null) {
				if(bf.getBfRecDt()==null) {
					map.put(ConstantsValues.RESULT,"BFError");
					map.put("msg", "Please Enter BillForwarding submission date of "+bf.getBfNo());
					return map;
				}
			}
			
			
			List<Bill> billList = new ArrayList<>();
			billList = billDAO.getCustBill(custId);
			System.out.println("size of billList = "+billList.size());
			if(!billList.isEmpty()){
				map.put("list",billList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put("msg","No Bill avaliable for bill forwarding");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put("msg","invalid customer");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/saveBillFrwd", method = RequestMethod.POST)
	public @ResponseBody Object saveBillFrwd(@RequestBody BillForwarding billForwarding) {
		System.out.println("enter into saveBillFrwd function");
		Map<String,Object> map = new HashMap<>();
		String bfNo = billForwardingDAO.saveBF(billForwarding);
		if(bfNo != null){
			map.put("bfNo",bfNo);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
}

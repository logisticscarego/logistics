package com.mylogistics.controller;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.UserDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.DownloadService;


@Controller
public class SuperAdminCntlr {

	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
	private DownloadService downloadService;
	//private DownloadService downloadService = new DownloadService();
	
	@Validated
	@RequestMapping(value = "/addNewUser" , method = RequestMethod.POST)
	public @ResponseBody Object addNewUser(@RequestBody @Valid User user , BindingResult result)throws Exception{
		System.out.println("enter into addNewUser function");
		Map<String,String> map = new HashMap<String,String>();
		String userRole = "";
		if(result.hasErrors()) {
			  System.out.println("exit from saveUser");
			  map.put("result", "Entered Data not valid user name");
	            return map;
	    }else{
			if(user.getUserRole() == 11){
				userRole = "Super Admin";
				user.setUserCode(ConstantsValues.SUPER_ADMIN_U_CODE);
				user.setUserBranchCode(ConstantsValues.SUPER_ADMIN_B_CODE);
			}else if(user.getUserRole() == 22){
				userRole = "Admin";
			}else if(user.getUserRole() == 33){
				userRole = "Operator";
			}else{
				System.out.println("---------fake user-------------");
				map.put("result","failure");
				return map;
			}
			
			String password = user.getPassword();
			MessageDigest md = MessageDigest.getInstance("MD5");
		    md.update(password.getBytes());
		    byte[] digest = md.digest();
		    StringBuffer sb = new StringBuffer();
		    for (byte b : digest) {
	            sb.append(String.format("%02x", b & 0xff));
	        }
		    System.out.println("original:" + password);
	        System.out.println("digested(hex):" + sb.toString());
	        
	        user.setPassword(sb.toString());
			String finalResult = userDAO.addNewUser(user);
			
			if(finalResult.equals("success")){
				map.put("result","successfully add "+user.getUserName()+" as a "+userRole);
			}else if(finalResult.equals("failure")){
				map.put("result",finalResult);
			}else if(finalResult.equals("USERNAME ALREADY EXIST")){
				map.put("result",finalResult);
			}
			return map;
	    }
	}
	
	@RequestMapping(value = "/fetchUserList" , method = RequestMethod.GET)
	public @ResponseBody Object fetchUserList(){
		System.out.println("enter into fetchUserList function");
		Map<String,Object> map = new HashMap<String,Object>();
		List<User> userList = userDAO.getAllUser();
		if(userList.isEmpty()){
			map.put("result","failure");
		}else{
			map.put("result",userList);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/updateUserStatus" , method = RequestMethod.POST)
	public @ResponseBody Object updateUserStatus(@RequestBody List<User> userList){
		System.out.println("enter into updateUserStatus function");
		Map<String,String> map = new HashMap<String,String>();
		if(!userList.isEmpty()){
			userDAO.updateUserList(userList);
			map.put("result","success");
			return map;
		}else{
			map.put("result","failure");
			return map;
		}
		
	}
	
	@RequestMapping(value = "/fetchAllBranchAndEmployee" , method = RequestMethod.GET)
	public @ResponseBody Object fetchAllBranchAndEmployee(){
		System.out.println("enter into fetchAllBranch function");
		Map<String,Object> map = new HashMap<String,Object>();
		List<Branch> branchList = branchDAO.getAllActiveBranches();
		List<Employee> employeeList = employeeDAO.getAllEmployee();
		if(!branchList.isEmpty()){
			map.put("message","success");
			map.put("bList",branchList);
			map.put("eList",employeeList);
			return map;
		}else{
			map.put("message","failure");
			return map;
		}
	}
	
	
	@RequestMapping(value = "/downloadExl" , method = RequestMethod.GET)
	public void downloadExl(HttpServletResponse response)throws Exception{
		System.out.println("enter into downloadExl function");
		downloadService.downloadXLS(response);
	}
	
}

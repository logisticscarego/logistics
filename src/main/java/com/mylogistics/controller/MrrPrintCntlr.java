package com.mylogistics.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.MoneyReceiptDAO;
import com.mylogistics.services.ConstantsValues;

@Controller
public class MrrPrintCntlr {

	@Autowired
	private HttpSession httpSession;  
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private MoneyReceiptDAO moneyReceiptDAO;
	
	@RequestMapping(value = "/getMrrBranch", method = RequestMethod.POST)
	public @ResponseBody Object getMrrBranch() {
		System.out.println("enter into getMrrBranch function");
		Map<String,Object> map = new HashMap<>();
		List<Map<String,Object>> brhList = branchDAO.getNameAndCode();
		if(!brhList.isEmpty()){
			map.put("list",brhList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/mrrPrintSubmit", method = RequestMethod.POST)
	public @ResponseBody Object mrrPrintSubmit(@RequestBody Map<String,Object> clientMap) {
		System.out.println("enter into mrrPrintSubmit function");
		Map<String,Object> map = new HashMap<>();
		int brhId = Integer.parseInt(String.valueOf(clientMap.get("brhId")));
		Date date = Date.valueOf((String) clientMap.get("mrDt"));          
		
		if(brhId > 0){
				List<Map<String,Object>> mrList = new ArrayList<>();
				mrList = moneyReceiptDAO.getMrrFrPrint(brhId,date);
				if(!mrList.isEmpty()){
					System.out.println("size of blList = "+mrList.size());
					map.put("list",mrList);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put("msg","Branch Does not have any M.R.");
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
		}else{
			map.put("msg","Invalid Branch");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
}

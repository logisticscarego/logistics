package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.ContToStnDAO;
import com.mylogistics.DAO.ContractRightsDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.CustomerRepresentativeDAO;
import com.mylogistics.DAO.DailyContractDAO;
import com.mylogistics.DAO.DlyContAuthDAO;
import com.mylogistics.DAO.FAParticularDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.PenaltyBonusDetentionDAO;
import com.mylogistics.DAO.ProductTypeDAO;
import com.mylogistics.DAO.RateByKmDAO;
import com.mylogistics.DAO.RegularContractDAO;
import com.mylogistics.DAO.StateDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.DAO.TermNConditionDAO;
import com.mylogistics.DAO.VehicleTypeDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.ContractRights;
import com.mylogistics.model.Customer;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.DlyContAuth;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.PenaltyBonusDetention;
import com.mylogistics.model.ProductType;
import com.mylogistics.model.RateByKm;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.State;
import com.mylogistics.model.Station;
import com.mylogistics.model.TermNCondition;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleType;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.ContToStnService;
import com.mylogistics.services.ContToStnServiceImpl;
import com.mylogistics.services.CustomerNCustomerRep;
import com.mylogistics.services.PBDService;
import com.mylogistics.services.PBDServiceImpl;
import com.mylogistics.services.RBKMService;
import com.mylogistics.services.RBKMServiceImpl;

@Controller
public class DailyContractCntlr {

	@Autowired
	private DailyContractDAO dailyContractDAO;

	@Autowired
	private CustomerDAO customerDAO;

	@Autowired
	private BranchDAO branchDAO;

	@Autowired
	private StationDAO stationDAO;

	@Autowired
	private CustomerRepresentativeDAO customerRepresentativeDAO;

	@Autowired
	private VehicleTypeDAO vehicleTypeDAO;

	@Autowired
	private ProductTypeDAO productTypeDAO;

	@Autowired
	private RateByKmDAO rateByKmDAO;

	@Autowired
	private TermNConditionDAO termNConditionDAO;

	@Autowired
	private StateDAO stateDAO;

	@Autowired
	private PenaltyBonusDetentionDAO penaltyBonusDetentionDAO;

	@Autowired
	private RegularContractDAO regularContractDAO;
	
	@Autowired
	private ContToStnDAO contToStnDAO;
	
	@Autowired
	private ContractRightsDAO contractRightsDAO;
	
	@Autowired
	private FAParticularDAO faParticularDAO;
	
	@Autowired
    private HttpSession httpSession;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@Autowired
	private DlyContAuthDAO dlyContAuthDAO;

	private RBKMService rbkmService = new RBKMServiceImpl();

	private PBDService pbdService = new PBDServiceImpl();
	
	private ContToStnService contToStnService = new ContToStnServiceImpl();
	
	String dlyContCode = null;
	
	String metric=null;
	
	//Map<String, String> tempMap = new HashMap<String, String>();
	

		@RequestMapping(value = "/saveDailyContract", method = RequestMethod.POST)
		public @ResponseBody Object saveDailyContract(@RequestBody DailyContract dlyCnt) {
		System.out.println("Enter into saveDailyContract function------");
			List<RateByKm> rbkmList = rbkmService.getAllRBKM();
			List<PenaltyBonusDetention> pbdList = pbdService.getAllPBD();
			List<ContToStn> cList = contToStnService.getAllCTS();
			Map<String, String> map = new HashMap<String, String>();
			User currentUser = (User)httpSession.getAttribute("currentUser");
			//DecimalFormat df = new DecimalFormat("#.##");
			String last = null;
			String faCode = null;
			long end = 0;

			long totalRows = dailyContractDAO.totalCount();
			
			if (!(totalRows == -1)) {
				if (totalRows == 0) {
					last = "0000001";
					dlyContCode="dly"+"1";
				}else {
					long id = (long)dailyContractDAO.getLastDlycontractId()+1;
					end = 10000000 + id;
					last = String.valueOf(end).substring(1, 8);
					dlyContCode="dly"+id;
				}
				
				//dlyContCode = CodePatternService.dlyContCodeGen(dlyCnt,last);
				System.out.println("After generating the code"+dlyContCode);
				
				List<FAParticular> faParticulars = faParticularDAO.getFAParticular(ConstantsValues.FAP_DLY_CONT);
				FAParticular fAParticular = new FAParticular();
				fAParticular=faParticulars.get(0);
				int fapId=fAParticular.getFaPerId();
				String fapIdStr = String.valueOf(fapId); 
				System.out.println("the fap id is--------------------"+fapId+""+fapIdStr);
				if(fapId<10){
					fapIdStr="0"+fapIdStr;
					System.out.println("After adding zeroes--"+fapIdStr);
				}
				dlyCnt.setfAParticular(fAParticular);
				dlyCnt.setBranchCode(currentUser.getUserBranchCode());
				dlyCnt.setView(false);
				dlyCnt.setDlyContCode(dlyContCode);
				dlyCnt.setDlyContIsVerify("no");
				dlyCnt.setUserCode(currentUser.getUserCode());
				dlyCnt.setbCode(currentUser.getUserBranchCode());
				
				int temp = dailyContractDAO.saveDailyContract(dlyCnt);
				if(temp > 0){
					int result = dlyContAuthDAO.againAlwFrCont(dlyCnt.getDlyContBLPMCode());
					if(result > 0){
						System.out.println("again allow this customer for daily contract");
					}
				}
				System.out.println("Contract saved with id ----"+temp);
				temp=temp+100000;
				String contId = String.valueOf(temp).substring(1,6);
				faCode=fapIdStr+contId;
				System.out.println("faCode code is------------------"+faCode);
				dlyCnt.setDlyContFaCode(faCode);
				int updateCont = dailyContractDAO.updateDailyContract(dlyCnt);
				System.out.println("Cont updated-------"+updateCont);
			
				List<Customer> customerList= customerDAO.getCustomer(dlyCnt.getDlyContBLPMCode());
				String custName=customerList.get(0).getCustName();
				/*FAMaster faMaster = new FAMaster();
				faMaster.setFaMfaCode(faCode);
				faMaster.setFaMfaType("dlycontract");
				faMaster.setFaMfaName(custName);
				faMaster.setbCode(currentUser.getUserBranchCode());
				faMaster.setUserCode(currentUser.getUserCode());
				faMasterDAO.saveFaMaster(faMaster);*/
				if (temp >= 0) {
					map.put("faCode",faCode);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else {
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
				}else {
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
				
			if(!rbkmList.isEmpty()){
			for(int i = 0; i < rbkmList.size(); i++) {
				double rbkmRate=0;
				if(metric.equals("Ton")){
					rbkmRate = rbkmList.get(i).getRbkmRate();
					rbkmRate = (rbkmRate/ConstantsValues.kgInTon);
					//rbkmRate=Double.parseDouble((df.format(rbkmRate)));
					  //rbkmRate = (double)Math.round(rbkmRate*100.0)/100.0;
					rbkmList.get(i).setRbkmRate(rbkmRate);
				}
				System.out.println("Rbkm rate is-----"+rbkmRate);
				rbkmList.get(i).setRbkmContCode(dlyContCode);
				rbkmList.get(i).setbCode(currentUser.getUserBranchCode());
				rbkmList.get(i).setUserCode(currentUser.getUserCode());
			int temp =rateByKmDAO.saveRBKM(rbkmList.get(i));
			if(temp>0){
				map.put(ConstantsValues.RESULT+"Rbkm",ConstantsValues.SUCCESS+"Rbkm");
			}else{
				map.put(ConstantsValues.RESULT+"Rbkm",ConstantsValues.ERROR+"Rbkm");
			}
			}
			}
			
			if(!pbdList.isEmpty()){
			for (int i = 0; i < pbdList.size(); i++) {
				pbdList.get(i).setPbdContCode(dlyContCode);
				pbdList.get(i).setbCode(currentUser.getUserBranchCode());
				pbdList.get(i).setUserCode(currentUser.getUserCode());
				int temp = penaltyBonusDetentionDAO.savePBD(pbdList.get(i));
				if(temp>0){
					map.put(ConstantsValues.RESULT+"Pbd",ConstantsValues.SUCCESS+"Pbd");
				}else{
					map.put(ConstantsValues.RESULT+"Pbd",ConstantsValues.ERROR+"Pbd");
				}
			}
		}
			
			if(!cList.isEmpty()){
			for (int i = 0; i < cList.size(); i++) {
				double ctsRate=0;
				double ctsFromWt=0;
				double ctsToWt=0;
				if(metric.equals("Ton") && (dlyCnt.getDlyContProportionate().equals("P"))){
					ctsRate = cList.get(i).getCtsRate();
					ctsRate = ctsRate/ConstantsValues.kgInTon;
					//ctsRate = (double)Math.round(ctsRate*100.0)/100.0;
					cList.get(i).setCtsRate(ctsRate);
					
					ctsFromWt = cList.get(i).getCtsFromWt();
					ctsFromWt = ctsFromWt*ConstantsValues.kgInTon;
					//ctsFromWt = (double)Math.round(ctsFromWt*100.0)/100.0;
					cList.get(i).setCtsFromWt(ctsFromWt);
					
					ctsToWt = cList.get(i).getCtsToWt();
					ctsToWt = ctsToWt*ConstantsValues.kgInTon;
					//ctsToWt = (double)Math.round(ctsToWt*100.0)/100.0;
					cList.get(i).setCtsToWt(ctsToWt);
					
				}else if(metric.equals("Ton") && (dlyCnt.getDlyContProportionate().equals("F"))){
					ctsRate = cList.get(i).getCtsRate();
					cList.get(i).setCtsRate(ctsRate);
					
					ctsFromWt = cList.get(i).getCtsFromWt();
					ctsFromWt = ctsFromWt*ConstantsValues.kgInTon;
					//ctsFromWt = (double)Math.round(ctsFromWt*100.0)/100.0;
					cList.get(i).setCtsFromWt(ctsFromWt);
					
					ctsToWt = cList.get(i).getCtsToWt();
					ctsToWt = ctsToWt*ConstantsValues.kgInTon;
					//ctsToWt = (double)Math.round(ctsToWt*100.0)/100.0;
					cList.get(i).setCtsToWt(ctsToWt);
				}
				System.out.println("proportionate is ----"+dlyCnt.getDlyContProportionate());
				System.out.println("cts rate is ----"+ctsRate);
				System.out.println("ctsFromWt is ----"+ctsFromWt);
				System.out.println("ctsToWt is ----"+ctsToWt);
				cList.get(i).setCtsContCode(dlyContCode);
				cList.get(i).setbCode(currentUser.getUserBranchCode());
				cList.get(i).setUserCode(currentUser.getUserCode());
				cList.get(i).setCtsFrDt(dlyCnt.getDlyContStartDt());
				cList.get(i).setCtsToDt(dlyCnt.getDlyContEndDt());
				int temp=contToStnDAO.saveContToStn(cList.get(i));
				if(temp>0){
					map.put(ConstantsValues.RESULT+"Cts",ConstantsValues.SUCCESS+"Cts");
				}else{
					map.put(ConstantsValues.RESULT+"Cts",ConstantsValues.ERROR+"Cts");
				}
			}
			}
			
				ContractRights contractRights = new ContractRights();
				contractRights.setbCode(currentUser.getUserBranchCode());
				contractRights.setUserCode(currentUser.getUserCode());
				contractRights.setContCode(dlyContCode);
				contractRights.setCrContBillBasis("chargeWt");
				int saveContRghts=contractRightsDAO.saveContractRights(contractRights);
				System.out.println("After saving contract rights---------"+saveContRghts);
				
			rbkmService.deleteAllRBKM();
			pbdService.deleteAllPBD();
			
		return map;
	}

		@RequestMapping(value = "/saveTnc", method = RequestMethod.POST)
		public @ResponseBody Object saveTnc(@RequestBody TermNCondition termNCondition) {

			System.out.println("Controller called of saveTnc");
			Map<String, String> map = new HashMap<String, String>();
			
			User currentUser = (User)httpSession.getAttribute("currentUser");
			System.out.println("The code for tnc is----"+dlyContCode);
			termNCondition.setTncContCode(dlyContCode);
			termNCondition.setUserCode(currentUser.getUserCode());
			termNCondition.setbCode(currentUser.getUserBranchCode());
			int temp = termNConditionDAO.saveTnc(termNCondition);

			if (temp >= 0) {
				map.put(ConstantsValues.RESULT+"Tnc", ConstantsValues.SUCCESS+"Tnc");
			} else {
				map.put(ConstantsValues.RESULT+"Tnc", ConstantsValues.ERROR+"Tnc");
			}
		return map;
	}

		@RequestMapping(value = "/getCustomerData", method = RequestMethod.POST)
		public @ResponseBody Object getCustomerData() {
		
			Map<String, Object> map = new HashMap<String, Object>();
			
			List<String> custList = dlyContAuthDAO.getDCACustCode();
			List<Customer> customerList = new ArrayList<Customer>();
			if(!custList.isEmpty()){
				for(int i=0;i<custList.size();i++){
					List<Customer> cList = customerDAO.getCustomer(custList.get(i));
					if(!cList.isEmpty()){
						customerList.add(cList.get(0));
					}
				}
				map.put("list", customerList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			
			//List<Customer> customerList=customerDAO.getCustomerListForDailyContract();
			
			/*if(!customerList.isEmpty()){
				map.put("list", customerList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}*/
			return map;
		}

		@RequestMapping(value = "/getBranchData", method = RequestMethod.POST)
		public @ResponseBody Object getBranchData() {

			List<Branch> branchList = new ArrayList<Branch>();
			Map<String, Object> map = new HashMap<String, Object>();
			branchList = branchDAO.getAllActiveBranches();	
			if(!branchList.isEmpty()){
				map.put("list", branchList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;
		}

		@RequestMapping(value = "/getStationData", method = RequestMethod.POST)
		public @ResponseBody Object getStationData() {

			Map<String, Object> map = new HashMap<String, Object>();
			List<Station> stationList = new ArrayList<Station>();
			 stationList = stationDAO.getStationData();

			 if(!stationList.isEmpty()){
					map.put("list", stationList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				} else {
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
		return map;
	}

		@RequestMapping(value = "/getStateData", method = RequestMethod.POST)
		public @ResponseBody Object getStateData() {
			
			List<State> listState = stateDAO.getStateData();
			Map<String, Object> map = new HashMap<String, Object>();
			if(!listState.isEmpty()){
				map.put("list", listState);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;
		}

		@RequestMapping(value = "/getCustomerRepresentativeData", method = RequestMethod.POST)
		public @ResponseBody Object getCustomerRepresentativeData(@RequestBody String custCode) {
			System.out.println("customer code is"+custCode);
			Map<String, Object> map = new HashMap<String, Object>();
			List<CustomerNCustomerRep> list = new ArrayList<CustomerNCustomerRep>();
			List<Object[]> list1 =customerRepresentativeDAO.getCRCodeNameDesigCustName(custCode);

			if(!list1.isEmpty()){
			for (Object[] objects : list1) {
				CustomerNCustomerRep customerNCustomerRep = new CustomerNCustomerRep();
				customerNCustomerRep.setCustRepName((String) objects[0]);
				customerNCustomerRep.setCustRepCode((String) objects[1]);
				customerNCustomerRep.setCustRepDesig((String) objects[2]);
				customerNCustomerRep.setCustName((String) objects[3]);
				list.add(customerNCustomerRep);
			}
			
			List<DlyContAuth> dcaList = dlyContAuthDAO.getDCAbyCustCode(custCode);
			if(!dcaList.isEmpty()){
				map.put("isDca", "true");
				map.put("dca", dcaList.get(0));
				
				List<String> stCode = dcaList.get(0).getDcaStatList();
				if(!stCode.isEmpty()){
					List<Station> stationList = new ArrayList<Station>();
					for(int i=0;i<stCode.size();i++){
						List<Station> stList = stationDAO.retrieveStation(stCode.get(i));
						if(!stList.isEmpty()){
							stationList.add(stList.get(0));
						}
					}
					map.put("stationList", stationList);
				}
			}else{
				
			}
			map.put("list", list);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}
			else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;
		}

		@RequestMapping(value = "/getVehicleTypeCode", method = RequestMethod.POST)
		public @ResponseBody Object getVehicleTypeCode() {
			Map<String, Object> map = new HashMap<String, Object>();
			List<VehicleType> vtList = new ArrayList<VehicleType>();
			 vtList = vehicleTypeDAO.getVehicleType();
			 if (!vtList.isEmpty()) {	 
				 map.put("list", vtList);
				 map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			 }else{
				 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			 }
			return map;
		}

		@RequestMapping(value = "/getProductName", method = RequestMethod.POST)
		public @ResponseBody Object getProductName() {
			Map<String, Object> map = new HashMap<String, Object>();
			List<String> ptList = productTypeDAO.getProductName();

			if(!ptList.isEmpty())
			{
				map.put("list", ptList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;
		}

		@RequestMapping(value = "/EditDailyContract", method = RequestMethod.POST)
		public @ResponseBody Object EditDailyContract(@RequestBody String dlyContCode) {

			DailyContract dailyContract = new DailyContract();

			Map<String, Object> map = new HashMap<String, Object>();
			List<DailyContract> list = dailyContractDAO.getDailyContract(dlyContCode);
			
			if (!list.isEmpty()) {
				dailyContract = list.get(0);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				map.put("dailyContract", dailyContract);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			
			map.put("dailyContract", dailyContract);
			return map;
		}
		

		@RequestMapping(value="/verifyDailyContract	",method = RequestMethod.POST)
		public @ResponseBody Object verifyDailyContract(){
		
			List<Map<String, Object>> mapList = new ArrayList<Map<String,Object>>();
			
			List<DailyContract> dlyList = dailyContractDAO.getDlyContractisVerifyNo();
			List<RegularContract> regList = regularContractDAO.getRegContractisVerifyNo();
			
			Map<String,Object> map = new HashMap<String,Object>();
			
			if (!dlyList.isEmpty() || !regList.isEmpty()) {
				for(int i=0;i<dlyList.size();i++){
					Map<String, Object> dlyMap = new HashMap<String, Object>();
					dlyMap.put("contCode", dlyList.get(i).getDlyContCode());
					dlyMap.put("branchCode", dlyList.get(i).getBranchCode());
					dlyMap.put("crName", dlyList.get(i).getDlyContCrName());
					mapList.add(dlyMap);	
				}
				
				for(int i=0;i<regList.size();i++){
					Map<String, Object> regMap = new HashMap<String, Object>();
					regMap.put("contCode", regList.get(i).getRegContCode());
					regMap.put("branchCode", regList.get(i).getBranchCode());
					regMap.put("crName", regList.get(i).getRegContCrName());
					mapList.add(regMap);
				}
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				map.put("list", mapList);
				
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;	
		}	
		
		@RequestMapping(value = "/saveproducttype", method = RequestMethod.POST)
		public @ResponseBody Object saveproducttype(@RequestBody ProductType productType) {

			System.out.println("Controller called of saveproducttype");

			Map<String, String> map = new HashMap<String, String>();
			User currentUser = (User)httpSession.getAttribute("currentUser");
			
			productType.setUserCode(currentUser.getUserCode());
			productType.setbCode(currentUser.getUserBranchCode());
			int temp = productTypeDAO.saveProductTypeToDB(productType);
			if (temp >= 0) {
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;
		}

		@RequestMapping(value = "/saveVehicleType", method = RequestMethod.POST)
		public @ResponseBody Object saveVehicleType(@RequestBody VehicleType vehicleType){

			Map<String, String> map = new HashMap<String, String>();
			
			User currentUser = (User)httpSession.getAttribute("currentUser");
			
			vehicleType.setUserCode(currentUser.getUserCode());
			vehicleType.setbCode(currentUser.getUserBranchCode());

			int temp = vehicleTypeDAO.saveVehicleTypeToDB(vehicleType);
			if (temp >= 0) {
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;
		}

		@RequestMapping(value = "/fetchRbkmList", method = RequestMethod.GET)
		public @ResponseBody Object fetchRbkmList() {
			System.out.println("enter into fetchRbkmList function");

			List<RateByKm> rateByKmList = rbkmService.getAllRBKM();

			Map<String, Object> map = new HashMap<String, Object>();
			if(!rateByKmList.isEmpty()){
				map.put("list", rateByKmList);	
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else{
				map.put("list", rateByKmList);	
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			
			return map;
		}

		
		//TODO
		@RequestMapping(value = "/addRbkm", method = RequestMethod.POST)
		public @ResponseBody Object addRbkm(@RequestBody RateByKm rbkm) {

			System.out.println("enter into addRbkm function: ");
			rbkmService.addRBKM(rbkm);
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			return map;
		}

		@RequestMapping(value = "/removeRbkm", method = RequestMethod.POST)
		public @ResponseBody Object removeRbkm(@RequestBody RateByKm rbkm) {

			System.out.println("enter into removeRbkm function");
			rbkmService.deleteRBKM(rbkm);

			Map<String, Object> map = new HashMap<String, Object>();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			return map;
		}

		@RequestMapping(value = "/removeAllRbkm", method = RequestMethod.POST)
		public @ResponseBody Object removeAllRbkm() {

			System.out.println("enter into removeAllRbkm function");
			rbkmService.deleteAllRBKM();
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			return map;
		}
		
		@RequestMapping(value = "/fetchPbdList", method = RequestMethod.GET)
		public @ResponseBody Object fetchpbdList() {
			System.out.println("enter into fetchPbdList function");

			List<PenaltyBonusDetention> pList = pbdService.getAllPBD();

			Map<String, Object> map = new HashMap<String, Object>();
			if(!pList.isEmpty()){
				map.put("list", pList);	
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			
			return map;
		}

		@RequestMapping(value = "/addPbd", method = RequestMethod.POST)
		public @ResponseBody Object addPbd(@RequestBody PenaltyBonusDetention penaltyBonusDetention) {

			System.out.println("enter into addPbd function");
			pbdService.addPBD(penaltyBonusDetention);

			Map<String, Object> map = new HashMap<String, Object>();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			return map;
		}

		@RequestMapping(value = "/removePbd", method = RequestMethod.POST)
		public @ResponseBody Object removePbd(@RequestBody PenaltyBonusDetention penaltyBonusDetention) {

			System.out.println("enter into removePbd function");
			pbdService.deletePBD(penaltyBonusDetention);

			Map<String, Object> map = new HashMap<String, Object>();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			return map;
		}

		@RequestMapping(value = "/removeAllPbd", method = RequestMethod.POST)
		public @ResponseBody Object removeAllPbd() {

			System.out.println("enter into removeAllPbd function");
			pbdService.deleteAllPBD();

			Map<String, Object> map = new HashMap<String, Object>();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			return map;
		}
		
		/*@RequestMapping(value = "/getDlyRegContractCodes", method = RequestMethod.POST)
		public @ResponseBody Object getDlyRegContractCodes() {
			Map<String, Object> map = new HashMap<String, Object>();
			List<String> dList = new ArrayList<String>();
			List<String> rList = new ArrayList<String>();
			
			dList = dailyContractDAO.getDailyContractCode();
			rList = regularContractDAO.getRegularContractCode();
			
			List<String> strings = new ArrayList<String>();
			
			strings.addAll(dList);
			strings.addAll(rList);
			
			
			 if (!strings.isEmpty()) {	 
				 map.put("list", strings);
				 map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			 }else{
				 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			 }
			return map;
		}*/
		
		
		@RequestMapping(value = "/fetchCTSList", method = RequestMethod.GET)
		public @ResponseBody Object fetchCTSList() {
			System.out.println("enter into fetchCTSList function");

			List<ContToStn> cList = contToStnService.getAllCTS();

			Map<String, Object> map = new HashMap<String, Object>();
			if(!cList.isEmpty()){
				map.put("list", cList);	
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else{
				map.put("list", cList);	
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			
			return map;
		}

		@RequestMapping(value = "/addCTS", method = RequestMethod.POST)
		public @ResponseBody Object addCTS(@RequestBody ContToStn contToStn) {

			System.out.println("enter into addCTS function----->");
			contToStnService.addCTS(contToStn);
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			return map;
		}

		@RequestMapping(value = "/removeCTS", method = RequestMethod.POST)
		public @ResponseBody Object removeCTS(@RequestBody ContToStn contToStn) {

			System.out.println("enter into removeCTS function");
			contToStnService.deleteCTS(contToStn);

			Map<String, Object> map = new HashMap<String, Object>();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			return map;
		}

		@RequestMapping(value = "/removeAllCTS", method = RequestMethod.POST)
		public @ResponseBody Object removeAllCTS() {

			System.out.println("enter into removeAllCTS function");
			contToStnService.deleteAllCTS();
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			return map;
		}
		
		@RequestMapping(value = "/sendMetricType", method = RequestMethod.POST)
		public @ResponseBody Object sendMetricType(@RequestBody String metricType) {

			System.out.println("enter into sendMetricType function");
			 metric = metricType;
			System.out.println("Metric type is ----"+metric);
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			return map;
		}
}
package com.mylogistics.controller.webservices;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.rowset.serial.SerialBlob;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.itextpdf.text.log.SysoCounter;
import com.mylogistics.DAO.UserDAO;
import com.mylogistics.DAO.werservice.ChallanWebServiceDao;
import com.mylogistics.model.Broker;
import com.mylogistics.model.BrokerImg;
import com.mylogistics.model.Owner;
import com.mylogistics.model.OwnerImg;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class ImageUpload {
	
	@Autowired
	private ChallanWebServiceDao challanWebServiceDao;
	@Autowired
	private UserDAO userDAO;
	public static Logger logger = Logger.getLogger(ImageUpload.class);
	
	@RequestMapping(value = "/uploadPanDecWs", method = RequestMethod.POST)
	public @ResponseBody Object uploadPanDec(@RequestParam Map<String, Object> initParam, @RequestParam("panImg") MultipartFile panImg, @RequestParam("decImg") MultipartFile decImg){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.error("Both Pan/Dec");
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));			
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));			
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");			
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");
				
				String code = String.valueOf(initParam.get("code"));
				String type = String.valueOf(initParam.get("type"));
				String ownBrkId = String.valueOf(initParam.get("ownBrkId"));
				String panName = String.valueOf(initParam.get("panName"));
				String panNo = String.valueOf(initParam.get("panNo"));
				String panDOB = String.valueOf(initParam.get("panDOB"));
				String panDate = String.valueOf(initParam.get("panDate"));
				
				resultMap = challanWebServiceDao.uploadImage(code,type, ownBrkId, panName, panNo, panDOB, panDate, panImg, decImg, user);
				
			}
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/uploadPanWs", method = RequestMethod.POST)
	public @ResponseBody Object uploadPan(@RequestParam Map<String, Object> initParam, @RequestParam("panImg") MultipartFile panImg){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.error("Only Pan");
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));			
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));			
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");			
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");
				
				String code = String.valueOf(initParam.get("code"));
				String type = String.valueOf(initParam.get("type"));
				String ownBrkId = String.valueOf(initParam.get("ownBrkId"));
				String panName = String.valueOf(initParam.get("panName"));
				String panNo = String.valueOf(initParam.get("panNo"));
				String panDOB = String.valueOf(initParam.get("panDOB"));
				String panDate = String.valueOf(initParam.get("panDate"));
				MultipartFile decImg = null;
				resultMap = challanWebServiceDao.uploadImage(code,type, ownBrkId, panName, panNo, panDOB, panDate, panImg, decImg, user);
				
			}
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/uploadDecWs", method = RequestMethod.POST)
	public @ResponseBody Object uploadDec(@RequestParam Map<String, Object> initParam, @RequestParam("decImg") MultipartFile decImg){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.error("Only Dec");
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));			
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));			
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");			
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");
				
				String code = String.valueOf(initParam.get("code"));
				String type = String.valueOf(initParam.get("type"));
				String ownBrkId = String.valueOf(initParam.get("ownBrkId"));
				String panName = String.valueOf(initParam.get("panName"));
				String panNo = String.valueOf(initParam.get("panNo"));
				String panDOB = String.valueOf(initParam.get("panDOB"));
				String panDate = String.valueOf(initParam.get("panDate"));
				MultipartFile panImg = null;
				resultMap = challanWebServiceDao.uploadImage(code,type, ownBrkId, panName, panNo, panDOB, panDate, panImg, decImg, user);				
			}
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		return resultMap;
	}
	/*
	@RequestMapping(value = "/uploadPanDecWs", method = RequestMethod.POST)
	public @ResponseBody Object uploadPanDec(@RequestParam Map<String, Object> initParam, @RequestParam("image") MultipartFile image){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));			
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));			
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");			
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");
				
				String code = String.valueOf(initParam.get("code"));			
				String imageType = String.valueOf(initParam.get("imageType"));			
			
				System.out.println("Code : "+code);;
				System.out.println("Image Type : "+imageType);
				
				if(code.contains("own")){				
					String ownId = String.valueOf(initParam.get("ownId"));
					System.out.println("Owner ID : "+ownId);
					Owner owner = challanWebServiceDao.getOwnerById(Integer.parseInt(ownId));					
					if(imageType.equalsIgnoreCase("pancard")){
						String panName = String.valueOf(initParam.get("panName"));
						String panNo = String.valueOf(initParam.get("panNo"));
						String panDOB = String.valueOf(initParam.get("panDOB"));
						String panDate = String.valueOf(initParam.get("panDate"));
						
						byte imageInBytes[] = image.getBytes();
						Blob blob = new SerialBlob(imageInBytes);
						
						OwnerImg ownerImg = new OwnerImg();
						ownerImg.setbCode(user.getUserBranchCode());
						ownerImg.setUserCode(user.getUserCode());
						ownerImg.setOwnId(Integer.parseInt(ownId));
						ownerImg.setOwnPanImg(blob);						
						owner.setOwnIsPanImg(true);						
						owner.setOwnPanIntRt(0);
						
						try{
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
							Date panDOB1 = dateFormat.parse(panDOB);
							Date panDate1 = dateFormat.parse(panDate);
							owner.setOwnPanDOB(new java.sql.Date(panDOB1.getTime()));
							owner.setOwnPanDt(new java.sql.Date(panDate1.getTime()));
						}catch(Exception e){
							System.out.println("Error in converting Date : "+e);
						}					
						owner.setOwnPanName(panName);
						owner.setOwnPanNo(panNo);
						
						resultMap = challanWebServiceDao.uploadOwnPanDecImage(ownerImg, owner);						
					}else if(imageType.equalsIgnoreCase("declaration")){
						byte imageInBytes[] = image.getBytes();
						System.out.println("Image in Bytes : "+imageInBytes);
						Blob blob = new SerialBlob(imageInBytes);
						
						OwnerImg ownerImg = new OwnerImg();
						ownerImg.setbCode(user.getUserBranchCode());
						ownerImg.setUserCode(user.getUserCode());
						ownerImg.setOwnId(Integer.parseInt(ownId));
						ownerImg.setOwnDecImg(blob);						
						
						owner.setOwnIsDecImg(true);					
						owner.setOwnPanIntRt(0);
						
						resultMap = challanWebServiceDao.uploadOwnPanDecImage(ownerImg, owner);
					}
				}else if(code.contains("brk")){
					String brkId = String.valueOf(initParam.get("brkId"));
					System.out.println("brkId : "+brkId);
					Broker broker = challanWebServiceDao.getBrokerById(Integer.parseInt(brkId));
					if(imageType.equalsIgnoreCase("pancard")){
						String panName = String.valueOf(initParam.get("panName"));
						String panNo = String.valueOf(initParam.get("panNo"));
						String panDOB = String.valueOf(initParam.get("panDOB"));
						String panDate = String.valueOf(initParam.get("panDate"));
						
						byte imageInBytes[] = image.getBytes();
						Blob blob = new SerialBlob(imageInBytes);
						
						BrokerImg brokerImg = new BrokerImg();
						brokerImg.setbCode(user.getUserBranchCode());
						brokerImg.setUserCode(user.getUserCode());
						brokerImg.setBrkId(Integer.parseInt(brkId));
						brokerImg.setBrkPanImg(blob);						
						broker.setBrkIsPanImg(true);						
						broker.setBrkPanIntRt(0);						
						try{
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
							Date panDOB1 = dateFormat.parse(panDOB);
							Date panDate1 = dateFormat.parse(panDate);
							broker.setBrkPanDOB(new java.sql.Date(panDOB1.getTime()));
							broker.setBrkPanDt(new java.sql.Date(panDate1.getTime()));
						}catch(Exception e){
							System.out.println("Error in converting Date : "+e);
						}					
						broker.setBrkPanName(panName);
						broker.setBrkPanNo(panNo);
						
						resultMap = challanWebServiceDao.uploadBrkPanDecImage(brokerImg, broker);		
					}else if(imageType.equalsIgnoreCase("declaration")){
						byte imageInBytes[] = image.getBytes();
						Blob blob = new SerialBlob(imageInBytes);
						
						BrokerImg brokerImg = new BrokerImg();
						brokerImg.setbCode(user.getUserBranchCode());
						brokerImg.setUserCode(user.getUserCode());
						brokerImg.setBrkId(Integer.parseInt(brkId));
						brokerImg.setBrkDecImg(blob);						
						
						broker.setBrkIsDecImg(true);					
						broker.setBrkPanIntRt(0);
						
						resultMap = challanWebServiceDao.uploadBrkPanDecImage(brokerImg, broker);
					}
				}				
			}			
		}catch(Exception e){
			System.out.println("Error in uploadPanDec - ImageUpload : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		return resultMap;
	}
	*/
	public Map getUserByUN(String userName, String password){
		System.out.println("getUserByUN()......");
		List<User> userList = new ArrayList<User>();
		Map<String, Object> map = new HashMap<>();
		try{
			//convert pass to md5
			String pass = null;
	        try {
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(password.getBytes());
			    byte[] digest = md.digest();
			    StringBuffer sb = new StringBuffer();
			    for (byte b : digest) {
		            sb.append(String.format("%02x", b & 0xff));
		        }
			    pass = sb.toString();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}  
	        userList = userDAO.getUserByUN(userName, password);
	        if (!userList.isEmpty()) {
					User user = userList.get(0);					
				if (user.getPassword().equalsIgnoreCase(pass)) {				
					map.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
					map.put("msg", "valid password");
					map.put("user", user);
				} else {
					map.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					map.put("msg", "invalid password");
				}
			} else {
				map.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
				map.put("msg", "user not exist");
			}
		}catch(Exception e){
			System.out.println("Error in getUserByUN - WebService : "+e);
		}
		return map;
	}
	
}
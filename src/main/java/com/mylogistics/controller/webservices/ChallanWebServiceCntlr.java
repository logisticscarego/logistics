package com.mylogistics.controller.webservices;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import sun.java2d.pipe.SpanShapeRenderer.Simple;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.mylogistics.DAO.ContToStnDAO;
import com.mylogistics.DAO.DailyContractDAO;
import com.mylogistics.DAO.RateByKmDAO;
import com.mylogistics.DAO.RegularContractDAO;
import com.mylogistics.DAO.UserDAO;
import com.mylogistics.DAO.werservice.ChallanWebServiceDao;
import com.mylogistics.DAO.werservice.WebServiceDAO;
import com.mylogistics.model.Broker;
import com.mylogistics.model.BrokerImg;
import com.mylogistics.model.Challan;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.Cnmt_Challan;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.Owner;
import com.mylogistics.model.OwnerImg;
import com.mylogistics.model.RateByKm;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleVendorMstr;
import com.mylogistics.model.webservice.ChallanApp;
import com.mylogistics.model.webservice.ChallanDTApp;
import com.mylogistics.model.webservice.CnmtChlnApp;
import com.mylogistics.services.ConstantsValues;

@RestController
public class ChallanWebServiceCntlr {

	@Autowired
	private HttpSession httpSession;	
	@Autowired
	private ChallanWebServiceDao challanWebServiceDao;
	@Autowired
	private UserDAO userDAO;
	@Autowired
	private DailyContractDAO dailyContractDAO;
	@Autowired
	private ContToStnDAO contToStnDAO;
	@Autowired
	private RateByKmDAO rateByKmDAO;
	@Autowired
	private RegularContractDAO regularContractDAO;

	@RequestMapping(value = "/getCnmtCodeForChallanWs", method = RequestMethod.POST)
	public @ResponseBody Object getCnmtCodeForChallan(@RequestParam Map<String, Object> initParam){
		System.out.println("getCnmtCodeForChallanWs()..........");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");				
				resultMap = challanWebServiceDao.getCnmtCodeForChallan(user.getUserBranchCode());
				resultMap.put("branchCode", user.getUserBranchCode());
				resultMap.put("branchName", challanWebServiceDao.getBranchNameByCode(user.getUserBranchCode()));
				Map chlnCodeMap = challanWebServiceDao.getChlnCodeForChallan(user.getUserBranchCode());
				resultMap.put("chlnCodeList", chlnCodeMap.get("chlnCodeList"));
				Map vehicleNoMap = challanWebServiceDao.getVehicleNoList();
				resultMap.put("vehicleNoList", vehicleNoMap.get("vehicleNoList"));				
			}
		}catch(Exception e){
			Map<String, Object> errorMap = new HashMap<String, Object>();
			errorMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			errorMap.put("msg", "Something is going wrong !");
			return errorMap;
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/getCnmtDetailForChallanWs", method = RequestMethod.POST)
	public @ResponseBody Object getCnmtDetailForChallan(@RequestParam Map<String, Object> initParam){
		System.out.println("getCnmtDetailForChallanWs()..........");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");
				String cnmtCode = String.valueOf(initParam.get("cnmtCode"));
				resultMap = challanWebServiceDao.getCnmtDetailForChallan(cnmtCode);				
			}
		}catch(Exception e){
			Map<String, Object> errorMap = new HashMap<String, Object>();
			errorMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			errorMap.put("msg", "Something is going wrong !");
			return errorMap;
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/getVehicleNoForChallanWs", method = RequestMethod.POST)
	public @ResponseBody Object getVehicleNoForChallanWs(@RequestParam Map<String, Object> initParam){
		System.out.println("getVehicleNoForChallanWs()..........");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");				
				resultMap = challanWebServiceDao.getVehicleNoList();			
			}
		}catch(Exception e){
			Map<String, Object> errorMap = new HashMap<String, Object>();
			errorMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			errorMap.put("msg", "Something is going wrong !");
			return errorMap;
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/getChlnCodeForChallanWs", method = RequestMethod.POST)
	public @ResponseBody Object getChlnCodeForChallanWs(@RequestParam Map<String, Object> initParam){
		System.out.println("getChlnCodeForChallanWs()..........");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");				
				resultMap = challanWebServiceDao.getChlnCodeForChallan(user.getUserBranchCode());				
			}
		}catch(Exception e){
			Map<String, Object> errorMap = new HashMap<String, Object>();
			errorMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			errorMap.put("msg", "Something is going wrong !");
			return errorMap;
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/getTransitDayWs", method = RequestMethod.POST)
	public @ResponseBody Object getTransitDay(@RequestParam Map<String, Object> initParam) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");
				String cnmtCode = String.valueOf(initParam.get("cnmtCode"));
				List<Cnmt> cnmtList = challanWebServiceDao.getCnmtByCnmtCode(cnmtCode, "cnmt");
				if(!cnmtList.isEmpty()){
					Cnmt cnmt = cnmtList.get(0);
					int cnmtId = cnmt.getCmId();
					List<Integer> cnmt_ChallanList = challanWebServiceDao.getCnmt_ChallanByCnmtId(cnmtId);
					if(! cnmt_ChallanList.isEmpty()){
						System.out.println("transhipment");
						int chlnId = cnmt_ChallanList.get(cnmt_ChallanList.size()-1);
						List<String> challanList = challanWebServiceDao.getChallanToStationById(chlnId);						
						String toStation = challanList.get(0);
						resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
						resultMap.put("mc","true");
						resultMap.put("fromStation", toStation);
						resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
					}else{
						String contractCode = cnmt.getContractCode();						
						String contractType = contractCode.substring(0,3);						
						List<DailyContract> dailyContList = new ArrayList<DailyContract>();
						List<RegularContract> regContList = new ArrayList<RegularContract>();						
						if(contractType.equalsIgnoreCase("dly")){							
							dailyContList = dailyContractDAO.getDailyContract(contractCode);
							if(!dailyContList.isEmpty()){
								DailyContract dailyContract = dailyContList.get(0);
								String fromStation = dailyContract.getDlyContFromStation();
								String stnCode = cnmt.getCnmtToSt();
								String stateCode = cnmt.getCnmtState();
								String vehicleType = cnmt.getCnmtVehicleType();
								String productType = cnmt.getCnmtProductType();
								java.sql.Date cnmtDt=cnmt.getCnmtDt();
								int transitDay = 0;
								//int transitDay = dailyContract.getDlyContTransitDay();
								if(dailyContract.getDlyContType().equalsIgnoreCase("W")){
									List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateW(contractCode , stnCode , vehicleType);
									if(!ContToStnList.isEmpty()){
										transitDay = ContToStnList.get(0).getCtsToTransitDay();
									}
								}else if(dailyContract.getDlyContType().equalsIgnoreCase("Q")){
									List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateQ(contractCode , stnCode , productType,vehicleType,cnmtDt);
									if(!ContToStnList.isEmpty()){
										transitDay = ContToStnList.get(0).getCtsToTransitDay();
									}
								}else if(dailyContract.getDlyContType().equalsIgnoreCase("K")){
									String toStation = dailyContract.getDlyContToStation();
									List<RateByKm> rbkmList = rateByKmDAO.getRbkmForChln(contractCode , fromStation , toStation , stateCode);
									if(!rbkmList.isEmpty()){
										transitDay = rbkmList.get(0).getRbkmTransitDay();
									}
								}else{
									System.out.println("invalid contract");
								}							
								resultMap.put("fromStation", fromStation);
								resultMap.put("transitDay", transitDay);
								resultMap.put("mc","false");
								resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
							}else{
								resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
								resultMap.put("msg", "Contact does not exists !");
							}
						}else if(contractType.equalsIgnoreCase("reg")){
							regContList = regularContractDAO.getRegularContract(contractCode);
							if(!regContList.isEmpty()){
								RegularContract regularContract = regContList.get(0);
								String fromStation = regularContract.getRegContFromStation();
								String stnCode = cnmt.getCnmtToSt();
								String stateCode = cnmt.getCnmtState();
								String vehicleType = cnmt.getCnmtVehicleType();
								String productType = cnmt.getCnmtProductType();
								java.sql.Date cnmtDt=cnmt.getCnmtDt();
								int transitDay = 0;
								if(regularContract.getRegContType().equalsIgnoreCase("W")){
									List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateW(contractCode , stnCode , vehicleType);
									if(!ContToStnList.isEmpty()){
										transitDay = ContToStnList.get(0).getCtsToTransitDay();
									}
								}else if(regularContract.getRegContType().equalsIgnoreCase("Q")){
									List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateQ(contractCode , stnCode , productType,vehicleType,cnmtDt);
									if(!ContToStnList.isEmpty()){
										transitDay = ContToStnList.get(0).getCtsToTransitDay();
									}
								}else if(regularContract.getRegContType().equalsIgnoreCase("K")){
									String toStation = regularContract.getRegContToStation();
									List<RateByKm> rbkmList = rateByKmDAO.getRbkmForChln(contractCode , fromStation , toStation , stateCode);
									if(!rbkmList.isEmpty()){
										transitDay = rbkmList.get(0).getRbkmTransitDay();
									}
								}else{
									System.out.println("invalid contract");
								}
								//int transitDay = regularContract.getRegContTransitDay();
								resultMap.put("fromStation", fromStation);
								resultMap.put("transitDay", transitDay);
								resultMap.put("mc","false");
								resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
							}else{
								resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
								resultMap.put("msg", "Contact does not exists !");
							}
						}else{
							resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
						}
					}
				}else{
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					resultMap.put("msg", "No Such Record !");
				}
				//resultMap = challanWebServiceDao.getTransitDay(cnmtCode);			
			}
		}catch(Exception e){
			Map<String, Object> errorMap = new HashMap<String, Object>();
			errorMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			errorMap.put("msg", "Something is going wrong !");
			return errorMap;
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/getVehicleForChlnWs", method = RequestMethod.POST)
	public @ResponseBody Object getVehicleForChlnWs(@RequestParam Map<String, Object> initParam){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));			
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");				
				resultMap = challanWebServiceDao.getVehicleForChln();
			}
		}catch(Exception e){
			System.out.println("Error in getVehicleForChlnWs - ChallanWebServiceCntrl : "+e);
			Map<String, Object> errorMap = new HashMap<String, Object>();
			errorMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			errorMap.put("msg", "Something is going wrong !");
			return errorMap;
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/getChlnDtByVehicleNo", method = RequestMethod.POST)
	public @ResponseBody Object getChlnDtByVehicleNo(@RequestParam Map<String, Object> initParam){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));			
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");				
				String vehicleNo = String.valueOf(initParam.get("vehicleNo"));				
				resultMap = challanWebServiceDao.getChlnDtByVehicleNo(vehicleNo);
				status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
				if(status.equalsIgnoreCase("success")){
					VehicleVendorMstr veh = (VehicleVendorMstr)resultMap.get("vehicleMst");
					resultMap.remove("vehicleMst");										
					resultMap.put("vehM", veh);					
				}
			}
		}catch(Exception e){
			System.out.println("Error in getVehicleForChlnWs - ChallanWebServiceCntrl : "+e);
			Map<String, Object> errorMap = new HashMap<String, Object>();
			errorMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			errorMap.put("msg", "Something is going wrong !");
			return errorMap;
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/submitChlnAndChlnDtWs", method = RequestMethod.POST)
	public @ResponseBody Object submitChlnAndChlnDt(@RequestParam Map<String, Object> initParam){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{				
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));			
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");				
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");				
				
				ChallanApp challanApp = null;
				ChallanDTApp challanDTApp = null;
				List<CnmtChlnApp> cnmtChlnAppList = null;			
			
				String challanAppJSON = String.valueOf(initParam.get("challanApp"));			
				String challanDTAppJSON = String.valueOf(initParam.get("challanDTApp"));
				String cnmtChlnAppJSONList = String.valueOf(initParam.get("cnmtChlnAppList"));				
				
				
				if(challanAppJSON == null){
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					resultMap.put("msg", "Not found necessary information of Challan !");
					return resultMap;
				}else if(challanDTAppJSON == null){
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					resultMap.put("msg", "Not found necessary information of Challan Detail !");
					return resultMap;
				}else if(cnmtChlnAppJSONList == null){
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					resultMap.put("msg", "Not found necessary information of CNMT !");
					return resultMap;
				}
				
				// Converting JSON String to Corresponding Class Object
				ObjectMapper mapper = new ObjectMapper();
				if(challanAppJSON != null){
					try{					
						challanApp = mapper.readValue(challanAppJSON, ChallanApp.class);
					}catch(Exception e){
						System.out.println("Exception : "+e);
					}
				}
				if(challanDTAppJSON != null){
					try{		
						challanDTApp = mapper.readValue(challanDTAppJSON, ChallanDTApp.class);
					}catch(Exception e){
						System.out.println("Exception : "+e);
					}
				}
				if(cnmtChlnAppJSONList != null){
					try{
					cnmtChlnAppList = mapper.readValue(cnmtChlnAppJSONList, TypeFactory.defaultInstance().constructCollectionType(List.class, CnmtChlnApp.class));
					}catch(Exception e){
						System.out.println("Exception : "+e);
					}
				}
				
				Boolean available = challanWebServiceDao.isChlnAvailable(challanApp.getChlnCode());
				
				if(! available){
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					resultMap.put("msg", "Challan already exist !");
					return resultMap;
				}
				
				challanApp.setChlnLryNo(challanDTApp.getChdLorryNo());
				challanApp.setBranchCode(user.getUserBranchCode());
				challanApp.setUserCode(user.getUserCode());
				challanApp.setIsPending(true);
				challanApp.setIsCancel(false);
				
				challanDTApp.setBranchCode(user.getUserBranchCode());
				challanDTApp.setUserCode(user.getUserCode());
				challanDTApp.setChlnCode(challanApp.getChlnCode());
				
				status = challanWebServiceDao.saveChallanAndChallanDTAndCnmtChlnApp(challanApp, challanDTApp, cnmtChlnAppList);
				
				if(status.equalsIgnoreCase("success")){				
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);	
					resultMap.put("msg", "Your Challn has been submitted !");				
				}else{
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					resultMap.put("msg", "Your Challn has not been submitted ! Try Again !");
				}				
			}
		}catch(Exception e){
			System.out.println("Error in submitChlnAndChlnDt - ChallanWebServiceCntrl : "+e);
			Map<String, Object> errorMap = new HashMap<String, Object>();
			errorMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			errorMap.put("msg", "Something is going wrong !");
			return errorMap;
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/getVehicleDtWs", method = RequestMethod.POST)
	public @ResponseBody Object getVehicleDtWe(@RequestParam Map<String, Object> initParam){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));			
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");			
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");				
				String vehicleNo = String.valueOf(initParam.get("vehicleNo"));
				System.out.println("Vehicle No : "+vehicleNo);
				resultMap = challanWebServiceDao.getVehicleDtWe(vehicleNo);			
			}
		}catch(Exception e){
			System.out.println("Error in getVehicleDtWe - ChallanWebServiceCntlr : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		return resultMap;
	}
	
	public Map getUserByUN(String userName, String password){
		System.out.println("getUserByUN()......");
		List<User> userList = new ArrayList<User>();
		Map<String, Object> map = new HashMap<>();
		try{
			//convert pass to md5
			String pass = null;
	        try {
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(password.getBytes());
			    byte[] digest = md.digest();
			    StringBuffer sb = new StringBuffer();
			    for (byte b : digest) {
		            sb.append(String.format("%02x", b & 0xff));
		        }
			    pass = sb.toString();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}  
	        userList = userDAO.getUserByUN(userName, password);
	        if (!userList.isEmpty()) {
					User user = userList.get(0);					
				if (user.getPassword().equalsIgnoreCase(pass)) {				
					map.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
					map.put("msg", "valid password");
					map.put("user", user);
				} else {
					map.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					map.put("msg", "invalid password");
				}
			} else {
				map.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
				map.put("msg", "user not exist");
			}
		}catch(Exception e){
			System.out.println("Error in getUserByUN - WebService : "+e);
		}
		return map;
	}
	
	public HttpSession getHttpSession() {
		return httpSession;
	}

	public void setHttpSession(HttpSession httpSession) {
		this.httpSession = httpSession;
	}

	public ChallanWebServiceDao getChallanWebServiceDao() {
		return challanWebServiceDao;
	}

	public void setChallanWebServiceDao(ChallanWebServiceDao challanWebServiceDao) {
		this.challanWebServiceDao = challanWebServiceDao;
	}	
	
}
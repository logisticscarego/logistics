package com.mylogistics.controller.webservices;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mylogistics.DAO.UserDAO;
import com.mylogistics.DAO.werservice.VehicleWebServiceDao;
import com.mylogistics.model.User;
import com.mylogistics.model.webservice.VehicleApp;
import com.mylogistics.services.ConstantsValues;

@RestController
public class VehicleWebService {

	public static Logger logger = Logger.getLogger(VehicleWebService.class);
	@Autowired	
	private VehicleWebServiceDao vehicleWebServiceDao;
	@Autowired
	private UserDAO userDAO;
	
	@RequestMapping(value = "/getOwnsBrksWs", method = RequestMethod.POST)
	public @ResponseBody Object getOwnsBrksWs(@RequestParam  Map<String, Object> initParam){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("Enter into getOwnsBrksWs()...");
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");			
				resultMap = vehicleWebServiceDao.getOwnsBrksWs();
			}
		}catch(Exception e){
			logger.error("Something is going wrong - getOwnsBrksWs() ! : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		logger.info("Exit from getOwnsBrksWs()...");
		return resultMap;
	}
	
	@RequestMapping(value = "/vehicleCheckWs", method = RequestMethod.POST)
	public @ResponseBody Object vehicleCheckWs(@RequestParam Map<String, Object> initParam){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("Enter into vehicleCheckWs()...");
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");			
				if(initParam.get("rcNo") == null){
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					resultMap.put("msg", "Please enter RC No...");
					return resultMap;
				}
				String rcNo = String.valueOf(initParam.get("rcNo"));
				resultMap = vehicleWebServiceDao.vehicleCheck(rcNo);
			}
		}catch(Exception e){
			logger.error("Something is going wrong - vehicleCheckWs() ! : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		logger.info("Exit From vehicleCheckWs()...");
		return resultMap;
	}
	
	@RequestMapping(value = "/addNewVehicleWs", method = RequestMethod.POST)
	public @ResponseBody Object addNewVehicleWs(@RequestParam Map<String, Object> initParam){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("Enter into addNewVehicleWs()...");
		try{
			String userName = String.valueOf(initParam.get("userName"));
			String password = String.valueOf(initParam.get("password"));			
			resultMap = getUserByUN(userName, password);
			String status = String.valueOf(resultMap.get(ConstantsValues.STATUS));
			if(status.equalsIgnoreCase(ConstantsValues.SUCCESS)){
				logger.info("User Found !");
				User user = (User)resultMap.get("user");
				resultMap.remove(ConstantsValues.STATUS);
				resultMap.remove("user");			
				String vehicleJSON = String.valueOf(initParam.get("vehicle"));
				logger.info("Vehicle JSON Found !");
				VehicleApp vehicleApp = null;
				ObjectMapper objectMapper = new ObjectMapper();
				if(vehicleJSON != null){
					vehicleApp = objectMapper.readValue(vehicleJSON, VehicleApp.class);
					logger.info("JSON Converted !");
					Boolean alreadyVehicle = vehicleWebServiceDao.findAlreadyVehicle(vehicleApp.getvRcNo());
					if(alreadyVehicle){						
						resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
						resultMap.put("msg", "Vehicle already exists !");
					}else{
						vehicleApp.setIsPending(true);
						vehicleApp.setIsCancel(false);
						vehicleApp.setBranchCode(user.getUserBranchCode());
						vehicleApp.setUserCode(user.getUserCode());
						resultMap = vehicleWebServiceDao.addNewVehicle(vehicleApp);
					}
				}else{
					logger.info("Vehicle detail not found !");
					resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					resultMap.put("msg", "Vehicle details not found !");
				}
			}
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		logger.info("Exit from addNewVehicleWs()...");
		return resultMap;
	}
	
	public Map getUserByUN(String userName, String password){
		System.out.println("getUserByUN()......");
		List<User> userList = new ArrayList<User>();
		Map<String, Object> map = new HashMap<>();
		try{
			//convert pass to md5
			String pass = null;
	        try {
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(password.getBytes());
			    byte[] digest = md.digest();
			    StringBuffer sb = new StringBuffer();
			    for (byte b : digest) {
		            sb.append(String.format("%02x", b & 0xff));
		        }
			    pass = sb.toString();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}  
	        userList = userDAO.getUserByUN(userName, password);
	        if (!userList.isEmpty()) {
					User user = userList.get(0);					
				if (user.getPassword().equalsIgnoreCase(pass)) {				
					map.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
					map.put("msg", "valid password");
					map.put("user", user);
				} else {
					map.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
					map.put("msg", "invalid password");
				}
			} else {
				map.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
				map.put("msg", "user not exist");
			}
		}catch(Exception e){
			System.out.println("Error in getUserByUN - WebService : "+e);
		}
		return map;
	}
	
}
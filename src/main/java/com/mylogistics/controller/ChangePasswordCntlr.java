package com.mylogistics.controller;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.UserDAO;
import com.mylogistics.model.User;

@Controller

public class ChangePasswordCntlr {
	@Autowired
	private UserDAO userDAO;
	
	@RequestMapping(value = "/changePassword" , method = RequestMethod.POST)
	public @ResponseBody Object changePassword(@RequestBody User user) throws Exception{
		System.out.println("enter into changePassword function");
		Map<String,String> map = new HashMap<String,String>();
		String pass = user.getPassword();
		
		if(user.getUserId()!=0 && (user.getPassword()!=null || user.getPassword()!="")){
			
			MessageDigest md = MessageDigest.getInstance("MD5");
		    md.update(pass.getBytes());
		    byte[] digest = md.digest();
		    StringBuffer sb = new StringBuffer();
		    for (byte b : digest) {
	            sb.append(String.format("%02x", b & 0xff));
	        }
		    
	        String password = sb.toString();
	        
	        user.setPassword(password);
	        
			int i=userDAO.changePass(user);
			if(i==1){
					map.put("result","success");
					return map;
			}else{
					map.put("result","failure");
					return map;
			}
		}else{
			map.put("result","failure");
			return map;
		}
	}
}

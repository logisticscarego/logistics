package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.JVoucherDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.JVouchService;
import com.mylogistics.services.JourVouchDRCR;
import com.mylogistics.services.VoucherService;

@Controller
public class JourVoucherCntlr {

	public static final Logger logger = Logger.getLogger(JourVoucherCntlr.class);
	
	@Autowired
	private HttpSession httpSession;
	@Autowired
	private BranchDAO branchDAO;
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	@Autowired
	private BankMstrDAO bankMstrDAO;
	@Autowired
	private FaMasterDAO faMasterDAO;
	@Autowired
	private JVoucherDAO jVoucherDAO;
	
	@RequestMapping(value = "/getVDetFrJV" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrJV() {  
		logger.info("Enter into getVDetFrJV---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				logger.info("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				logger.info("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					logger.info("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getAllFaCodeFrJV" , method = RequestMethod.POST)  
	public @ResponseBody Object getAllFaCodeFrJV() {  
		logger.info("Enter into getAllFaCodeFrJV---->");
		Map<String,Object> map = new HashMap<>();
		List<FAMaster> faMList = faMasterDAO.getAllFAM();
		if(!faMList.isEmpty()){
			map.put("list",faMList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/submitJPVoucher" , method = RequestMethod.POST)  
	public @ResponseBody Object submitJPVoucher(@RequestBody JVouchService jVouchService) {  
		logger.info("Enter into submitJPVoucher---->");
		Map<String,Object> map = new HashMap<>();
		VoucherService voucherService = jVouchService.getVoucherService();
		List<JourVouchDRCR> jvList = jVouchService.getJvList();
		logger.info("size of jvList = "+jvList.size());
		
		if(voucherService.getVoucherType().equalsIgnoreCase(ConstantsValues.JR_VOUCHER)) {
			 map = jVoucherDAO.saveJVoucher(voucherService , jvList);
			 int vhNo = (int) map.get("vhNo");
			if(vhNo>0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}
		return map;
	}
	
	@RequestMapping(value = "/submitJPVoucherN" , method = RequestMethod.POST)  
	public @ResponseBody Object submitJPVoucherN(@RequestBody JVouchService jVouchService) {  
		logger.info("Enter into submitJPVoucher---->");
		Map<String,Object> map = new HashMap<>();
		VoucherService voucherService = jVouchService.getVoucherService();
		List<JourVouchDRCR> jvList = jVouchService.getJvList();
		logger.info("size of jvList = "+jvList.size());
		
		if(voucherService.getVoucherType().equalsIgnoreCase(ConstantsValues.JR_VOUCHER)) {
			 map = jVoucherDAO.saveJVoucherN(voucherService , jvList);
			 int vhNo = (int) map.get("vhNo");
			if(vhNo>0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}
		return map;
	}
	
}
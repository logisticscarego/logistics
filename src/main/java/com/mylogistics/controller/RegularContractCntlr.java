package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.ContToStnDAO;
import com.mylogistics.DAO.ContractRightsDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.CustomerRepresentativeDAO;
import com.mylogistics.DAO.FAParticularDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.PenaltyBonusDetentionDAO;
import com.mylogistics.DAO.ProductTypeDAO;
import com.mylogistics.DAO.RateByKmDAO;
import com.mylogistics.DAO.RegularContractDAO;
import com.mylogistics.DAO.StateDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.DAO.TermNConditionDAO;
import com.mylogistics.DAO.VehicleTypeDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.ContractRights;
import com.mylogistics.model.Customer;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.PenaltyBonusDetention;
import com.mylogistics.model.ProductType;
import com.mylogistics.model.RateByKm;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.State;
import com.mylogistics.model.Station;
import com.mylogistics.model.TermNCondition;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleType;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.ContToStnService;
import com.mylogistics.services.ContToStnServiceImpl;
import com.mylogistics.services.CustomerNCustomerRep;
import com.mylogistics.services.PBDService;
import com.mylogistics.services.PBDServiceImpl;
import com.mylogistics.services.RBKMService;
import com.mylogistics.services.RBKMServiceImpl;

@Controller
public class RegularContractCntlr {
	
	@Autowired
	private RegularContractDAO regularContractDAO;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private CustomerRepresentativeDAO customerRepresentativeDAO;
	
	@Autowired
	private StationDAO stationDAO;
	
	@Autowired
	private ProductTypeDAO productTypeDAO;
	
	@Autowired
	private VehicleTypeDAO vehicleTypeDAO;
	
	@Autowired
	private StateDAO stateDAO;
	
	@Autowired
	private RateByKmDAO rateByKmDAO;
	
	@Autowired
	private PenaltyBonusDetentionDAO penaltyBonusDetentionDAO;
	
	@Autowired
	private TermNConditionDAO termNConditionDAO;
	
	@Autowired
    private HttpSession httpSession;
	
	@Autowired
	private ContToStnDAO contToStnDAO;
	
	@Autowired
	private ContractRightsDAO contractRightsDAO;
	
	@Autowired
	private FAParticularDAO faParticularDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	private RBKMService rbkmService = new RBKMServiceImpl();
	
	private PBDService pbdService = new PBDServiceImpl();
	
	private ContToStnService contToStnService = new ContToStnServiceImpl();
	
	String regContCode = null;
	
	String metric = null;
	
		@RequestMapping(value="/saveRegularContract",method = RequestMethod.POST)
		public @ResponseBody Object saveRegularContract(@RequestBody RegularContract regularContract){
		
			List<RateByKm> rbkmList = rbkmService.getAllRBKM();
			List<PenaltyBonusDetention> pbdList = pbdService.getAllPBD();
			List<ContToStn> cList = contToStnService.getAllCTS();
			Map<String, String> map = new HashMap<String, String>();
			
			User currentUser = (User)httpSession.getAttribute("currentUser");
			
			String last = null;
			long end = 0;
			String faCode=null;
		
			long totalRows = regularContractDAO.totalCount();
			
			if (!(totalRows == -1)) {
				if(totalRows==0){
					last="0000001";
					regContCode="reg"+1;
				}else{	
					long id = regularContractDAO.getLastRegcontractId()+1;
					end = 10000000+id;
					last = String.valueOf(end).substring(1,8);
					regContCode="reg"+id;
					}
				
				//regContCode = CodePatternService.regContCodeGen(regularContract,last);
				System.out.println("After generating the code"+regContCode);
				
				List<FAParticular> faParticulars = faParticularDAO.getFAParticular(ConstantsValues.FAP_REG_CONT);
				FAParticular fAParticular = new FAParticular();
				fAParticular=faParticulars.get(0);
				int fapId=fAParticular.getFaPerId();
				String fapIdStr = String.valueOf(fapId); 
				System.out.println("the fap id is--------------------"+fapId+""+fapIdStr);
				if(fapId<10){
					fapIdStr="0"+fapIdStr;
					System.out.println("After adding zeroes--"+fapIdStr);
				}
				
				regularContract.setfAParticular(fAParticular);
				regularContract.setView(false);
				regularContract.setBranchCode(currentUser.getUserBranchCode());
				regularContract.setUserCode(currentUser.getUserCode());
				regularContract.setbCode(currentUser.getUserBranchCode());
				regularContract.setRegContCode(regContCode);
				regularContract.setRegContIsVerify("no");
			
				int temp=regularContractDAO.saveRegularContract(regularContract);
				System.out.println("Contract saved with id------------"+temp);
				temp=temp+100000;
				String contId = String.valueOf(temp).substring(1,6);
				faCode=fapIdStr+contId;
				System.out.println("faCode code is------------------"+faCode);
				regularContract.setRegContFaCode(faCode);
				int updateCont = regularContractDAO.updateRegularContract(regularContract);
				if(updateCont > 0){
					map.put("regContFaCode" , regularContract.getRegContFaCode());
				}
				System.out.println("Cont updated-------"+regularContract.getRegContFaCode());
				
				List<Customer> customerList= customerDAO.getCustomer(regularContract.getRegContBLPMCode());
				String custName=customerList.get(0).getCustName();
				/*FAMaster faMaster = new FAMaster();
				faMaster.setFaMfaCode(faCode);
				faMaster.setFaMfaType("regcontract");
				faMaster.setFaMfaName(custName);
				faMaster.setbCode(currentUser.getUserBranchCode());
				faMaster.setUserCode(currentUser.getUserCode());
				faMasterDAO.saveFaMaster(faMaster);*/
				
				if (temp >= 0) {
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				} else {
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else {
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			
			if(!rbkmList.isEmpty()){
				for(int i = 0; i < rbkmList.size(); i++) {
					double rbkmRate=0;
					if(metric.equals("Ton")){
						rbkmRate = rbkmList.get(i).getRbkmRate();
						rbkmRate = (rbkmRate/ConstantsValues.kgInTon);
						//rbkmRate=Double.parseDouble((df.format(rbkmRate)));
						  //rbkmRate = (double)Math.round(rbkmRate*100.0)/100.0;
						rbkmList.get(i).setRbkmRate(rbkmRate);
					}
					System.out.println("Rbkm rate is-----"+rbkmRate);
					rbkmList.get(i).setRbkmContCode(regContCode);
					rbkmList.get(i).setbCode(currentUser.getUserBranchCode());
					rbkmList.get(i).setUserCode(currentUser.getUserCode());
				int temp =rateByKmDAO.saveRBKM(rbkmList.get(i));
				if(temp>0){
					map.put(ConstantsValues.RESULT+"Rbkm",ConstantsValues.SUCCESS+"Rbkm");
				}else{
					map.put(ConstantsValues.RESULT+"Rbkm",ConstantsValues.ERROR+"Rbkm");
				}
				}
				}
			
			if(!pbdList.isEmpty()){
				for (int i = 0; i < pbdList.size(); i++) {
					pbdList.get(i).setPbdContCode(regContCode);
					pbdList.get(i).setbCode(currentUser.getUserBranchCode());
					pbdList.get(i).setUserCode(currentUser.getUserCode());
					int temp = penaltyBonusDetentionDAO.savePBD(pbdList.get(i));
					if(temp>0){
						map.put(ConstantsValues.RESULT+"Pbd",ConstantsValues.SUCCESS+"Pbd");
					}else{
						map.put(ConstantsValues.RESULT+"Pbd",ConstantsValues.ERROR+"Pbd");
					}
				}
			}
			
			
			if(!cList.isEmpty()){
				for(int i = 0; i < cList.size();i++) {
					double ctsRate=0;
					double ctsFromWt=0;
					double ctsToWt=0;
					if(metric.equals("Ton") && (regularContract.getRegContProportionate().equals("P"))){
						ctsRate = cList.get(i).getCtsRate();
						ctsRate = ctsRate/ConstantsValues.kgInTon;
						//ctsRate = (double)Math.round(ctsRate*100.0)/100.0;
						cList.get(i).setCtsRate(ctsRate);
						
						ctsFromWt = cList.get(i).getCtsFromWt();
						ctsFromWt = ctsFromWt*ConstantsValues.kgInTon;
						//ctsFromWt = (double)Math.round(ctsFromWt*100.0)/100.0;
						cList.get(i).setCtsFromWt(ctsFromWt);
						
						ctsToWt = cList.get(i).getCtsToWt();
						ctsToWt = ctsToWt*ConstantsValues.kgInTon;
						//ctsToWt = (double)Math.round(ctsToWt*100.0)/100.0;
						cList.get(i).setCtsToWt(ctsToWt);
						
					}else if(metric.equals("Ton") && (regularContract.getRegContProportionate().equals("F"))){
						ctsRate = cList.get(i).getCtsRate();
						cList.get(i).setCtsRate(ctsRate);
						
						ctsFromWt = cList.get(i).getCtsFromWt();
						ctsFromWt = ctsFromWt*ConstantsValues.kgInTon;
						//ctsFromWt = (double)Math.round(ctsFromWt*100.0)/100.0;
						cList.get(i).setCtsFromWt(ctsFromWt);
						
						ctsToWt = cList.get(i).getCtsToWt();
						ctsToWt = ctsToWt*ConstantsValues.kgInTon;
						//ctsToWt = (double)Math.round(ctsToWt*100.0)/100.0;
						cList.get(i).setCtsToWt(ctsToWt);
					}
					System.out.println("proportionate is ----"+regularContract.getRegContProportionate());
					System.out.println("cts rate is ----"+ctsRate);
					System.out.println("ctsFromWt is ----"+ctsFromWt);
					System.out.println("ctsToWt is ----"+ctsToWt);
					
					cList.get(i).setCtsContCode(regContCode);
					cList.get(i).setbCode(currentUser.getUserBranchCode());
					cList.get(i).setUserCode(currentUser.getUserCode());
					cList.get(i).setCtsFrDt(regularContract.getRegContFromDt());
					cList.get(i).setCtsToDt(regularContract.getRegContToDt());
					int temp=contToStnDAO.saveContToStn(cList.get(i));
					if(temp>0){
						map.put(ConstantsValues.RESULT+"Cts",ConstantsValues.SUCCESS+"Cts");
					}else{
						map.put(ConstantsValues.RESULT+"Cts",ConstantsValues.ERROR+"Cts");
					}
				}
			}
			
				ContractRights contractRights = new ContractRights();
				contractRights.setbCode(currentUser.getUserBranchCode());
				contractRights.setUserCode(currentUser.getUserCode());
				contractRights.setContCode(regContCode);
				contractRights.setCrContBillBasis("chargeWt");
				int saveContRghts=contractRightsDAO.saveContractRights(contractRights);
				System.out.println("After saving contract rights---------"+saveContRghts);
				
			rbkmService.deleteAllRBKM();
			pbdService.deleteAllPBD();
			
			
		return map;
		}
	
		@RequestMapping(value="/saveTncForRegCont",method = RequestMethod.POST)
		public @ResponseBody Object saveTncForRegCont(@RequestBody TermNCondition termNCondition){
			
			System.out.println("Controller called of saveTnc");
			Map<String,String> map = new HashMap<String,String>();

			User currentUser = (User)httpSession.getAttribute("currentUser");
			termNCondition.setTncContCode(regContCode);
			termNCondition.setUserCode(currentUser.getUserCode());
			termNCondition.setbCode(currentUser.getUserBranchCode());
			int temp = termNConditionDAO.saveTnc(termNCondition);
			
				if (temp>=0) {
					map.put(ConstantsValues.RESULT+"Tnc",ConstantsValues.SUCCESS+"Tnc");
				}else {
					map.put(ConstantsValues.RESULT+"Tnc",ConstantsValues.ERROR+"Tnc");
				}
				
				return map;	
			}
		
		@RequestMapping(value = "/getBranchDataForRegCont", method = RequestMethod.POST)
		public @ResponseBody Object getBranchDataForRegCont() {
			System.out.println("enter into getBranchDataForRegCont function");
			List<Branch> branchList = new ArrayList<Branch>();
			Map<String, Object> map = new HashMap<String, Object>();
			branchList = branchDAO.getAllActiveBranches();
			System.out.println("size of branchList------>"+branchList.size());
			if(!branchList.isEmpty()){
				map.put("list", branchList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;
		}
		
		@RequestMapping(value = "/getCustomerDataForRegCont", method = RequestMethod.POST)
		public @ResponseBody Object getCustomerDataForRegCont() {

			Map<String, Object> map = new HashMap<String, Object>();
			List<Customer> customerList=customerDAO.getCustomerCodeNameContract();
			if(!customerList.isEmpty()){
				map.put("list", customerList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;
		}
	
		@RequestMapping(value = "/getCRDataForRegCont", method = RequestMethod.POST)
		public @ResponseBody Object getCRDataForRegCont(@RequestBody String custCode) {
			
			System.out.println("Enter into getCRDataForRegCont function");
			
			Map<String, Object> map = new HashMap<String, Object>();
			List<CustomerNCustomerRep> list = new ArrayList<CustomerNCustomerRep>();
			List<Object[]> list1 = customerRepresentativeDAO.getCRCodeNameDesigCustName(custCode);

			if(!list1.isEmpty()){
			for (Object[] objects : list1) {
				CustomerNCustomerRep customerNCustomerRep = new CustomerNCustomerRep();
				customerNCustomerRep.setCustRepName((String) objects[0]);
				customerNCustomerRep.setCustRepCode((String) objects[1]);
				customerNCustomerRep.setCustRepDesig((String) objects[2]);
				customerNCustomerRep.setCustName((String) objects[3]);
				list.add(customerNCustomerRep);
			}
			map.put("list", list);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;
		}
		

		@RequestMapping(value = "/getStationDataForRegCont", method = RequestMethod.POST)
		public @ResponseBody Object getStationDataForRegCont() {

			Map<String, Object> map = new HashMap<String, Object>();
			List<Station> stationList = new ArrayList<Station>();
			 stationList = stationDAO.getStationData();

			 if(!stationList.isEmpty()){
					map.put("list", stationList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				} else {
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
		return map;
	}
		
		@RequestMapping(value = "/getVehicleTypeCodeForRegCont", method = RequestMethod.POST)
		public @ResponseBody Object getVehicleTypeCodeForRegCont() {
			Map<String, Object> map = new HashMap<String, Object>();
			List<VehicleType> vtList = new ArrayList<VehicleType>();
			 vtList = vehicleTypeDAO.getVehicleType();
			 if (!vtList.isEmpty()) {	 
				 map.put("list", vtList);
				 map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			 }else{
				 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			 }
			return map;
		}
		
		
		@RequestMapping(value = "/getProductNameForRegCont", method = RequestMethod.POST)
		public @ResponseBody Object getProductNameForRegCont() {
			Map<String, Object> map = new HashMap<String, Object>();
			List<String> ptList = productTypeDAO.getProductName();

			if(!ptList.isEmpty()){
				map.put("list", ptList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;
		}

		@RequestMapping(value = "/getStateDataForRegCont", method = RequestMethod.POST)
		public @ResponseBody Object getStateDataForRegCont() {
			
			List<State> listState = stateDAO.getStateData();
			Map<String, Object> map = new HashMap<String, Object>();
			if(!listState.isEmpty()){
				map.put("list", listState);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;
		}
	
		@RequestMapping(value = "/EditRegularContract", method = RequestMethod.POST)
		public @ResponseBody Object EditRegularContract(@RequestBody String regContCode){
	    	
			RegularContract regularContract = new RegularContract();
			Map<String, Object> map = new HashMap<String, Object>();
      		List<RegularContract> list = regularContractDAO.getRegularContract(regContCode);
	  		if (!list.isEmpty()) {
	  			regularContract = list.get(0);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				map.put("regularContract",regularContract);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
	  	    return map;	
	  		}
		
		@RequestMapping(value="/saveproducttypeforRegCont",method = RequestMethod.POST)
		public @ResponseBody Object saveproducttype(@RequestBody ProductType productType){
			
			Map<String,String> map = new HashMap<String,String>();
			
			User currentUser = (User)httpSession.getAttribute("currentUser");
			
			productType.setUserCode(currentUser.getUserCode());
			productType.setbCode(currentUser.getUserBranchCode());
			int temp = productTypeDAO.saveProductTypeToDB(productType);
			
			if (temp>=0) {
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR );
			}
			return map;
		}
		
		@RequestMapping(value="/saveVehicleTypeForRegCont",method = RequestMethod.POST)
		public @ResponseBody Object saveVehicleType(@RequestBody VehicleType vehicleType){
					
			Map<String,String> map = new HashMap<String,String>();
			
			User currentUser = (User)httpSession.getAttribute("currentUser");
			
			vehicleType.setUserCode(currentUser.getUserCode());
			vehicleType.setbCode(currentUser.getUserBranchCode());

			int temp=vehicleTypeDAO.saveVehicleTypeToDB(vehicleType);
			
			if (temp>=0) {
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR );
			}
			return map;
			}
		
		@RequestMapping(value="/fetchRbkmListForRegCont",method = RequestMethod.GET)
		public @ResponseBody Object fetchRbkmList(){
		
		List<RateByKm> rateByKmList =  rbkmService.getAllRBKM();
		
		Map<String,Object> map = new HashMap<String,Object>();
		if(!rateByKmList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",rateByKmList);	
		}else{
			map.put("list",rateByKmList);
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
		}
	
		@RequestMapping(value="/addRbkmForRegCont",method = RequestMethod.POST)
		public @ResponseBody Object addRbkm(@RequestBody RateByKm rbkm){
			rbkmService.addRBKM(rbkm);
		
			Map<String,Object> map = new HashMap<String,Object>();
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			return map;	
		}
	
		@RequestMapping(value="/removeRbkmForRegCont",method = RequestMethod.POST)
		public @ResponseBody Object removeRbkm(@RequestBody RateByKm rbkm){
			rbkmService.deleteRBKM(rbkm);
		
			Map<String,Object> map = new HashMap<String,Object>();
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			return map;
		}
	
		@RequestMapping(value="/removeAllRbkmForRegCont",method = RequestMethod.POST)
		public @ResponseBody Object removeAllRbkm(){
			rbkmService.deleteAllRBKM();
		
			Map<String,Object> map = new HashMap<String,Object>();
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			return map;	
		}
	
		@RequestMapping(value="/fetchPbdListForRegCont",method = RequestMethod.GET)
		public @ResponseBody Object fetchpbdList(){
			
			List<PenaltyBonusDetention> pList = pbdService.getAllPBD();
		
			Map<String,Object> map = new HashMap<String,Object>();
			if(!pList.isEmpty()){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				map.put("list", pList);	
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			
			return map;
		}
	
		@RequestMapping(value="/addPbdForRegCont",method = RequestMethod.POST)
		public @ResponseBody Object addPbd(@RequestBody PenaltyBonusDetention penaltyBonusDetention){
			pbdService.addPBD(penaltyBonusDetention);
		
			Map<String,Object> map = new HashMap<String,Object>();
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			return map;	
		}
	
		@RequestMapping(value="/removePbdForRegCont",method = RequestMethod.POST)
		public @ResponseBody Object removePbd(@RequestBody PenaltyBonusDetention penaltyBonusDetention){
			pbdService.deletePBD(penaltyBonusDetention);

			Map<String,Object> map = new HashMap<String,Object>();
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			return map;
		}
	
		@RequestMapping(value="/removeAllPbdForRegCont",method = RequestMethod.POST)
		public @ResponseBody Object removeAllPbd(){
			pbdService.deleteAllPBD();
		
			Map<String,Object> map = new HashMap<String,Object>();
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			return map;	
		}	
		
		@RequestMapping(value = "/fetchCTSListForRC", method = RequestMethod.GET)
		public @ResponseBody Object fetchCTSListForRC() {
			System.out.println("enter into fetchCTSListForRC function");

			List<ContToStn> cList = contToStnService.getAllCTS();

			Map<String, Object> map = new HashMap<String, Object>();
			if(!cList.isEmpty()){
				map.put("list", cList);	
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else{
				map.put("list", cList);	
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			
			return map;
		}

		@RequestMapping(value = "/addCTSForRC", method = RequestMethod.POST)
		public @ResponseBody Object addCTSForRC(@RequestBody ContToStn contToStn) {

			System.out.println("enter into addCTSForRC function");
			contToStnService.addCTS(contToStn);
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			return map;
		}

		@RequestMapping(value = "/removeCTSForRC", method = RequestMethod.POST)
		public @ResponseBody Object removeCTSForRC(@RequestBody ContToStn contToStn) {

			System.out.println("enter into removeCTSForRC function");
			contToStnService.deleteCTS(contToStn);

			Map<String, Object> map = new HashMap<String, Object>();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			return map;
		}

		@RequestMapping(value = "/removeAllCTSForRC", method = RequestMethod.POST)
		public @ResponseBody Object removeAllCTSForRC() {

			System.out.println("enter into removeAllCTSForRC function");
			contToStnService.deleteAllCTS();
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			return map;
		}
		
		@RequestMapping(value = "/sendMetricTypeToRC", method = RequestMethod.POST)
		public @ResponseBody Object sendMetricTypeToRC(@RequestBody String metricType) {

			System.out.println("enter into sendMetricTypeToRC function");
			 metric = metricType;
			System.out.println("Metric type is ----"+metric);
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			return map;
		}
		
		@RequestMapping(value="/getContractList", method=RequestMethod.POST)
		public @ResponseBody Object getContractList(@RequestBody String custId){
			System.out.println("getContractList()");
			Map<String, Object> map = new HashMap<>();
			
			List<Map<String, Object>> contList = regularContractDAO.getContList(custId);
			
			System.out.println("custId: "+custId);
			System.out.println("contListSize: "+contList.size());
			
			if (!contList.isEmpty()) {
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				map.put("contList", contList);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			
			return map;
		}
		
		@RequestMapping(value="/getRate", method=RequestMethod.POST)
		public @ResponseBody Object getRate(@RequestBody String contCode){
			System.out.println("getRate()");
			Map<String, Object> map = new HashMap<>();
			
			List<Map<String, Object>> rateList = regularContractDAO.getRateList(contCode);
			
			if (!rateList.isEmpty()) {
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				map.put("rateList", rateList);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			
			return map;
		}
}

package com.mylogistics.controller;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.LhpvStatusDAO;
import com.mylogistics.services.ConstantsValues;

@Controller
public class CloseLhpvTempCntlr {

	@Autowired
	private LhpvStatusDAO lhpvStatusDAO;
	
	@RequestMapping(value = "/closeLhpvTemp" , method = RequestMethod.POST)  
	public @ResponseBody Object closeLhpvTemp(@RequestBody Map<String,Object> clientMap) {  
		System.out.println("Enter into getLhpvBalDet---->");
		Map<String, Object> map = new HashMap<>();	
		Date date = Date.valueOf((String) clientMap.get("date"));   
		int brhId = (int) clientMap.get("brhId");
		System.out.println("branch = "+brhId+"     date = "+date);
		if(date != null && brhId > 0){
			Map<String,Object> chkMap = new HashMap<String, Object>();
			chkMap = lhpvStatusDAO.chkTempLhpv(date, brhId);
			if(String.valueOf(chkMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.SUCCESS)){
				int res = lhpvStatusDAO.closeLhpvTemp(date, brhId);
				if(res > 0){
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put("msg",String.valueOf(chkMap.get("msg")));
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put("msg","SERVER ERROR");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
}

package com.mylogistics.controller;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.ArrivalReportDAO;
import com.mylogistics.DAO.ChallanDAO;
import com.mylogistics.constants.ArrivalReportCNTS;
import com.mylogistics.constants.CnmtCNTS;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.Challan;
import com.mylogistics.model.Employee;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class DisplayArrivalReportCntlr {

	@Autowired
	ArrivalReportDAO arrivalReportDAO;


	@Autowired
	private HttpSession httpSession;

	@RequestMapping(value="/getARIsViewNo",method = RequestMethod.POST)
	public @ResponseBody Object getChlnIsViewNo(){

		System.out.println("Controller called of getARIsViewNo");
		Map<String,Object> map = new HashMap<String,Object>();

		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();

		List<ArrivalReport> arList = arrivalReportDAO.getARisViewFalse(branchCode);
		//List<Map<String,Object>> finalArList = new ArrayList<Map<String,Object>>();
		System.out.println("Controller called after retrieving list---------"+arList.get(0).getArRepType());

		/*if(!arList.isEmpty()){
			for(int i=0;i<arList.size();i++){
				System.out.println("The values in the list is"+challans.get(i).getBranchCode());
				System.out.println(" -------------->"+challans.get(0).getChallanImage().length());
				Map<String,Object> arMap = new HashMap<String, Object>();
				arMap.put(ArrivalReportCNTS.AR_ID,arList.get(i).getArId());
				arMap.put(ArrivalReportCNTS.AR_CODE,arList.get(i).getArCode());
				arMap.put(ArrivalReportCNTS.AR_DATE,arList.get(i).getArDt());
				arMap.put(ArrivalReportCNTS.AR_RCV_WEIGHT,arList.get(i).getArRcvWt());
				arMap.put(ArrivalReportCNTS.AR_PACKAGE,arList.get(i).getArPkg());
				arMap.put(ArrivalReportCNTS.AR_UNLOADING,arList.get(i).getArUnloading());
				arMap.put(ArrivalReportCNTS.AR_CLAIM,arList.get(i).getArClaim());
				arMap.put(ArrivalReportCNTS.AR_DETENTION,arList.get(i).getArDetention());
				arMap.put(ArrivalReportCNTS.AR_OTHER,arList.get(i).getArOther());
				arMap.put(ArrivalReportCNTS.AR_LATE_DELIVERY,arList.get(i).getArLateDelivery());
				arMap.put(ArrivalReportCNTS.AR_LATE_ACKNOWLEDGEMENT,arList.get(i).getArLateAck());
				arMap.put(ArrivalReportCNTS.AR_REP_DATE,arList.get(i).getArRepDt());
				arMap.put(ArrivalReportCNTS.AR_REP_TIME,arList.get(i).getArRepTime());
				arMap.put(ArrivalReportCNTS.USER_CODE,arList.get(i).getUserCode());
				arMap.put(ArrivalReportCNTS.AR_CHLN_CODE,arList.get(i).getArchlnCode());
				arMap.put(ArrivalReportCNTS.CREATION_TS,arList.get(i).getCreationTS());
				arMap.put(ArrivalReportCNTS.USER_BRANCH_CODE,arList.get(i).getbCode());
				arMap.put(ArrivalReportCNTS.IS_VIEW,arList.get(i).isView());

				finalArList.add(arMap);
			}
		}*/

		if(!arList.isEmpty()){
			map.put("list",arList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);	
			System.out.println("map is ready for client");
		}else {
			System.out.println("enter into else -------------->");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		System.out.println("finally send the data");
		return map;
	}

	@RequestMapping(value="/downloadAr" , method = RequestMethod.POST)
	public void downloadCnmt(HttpServletRequest request , HttpServletResponse response )throws Exception {
		System.out.println("enter into downloadAr funntion");
		String arCode = request.getParameter("submitImageName");
		System.out.println("*************arCode = "+arCode);
		//String chlnCode = challan.getChlnCode();
		String fileName = arCode+".jpg";

		Blob arImage =arrivalReportDAO.getArImageByCode(arCode);

		//Blob chlnImage = challan.getChallanImage();

		InputStream in = arImage.getBinaryStream();
		int fileLength = in.available();
		System.out.println("####################size of downloading image = " + fileLength);
		String mimeType = "application/octet-stream";
		response.setContentType(mimeType);
		response.setContentLength(fileLength);
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", fileName);
		response.setHeader(headerKey, headerValue);
		OutputStream out = response.getOutputStream();
		byte[] buffer = new byte[2048];
		int bytesRead = -1;
		while ((bytesRead = in.read(buffer)) != -1) {
			out.write(buffer, 0, bytesRead);
		}
		in.close();
		out.flush();
		out.close(); 
		//return "success";
	}

	@RequestMapping(value="/getAllArCode",method = RequestMethod.POST)
	public @ResponseBody Object getAllEmpCodeForAdmin(){


		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();

		List<ArrivalReport> arCodeList = arrivalReportDAO.getAllArCodes(branchCode);
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(arCodeList.isEmpty())) {			
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("arCodeList",arCodeList);
		}else {
			map.put(ConstantsValues.RESULT,"No more arrival report to show...");
		}
		return map;	
	}


}

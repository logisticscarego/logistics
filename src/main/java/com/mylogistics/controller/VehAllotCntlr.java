package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.AddressDAO;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.VehicleMstrDAO;
import com.mylogistics.model.Address;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleMstr;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VMstrService;

@Controller
public class VehAllotCntlr {

	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
	private AddressDAO addressDAO;
	
	@Autowired
	private VehicleMstrDAO vehicleMstrDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@RequestMapping(value = "/getBrListFrVehA" , method = RequestMethod.POST)  
	public @ResponseBody Object getBrListFrCrnA() {  
		System.out.println("Enter into getBrListFrVehA---->");
		Map<String, Object> map = new HashMap<>();	
		List<Branch> branchList = branchDAO.getAllActiveBranches();
		if(!branchList.isEmpty()){
			map.put("brList",branchList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getOwnAddFrVehA" , method = RequestMethod.POST)  
	public @ResponseBody Object getOwnAddFrVehA() {  
		System.out.println("Enter into getOwnAddFrVehA---->");
		Map<String, Object> map = new HashMap<>();	
		List<Address> addList = addressDAO.getAddByType(ConstantsValues.VEH_OWN_ADD);
		if(!addList.isEmpty()){
			map.put("list",addList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getEmpListFrVehA" , method = RequestMethod.POST)  
	public @ResponseBody Object getEmpListFrVehA(@RequestBody Branch branch) {  
		System.out.println("Enter into getEmpListFrVehA---->");
		Map<String, Object> map = new HashMap<>();
		if(branch != null){
			List<Employee> empList = employeeDAO.getAllEmpForAdmin(branch.getBranchCode());
			if(!empList.isEmpty()){
				map.put("empList",empList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	@RequestMapping(value = "/getRtoAddList" , method = RequestMethod.POST)  
	public @ResponseBody Object getRtoAddList() {  
		System.out.println("Enter into getRtoAddList---->");
		Map<String, Object> map = new HashMap<>();
		List<Address> addList = addressDAO.getRTOAddress();
		if(!addList.isEmpty()){
			map.put("list",addList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	@RequestMapping(value = "/saveVms" , method = RequestMethod.POST)  
	public @ResponseBody Object saveVms(@RequestBody VMstrService vMstrService) {  
		System.out.println("Enter into saveVms---->");
		Map<String, Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		VehicleMstr vehicleMstr = vMstrService.getVehicleMstr();
		Address address = vMstrService.getAddress();
		Address ownAdd = vMstrService.getOwnAdd();
		
		if(ownAdd.getAddId() > 0){
			vehicleMstr.setVehOwnAdd(ownAdd);
		}else{
			ownAdd.setUserCode(currentUser.getUserCode());
			ownAdd.setbCode(currentUser.getUserBranchCode());
			ownAdd.setAddType(ConstantsValues.VEH_OWN_ADD);
			addressDAO.saveAddress(ownAdd);
			vehicleMstr.setVehOwnAdd(ownAdd);
		}
				
		if(address.getAddId() > 0){
			vehicleMstr.setVehRtoAdd(address);
		}else{
			address.setUserCode(currentUser.getUserCode());
			address.setbCode(currentUser.getUserBranchCode());
			address.setAddType(ConstantsValues.RTO_ADD);
			addressDAO.saveAddress(address);
			vehicleMstr.setVehRtoAdd(address);
		}
		
		Branch branch = vMstrService.getBranch();
		Employee employee = vMstrService.getEmployee();
		vehicleMstr.setbCode(currentUser.getUserBranchCode());
		vehicleMstr.setUserCode(currentUser.getUserCode());
		
		int temp = vehicleMstrDAO.saveVehicleMstr(branch , employee , vehicleMstr);
		if(temp > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
}

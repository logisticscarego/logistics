package com.mylogistics.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mylogistics.DAO.BrokerDAO;
import com.mylogistics.DAO.OwnerDAO;
import com.mylogistics.DAO.bank.PanDAO;
import com.mylogistics.model.Broker;
import com.mylogistics.model.BrokerImg;
import com.mylogistics.model.ChallanDetail;
import com.mylogistics.model.Owner;
import com.mylogistics.model.OwnerImg;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.ImageService;
import com.mylogistics.services.PanService;

@Controller
public class PanCntlr {

	private ImageService imageService = new ImageService();
	private static Logger logger = Logger.getLogger(PanCntlr.class);
	
	@Autowired
	private HttpSession httpSession;

	@Autowired
	private OwnerDAO ownerDAO;

	@Autowired
	private BrokerDAO brokerDAO;

	@Autowired
	private PanDAO panDAO;
	
	@Autowired
	private HttpServletRequest httpServletRequest;
	
	Blob blob;
	
	private String ownerPanImgPath = "/var/www/html/Erp_Image/Owner/Pan";
	private String ownerDecImgPath = "/var/www/html/Erp_Image/Owner/Dec";
	
	private String brokerPanImgPath = "/var/www/html/Erp_Image/Broker/Pan";
	private String brokerDecImgPath = "/var/www/html/Erp_Image/Broker/Dec";

	@RequestMapping(value = "/uploadPanImg", method = RequestMethod.POST)
	public @ResponseBody
	Object uploadPanImg(@RequestParam("file") MultipartFile file)
			throws Exception {
		System.out.println("enter into uploadPanImg funciton");
		byte[] byteArr = file.getBytes();
		Map<String, Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try {
			// blob = Hibernate.createBlob(fileContent);
			imageService.setImage(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ===" + blob.length());
			imageService.setImage(blob);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}

	@RequestMapping(value = "/uploadDecImg", method = RequestMethod.POST)
	public @ResponseBody
	Object upldOwnDecImg(@RequestParam("file") MultipartFile file)
			throws Exception {
		System.out.println("enter into upldOwnDecImg funciton");
		byte[] byteArr = file.getBytes();
		Map<String, Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try {
			// blob = Hibernate.createBlob(fileContent);
			imageService.setDecImg(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ===" + blob.length());
			imageService.setDecImg(blob);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}

	@RequestMapping(value = "/savePan", method = RequestMethod.POST)
	public @ResponseBody Object savePan(@RequestBody PanService panService) {
		User currentUser = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> map = new HashMap<String, Object>();
		
		logger.info("UserID = "+currentUser.getUserId()+" : Enter Into savePan()...");

		if (panService.getOwnId() > 0) {
			logger.info("going to write owner pan/dec image ...");
			
			float ownIntRate = -1;
			boolean isDecImg = false;
			boolean isPanImg = false;

			OwnerImg ownerImg = new OwnerImg();
			ownerImg.setbCode(currentUser.getUserBranchCode());
			ownerImg.setUserCode(currentUser.getUserCode());
			ownerImg.setOwnId(panService.getOwnId());

			if (imageService.getImage() != null) {
				ownerImg.setOwnPanImgPath(ownerPanImgPath);
				isPanImg = true;
				// Create directory if not exist
				try{
					Path path1 = Paths.get(ownerPanImgPath);
					if(! Files.exists(path1))
						Files.createDirectories(path1);					
				}catch(Exception e){
					logger.error("Error in creating directory savePan()...");
				}
				
			}
			if (imageService.getDecImg() != null) {
				ownerImg.setOwnDecImgPath(ownerDecImgPath);
				isDecImg = true;				
				// Create directory if not exist
				try{
					Path path2 = Paths.get(ownerDecImgPath);
					if(! Files.exists(path2))
						Files.createDirectories(path2);								
				}catch(Exception e){
					logger.error("Error in creating directory savePan()...");
				}
			}

			if (imageService.getImage() != null && imageService.getDecImg() != null)
				ownIntRate = 0;
			else if (imageService.getImage() != null) {
				if (panService.getPanNo().charAt(3) == 'P' || panService.getPanNo().charAt(3) == 'H' || panService.getPanNo().charAt(3) == 'J')
					ownIntRate = 1;
				else
					ownIntRate = 2;
			}

			map = ownerDAO.saveOwnPan(panService, ownerImg, ownIntRate, isPanImg, isDecImg, imageService.getImage(), imageService.getDecImg());
			
		} else if (panService.getBrkId() > 0) {
			logger.info("going to write broker pan/dec image ...");
			float brkIntRate = -1;
			boolean isDecImg = false;
			boolean isPanImg = false;

			BrokerImg brokerImg = new BrokerImg();
			brokerImg.setbCode(currentUser.getUserBranchCode());
			brokerImg.setUserCode(currentUser.getUserCode());
			brokerImg.setBrkId(panService.getBrkId());

			if (imageService.getImage() != null) {
				isPanImg = true;				
				brokerImg.setBrkPanImgPath(brokerPanImgPath);	
				// Create directory if not exist
				try{
					Path path1 = Paths.get(brokerPanImgPath);
					if(! Files.exists(path1))
						Files.createDirectories(path1);					
				}catch(Exception e){
					logger.error("Error in creating directory savePan().....");
				}
			}
			if (imageService.getDecImg() != null) {
				isDecImg = true;
				brokerImg.setBrkDecImgPath(brokerDecImgPath);
				// Create directory if not exist
				try{
					Path path1 = Paths.get(brokerDecImgPath);
					if(! Files.exists(path1))
						Files.createDirectories(path1);					
				}catch(Exception e){
					logger.error("Error in creating directory savePan().....");
				}
			}

			if (imageService.getImage() != null && imageService.getDecImg() != null)
				brkIntRate = 0;
			else if (imageService.getImage() != null) {
				if (panService.getPanNo().charAt(3) == 'P' || panService.getPanNo().charAt(3) == 'H' || panService.getPanNo().charAt(3) == 'J')
					brkIntRate = 1;
				else
					brkIntRate = 2;
			}

			map = brokerDAO.saveBrkPan(panService, brokerImg, brkIntRate,
					isPanImg, isDecImg, imageService.getImage(), imageService.getDecImg());
			
		}
		
		imageService.setImage(null);
		imageService.setDecImg(null);
		
		logger.info("Exit from /savePan()...");
		return map;
	}

	@RequestMapping(value = "/getChlnForPanValid", method = RequestMethod.POST)
	public @ResponseBody Object getChlnForPanValid(@RequestBody Map<String, String> initParam){ 
		logger.info("Enter into /getChlnFormPanValid() : BranchFaCode = "+initParam.get("branchCode")+" : From Date = "+initParam.get("dateFrom")+" : To Date = "+initParam.get("dateTo"));
		Map<String, Object> resultMap = new HashMap<>();
		
		String branchCode = initParam.get("branchCode");
		String panHldr = initParam.get("panHldr");
		String dateFrom = initParam.get("dateFrom");
		String dateTo = initParam.get("dateTo");		

		List<Map<String, Object>> chdOwnBrkValidList = panDAO.getChdOwnBrkValidList(branchCode, dateFrom, dateTo,panHldr);
		if(chdOwnBrkValidList.isEmpty()){
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "No such record found");
		}else{
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			resultMap.put("chlnDetPanValidList", chdOwnBrkValidList);
		}		
		logger.info("Exit from /getChlnFromPanValid()");
		return resultMap;
	}
	
	
	@RequestMapping(value = "/downloadPANImg", method = RequestMethod.POST)
	public void downloadImg(HttpServletRequest request, HttpServletResponse response){	
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if(user != null){
			logger.info("UserID = "+user.getUserId()+" : Enter into /downloadImg()");		
			try{
				if(blob != null){
					ServletOutputStream out = response.getOutputStream();
					out.write(blob.getBytes(1, (int) blob.length()));
					out.close();
					blob = null;
				}					
			}catch(Exception e){
				logger.error("Exception : "+e);
			}
		}		
		logger.info("UserID = "+user.getUserId()+" : Exit from /downloadImg()");
	}
	
	
	@RequestMapping(value = "/getImgForValidation", method = RequestMethod.POST)
    public @ResponseBody Object getImgForValidation(@RequestBody Map<String, Object> imgService)throws Exception {
        System.out.println("enter into getImage function");
        Map<String, Object> map = new HashMap<String, Object>();
        
        System.out.println("panHolder: "+imgService.get("panHolder"));
        System.out.println("imgId: "+imgService.get("imgId"));
        
        //Blob blob = null;
        
        if (String.valueOf(imgService.get("panHolder")).equalsIgnoreCase("owner")) {
			//image from owner
        	blob = ownerDAO.getOwnPanImg(Integer.parseInt(String.valueOf(imgService.get("imgId"))));
		}else {
			//image from broker
			blob = brokerDAO.getBrkPanImg(Integer.parseInt(String.valueOf(imgService.get("imgId"))));
		}
        
        if(blob == null){	
        	map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
        	map.put("msg", "Image not found !");					
		}else
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
        
        /*InputStream in = blob.getBinaryStream();
        
        int blobLength = (int) blob.length();  
        byte[] blobAsBytes = blob.getBytes(1, blobLength);
        
        System.out.println("&&&&&&&&&&&&&&&&&&&"+blobAsBytes.length);
        map.put("panImage",blobAsBytes);*/
        return map;
    }
	
	@RequestMapping(value = "/panValidated", method = RequestMethod.POST)
	public @ResponseBody Object panValidated(@RequestBody Map<String, Object> panValidatedMap){ 
		System.out.println("PanCntlr.panValidated()");
		Map<String, Object> map = new HashMap<>();
		
		int temp = panDAO.validatedPanNo(panValidatedMap);
		
		if (temp>0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}

	public ImageService getImageService() {
		return imageService;
	}

	public void setImageService(ImageService imageService) {
		this.imageService = imageService;
	}

	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		PanCntlr.logger = logger;
	}

	public HttpSession getHttpSession() {
		return httpSession;
	}

	public void setHttpSession(HttpSession httpSession) {
		this.httpSession = httpSession;
	}

	public OwnerDAO getOwnerDAO() {
		return ownerDAO;
	}

	public void setOwnerDAO(OwnerDAO ownerDAO) {
		this.ownerDAO = ownerDAO;
	}

	public BrokerDAO getBrokerDAO() {
		return brokerDAO;
	}

	public void setBrokerDAO(BrokerDAO brokerDAO) {
		this.brokerDAO = brokerDAO;
	}

	public PanDAO getPanDAO() {
		return panDAO;
	}

	public void setPanDAO(PanDAO panDAO) {
		this.panDAO = panDAO;
	}
	
	
		
}

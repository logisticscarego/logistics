package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchSStatsDAO;
import com.mylogistics.DAO.BranchStockDispatchDAO;
import com.mylogistics.DAO.BranchStockLeafDetDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.UserDAO;
import com.mylogistics.model.BranchStockDispatch;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class Admin_DashboardCntlr {
	
	@Autowired
	private BranchStockDispatchDAO branchStockDispatchDAO;
	
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchSStatsDAO branchSStatsDAO;
	
	@Autowired
	private BranchStockLeafDetDAO branchStockLeafDetDAO;
	
	public static Logger logger = Logger.getLogger(Admin_DashboardCntlr.class);

	@RequestMapping(value = "/getBranchStockDispatchData", method = RequestMethod.POST)
	public @ResponseBody Object getBranchStockDispatchData(){
		logger.info("Enter into /getBranchStockDispatchData()");
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		List<BranchStockDispatch> bList = branchStockDispatchDAO.getBranchStockDispatchData(currentUser.getUserBranchCode());
			
		if(!bList.isEmpty())
		{
			map.put("list", bList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		logger.info("Exit from /getBranchStockDispatchData()");
		return map;
	}
	
	@RequestMapping(value = "/getOperatorCodeForAdmin", method = RequestMethod.POST)
	public @ResponseBody Object getOperatorCodeForAdmin() {
		logger.info("Enter into /getOperatorCodeForAdmin()");		
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		
		List<User> operatorList = userDAO.getSubBranchOperator(currentUser.getUserBranchCode());	
		if(!operatorList.isEmpty()){
			map.put("list", operatorList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		logger.info("Exit from /getOperatorCodeForAdmin()");
		return map;
	}
	
	@RequestMapping(value = "/getOperatorCodeForStock", method = RequestMethod.POST)
	public @ResponseBody Object getOperatorCodeForStock(@RequestBody String branchCode) {
		logger.info("Enter into /getOperatorCodeForStock()");		
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = (User)httpSession.getAttribute("currentUser");	
		List<User> operatorList = userDAO.getSubBranchOperator(branchCode);	
		if(!operatorList.isEmpty()){
			map.put("list", operatorList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		logger.info("Exit from /getOperatorCodeForStock()");
		return map;
	}
	
	@RequestMapping(value = "/confirmDispatch", method = RequestMethod.POST)
	public @ResponseBody Object confirmDispatch(@RequestBody String bSDCode) {
		System.out.println("enter into confirmDispatch function----->"+bSDCode);
		
		Map<String, Object> map = new HashMap<String, Object>();
		int temp = branchStockDispatchDAO.stockRecieved(bSDCode);
		if(temp >0){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	/*@RequestMapping(value = "/getCnmtDaysLast", method = RequestMethod.POST)
	public @ResponseBody Object getCnmtDaysLast() {
		
		 Date date = new Date();
		 Calendar calendar =Calendar.getInstance();
		 calendar.setTime(date);
		 calendar.add(Calendar.MONTH, -1);
		 Date endDate = calendar.getTime();
		 int cnmtLeft=0;
		 User currentUser = (User)httpSession.getAttribute("currentUser");
		
		 Map<String, Object> map = new HashMap<String, Object>();
		int cnmtPerDayUsed = branchSStatsDAO.getLastMonthPerDayUsedCnmt(date,endDate);
		if(cnmtPerDayUsed>0){
			 cnmtLeft = branchStockLeafDetDAO.getLeftCnmts(currentUser.getUserBranchCode());
		}
			int daysLeft=cnmtLeft/cnmtPerDayUsed;
			map.put("daysLeft", daysLeft);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}*/
	
		/*@RequestMapping(value = "/getAvgMonthlyUsageForCnmt", method = RequestMethod.POST)
		public @ResponseBody Object getAvgMonthlyUsageForCnmt() {
		
		 System.out.println("------Enter into getAvgMonthlyUsage function");
		 
		 Date todayDate = new Date();
		 Date firstEntryDate = branchSStatsDAO.getFirstEntryDateOfCnmt();
		 
		 Calendar calendar =Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.MONTH, -1);
			Date oneMonthPrevDate = calendar.getTime();
			
			calendar.add(Calendar.MONTH, -1);
			Date twoMonthPrevDate = calendar.getTime();
			
			calendar.add(Calendar.MONTH, -1);
			Date threeMonthPrevDate = calendar.getTime();
			
			double days = (double)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
			
			double cnmtForOneMnth = branchSStatsDAO.getNoOfCnmt(todayDate, oneMonthPrevDate);
			double cnmtForScndMnth = branchSStatsDAO.getNoOfCnmt(oneMonthPrevDate, twoMonthPrevDate);
			double cnmtForThirdMnth = branchSStatsDAO.getNoOfCnmt(twoMonthPrevDate, threeMonthPrevDate);
			double cnmtBwFrstAndScndMnth = branchSStatsDAO.getNoOfCnmt(firstEntryDate, twoMonthPrevDate);
			double cnmtBwFrstAndOneMnth=branchSStatsDAO.getNoOfCnmt(firstEntryDate, oneMonthPrevDate);
			double cnmtBwTodayAndFrstDate=branchSStatsDAO.getNoOfCnmt(todayDate, firstEntryDate);
			
			double average=0.00;
			
			if(days>=90){
				 average = ((cnmtForOneMnth*3) + (cnmtForScndMnth*2) + cnmtForThirdMnth)/6;
			}
			else if(days>=60 && days<90){
				int daysBwFrstAndScndMnth = (int)( (twoMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));		
				
				  average = (((cnmtForOneMnth*3) + (cnmtForScndMnth*2) + (cnmtBwFrstAndScndMnth/daysBwFrstAndScndMnth))*30)/6;
			}
			else if(days>=30 && days<60){
				
				int daysBwFrstAndOneMnth = (int)( (oneMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				 average = (((cnmtForOneMnth*2) +  (cnmtBwFrstAndOneMnth/daysBwFrstAndOneMnth))*30)/3;
			}
			else if(days<30){
				int daysBwFrstAndToday = (int)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));	
				 average = ((cnmtBwTodayAndFrstDate/daysBwFrstAndToday)*30);
			}
		
			Map<String,Object> map = new HashMap<String, Object>();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("averageMonthlyUsage",average);
		return map;
	}
	*/
		/*@RequestMapping(value = "/getChlnDaysLast", method = RequestMethod.POST)
		public @ResponseBody Object getChlnDaysLast() {
			
			 Date date = new Date();
			 Calendar calendar =Calendar.getInstance();
			 calendar.setTime(date);
			 calendar.add(Calendar.MONTH, -1);
			 Date endDate = calendar.getTime();
			 int chlnLeft=0;
			 User currentUser = (User)httpSession.getAttribute("currentUser");
			
			 Map<String, Object> map = new HashMap<String, Object>();
			int chlnPerDayUsed = branchSStatsDAO.getLastMonthPerDayUsedChln(date,endDate);
			if(chlnPerDayUsed>0){
				 chlnLeft = branchStockLeafDetDAO.getLeftChlns(currentUser.getUserBranchCode());
			}
				int daysLeft=chlnLeft/chlnPerDayUsed;
				map.put("daysLeft", daysLeft);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			return map;
		}*/
		
		/*@RequestMapping(value = "/getAvgMonthlyUsageForChln", method = RequestMethod.POST)
		public @ResponseBody Object getAvgMonthlyUsageForChln() {
		
		 System.out.println("------Enter into getAvgMonthlyUsage function");
		 
		 Date todayDate = new Date();
		 Date firstEntryDate = branchSStatsDAO.getFirstEntryDateOfChln();
		 
		 Calendar calendar =Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.MONTH, -1);
			Date oneMonthPrevDate = calendar.getTime();
			
			calendar.add(Calendar.MONTH, -1);
			Date twoMonthPrevDate = calendar.getTime();
			
			calendar.add(Calendar.MONTH, -1);
			Date threeMonthPrevDate = calendar.getTime();
			
			double days = (double)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
			
			double chlnForOneMnth = branchSStatsDAO.getNoOfChln(todayDate, oneMonthPrevDate);
			double chlnForScndMnth = branchSStatsDAO.getNoOfChln(oneMonthPrevDate, twoMonthPrevDate);
			double chlnForThirdMnth = branchSStatsDAO.getNoOfChln(twoMonthPrevDate, threeMonthPrevDate);
			double chlnBwFrstAndScndMnth = branchSStatsDAO.getNoOfChln(firstEntryDate, twoMonthPrevDate);
			double chlnBwFrstAndOneMnth=branchSStatsDAO.getNoOfChln(firstEntryDate, oneMonthPrevDate);
			double chlnBwTodayAndFrstDate=branchSStatsDAO.getNoOfChln(todayDate, firstEntryDate);
			
			double average=0.00;
			
			if(days>=90){
				 average = ((chlnForOneMnth*3) + (chlnForScndMnth*2) + chlnForThirdMnth)/6;
			}
			else if(days>=60 && days<90){
				int daysBwFrstAndScndMnth = (int)( (twoMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));		
				
				  average = (((chlnForOneMnth*3) + (chlnForScndMnth*2) + (chlnBwFrstAndScndMnth/daysBwFrstAndScndMnth))*30)/6;
			}
			else if(days>=30 && days<60){
				
				int daysBwFrstAndOneMnth = (int)( (oneMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				 average = (((chlnForOneMnth*2) +  (chlnBwFrstAndOneMnth/daysBwFrstAndOneMnth))*30)/3;
			}
			else if(days<30){
				int daysBwFrstAndToday = (int)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));	
				 average = ((chlnBwTodayAndFrstDate/daysBwFrstAndToday)*30);
			}
		
			Map<String,Object> map = new HashMap<String, Object>();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("averageMonthlyUsage",average);
		return map;
	}
		*/
		/*@RequestMapping(value = "/getSedrDaysLast", method = RequestMethod.POST)
		public @ResponseBody Object getSedrDaysLast() {
			
			 Date date = new Date();
			 Calendar calendar =Calendar.getInstance();
			 calendar.setTime(date);
			 calendar.add(Calendar.MONTH, -1);
			 Date endDate = calendar.getTime();
			 int sedrLeft=0;
			 User currentUser = (User)httpSession.getAttribute("currentUser");
			
			 Map<String, Object> map = new HashMap<String, Object>();
			int sedrPerDayUsed = branchSStatsDAO.getLastMonthPerDayUsedChln(date,endDate);
			if(sedrPerDayUsed>0){
				 sedrLeft = branchStockLeafDetDAO.getLeftSedrs(currentUser.getUserBranchCode());
			}
				int daysLeft=sedrLeft/sedrPerDayUsed;
				map.put("daysLeft", daysLeft);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			return map;
		}
		
		@RequestMapping(value = "/getAvgMonthlyUsageForSedr", method = RequestMethod.POST)
		public @ResponseBody Object getAvgMonthlyUsageForSedr() {
		
		 System.out.println("------Enter into getAvgMonthlyUsage function");
		 
		 Date todayDate = new Date();
		 Date firstEntryDate = branchSStatsDAO.getFirstEntryDateOfChln();
		 
		 Calendar calendar =Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.MONTH, -1);
			Date oneMonthPrevDate = calendar.getTime();
			
			calendar.add(Calendar.MONTH, -1);
			Date twoMonthPrevDate = calendar.getTime();
			
			calendar.add(Calendar.MONTH, -1);
			Date threeMonthPrevDate = calendar.getTime();
			
			double days = (double)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
			
			double sedrForOneMnth = branchSStatsDAO.getNoOfChln(todayDate, oneMonthPrevDate);
			double sedrForScndMnth = branchSStatsDAO.getNoOfChln(oneMonthPrevDate, twoMonthPrevDate);
			double sedrForThirdMnth = branchSStatsDAO.getNoOfChln(twoMonthPrevDate, threeMonthPrevDate);
			double sedrBwFrstAndScndMnth = branchSStatsDAO.getNoOfChln(firstEntryDate, twoMonthPrevDate);
			double sedrBwFrstAndOneMnth=branchSStatsDAO.getNoOfChln(firstEntryDate, oneMonthPrevDate);
			double sedrBwTodayAndFrstDate=branchSStatsDAO.getNoOfChln(todayDate, firstEntryDate);
			
			double average=0.00;
			
			if(days>=90){
				 average = ((sedrForOneMnth*3) + (sedrForScndMnth*2) + sedrForThirdMnth)/6;
			}
			else if(days>=60 && days<90){
				int daysBwFrstAndScndMnth = (int)( (twoMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));		
				
				  average = (((sedrForOneMnth*3) + (sedrForScndMnth*2) + (sedrBwFrstAndScndMnth/daysBwFrstAndScndMnth))*30)/6;
			}
			else if(days>=30 && days<60){
				
				int daysBwFrstAndOneMnth = (int)( (oneMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				 average = (((sedrForOneMnth*2) +  (sedrBwFrstAndOneMnth/daysBwFrstAndOneMnth))*30)/3;
			}
			else if(days<30){
				int daysBwFrstAndToday = (int)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));	
				 average = ((sedrBwTodayAndFrstDate/daysBwFrstAndToday)*30);
			}
		
			Map<String,Object> map = new HashMap<String, Object>();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("averageMonthlyUsage",average);
		return map;
	}*/
}

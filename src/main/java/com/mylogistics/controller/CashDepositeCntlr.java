package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;

@Controller
public class CashDepositeCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@RequestMapping(value = "/getCashDepositeDet" , method = RequestMethod.POST)  
	public @ResponseBody Object getCashDepositeDet() {  
		System.out.println("Enter into getCashDepositeDet---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}
	
	
	@RequestMapping(value = "/submitCashD" , method = RequestMethod.POST)  
	public @ResponseBody Object submitCashD(@RequestBody VoucherService voucherService) {  
		System.out.println("Enter into submitCashD---->");
		Map<String, Object> map = new HashMap<>();	
		if(voucherService.getVoucherType().equalsIgnoreCase(ConstantsValues.CASH_DEPOSITE_VOUCH)){
			 map = saveCashDeposite(voucherService);
			 int vhNo = (int) map.get("vhNo");
			if(vhNo>0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}
		return map;
	}
	
	
	public Map<String,Object> saveCashDeposite(VoucherService voucherService){
		System.out.println("Enter into saveCashDeposite----->>>");
		User currentUser =	(User)httpSession.getAttribute("currentUser");
		Map<String,Object> map = new HashMap<String,Object>();
			String bankCode = voucherService.getBankCode();
			List<BankMstr> bankMList = bankMstrDAO.getBankByBankCode(bankCode);
			List<FAMaster> famList = faMasterDAO.getFAMByFaName(ConstantsValues.CASH_IN_HAND);
			
			Date date = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			long timesInMillis = calendar.getTimeInMillis();
			String tvNo = String.valueOf(timesInMillis);
			String payMode = "C";
			
			if(!bankMList.isEmpty()){
					//String tvNo = "0000000000";
				
					int res = bankMstrDAO.incBnkBal(bankMList.get(0).getBnkId(),voucherService.getAmount());
				
				
					CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
					if(cashStmtStatus.getCssId() > 0){
						CashStmt cashStmt = new CashStmt();
						cashStmt.setbCode(currentUser.getUserBranchCode());
						cashStmt.setUserCode(currentUser.getUserCode());
						cashStmt.setCsDescription(voucherService.getDesc());
						cashStmt.setCsDrCr('D');
						cashStmt.setCsAmt(voucherService.getAmount());
						cashStmt.setCsType(voucherService.getVoucherType());
						cashStmt.setCsTvNo(tvNo);
						cashStmt.setCsVouchType("cash");
						cashStmt.setCsFaCode(bankCode);
						cashStmt.setPayMode(payMode);
						
						java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
						cashStmt.setCsDt(sqlDate);
			
						map = cashStmtStatusDAO.updateCSSByCS(cashStmtStatus.getCssId() , cashStmt);
						
						
						CashStmt cashStmt1 = new CashStmt();
						cashStmt1.setbCode(currentUser.getUserBranchCode());
						cashStmt1.setUserCode(currentUser.getUserCode());
						cashStmt1.setCsDescription(voucherService.getDesc());
						cashStmt1.setCsDrCr('C');
						cashStmt1.setCsAmt(voucherService.getAmount());
						cashStmt1.setCsType(voucherService.getVoucherType());
						cashStmt1.setCsTvNo(tvNo);
						cashStmt1.setCsVouchType("bank");
						cashStmt1.setCsFaCode(famList.get(0).getFaMfaCode());
						cashStmt1.setCsVouchNo((int)map.get("vhNo"));
						//java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
						cashStmt1.setCsDt(sqlDate);
						cashStmt1.setPayMode(payMode);
						
						map = cashStmtStatusDAO.updateCSSByCS(cashStmtStatus.getCssId() , cashStmt1);
						
						
						
						/*CashStmt cashStmt2 = new CashStmt();
						cashStmt2.setbCode(currentUser.getUserBranchCode());
						cashStmt2.setUserCode(currentUser.getUserCode());
						cashStmt2.setCsDescription(voucherService.getDesc());
						cashStmt2.setCsDrCr('D');
						cashStmt2.setCsAmt(voucherService.getAmount());
						cashStmt2.setCsType(voucherService.getVoucherType());
						//cashStmt.setCsTvNo(tvNo);
						cashStmt2.setCsVouchType("bank");
						cashStmt2.setCsFaCode(bankCode);
						cashStmt2.setCsVouchNo((int)map.get("vhNo"));
						//java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
						cashStmt2.setCsDt(sqlDate);
			
						map = cashStmtStatusDAO.updateCSSByCS(cashStmtStatus.getCssId() , cashStmt2);
						if(vhNo > 0){
							map.put("vhNo",vhNo);
							map.put("tvNo",tvNo);
							return map;
						}*/
						return map;
					}else{
						return map;
					}
			}else{
				return map;
			}
	}
	
	//TODO Back Data
	@RequestMapping(value = "/getCashDepositeDetBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getCashDepositeDetBack() {  
		System.out.println("Enter into getCashDepositeDetBack---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}

	
}

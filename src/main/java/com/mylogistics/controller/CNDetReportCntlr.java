package com.mylogistics.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.AddressDAO;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BrokerDAO;
import com.mylogistics.DAO.ChallanDAO;
import com.mylogistics.DAO.ChallanDetailDAO;
import com.mylogistics.DAO.CnmtDAO;
import com.mylogistics.DAO.Cnmt_ChallanDAO;
import com.mylogistics.DAO.ContPersonDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.OwnerDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.DAO.report.ReportDAO;
import com.mylogistics.model.Address;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Broker;
import com.mylogistics.model.Challan;
import com.mylogistics.model.ChallanDetail;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.Cnmt_Challan;
import com.mylogistics.model.ContPerson;
import com.mylogistics.model.Customer;
import com.mylogistics.model.Owner;
import com.mylogistics.model.Station;
import com.mylogistics.services.CnDetailService;
import com.mylogistics.services.ConstantsValues;

@Controller
public class CNDetReportCntlr {

	@Autowired
	private Cnmt_ChallanDAO cnmt_ChallanDAO;
	
	@Autowired
	private CnmtDAO cnmtDAO;
	
	@Autowired
	private ChallanDAO challanDAO;
	
	@Autowired
	private OwnerDAO ownerDAO;
	
	@Autowired
	private BrokerDAO brokerDAO;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private StationDAO stationDAO;
	
	@Autowired
	private ChallanDetailDAO challanDetailDAO;
	
	@Autowired
	private ContPersonDAO contPersonDAO;
	
	@Autowired
	private AddressDAO addressDAO;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private ReportDAO reportDAO;
	
	public List<CnDetailService> getAllCnDetail(){
		System.out.println("enter into getAllCnDetail function");
		List<Cnmt_Challan> ccList  = cnmt_ChallanDAO.getAllCnChln();
		List<Cnmt> cnmtList = cnmtDAO.getAllCnmt();
		List<Challan> chlnList = challanDAO.getAllChallan();
		List<ChallanDetail> chldList = challanDetailDAO.getChlnDet();
		List<Owner> ownList = ownerDAO.getOwner();
		List<Broker> brkList = brokerDAO.getBroker();
		List<ContPerson> cpList = contPersonDAO.getContPerson();
		List<Customer> custList = customerDAO.getAllCustomer();
		List<Station> stntList = stationDAO.getStationData();
 		System.out.println("size of ccList ===> "+ccList.size());
 		System.out.println("size of cnmtList ===> "+cnmtList.size());
 		System.out.println("size of chlnList ===> "+chlnList.size());
 		
 		List<CnDetailService> cnDetList = new ArrayList<>();
 		
 		if(!ccList.isEmpty()){
 			for(int i=0;i<ccList.size();i++){
 				CnDetailService cnDetailService = new CnDetailService();
 				for(int j=0;j<cnmtList.size();j++){
 					if(ccList.get(i).getCnmtId() == cnmtList.get(j).getCnmtId()){
 						cnDetailService.setCnmtNo(cnmtList.get(j).getCnmtCode());
 						cnDetailService.setCustCode(cnmtList.get(j).getCustCode());
 						cnDetailService.setCnmtDt(cnmtList.get(j).getCnmtDt());
 						cnDetailService.setBrhCode(cnmtList.get(j).getBranchCode());
 	
 						for(int k=0;k<custList.size();k++){
 							
 							if(cnDetailService.getConsignor() == null){
 								if(cnmtList.get(j).getCnmtConsignor() != null && custList.get(k).getCustCode() != null){
 	 								if(cnmtList.get(j).getCnmtConsignor().equalsIgnoreCase(custList.get(k).getCustCode())){
 	 	 								cnDetailService.setConsignor(custList.get(k).getCustName());
 	 	 							}
 	 							}
 							}
 							
 							
 							if(cnDetailService.getConsignee() == null){
 								if(cnmtList.get(j).getCnmtConsignee() != null && custList.get(k).getCustCode() != null){
 	 								if(cnmtList.get(j).getCnmtConsignee().equalsIgnoreCase(custList.get(k).getCustCode())){
 	 	 								cnDetailService.setConsignee(custList.get(k).getCustName());
 	 	 							}
 	 							}
 							}
 							
 		
 							if(cnDetailService.getBlngParty() == null){
 								if(cnmtList.get(j).getCustCode() != null && custList.get(k).getCustCode() != null){
 	 								if(cnmtList.get(j).getCustCode().equalsIgnoreCase(custList.get(k).getCustCode())){
 	 	 								cnDetailService.setBlngParty(custList.get(k).getCustName());
 	 	 							}
 	 							}
 							}
 							
 							if(cnDetailService.getConsignor() != null && cnDetailService.getConsignee() != null && cnDetailService.getBlngParty() != null){
 								System.out.println("Exit from custlist loop");
 								break;
 							}
 							
 						}
 						
 						if(cnmtList.get(j).getCnmtActualWt() > 0){
 							cnDetailService.setActWt(cnmtList.get(j).getCnmtActualWt() / ConstantsValues.kgInTon);
 						}else{
 							cnDetailService.setActWt(0);
 						}
 						
 						if(cnmtList.get(j).getCnmtGuaranteeWt() > 0){
 							cnDetailService.setWtChrg(cnmtList.get(j).getCnmtGuaranteeWt() / ConstantsValues.kgInTon);
 						}else{
 							cnDetailService.setWtChrg(0);
 						}
 						
 						cnDetailService.setNoOfPckg(cnmtList.get(j).getCnmtNoOfPkg());
 						cnDetailService.setInvList(cnmtList.get(j).getCnmtInvoiceNo());
 						cnDetailService.setVog(cnmtList.get(j).getCnmtVOG());
 						
 						for(int m=0;m<stntList.size();m++){
 							
 							if(cnDetailService.getFrStn() == null){
 								if(cnmtList.get(j).getCnmtFromSt() != null && stntList.get(m).getStnCode() != null){
 	 								if(cnmtList.get(j).getCnmtFromSt().equalsIgnoreCase(stntList.get(m).getStnCode())){
 	 	 								cnDetailService.setFrStn(stntList.get(m).getStnName());
 	 	 							}
 	 							}
 							}
 							
 							if(cnDetailService.getToStn() == null){
 								if(cnmtList.get(j).getCnmtToSt() != null && stntList.get(m).getStnCode() != null){
 	 								if(cnmtList.get(j).getCnmtToSt().equalsIgnoreCase(stntList.get(m).getStnCode())){
 	 	 								cnDetailService.setToStn(stntList.get(m).getStnName());
 	 	 							}
 	 							}
 							}
 							
 							if(cnDetailService.getFrStn() != null && cnDetailService.getToStn() != null){
 								System.out.println("exit from stnList loop");
 								break;
 							}
 							
 						}
 						cnDetailService.setExpDtOfDly(cnmtList.get(j).getCnmtDtOfDly());
 						System.out.println("cnmt of "+j+" no in cnmtList");
 						
 						if(cnmtList.get(j).getCnmtRate() >= 0){
 							cnDetailService.setPrMtRt(cnmtList.get(j).getCnmtRate() * 1000);
 						}else{
 							cnDetailService.setPrMtRt(0);
 						}
 		
 						System.out.println("*****************");
 					}
 				}
 				
 				for(int n=0;n<chlnList.size();n++){
 					if(ccList.get(i).getChlnId() == chlnList.get(n).getChlnId()){
 						cnDetailService.setChlnNo(chlnList.get(n).getChlnCode());
 						cnDetailService.setChlnDt(chlnList.get(n).getChlnDt());
 						if(chlnList.get(n).getChlnFreight() != null){
 							cnDetailService.setLryHire(Double.parseDouble(chlnList.get(n).getChlnFreight()));
 						}else{
 							cnDetailService.setLryHire(0);
 						}
 						System.out.println("***************** 1");
 						
 						if(chlnList.get(n).getChlnFreight() != null && chlnList.get(n).getChlnTotalFreight() >= 0){
 							cnDetailService.setAnyOthChrg(Float.parseFloat(chlnList.get(n).getChlnFreight()) - new Double(chlnList.get(n).getChlnTotalFreight()).floatValue());
 						}else{
 							cnDetailService.setAnyOthChrg(0);
 						}
 						
 						System.out.println("***************** 2");
 						cnDetailService.setTotHireChrg(chlnList.get(n).getChlnTotalFreight());
 						cnDetailService.setAdv(Double.parseDouble(chlnList.get(n).getChlnAdvance()));
 						cnDetailService.setBal(chlnList.get(n).getChlnBalance());
 						
 						if(chlnList.get(n).getChlnLryRate() != null){
 							cnDetailService.setPrMtCost(Double.parseDouble(chlnList.get(n).getChlnLryRate()) * 1000);
 						}else{
 							cnDetailService.setPrMtCost(0);
 						}
 						
 						System.out.println("***************** 3");
 						boolean chldFlag = false;
 						
 						for(int p=0;p<chldList.size();p++){
 							System.out.println("chlnList.get(n).getChlnCode() === "+chlnList.get(n).getChlnCode());
 							System.out.println("chldList.get(p).getChdChlnCode() === "+chldList.get(p).getChdChlnCode());
 							
 							if(cnDetailService.getDriverName() == null){
 	 							if(chlnList.get(n).getChlnCode() != null && chldList.get(p).getChdChlnCode() != null){
 	 	 							if(chlnList.get(n).getChlnCode().equalsIgnoreCase(chldList.get(p).getChdChlnCode())){
 	 	 								cnDetailService.setDriverName(chldList.get(p).getChdDvrName());
 	 	 								cnDetailService.setDrvCntNo(chldList.get(p).getChdDvrMobNo());
 	 	 								if(cnDetailService.getBrkName() == null){
 	 	 									for(int q=0;q<brkList.size();q++){
 	 	 	 									if(chldList.get(p).getChdBrCode() != null && brkList.get(q).getBrkCode() != null){
 	 	 	 										if(chldList.get(p).getChdBrCode().equalsIgnoreCase(brkList.get(q).getBrkCode())){
 	 	 	 	 										cnDetailService.setBrkName(brkList.get(q).getBrkName());
 	 	 	 	 										for(int x=0;x<cpList.size();x++){
 	 	 	 	 											//if(cnDetailService.getBrkCntList() == null || cnDetailService.getBrkCntList().isEmpty()){
	 	 	 	 	 											if(brkList.get(q).getBrkCode() != null && cpList.get(x).getCpRefCode() != null){
	 	 	 	 	 												if(brkList.get(q).getBrkCode().equalsIgnoreCase(cpList.get(x).getCpRefCode())){
	 	 	 	 	 	 												cnDetailService.setBrkCntList(cpList.get(x).getCpMobile());
	 	 	 	 	 	 												break;
	 	 	 	 	 	 											}
	 	 	 	 	 											}
 	 	 	 	 											//}
 	 	 	 	 										}
 	 	 	 	 									}
 	 	 	 									}
 	 	 	 									
 	 	 	 									if(cnDetailService.getBrkName() != null){
 	 	 	 										break;
 	 	 	 									}
 	 	 	 								}
 	 	 								}
 	 	 								
 	 	 								if(cnDetailService.getOwnName() == null){
 	 	 	 								for(int r=0;r<ownList.size();r++){
 	 	 	 									System.out.println(p + "chldList.get(p).getChdOwnCode() == "+chldList.get(p).getChdOwnCode());
 	 	 	 									System.out.println(r + "ownList.get(r).getOwnCode()) == "+ownList.get(r).getOwnCode());
 	 	 	 									if(chldList.get(p).getChdOwnCode() != null && ownList.get(r).getOwnCode() != null){
 	 	 	 										if(chldList.get(p).getChdOwnCode().equalsIgnoreCase(ownList.get(r).getOwnCode())){
 	 	 	 	 										cnDetailService.setOwnName(ownList.get(r).getOwnName());
 	 	 	 	 										cnDetailService.setOwnPan(ownList.get(r).getOwnPanNo());
 	 	 	 	 										for(int x=0;x<cpList.size();x++){
 	 	 	 	 											//if(cnDetailService.getOwnCntList() == null || cnDetailService.getOwnCntList().isEmpty()){
	 	 	 	 	 											if(ownList.get(r).getOwnCode() != null && cpList.get(x).getCpRefCode() != null){
	 	 	 	 	 												if(ownList.get(r).getOwnCode().equalsIgnoreCase(cpList.get(x).getCpRefCode())){
	 	 	 	 	 	 												cnDetailService.setOwnCntList(cpList.get(x).getCpMobile());
	 	 	 	 	 	 												break;
	 	 	 	 	 	 											}
	 	 	 	 	 											}
 	 	 	 	 											//}
 	 	 	 	 										}
 	 	 	 	 										List<Address> addList = new ArrayList<>();
 	 	 	 	 										if(ownList.get(r).getOwnCode() != null){
 	 	 	 	 											addList = addressDAO.getAddressData(ownList.get(r).getOwnCode());
 	 	 	 	 										}
 	 	 	 	 										if(!addList.isEmpty()){
 	 	 	 	 											for(int y=0;y<addList.size();y++){
 	 	 	 	 												if(addList.get(y).getAddType() != null){
 	 	 	 	 													if(addList.get(y).getAddType().equalsIgnoreCase(ConstantsValues.OTH_ADD)){
 	 	 	 	 	 													String othAdd = addList.get(y).getCompleteAdd() +", "+ addList.get(y).getAddCity() + "(" + addList.get(y).getAddPin() + "), " + addList.get(y).getAddState();
 	 	 	 	 	 													cnDetailService.setOwnAdd(othAdd);
 	 	 	 	 	 													break;
 	 	 	 	 	 												}
 	 	 	 	 												}
 	 	 	 	 											}
 	 	 	 	 										}
 	 	 	 	 									}
 	 	 	 									}
 	 	 	 									
	 	 	 	 								if(cnDetailService.getOwnName() != null){
		 	 	 										break;
		 	 	 								}
 	 	 	 								}
 	 	 								}
 	 	 									
 	 	 								chldFlag = true;
 	 	 							}
 	 							}else{
 	 								
 	 							}
 							}
 							
 							if(chldFlag){
 								break;
 							}
 						}
 						
 						
 						System.out.println("exit from chlnList loop");
 						break;
 					}
 				}
 				
 				
 				
 				cnDetList.add(cnDetailService);
 			}
 		}
 		
 		
 		return cnDetList;
	}
	
	
	
	@RequestMapping(value = "/getCnReport", method = RequestMethod.POST)
	public @ResponseBody Object getCnReport(@RequestBody Map<String,Object> clientMap) {
		System.out.println("enter into getCnReport funciton ="+(String) clientMap.get("frDt"));
		Map<String, Object> map = new HashMap<String, Object>();
		Date frDt = Date.valueOf((String) clientMap.get("frDt"));
		Date toDt = Date.valueOf((String) clientMap.get("toDt"));
		
		List<CnDetailService> cnDetList = reportDAO.getCnDetByDt(frDt, toDt);
 		if(!cnDetList.isEmpty()){
 			map.put("list",cnDetList);
 			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
 		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getCnRepCust", method = RequestMethod.POST)
	public @ResponseBody Object getCnRepCust() {
		System.out.println("enter into getCnRepCust funciton");
		Map<String,Object> map = new HashMap<>();
		List<Customer> custList = customerDAO.getAllCustomer();
		if(!custList.isEmpty()){
			map.put("list",custList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getCnRepBrh", method = RequestMethod.POST)
	public @ResponseBody Object getCnRepBrh() {
		System.out.println("enter into getCnRepBrh funciton");
		Map<String,Object> map = new HashMap<>();
		List<Branch> branchList = new ArrayList<Branch>();
		branchList = branchDAO.getAllActiveBranches();
		if(!branchList.isEmpty()){
			map.put("list",branchList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getCnRepFrCust", method = RequestMethod.POST)
	public @ResponseBody Object getCnRepFrCust(@RequestBody Map<String,Object> clientMap) {
		System.out.println("enter into getCnRepFrCust function");
		Map<String,Object> map = new HashMap<>();
		/*if(custCode != null){
			List<CnDetailService> cnDetList = getAllCnDetail();	
			if(!cnDetList.isEmpty()){
				List<CnDetailService> actCnDetList = new ArrayList<>();	
				for(int i=0;i<cnDetList.size();i++){
					if(cnDetList.get(i).getCustCode() != null){
						if(cnDetList.get(i).getCustCode().equalsIgnoreCase(custCode)){
							actCnDetList.add(cnDetList.get(i));
						}
					}
				}
				if(!actCnDetList.isEmpty()){
					map.put("list",actCnDetList);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}*/
		
		Date frDt = Date.valueOf((String) clientMap.get("frDt"));
		Date toDt = Date.valueOf((String) clientMap.get("toDt"));
		String custCode = (String) clientMap.get("custCode");
		
		List<CnDetailService> cnDetList = reportDAO.getCnDetByCust(frDt, toDt, custCode);
 		if(!cnDetList.isEmpty()){
 			map.put("list",cnDetList);
 			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
 		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	@RequestMapping(value = "/getCnRepFrBrh", method = RequestMethod.POST)
	public @ResponseBody Object getCnRepFrBrh(@RequestBody Map<String,Object> clientMap) {
		System.out.println("enter into getCnRepFrBrh function");
		Map<String,Object> map = new HashMap<>();
		/*if(brhCode != null){
			List<CnDetailService> cnDetList = getAllCnDetail();	
			if(!cnDetList.isEmpty()){
				List<CnDetailService> actCnDetList = new ArrayList<>();	
				for(int i=0;i<cnDetList.size();i++){
					if(cnDetList.get(i).getBrhCode() != null){
						if(cnDetList.get(i).getBrhCode().equalsIgnoreCase(brhCode)){
							actCnDetList.add(cnDetList.get(i));
						}
					}
				}
				if(!actCnDetList.isEmpty()){
					map.put("list",actCnDetList);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}*/
		
		Date frDt = Date.valueOf((String) clientMap.get("frDt"));
		Date toDt = Date.valueOf((String) clientMap.get("toDt"));
		String brhCode = (String) clientMap.get("brhCode");
		
		List<CnDetailService> cnDetList = reportDAO.getCnDetByBrh(frDt, toDt, brhCode);
 		if(!cnDetList.isEmpty()){
 			map.put("list",cnDetList);
 			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
 		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		
		return map;
	}	
	
	
	
	@RequestMapping(value = "/getCnRepFrDate", method = RequestMethod.POST)
	public @ResponseBody Object getCnRepFrDate(@RequestBody Map<String,Date> clientMap) {
		System.out.println("enter into getCnRepFrDate function");
		Map<String,Object> map = new HashMap<>();
		Date stDt = clientMap.get("stDt");
		Date endDt = clientMap.get("endDt");
		System.out.println("start Date ===> "+stDt);
		System.out.println("start Date ===> "+clientMap.get("endDt"));
		if(stDt!=null && endDt!=null){
			List<CnDetailService> cnDetList = getAllCnDetail();	
			if(!cnDetList.isEmpty()){
				List<CnDetailService> actCnDetList = new ArrayList<>();	
				for(int i=0;i<cnDetList.size();i++){
					int result1 = cnDetList.get(i).getCnmtDt().compareTo(stDt);
					int result2 = cnDetList.get(i).getCnmtDt().compareTo(endDt);
					if(result1 >= 0 && result2 <= 0){
						actCnDetList.add(cnDetList.get(i));
					}
				}
				if(!actCnDetList.isEmpty()){
					map.put("list",actCnDetList);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
}

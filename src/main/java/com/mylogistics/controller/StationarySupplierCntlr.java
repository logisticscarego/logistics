package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.StateDAO;
import com.mylogistics.DAO.StationarySupplierDAO;
import com.mylogistics.model.State;
import com.mylogistics.model.StationarySupplier;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class StationarySupplierCntlr {

	@Autowired
	private StationarySupplierDAO stsupplierDAO;
	
	/*@Autowired
	private StationarySupplierOldDAO stsupplierOldDAO;*/
	
	@Autowired
	private StateDAO stateDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@RequestMapping(value = "/submitSupplier", method = RequestMethod.POST)
    public @ResponseBody Object submitSupplier(@RequestBody StationarySupplier stsupplier){
		
		System.out.println("Entered into submitSupplier function in controller----");
		Map<String,String> map = new HashMap<String,String>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		stsupplier.setUserCode(currentUser.getUserCode());
		stsupplier.setbCode(currentUser.getUserBranchCode());
		
		//String lastFiveChar = null;
		String stSupplierCode = null;
		long rowCount = stsupplierDAO.totalCount();
		
		 if(!(rowCount==-1)){
			 
	        	if(rowCount==0){
	        		stSupplierCode="1";
	        	}else{
	        		List<StationarySupplier> listStSupplier = stsupplierDAO.getLastStationarySupplier();			    		
			    	int lastStSupplierId = listStSupplier.get(0).getStSupID();
			    	int stSupplierId = lastStSupplierId+1;
			    	stSupplierCode = String.valueOf(stSupplierId);
			    	/*int end = 100000+stSupplierId;
			    	lastFiveChar = String.valueOf(end).substring(1,6);	*/	
	        	}
	        	//String stSupplierCode = CodePatternService.stSupplierCodeGen(stsupplier, lastFiveChar);
	        	stsupplier.setStSupCode(stSupplierCode);
		 }   	
				
		int temp=stsupplierDAO.saveStationarySupplier(stsupplier);
		if (temp>=0) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
	
		return map;
    
	}
	
	@RequestMapping(value = "/getAllSupplierCodes", method = RequestMethod.POST)
	  public @ResponseBody Object getAllSupplierCodes(){
		
		List<StationarySupplier> supplierCodeList = stsupplierDAO.getStationarySupplierCode();
		
		Map<String,Object> map = new HashMap<String , Object>();
		if (!(supplierCodeList.isEmpty())) {
		    map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    map.put("supplierCodeList",supplierCodeList);
		}else{
		  	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	 }
	
	 @RequestMapping(value = "/stSupplierDetails", method = RequestMethod.POST)
	  public @ResponseBody Object stSupplierDetails(@RequestBody String stSupplierCode){
		 
		StationarySupplier stsupplier = new StationarySupplier();
		List<StationarySupplier> list = stsupplierDAO.retrieveSupplier(stSupplierCode);
		  		
		Map<String,Object> map = new HashMap<String , Object>();
		if (!(list.isEmpty())) {
			stsupplier = list.get(0);
		    map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    map.put("stsupplier",stsupplier);
		}else{
		  	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		   return map;	
	   }
	 
	 /*@RequestMapping(value = "/editStSupplier", method = RequestMethod.POST)
	    public @ResponseBody Object editStSupplier(@RequestBody StationarySupplier stsupplier){
		 	System.out.println("Entered into editStSupplier of controller----");
		 	
		 	Map<String,String> map = new HashMap<String, String>();
	        
	        int oldStSupplierId = stsupplier.getStSupID();
	        List<StationarySupplier> stSuplist = stsupplierDAO.getOldStSupplier(oldStSupplierId);
	        if (!(stSuplist.isEmpty())) {
	        	 StationarySupplierOld stSupplierOld = new StationarySupplierOld();
	        	 stSupplierOld.setStSupID(stSuplist.get(0).getStSupID());
	        	 stSupplierOld.setStSupName(stSuplist.get(0).getStSupName());
	        	 stSupplierOld.setStSupCode(stSuplist.get(0).getStSupCode());
	        	 stSupplierOld.setStSupAdd(stSuplist.get(0).getStSupAdd());
	        	 stSupplierOld.setStSupCity(stSuplist.get(0).getStSupCity());
	        	 stSupplierOld.setStSupState(stSuplist.get(0).getStSupState());
	        	 stSupplierOld.setStSupPin(stSuplist.get(0).getStSupPin());
	        	 stSupplierOld.setStSupPanNo(stSuplist.get(0).getStSupPanNo());
	        	 stSupplierOld.setStSupTdsCode(stSuplist.get(0).getStSupTdsCode());
	        	 stSupplierOld.setStSupSubGroup(stSuplist.get(0).getStSupSubGroup());
	        	 stSupplierOld.setStSupGroup(stSuplist.get(0).getStSupGroup());
	        	 stSupplierOld.setUserCode(stSuplist.get(0).getUserCode());
	        	 stSupplierOld.setbCode(stSuplist.get(0).getbCode());
	        	 
	        	 int tmp = stsupplierOldDAO.saveStnSupOld(stSupplierOld);
	        	 if (tmp>=0) {
	 	        	map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				 }else {
					  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				 }
	        }else{
	        	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
	        }
	        
	        String subStSupName = stsupplier.getStSupName().substring(0,3);
	        String oldStSupCode =  stsupplier.getStSupCode();
	        String subOldStSupCode = oldStSupCode.substring(0,3);
	        String newOldStSupCode = oldStSupCode.replace(subOldStSupCode, subStSupName);
	        stsupplier.setStSupCode(newOldStSupCode);
	
	        int temp=stsupplierDAO.saveStationarySupplier(stsupplier);
	        if (temp>=0) {
	        	
	        	 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			} else {
				  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}	    	
	    	return map;
	    }*/
	 
	 @RequestMapping(value="/getStateForSupplier",method = RequestMethod.POST)
		public @ResponseBody Object getStateDetails(){
		 System.out.println("Entered intogetStateForSupplier functon");
			
			List<State> stateList = stateDAO.getStateData();
			
			Map<String,Object> map = new HashMap<String,Object>();
			if (!(stateList.isEmpty())) {
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
	  			map.put("list",stateList);	
	  		}else {
	  			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;	
		}
	 
}

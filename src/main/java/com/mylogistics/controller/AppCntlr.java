package com.mylogistics.controller;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.sql.Blob;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;
import javax.swing.text.DefaultEditorKit.CutAction;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.mylogistics.DAO.AddressDAO;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BranchMUStatsDAO;
import com.mylogistics.DAO.BranchSStatsDAO;
import com.mylogistics.DAO.BranchStockLeafDetDAO;
import com.mylogistics.DAO.BrokerDAO;
import com.mylogistics.DAO.ChallanDAO;
import com.mylogistics.DAO.CnmtDAO;
import com.mylogistics.DAO.Cnmt_ChallanDAO;
import com.mylogistics.DAO.FAParticularDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.OwnerDAO;
import com.mylogistics.DAO.UserDAO;
import com.mylogistics.DAO.werservice.ChallanWebServiceDao;
import com.mylogistics.DAO.werservice.OwnBrkDao;
import com.mylogistics.DAO.werservice.VehicleWebServiceDao;
import com.mylogistics.DAO.werservice.WebServiceDAO;
import com.mylogistics.DAOImpl.webservice.OwnBrkDaoImpl;
import com.mylogistics.model.Address;
import com.mylogistics.model.BranchMUStats;
import com.mylogistics.model.BranchMUStatsBk;
import com.mylogistics.model.BranchSStats;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.Broker;
import com.mylogistics.model.BrokerImg;
import com.mylogistics.model.Challan;
import com.mylogistics.model.ChallanDetail;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.Cnmt_Challan;
import com.mylogistics.model.Customer;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.MultipleState;
import com.mylogistics.model.MultipleStation;
import com.mylogistics.model.Owner;
import com.mylogistics.model.OwnerImg;
import com.mylogistics.model.User;
import com.mylogistics.model.webservice.CnmtApp;
import com.mylogistics.model.webservice.ImageApp;
import com.mylogistics.model.webservice.OwnBrkApp;
import com.mylogistics.services.Chln_ChlnDetService;
import com.mylogistics.services.Cnmt_ChallanService;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.ImageService;
import com.mylogistics.services.InvAndDateService;
import com.mylogistics.services.InvoiceService;
import com.mylogistics.services.InvoiceServiceImpl;
import com.mylogistics.services.ModelService;
import com.mylogistics.services.VehVendorService;
import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;

@Controller
public class AppCntlr {
	
	@Autowired
	private WebServiceDAO webServiceDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchSStatsDAO branchSStatsDAO;
	
	@Autowired
	private BranchStockLeafDetDAO branchStockLeafDetDAO;
	
	private ModelService modelService = new ModelService();
	
	private InvoiceService invoiceService = new InvoiceServiceImpl();
	
	@Autowired
	private CnmtDAO cnmtDAO;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private ChallanWebServiceDao challanWebServiceDao;
	
	@Autowired
	private BranchMUStatsDAO branchMUStatsDAO;
	
	@Autowired
	private ChallanDAO challanDAO;
	
	@Autowired
	private Cnmt_ChallanDAO cnmt_ChallanDAO;
	
	@Autowired
	private OwnBrkDao ownBrkDao;
	
	@Autowired
	private FAParticularDAO faParticularDAO;
	
	@Autowired
	private OwnerDAO ownerDAO;
	
	@Autowired
	private BrokerDAO brokerDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@Autowired
	private AddressDAO addressDAO;
	
	@Autowired
	private VehicleWebServiceDao vehicleWebServiceDao;
	
	private ImageService imageService = new ImageService();
	
	public static Logger logger = Logger.getLogger(AppCntlr.class);
	

	@RequestMapping(value = "/getPendingCnmt", method = RequestMethod.POST)
	public @ResponseBody Object getPendingCnmt(@RequestBody Map<String, Object> initParam){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		String cnmtCode = String.valueOf(initParam.get("cnmtCode"));
		resultMap = webServiceDAO.getPendingCnmtDetail(cnmtCode, currentUser.getUserBranchCode());
		return resultMap;
	}
	
	@RequestMapping(value = "/getCnmtPendingCodeList", method = RequestMethod.POST)
	public @ResponseBody Object getCnmtPendingCodeList(){
		logger.info("Enter into getCnmtPendingCodeList()...");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		resultMap = webServiceDAO.getCnmtPendingCodeList(currentUser.getUserBranchCode());
		logger.info("Exit from getCnmtPendingCodeList()...");
		return resultMap;
	}
	
	@RequestMapping(value = "/validateCnmtPending", method = RequestMethod.POST)
	public @ResponseBody Object validateCnmtPending(@RequestBody Cnmt cnmt){
		cnmt.setView(false);
		User currentUser = (User)httpSession.getAttribute("currentUser");
		cnmt.setUserCode(currentUser.getUserCode());
		cnmt.setbCode(currentUser.getUserBranchCode());
		cnmt.setBranchCode(currentUser.getUserBranchCode());

		String cnmtCode =cnmt.getCnmtCode();
		double daysLeft = getCnmtDaysLast();
		double average = getAvgMonthlyUsageForCnmt();
		System.out.println("daysLeft-----------------"+daysLeft);
		String brsLeafDetStatus = "cnmt";
		String actualSNo = cnmtCode;
		
		Blob blob = modelService.getBlob();
		Blob confirmBlob = modelService.getConfirmBlob();
		List<InvAndDateService> invList = invoiceService.getAllInvoice();
		int temp = cnmtDAO.saveCnmtToDB(cnmt,cnmtCode,brsLeafDetStatus,daysLeft,average,blob,confirmBlob,invList);
		invoiceService.deleteAll();
		Map<String,String> map = new HashMap<String,String>();
		if (temp>=0) {
			modelService.setBlob(null);
			modelService.setConfirmBlob(null);
			
			String code = (String)cnmt.getCnmtCode();
			String status = webServiceDAO.updatePendingCnmt(code);
			if(cnmt.getDirty())
				webServiceDAO.updatePendingCnmtChange(cnmt, currentUser);
			if(status.equalsIgnoreCase("success")){				
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);				
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				map.put("msg", "Your Pending CNMT is not updated");
			}
		} else {
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}	 	
		return map;   		
		
	}
	
	@RequestMapping(value = "/getPendingChallanCode", method = RequestMethod.POST)
	public @ResponseBody Object getPendingChallanCode(){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			User currentUser = (User)httpSession.getAttribute("currentUser");
			List<String> chlnCodeList = challanWebServiceDao.getPendingChallanCode();
			if(chlnCodeList.isEmpty()){
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
				resultMap.put("msg", "You don't have pending challan !");
			}else{
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
				resultMap.put("chlnCodeList", chlnCodeList);				
				resultMap.put("branchName",  branchDAO.getBrNameByBrCode(currentUser.getUserBranchCode()));
				resultMap.put("branchCode", currentUser.getUserBranchCode());
			}
		}catch(Exception e){
			System.out.println("Error in getPendingChallanCode - AppCntlr : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/getPendingChallanDetail", method = RequestMethod.POST)
	public @ResponseBody Object getPendingChallanDetail(@RequestBody String chlnCode){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			resultMap = challanWebServiceDao.getPendingChallanDetail(chlnCode);
		}catch(Exception e){
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
			System.out.println("Error in getPendingChallanDetail - AppCntlr : "+e);
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/getPendingOwners", method = RequestMethod.POST)
	public @ResponseBody Object getPendingOwners(){
		logger.info("Enter into /getPendingOwners()...");
		Map<String, Object> resultMap = new HashMap<String, Object>();		
		try{
			resultMap = ownBrkDao.getPendingOwnBrk("owner");			
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		logger.info("Exit from /getPendingOwners()...");
		return resultMap;
	}
	
	@RequestMapping(value = "/getPendingBrokers", method = RequestMethod.POST)
	public @ResponseBody Object getPendingBrokers(){
		logger.info("Enter into /getPendingBroker()...");
		Map<String, Object> resultMap = new HashMap<String, Object>();		
		try{
			resultMap = ownBrkDao.getPendingOwnBrk("broker");			
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		logger.info("Exit from /getPendingBrokers()...");
		return resultMap;
	}
	
	@RequestMapping(value = "/getOwnBrkDtById", method = RequestMethod.POST)
	public @ResponseBody Object getOwnBrkDtById(@RequestBody OwnBrkApp ownBrkApp){
		logger.info("Enter into /getOwnBrkDtById()...");
		Map<String, Object> resultMap = new HashMap<String, Object>();		
		try{
			resultMap = ownBrkDao.getOwnBrkDtById(ownBrkApp);			
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		logger.info("Exit from /getOwnBrkDtById()...");
		return resultMap;
	}	
	
	public Map<String, Object> varifyChlnApp(Cnmt_ChallanService cnmt_ChallanService){
		Challan challan = cnmt_ChallanService.getChallan();
		List cnmtList = cnmt_ChallanService.getCnmtCodeList();
		return challanWebServiceDao.updateChlnApp(challan, cnmtList);	
	}
	
	@RequestMapping(value ="/varifyChallan", method = RequestMethod.POST)
	public @ResponseBody Object submitChallan(@RequestBody Cnmt_ChallanService cnmt_ChallanService){
		Map<String,Object> map = new HashMap<String, Object>();
		
		map = varifyChlnApp(cnmt_ChallanService);		
		String res = String.valueOf(map.get(ConstantsValues.RESULT));
		System.out.println("Result : "+res);
		if(res.equalsIgnoreCase("error")){
			return map;
		}
		
		sendEmailAndMsg(cnmt_ChallanService.getCnmtCodeList(), cnmt_ChallanService.getChallanDetail());
		
		System.out.println("enter into submitChallan fucntion");
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		Map<String,Object> finalResult = new HashMap<String,Object>();
		Challan challan = cnmt_ChallanService.getChallan();

		ChallanDetail challanDetail = cnmt_ChallanService.getChallanDetail();

		String chlanCode =challan.getChlnCode();
		//int serialNo = Integer.parseInt(chlanCode);
		String actualSNo = chlanCode;
		double daysLeft = getChlnDaysLast();
		double average = getAvgMonthlyUsageForChln();
		System.out.println("daysLeft-----------------"+daysLeft);
		/*System.out.println("SerialNo--------------"+serialNo);*/
		System.out.println("ChlnCode-----------------"+chlanCode);
		/*System.out.println("SerialNo--------------"+serialNo);*/
		String brsLeafDetStatus = "chln";
		//String actualSNo = String.valueOf(serialNo);
		
		BranchStockLeafDet branchStockLeafDet = branchStockLeafDetDAO.deleteBSLD(actualSNo,brsLeafDetStatus);
		//System.out.println("deleteBSLD-------------------"+deleteBSLD);

		if(branchStockLeafDet != null){
			BranchSStats branchSStats = new BranchSStats();
			branchSStats.setBrsStatsBrCode(branchStockLeafDet.getBrsLeafDetBrCode());
			branchSStats.setBrsStatsType("chln");
			branchSStats.setBrsStatsSerialNo(branchStockLeafDet.getBrsLeafDetSNo());
			branchSStats.setUserCode(currentUser.getUserCode());
			branchSStats.setbCode(currentUser.getUserBranchCode());
			branchSStats.setBrsStatsDt(challan.getChlnDt());
			int result = branchSStatsDAO.saveBranchSStats(branchSStats);
			System.out.println("after saving result = "+result);

			Calendar cal = Calendar.getInstance();
			Date currentDate = challan.getChlnDt();
			System.out.println("Current date is-----"+currentDate);
			cal.setTime(currentDate);
			System.out.println("Current date is-----"+currentDate);
			int currentMonth = cal.get(Calendar.MONTH)+1;
			System.out.println("Current month is-----"+currentMonth);
			int currentYear = cal.get(Calendar.YEAR);
			System.out.println("Current year is-----"+currentYear);

			if(result==1){
				BranchMUStats branchMUStats = new BranchMUStats();
				branchMUStats.setBmusDaysLast(daysLeft);
				branchMUStats.setBmusStType("chln");
				branchMUStats.setBmusAvgMonthUsage(average);
				branchMUStats.setbCode(currentUser.getUserBranchCode());
				branchMUStats.setUserCode(currentUser.getUserCode());
				branchMUStats.setBmusBranchCode(branchStockLeafDet.getBrsLeafDetBrCode());
				branchMUStats.setBmusDt(currentDate);


				long rowsCount = branchMUStatsDAO.totalBMUSRows("chln");
				System.out.println("Rows count for Chln is-----------"+rowsCount);

				if(rowsCount!=-1){
					if(rowsCount==0){ 
						branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats);
					}else{
						List<BranchMUStats> listStats = branchMUStatsDAO.getLastData(branchStockLeafDet.getBrsLeafDetBrCode(),"chln");
						BranchMUStats branchMUStats2 = listStats.get(0);
						Date lastDate = listStats.get(0).getBmusDt();
						int lastId = listStats.get(0).getBmusId();

						System.out.println("The last date is ==========="+lastDate);
						Calendar calender = Calendar.getInstance();
						calender.setTime(lastDate);
						int lastMonth = calender.get(Calendar.MONTH)+1;
						System.out.println("lastMonth---------------"+lastMonth);
						int lastyear = calender.get(Calendar.YEAR);
						System.out.println("lastyear---------------"+lastyear);
						if((currentMonth==lastMonth) && (currentYear==lastyear)){
							branchMUStats.setBmusAvgMonthUsage(average);
							branchMUStats.setBmusDt(currentDate);
							branchMUStats.setBmusDaysLast(daysLeft);
							branchMUStats.setBmusId(lastId);
							branchMUStatsDAO.updateBMUS(branchMUStats);
							System.out.println("After update");
						}else if((currentMonth!=lastMonth) && (currentYear!=lastyear)){
							System.out.println("Enter into function with id ---------"+lastId);
							int delete = branchMUStatsDAO.deleteBMUS(lastId);
							System.out.println("After deleting old entry---------------------"+delete);
							if(delete==1){
								int save =branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats);
								System.out.println("--------------------After saving new entry"+save);
								if(save == 1){
									BranchMUStatsBk bk = new BranchMUStatsBk();
									bk.setbCode(branchMUStats2.getbCode());
									bk.setBmusAvgMonthUsage(branchMUStats2.getBmusAvgMonthUsage());
									bk.setBmusBranchCode(branchMUStats2.getBmusBranchCode());
									bk.setBmusDaysLast(branchMUStats2.getBmusDaysLast());
									bk.setBmusDt(branchMUStats2.getBmusDt());
									bk.setBmusId(branchMUStats2.getBmusId());
									bk.setBmusStType(branchMUStats2.getBmusStType());
									bk.setUserCode(branchMUStats2.getUserCode());
									branchMUStatsDAO.saveBMUSBK(bk);
									System.out.println("---------------After saving into backup");
								}else{
									map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);	
								}
							}	else{
								map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
							}
						}else if((currentMonth!=lastMonth) && (currentYear==lastyear)){
							System.out.println("Enter into function with id ---------"+lastId);
							int delete = branchMUStatsDAO.deleteBMUS(lastId);
							System.out.println("After deleting old entry---------------------"+delete);
							if(delete==1){
								int save =branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats);
								System.out.println("--------------------After saving new entry"+save);
								if(save == 1){
									BranchMUStatsBk bk = new BranchMUStatsBk();
									bk.setbCode(branchMUStats2.getbCode());
									bk.setBmusAvgMonthUsage(branchMUStats2.getBmusAvgMonthUsage());
									bk.setBmusBranchCode(branchMUStats2.getBmusBranchCode());
									bk.setBmusDaysLast(branchMUStats2.getBmusDaysLast());
									bk.setBmusDt(branchMUStats2.getBmusDt());
									bk.setBmusId(branchMUStats2.getBmusId());
									bk.setBmusStType(branchMUStats2.getBmusStType());
									bk.setUserCode(branchMUStats2.getUserCode());
									branchMUStatsDAO.saveBMUSBK(bk);
									System.out.println("---------------After saving into backup");
								}else{
									map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
								}
							}else{		
								map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
							}
						}
					}	
				}
			} 
		}
		System.out.println("After inserting into branchSStats ");

		
		//TODO
	
		List<Map<String,Object>> cnmtCodeList = new ArrayList<Map<String,Object>>();
		cnmtCodeList = cnmt_ChallanService.getCnmtCodeList();
		String cnmtCode = "";

		if(cnmtCodeList.size() > 1){
			
			List<String> cnmtList = new ArrayList<String>();
			for(int m=0;m<cnmtCodeList.size();m++){
				cnmtList.add(String.valueOf(cnmtCodeList.get(m).get("cnmt")));
			}
			
			
			List<Cnmt> multiCnmtList = cnmtDAO.getAllCnmtByCode(cnmtList);
		
			challan.setView(false);
			challan.setUserCode(currentUser.getUserCode());
			challan.setbCode(currentUser.getUserBranchCode());
			System.out.println("challan.lryNo = "+challan.getChlnLryNo());
		

			challanDetail.setbCode(currentUser.getUserBranchCode());
			challanDetail.setUserCode(currentUser.getUserCode());
			int challanId = challanDAO.saveChlnAndChd(challan , challanDetail);
			
			

			//TODO
			if(challanId > 0){
				for(int i=0;i<multiCnmtList.size();i++){
					Cnmt cnmt = multiCnmtList.get(i);
					int pkg = 0;
					double wt = 0.0;
					for(int j=0;j<cnmtCodeList.size();j++){
						if(cnmt.getCnmtCode().equalsIgnoreCase(String.valueOf(cnmtCodeList.get(j).get("cnmt")))){
							pkg = Integer.parseInt(String.valueOf(cnmtCodeList.get(j).get("pkg")));
							wt = Double.parseDouble(String.valueOf(cnmtCodeList.get(j).get("wt")));
						}
					}
					cnmt.setIsDone(3);
					int cnmtId = cnmtDAO.updateCnmt(cnmt);
					Cnmt_Challan cnmt_Challan = new Cnmt_Challan();
					cnmt_Challan.setChlnId(challanId);
					cnmt_Challan.setCnmtId(cnmtId);
					cnmt_Challan.setCnmtNoOfPkg(pkg);
					cnmt_Challan.setCnmtTotalWt(wt);
					cnmt_ChallanDAO.saveCnmt_Challan(cnmt_Challan);
				}
				finalResult.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
			return finalResult;

		}else if(cnmtCodeList.size() == 1){
			System.out.println("*********************cnmtCodeList size = 1");
			System.out.println("String.valueOf(cnmtCodeList.get(0).get(cnmt)) = "+String.valueOf(cnmtCodeList.get(0).get("cnmt")));
			
			List<Cnmt> cnmtList = new ArrayList<Cnmt>();
			cnmtList = cnmtDAO.getCnmt(String.valueOf(cnmtCodeList.get(0).get("cnmt")));
	
			if(!cnmtList.isEmpty()){		
						
						
					Cnmt cnmt = cnmtList.get(0);
					
					if(challan.getChlnToStn().equalsIgnoreCase(cnmt.getCnmtToSt())){
						challan.setView(false);
						challan.setUserCode(currentUser.getUserCode());
						challan.setbCode(currentUser.getUserBranchCode());
						System.out.println("challan.lryNo = "+challan.getChlnLryNo());
						
						challanDetail.setbCode(currentUser.getUserBranchCode());
						challanDetail.setUserCode(currentUser.getUserCode());
						int challanId = challanDAO.saveChlnAndChd(challan , challanDetail);
						
						if(challanId > 0){
							Cnmt_Challan cnmt_Challan = new Cnmt_Challan();
							cnmt_Challan.setCnmtId(cnmt.getCnmtId());
							cnmt_Challan.setChlnId(challanId);
							cnmt_Challan.setCnmtNoOfPkg(Integer.parseInt(String.valueOf(cnmtCodeList.get(0).get("pkg"))));
							cnmt_Challan.setCnmtTotalWt(Double.parseDouble(String.valueOf(cnmtCodeList.get(0).get("wt"))));
							cnmt_ChallanDAO.saveCnmt_Challan(cnmt_Challan);
							
							cnmt.setTrans(false);
							cnmtDAO.updateCnmt(cnmt);
							finalResult.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
						}else{
							finalResult.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
						}
					}else{
						challan.setView(false);
						challan.setUserCode(currentUser.getUserCode());
						challan.setbCode(currentUser.getUserBranchCode());
						System.out.println("challan.lryNo = "+challan.getChlnLryNo());
						//int challanId = challanDAO.saveChallan(challan);
						
						challanDetail.setbCode(currentUser.getUserBranchCode());
						challanDetail.setUserCode(currentUser.getUserCode());
						int challanId = challanDAO.saveChlnAndChd(challan , challanDetail);
						
						if(challanId > 0){
							Cnmt_Challan cnmt_Challan = new Cnmt_Challan();
							cnmt_Challan.setCnmtId(cnmt.getCnmtId());
							cnmt_Challan.setChlnId(challanId);
							cnmt_Challan.setCnmtNoOfPkg(Integer.parseInt(String.valueOf(cnmtCodeList.get(0).get("pkg"))));
							cnmt_Challan.setCnmtTotalWt(Double.parseDouble(String.valueOf(cnmtCodeList.get(0).get("wt"))));
							cnmt_ChallanDAO.saveCnmt_Challan(cnmt_Challan);
							
							cnmt.setTrans(true);
							cnmtDAO.updateCnmt(cnmt);
							finalResult.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
						}else{
							finalResult.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
						}
					}	
			}else{
				System.out.println("cnmt not exist");
			}
			return finalResult;
		}else{
			finalResult.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			return finalResult;
		}
	}
	
	public void sendEmailAndMsg(List<Map<String, Object>> cnmtList, ChallanDetail challanDetail){
		try{
			List<Customer> customerList = challanWebServiceDao.getCustomerListByCnmtList(cnmtList);
			System.out.println("Customer List Size : "+ customerList.size());
			Iterator<Customer> custIterator = customerList.iterator();
			while(custIterator.hasNext()){
				Customer customer = (Customer)custIterator.next();
				
			}
		}catch(Exception e){
			System.out.println("Error in sendEmailAndMsg - AppCntrl : "+e);
		}
	}
	
	public  double getCnmtDaysLast() {
		Date date = new Date(new java.util.Date().getTime());
		Calendar calendar =Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		Date endDate = new Date(calendar.getTime().getTime());
		double cnmtLeft=0;
		User currentUser = (User)httpSession.getAttribute("currentUser");

		double cnmtPerDayUsed = branchSStatsDAO.getLastMonthPerDayUsed(date,endDate,"cnmt");
		double daysLeft = 0;
		if(cnmtPerDayUsed>0){
			cnmtLeft = branchStockLeafDetDAO.getLeftStationary(currentUser.getUserBranchCode(),"cnmt");
			daysLeft=cnmtLeft/cnmtPerDayUsed;
		}

		return daysLeft;
	}
	
	public double getAvgMonthlyUsageForCnmt() {

		System.out.println("------Enter into getAvgMonthlyUsage function");

		Date todayDate = new Date(new java.util.Date().getTime());
		Date firstEntryDate = branchSStatsDAO.getFirstEntryDate("cnmt");
		System.out.println("**************firstEntryDate = "+firstEntryDate);
		double average=0.00;
		if(firstEntryDate != null){
			Calendar calendar =Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.MONTH, -1);
			Date oneMonthPrevDate = new Date(calendar.getTime().getTime());

			calendar.add(Calendar.MONTH, -1);
			Date twoMonthPrevDate = new Date(calendar.getTime().getTime());

			calendar.add(Calendar.MONTH, -1);
			Date threeMonthPrevDate = new Date(calendar.getTime().getTime());

			double days = (double)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));


			double cnmtForOneMnth = branchSStatsDAO.getNoOfStationary(todayDate, oneMonthPrevDate,"cnmt");
			double cnmtForScndMnth = branchSStatsDAO.getNoOfStationary(oneMonthPrevDate, twoMonthPrevDate,"cnmt");
			double cnmtForThirdMnth = branchSStatsDAO.getNoOfStationary(twoMonthPrevDate, threeMonthPrevDate,"cnmt");
			double cnmtBwFrstAndScndMnth = branchSStatsDAO.getNoOfStationary(firstEntryDate, twoMonthPrevDate,"cnmt");
			double cnmtBwFrstAndOneMnth=branchSStatsDAO.getNoOfStationary(firstEntryDate, oneMonthPrevDate,"cnmt");
			double cnmtBwTodayAndFrstDate=branchSStatsDAO.getNoOfStationary(todayDate, firstEntryDate,"cnmt");


			if(days>=90){
				average = ((cnmtForOneMnth*3) + (cnmtForScndMnth*2) + cnmtForThirdMnth)/6;
			}
			else if(days>=60 && days<90){
				int daysBwFrstAndScndMnth = (int)( (twoMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));		

				average = (((cnmtForOneMnth*3) + (cnmtForScndMnth*2) + (cnmtBwFrstAndScndMnth/daysBwFrstAndScndMnth))*30)/6;
			}
			else if(days>=30 && days<60){

				int daysBwFrstAndOneMnth = (int)( (oneMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				average = (((cnmtForOneMnth*2) +  (cnmtBwFrstAndOneMnth/daysBwFrstAndOneMnth))*30)/3;
			}
			else if(days<30){
				int daysBwFrstAndToday = (int)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				if(daysBwFrstAndToday == 0){
					average = cnmtBwTodayAndFrstDate*30;
				}else{
					average = ((cnmtBwTodayAndFrstDate/daysBwFrstAndToday)*30);
				}
			}
		}	

		return average;
	}
	
	public double getChlnDaysLast() {

		Date date = new Date(new java.util.Date().getTime());
		Calendar calendar =Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		Date endDate = new Date(calendar.getTime().getTime());
		double chlnLeft=0;
		User currentUser = (User)httpSession.getAttribute("currentUser");
		double daysLeft = 0;
		double chlnPerDayUsed = branchSStatsDAO.getLastMonthPerDayUsed(date,endDate,"chln");
		if(chlnPerDayUsed>0){
			chlnLeft = branchStockLeafDetDAO.getLeftStationary(currentUser.getUserBranchCode(),"chln");
			daysLeft=chlnLeft/chlnPerDayUsed;
		}


		return daysLeft;
	}
	
	public double getAvgMonthlyUsageForChln() {

		System.out.println("------Enter into getAvgMonthlyUsage function");

		Date todayDate = new Date(new java.util.Date().getTime());
		Date firstEntryDate = branchSStatsDAO.getFirstEntryDate("chln");
		double average=0.00;
		if(firstEntryDate != null){
			Calendar calendar =Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.MONTH, -1);
			Date oneMonthPrevDate = new Date(calendar.getTime().getTime());

			calendar.add(Calendar.MONTH, -1);
			Date twoMonthPrevDate = new Date(calendar.getTime().getTime());

			calendar.add(Calendar.MONTH, -1);
			Date threeMonthPrevDate = new Date(calendar.getTime().getTime());

			double days = (double)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));

			double chlnForOneMnth = branchSStatsDAO.getNoOfStationary(todayDate, oneMonthPrevDate,"chln");
			double chlnForScndMnth = branchSStatsDAO.getNoOfStationary(oneMonthPrevDate, twoMonthPrevDate,"chln");
			double chlnForThirdMnth = branchSStatsDAO.getNoOfStationary(twoMonthPrevDate, threeMonthPrevDate,"chln");
			double chlnBwFrstAndScndMnth = branchSStatsDAO.getNoOfStationary(firstEntryDate, twoMonthPrevDate,"chln");
			double chlnBwFrstAndOneMnth=branchSStatsDAO.getNoOfStationary(firstEntryDate, oneMonthPrevDate,"chln");
			double chlnBwTodayAndFrstDate=branchSStatsDAO.getNoOfStationary(todayDate, firstEntryDate,"chln");



			if(days>=90){
				average = ((chlnForOneMnth*3) + (chlnForScndMnth*2) + chlnForThirdMnth)/6;
			}
			else if(days>=60 && days<90){
				int daysBwFrstAndScndMnth = (int)( (twoMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));		

				average = (((chlnForOneMnth*3) + (chlnForScndMnth*2) + (chlnBwFrstAndScndMnth/daysBwFrstAndScndMnth))*30)/6;
			}
			else if(days>=30 && days<60){

				int daysBwFrstAndOneMnth = (int)( (oneMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				average = (((chlnForOneMnth*2) +  (chlnBwFrstAndOneMnth/daysBwFrstAndOneMnth))*30)/3;
			}
			else if(days<30){
				int daysBwFrstAndToday = (int)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				if(daysBwFrstAndToday == 0){
					average = chlnBwTodayAndFrstDate*30;
				}else{
					average = ((chlnBwTodayAndFrstDate/daysBwFrstAndToday)*30);
				}
			}
		}	
		return average;
	}
	
	@RequestMapping(value = "/upldPendingOwnPanImg", method = RequestMethod.POST)
	public @ResponseBody Object upldOwnPanImg(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into upldOwnPanImg funciton");
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			imageService.setImage(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			imageService.setImage(blob) ;
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	@RequestMapping(value = "/upldPendingOwnDecImg", method = RequestMethod.POST)
	public @ResponseBody Object upldOwnDecImg(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into upldOwnDecImg funciton");
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			imageService.setDecImg(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			imageService.setDecImg(blob);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}

	@RequestMapping(value = "/verifyOwner", method = RequestMethod.POST)
	public @ResponseBody Object verifyOwner(
			@RequestBody ModelService modelService 
			){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.error("Enter into verifyOwner()....");
		try{	
			Address currentAddress = modelService.getCurrentAddress();
			Address registerAddress = modelService.getRegisterAddress();
			Address otherAddress = modelService.getOtherAddress();
			Owner owner = modelService.getOwner();
			
			User currentUser = (User)httpSession.getAttribute("currentUser");
			
			Integer ownBrkId = owner.getOwnId();
			String ownerCode=null;
			String faCode=null;
			
			Boolean available = ownerDAO.isOwnerAvailable(owner.getOwnName(), owner.getBranchCode());
			
			if(available){
				logger.info("Owner is available !");
				List<FAParticular> faParticulars = faParticularDAO.getFAParticular(ConstantsValues.FAP_OWN);
				FAParticular fAParticular = new FAParticular();
				fAParticular=faParticulars.get(0);
				int fapId=fAParticular.getFaPerId();
				String fapIdStr = String.valueOf(fapId); 
				if(fapId<10)
					fapIdStr="0"+fapIdStr;
				owner.setfAParticular(fAParticular);
				owner.setView(false);
				owner.setUserCode(currentUser.getUserCode());
				owner.setbCode(currentUser.getUserBranchCode());
				
				logger.info("FaParticular is Set : FaId = "+fapIdStr);
				
				OwnerImg ownImg = new OwnerImg();
				
				ImageApp imgApp = ownBrkDao.findOwnBrkImg(owner.getOwnId(),"owner");
				if(imgApp == null)
					imgApp = new ImageApp();		
				if( (imageService.getImage() != null && imageService.getDecImg() != null) || (imgApp.getPanPath() != null && imgApp.getDecPath() != null) ) {
					owner.setOwnPanIntRt(0);
				}else if (imageService.getImage() != null || imgApp.getPanPath() != null ) {				
					if (owner.getOwnPanNo().charAt(3) == 'P' ||	owner.getOwnPanNo().charAt(3) == 'H' ||	owner.getOwnPanNo().charAt(3) == 'J') {
						owner.setOwnPanIntRt(1);
					}else {
						owner.setOwnPanIntRt(2);
					}
				}
				
				Integer ownId = ownerDAO.saveOwnAndImg(owner, ownImg, imageService.getImage(), imageService.getDecImg());
				
				if(ownId > 0){
					ownerCode = "own"+String.valueOf(ownId); 
					ownId=ownId+100000;
					String ownIdStr = String.valueOf(ownId).substring(1,6);
					faCode=fapIdStr+ownIdStr;
					logger.info("Owner FaCode = "+faCode);
					owner.setOwnFaCode(faCode);
					int updateOwner= ownerDAO.updateOwner(owner);
					
					FAMaster faMaster = new FAMaster();
					faMaster.setFaMfaCode(faCode);
					faMaster.setFaMfaType("owner");
					faMaster.setFaMfaName(owner.getOwnName());
					faMaster.setbCode(currentUser.getUserBranchCode());
					faMaster.setUserCode(currentUser.getUserCode());
					faMasterDAO.saveFaMaster(faMaster);				
			
					if (currentAddress.getAddType() != null) {
						currentAddress.setUserCode(currentUser.getUserCode());
						currentAddress.setbCode(currentUser.getUserBranchCode());
						currentAddress.setAddRefCode(ownerCode);
						if(currentAddress.getCompleteAdd().length() > 2)
							addressDAO.saveAddress(currentAddress);
					}
				
					if(registerAddress.getAddType() != null){
						registerAddress.setUserCode(currentUser.getUserCode());
						registerAddress.setbCode(currentUser.getUserBranchCode());
						registerAddress.setAddRefCode(ownerCode);
						if(registerAddress.getCompleteAdd().length() > 2)
							addressDAO.saveAddress(registerAddress);
					}
				
					if(otherAddress.getAddType() != null && !(otherAddress.getAddType().equalsIgnoreCase("no"))){
						otherAddress.setUserCode(currentUser.getUserCode());
						otherAddress.setbCode(currentUser.getUserBranchCode());
						otherAddress.setAddRefCode(ownerCode);
						if(otherAddress.getCompleteAdd().length() > 2)
							addressDAO.saveAddress(otherAddress);
					}
							
				
				if(imgApp.getId() != null){
					ownBrkDao.copyImagesOrDeleteOwn(imgApp, ownerCode, imageService.getImage(), imageService.getDecImg(), currentUser);				
				}
				ownBrkDao.updateOwnBrkRecords(ownBrkId, ownerCode);
				
				imageService.setImage(null);
				imageService.setDecImg(null);
				
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				resultMap.put("msg", "Owner is successfully saved ......");
				logger.info("Owner is successfully saved !");
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Owner is not saved !");
			}
			
		}else{
			resultMap.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			resultMap.put("msg", "Owner already exits !");
		}
			
		}catch(Exception e){
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "ERROR !");
			logger.error("Exception : "+e);
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/verifyBroker", method = RequestMethod.POST)
	public @ResponseBody Object verifyBroker(
			@RequestBody ModelService modelService 
			){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{		
			Address currentAddress = modelService.getCurrentAddress();
			Address registerAddress = modelService.getRegisterAddress();
			Address otherAddress = modelService.getOtherAddress();
			Broker broker = modelService.getBroker();
			Integer ownBrkId = broker.getBrkId();
			User currentUser = (User)httpSession.getAttribute("currentUser");
			
			String brokerCode=null;
			String faCode=null;
			// Find faparticular for owner
			List<FAParticular> faParticulars = faParticularDAO.getFAParticular(ConstantsValues.FAP_BRK);
			FAParticular fAParticular = new FAParticular();
			fAParticular=faParticulars.get(0);
			int fapId=fAParticular.getFaPerId();
			String fapIdStr = String.valueOf(fapId); 
			System.out.println("the fap id is--------------------"+fapId+""+fapIdStr);
			if(fapId<10){
				fapIdStr="0"+fapIdStr;
				System.out.println("After adding zeroes--"+fapIdStr);
			}
			// Set faparticular for owner
			broker.setfAParticular(fAParticular);
			
			broker.setView(false);			
			broker.setUserCode(currentUser.getUserCode());
			broker.setbCode(currentUser.getUserBranchCode());
			
			BrokerImg brkImg = new BrokerImg();
			
			// Find pan and dec image already given or not
			ImageApp imgApp = ownBrkDao.findOwnBrkImg(ownBrkId, "broker");
			
			if(imgApp == null)
				imgApp = new ImageApp();		
			
			if ( (imageService.getImage() != null && imageService.getDecImg() != null) || (imgApp.getPanPath() != null && imgApp.getDecPath() != null) ) {
				broker.setBrkPanIntRt(0);
			} else if (imageService.getImage() != null || imgApp.getPanPath() != null ) {				
				if (broker.getBrkPanNo().charAt(3) == 'P' ||	broker.getBrkPanNo().charAt(3) == 'H' ||	broker.getBrkPanNo().charAt(3) == 'J') {
					broker.setBrkPanIntRt(1);
				}else {
					broker.setBrkPanIntRt(2);
				}
			} 
			
			Broker isAvailable = brokerDAO.findBrokerByBrh_Name(broker.getBranchCode(), broker.getBrkName());
			if(isAvailable == null){
				isAvailable = null;
					
				Integer brkId = brokerDAO.saveBrkAndImg(broker, brkImg, imageService.getImage(), imageService.getDecImg());
				
				// If broker is saved
				if(brkId > 0){
				    brokerCode = "brk"+String.valueOf(brkId);				
					brkId = brkId+100000;
					String brkIdStr = String.valueOf(brkId).substring(1,6);
					faCode = fapIdStr + brkIdStr;
					System.out.println("faCode code is------------------"+faCode);
					broker.setBrkFaCode(faCode);
					int updateBroker = brokerDAO.updateBroker(broker);
					System.out.println("After updating owner----------------"+updateBroker);
					
					FAMaster faMaster = new FAMaster();
					faMaster.setFaMfaCode(faCode);
					faMaster.setFaMfaType("broker");
					faMaster.setFaMfaName(broker.getBrkName());
					faMaster.setbCode(currentUser.getUserBranchCode());
					faMaster.setUserCode(currentUser.getUserCode());
					faMasterDAO.saveFaMaster(faMaster);				
			
					if (currentAddress.getAddType() != null) {
						currentAddress.setUserCode(currentUser.getUserCode());
						currentAddress.setbCode(currentUser.getUserBranchCode());
						currentAddress.setAddRefCode(brokerCode);
						addressDAO.saveAddress(currentAddress);
					}
				
					if(registerAddress.getAddType() != null && !(registerAddress.getAddType().equalsIgnoreCase("no"))){
						registerAddress.setUserCode(currentUser.getUserCode());
						registerAddress.setbCode(currentUser.getUserBranchCode());
						registerAddress.setAddRefCode(brokerCode);
						addressDAO.saveAddress(registerAddress);
					}
				
					if(otherAddress.getAddType() != null && !(otherAddress.getAddType().equalsIgnoreCase("no"))){
						otherAddress.setUserCode(currentUser.getUserCode());
						otherAddress.setbCode(currentUser.getUserBranchCode());
						otherAddress.setAddRefCode(brokerCode);
						addressDAO.saveAddress(otherAddress);
					}
					
				if(imgApp.getId() != null){			
					ownBrkDao.copyImagesOrDeleteBrk(imgApp, brokerCode, imageService.getImage(), imageService.getDecImg() ,currentUser);								
				}			
				System.out.println("Broker ID: "+ownBrkId);
				ownBrkDao.updateOwnBrkRecords(ownBrkId, brokerCode);
				
				imageService.setImage(null);
				imageService.setDecImg(null);
				
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);	
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Please try again !");
			}
				
		}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Broker already exists !");
			}			
		}catch(Exception e){
			logger.error("Exception : "+e);
		}
		return resultMap;
	}
		
	@RequestMapping(value = "/getPendingVehicle", method = RequestMethod.POST)
	public @ResponseBody Object getPendingVehicle(){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("Enter into /getPendingVehicle()...");
		try{			
			resultMap = vehicleWebServiceDao.getPendingVehicle();			
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		logger.info("Exit from /getPendingVehicle()...");
		return resultMap;
	}	
	
	@RequestMapping(value = "/getPendingVehDt", method = RequestMethod.POST)
	public @ResponseBody Object getPendingVehDt(@RequestBody Map<String, Object> initParam){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("Enter into /getPendingVehDt()...");
		try{
			String vvRcNo = String.valueOf(initParam.get("vvRcNo"));
			logger.error("Vehicle RC.No. : "+vvRcNo);
			if(vvRcNo.equalsIgnoreCase("")){
				logger.info("Vehicle no. not given !");
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Please enter vehicle no. !");
			}else{
				resultMap = vehicleWebServiceDao.getPendingVehDt(vvRcNo);
			}
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		logger.info("Exit from /getPendingVehDt()...");
		return resultMap;
	}
	
	@RequestMapping(value = "/verifyVehVenMstr", method = RequestMethod.POST)
	public @ResponseBody Object verifyVehVenMstr(@RequestBody VehVendorService vehVendorService){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("Enter into /verifyVehVenMstr()...");
		try{
			resultMap = vehicleWebServiceDao.verifyVehicle(vehVendorService);		
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		logger.info("Exit from /verifyVehVenMstr()...");
		return resultMap;
	}
	
	@RequestMapping(value = "/getEntryCount", method = RequestMethod.POST)
	public @ResponseBody Object getEntryCount(@RequestBody Map<String, Object> initParam){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("Enter into /getEntryCount()...");
		try{
			resultMap = webServiceDAO.getEntryCount(initParam);		
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}
		logger.info("Exit from /getEntryCount()...");
		return resultMap;
	}
	
	@RequestMapping(value = "/generateNewOTP", method = RequestMethod.POST)
	public @ResponseBody Object generateNewOTP(@RequestBody Map<String, Object> initParam){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		String phoneNo=initParam.get("phoneNo").toString();
		
		if(phoneNo!=null && !"".equalsIgnoreCase(phoneNo.trim())) {
			 System.out.println("Generating OTP using random() : "); 
		        System.out.print("You OTP is : "); 
		  
		        // Using numeric values 
		        String numbers = "0123456789"; 
		  
		        // Using random method 
		        Random rndm_method = new Random(); 
		  
		        char[] otp = new char[6]; 
		  
		        for (int i = 0; i < 6; i++) 
		        { 
		            // Use of charAt() method : to get character value 
		            // Use of nextInt() as it is scanning the value as int 
		            otp[i] = 
		             numbers.charAt(rndm_method.nextInt(numbers.length())); 
		        } 
		        System.out.println("New OTP="+otp);
		        System.out.println("New OTP="+otp.toString());
			
		        String otpNo=new String(otp);
		        System.out.println("New OTP="+otpNo);
		   try{ 
		        
		        String myPasscode = "pwd2018";
		        String myUsername = "CareGo";    
		       String message=URLEncoder.encode("OTP for creating new user is "+otpNo+"", "UTF-8");;
		        String requestUrl="http://www.apiconnecto.com/API/SMSHttp.aspx?UserId="+myUsername+"&pwd="+myPasscode+"&message="+message+"&Contacts="+phoneNo+"&SenderId=CAREGO&ServiceName=SMSTRANS";
		        System.out.println(requestUrl+"5ssss");
		        java.net.URL url = new java.net.URL(requestUrl);
		        HttpURLConnection uc = (HttpURLConnection)url.openConnection();
		        System.out.println(uc.getResponseMessage()+" ssss");
		        String resp=uc.getResponseMessage();
		        System.out.println(resp);
		        
		        uc.disconnect();
		        
			
				if(resp.equalsIgnoreCase("OK"))
					resultMap = webServiceDAO.saveOrUpdateOTP(phoneNo, otpNo);
				else {
					resultMap.put("result", "error");
					resultMap.put("msg", "OTP not send! retry");
				}
				
			}catch(Exception e){
				logger.error("Exception : "+e);
				resultMap.put("result", "error");
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Something is going wrong !");
			}
		}else {
			resultMap.put("result", "error");
			resultMap.put("msg", "Please enter mobile No.");
		}
		
		
		
		return resultMap;
	}
	
	
	@RequestMapping(value = "/verifyOTP", method = RequestMethod.POST)
	public @ResponseBody Object verifyOTP(@RequestBody Map<String, Object> initParam){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		String phoneNo=initParam.get("phoneNo").toString();
		String otpNo=initParam.get("otp").toString();
		System.out.println("otp="+otpNo+" Phone No.="+phoneNo);
		if(otpNo!=null && !"".equalsIgnoreCase(otpNo.trim())) {
			resultMap = webServiceDAO.verifyOTP(phoneNo, otpNo);
		}else {
			resultMap.put("result", "error");
			resultMap.put("msg", "OTP not found");
		}
		
		return resultMap;
	}
	
	
	
	@RequestMapping(value = "/generateNewAppLogin", method = RequestMethod.POST)
	public @ResponseBody Object generateNewAppLogin(@RequestBody Map<String, Object> initParam){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		String phoneNo=initParam.get("phoneNo").toString();
		
		   try{ 
		        
					//resultMap = webServiceDAO.saveAndReturnOTP(phoneNo, otpNo);
				
			}catch(Exception e){
				logger.error("Exception : "+e);
				resultMap.put("result", "error");
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Something is going wrong !");
			}
		
		
		
		return resultMap;
	}
	
	
	
	
	/* //code for creating encrypted password 
	 * String password = user.getPassword();
			MessageDigest md = MessageDigest.getInstance("MD5");
		    md.update(password.getBytes());
		    byte[] digest = md.digest();
		    StringBuffer sb = new StringBuffer();
		    for (byte b : digest) {
	            sb.append(String.format("%02x", b & 0xff));
	        }
		    System.out.println("original:" + password);
	        System.out.println("digested(hex):" + sb.toString());
	        
	        user.setPassword(sb.toString());
	 */
	 
	
	
	
}
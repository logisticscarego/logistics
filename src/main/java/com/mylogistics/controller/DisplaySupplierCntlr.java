package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.StationarySupplierDAO;
import com.mylogistics.model.StationarySupplier;
import com.mylogistics.services.ConstantsValues;


@Controller
public class DisplaySupplierCntlr {
	
	@Autowired
	private StationarySupplierDAO stationarySupplierDAO;
		
	@RequestMapping(value = "/getSupplierForAdmin", method = RequestMethod.POST)
	public @ResponseBody Object getSupplierForAdmin() {
		System.out.println("enter into getSupplier function");
		Map<String, Object> map = new HashMap<String, Object>();
		List<StationarySupplier> supplierList = stationarySupplierDAO.getSupplierListForAdmin();
		if(!supplierList.isEmpty()){
			map.put("supList",supplierList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, "No more supplier to show...");
		}
		return map;
	}
	
	@RequestMapping(value = "/editSupplier", method = RequestMethod.POST)
	public @ResponseBody Object editSupplier(@RequestBody StationarySupplier stnSup) {
		System.out.println("enter into editSupplier function");
		Map<String, Object> map = new HashMap<String, Object>();
		int res = stationarySupplierDAO.saveStationarySupplier(stnSup);
		if(res > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/updateIsViewStSupplier", method = RequestMethod.POST)
    public @ResponseBody Object updateIsViewStSupplier(@RequestBody String selection) {

        String contids[] = selection.split(",");
        int temp=stationarySupplierDAO.updateStSupplierIsViewTrue(contids);
        Map<String, Object> map = new HashMap<String, Object>();
        if(temp>0){
            map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
        }
        else{
            map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
        }
        return map;
    }
	
	@RequestMapping(value = "/getSupplierCodesForAdmin", method = RequestMethod.POST)
	  public @ResponseBody Object getSupplierCodesForAdmin(){
		
		List<StationarySupplier> supplierCodeList = stationarySupplierDAO.getSupplierListForAdmin();
		
		Map<String,Object> map = new HashMap<String , Object>();
		if (!(supplierCodeList.isEmpty())) {
		    map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    map.put("supplierCodeList",supplierCodeList);
		}else{
		  	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	 }
	
}

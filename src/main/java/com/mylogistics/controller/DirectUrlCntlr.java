package com.mylogistics.controller;

import java.sql.Date;
import java.util.TimeZone;

import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.mylogistics.model.User;
import com.mylogistics.model.UserRights;

@Controller
public class DirectUrlCntlr {

	private static final String TAG = "DirectUrlCntlr: ";
	private static final String AUTH = "authorized";
	private static final String UN_AUTH = "unAuthorized";

	@Autowired
	private HttpSession httpSession;

	public String authentication(){
		System.out.println("---------enter into authentication function");
		User currentUser = (User)httpSession.getAttribute("currentUser");

		if(currentUser == null){
			System.out.println("current user is null");
			return UN_AUTH;
		}else{
			System.out.println("current user authentication token ="+currentUser.getUserAuthToken());
			System.out.println("current session id ="+httpSession.getId());
			if(currentUser.getUserAuthToken().equalsIgnoreCase(httpSession.getId())){
				System.out.println("authorized user set in session");
				return AUTH;
			}else{
				System.out.println("unAuthorized user set in session");
				return UN_AUTH;
			}
		}
	}

	@RequestMapping("/")  
	public String index() {  
		System.out.println(TAG+"index");
		if(httpSession.isNew()){
			System.out.println("-------new session-------");
			System.out.println("-------session-------"+httpSession.getId());
			System.out.println("-------session creation time-------"+httpSession.getCreationTime());
			System.out.println("-------session LastAccessed time-------"+httpSession.getLastAccessedTime());
			//httpSession.setMaxInactiveInterval(SESSION_EXP_TIME);
			System.out.println("-------session MaxInactiveInterval time-------"+httpSession.getMaxInactiveInterval());
			//httpSession.setMaxInactiveInterval(SESSION_EXP_TIME);
			System.out.println("-------session getClass time-------"+httpSession.getClass());
		}else{
			System.out.println("-------previous session-------");
			System.out.println("-------session-------"+httpSession.getId());
			System.out.println("-------session creation time-------"+httpSession.getCreationTime());
			System.out.println("-------session LastAccessed time-------"+httpSession.getLastAccessedTime());
			System.out.println("-------session MaxInactiveInterval time-------"+httpSession.getMaxInactiveInterval());
			System.out.println("-------session getClass time-------"+httpSession.getClass());
		}
		return "index";  
	}

	@RequestMapping("/header")  
	public String header() {  
		System.out.println(TAG+"header");
		return "header";  
	}

	@RequestMapping("/footer")  
	public String footer() {  
		System.out.println(TAG+"footer");
		return "footer";  
	}
	
	/*@Scheduled(fixedRate=5000)
	public void testMethod(){
		System.out.println("scheduled method running in directURL Cntlr: "+System.currentTimeMillis()/1000);
	}*/

	/*@RequestMapping("/login")  
	public String login() {  
		System.out.println(TAG+"Login");
		return "login";  
	}*/

	@RequestMapping("/operator")  
	public String operator() {  
		System.out.println(TAG+"Operator");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "operator"; 	 
	}

	@RequestMapping("/operatormenu")  
	public String operatorMenu() {  
		System.out.println(TAG+"operatormenu");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "blankMenu";
		else	
			return "operatormenu";		  
	}

	@RequestMapping("/adminmenu")  
	public String adminMenu() {  
		System.out.println(TAG+"adminmenu");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "blankMenu";
		else	
			return "adminmenu";  
	}

	@RequestMapping("/admin")  
	public String admin() {  
		System.out.println(TAG+"Admin");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "admin";  		
	}

	@RequestMapping("/superAdmin")  
	public String superAdmin() {  
		System.out.println(TAG+"superAdmin");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "superAdmin";  
	}


	@RequestMapping("/superAdminMenu")  
	public String superAdminMenu() {  
		System.out.println(TAG+"superAdminMenu");
		String authentication = authentication();
		System.err.println("Authentication : "+authentication);
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "blankMenu";
		else	
			return "superAdminMenu";    
	}

	@RequestMapping("/errorpage")  
	public String errorPage() {  
		System.out.println(TAG+"errorpage");
		return "errorpage";  
	}


	@RequestMapping("/dailycontract")  
	public String dailycontract() {  
		System.out.println(TAG+"dailycontract");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "dailycontract";   
	}

	@RequestMapping("/regularcontract")  
	public String regularcontract() {  
		System.out.println(TAG+"regularcontract");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "regularcontract";    
	}

	@RequestMapping("/viewcontract")  
	public String viewcontract() {  
		System.out.println(TAG+"viewcontract");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "viewcontract";
	}

	@RequestMapping("/employee")  
	public String employee() {  
		System.out.println(TAG+"employee");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
		{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrEmployee() = "+userRights.isUrEmployee());
			if(userRights.isUrEmployee()){
				return "employee";
			}else{
				return "sessionExpired";
			}
		}  
	}


	@RequestMapping("/viewemployee")  
	public String viewEmployee() {  
		System.out.println(TAG+"viewemployee");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrEmployee() = "+userRights.isUrEmployee());
			if(userRights.isUrEmployee()){
				return "viewemployee";
			}else{
				return "sessionExpired";
			}
		} 	  
	}

	@RequestMapping("/cnmt")  
	public String cnmt() {  
		System.out.println(TAG+"cnmt");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrCnmt() = "+userRights.isUrCnmt());
			if(userRights.isUrCnmt()){
				return "cnmt";
			}else{
				return "sessionExpired";
			}
		}  	  	 
	}
	
	@RequestMapping("/cnmtBbl")  
	public String cnmtBbl() {  
		System.out.println(TAG+"cnmtBbl");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrCnmt() = "+userRights.isUrCnmtBbl());
			if(userRights.isUrCnmtBbl()){
				return "cnmtBbl";
			}else{
				return "sessionExpired";
			}
		}  	  	 
	}

	@RequestMapping("/addcustomerrepresentative")  
	public String addcustomerrepresentative() {  
		System.out.println(TAG+"addcustomerrepresentative");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "addcustomerrepresentative";  	 
	}

	@RequestMapping("/customerrepresentativenew")  
	public String customerrepresentativenew() {  
		System.out.println(TAG+"customerrepresentativenew");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrCustomer() = "+userRights.isUrCustomer());
			if(userRights.isUrCustomer()){
				return "customerrepresentativenew";  
			}else{
				return "sessionExpired";
			}
		} 	 
	}

	@RequestMapping("/customer")  
	public String customer() {  
		System.out.println(TAG+"customer");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrCustomer() = "+userRights.isUrCustomer());
			if(userRights.isUrCustomer()){
				return "customer";  
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	@RequestMapping("/addGST")  
	public String addGST() {  
		System.out.println(TAG+"customer");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrCustomer() = "+userRights.isUrCustomer());
			if(userRights.isUrCustomer()){
				return "addGST";  
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	

	@RequestMapping("/viewcustomer")  
	public String viewcustomer() {  
		System.out.println(TAG+"viewcustomer");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrCustomer() = "+userRights.isUrCustomer());
			if(userRights.isUrCustomer()){
				return "viewcustomer";  
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/displaycustomerrepresentative")  
	public String displaycustomerrepresentative() {  
		System.out.println(TAG+"displaycustomerrepresentative");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrCustomer() = "+userRights.isUrCustomer());
			if(userRights.isUrCustomer()){
				return "displaycustomerrepresentative";	    
			}else{
				return "sessionExpired";
			}
		}
	}

	@RequestMapping("/editadmincustomer")  
	public String editadmincustomer() {  
		System.out.println(TAG+"editadmincustomer");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "editadmincustomer"; 	 
	}

	/*@RequestMapping("/viewregularcontract")  
	public String viewregularcontract() {  
		System.out.println(TAG+"viewregularcontract");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "viewregularcontract"; 	 
    }*/

	/*@RequestMapping("/viewdailycontract")  
	public String viewdailycontract() {  
		System.out.println(TAG+"viewdailycontract");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "viewdailycontract"; 	 	 
    }*/

	@RequestMapping("/editcontract")  
	public String editcontract() {  
		System.out.println(TAG+"editcontract");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "editcontract"; 		 
	}

	@RequestMapping("/editRegularContract")  
	public String editRegularContract() {  
		System.out.println(TAG+"editRegularContract");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "editregularcontract";  
	}

	@RequestMapping("/verifycontract")  
	public String verifycontract() {  
		System.out.println(TAG+"verifycontract");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "verifycontract";	  
	}

	/*@RequestMapping("/verifyregularcontract")  
	public String verifyregularcontract() {  
		System.out.println(TAG+"verifyregularcontract");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "verifyregularcontract"; 	 
    }*/

	@RequestMapping("/viewcnmt")  
	public String viewcnmt() {  
		System.out.println(TAG+"viewcnmt");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrCnmt() = "+userRights.isUrCnmt());
			if(userRights.isUrCnmt()){
				return "viewcnmt";
			}else{
				return "sessionExpired";
			}
		} 		 
	}


	@RequestMapping("/verifycustomer")  
	public String verifycustomer() {  
		System.out.println(TAG+"verifycustomer");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "verifycustomer";		  
	}

	@RequestMapping("/addUser")  
	public String addUser() {  
		System.out.println(TAG+"addUser");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "addUser";  
	}

	@RequestMapping("/viewUser")  
	public String viewUser() {  
		System.out.println(TAG+"viewUser");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "viewUser";  		
	}
	
	@RequestMapping("/changePassword")  
	public String changePassword() {  
		System.out.println(TAG+"changePassword");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "changePassword";  		
	}

	@RequestMapping("/stationary")  
	public String stationaryorder() {  
		System.out.println(TAG+"stationaryorder");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else	
			return "stationary";  	
	}

	@RequestMapping("/challan")  
	public String challan() {  
		System.out.println(TAG+"challan");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrChallan() = "+userRights.isUrChallan());
			if(userRights.isUrChallan()){
				return "challan";
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/viewChallan")  
	public String viewchallan() {  
		System.out.println(TAG+"viewchallan");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrChallan() = "+userRights.isUrChallan());
			if(userRights.isUrChallan()){
				return "viewchallan";
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/arrivalreport")  
	public String arrivalreport() {  
		System.out.println(TAG+"arrivalreport");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrSedr() = "+userRights.isUrSedr());
			if(userRights.isUrSedr()){
				return "arrivalreport";
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/viewarrivalreport")  
	public String viewarrivalreport() {  
		System.out.println(TAG+"viewarrivalreport");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrSedr() = "+userRights.isUrSedr());
			if(userRights.isUrSedr()){
				return "viewarrivalreport";
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/branch")  
	public String branch() {  
		System.out.println(TAG+"branch");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBranch() = "+userRights.isUrBranch());
			if(userRights.isUrBranch()){
				return "branch";
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/station")  
	public String station() {  
		System.out.println(TAG+"station");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrMisc() = "+userRights.isUrMisc());
			if(userRights.isUrMisc()){
				return "station";      
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/viewstation")  
	public String viewstation() {  
		System.out.println(TAG+"viewstation");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrMisc() = "+userRights.isUrMisc());
			if(userRights.isUrMisc()){
				return "viewstation";      
			}else{
				return "sessionExpired";
			}
		}  
	}


	@RequestMapping("/state")  
	public String state() {  
		System.out.println(TAG+"state");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrMisc() = "+userRights.isUrMisc());
			if(userRights.isUrMisc()){
				return "state";      
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/viewstate")  
	public String viewState() {  
		System.out.println(TAG+"viewstate");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrMisc() = "+userRights.isUrMisc());
			if(userRights.isUrMisc()){
				return "viewstate";      
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/vehicletype")  
	public String vehicletype() {  
		System.out.println(TAG+"vehicletype");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrMisc() = "+userRights.isUrMisc());
			if(userRights.isUrMisc()){
				return "vehicletype";    
			}else{
				return "sessionExpired";
			}
		}
	}

	@RequestMapping("/viewvehicletype")  
	public String viewvehicletype() {  
		System.out.println(TAG+"viewvehicletype");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrMisc() = "+userRights.isUrMisc());
			if(userRights.isUrMisc()){
				return "viewvehicletype";      
			}else{
				return "sessionExpired";
			}
		}
	}

	@RequestMapping("/producttype")  
	public String producttype() {  
		System.out.println(TAG+"producttype");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{	
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrMisc() = "+userRights.isUrMisc());
			if(userRights.isUrMisc()){
				return "producttype";  
			}else{
				return "sessionExpired";
			}
		}
	}

	@RequestMapping("/viewproducttype")  
	public String viewproducttype() {  
		System.out.println(TAG+"viewproducttype");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrMisc() = "+userRights.isUrMisc());
			if(userRights.isUrMisc()){
				return "viewproducttype";  
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/owner")  
	public String owner() {  
		System.out.println(TAG+"owner");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrOwner() = "+userRights.isUrOwner());
			if(userRights.isUrOwner()){
				return "owner"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/broker")  
	public String broker() {  
		System.out.println(TAG+"broker");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBroker() = "+userRights.isUrBroker());
			if(userRights.isUrBroker()){
				return "broker"; 
			}else{
				return "sessionExpired";
			}
		}  
	}


	@RequestMapping("/viewbranch")  
	public String viewbranch() {  
		System.out.println(TAG+"viewbranch");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBranch() = "+userRights.isUrBranch());
			if(userRights.isUrBranch()){
				return "viewbranch";
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/displayEmployee")  
	public String displayEmployee() {  
		System.out.println(TAG+"displayEmployee");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "displayEmployee";  
	}

	@RequestMapping("/displayBranch")  
	public String displayBranch() {  
		System.out.println(TAG+"displayBranch");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "displayBranch";  
	}

	@RequestMapping("/displayCustomer")  
	public String displaycustomer() {  
		System.out.println(TAG+"displaycustomer");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "displayCustomer";  
	}

	@RequestMapping("/displayCustRep")  
	public String displayCustRep() {  
		System.out.println(TAG+"displayCustRep");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "displayCustRep";  
	}

	@RequestMapping("/displayDailyContract")  
	public String displayContract() {  
		System.out.println(TAG+"displayDailyContract");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "displayDailyContract";  
	}

	@RequestMapping("/displayRegularContract")  
	public String displayRegularContract() {  
		System.out.println(TAG+"displayRegularContract");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "displayRegularContract";  
	}

	@RequestMapping("/displayCnmt")  
	public String displayCnmt() {  
		System.out.println(TAG+"displayCnmt");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "displayCnmt";  
	}

	@RequestMapping("/displayChallan")  
	public String displayChallan() {  
		System.out.println(TAG+"displayChallan");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "displayChallan";  
	}


	@RequestMapping("/displayBroker")  
	public String displayBroker() {  
		System.out.println(TAG+"displayBroker");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "displayBroker";  
	}

	@RequestMapping("/displayOwner")  
	public String displayOwner() {  
		System.out.println(TAG+"displayOwner");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "displayOwner";  
	}

	@RequestMapping("/displayArrivalReport")  
	public String displayArrivalReport() {  
		System.out.println(TAG+"displayArrivalReport");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "displayArrivalReport";  
	}

	@RequestMapping("/viewOwner")  
	public String viewOwner() {  
		System.out.println(TAG+"viewOwner");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrOwner() = "+userRights.isUrOwner());
			if(userRights.isUrOwner()){
				return "viewowner"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/viewBroker")  
	public String viewBroker() {  
		System.out.println(TAG+"viewBroker");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBroker() = "+userRights.isUrBroker());
			if(userRights.isUrBroker()){
				return "viewbroker"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/creatDispatchOrder")  
	public String creatDispatchOrder() {  
		System.out.println(TAG+"creatDispatchOrder");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "creatDispatchOrder";  
	}

	@RequestMapping("/dispatchDetails")  
	public String dispatchDetails() {  
		System.out.println(TAG+"dispatchDetails");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "dispatchDetails";  
	}

	@RequestMapping("/displaySupplier")  
	public String displaySupplier() {  
		System.out.println(TAG+"displaySupplier");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "displaySupplier";  
	}


	@RequestMapping("/supplier")  
	public String supplier() {  
		System.out.println(TAG+"supplier");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrStnSup() = "+userRights.isUrStnSup());
			if(userRights.isUrStnSup()){
				return "stationarySupplier"; 
			}else{
				return "sessionExpired";
			}
		} 
	}

	@RequestMapping("/viewsupplier")  
	public String viewsupplier() {  
		System.out.println(TAG+"viewsupplier");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrStnSup() = "+userRights.isUrStnSup());
			if(userRights.isUrStnSup()){
				return "viewStationarySupplier"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/stnOdrInv")  
	public String stnOdrInv() {  
		System.out.println(TAG+"stnOdrInv");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "stnOdrInv";  
	}

	@RequestMapping("/allowAdvance")  
	public String allowAdvance() {  
		System.out.println(TAG+"allowAdvance-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "allowAdvance";  
	}

	@RequestMapping("/serviceTax")  
	public String serviceTax() {  
		System.out.println(TAG+"serviceTax-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "serviceTax";  
	}

	@RequestMapping("/disallowAdvance")  
	public String disallowAdvance() {  
		System.out.println(TAG+"disallowAdvance-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "disallowAdvance";  
	}

	@RequestMapping("/contractRights")  
	public String contractRights() {  
		System.out.println(TAG+"contractRights-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "contractRights";  
	}

	
	@RequestMapping("/newVoucher")  
	public String newVoucher() {  
		System.out.println(TAG+"newVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newVoucher"; 
			}else{
				return "sessionExpired";
			}
		} 
	}

	@RequestMapping("/newVoucherBack")  
	public String newVoucherBack() {  
		System.out.println(TAG+"newVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newVoucherBack"; 
			}else{
				return "sessionExpired";
			}
		} 
	}

	
	
	@RequestMapping("/viewVoucher")  
	public String viewVoucher() {  
		System.out.println(TAG+"viewVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "viewVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/brkbnkDet")  
	public String brkbnkDet() {  
		System.out.println(TAG+"brkbnkDet-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBroker() = "+userRights.isUrBroker());
			if(userRights.isUrBroker()){
				return "brkbnkDet"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/ownbnkDet")  
	public String ownbnkDet() {  
		System.out.println(TAG+"ownbnkDet-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrOwner() = "+userRights.isUrOwner());
			if(userRights.isUrOwner()){
				return "ownbnkDet"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/dataIntegration")  
	public String dataIntegration() {  
		System.out.println(TAG+"dataIntegration-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "dataIntegration";  
	}


	@RequestMapping("/dlyContAuth")  
	public String dlyContAuth() {  
		System.out.println(TAG+"dlyContAuth-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "dlyContAuth";  
	}

	@RequestMapping("/newBank")  
	public String newBank() {  
		System.out.println(TAG+"newBank-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBank() = "+userRights.isUrBank());
			if(userRights.isUrBank()){
				return "newBank";   
			}else{
				return "sessionExpired";
			}
		}
	}

	@RequestMapping("/viewBank")  
	public String viewBank() {  
		System.out.println(TAG+"viewBank-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBank() = "+userRights.isUrBank());
			if(userRights.isUrBank()){
				return "viewBank";   
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/assignBank")  
	public String assignBank() {  
		System.out.println(TAG+"viewBank-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBank() = "+userRights.isUrBank());
			if(userRights.isUrBank()){
				return "assignBank";   
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/viewCashReciept")  
	public String viewCashReciept() {  
		System.out.println(TAG+"viewCashReciept-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "viewCashReciept"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/newCashReciept")  
	public String newCashReciept() {  
		System.out.println(TAG+"newCashReciept-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newCashReciept"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	@RequestMapping("/newCashRecieptBack")  
	public String newCashRecieptBack() {  
		System.out.println(TAG+"newCashReciept-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newCashRecieptBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	

	@RequestMapping("/closeVoucher")  
	public String closeVoucher() {  
		System.out.println(TAG+"closeVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "closeVoucher";   
			}else{
				return "sessionExpired";
			}
		}
	}
	
	@RequestMapping("/closeVoucherBack")  
	public String closeVoucherBack() {  
		System.out.println(TAG+"closeVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "closeVoucherBack";   
			}else{
				return "sessionExpired";
			}
		}
	}
	

	@RequestMapping("/newCashDeposite")  
	public String newCashDeposite() {  
		System.out.println(TAG+"newCashDeposite-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newCashDeposite"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	@RequestMapping("/newCashDepositeBack")  
	public String newCashDepositeBack() {  
		System.out.println(TAG+"newCashDeposite-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newCashDepositeBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	
	@RequestMapping("/newCashPayment")  
	public String newCashPayment() {  
		System.out.println(TAG+"newCashPayment-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newCashPayment"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/newCashPaymentBack")  
	public String newCashPaymentBack() {  
		System.out.println(TAG+"newCashPaymentBack-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newCashPaymentBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	

	@RequestMapping("/newTelephoneVoucher")  
	public String newTelephoneVoucher() {  
		System.out.println(TAG+"newTelephoneVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newTelephoneVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	@RequestMapping("/newTelephoneVoucherBack")  
	public String newTelephoneVoucherBack() {  
		System.out.println(TAG+"newTelephoneVoucherBack-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newTelephoneVoucherBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	
	@RequestMapping("/newPhoneAllot")  
	public String newPhoneAllot() {  
		System.out.println(TAG+"newPhoneAllot-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrTel() = "+userRights.isUrTel());
			if(userRights.isUrTel()){
				return "newPhoneAllot";   
			}else{
				return "sessionExpired";
			}
		}
	}

	@RequestMapping("/newCRNAllot")  
	public String newCRNAllot() {  
		System.out.println(TAG+"newCRNAllot-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrEle() = "+userRights.isUrEle());
			if(userRights.isUrEle()){
				return "newCRNAllot";   
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/viewCRNAllot")  
	public String viewCRNAllot() {  
		System.out.println(TAG+"viewCRNAllot-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrEle() = "+userRights.isUrEle());
			if(userRights.isUrEle()){
				return "viewCRNAllot";   
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/newElectVoucher")  
	public String newElectVoucher() {  
		System.out.println(TAG+"newElectVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newElectVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	@RequestMapping("/newElectVoucherBack")  
	public String newElectVoucherBack() {  
		System.out.println(TAG+"newElectVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newElectVoucherBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	

	@RequestMapping("/viewElectVoucher")  
	public String viewElectVoucher() {  
		System.out.println(TAG+"viewElectVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "viewElectVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/newVehAllot")  
	public String newVehAllot() {  
		System.out.println(TAG+"newVehAllot-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVeh() = "+userRights.isUrVeh());
			if(userRights.isUrVeh()){
				return "newVehAllot";   
			}else{
				return "sessionExpired";
			}
		}
	}


	@RequestMapping("/viewVehAllot")  
	public String viewVehAllot() {  
		System.out.println(TAG+"viewVehAllot-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVeh() = "+userRights.isUrVeh());
			if(userRights.isUrVeh()){
				return "viewVehAllot";   
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/newVehVoucher")  
	public String newVehVoucher() {  
		System.out.println(TAG+"newVehVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newVehVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	@RequestMapping("/newVehVoucherBack")  
	public String newVehVoucherBack() {  
		System.out.println(TAG+"newVehVoucherBack-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newVehVoucherBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	
	@RequestMapping("/viewVehVoucher")  
	public String viewVehVoucher() {  
		System.out.println(TAG+"viewVehVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "viewVehVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}


	@RequestMapping("/newTravVoucher")  
	public String newTravVoucher() {  
		System.out.println(TAG+"newTravVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newTravVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	@RequestMapping("/newTravVoucherBack")  
	public String newTravVoucherBack() {  
		System.out.println(TAG+"newTravVoucherBack-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newTravVoucherBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	
	@RequestMapping("/viewTravVoucher")  
	public String viewTravVoucher() {  
		System.out.println(TAG+"viewTravVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "viewTravVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/newBPVoucher")  
	public String newBPVoucher() {  
		System.out.println(TAG+"newBPVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newBPVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	

	@RequestMapping("/newBPVoucherBack")  
	public String newBPVoucherBack() {  
		System.out.println(TAG+"newBPVoucherBack-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newBPVoucherBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	@RequestMapping("/viewBPVoucher")  
	public String viewBPVoucher() {  
		System.out.println(TAG+"viewBPVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "viewBPVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/hoChqRequest")  
	public String hoChqRequest() {  
		System.out.println(TAG+"hoChqRequest-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrChq() = "+userRights.isUrChq());
			if(userRights.isUrChq()){
				return "hoChqRequest";   
			}else{
				return "sessionExpired";
			}
		}
	}
	
	@RequestMapping("/hoChqReceive")  
	public String hoChqReceive() {  
		System.out.println(TAG+"hoChqReceive-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrChq() = "+userRights.isUrChq());
			if(userRights.isUrChq()){
				return "hoChqReceive";   
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/hoChqIssue")  
	public String hoChqIssue() {  
		System.out.println(TAG+"hoChqIssue-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrChq() = "+userRights.isUrChq());
			if(userRights.isUrChq()){
				return "hoChqIssue";   
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/hoChqStatus")  
	public String hoChqStatus() {  
		System.out.println(TAG+"hoChqStatus-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrChq() = "+userRights.isUrChq());
			if(userRights.isUrChq()){
				return "hoChqStatus";   
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/brChqRequest")  
	public String brChqReques() {  
		System.out.println(TAG+"brChqReques-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "brChqRequest";  
	}
	
	@RequestMapping("/brChqReceive")  
	public String brChqReceive() {  
		System.out.println(TAG+"brChqReceive-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrChq() = "+userRights.isUrChq());
			if(userRights.isUrChq()){
				return "brChqReceive";    
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/brChqStatus")  
	public String brChqStatus() {  
		System.out.println(TAG+"brChqStatus-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrChq() = "+userRights.isUrChq());
			if(userRights.isUrChq()){
				return "brChqStatus";    
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	@RequestMapping("/newChqPayVoucher")  
	public String newChqPayVoucher() {  
		System.out.println(TAG+"newChqPayVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newChqPayVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	@RequestMapping("/newChqPayVoucherBack")  
	public String newChqPayVoucherBack() {  
		System.out.println(TAG+"newChqPayVoucherBack-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newChqPayVoucherBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	
	@RequestMapping("/viewChqPayVoucher")  
	public String viewChqPayVoucher() {  
		System.out.println(TAG+"viewChqPayVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "viewChqPayVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	@RequestMapping("/newJourVoucher")  
	public String newJourVoucher() {  
		System.out.println(TAG+"newJourVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newJourVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/viewJourVoucher")  
	public String viewJourVoucher() {  
		System.out.println(TAG+"viewJourVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "viewJourVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/newRevVoucher")  
	public String newRevVoucher() {  
		System.out.println(TAG+"newRevVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newRevVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	
	@RequestMapping("/newRevVoucherBack")  
	public String newRevVoucherBack() {  
		System.out.println(TAG+"newRevVoucherBack-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newRevVoucherBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	@RequestMapping("/ViewRevVoucherCntlr")  
	public String ViewRevVoucherCntlr() {  
		System.out.println(TAG+"ViewRevVoucherCntlr-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "ViewRevVoucherCntlr"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/newIntBrTVCrDr")  
	public String newIntBrTVCrDr() {  
		System.out.println(TAG+"newIntBrTVCrDr-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newIntBrTVCrDr"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	
	@RequestMapping("/newIntBrTVCrDrBack")  
	public String newIntBrTVCrDrBack() {  
		System.out.println(TAG+"newIntBrTVCrDrBack-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newIntBrTVCrDrBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	
	@RequestMapping("/viewIntBrTVCrDr")  
	public String viewIntBrTVCrDr() {  
		System.out.println(TAG+"viewIntBrTVCrDr-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "viewIntBrTVCrDr"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/clrPendingIBTV")  
	public String clrPendingIBTV() {  
		System.out.println(TAG+"clrPendingIBTV-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "clrPendingIBTV"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	@RequestMapping("/clrPendingIBTVBack")  
	public String clrPendingIBTVBack() {  
		System.out.println(TAG+"clrPendingIBTVBack-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "clrPendingIBTVBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	
	@RequestMapping("/empWiseAssgn")  
	public String empWiseAssgn() {  
		System.out.println(TAG+"empWiseAssgn-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrAssigned() = "+userRights.isUrAssigned());
			if(userRights.isUrAssigned()){
				return "empWiseAssgn";  
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/brhWiseAssgn")  
	public String brhWiseAssgn() {  
		System.out.println(TAG+"brhWiseAssgn-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrAssigned() = "+userRights.isUrAssigned());
			if(userRights.isUrAssigned()){
				return "brhWiseAssgn";  
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/asgnUser")  
	public String asgnUser() {  
		System.out.println(TAG+"asgnUser-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrAssigned() = "+userRights.isUrAssigned());
			if(userRights.isUrAssigned()){
				return "asgnUser";  
			}else{
				return "sessionExpired";
			}
		}
	}
	
	@RequestMapping("/addContToStn")  
	public String addContToStn() {  
		System.out.println(TAG+"addContToStn-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "addContToStn";  
	}
	
	@RequestMapping("/cnDetReport")  
	public String cnDetReport() {  
		System.out.println(TAG+"cnDetReport-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "cnDetReport";  
	}
	
	@RequestMapping("/cnMisReport")  
	public String cnMisReport() {  
		System.out.println(TAG+"cnDetReport-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "cnMisReport";  
	}
	
	@RequestMapping("/userRights")  
	public String userRights() {  
		System.out.println(TAG+"userRights-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "userRights";  
	}
	
	@RequestMapping("/newLhpvTemp")  
	public String newLhpvTemp() {  
		System.out.println(TAG+"newLhpvTemp-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "newLhpvTemp";  
	}
	
	@RequestMapping("/bTBFundTrans")  
	public String bTBFundTrans() {  
		System.out.println(TAG+"bTBFundTrans-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrFT() = "+userRights.isUrFT());
			if(userRights.isUrFT()){
				return "bTBFundTrans";   
			}else{
				return "sessionExpired";
			}
		}
	}
	
	@RequestMapping("/newRent")  
	public String newRent() {  
		System.out.println(TAG+"newRent-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrRent() = "+userRights.isUrRent());
			if(userRights.isUrRent()){
				return "newRent";   
			}else{
				return "sessionExpired";
			}
		}
	}

	@RequestMapping("/newRentVoucher")  
	public String newRentVoucher() {  
		System.out.println(TAG+"newRentVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newRentVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/newRentVoucherBack")  
	public String newRentVoucherBack() {  
		System.out.println(TAG+"newRentVoucherBack-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newRentVoucherBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	
	@RequestMapping("/viewRent")  
	public String viewRent() {  
		System.out.println(TAG+"viewRent-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrRent() = "+userRights.isUrRent());
			if(userRights.isUrRent()){
				return "viewRent"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/newAtmCard")  
	public String newAtmCard() {  
		System.out.println(TAG+"newAtmCard-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrAtm() = "+userRights.isUrAtm());
			if(userRights.isUrAtm()){
				return "newAtmCard";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/viewAtmCard")  
	public String viewAtmCard() {  
		System.out.println(TAG+"viewAtmCard-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrAtm() = "+userRights.isUrAtm());
			if(userRights.isUrAtm()){
				return "viewAtmCard";  
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/newAtmVoucher")  
	public String newAtmVoucher() {  
		System.out.println(TAG+"newAtmVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newAtmVoucher"; 
			}else{
				return "sessionExpired";
			}
		}
	}
	
	
	@RequestMapping("/newAtmVoucherBack")  
	public String newAtmVoucherBack() {  
		System.out.println(TAG+"newAtmVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newAtmVoucherBack"; 
			}else{
				return "sessionExpired";
			}
		}
	}
	
	
	
	@RequestMapping("/viewAtmVoucher")  
	public String viewAtmVoucher() {  
		System.out.println(TAG+"viewAtmVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "viewAtmVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/cancelChq")  
	public String cancelChq() {  
		System.out.println(TAG+"cancelChq-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "cancelChq";   
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	@RequestMapping("/billPrint")  
	public String billPrint() {  
		System.out.println(TAG+"billPrint-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrBill());
			if(userRights.isUrBillPrint()){
				return "billPrint";   
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	@RequestMapping("/mrPrint")  
	public String mrPrint() {  
		System.out.println(TAG+"mrPrint-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "mrPrint";   
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	@RequestMapping("/mrrPrint")  
	public String mrrPrint() {  
		System.out.println(TAG+"mrrPrint-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "mrrPrint";   
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/cashStmt")  
	public String cashStmt() {  
		System.out.println(TAG+"cashStmt-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "cashStmt";   
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/dlyRtAllow")  
	public String dlyRtAllow() {  
		System.out.println(TAG+"dlyRtAllow-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "dlyRtAllow";  
	}
	
	@RequestMapping("/newVehicleVendor")  
	public String newVehicleVendor() {  
		System.out.println(TAG+"newVehicleVendor-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVehVen() = "+userRights.isUrVehVen());
			if(userRights.isUrVehVen()){
				return "newVehicleVendor";  
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/viewVehicleVendor")  
	public String viewVehicleVendor() {  
		System.out.println(TAG+"viewVehicleVendor-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVehVen() = "+userRights.isUrVehVen());
			if(userRights.isUrVehVen()){
				return "viewVehicleVendor";  
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/editVehicleVendor")  
	public String editVehicleVendor() {  
		System.out.println(TAG+"editVehicleVendor-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVehVen() = "+userRights.isUrVehVen());
			if(userRights.isUrVehVen()){
				return "editVehicleVendor";    
			}else{
				return "sessionExpired";
			}
		}
	}
	
	@RequestMapping("/addPan")  
	public String addPan() {  
		System.out.println(TAG+"addPan-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVehVen() = "+userRights.isUrVehVen());
			if(userRights.isUrVehVen()){
				return "addPan";    
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/billTemp")  
	public String billTemp() {  
		System.out.println(TAG+"billTemp-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "billTemp";  
	}
	
	
	@RequestMapping("/newLhpvAdv")  
	public String newLhpvAdv() {  
		System.out.println(TAG+"newLhpvAdv-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newLhpvAdv"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	@RequestMapping("/newLhpvAdvBack")  
	public String newLhpvAdvBack() {  
		System.out.println(TAG+"newLhpvAdv-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newLhpvAdvBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
/*	@RequestMapping("/viewLhpvAdv")  
	public String viewLhpvAdv() {  
		System.out.println(TAG+"viewLhpvAdv-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "viewLhpvAdv"; 
			}else{
				return "sessionExpired";
			}
		}  
	}*/
	
	@RequestMapping("/rvrsLhpvAdv")  
	public String rvrsLhpvAdv() {  
		System.out.println(TAG+"rvrsLhpvAdv-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrRvrsLhpvAdv() = "+userRights.isUrRvrsLhpvAdv());
			if(userRights.isUrRvrsLhpvAdv()){
				return "rvrsLhpvAdv"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	@RequestMapping("/newLhpvBal")  
	public String newLhpvBal() {  
		System.out.println(TAG+"newLhpvBal-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newLhpvBal"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	@RequestMapping("/newLhpvBalBack")  
	public String newLhpvBalBack() {  
		System.out.println(TAG+"newLhpvBalBack-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newLhpvBalBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	
	@RequestMapping("/viewLhpvBal")  
	public String viewLhpvBal() {  
		System.out.println(TAG+"viewLhpvBal-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "viewLhpvBal"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/newArvRepAlw")  
	public String newArvRepAlw() {  
		System.out.println(TAG+"newArvRepAlw-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrSedrAlw() = "+userRights.isUrSedrAlw());
			if(userRights.isUrSedrAlw()){
				return "newArvRepAlw"; 
			}else{
				return "sessionExpired";
			}
		}
			  
	}
	
	@RequestMapping("/viewArvRepAlw")  
	public String viewArvRepAlw() {  
		System.out.println(TAG+"viewArvRepAlw-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "viewArvRepAlw";  
	}
	
	
	@RequestMapping("/newLhpvSup")  
	public String newLhpvSup() {  
		System.out.println(TAG+"newLhpvSup-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newLhpvSup"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	@RequestMapping("/newLhpvSupBack")  
	public String newLhpvSupBack() {  
		System.out.println(TAG+"newLhpvSupBack-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newLhpvSupBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	
	@RequestMapping("/viewLhpvSup")  
	public String viewLhpvSup() {  
		System.out.println(TAG+"viewLhpvSup-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "viewLhpvSup"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/closeLhpv")  
	public String closeLhpv() {  
		System.out.println(TAG+"closeLhpv-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "closeLhpv"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	@RequestMapping("/closeLhpvBack")  
	public String closeLhpvBack() {  
		System.out.println(TAG+"closeLhpv-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "closeLhpvBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	
	@RequestMapping("/ledger")  
	public String ledger() {  
		System.out.println(TAG+"ledger-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "ledger";  
	}
	
	@RequestMapping("/trial")  
	public String trial() {  
		System.out.println(TAG+"trial-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "trial";  
	}
	
	@RequestMapping("/brhWiseMuns")  
	public String brhWiseMuns() {  
		System.out.println(TAG+"brhWiseMuns-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "brhWiseMuns";  
	}
	
	@RequestMapping("/viewBrhWiseMuns")  
	public String viewBrhWiseMuns() {  
		System.out.println(TAG+"viewBrhWiseMuns-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "viewBrhWiseMuns";  
	}
	
	@RequestMapping("/newChqCancelVoucher")  
	public String newChqCancelVoucher() {  
		System.out.println(TAG+"newChqCancelVoucher-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newChqCancelVoucher"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	
	@RequestMapping("/newChqCancelVoucherBack")  
	public String newChqCancelVoucherBack() {  
		System.out.println(TAG+"newChqCancelVoucherBack-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "newChqCancelVoucherBack"; 
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	
	@RequestMapping("/editCashStmt")  
	public String editCashStmt() {  
		System.out.println(TAG+"editCashStmt-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrEditCS() = "+userRights.isUrEditCS());
			if(userRights.isUrEditCS()){
				return "editCashStmt";   
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	

	@RequestMapping("/lhpvPrint")  
	public String lhpvPrint() {  
		System.out.println(TAG+"lhpvPrint-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "lhpvPrint";  
	}
	
	@RequestMapping("/extContDt")  
	public String extContDt() {  
		System.out.println(TAG+"extContDt-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "extContDt";  
	}
	
	@RequestMapping("/editCnmt")  
	public String editCnmt() {  
		System.out.println(TAG+"editCnmt-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrEditCnmt() = "+userRights.isUrEditCnmt());
			if(userRights.isUrEditCnmt()){
				return "editCnmt";   
			}else{
				return "sessionExpired";
			}
		}
			  
	}
	
	@RequestMapping("/panValid")  
	public String panValid() {  
		System.out.println(TAG+"panValid-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrPanVal() = "+userRights.isUrPanVal());
			if(userRights.isUrPanVal()){
				return "panValid";  
			}else{
				return "sessionExpired";
			}
		}		
	}
	
	
	@RequestMapping("/newBill")  
	public String newBill() {  
		System.out.println(TAG+"newBill-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill() = "+userRights.isUrBill());
			if(userRights.isUrBill()){
				return "newBill";
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/newBillHO")  
	public String newBillHO() {  
		System.out.println(TAG+"newBill-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill() = "+userRights.isUrBill());
			if(userRights.isUrBill()){
				return "newBillHO";
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/custBillPrmsn")  
	public String custBillPrmsn() {  
		System.out.println(TAG+"custBillPrmsn-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "custBillPrmsn";  
	}
	
	@RequestMapping("/editChln")  
	public String editChln() {  
		System.out.println(TAG+"editChln-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrEditChln() = "+userRights.isUrEditChln());
			if(userRights.isUrEditChln()){
				return "editChln";   
			}else{
				return "sessionExpired";
			}
		}
	}
	
	@RequestMapping("/chlnNCnmt")  
	public String chlnNCnmt() {  
		System.out.println(TAG+"chlnNCnmt-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "chlnNCnmt";  
	}
	
	@RequestMapping("/billFrwd")  
	public String billFrwd() {  
		System.out.println(TAG+"billFrwd-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill() = "+userRights.isUrBill());
			if(userRights.isUrBill()){
				return "billFrwd"; 
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/billSbmsn")  
	public String billSbmsn() {  
		System.out.println(TAG+"billSbmsn-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill() = "+userRights.isUrBill());
			if(userRights.isUrBill()){
				return "billSbmsn";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	
	@RequestMapping("/newOnAccMR")  
	public String newOnAccMR() {  
		System.out.println(TAG+"newOnAccMR-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill() = "+userRights.isUrBill());
			if(userRights.isUrMR()){
				return "newOnAccMR";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/newOnAccMRN")  
	public String newOnAccMRN() {  
		System.out.println(TAG+"newOnAccMRN-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill() = "+userRights.isUrBill());
			if(userRights.isUrMR()){
				return "newOnAccMRN";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	
	@RequestMapping("/newOnAccMRBack")  
	public String newOnAccMRBack() {  
		System.out.println(TAG+"newOnAccMRBack-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill() = "+userRights.isUrBill());
			if(userRights.isUrMR()){
				return "newOnAccMRBack";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	
	
	@RequestMapping("/payDetMR")  
	public String payDetMR() {  
		System.out.println(TAG+"payDetMR-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill() = "+userRights.isUrBill());
			if(userRights.isUrMR()){
				return "payDetMR";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	
	@RequestMapping("/payDetMRN")  
	public String payDetMRN() {  
		System.out.println(TAG+"payDetMRN-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill() = "+userRights.isUrBill());
			if(userRights.isUrMR()){
				return "payDetMRN";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	
	@RequestMapping("/payDetMRBack")  
	public String payDetMRBack() {  
		System.out.println(TAG+"payDetMRBack-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill() = "+userRights.isUrBill());
			if(userRights.isUrMR()){
				return "payDetMRBack";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	
	@RequestMapping("/dirPayMR")  
	public String dirPayMR() {  
		System.out.println(TAG+"dirPayMR-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill() = "+userRights.isUrBill());
			if(userRights.isUrMR()){
				return "dirPayMR";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	

	@RequestMapping("/dirPayMRN")  
	public String dirPayMRN() {  
		System.out.println(TAG+"dirPayMRN-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill() = "+userRights.isUrBill());
			if(userRights.isUrMR()){
				return "dirPayMRN";  
			}else{
				return "sessionExpired";
			}
		} 
	}

	
	
	@RequestMapping("/dirPayMRBack")  
	public String dirPayMRBack() {  
		System.out.println(TAG+"dirPayMRBack-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill() = "+userRights.isUrBill());
			if(userRights.isUrMR()){
				return "dirPayMRBack";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	
	
	@RequestMapping("/lhpvStartTemp")  
	public String lhpvStartTemp() {  
		System.out.println(TAG+"lhpvStartTemp-------");
		String authentication = authentication();
		/*if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill() = "+userRights.isUrBill());
			if(userRights.isUrMR()){
				return "lhpvStartTemp";  
			}else{
				return "sessionExpired";
			}
		} */
		
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			return "lhpvStartTemp";
		}  
	}
	
	@RequestMapping("/enquiry")  
	public String enquiry() {  
		System.out.println(TAG+"enquiry-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrEnquiry() = "+userRights.isUrEnquiry());
			if(userRights.isUrEnquiry()){
				return "enquiry";   
			}else{
				return "sessionExpired";
			}
		}
	}
	
	@RequestMapping("/brReconRpt")  
	public String brReconRpt() {  
		System.out.println(TAG+"brReconRpt-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrReports() = "+userRights.isUrReports());
			if(userRights.isUrReports()){
				return "brReconRpt";   
			}else{
				return "sessionExpired";
			}
		}
	}
	
	@RequestMapping("/stockAndOsRpt")
	public String stockAndOsRpt(){
		System.out.println(TAG+"stockAndOsRpt-------");
		String authentication = authentication();
		if (authentication.equalsIgnoreCase(UN_AUTH)) {
			return "sessionExpired";
		} else {
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrReports() = "+userRights.isUrReports());
			if(userRights.isUrReports()){
				return "stockAndOsRpt";   
			}else{
				return "sessionExpired";
			}
		}
	}
	
	@RequestMapping("/stockRpt")
	public String stockRpt(){
		System.out.println(TAG+"stockRpt--------");
		String authentication = authentication();
		if (authentication.equalsIgnoreCase(UN_AUTH)) {
			return "sessionExpired";
		} else {
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println(""+userRights.isUrReports());
			if (userRights.isUrReports()) {
				return "stockRpt";
			} else {
				return "sessionExpired";
			}
		}
	}
	
	@RequestMapping("/osRpt")
	public String osRpt(){
		System.out.println(TAG+"osRpt--------");
		String authentication = authentication();
		if (authentication.equalsIgnoreCase(UN_AUTH)) {
			return "sessionExpired";
		} else {
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println(""+userRights.isUrReports());
			if (userRights.isUrReports()) {
				return "osRpt";
			} else {
				return "sessionExpired";
			}
		}
	}
	
	
	@RequestMapping("/relOsRpt")//relianceStock
	public String relOsRpt(){
		System.out.println(TAG+"relOsRpt--------");
		String authentication = authentication();
		if (authentication.equalsIgnoreCase(UN_AUTH)) {
			return "sessionExpired";
		} else {
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println(""+userRights.isUrReports());
			if (userRights.isUrReports()) {
				return "relOsRpt";
			} else {
				return "sessionExpired";
			}
		}
	}
	
	@RequestMapping("/relianceStock")
	public String relianceStock(){
		System.out.println(TAG+"relianceStock--------");
		String authentication = authentication();
		if (authentication.equalsIgnoreCase(UN_AUTH)) {
			return "sessionExpired";
		} else {
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println(""+userRights.isUrReports());
			if (userRights.isUrReports()) {
				return "relianceStock";
			} else {
				return "sessionExpired";
			}
		}
	}
	
	@RequestMapping("/newRelianceStock")
	public String newRelianceStock(){
		System.out.println(TAG+"relianceStock--------");
		String authentication = authentication();
		if (authentication.equalsIgnoreCase(UN_AUTH)) {
			return "sessionExpired";
		} else {
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println(""+userRights.isUrReports());
			if (userRights.isUrReports()) {
				return "newRelianceStock";
			} else {
				return "sessionExpired";
			}
		}
	}

	
	
	
	@RequestMapping("/osRptN")
	public String osRptN(){
		System.out.println(TAG+"osRptN--------");
		String authentication = authentication();
		if (authentication.equalsIgnoreCase(UN_AUTH)) {
			return "sessionExpired";
		} else {
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println(""+userRights.isUrReports());
			if (userRights.isUrReports()) {
				return "osRptN";
			} else {
				return "sessionExpired";
			}
		}
	}
	
	@RequestMapping("/reverseMR")  
	public String reverseMR() {  
		System.out.println(TAG+"reverseMR-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrMR() = "+userRights.isUrMR());
			if(userRights.isUrMR()){
				return "reverseMR";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/reverseMRN")  
	public String reverseMRN() {  
		System.out.println(TAG+"reverseMRN-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrMR() = "+userRights.isUrMR());
			if(userRights.isUrMR()){
				return "reverseMRN";  
			}else{
				return "sessionExpired";
			}
		} 
	}

	
	
	@RequestMapping("/reverseMRBack")  
	public String reverseMRBack() {  
		System.out.println(TAG+"reverseMRBack-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrMR() = "+userRights.isUrMR());
			if(userRights.isUrMR()){
				return "reverseMRBack";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	
	
	@RequestMapping("/custStationary")  
	public String custStationary() {  
		System.out.println(TAG+"custStationary-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrMR() = "+userRights.isUrCustStationary());
			if(userRights.isUrCustStationary()){
				return "custStationary";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/brhStkInfm")  
	public String brhStkInfm() {  
		System.out.println(TAG+" ****** BrhStkInfm ******");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrMR() = "+userRights.isUrCustStationary());
			if(userRights.isUrCustStationary()){
				return "brhStkInfm";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/stnTransfer")  
	public String stnTransfer() {  
		System.out.println(TAG+" ****** StnTransfer ******");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrMR() = "+userRights.isUrCustStationary());
			if(userRights.isUrCustStationary()){
				return "stnTransfer";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	
	
	@RequestMapping("/billFrwdPrint")  
	public String billFrwdPrint() {  
		System.out.println(TAG+"billFrwdPrint-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrBillPrint()){
				return "billFrwdPrint";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	

	@RequestMapping("/billCancel")  
	public String billCancel() {  
		System.out.println(TAG+"billCancel-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill = "+userRights.isUrBillCancel());
			if(userRights.isUrBillCancel()){
				return "billCancel";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	
	@RequestMapping("/cancelMR")  
	public String cancelMR() {  
		System.out.println(TAG+"cancelMR-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrCancelMr = "+userRights.isUrCancelMr());
			if(userRights.isUrCancelMr()){
				return "cancelMR";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/cancelMRN")  
	public String cancelMRN() {  
		System.out.println(TAG+"cancelMRN-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrCancelMr = "+userRights.isUrCancelMr());
			if(userRights.isUrCancelMr()){
				return "cancelMRN";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	
	
	@RequestMapping("/editBill")  
	public String editBill() {  
		System.out.println(TAG+"editBill-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrEditBill = "+userRights.isUrEditBill());
			if(userRights.isUrEditBill()){
				return "editBill";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	
	/*@RequestMapping("/relBill")  
	public String relBill() {  
		System.out.println(TAG+"relBill-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "relBill";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	*/
	
	
	@RequestMapping("/relBill")  
	public String relBill() {  
		System.out.println(TAG+"relBill-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrRelBl() = "+userRights.isUrRelBl());
			if(userRights.isUrRelBl()){
				return "relBill";   
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/lhpvAdvTemp")  
	public String lhpvAdvTemp() {  
		System.out.println(TAG+"lhpvAdvTemp-----");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "lhpvAdvTemp";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	
	@RequestMapping("/lhpvBalTemp")  
	public String lhpvBalTemp() {  
		System.out.println(TAG+"lhpvBalTemp-----");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "lhpvBalTemp";  
			}else{
				return "sessionExpired";
			}
		} 
	}	
	
	
	@RequestMapping("/closeLhpvTemp")  
	public String closeLhpvTemp() {  
		System.out.println(TAG+"closeLhpvTemp-----");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "closeLhpvTemp";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/cancelChallan")  
	public String cancelChallan() {  
		System.out.println(TAG+"cancelChallan-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrCancelChln = "+userRights.isUrCancelChln());
			if(userRights.isUrCancelChln()){
				return "cancelChallan";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/cancelCnmt")  
	public String cancelCnmt() {  
		System.out.println(TAG+"cancelCnmt-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrCancelCnmt = "+userRights.isUrCancelCnmt());
			if(userRights.isUrCancelCnmt()){
				return "cancelCnmt";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	
	@RequestMapping("/cancelSedr")  
	public String cancelSedr() {  
		System.out.println(TAG+"cancelSedr-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrCancelSedr = "+userRights.isUrCancelSedr());
			if(userRights.isUrCancelSedr()){
				return "cancelSedr";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/rvrsLhpvBal")  
	public String rvrsLhpvBal() {  
		System.out.println(TAG+"rvrsLhpvBal-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrRvrsLhpvBal() = "+userRights.isUrRvrsLhpvBal());
			if(userRights.isUrRvrsLhpvBal()){
				return "rvrsLhpvBal"; 
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	@RequestMapping("/challanApp")
	public String challanApp(){		
		System.out.println(TAG+"closeLhpvTemp-----");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "challanApp";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/countEntry")
	public String countEntry(){		
		System.out.println(TAG+"closeLhpvTemp-----");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "countEntry";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/cnmtApp")
	public String cnmtApp(){		
		System.out.println(TAG+"CNMTAPP-----");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "cnmtApp";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/vryOwner")
	public String ownerAppView(){		
		System.out.println(TAG+"-----");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "ownerApp";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/vryBroker")
	public String brokerAppView(){		
		System.out.println(TAG+"-----");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "brokerApp";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/allImg")
	public String allImg(){		
		System.out.println(TAG+"-----");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "allImg";
	}
	
	@RequestMapping("/vryVehicle")
	public String vehicleAppView(){		
		System.out.println(TAG+"-----");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrVouch() = "+userRights.isUrVouch());
			if(userRights.isUrVouch()){
				return "vehicleApp";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/shortExcessRpt")
	public String shortExcessRpt(){
		System.out.println(TAG+"shortExcess...");
		String authenication=authentication();
		if(authenication.equalsIgnoreCase(UN_AUTH)){
			return "sessionExpired";
		}else{
	    	UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrReports()= "+userRights.isUrReports());
			if(userRights.isUrReports()){
				return "shortExcessRpt";  
			}else{
				return "sessionExpired";
			}
		}
	}
	
	@RequestMapping("/cnmtCosting")  
	public String cnmtCosting() {  
		System.out.println(TAG+"cnmtCosting-------");
		String authenication=authentication();
		if(authenication.equalsIgnoreCase(UN_AUTH)){
			return "sessionExpired";
		}else{
	    	UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrReports()= "+userRights.isUrReports());
			if(userRights.isUrCosting()){
				return "cnmtCosting";  
			}else{
				return "sessionExpired";
			}
		}
	}
	
	
	@RequestMapping("/cnmtFrmToDtl")  
	public String cnmtFrmToDtl() {  
		System.out.println(TAG+"cnmtFrmToDtl-------");
		String authenication=authentication();
		if(authenication.equalsIgnoreCase(UN_AUTH)){
			return "sessionExpired";
		}else{
	    	UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrReports()= "+userRights.isUrReports());
			if(userRights.isUrReports()){
				return "cnmtFrmToDtl";  
			}else{
				return "sessionExpired";
			}
		}
	}

@RequestMapping("/bankStmtData")
	public String bankStmtData(){
		System.out.println(TAG+"bankStmtData...");
		String authentication=authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH)){
			return "sessionExpired";
		}
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBank()= "+userRights.isUrBank());
			if(userRights.isUrBank()){
				return "bankStmtData";
			}else{
				return "sessionExpired";
			}
		}
	}


	@RequestMapping("/customerGroup")
	public String customerGroup(){
		System.out.println(TAG+"Customergroup....");
		String authentication=authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrCustomerGroup() = "+userRights.isUrCustomer());
			if(userRights.isUrCustomer()){
				return "customerGroup";  
			}else{
				return "sessionExpired";
			}
		} 	 
	}
	
	@RequestMapping("/trafficOff")
	public String trafficOff(){
		System.out.println(TAG+"Customergroup....");
		String authentication=authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrCustomerGroup() = "+userRights.isUrCustomer());
			return "trafficOff";
			/*
			if(userRights.isUrCustomer()){
				return "customerGroup";  
			}else{
				return "sessionExpired";
			}
			*/
		} 	 
	}
	
	@RequestMapping("/allowSrtgDmg")  
	public String allowSrtgDmg() {  
		System.out.println(TAG+"allowSrtgDmg-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.allowSrtgDmg = "+userRights.isAlwSrtgDmgAr());
			if(userRights.isAlwSrtgDmgAr()){
				return "allowSrtgDmg";  
			}else{
				return "sessionExpired";
			}
		} 
	}
	
	@RequestMapping("/imgReader")  
	public String imgReader() {  
		System.out.println(TAG+"allowSrtgDmg-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			return "imgReader";
		} 
	}
			


	@RequestMapping("/holdDocs")
	public String holdDocs() {
		System.out.println(TAG + "holdDocs-------");
		String authentication = authentication();
		if (authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else {
			UserRights userRights = (UserRights) httpSession
					.getAttribute("currentUserRights");
			System.out.println("userRights.allowSrtgDmg = "
					+ userRights.isAlwSrtgDmgAr());
			if (userRights.isAlwSrtgDmgAr()) {
				return "holdDocs";
			} else {
				return "sessionExpired";
			}
		}
	}
	
	@RequestMapping("/relAnnextureRpt")
	public String relAnnextureRpt(){
		System.out.println(TAG+"relAnnextureRpt......");
		String authentication = authentication();
		if (authentication.equalsIgnoreCase(UN_AUTH)) {
			return "sessionExpired";
		} else {
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println(""+userRights.isUrReports());
			if (userRights.isUrReports()) {
				return "relAnnextureRpt";
			} else {
				return "sessionExpired";
			}
		}
	}


  @RequestMapping("/unHoldDocs")
	public String unHoldDocs() {
		System.out.println(TAG + "unHoldDocs-------");
		String authentication = authentication();
		if (authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else {
			UserRights userRights = (UserRights) httpSession
					.getAttribute("currentUserRights");
			System.out.println("userRights.allowSrtgDmg = "
					+ userRights.isAlwSrtgDmgAr());
			if (userRights.isAlwSrtgDmgAr()) {
				return "unHoldDocs";
			} else {
				return "sessionExpired";
			}
		}
	}

  @RequestMapping("/memo")  
  public String memo() {  
	System.out.println(TAG+"allowSrtgDmg-------");
	String authentication = authentication();
	if(authentication.equalsIgnoreCase(UN_AUTH))
		return "sessionExpired";
	else{
		UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
		return "memo";
	} 
  }
  
  @RequestMapping("/sendMsgToBranchs")  
  public String sendMsgToBranchs() {  
	System.out.println(TAG+"allowSrtgDmg-------");
	String authentication = authentication();
	if(authentication.equalsIgnoreCase(UN_AUTH))
		return "sessionExpired";
	else{
		UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
		return "sendMsgToBranchs";
	} 
  }
  
  @RequestMapping("/hoStockAndOsRpt")  
  public String hoStockAndOsRpt() {  
	System.out.println(TAG+"allowSrtgDmg-------");
	String authentication = authentication();
	if(authentication.equalsIgnoreCase(UN_AUTH))
		return "sessionExpired";
	else{
		UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
		return "hoStockAndOsRpt";
	} 
  }
  
  @RequestMapping("/hoCnmtCosting")  
  public String hoCnmtCosting() {  
	System.out.println(TAG+"allowSrtgDmg-------");
	String authentication = authentication();
	if(authentication.equalsIgnoreCase(UN_AUTH))
		return "sessionExpired";
	else{
		UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
		return "hoCnmtCosting";
	} 
  }
  
  @RequestMapping("/cnmtChlnSedrNarr")  
  public String cnmtChlnSedrNarr() {  
	System.out.println(TAG+"allowSrtgDmg-------");
	String authentication = authentication();
	if(authentication.equalsIgnoreCase(UN_AUTH))
		return "sessionExpired";
	else{
		UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
		return "cnmtChlnSedrNarr";
	} 
  }
  
  @RequestMapping("/hoCashStmt")  
  public String hoCashStmt() {  
	System.out.println(TAG+"allowSrtgDmg-------");
	String authentication = authentication();
	if(authentication.equalsIgnoreCase(UN_AUTH))
		return "sessionExpired";
	else{
		UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
		return "hoCashStmt";
	} 
  }
  
  @RequestMapping("/groupBill")  
  public String groupBill() {  
	System.out.println(TAG+"allowSrtgDmg-------");
	String authentication = authentication();
	if(authentication.equalsIgnoreCase(UN_AUTH))
		return "sessionExpired";
	else{
		UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
		return "groupBill";
	} 
  }
  
  
  
	@RequestMapping("/manualBill")  
	public String manualBill() {  
		System.out.println(TAG+"manualBill");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrManualBill() = "+userRights.isUrManualBill());
			if(userRights.isUrManualBill()){
				return "manualBill";  
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	@RequestMapping("/gstBill")  
	public String gstBill() {  
		System.out.println(TAG+"gstBill");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill() = "+userRights.isUrBill());
			if(userRights.isUrBill()){
				return "gstBill";  
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	@RequestMapping("/gstManualBill")  
	public String gstManualBill() {  
		System.out.println(TAG+"gstManualBill");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill() = "+userRights.isUrManualBill());
			if(userRights.isUrManualBill()){
				return "gstManualBill";  
			}else{
				return "sessionExpired";
			}
		}  
	}
	
	
	
	@RequestMapping("/gstRelBill")  
	public String gstRelBill() {  
		System.out.println(TAG+"gstRelBill");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.isUrBill() = "+userRights.isUrBill());
			if(userRights.isUrBill()){
				return "gstRelBill";  
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	@RequestMapping("/chlnMisReport")  
	public String chlnMisReport() {  
		System.out.println(TAG+"chlnDetReport-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "chlnMisReport";  
	}
	
	@RequestMapping("/sedrMisReport")  
	public String sedrMisReport() {  
		System.out.println(TAG+"chlnDetReport-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else
			return "sedrMisReport";  
	}
  
	@RequestMapping("/reIssue")  
	public String reIssue() {  
		System.out.println(TAG+"reIssue");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println("userRights.reIssue() = "+userRights.isReIssue());
			if(userRights.isReIssue()){
				return "reIssue";  
			}else{
				return "sessionExpired";
			}
		}  
	}

	
	
	 @RequestMapping("/relCNStatus")  
	  public String relCNStatus() {  
		System.out.println(TAG+"relCNStatus-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{ 
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println(""+userRights.isUrReports());
			if (userRights.isUrReports()) {
				return "relCNStatus";
			} else {
				return "sessionExpired";
			}
		
		} 
	  }

	 
	 @RequestMapping("/viewRelCNStatus")  
	  public String viewRelCNStatus() {  
		System.out.println(TAG+"viewRelCNStatus-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{ 
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println(""+userRights.isUrReports());
			if (userRights.isUrReports()) {
				return "viewRelCNStatus";
			} else {
				return "sessionExpired";
			}
		
		} 
	  }

	 
	 @RequestMapping("/allowAr")  
	  public String allowAr() {  
		System.out.println(TAG+"allowAr-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{ 
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println(""+userRights.isAllowAr());
			if (userRights.isAllowAr()) {
				return "allowAr";
			} else {
				return "sessionExpired";
			}
		
		} 
	  }

	 @RequestMapping("/relOs")  
	  public String relOs() {  
		System.out.println(TAG+"viewRelCNStatus-------");
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return "sessionExpired";
		else{ 
			UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
			System.out.println(""+userRights.isUrReports());
			if (userRights.isUrReports()) {
				return "relOs";
			} else {
				return "sessionExpired";
			}
		
		} 
	  }
	 
	 @RequestMapping("/editBillGST")  
		public String editBillGST() {  
			System.out.println(TAG+"editBill-------");
			String authentication = authentication();
			if(authentication.equalsIgnoreCase(UN_AUTH))
				return "sessionExpired";
			else{
				UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
				System.out.println("userRights.isUrEditBill = "+userRights.isUrEditBill());
				if(userRights.isUrEditBill()){
					return "editBillGST";  
				}else{
					return "sessionExpired";
				}
			} 
		}
	
	 
	 @RequestMapping("/lhpvBalPay")  
		public String lhpvBalPay() {  
			System.out.println(TAG+"newLhpvBal-------");
			String authentication = authentication();
			if(authentication.equalsIgnoreCase(UN_AUTH))
				return "sessionExpired";
			else{
				User currentUser = (User)httpSession.getAttribute("currentUser");
				UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
				System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
				//if(userRights.isUrVouch()){
					return "lhpvBalPay"; 
				/*}else{
					return "sessionExpired";
				}*/
			}  
		}
	 

	 @RequestMapping("/ackValidator")  
		public String ackValidator() {  
			System.out.println(TAG+"ackValidator-------");
			String authentication = authentication();
			if(authentication.equalsIgnoreCase(UN_AUTH))
				return "sessionExpired";
			else{
				User currentUser = (User)httpSession.getAttribute("currentUser");
				UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
				System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
				if(userRights.isUrAckValidation()){
					return "ackValidator"; 
				}else{
					return "sessionExpired";
				}
			}  
		}
	 
	 @RequestMapping("/lhpvBalPayHo")  
		public String lhpvBalPayHo() {  
			System.out.println(TAG+"newLhpvBal-------");
			String authentication = authentication();
			if(authentication.equalsIgnoreCase(UN_AUTH))
				return "sessionExpired";
			else{
				User currentUser = (User)httpSession.getAttribute("currentUser");
				UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
				System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
				if(currentUser.getUserBranchCode().equalsIgnoreCase("20") || userRights.isUrHoLhpv()){
					return "lhpvBalPayHo"; 
				}else{
					return "sessionExpired";
				}
			}  
		}

	 @RequestMapping("/payLhpvBalance")  
		public String payLhpvBalance() {  
			System.out.println(TAG+"payLhpvBalance-------");
			String authentication = authentication();
			if(authentication.equalsIgnoreCase(UN_AUTH))
				return "sessionExpired";
			else{
				User currentUser = (User)httpSession.getAttribute("currentUser");
				UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
				System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
				if( userRights.isUrGenrateLbExcel()){
					return "payLhpvBalance"; 
				}else{
					return "sessionExpired";
				}
			}  
		}	 
	 
	 
	 @RequestMapping("/relianceExcel")  
		public String relianceExcel() {  
			System.out.println(TAG+"relianceExcel-------");
			String authentication = authentication();
			if(authentication.equalsIgnoreCase(UN_AUTH))
				return "sessionExpired";
			else{
				User currentUser = (User)httpSession.getAttribute("currentUser");
				UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
				System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
				if( userRights.isUrGenrateRelExcel()){
					return "relianceExcel"; 
				}else{
					return "sessionExpired";
				}
			}  
		}	
	 
	 
	 @RequestMapping("/bankStmtUpdate")  
		public String bankStmtUpdate() {  
			System.out.println(TAG+"relianceExcel-------");
			String authentication = authentication();
			if(authentication.equalsIgnoreCase(UN_AUTH))
				return "sessionExpired";
			else{
				User currentUser = (User)httpSession.getAttribute("currentUser");
				UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
				System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
				if( userRights.isUrGenrateRelExcel()){
					return "bankStmtUpdate"; 
				}else{
					return "sessionExpired";
				}
			}  
		}
	
	 @RequestMapping("/fundAllocation")  
		public String fundAllocation() {  
			System.out.println(TAG+"fundAllocation-------");
			String authentication = authentication();
			if(authentication.equalsIgnoreCase(UN_AUTH))
				return "sessionExpired";
			else{
				User currentUser = (User)httpSession.getAttribute("currentUser");
				UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
				System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
				//if( userRights.isUrVouch()){
					return "fundAllocation"; 
				/*}else{
					return "sessionExpired";
				}*/
			}  
		}
	 
		 @RequestMapping("/generatedFundAllocation")  
			public String generatedFundAllocation() {  
				System.out.println(TAG+"generatedFundAllocation-------");
				String authentication = authentication();
				if(authentication.equalsIgnoreCase(UN_AUTH))
					return "sessionExpired";
				else{
					User currentUser = (User)httpSession.getAttribute("currentUser");
					UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
					System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
					if( userRights.isUrFundAllocation()){
						return "generatedFundAllocation"; 
					}else{
						return "sessionExpired";
					}
				}  
			}
		 
		 
		 @RequestMapping("/petroCard")  
			public String petroCard() {  
				System.out.println(TAG+"petroCard-------");
				String authentication = authentication();
				if(authentication.equalsIgnoreCase(UN_AUTH))
					return "sessionExpired";
				else{
					User currentUser = (User)httpSession.getAttribute("currentUser");
					UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
					System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
					//if( userRights.isUrVouch()){
						return "petroCard"; 
					/*}else{
						return "sessionExpired";
					}*/
				}  
			}
		 
		 @RequestMapping("/petroCardExcel")  
			public String petroCardExcel() {  
				System.out.println(TAG+"petroCardExcel-------");
				String authentication = authentication();
				if(authentication.equalsIgnoreCase(UN_AUTH))
					return "sessionExpired";
				else{
					User currentUser = (User)httpSession.getAttribute("currentUser");
					UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
					System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
					if( userRights.isUrPetroExcel()){
						return "petroCardExcel"; 
					}else{
						return "sessionExpired";
					}
				}  
			}
		 
		 
		 @RequestMapping("/newRentDetail")  
			public String newRentExcel() {  
				System.out.println(TAG+"newRentDetail-------");
				String authentication = authentication();
				if(authentication.equalsIgnoreCase(UN_AUTH))
					return "sessionExpired";
				else{
					User currentUser = (User)httpSession.getAttribute("currentUser");
					UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
					System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
					if( userRights.isUrPetroExcel()){
						return "newRentDetail"; 
					}else{
						return "sessionExpired";
					}
				}  
			}


		 @RequestMapping("/rentXlsx")  
			public String rentXlsx() {  
				System.out.println(TAG+"newRentDetail-------");
				String authentication = authentication();
				if(authentication.equalsIgnoreCase(UN_AUTH))
					return "sessionExpired";
				else{
					User currentUser = (User)httpSession.getAttribute("currentUser");
					UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
					System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
					if( userRights.isUrPetroExcel()){
						return "rentXlsx"; 
					}else{
						return "sessionExpired";
					}
				}  
			}
		 
		
		 
		 @RequestMapping("/payableRpt")  
			public String payableRpt() {  
				System.out.println(TAG+"payableRpt-------");
				String authentication = authentication();
				if(authentication.equalsIgnoreCase(UN_AUTH))
					return "sessionExpired";
				else{
					User currentUser = (User)httpSession.getAttribute("currentUser");
					UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
					System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
					if( userRights.isUrReports()){
						return "payableRpt"; 
					}else{
						return "sessionExpired";
					}
				}  
			}
		 
		 
		 @RequestMapping("/removeFrmShrtExcess")  
			public String removeFrmShrtExcess() {  
				System.out.println(TAG+"removeFrmShrtExcess-------");
				String authentication = authentication();
				if(authentication.equalsIgnoreCase(UN_AUTH))
					return "sessionExpired";
				else{
					User currentUser = (User)httpSession.getAttribute("currentUser");
					UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
					System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
					if( userRights.isUrShrtExcessUpdate()){
						return "removeFrmShrtExcess"; 
					}else{
						return "sessionExpired";
					}
				}  
			}
		
		 
		 @RequestMapping("/circular")  
			public String circular() {  
				System.out.println(TAG+"circular-------");
				String authentication = authentication();
				if(authentication.equalsIgnoreCase(UN_AUTH))
					return "sessionExpired";
				else{
					User currentUser = (User)httpSession.getAttribute("currentUser");
					UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
					System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
					//if( userRights.isUrReports()){
						return "circular"; 
					//}else{
				//		return "sessionExpired";
					//}
				}  
			}
		 
		 @RequestMapping("/newLhpvBalCash")  
			public String newLhpvBalCash() {  
				System.out.println(TAG+"newLhpvBalCash-------");
				String authentication = authentication();
				if(authentication.equalsIgnoreCase(UN_AUTH))
					return "sessionExpired";
				else{
					UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
					System.out.println("userRights.isUrLhpvBalCash() = "+userRights.isUrLhpvBalCash());
					if(userRights.isUrLhpvBalCash()){
						return "newLhpvBalCash"; 
					}else{
						return "sessionExpired";
					}
				}  
			}
		 
		 
		 @RequestMapping("/fileUpload")  
			public String fileUpload() {  
				System.out.println(TAG+"userRights-------");
				String authentication = authentication();
				if(authentication.equalsIgnoreCase(UN_AUTH))
					return "sessionExpired";
				else {
					User currentUser = (User)httpSession.getAttribute("currentUser");
					UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
					System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
					if( userRights.isUrFundAllocation()){
						return "fileUpload"; 
					}else{
						return "sessionExpired";
					}
				}
					  
			}
		 
		 
		 @RequestMapping("/challanEnquiry")  
			public String challanEnquiry() {  
				System.out.println(TAG+"challanEnquiry-------");
				String authentication = authentication();
				if(authentication.equalsIgnoreCase(UN_AUTH))
					return "sessionExpired";
				else {
					User currentUser = (User)httpSession.getAttribute("currentUser");
					UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
					System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
					if( userRights.isUrHoLhpv()){
						return "challanEnquiry"; 
					}else{
						return "sessionExpired";
					}
				}
					  
			}
		 
		 
		 @RequestMapping("/verifyAccount")  
			public String verifyAccount() {  
				System.out.println(TAG+"verifyAccount-------");
				String authentication = authentication();
				if(authentication.equalsIgnoreCase(UN_AUTH))
					return "sessionExpired";
				else {
					User currentUser = (User)httpSession.getAttribute("currentUser");
					UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
					System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
					if( userRights.isUrPanVal()){
						return "verifyAccount"; 
					}else{
						return "sessionExpired";
					}
				}
					  
			}
		 
		 @RequestMapping("/invalidAcc")  
			public String invalidAcc() {  
				System.out.println(TAG+"verifyAccount-------");
				String authentication = authentication();
				if(authentication.equalsIgnoreCase(UN_AUTH))
					return "sessionExpired";
				else {
					User currentUser = (User)httpSession.getAttribute("currentUser");
					UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
					System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
					if( userRights.isUrHoLhpv()){
						return "invalidAcc"; 
					}else{
						return "sessionExpired";
					}
				}
					  
			}
		 
		 @RequestMapping("/alwLryPymnt")  
			public String alwLryPymnt() {  
				System.out.println(TAG+"alwLryPymnt-------");
				String authentication = authentication();
				if(authentication.equalsIgnoreCase(UN_AUTH))
					return "sessionExpired";
				else {
					User currentUser = (User)httpSession.getAttribute("currentUser");
					UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
					System.out.println("currentUser.getUserBranchCode() = "+currentUser.getUserBranchCode());
					if( userRights.isUrLhpvAlw()){
						return "alwLryPymnt"; 
					}else{
						return "sessionExpired";
					}
				}
					  
			}
		 
		 
		/* @RequestMapping("/cashTransfer")  
			public String cashTransfer() {  
				System.out.println(TAG+"cashTransfer-------");
				String authentication = authentication();
				if(authentication.equalsIgnoreCase(UN_AUTH))
					return "sessionExpired";
				else {
					User currentUser = (User)httpSession.getAttribute("currentUser");
					UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
					if( userRights.isUrVouch()){
						return "cashTransfer"; 
					}else{
						return "sessionExpired";
					}
				}
					  
			}
		 
		 
		 @RequestMapping("/verifySupplier")  
			public String verifySupplier() {  
				System.out.println(TAG+"verifySupplier-------");
				String authentication = authentication();
				if(authentication.equalsIgnoreCase(UN_AUTH))
					return "sessionExpired";
				else {
					User currentUser = (User)httpSession.getAttribute("currentUser");
					UserRights userRights = (UserRights) httpSession.getAttribute("currentUserRights");
					//if( userRights.isUrVouch()){
						return "verifySupplier"; 
					}else{
						return "sessionExpired";
					}
				}
					  
			}*/
		 
		 
}


package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.ContToStnDAO;
import com.mylogistics.DAO.DailyContractDAO;
import com.mylogistics.DAO.PenaltyBonusDetentionDAO;
import com.mylogistics.DAO.RateByKmDAO;
import com.mylogistics.DAO.RegularContractDAO;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.PenaltyBonusDetention;
import com.mylogistics.model.RateByKm;
import com.mylogistics.services.ConstantsValues;


@Controller
public class ViewContractCntlr {

	@Autowired
	private DailyContractDAO dailyContractDAO;
	
	@Autowired
	private RateByKmDAO rateByKmDAO;
	
	@Autowired
	private RegularContractDAO regularContractDAO;
	
	@Autowired
	private ContToStnDAO contToStnDAO;
	
	@Autowired
	private PenaltyBonusDetentionDAO penaltyBonusDetentionDAO;
	
	@RequestMapping(value = "/getDlyRegContractCodes", method = RequestMethod.POST)
	public @ResponseBody Object getDlyRegContractCodes() {
		Map<String, Object> map = new HashMap<String, Object>();
		/*List<String> dList = new ArrayList<String>();
		List<String> rList = new ArrayList<String>();*/
		
		/*dList = dailyContractDAO.getDailyContractCode();
		rList = regularContractDAO.getRegularContractCode();
		*/
		//List<String> strings = new ArrayList<String>();
		
		//dList.addAll(rList);
		/*strings.addAll(dList);
		strings.addAll(rList);*/
		
		List<Map<String,String>> contList = new ArrayList<>();
		contList = dailyContractDAO.getDlyCont();
		 if (!contList.isEmpty()) {	 
			 map.put("list", contList);
			 map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		 }else{
			 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		 }
		return map;
	}
	
	@RequestMapping(value = "/getRBKMDataViewCont", method = RequestMethod.POST)
	public @ResponseBody Object getRBKMDataViewCont(@RequestBody String code) {

		System.out.println("Enter into getRBKMDataViewCont function-------"+code);
		List<RateByKm> rateByKms = new ArrayList<RateByKm>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		rateByKms = rateByKmDAO.getRateByKm(code);
		System.out.println("The size of rbkm list is ----"+rateByKms.size());	
		
		if(!rateByKms.isEmpty()){
			map.put("list", rateByKms);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getPBDDataViewCont", method = RequestMethod.POST)
	public @ResponseBody Object getPBDDataViewCont(@RequestBody String code) {

		System.out.println("Enter into getPBDDataViewCont function-------"+code);
		List<PenaltyBonusDetention> pList = new ArrayList<PenaltyBonusDetention>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		pList = penaltyBonusDetentionDAO.getPenaltyBonusDetention(code);
		
		System.out.println("The size of pbd list is ----"+pList.size());	
		
		if(!pList.isEmpty()){
			map.put("list", pList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getCTSDataViewCont", method = RequestMethod.POST)
	public @ResponseBody Object getCTSDataViewCont(@RequestBody String code) {

		System.out.println("Enter into getCTSDataViewCont function-------"+code);
		List<ContToStn> contToStns = new ArrayList<ContToStn>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		contToStns = contToStnDAO.getContToStn(code);
		System.out.println("The size of cts list is ----"+contToStns.size());	
		
		if(!contToStns.isEmpty()){
			map.put("list", contToStns);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
}

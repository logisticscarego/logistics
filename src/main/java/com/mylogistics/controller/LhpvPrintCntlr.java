package com.mylogistics.controller;

import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.LhpvStatusDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.User;
import com.mylogistics.model.lhpv.LhpvAdv;
import com.mylogistics.model.lhpv.LhpvBal;
import com.mylogistics.model.lhpv.LhpvCashSmry;
import com.mylogistics.model.lhpv.LhpvStatus;
import com.mylogistics.model.lhpv.LhpvSup;
import com.mylogistics.services.ConstantsValues;

@Controller
public class LhpvPrintCntlr {

	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private HttpSession httpSession; 
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private LhpvStatusDAO lhpvStatusDAO;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	private List<Map<String,Object>> actLhpvAdv = new ArrayList<>();
	private List<Map<String,Object>> actLhpvBal = new ArrayList<>();
	private List<Map<String,Object>> actLhpvSup = new ArrayList<>();
	private String brhName = "";
	private int lhpvNo = 0;
	private Date lhpvDt = null;
	private int shNo = 0;
	private double totAdvPay = 0;
	private double totBalPay = 0;
	private double totSupPay = 0;
	
	private double cashAdvPay = 0;
	private double chqAdvPay = 0;
	private double rtgsAdvPay = 0;
	private double petroAdvPay = 0;
	
	private double cashBalPay = 0;
	private double chqBalPay = 0;
	private double rtgsBalPay = 0;
	private double petroBalPay = 0;
	
	private double cashSupPay = 0;
	private double chqSupPay = 0;
	private double rtgsSupPay = 0;
	private double petroSupPay = 0;
	
	
	private static final String AUTH = "authorized";
	private static final String UN_AUTH = "unAuthorized";
	private static DecimalFormat df2 = new DecimalFormat("#.##");
	
	public String authentication(){
		System.out.println("---------enter into authentication function");
		User currentUser = (User)httpSession.getAttribute("currentUser");

		if(currentUser == null){
			System.out.println("current user is null");
			return UN_AUTH;
		}else{
			System.out.println("current user authentication token ="+currentUser.getUserAuthToken());
			System.out.println("current session id ="+httpSession.getId());
			if(currentUser.getUserAuthToken().equalsIgnoreCase(httpSession.getId())){
				System.out.println("authorized user set in session");
				return AUTH;
			}else{
				System.out.println("unAuthorized user set in session");
				return UN_AUTH;
			}
		}
	}
	
	
	@RequestMapping(value = "/getBrhFrLP" , method = RequestMethod.POST)  
	public @ResponseBody Object getBrhFrLP() {  
		System.out.println("Enter into getBrhFrLP---->");
		Map<String, Object> map = new HashMap<>();	
		List<Branch> brhList = branchDAO.getAllActiveBranches();
		if(!brhList.isEmpty()){
			map.put("list",brhList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/submitLP" , method = RequestMethod.POST)  
	public @ResponseBody Object submitLP(@RequestBody Map<String,Object> clientMap) {  
		System.out.println("Enter into getBrhFrCS---->");
		Map<String, Object> map = new HashMap<>();
		int brhId = (int) clientMap.get("brhId");
		Date date = Date.valueOf((String) clientMap.get("date"));              
		System.out.println("brhId = "+brhId);
		System.out.println("date = "+date);
		
		actLhpvAdv.clear();
		actLhpvBal.clear();
		actLhpvSup.clear();
		brhName = "";
		lhpvNo = 0;
		shNo = 0;
		lhpvDt = null;
		
		totAdvPay = 0;
		totBalPay = 0;
		totSupPay = 0;
		
		cashAdvPay = 0;
		chqAdvPay = 0;
		rtgsAdvPay = 0;
		petroAdvPay=0;
		
		cashBalPay = 0;
		chqBalPay = 0;
		rtgsBalPay = 0;
		petroBalPay = 0;
		
		cashSupPay = 0;
		chqSupPay = 0;
		rtgsSupPay = 0;
		petroSupPay = 0;
		
		Branch branch = branchDAO.getBranchById(brhId);
		if(branch != null){
			brhName = branch.getBranchName();
			User currentUser = (User) httpSession.getAttribute("currentUser");
			CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getCssByDt(branch.getBranchCode(), date);
			shNo = cashStmtStatus.getCssCsNo();
			
			LhpvStatus lhpvStatus = lhpvStatusDAO.getLssByDt(branch.getBranchCode(), date);
			
			
			if(lhpvStatus != null){
				lhpvNo = lhpvStatus.getLsNo();
				lhpvDt = lhpvStatus.getLsDt();
				
				List<LhpvAdv> lhpvAdvList = new ArrayList<>();
				List<LhpvBal> lhpvBalList = new ArrayList<>();
				List<LhpvSup> lhpvSupList = new ArrayList<>();
				List<LhpvCashSmry> lcsList = new ArrayList<>();
				lhpvAdvList = lhpvStatusDAO.getLhpvAdv(lhpvStatus.getLsId());
				lhpvBalList = lhpvStatusDAO.getLhpvBal(lhpvStatus.getLsId());
				lhpvSupList = lhpvStatusDAO.getLhpvSup(lhpvStatus.getLsId());
				lcsList = lhpvStatusDAO.getLhpvCash(lhpvStatus.getLsDt(),lhpvStatus.getbCode());
				
				/*System.out.println("lhpvAdvList of "+lhpvStatus.getLsDt()+" == "+lhpvAdvList.size());
				System.out.println("lhpvBalList of "+lhpvStatus.getLsDt()+" == "+lhpvBalList.size());
				System.out.println("lhpvSupList of "+lhpvStatus.getLsDt()+" == "+lhpvSupList.size());
				System.out.println("lcsList of "+lhpvStatus.getLsDt()+" == "+lcsList.size());*/
				
				
				if(!lhpvAdvList.isEmpty()){
					for(int i=0;i<lhpvAdvList.size();i++){
						if(lhpvAdvList.get(i).getLaPayMethod() == 'Q'){
							System.out.println("****************** 1");
							Map<String,Object> advMap = new HashMap<>();
							String bnkName = "";
							advMap.put("payM",lhpvAdvList.get(i).getLaPayMethod());
							advMap.put("chqNo",lhpvAdvList.get(i).getLaChqNo());
							if(lhpvAdvList.get(i).getLaBankCode() != null){
								advMap.put("bankCode",lhpvAdvList.get(i).getLaBankCode());
								bnkName = bankMstrDAO.getBankName(lhpvAdvList.get(i).getLaBankCode());
								if(bnkName != null){
									advMap.put("bnkName",bnkName);
								}
							}
							advMap.put("chqAmt",df2.format(lhpvAdvList.get(i).getLaTotPayAmt()));
							advMap.put("chlnNo",lhpvAdvList.get(i).getChallan().getChlnCode());
							advMap.put("advAmt",df2.format(lhpvAdvList.get(i).getLaLryAdvP()));
							advMap.put("csDis",df2.format(lhpvAdvList.get(i).getLaCashDiscR()));
							advMap.put("muns",df2.format(lhpvAdvList.get(i).getLaMunsR()));
							advMap.put("tds",df2.format(lhpvAdvList.get(i).getLaTdsR()));
							advMap.put("gdTot",df2.format(lhpvAdvList.get(i).getLaFinalTot()));
							
							chqAdvPay = chqAdvPay + lhpvAdvList.get(i).getLaFinalTot();
							totAdvPay = totAdvPay + lhpvAdvList.get(i).getLaFinalTot();
							
							actLhpvAdv.add(advMap);
							
						}else if(lhpvAdvList.get(i).getLaPayMethod() == 'R'){
							System.out.println("****************** 2");
							Map<String,Object> advMap = new HashMap<>();
							String bnkName = "";
							advMap.put("payM",lhpvAdvList.get(i).getLaPayMethod());
							advMap.put("chqNo","");
							if(lhpvAdvList.get(i).getLaBankCode() != null){
								advMap.put("bankCode",lhpvAdvList.get(i).getLaBankCode());
								bnkName = bankMstrDAO.getBankName(lhpvAdvList.get(i).getLaBankCode());
								if(bnkName != null){
									advMap.put("bnkName",bnkName);
								}
							}
							advMap.put("chqAmt","");
							advMap.put("chlnNo",lhpvAdvList.get(i).getChallan().getChlnCode());
							advMap.put("advAmt",df2.format(lhpvAdvList.get(i).getLaLryAdvP()));
							advMap.put("csDis",df2.format(lhpvAdvList.get(i).getLaCashDiscR()));
							advMap.put("muns",df2.format(lhpvAdvList.get(i).getLaMunsR()));
							advMap.put("tds",df2.format(lhpvAdvList.get(i).getLaTdsR()));
							advMap.put("gdTot",df2.format(lhpvAdvList.get(i).getLaFinalTot()));
							
							rtgsAdvPay = rtgsAdvPay + lhpvAdvList.get(i).getLaFinalTot();
							totAdvPay = totAdvPay + lhpvAdvList.get(i).getLaFinalTot();
							
							actLhpvAdv.add(advMap);
						}else if(lhpvAdvList.get(i).getLaPayMethod() == 'P'){
							System.out.println("****************** 2");
							Map<String,Object> advMap = new HashMap<>();
							String bnkName = "";
							advMap.put("payM",lhpvAdvList.get(i).getLaPayMethod());
							advMap.put("chqNo","");
							/*if(lhpvAdvList.get(i).getLaBankCode() != null){
								advMap.put("bankCode",lhpvAdvList.get(i).getLaBankCode());
								bnkName = bankMstrDAO.getBankName(lhpvAdvList.get(i).getLaBankCode());
								if(bnkName != null){
									advMap.put("bnkName",bnkName);
								}
							}*/
							advMap.put("bankCode","");
							advMap.put("bnkName","");
							advMap.put("chqAmt","");
							advMap.put("chlnNo",lhpvAdvList.get(i).getChallan().getChlnCode());
							advMap.put("advAmt",df2.format(lhpvAdvList.get(i).getLaLryAdvP()));
							advMap.put("csDis",df2.format(lhpvAdvList.get(i).getLaCashDiscR()));
							advMap.put("muns",df2.format(lhpvAdvList.get(i).getLaMunsR()));
							advMap.put("tds",df2.format(lhpvAdvList.get(i).getLaTdsR()));
							advMap.put("gdTot",df2.format(lhpvAdvList.get(i).getLaFinalTot()));
							
							petroAdvPay = petroAdvPay + lhpvAdvList.get(i).getLaFinalTot();
							totAdvPay = totAdvPay + lhpvAdvList.get(i).getLaFinalTot();
							
							actLhpvAdv.add(advMap);
						}else{
							System.out.println("****************** 3");
						}
					}
				}
				
				
				if(!lhpvBalList.isEmpty()){
					for(int i=0;i<lhpvBalList.size();i++){
						if(lhpvBalList.get(i).getLbPayMethod() == 'Q'){
							System.out.println("****************** 4");
							Map<String,Object> balMap = new HashMap<>();
							String bnkName = "";
							balMap.put("payM",lhpvBalList.get(i).getLbPayMethod());
							balMap.put("chqNo",lhpvBalList.get(i).getLbChqNo());
							if(lhpvBalList.get(i).getLbBankCode() != null){
								balMap.put("bankCode",lhpvBalList.get(i).getLbBankCode());
								bnkName = bankMstrDAO.getBankName(lhpvBalList.get(i).getLbBankCode());
								if(bnkName != null){
									balMap.put("bnkName",bnkName);
								}
							}
							balMap.put("chqAmt",df2.format(lhpvBalList.get(i).getLbTotPayAmt()));
							balMap.put("chlnNo",lhpvBalList.get(i).getChallan().getChlnCode());
							balMap.put("balAmt",df2.format(lhpvBalList.get(i).getLbLryBalP()));
							balMap.put("csDis",df2.format(lhpvBalList.get(i).getLbCashDiscR()));
							balMap.put("muns",df2.format(lhpvBalList.get(i).getLbMunsR()));
							balMap.put("tds",df2.format(lhpvBalList.get(i).getLbTdsR()));
							balMap.put("wtSrt",df2.format(lhpvBalList.get(i).getLbWtShrtgCR()));
							balMap.put("drCl",df2.format(lhpvBalList.get(i).getLbDrRcvrWtCR()));
							balMap.put("ltDel",df2.format(lhpvBalList.get(i).getLbLateDelCR()));
							balMap.put("ltAck",df2.format(lhpvBalList.get(i).getLbLateAckCR()));
							balMap.put("extKm",df2.format(lhpvBalList.get(i).getLbOthExtKmP()));
							balMap.put("ovrHt",df2.format(lhpvBalList.get(i).getLbOthOvrHgtP()));
							balMap.put("pen",df2.format(lhpvBalList.get(i).getLbOthPnltyP()));
							balMap.put("misc",df2.format(lhpvBalList.get(i).getLbOthMiscP()));
							balMap.put("det",df2.format(lhpvBalList.get(i).getLbUnpDetP()));
							balMap.put("unld",df2.format(lhpvBalList.get(i).getLbUnLoadingP()));
							balMap.put("gdTot",df2.format(lhpvBalList.get(i).getLbFinalTot()));
							
							chqBalPay = chqBalPay + lhpvBalList.get(i).getLbFinalTot();
							totBalPay = totBalPay + lhpvBalList.get(i).getLbFinalTot();
							
							actLhpvBal.add(balMap);
							
						}else if(lhpvBalList.get(i).getLbPayMethod() == 'R'){
							System.out.println("****************** 5");
							Map<String,Object> balMap = new HashMap<>();
							String bnkName = "";
							balMap.put("payM",lhpvBalList.get(i).getLbPayMethod());
							balMap.put("chqNo","");
							if(lhpvBalList.get(i).getLbBankCode() != null){
								balMap.put("bankCode",lhpvBalList.get(i).getLbBankCode());
								bnkName = bankMstrDAO.getBankName(lhpvBalList.get(i).getLbBankCode());
								if(bnkName != null){
									balMap.put("bnkName",bnkName);
								}
							}
							balMap.put("chlnNo",lhpvBalList.get(i).getChallan().getChlnCode());
							balMap.put("balAmt",df2.format(lhpvBalList.get(i).getLbLryBalP()));
							balMap.put("csDis",df2.format(lhpvBalList.get(i).getLbCashDiscR()));
							balMap.put("muns",df2.format(lhpvBalList.get(i).getLbMunsR()));
							balMap.put("tds",df2.format(lhpvBalList.get(i).getLbTdsR()));
							balMap.put("wtSrt",df2.format(lhpvBalList.get(i).getLbWtShrtgCR()));
							balMap.put("drCl",df2.format(lhpvBalList.get(i).getLbDrRcvrWtCR()));
							balMap.put("ltDel",df2.format(lhpvBalList.get(i).getLbLateDelCR()));
							balMap.put("ltAck",df2.format(lhpvBalList.get(i).getLbLateAckCR()));
							balMap.put("extKm",df2.format(lhpvBalList.get(i).getLbOthExtKmP()));
							balMap.put("ovrHt",df2.format(lhpvBalList.get(i).getLbOthOvrHgtP()));
							balMap.put("pen",df2.format(lhpvBalList.get(i).getLbOthPnltyP()));
							balMap.put("misc",df2.format(lhpvBalList.get(i).getLbOthMiscP()));
							balMap.put("det",df2.format(lhpvBalList.get(i).getLbUnpDetP()));
							balMap.put("unld",df2.format(lhpvBalList.get(i).getLbUnLoadingP()));
							balMap.put("gdTot",df2.format(lhpvBalList.get(i).getLbFinalTot()));
							
							rtgsBalPay = rtgsBalPay + lhpvBalList.get(i).getLbFinalTot();
							totBalPay = totBalPay + lhpvBalList.get(i).getLbFinalTot();
							
							actLhpvBal.add(balMap);
						}else if(lhpvBalList.get(i).getLbPayMethod() == 'P'){
							System.out.println("****************** 5");
							Map<String,Object> balMap = new HashMap<>();
							String bnkName = "";
							balMap.put("payM",lhpvBalList.get(i).getLbPayMethod());
							balMap.put("chqNo","");
							/*if(lhpvBalList.get(i).getLbBankCode() != null){
								balMap.put("bankCode",lhpvBalList.get(i).getLbBankCode());
								bnkName = bankMstrDAO.getBankName(lhpvBalList.get(i).getLbBankCode());
								if(bnkName != null){
									balMap.put("bnkName",bnkName);
								}
							}*/
							balMap.put("bnkName","");
							balMap.put("bankCode","");
							balMap.put("chlnNo",lhpvBalList.get(i).getChallan().getChlnCode());
							balMap.put("balAmt",df2.format(lhpvBalList.get(i).getLbLryBalP()));
							balMap.put("csDis",df2.format(lhpvBalList.get(i).getLbCashDiscR()));
							balMap.put("muns",df2.format(lhpvBalList.get(i).getLbMunsR()));
							balMap.put("tds",df2.format(lhpvBalList.get(i).getLbTdsR()));
							balMap.put("wtSrt",df2.format(lhpvBalList.get(i).getLbWtShrtgCR()));
							balMap.put("drCl",df2.format(lhpvBalList.get(i).getLbDrRcvrWtCR()));
							balMap.put("ltDel",df2.format(lhpvBalList.get(i).getLbLateDelCR()));
							balMap.put("ltAck",df2.format(lhpvBalList.get(i).getLbLateAckCR()));
							balMap.put("extKm",df2.format(lhpvBalList.get(i).getLbOthExtKmP()));
							balMap.put("ovrHt",df2.format(lhpvBalList.get(i).getLbOthOvrHgtP()));
							balMap.put("pen",df2.format(lhpvBalList.get(i).getLbOthPnltyP()));
							balMap.put("misc",df2.format(lhpvBalList.get(i).getLbOthMiscP()));
							balMap.put("det",df2.format(lhpvBalList.get(i).getLbUnpDetP()));
							balMap.put("unld",df2.format(lhpvBalList.get(i).getLbUnLoadingP()));
							balMap.put("gdTot",df2.format(lhpvBalList.get(i).getLbFinalTot()));
							
							petroBalPay = petroBalPay + lhpvBalList.get(i).getLbFinalTot();
							totBalPay = totBalPay + lhpvBalList.get(i).getLbFinalTot();
							
							actLhpvBal.add(balMap);
						}else{
							System.out.println("****************** 6");
						}
					}
				}
				
				
				if(!lhpvSupList.isEmpty()){
					for(int i=0;i<lhpvSupList.size();i++){
						if(lhpvSupList.get(i).getLspPayMethod() == 'Q'){
							System.out.println("****************** 7");
							Map<String,Object> supMap = new HashMap<>();
							String bnkName = "";
							supMap.put("payM",lhpvSupList.get(i).getLspPayMethod());
							supMap.put("chqNo",lhpvSupList.get(i).getLspChqNo());
							if(lhpvSupList.get(i).getLspBankCode() != null){
								supMap.put("bankCode",lhpvSupList.get(i).getLspBankCode());
								bnkName = bankMstrDAO.getBankName(lhpvSupList.get(i).getLspBankCode());
								if(bnkName != null){
									supMap.put("bnkName",bnkName);
								}
							}
							supMap.put("chqAmt",df2.format(lhpvSupList.get(i).getLspFinalTot()));
							supMap.put("chlnNo",lhpvSupList.get(i).getChallan().getChlnCode());
							supMap.put("wtSrt",df2.format(lhpvSupList.get(i).getLspWtShrtgCR()));
							supMap.put("drCl",df2.format(lhpvSupList.get(i).getLspDrRcvrWtCR()));
							supMap.put("ltDel",df2.format(lhpvSupList.get(i).getLspLateDelCR()));
							supMap.put("ltAck",df2.format(lhpvSupList.get(i).getLspLateAckCR()));
							supMap.put("extKm",df2.format(lhpvSupList.get(i).getLspOthExtKmP()));
							supMap.put("ovrHt",df2.format(lhpvSupList.get(i).getLspOthOvrHgtP()));
							supMap.put("pen",df2.format(lhpvSupList.get(i).getLspOthPnltyP()));
							supMap.put("misc",df2.format(lhpvSupList.get(i).getLspOthMiscP()));
							supMap.put("det",df2.format(lhpvSupList.get(i).getLspUnpDetP()));
							supMap.put("unld",df2.format(lhpvSupList.get(i).getLspUnLoadingP()));
							supMap.put("pay",df2.format(lhpvSupList.get(i).getLspTotPayAmt()));
							supMap.put("rcvr",df2.format(lhpvSupList.get(i).getLspTotRcvrAmt()));
							supMap.put("gdTot",df2.format(lhpvSupList.get(i).getLspFinalTot()));
							
							chqSupPay = chqSupPay + (lhpvSupList.get(i).getLspTotPayAmt() - lhpvSupList.get(i).getLspTotRcvrAmt());
							totSupPay = totSupPay + (lhpvSupList.get(i).getLspTotPayAmt() - lhpvSupList.get(i).getLspTotRcvrAmt());
							
							actLhpvSup.add(supMap);
							
						}else if(lhpvSupList.get(i).getLspPayMethod() == 'R'){
							System.out.println("****************** 8");
							Map<String,Object> supMap = new HashMap<>();
							String bnkName = "";
							supMap.put("payM",lhpvSupList.get(i).getLspPayMethod());
							supMap.put("chqNo","");
							if(lhpvSupList.get(i).getLspBankCode() != null){
								supMap.put("bankCode",lhpvSupList.get(i).getLspBankCode());
								bnkName = bankMstrDAO.getBankName(lhpvSupList.get(i).getLspBankCode());
								if(bnkName != null){
									supMap.put("bnkName",bnkName);
								}
							}
							supMap.put("chlnNo",lhpvSupList.get(i).getChallan().getChlnCode());
							supMap.put("wtSrt",df2.format(lhpvSupList.get(i).getLspWtShrtgCR()));
							supMap.put("drCl",df2.format(lhpvSupList.get(i).getLspDrRcvrWtCR()));
							supMap.put("ltDel",df2.format(lhpvSupList.get(i).getLspLateDelCR()));
							supMap.put("ltAck",df2.format(lhpvSupList.get(i).getLspLateAckCR()));
							supMap.put("extKm",df2.format(lhpvSupList.get(i).getLspOthExtKmP()));
							supMap.put("ovrHt",df2.format(lhpvSupList.get(i).getLspOthOvrHgtP()));
							supMap.put("pen",df2.format(lhpvSupList.get(i).getLspOthPnltyP()));
							supMap.put("misc",df2.format(lhpvSupList.get(i).getLspOthMiscP()));
							supMap.put("det",df2.format(lhpvSupList.get(i).getLspUnpDetP()));
							supMap.put("unld",df2.format(lhpvSupList.get(i).getLspUnLoadingP()));
							supMap.put("pay",df2.format(lhpvSupList.get(i).getLspTotPayAmt()));
							supMap.put("rcvr",df2.format(lhpvSupList.get(i).getLspTotRcvrAmt()));
							supMap.put("gdTot",df2.format(lhpvSupList.get(i).getLspFinalTot()));
							
							rtgsSupPay = rtgsSupPay + (lhpvSupList.get(i).getLspTotPayAmt() - lhpvSupList.get(i).getLspTotRcvrAmt());
							totSupPay = totSupPay + (lhpvSupList.get(i).getLspTotPayAmt() - lhpvSupList.get(i).getLspTotRcvrAmt());
							
							actLhpvSup.add(supMap);
						}else if(lhpvSupList.get(i).getLspPayMethod() == 'P'){
							System.out.println("****************** 8");
							Map<String,Object> supMap = new HashMap<>();
							String bnkName = "";
							supMap.put("payM",lhpvSupList.get(i).getLspPayMethod());
							supMap.put("chqNo","");
							/*if(lhpvSupList.get(i).getLspBankCode() != null){
								supMap.put("bankCode",lhpvSupList.get(i).getLspBankCode());
								bnkName = bankMstrDAO.getBankName(lhpvSupList.get(i).getLspBankCode());
								if(bnkName != null){
									supMap.put("bnkName",bnkName);
								}
							}*/
							supMap.put("bankCode","");
							supMap.put("bnkName","");
							supMap.put("chlnNo",lhpvSupList.get(i).getChallan().getChlnCode());
							supMap.put("wtSrt",df2.format(lhpvSupList.get(i).getLspWtShrtgCR()));
							supMap.put("drCl",df2.format(lhpvSupList.get(i).getLspDrRcvrWtCR()));
							supMap.put("ltDel",df2.format(lhpvSupList.get(i).getLspLateDelCR()));
							supMap.put("ltAck",df2.format(lhpvSupList.get(i).getLspLateAckCR()));
							supMap.put("extKm",df2.format(lhpvSupList.get(i).getLspOthExtKmP()));
							supMap.put("ovrHt",df2.format(lhpvSupList.get(i).getLspOthOvrHgtP()));
							supMap.put("pen",df2.format(lhpvSupList.get(i).getLspOthPnltyP()));
							supMap.put("misc",df2.format(lhpvSupList.get(i).getLspOthMiscP()));
							supMap.put("det",df2.format(lhpvSupList.get(i).getLspUnpDetP()));
							supMap.put("unld",df2.format(lhpvSupList.get(i).getLspUnLoadingP()));
							supMap.put("pay",df2.format(lhpvSupList.get(i).getLspTotPayAmt()));
							supMap.put("rcvr",df2.format(lhpvSupList.get(i).getLspTotRcvrAmt()));
							supMap.put("gdTot",df2.format(lhpvSupList.get(i).getLspFinalTot()));
							
							petroSupPay = petroSupPay + (lhpvSupList.get(i).getLspTotPayAmt() - lhpvSupList.get(i).getLspTotRcvrAmt());
							totSupPay = totSupPay + (lhpvSupList.get(i).getLspTotPayAmt() - lhpvSupList.get(i).getLspTotRcvrAmt());
							
							actLhpvSup.add(supMap);
						}else{
							System.out.println("****************** 9");
						}
					}
				}
			
				
				if(!lcsList.isEmpty()){
					for(int i=0;i<lcsList.size();i++){
						if(lcsList.get(i).getLaId() > 0){
							System.out.println("****************** 10");
							Map<String,Object> advMap = new HashMap<>();
							double tot = 0;
							tot = lcsList.get(i).getLcsPayAmt() - (lcsList.get(i).getLcsCashDiscR() + lcsList.get(i).getLcsMunsR() + lcsList.get(i).getLcsTdsR());
							
							advMap.put("payM",'C');
							advMap.put("chqNo","");
							advMap.put("bankCode","");
							advMap.put("bnkName","");
							advMap.put("chqAmt","");
							advMap.put("chlnNo",lcsList.get(i).getLcsChlnCode());
							advMap.put("advAmt",df2.format(lcsList.get(i).getLcsPayAmt()));
							advMap.put("csDis",df2.format(lcsList.get(i).getLcsCashDiscR()));
							advMap.put("muns",df2.format(lcsList.get(i).getLcsMunsR()));
							advMap.put("tds",df2.format(lcsList.get(i).getLcsTdsR()));
							advMap.put("gdTot",df2.format(tot));
							
							cashAdvPay = cashAdvPay + tot;
							totAdvPay = totAdvPay + tot;
							
							actLhpvAdv.add(advMap);
						}
						
						if(lcsList.get(i).getLbId() > 0){
							System.out.println("****************** 11");
							Map<String,Object> balMap = new HashMap<>();
							double balAmt = 0;
							double tot = 0;
							/*balAmt = lcsList.get(i).getLcsPayAmt() + ((lcsList.get(i).getLcsCashDiscR() + 
									lcsList.get(i).getLcsMunsR() + lcsList.get(i).getLcsTdsR() + lcsList.get(i).getLcsWtShrtgCR()+
									lcsList.get(i).getLcsDrRcvrWtCR() + lcsList.get(i).getLcsLateDelCR() + 
									lcsList.get(i).getLcsLateAckCR()) - (lcsList.get(i).getLcsOthExtKmP() +
									lcsList.get(i).getLcsOthOvrHgtP() + lcsList.get(i).getLcsOthPnltyP() +
									lcsList.get(i).getLcsOthMiscP() + lcsList.get(i).getLcsUnpDetP() + 
									lcsList.get(i).getLcsUnLoadingP()));*/
							
							balAmt = lcsList.get(i).getLcsPayAmt() - (lcsList.get(i).getLcsOthExtKmP() +
									lcsList.get(i).getLcsOthOvrHgtP() + lcsList.get(i).getLcsOthPnltyP() +
									lcsList.get(i).getLcsUnpDetP() + lcsList.get(i).getLcsUnLoadingP());
							
							tot = lcsList.get(i).getLcsPayAmt() - (lcsList.get(i).getLcsCashDiscR() + 
									lcsList.get(i).getLcsMunsR() + lcsList.get(i).getLcsTdsR() + lcsList.get(i).getLcsWtShrtgCR()+
									lcsList.get(i).getLcsDrRcvrWtCR() + lcsList.get(i).getLcsLateDelCR() + 
									lcsList.get(i).getLcsLateAckCR() + lcsList.get(i).getLcsOthMiscP());
							
							balMap.put("payM",'C');
							balMap.put("chqNo","");
							balMap.put("bankCode","");
							balMap.put("bnkName","");
							balMap.put("chqAmt","");
							balMap.put("chlnNo",lcsList.get(i).getLcsChlnCode());
							balMap.put("balAmt",df2.format(balAmt));
							balMap.put("csDis",df2.format(lcsList.get(i).getLcsCashDiscR()));
							balMap.put("muns",df2.format(lcsList.get(i).getLcsMunsR()));
							balMap.put("tds",df2.format(lcsList.get(i).getLcsTdsR()));
							balMap.put("wtSrt",df2.format(lcsList.get(i).getLcsWtShrtgCR()));
							balMap.put("drCl",df2.format(lcsList.get(i).getLcsDrRcvrWtCR()));
							balMap.put("ltDel",df2.format(lcsList.get(i).getLcsLateDelCR()));
							balMap.put("ltAck",df2.format(lcsList.get(i).getLcsLateAckCR()));
							balMap.put("extKm",df2.format(lcsList.get(i).getLcsOthExtKmP()));
							balMap.put("ovrHt",df2.format(lcsList.get(i).getLcsOthOvrHgtP()));
							balMap.put("pen",df2.format(lcsList.get(i).getLcsOthPnltyP()));
							balMap.put("misc",df2.format(lcsList.get(i).getLcsOthMiscP()));
							balMap.put("det",df2.format(lcsList.get(i).getLcsUnpDetP()));
							balMap.put("unld",df2.format(lcsList.get(i).getLcsUnLoadingP()));
							balMap.put("gdTot",df2.format(tot));
							
							cashBalPay = cashBalPay + tot;
							totBalPay = totBalPay + tot;
							
							actLhpvBal.add(balMap);
						}
						
						if(lcsList.get(i).getLspId() > 0){
							System.out.println("****************** 12");
							Map<String,Object> supMap = new HashMap<>();
							double rcvr = 0;
							rcvr = lcsList.get(i).getLcsDrRcvrWtCR() + lcsList.get(i).getLcsLateDelCR() +
									lcsList.get(i).getLcsLateAckCR() + lcsList.get(i).getLcsWtShrtgCR() +
									lcsList.get(i).getLcsOthMiscP();
							supMap.put("payM",'C');
							supMap.put("chqNo","");
							supMap.put("bankCode","");
							supMap.put("bnkName","");
							supMap.put("chqAmt","");
							supMap.put("chlnNo",lcsList.get(i).getLcsChlnCode());
							supMap.put("wtSrt",df2.format(lcsList.get(i).getLcsWtShrtgCR()));
							supMap.put("drCl",df2.format(lcsList.get(i).getLcsDrRcvrWtCR()));
							supMap.put("ltDel",df2.format(lcsList.get(i).getLcsLateDelCR()));
							supMap.put("ltAck",df2.format(lcsList.get(i).getLcsLateAckCR()));
							supMap.put("extKm",df2.format(lcsList.get(i).getLcsOthExtKmP()));
							supMap.put("ovrHt",df2.format(lcsList.get(i).getLcsOthOvrHgtP()));
							supMap.put("pen",df2.format(lcsList.get(i).getLcsOthPnltyP()));
							supMap.put("misc",df2.format(lcsList.get(i).getLcsOthMiscP()));
							supMap.put("det",df2.format(lcsList.get(i).getLcsUnpDetP()));
							supMap.put("unld",df2.format(lcsList.get(i).getLcsUnLoadingP()));
							supMap.put("pay",df2.format(lcsList.get(i).getLcsPayAmt()));
							supMap.put("rcvr",df2.format(rcvr));
							supMap.put("gdTot",df2.format(lcsList.get(i).getLcsPayAmt() - rcvr));
							
							cashSupPay = cashSupPay + (lcsList.get(i).getLcsPayAmt() - rcvr);
							totSupPay = totSupPay + (lcsList.get(i).getLcsPayAmt() - rcvr);
							
							actLhpvSup.add(supMap);
						}
					}
				}
				
				System.out.println("Size of actLhpvAdv = "+actLhpvAdv.size());
				System.out.println("Size of actLhpvBal = "+actLhpvBal.size());
				System.out.println("Size of actLhpvSup = "+actLhpvSup.size());
				
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				map.put("msg","There is no LHPV for "+date);
			}
			
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			map.put("msg","Server Error");
		}
		return map;
	}	
	
	
	@RequestMapping(value = "/printLhpvBtDy")  
	public ModelAndView printLhpvBtDy() {  
		System.out.println("Enter into printLhpvBtDy---->");
		Map<String, Object> map = new HashMap<>();
		map.put("laList",actLhpvAdv);
		map.put("lbList",actLhpvBal);
		map.put("lspList",actLhpvSup);
		map.put("brhName",brhName);
		map.put("lhpvNo", lhpvNo);
		map.put("lhpvDt", lhpvDt);
		map.put("shNo", shNo);
		
		map.put("totAdvPay", totAdvPay);
		map.put("totBalPay", totBalPay);
		map.put("totSupPay", totSupPay);
		
		map.put("cashAdvPay", cashAdvPay);
		map.put("chqAdvPay", chqAdvPay);
		map.put("rtgsAdvPay", rtgsAdvPay);
		map.put("petroAdvPay", petroAdvPay);
		
		map.put("cashBalPay", cashBalPay);
		map.put("chqBalPay", chqBalPay);
		map.put("rtgsBalPay", rtgsBalPay);
		map.put("petroBalPay", petroBalPay);
		
		map.put("cashSupPay", cashSupPay);
		map.put("chqSupPay", chqSupPay);
		map.put("rtgsSupPay", rtgsSupPay);
		map.put("petroSupPay", petroSupPay);
		
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return new ModelAndView("sessionExpired", null);
		else	
			return new ModelAndView("lhpvPdfView", map);
		
	}	

}

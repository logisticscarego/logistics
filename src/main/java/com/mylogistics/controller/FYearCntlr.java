package com.mylogistics.controller;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.FYearDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.FYear;
import com.mylogistics.services.ConstantsValues;

@Controller
public class FYearCntlr {
	
	@Autowired
	private FYearDAO fYearDAO;
	
	@RequestMapping(value="/isInCurntFYear",method = RequestMethod.POST)
	public @ResponseBody Object isInCurntFYear(@RequestBody String s){
		System.out.println("enter into isInCurntFYear function");
		Map<String,Object> map = new HashMap<>();
		Date date=  Date.valueOf(s);
		System.out.println("Date="+date);
		FYear fYear=fYearDAO.getCurrentFYear();
		System.out.println("hii");
		System.out.println(fYear.getFyFrmDt());
		System.out.println(fYear.getFyToDt());
		if(!fYear.getFyFrmDt().after(date) && !fYear.getFyToDt().before(date)) {
			map.put("result", "success");
			map.put("fYear", fYear);
		}else{
			map.put("result", "error");
		}
		
		return map;		
	}
	
	
	@RequestMapping(value="/getCurntFYear",method = RequestMethod.GET)
	public @ResponseBody Object getCurntFYear(){
		System.out.println("enter into getCurntFYear function");
		Map<String,Object> map = new HashMap<>();
		FYear fYear=fYearDAO.getCurrentFYear();
		System.out.println("hii");
		System.out.println(fYear.getFyFrmDt());
		System.out.println(fYear.getFyToDt());
		if(fYear!=null){
			map.put("result", "success");
			map.put("fYear", fYear);
		}else{
			map.put("result", "error");
		}
		return map;		
	}
	
	
}

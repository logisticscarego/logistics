package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchStockDispatchDAO;
import com.mylogistics.DAO.HBO_StnNotificationDAO;
import com.mylogistics.DAO.HBOptr_NotificationDAO;
import com.mylogistics.DAO.Optr_NotificationDAO;
import com.mylogistics.model.HBO_StnNotification;
import com.mylogistics.model.HBOptr_Notification;
import com.mylogistics.model.Optr_Notification;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.HBO_NotificationService;
import com.mylogistics.services.NotificationDate;
import com.mylogistics.services.StnOdrInvService;

@Controller
public class Operator_DashboardCntlr {
	
	@Autowired
	private HBOptr_NotificationDAO hbOptr_NotificationDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private Optr_NotificationDAO optr_NotificationDAO;
	
	@Autowired
	private BranchStockDispatchDAO branchStockDispatchDAO;
	
	@Autowired
	private HBO_StnNotificationDAO hBO_StnNotificationDAO;
	
	private HBO_NotificationService hBO_NotificationService = new HBO_NotificationService();
	
	private StnOdrInvService stnOdrInvService = new StnOdrInvService();
	
	public static Logger logger = Logger.getLogger(Operator_DashboardCntlr.class);
	
	
	@RequestMapping(value = "/getHBOprtrData", method = RequestMethod.POST)
	public @ResponseBody Object getHBOprtrData() {
		logger.info("Enter into getHBOprtrData()");
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		List<HBOptr_Notification> hList = hbOptr_NotificationDAO.getHBOptr_NotificationData(currentUser.getUserCode());
			
		if(!hList.isEmpty()){
			map.put("list", hList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		logger.info("Exit from getHBOOprtrData()");
		return map;
	}
	
	
	@RequestMapping(value = "/getTotalHBONData", method = RequestMethod.POST)
	public @ResponseBody Object getTotalHBONData() {
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		List<HBOptr_Notification> hList = hbOptr_NotificationDAO.getTotalHBON_Data(currentUser.getUserCode());
		
		if(!hList.isEmpty())
		{
			map.put("list", hList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getNotificationWithDate", method = RequestMethod.POST)
	public @ResponseBody Object getNotificationWithDate(@RequestBody NotificationDate notificationDate) {
		System.out.println("Enter into getNotificationWithDate function -->"+notificationDate.getDate());
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		List<HBOptr_Notification> hList = hbOptr_NotificationDAO.getHBOptr_NotificationDataWithDate(currentUser.getUserCode(),notificationDate.getDate());
		
		for(int i =0;i<hList.size();i++){
			System.out.println("-----------"+hList.get(i).getNoChln());
			System.out.println("-----------"+hList.get(i).getNoCnmt());
			System.out.println("-----------"+hList.get(i).getNoSedr());
		}
			
		if(!hList.isEmpty())
		{
			map.put("list", hList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/sendToOperator", method = RequestMethod.POST)
	public @ResponseBody Object sendToOperator(@RequestBody Optr_Notification optr_Notification) {
		logger.info("Ente into /sendToOperator() : DispatchCode = "+optr_Notification.getDispatchCode());
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = (User)httpSession.getAttribute("currentUser");		
		String dispatchCode = optr_Notification.getDispatchCode();
		int done = branchStockDispatchDAO.updateIsAdminView(dispatchCode);
		if(done == 1){
			optr_Notification.setUserCode(currentUser.getUserCode());
			optr_Notification.setbCode(currentUser.getUserBranchCode());
			optr_Notification.setIsRecieved("no");
		       
	       int temp=optr_NotificationDAO.saveOptr_Notification(optr_Notification);
	    	if (temp>=0) {
	    		  map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		logger.info("Exit from /sendToOperator()");
		return map;
	}
	
	
	@RequestMapping(value = "/getOprtrNotfctnData", method = RequestMethod.POST)
	public @ResponseBody Object getOprtrNotfctnData(){
		logger.info("Enter into /getOprtrNotfctnData()");
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = (User)httpSession.getAttribute("currentUser");		
		List<Optr_Notification> oList = optr_NotificationDAO.getOptr_NotificationData(currentUser.getUserCode());
		System.out.println("----------------------"+oList.size());
		for(int i=0;i<oList.size();i++){
			System.out.println("----------values int the list is"+oList.get(i).getDispatchCode());
			System.out.println("----------values int the list is"+oList.get(i).getIsRecieved());
			System.out.println("----------values int the list is"+oList.get(i).getOperatorCode());
			System.out.println("----------values int the list is"+oList.get(i).getUserCode());
			System.out.println("----------values int the list is"+oList.get(i).getOnId());
		}
		
		if(!oList.isEmpty()){
			map.put("list", oList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		logger.info("Exit from /getOprtrNotfctnData() : Ope_Noti Size = "+oList.size());
		return map;
	}
	
	@RequestMapping(value = "/createOrder", method = RequestMethod.POST)
	public @ResponseBody Object createOrder(@RequestBody HBOptr_Notification hboN) {
		System.out.println("enter into createOrder function");
	/*	String branchCode = hboN.getDisBranchCode();
		String dispatchCode = branchCode.substring(0, 4);
		dispatchCode = dispatchCode + "kaushik";
		hboN.setDispatchCode(dispatchCode);*/
		
		hBO_NotificationService.setHboN(hboN);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		return map;
	}
	

	@RequestMapping(value = "/getDispatchRequirements", method = RequestMethod.POST)
	public @ResponseBody Object getDispatchRequirements() {
		System.out.println("enter into getDispatchRequirements function");
		HBOptr_Notification hboN= hBO_NotificationService.getHboN();
	/*	String branchCode = hboN.getDisBranchCode();
		String dispatchCode = branchCode.substring(0, 4);
		dispatchCode = dispatchCode + "kaushik";
		hboN.setDispatchCode(dispatchCode);
		hBO_NotificationService.setHboN(hboN);*/
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		map.put("hboN",hboN);
		return map;
	}
	
	
	@RequestMapping(value = "/getHBOSNData", method = RequestMethod.POST)
	public @ResponseBody Object getHBOSNData() {
		logger.info("Enter into /getHBOSNData()");
		User currentUser = (User)httpSession.getAttribute("currentUser");
		List<HBO_StnNotification> HBOSNList = hBO_StnNotificationDAO.getHBOSNData(currentUser.getUserCode());
		Map<String, Object> map = new HashMap<String, Object>();
		if(!HBOSNList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("HBOSNList",HBOSNList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			map.put("msg", "No Stationary notification");
		}
		logger.info("Exit from /getHBOSNData()");
		return map;
	}
	
	
	@RequestMapping(value = "/updateHBOSN", method = RequestMethod.POST)
	public @ResponseBody Object updateHBOSN(@RequestBody Map<String,Object> clientMap) {
		System.out.println("enter into updateHBOSN function--->HBOSN.gethBOSN_Id() = ");
		Map<String, Object> map = new HashMap<String, Object>();
		String cnmtSeq = (String) clientMap.get("nowCnmtSeq");
		String chlnSeq = (String) clientMap.get("nowChlnSeq");
		String sedrSeq = (String) clientMap.get("nowSedrSeq");
		int hbosnId = (int) clientMap.get("hbosnId");
		if(hbosnId > 0){
			int temp = hBO_StnNotificationDAO.updateHBO(clientMap);
			System.out.println("Temp NO = "+temp);
			if(temp > 0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/saveStnOdrInv", method = RequestMethod.POST)
	public @ResponseBody Object saveStnOdrInv(@RequestBody StnOdrInvService stOIV) {
		System.out.println("enter into saveStnOdrInv function--->stationary.getStCode()=");
		Map<String, Object> map = new HashMap<String, Object>();
		stnOdrInvService = stOIV;
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		
		return map;
	}
	
	@RequestMapping(value = "/getStnOdrInvData", method = RequestMethod.POST)
	public @ResponseBody Object getStnOdrInvData() {
		System.out.println("enter into getStnOdrInvData function--->");
		Map<String, Object> map = new HashMap<String, Object>();

			map.put("stnOdrInvService",stnOdrInvService);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		
		return map;
	}
	
}

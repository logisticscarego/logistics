package com.mylogistics.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.StateDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.model.AllStation;
import com.mylogistics.model.State;
import com.mylogistics.model.Station;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class StationCntlr {
	
	@Autowired
	private StationDAO stationDAO;
	
	@Autowired
	private StateDAO stateDAO;
	
    @Autowired
	private HttpSession httpSession;
    
    public static Logger logger = Logger.getLogger(StationCntlr.class);
    
    @Autowired
    private SessionFactory sessionFactory;
	
	@RequestMapping(value = "/submitStation", method = RequestMethod.POST)
	public @ResponseBody Object submitStation(@RequestBody Station station){
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		station.setUserCode(currentUser.getUserCode());
		station.setbCode(currentUser.getUserBranchCode());
		Map<String,String> map = new HashMap<String,String>();
		
		Boolean stnExist=stationDAO.getStationExist(station.getStnName(), station.getStnCity(), station.getStnPin());
		
		int temp = 0;
		if(stnExist){
			System.out.println("****************************&&&&&&");
			map.put("msg","This Station already present in DB");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}else{
			temp=stationDAO.saveStationToDB(station);
		}
		
		if (temp>0) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
    	
	}
	
	/*@RequestMapping(value="/getStateCodeDetails",method = RequestMethod.POST)
	public @ResponseBody Object getStateCodeData(){
		
		List<String> stateList = stateDAO.getStateCode();
		
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(stateList.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",stateList);	
  		}else {
  			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}*/
	
	@RequestMapping(value="/getStateForStn",method = RequestMethod.POST)
	public @ResponseBody Object getStateDetails(){
		
		List<State> stateList = stateDAO.getStateData();
		
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(stateList.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
  			map.put("list",stateList);	
  		}else {
  			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	@RequestMapping(value="/getStationDetails",method = RequestMethod.POST)
	public @ResponseBody Object getStationDetails(){
		
		List<Station> stationList = stationDAO.getStationData();
		
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(stationList.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
  			map.put("list",stationList);	
  		}else {
  		  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	/*@RequestMapping(value="/getStationCodeList",method = RequestMethod.POST)
	public @ResponseBody Object getStationCodeList(){
		
		List<Station> stationList = stationDAO.getStationData();
		
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(stationList.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",stationList);	
  		}else {
  			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}*/
	
	 @RequestMapping(value = "/stationDetails", method = RequestMethod.POST)
	  public @ResponseBody Object stationDetails(@RequestBody String stationCode){
		 
	   Station station = new Station();
		List<Station> list = stationDAO.retrieveStation(stationCode);
		  		
		Map<String,Object> map = new HashMap<String , Object>();
		if (!(list.isEmpty())) {
			station = list.get(0);
		    map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		    map.put("station",station);
		}else{
		  	 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		   return map;	
	   }
	 
	 @RequestMapping(value = "/updateStation", method = RequestMethod.POST)
	 public @ResponseBody Object updateStation(@RequestBody Station station){
		 System.out.println("Entered into updateStation of controller----");
		 	
		 Map<String,String> map = new HashMap<String, String>();
		 /*String stationCode = CodePatternService.stationCodeGen(station);
			station.setStnCode(stationCode);*/
	
		 int temp=stationDAO.updateStation(station);
		 if (temp>=0) {
	        	
			 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		 } else {
			 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		 }	    	
		 return map;
	 }
	 
	 @RequestMapping(value = "/getStnList", method = RequestMethod.POST)
	 public @ResponseBody Object getStnList(){
		 System.out.println("enter into getStnList list");
		 Map<String, Object> map = new HashMap<>();
		 
		 List<Map<String, Object>> stnList = stationDAO.getStnNameCodePin();
		 
		 if (!stnList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("stnList", stnList);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		 
		 return map;
	 }
	 
	 @RequestMapping(value="/getFrmStnList", method=RequestMethod.POST)
	 public @ResponseBody Object getFrmStnList(@RequestBody String custCode){
		 System.out.println("StationCntlr.getFrmStnList()");
		 Map<String, Object> map = new HashMap<>();
		 
		 List<Station> frmStnList = stationDAO.getFrmStnList(custCode);
		 
		 if (!frmStnList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("frmStnList", frmStnList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		 
		 return map;
	 }
	 
	 @RequestMapping(value="/getToStnList", method=RequestMethod.POST)
	 public @ResponseBody Object getToStnList(@RequestBody String contCode){
		 System.out.println("StationCntlr.getToStnList()");
		 Map<String, Object> map = new HashMap<>();
		 
		 List<Station> toStnList = stationDAO.getToStnList(contCode);
		 
		 if (!toStnList.isEmpty()) {
			 map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			 map.put("toStnList", toStnList);
		 } else {
			 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		 }
		 
		 return map;
	 }
	 
	
	 @RequestMapping(value="/getStationByNameCode", method=RequestMethod.POST)
	 public @ResponseBody Object getStationByNameCode(@RequestBody String stnNameCode){
		 User user = (User) httpSession.getAttribute("currentUser");
		 logger.info("UserID = "+user.getUserId()+" : Enter into /getStationByNameCode : StnNameCode = "+stnNameCode);
		 Map<String, Object> resultMap = new HashMap<String, Object>();
		 Session session = this.sessionFactory.openSession();
		 try{
			 List<Station> stnList = stationDAO.getStationDataByNameCode(session, user, stnNameCode);
			 if(stnList.isEmpty()){
				 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				 resultMap.put("msg", "No such station found !");
			 }else{
				 resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				 resultMap.put("stationList", stnList);
			 }
		 }catch(Exception e){
			 logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		 }finally{
			 session.clear();
			 session.close();
		 }
		 logger.info("UserID = "+user.getUserId()+" : Exit from into /getStationByNameCode");
		 return resultMap;
	 }
	 
	 @RequestMapping(value="/getStationByStnCode", method=RequestMethod.POST)
	 public @ResponseBody Object getStationByStnCode(@RequestBody String stnCode){
		 User user = (User) httpSession.getAttribute("currentUser");
		 logger.info("UserID = "+user.getUserId()+" : Enter into /getStationByStnCode : StnNameCode = "+stnCode);
		 Map<String, Object> resultMap = new HashMap<String, Object>();
		 Session session = this.sessionFactory.openSession();
		 try{			 
			 Station station = stationDAO.getStationByStnCode(session, user, stnCode);
			 if(station == null){
				 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				 resultMap.put("msg", "No such station found !");
			 }else{
				 resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				 resultMap.put("station", station);
			 }
		 }catch(Exception e){
			 logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		 }finally{
			 session.clear();
			 session.close();
		 }
		 logger.info("UserID = "+user.getUserId()+" : Exit from into /getStationByNameCode");
		 return resultMap;
	 }
	 
	 
	
	 
	 @RequestMapping(value="/getADistByStateCode", method=RequestMethod.POST)
	 public @ResponseBody Object getADistByStateCode(@RequestBody String stateCode){
		 Map<String, Object> resultMap = new HashMap<String, Object>();
		 
		 List list=stationDAO.getADistByStateCode(stateCode);
		 if(list.isEmpty()) {
			 resultMap.put(ConstantsValues.RESULT, "error");
		 }else {
			 resultMap.put("list", list);
			 resultMap.put(ConstantsValues.RESULT, "success");
		 }
		 return resultMap;
	 }
	 
	 
	 @RequestMapping(value="/getACityByDistName", method=RequestMethod.POST)
	 public @ResponseBody Object getACityByDistName(@RequestBody Map<String,String> clientMap){
		 Map<String, Object> resultMap = new HashMap<String, Object>();
		 
		 List list=stationDAO.getACityByDistName(clientMap.get("dist"), clientMap.get("state"));
		 if(list.isEmpty()) {
			 resultMap.put(ConstantsValues.RESULT, "error");
		 }else {
			 resultMap.put("list", list);
			 resultMap.put(ConstantsValues.RESULT, "success");
		 }
		 return resultMap;
	 }
	 
	 @RequestMapping(value="/getAStnByCityDistName", method=RequestMethod.POST)
	 public @ResponseBody Object getAStnByCityDistName(@RequestBody Map<String,String> clientMap){
		 Map<String, Object> resultMap = new HashMap<String, Object>();
		 
		 List<AllStation> list=stationDAO.getAStnByCityDistState(clientMap.get("city"),clientMap.get("dist"), clientMap.get("state"));
		 if(list.isEmpty()) {
			 resultMap.put(ConstantsValues.RESULT, "error");
		 }else {
			 resultMap.put("list", list);
			 resultMap.put(ConstantsValues.RESULT, "success");
		 }
		 return resultMap;
	 }
	 
	 @RequestMapping(value="/getStnByPin", method=RequestMethod.POST)
	 public @ResponseBody Object getStnByPin(@RequestBody Integer pinCode){
		 Map<String, Object> resultMap = new HashMap<String, Object>();
		 
		 List<Station> list=stationDAO.getStnByPin(pinCode.toString());
		 if(list.isEmpty()) {
			 resultMap.put(ConstantsValues.RESULT, "error");
		 }else {
			 
			 List<State> stateList=stateDAO.getStateNameByCode(list.get(0).getStateCode());
			 if(!stateList.isEmpty()) {
				 resultMap.put("stateName", stateList.get(0).getStateName());
				 resultMap.put("stateGST", stateList.get(0).getStateGST());
			 }
			 resultMap.put("list", list);
			 resultMap.put(ConstantsValues.RESULT, "success");
		 }
		 return resultMap;
	 }
	 
	 
	 
	 
	 //TODO getData from allstation
	 @RequestMapping(value="/getDistByStateCode", method=RequestMethod.POST)
	 public @ResponseBody Object getDistByStateCode(@RequestBody String stateCode){
		 Map<String, Object> resultMap = new HashMap<String, Object>();
		 
		 List list=stationDAO.getDistByStateCode(stateCode);
		 if(list.isEmpty()) {
			 resultMap.put(ConstantsValues.RESULT, "error");
		 }else {
			 resultMap.put("list", list);
			 resultMap.put(ConstantsValues.RESULT, "success");
		 }
		 return resultMap;
	 }
	 
	 
	 @RequestMapping(value="/getCityByDistName", method=RequestMethod.POST)
	 public @ResponseBody Object getCityByDistName(@RequestBody Map<String,String> clientMap){
		 Map<String, Object> resultMap = new HashMap<String, Object>();
		 
		 List list=stationDAO.getCityByDistName(clientMap.get("dist"), clientMap.get("state"));
		 if(list.isEmpty()) {
			 resultMap.put(ConstantsValues.RESULT, "error");
		 }else {
			 resultMap.put("list", list);
			 resultMap.put(ConstantsValues.RESULT, "success");
		 }
		 return resultMap;
	 }
	 
	 @RequestMapping(value="/getStnByCityDistName", method=RequestMethod.POST)
	 public @ResponseBody Object getStnByCityDistName(@RequestBody Map<String,String> clientMap){
		 Map<String, Object> resultMap = new HashMap<String, Object>();
		 
		 List<Station> list=stationDAO.getStnByCityDistState(clientMap.get("city"),clientMap.get("dist"), clientMap.get("state"));
		 if(list.isEmpty()) {
			 resultMap.put(ConstantsValues.RESULT, "error");
		 }else {
			 resultMap.put("list", list);
			 resultMap.put(ConstantsValues.RESULT, "success");
		 }
		 return resultMap;
	 }
	 
	 @RequestMapping(value="/getAStnByPin", method=RequestMethod.POST)
	 public @ResponseBody Object getAStnByPin(@RequestBody Integer pinCode){
		 Map<String, Object> resultMap = new HashMap<String, Object>();
		 
		 List<AllStation> list=stationDAO.getAStnByPin(pinCode.toString());
		 if(list.isEmpty()) {
			 resultMap.put(ConstantsValues.RESULT, "error");
		 }else {
			 
			 List<State> stateList=stateDAO.getStateNameByCode(list.get(0).getStateCode().toString());
			 if(!stateList.isEmpty()) {
				 resultMap.put("stateName", stateList.get(0).getStateName());
				 resultMap.put("stateGST", stateList.get(0).getStateGST());
			 }
			 resultMap.put("list", list);
			 resultMap.put(ConstantsValues.RESULT, "success");
		 }
		 return resultMap;
	 }
	 
}

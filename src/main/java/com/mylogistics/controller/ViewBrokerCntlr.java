package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.AddressDAO;
import com.mylogistics.DAO.BrokerDAO;
import com.mylogistics.DAO.BrokerOldDAO;
import com.mylogistics.DAO.ContPersonDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.MultipleStateDAO;
import com.mylogistics.DAO.MultipleStationDAO;
import com.mylogistics.model.Address;
import com.mylogistics.model.Broker;
import com.mylogistics.model.BrokerOld;
import com.mylogistics.model.ContPerson;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.MultipleState;
import com.mylogistics.model.MultipleStation;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.ModelService;
import com.mylogistics.services.MultipleStateService;
import com.mylogistics.services.MultipleStateServiceImpl;
import com.mylogistics.services.MultipleStationService;
import com.mylogistics.services.MultipleStationServiceImpl;

@Controller
public class ViewBrokerCntlr {
	
//	@Autowired
//	private ContPersonDAO contPersonDAO;
	
	@Autowired
	private AddressDAO addressDAO;
	
	@Autowired
	private BrokerDAO brokerDAO;
	
//	@Autowired
//	private MultipleStateDAO multipleStateDAO;
//	
//	@Autowired
//	private MultipleStationDAO multipleStationDAO;
	
	@Autowired
    private HttpSession httpSession;
	
	@Autowired
	private BrokerOldDAO brokerOldDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@Autowired
	private SessionFactory sessionFactory;
	
//	private MultipleStateService multipleStateService = new MultipleStateServiceImpl();
//	
//	private MultipleStationService multipleStationService = new MultipleStationServiceImpl();
	
	/*@RequestMapping(value = "/getCPForBroker", method = RequestMethod.POST)
	public @ResponseBody Object getCPForBroker(@RequestBody String brokerCode) {
		System.out.println("Enter into getCPForBroker--------"+brokerCode);
		Map<String, Object> map = new HashMap<String, Object>();
		List<ContPerson> cList = new ArrayList<ContPerson>();
		cList = contPersonDAO.getContactPerson(brokerCode);
		ContPerson contPerson = cList.get(0);
		 if(!cList.isEmpty()){
				map.put("list", contPerson);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		 return map;
	}*/
	
	@RequestMapping(value = "/getCurrentAddress", method = RequestMethod.POST)
	public @ResponseBody Object getCurrentAddress(@RequestBody String brokerCode) {
		System.out.println("Enter into getCurrentAddress function-----------------");
		Map<String, Object> map = new HashMap<String, Object>();
		List<Address> addresses = new ArrayList<Address>();
		addresses = addressDAO.getAddress(brokerCode,"Current Address");
		 if(!addresses.isEmpty()){
			 Address address = addresses.get(0);
				map.put("list", address);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
	return map;
		}
	
	@RequestMapping(value = "/getRegisterAddress", method = RequestMethod.POST)
	public @ResponseBody Object getRegisterAddress(@RequestBody String brokerCode) {
		System.out.println("Enter into getRegisterAddress function-----------------");
		Map<String, Object> map = new HashMap<String, Object>();
		List<Address> addresses = new ArrayList<Address>();
		addresses = addressDAO.getAddress(brokerCode,"register address");
		
		 if(!addresses.isEmpty()){
			 Address address = addresses.get(0);
				map.put("list", address);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
	return map;
	}
	
	@RequestMapping(value = "/getOtherAddress", method = RequestMethod.POST)
	public @ResponseBody Object getOtherAddress(@RequestBody String brokerCode) {
		System.out.println("Enter into getOtherAddress function-----------------");
		Map<String, Object> map = new HashMap<String, Object>();
		List<Address> addresses = new ArrayList<Address>();
		addresses = addressDAO.getAddress(brokerCode,"other address");
		 if(!addresses.isEmpty()){
				Address address = addresses.get(0);
				map.put("list", address);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
	return map;
	}
	
	/*@RequestMapping(value = "/getMultipleState", method = RequestMethod.POST)
	public @ResponseBody Object getMultipleState(@RequestBody String code) {
		System.out.println("Enter into getMultipleState function-----------------");
		Map<String, Object> map = new HashMap<String, Object>();
		List<MultipleState> mList = new ArrayList<MultipleState>();
		mList = multipleStateDAO.getMultipleState(code);
		System.out.println("The size of multiple state list is "+mList.size());
		for(int i=0;i<mList.size();i++){
			System.out.println(""+mList.get(i).getMulStateId());
			System.out.println(""+mList.get(i).getMulStateRefCode());
			System.out.println(""+mList.get(i).getMulStateStateCode());
		}
		
		 if(!mList.isEmpty()){
				map.put("list", mList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
	return map;
	}*/

	/*@RequestMapping(value = "/getMultipleStation", method = RequestMethod.POST)
	public @ResponseBody Object getMultipleStation(@RequestBody String code) {
		System.out.println("Enter into getMultipleStation function-----------------");
		Map<String, Object> map = new HashMap<String, Object>();
		List<MultipleStation> mList = new ArrayList<MultipleStation>();
		mList = multipleStationDAO.getMultipleStaion(code);
		System.out.println("The size of multiple station list is "+mList.size());
		for(int i=0;i<mList.size();i++){
			System.out.println(""+mList.get(i).getMulStnId());
			System.out.println(""+mList.get(i).getMulStnRefCode());
			System.out.println(""+mList.get(i).getMulStnStnCode());
		}
		 if(!mList.isEmpty()){
				map.put("list", mList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
	return map;
	}*/

	
	@RequestMapping(value = "/submitEditBroker", method = RequestMethod.POST)
	public @ResponseBody Object submitEditBroker(@RequestBody ModelService modelService) {
		System.out.println("Enter into submitEditBroker function-----------------");
		Map<String, Object> map = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Address currentAddress = modelService.getCurrentAddress();
			Broker broker = modelService.getBroker();

			int id = broker.getBrkId();
			Broker brk = brokerDAO.getBrokerData(id);

			BrokerOld brokerOld = new BrokerOld();
			brokerOld.setbCode(brk.getbCode());
			brokerOld.setBranchCode(brk.getBranchCode());
			brokerOld.setBrkActiveDt(brk.getBrkActiveDt());
			brokerOld.setBrkBsnCard(brk.getBrkBsnCard());
			brokerOld.setBrkCIN(brk.getBrkCIN());
			brokerOld.setBrkCode(brk.getBrkCode());
			brokerOld.setBrkComStbDt(brk.getBrkComStbDt());
			brokerOld.setBrkEmailId(brk.getBrkEmailId());
			brokerOld.setBrkFirmRegNo(brk.getBrkFirmRegNo());
			brokerOld.setBrkFirmType(brk.getBrkFirmType());
			brokerOld.setBrkName(brk.getBrkName());
			brokerOld.setBrkPanDOB(brk.getBrkPanDOB());
			brokerOld.setBrkPanDt(brk.getBrkPanDt());
			brokerOld.setBrkPanName(brk.getBrkPanName());
			brokerOld.setBrkPPNo(brk.getBrkPPNo());
			brokerOld.setBrkRegPlace(brk.getBrkRegPlace());
			brokerOld.setBrkSrvTaxNo(brk.getBrkSrvTaxNo());
			brokerOld.setBrkUnion(brk.getBrkUnion());
			brokerOld.setBrkVehicleType(brk.getBrkVehicleType());
			brokerOld.setBrkVoterId(brk.getBrkVoterId());
			brokerOld.setUserCode(brk.getUserCode());
			brokerOld.setView(false);

			int saveOldBroker = brokerOldDAO.saveBrOld(brokerOld, session);
			System.out.println("successfully saved old broker data" + saveOldBroker);

			if (saveOldBroker > 0) {
				User currentUser = (User) httpSession.getAttribute("currentUser");

				if (broker.getBrkCode() != null) {
					brk.setBrkFirmType(broker.getBrkFirmType());
					brk.setBrkName(broker.getBrkName());
					brk.setBrkPhNoList(broker.getBrkPhNoList());
					brk.setBrkPanName(broker.getBrkPanName());
					brk.setBrkPanNo(broker.getBrkPanNo());
					brk.setBrkEmailId(broker.getBrkEmailId());
					brk.setBrkFirmName(broker.getBrkFirmName());
					brk.setUserCode(currentUser.getUserCode());
					brokerDAO.updateBroker(session, brk);
				}
				if (currentAddress != null) {
					currentAddress.setAddRefCode(broker.getBrkCode());
					currentAddress.setAddType("Current Address");
					currentAddress.setUserCode(currentUser.getUserCode());
					currentAddress.setbCode(currentUser.getUserBranchCode());
					addressDAO.updateAddress(currentAddress, session);
				}

				List<FAMaster> fList = faMasterDAO.retrieveFAMaster(broker.getBrkFaCode(), session);
				FAMaster faMaster = new FAMaster();
				faMaster = fList.get(0);
				faMaster.setFaMfaName(broker.getBrkName());
				faMasterDAO.updateFaMaster(faMaster, session);

			}

			session.flush();
			session.clear();
			transaction.commit();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		} finally {
			session.close();
		}

		return map;
	}
	
	/*@RequestMapping(value = "/deleteStation", method = RequestMethod.POST)
	public @ResponseBody Object deleteStation(@RequestBody MultipleStation multipleStation) {
		System.out.println("Enter into deleteStation function");
		
		multipleStationService.deleteStation(multipleStation);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}*/
	
	/*@RequestMapping(value = "/deleteState", method = RequestMethod.POST)
	public @ResponseBody Object deleteState(@RequestBody MultipleState multipleState) {
		System.out.println("Enter into deleteState function");
		multipleStateService.deleteState(multipleState);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeMultipleStation", method = RequestMethod.POST)
	public @ResponseBody Object removeMultipleStation(@RequestBody MultipleStation multipleStation) {
		
		System.out.println("Enter into removeMultipleStation"+multipleStation.getMulStnRefCode());

		int temp=multipleStationDAO.deleteStation(multipleStation);
		
		Map<String, Object> map = new HashMap<String, Object>();
		if(temp==1){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/removeMultipleState", method = RequestMethod.POST)
	public @ResponseBody Object removeMultipleState(@RequestBody MultipleState multipleState) {
		System.out.println("Enter into deleteMultipleState function"+multipleState.getMulStateRefCode());
	
		multipleStateDAO.deleteState(multipleState.getMulStateRefCode());
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/addNewState", method = RequestMethod.POST)
	public @ResponseBody Object addNewState(@RequestBody MultipleState multipleState) {
		System.out.println("Enter into addNewState function");
		multipleStateService.addState(multipleState);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/addNewStation", method = RequestMethod.POST)
	public @ResponseBody Object addNewStation(@RequestBody MultipleStation multipleStation) {
		System.out.println("Enter into addNewStation function");
		multipleStationService.addStation(multipleStation);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/fetchMultipleStationList", method = RequestMethod.GET)
	public @ResponseBody Object fetchStationList() {

		List<MultipleStation> multipleStations = multipleStationService.getAllStation();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", multipleStations);
		return map;
	}
	
	@RequestMapping(value = "/fetchMultipleStateList", method = RequestMethod.GET)
	public @ResponseBody Object fetchStateList() {

		List<MultipleState> multipleStates = multipleStateService.getAllState();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", multipleStates);
		return map;
	}
	
	@RequestMapping(value = "/removeAllMultipleStation", method = RequestMethod.POST)
	public @ResponseBody Object removeAllMultipleStation() {

		multipleStationService.deleteAllStation();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeAllMultipleState", method = RequestMethod.POST)
	public @ResponseBody Object removeAllMultipleState() {
	
		multipleStateService.deleteAllState();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}*/
	
	
	@RequestMapping(value = "/getBrKCodesFrV", method = RequestMethod.POST)
	public @ResponseBody Object getBrKCodesFrV() {
		System.out.println("Enter into getBrKCodesFrV function");
		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String,String>> reqList = brokerDAO.getBrkNCF();
		if(!reqList.isEmpty()){
			map.put("list",reqList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		
		return map;
	}
	
	
}

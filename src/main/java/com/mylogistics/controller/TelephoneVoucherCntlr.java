package com.mylogistics.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.TelephoneMstrDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.DAO.bank.ChequeLeavesDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.SubTelephoneMstr;
import com.mylogistics.model.TelephoneMstr;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.TelM_SubTelMService;
import com.mylogistics.services.TelVoucherService;
import com.mylogistics.services.TelVoucherServiceImpl;
import com.mylogistics.services.VoucherService;

@Controller
public class TelephoneVoucherCntlr {

	private static final Logger logger = Logger.getLogger(TelephoneVoucherCntlr.class);
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	@Autowired
	private BranchDAO branchDAO;
	@Autowired
	private HttpSession httpSession;
	@Autowired
	private BankMstrDAO bankMstrDAO;
	@Autowired
	private ChequeLeavesDAO chequeLeavesDAO;
	@Autowired
	private CashStmtDAO cashStmtDAO;
	@Autowired
	private TelephoneMstrDAO telephoneMstrDAO;
	@Autowired
	private FaMasterDAO faMasterDAO;
	@Autowired
	private SessionFactory sessionFactory;
	
	private TelVoucherService telVoucherService = new TelVoucherServiceImpl();
	
	@RequestMapping(value = "/getVDetFrTelV" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrTelV() {  
		logger.info("Enter into getVDetFrTelV---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				logger.info("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				logger.info("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					logger.info("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getTelephoneList" , method = RequestMethod.POST)  
	public @ResponseBody Object getTelephoneList() {  
		logger.info("enter into getTelephoneList function");
		Map<String,Object> map = new HashMap<String,Object>();
		List<TelephoneMstr> telMList = telephoneMstrDAO.getAllTelMstr();
		logger.info("employee of telephoneMstr = "+telMList.get(0).getEmployee().getEmpName());
		logger.info("Branch of telephoneMstr = "+telMList.get(0).getBranch().getbCode());
		if(!telMList.isEmpty()){
			map.put("tmList",telMList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/getChequeNoFrTV" , method = RequestMethod.POST)  
	public @ResponseBody Object getChequeNoFrTV(@RequestBody Map<String,String> clientMap) {  
		logger.info("Enter into getChequeNoFrTV---->");
		Map<String, Object> map = new HashMap<>();	
		String bankCode = clientMap.get("bankCode");
		String cType = clientMap.get("CType");
		char CType = cType.charAt(0);
		logger.info("value of CType = "+CType);
		ChequeLeaves chequeLeaves = bankMstrDAO.getAccPayChqByBnkC(bankCode,CType);
		List<ChequeLeaves> chqList = bankMstrDAO.getAPChqLByBnkC(bankCode,CType);
		logger.info("size of chqList = "+chqList.size());
		if(chequeLeaves != null){
			logger.info("***************** 1");
			map.put(ConstantsValues.CHEQUE_LEAVES,chequeLeaves);
			map.put("list",chqList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			logger.info("***************** 2");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/submitTMVoucher" , method = RequestMethod.POST)  
	public @ResponseBody Object submitTMVoucher(@RequestBody TelM_SubTelMService tm_stm) {  
		logger.info("enter into submitTMVoucher function");
		Map<String,Object> map = new HashMap<String,Object>();
		if(tm_stm != null){
			telVoucherService.addTmAndSTm(tm_stm);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}			
		return map;
	}
	
	@RequestMapping(value = "/fetchTmAndStm" , method = RequestMethod.POST)  
	public @ResponseBody Object fetchTmAndStm() {  
		logger.info("enter into fetchTmAndStm function");
		Map<String,Object> map = new HashMap<String,Object>();
		List<TelM_SubTelMService> tmstmList = telVoucherService.getAllTmAndSTm();
		logger.info("size of tmstmList ="+tmstmList.size());
		map.put("list",tmstmList);
		return map;
	}
	
	@RequestMapping(value = "/removeStmV" , method = RequestMethod.POST)  
	public @ResponseBody Object removeStmV(@RequestBody Map<String,Object> clientMap) {  
		logger.info("enter into removeStmV function ");
		int index = (int) clientMap.get("index");
		Map<String,Object> map = new HashMap<String,Object>();
		telVoucherService.remove(index);
		return map;
	}
	
	@RequestMapping(value = "/deleteAllTmAndStm" , method = RequestMethod.POST)  
	public @ResponseBody Object deleteAllTmAndStm() {  
		logger.info("enter into deleteAllTmAndStm function");
		Map<String,Object> map = new HashMap<String,Object>();
		telVoucherService.deleteAllTmAndSTm();
		return map;
	}
	
	
	@RequestMapping(value = "/submitTelVoucher" , method = RequestMethod.POST)
	public @ResponseBody Object submitTelVoucher(@RequestBody VoucherService voucherService) {  
		logger.info("enter into submitTelVoucher function");
		Map<String,Object> map = new HashMap<String,Object>();
		if(voucherService.getVoucherType().equalsIgnoreCase("Telephone")){
			 map = saveTelVoucher(voucherService);
			 int vhNo = (int) map.get("vhNo");
			if(vhNo>0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}
		return map;
	}
	
	
	public Map<String,Object> saveTelVoucher(VoucherService voucherService){
		logger.info("Enter into saveTelVoucher----->>>");
		User currentUser =	(User)httpSession.getAttribute("currentUser");
		Map<String,Object> map = new HashMap<String,Object>();
		
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		try {
			String brFaCode = voucherService.getBranch().getBranchFaCode();
			List<FAMaster> famList = faMasterDAO.getFAMByFaName(ConstantsValues.TEL_FANAME);
			String telFaCode = "";
			if(!famList.isEmpty()){
				telFaCode = famList.get(0).getFaMfaCode();
			}
			
			char payBy = voucherService.getPayBy();
			logger.info("value of payBy = "+payBy);
			String payMode = "";
			if(payBy == 'C'){
				payMode = "C";
				CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
				List<TelM_SubTelMService> tm_stmList = telVoucherService.getAllTmAndSTm();
				int voucherNo = 0;
				if(!tm_stmList.isEmpty()){
					
					for(int i=0;i<tm_stmList.size();i++){
						Map<String,Object> newMap = new HashMap<String,Object>();
						
						TelM_SubTelMService telM_SubTelM = tm_stmList.get(i);
						TelephoneMstr telMstr = telM_SubTelM.gettMstr();
						SubTelephoneMstr sTelMstr = telM_SubTelM.getStMstr();
						
						String tvNo = telMstr.getTmPhNo();					
						
						String telBrFaCode = telMstr.getBranch().getBranchFaCode(); 
						
						if(brFaCode.equalsIgnoreCase(telBrFaCode)){
							
							CashStmt cashStmt = new CashStmt();
							cashStmt.setbCode(currentUser.getUserBranchCode());
							cashStmt.setUserCode(currentUser.getUserCode());
							cashStmt.setCsDescription(voucherService.getDesc());
							cashStmt.setCsDrCr('D');
							cashStmt.setCsAmt(sTelMstr.getStmPayAmt());
							cashStmt.setCsType(voucherService.getVoucherType());
							cashStmt.setCsVouchType("cash");
							cashStmt.setCsFaCode(telFaCode);
							cashStmt.setCsTvNo(tvNo);						
							cashStmt.setCsVouchNo(voucherNo);
							cashStmt.setPayMode(payMode);
							
							java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
							cashStmt.setCsDt(sqlDate);
							
							newMap = cashStmtStatusDAO.updateCSSForTelV(session, cashStmtStatus.getCssId(), cashStmt);
							voucherNo = (int) newMap.get("vhNo");
							
							logger.info("cashStmt id = "+cashStmt.getCsId());
							sTelMstr.setStmCsId(cashStmt.getCsId());
							sTelMstr.setStmTvNo(cashStmt.getCsTvNo());
							sTelMstr.setbCode(currentUser.getUserBranchCode());
							sTelMstr.setUserCode(currentUser.getUserCode());
							telephoneMstrDAO.updateTMBySTM(session, telMstr.getTmId(), sTelMstr);
							
							cashStmt.setCsSFId(sTelMstr.getStmId());
							cashStmtStatusDAO.updateCSSBySFId(session, cashStmt.getCsId() , sTelMstr.getStmId());
							
							double extraAmt = sTelMstr.getStmTotDedAmt() - sTelMstr.getStmPayAmt();
							if(extraAmt > 0){
								CashStmt exCashStmt = new CashStmt();
								exCashStmt.setbCode(currentUser.getUserBranchCode());
								exCashStmt.setUserCode(currentUser.getUserCode());
								exCashStmt.setCsDescription(voucherService.getDesc());
								exCashStmt.setCsDrCr('D');
								exCashStmt.setCsAmt(extraAmt);
								exCashStmt.setCsType(voucherService.getVoucherType());
								exCashStmt.setCsVouchType("cash");
								exCashStmt.setCsFaCode(telMstr.getEmployee().getEmpFaCode());
								exCashStmt.setCsTvNo(tvNo);
								exCashStmt.setCsVouchNo(voucherNo);
								exCashStmt.setPayMode(payMode);
								
								exCashStmt.setCsDt(sqlDate);
								
								newMap = cashStmtStatusDAO.updateCSSForTelV(session, cashStmtStatus.getCssId(), exCashStmt);
								voucherNo = (int) newMap.get("vhNo");
							}
						
						}else{
							
							CashStmt cashStmt1 = new CashStmt();
							cashStmt1.setbCode(currentUser.getUserBranchCode());
							cashStmt1.setUserCode(currentUser.getUserCode());
							cashStmt1.setCsDescription(voucherService.getDesc());
							cashStmt1.setCsDrCr('D');
							cashStmt1.setCsAmt(sTelMstr.getStmTotDedAmt());
							cashStmt1.setCsType(voucherService.getVoucherType());
							cashStmt1.setCsVouchType("cash");
							cashStmt1.setCsFaCode(telMstr.getBranch().getBranchFaCode());
							cashStmt1.setCsTvNo(tvNo);						
							cashStmt1.setCsVouchNo(voucherNo);
							cashStmt1.setPayMode(payMode);
							
							java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
							cashStmt1.setCsDt(sqlDate);
							
							newMap = cashStmtStatusDAO.updateCSSForTelV(session, cashStmtStatus.getCssId(), cashStmt1);
							voucherNo = (int) newMap.get("vhNo");
							
							
							logger.info("cashStmt id = "+cashStmt1.getCsId());
							sTelMstr.setStmCsId(cashStmt1.getCsId());
							sTelMstr.setStmTvNo(cashStmt1.getCsTvNo());
							sTelMstr.setbCode(currentUser.getUserBranchCode());
							sTelMstr.setUserCode(currentUser.getUserCode());
							telephoneMstrDAO.updateTMBySTM(session, telMstr.getTmId(), sTelMstr);
							
							cashStmt1.setCsSFId(sTelMstr.getStmId());
							cashStmtStatusDAO.updateCSSBySFId(session, cashStmt1.getCsId() , sTelMstr.getStmId());
							
							CashStmtTemp cashStmt2 = new CashStmtTemp();
							cashStmt2.setbCode(currentUser.getUserBranchCode());
							cashStmt2.setUserCode(currentUser.getUserCode());
							cashStmt2.setCsDescription(voucherService.getDesc());
							cashStmt2.setCsDrCr('C');
							cashStmt2.setCsAmt(sTelMstr.getStmTotDedAmt());
							cashStmt2.setCsType(voucherService.getVoucherType());
							cashStmt2.setCsVouchType("contra");
							cashStmt2.setCsFaCode(voucherService.getBranch().getBranchFaCode());
							cashStmt2.setCsBrhFaCode(telMstr.getBranch().getBranchFaCode());
							cashStmt2.setCsTvNo(tvNo);						
							cashStmt2.setCsDt(sqlDate);
							cashStmt2.setPayMode(payMode);
							
							cashStmtStatusDAO.saveCsTemp(session, cashStmt2);
							
							CashStmtTemp cashStmt3 = new CashStmtTemp();
							cashStmt3.setbCode(currentUser.getUserBranchCode());
							cashStmt3.setUserCode(currentUser.getUserCode());
							cashStmt3.setCsDescription(voucherService.getDesc());
							cashStmt3.setCsDrCr('D');
							cashStmt3.setCsAmt(sTelMstr.getStmPayAmt());
							cashStmt3.setCsType(voucherService.getVoucherType());
							cashStmt3.setCsVouchType("contra");
							cashStmt3.setCsFaCode(telFaCode);
							cashStmt3.setCsBrhFaCode(telMstr.getBranch().getBranchFaCode());
							cashStmt3.setCsTvNo(tvNo);						
							cashStmt3.setCsDt(sqlDate);
							cashStmt3.setPayMode(payMode);
							
							cashStmtStatusDAO.saveCsTemp(session, cashStmt3);
							
							double extraAmt = sTelMstr.getStmTotDedAmt() - sTelMstr.getStmPayAmt();
							if(extraAmt > 0){
								CashStmtTemp exCashStmt = new CashStmtTemp();
								exCashStmt.setbCode(currentUser.getUserBranchCode());
								exCashStmt.setUserCode(currentUser.getUserCode());
								exCashStmt.setCsDescription(voucherService.getDesc());
								exCashStmt.setCsDrCr('D');
								exCashStmt.setCsAmt(extraAmt);
								exCashStmt.setCsType(voucherService.getVoucherType());
								exCashStmt.setCsVouchType("contra");
								exCashStmt.setCsFaCode(telMstr.getEmployee().getEmpFaCode());
								exCashStmt.setCsTvNo(tvNo);
								exCashStmt.setCsBrhFaCode(telMstr.getBranch().getBranchFaCode());
								exCashStmt.setCsDt(sqlDate);
								exCashStmt.setPayMode(payMode);
								
								cashStmtStatusDAO.saveCsTemp(session, exCashStmt);
							}
						}
					}
					
					telVoucherService.deleteAllTmAndSTm();
					map.put("vhNo",voucherNo);
				  }
			}else if(payBy == 'Q'){
					payMode = "Q";
					char chequeType = voucherService.getChequeType();
					logger.info("chequeType = "+chequeType);
					CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
					String bankCode = voucherService.getBankCode();
					List<BankMstr> bankMList = bankMstrDAO.getBankByBankCode(session, bankCode);
					if(!bankMList.isEmpty()){
						BankMstr bankMstr = bankMList.get(0);
						List<TelM_SubTelMService> tm_stmList = telVoucherService.getAllTmAndSTm();
						int voucherNo = 0;
						double totAmt = 0.0;
						if(!tm_stmList.isEmpty()){
							for(int i=0;i<tm_stmList.size();i++){
								totAmt = totAmt + tm_stmList.get(i).getStMstr().getStmTotDedAmt();
							}	
						}
						ChequeLeaves chequeLeaves = voucherService.getChequeLeaves();
						chequeLeaves.setBankMstr(bankMstr);
						chequeLeaves.setChqLChqAmt(totAmt);
						chequeLeaves.setChqLUsedDt(new Date(new java.util.Date().getTime()));
						chequeLeaves.setChqLUsed(true);
						String tvNo = chequeLeaves.getChqLChqNo();
						String phoneNo = "";					
						int temp = chequeLeavesDAO.updateChqLeaves(session, chequeLeaves);
						// InterBranch Case Main Cash Stmt Start
						double amount = 0.0;
						int count = 0;
						for(int i=0;i<tm_stmList.size();i++){
			
							TelM_SubTelMService telM_SubTelM = tm_stmList.get(i);
							SubTelephoneMstr sTelMstr = telM_SubTelM.getStMstr();
							amount = amount + sTelMstr.getStmTotDedAmt();
						}
						logger.info(count+" phone no are from other branches--");
						CashStmt mainCashStmt = new CashStmt();
						
							Map<String,Object> newMap11 = new HashMap<String,Object>();
							
							mainCashStmt.setbCode(currentUser.getUserBranchCode());
							mainCashStmt.setUserCode(currentUser.getUserCode());
							mainCashStmt.setCsDescription(voucherService.getDesc());
							mainCashStmt.setCsDrCr('C');
							mainCashStmt.setCsAmt(amount);
							mainCashStmt.setCsType(voucherService.getVoucherType());
							mainCashStmt.setCsVouchType("bank");
							mainCashStmt.setCsChequeType(chequeType);
							mainCashStmt.setCsPayTo(voucherService.getPayTo());
							mainCashStmt.setCsFaCode(voucherService.getBankCode());					
							mainCashStmt.setCsTvNo(tvNo);						
							java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
							mainCashStmt.setCsDt(sqlDate);
							mainCashStmt.setPayMode(payMode);
							
							newMap11 = cashStmtStatusDAO.updateCSSForTelV(session, cashStmtStatus.getCssId(), mainCashStmt);
							voucherNo = (int) newMap11.get("vhNo");
						// InterBranch Case Main Cash Stmt End
						
						if(temp > 0){
							if(!tm_stmList.isEmpty()){
								for(int i=0;i<tm_stmList.size();i++){
									Map<String,Object> newMap = new HashMap<String,Object>();
									
									TelM_SubTelMService telM_SubTelM = tm_stmList.get(i);
									TelephoneMstr telMstr = telM_SubTelM.gettMstr();
									SubTelephoneMstr sTelMstr = telM_SubTelM.getStMstr();
									
									phoneNo = telMstr.getTmPhNo();
									String telBrFaCode = telMstr.getBranch().getBranchFaCode(); 
									
									if(brFaCode.equalsIgnoreCase(telBrFaCode)){
										
										CashStmt cashStmt = new CashStmt();
										cashStmt.setbCode(currentUser.getUserBranchCode());
										cashStmt.setUserCode(currentUser.getUserCode());
										cashStmt.setCsDescription(voucherService.getDesc());
										cashStmt.setCsDrCr('D');
										cashStmt.setCsAmt(sTelMstr.getStmPayAmt());
										cashStmt.setCsType(voucherService.getVoucherType());
										cashStmt.setCsVouchType("bank");
										cashStmt.setCsChequeType(chequeType);
										cashStmt.setCsPayTo(voucherService.getPayTo());
										cashStmt.setCsFaCode(telFaCode);
										cashStmt.setCsVouchNo(voucherNo);
										cashStmt.setCsTvNo(phoneNo+" ("+tvNo+")");									
										sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
										cashStmt.setCsDt(sqlDate);
										cashStmt.setPayMode(payMode);
										
										newMap = cashStmtStatusDAO.updateCSSForTelV(session,  cashStmtStatus.getCssId(), cashStmt);
										voucherNo = (int) newMap.get("vhNo");
										logger.info("cashStmt id = "+cashStmt.getCsId());
										
										sTelMstr.setStmCsId(cashStmt.getCsId());
										sTelMstr.setStmTvNo(cashStmt.getCsTvNo());
										sTelMstr.setbCode(currentUser.getUserBranchCode());
										sTelMstr.setUserCode(currentUser.getUserCode());
										telephoneMstrDAO.updateTMBySTM(session, telMstr.getTmId(), sTelMstr);
										
										cashStmt.setCsSFId(sTelMstr.getStmId());
										cashStmtStatusDAO.updateCSSBySFId(session, cashStmt.getCsId() , sTelMstr.getStmId());
										
										double extraAmt = sTelMstr.getStmTotDedAmt() - sTelMstr.getStmPayAmt();
										if(extraAmt > 0){
											CashStmt exCashStmt = new CashStmt();
											exCashStmt.setbCode(currentUser.getUserBranchCode());
											exCashStmt.setUserCode(currentUser.getUserCode());
											exCashStmt.setCsDescription(voucherService.getDesc());
											exCashStmt.setCsDrCr('D');
											exCashStmt.setCsAmt(extraAmt);
											exCashStmt.setCsType(voucherService.getVoucherType());
											exCashStmt.setCsVouchType("bank");
											exCashStmt.setCsChequeType(chequeType);
											exCashStmt.setCsPayTo(voucherService.getPayTo());
											exCashStmt.setCsFaCode(telMstr.getEmployee().getEmpFaCode());
											exCashStmt.setCsTvNo(phoneNo+" ("+tvNo+")");
											exCashStmt.setCsVouchNo(voucherNo);										
											exCashStmt.setCsDt(sqlDate);
											exCashStmt.setPayMode(payMode);
											
											newMap = cashStmtStatusDAO.updateCSSForTelV(session, cashStmtStatus.getCssId(), exCashStmt);
											voucherNo = (int) newMap.get("vhNo");
										}
										
									}else{
										
										CashStmt cashStmt1 = new CashStmt();
										cashStmt1.setbCode(currentUser.getUserBranchCode());
										cashStmt1.setUserCode(currentUser.getUserCode());
										cashStmt1.setCsDescription(voucherService.getDesc());
										cashStmt1.setCsDrCr('D');
										cashStmt1.setCsAmt(sTelMstr.getStmTotDedAmt());
										cashStmt1.setCsType(voucherService.getVoucherType());
										cashStmt1.setCsVouchType("bank");
										cashStmt1.setCsChequeType(chequeType);
										cashStmt1.setCsPayTo(voucherService.getPayTo());
										cashStmt1.setCsFaCode(telMstr.getBranch().getBranchFaCode());
										cashStmt1.setCsVouchNo(voucherNo);
										cashStmt1.setCsTvNo(phoneNo+" ("+tvNo+")");									
										sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
										cashStmt1.setCsDt(sqlDate);
										cashStmt1.setPayMode(payMode);
										
										logger.info("cashStmt1 is going to save in DB");
										newMap = cashStmtStatusDAO.updateCSSForTelV(session, cashStmtStatus.getCssId(), cashStmt1);
										voucherNo = (int) newMap.get("vhNo");
										logger.info("cashStmt id = "+cashStmt1.getCsId());
										
										sTelMstr.setStmCsId(cashStmt1.getCsId());
										sTelMstr.setStmTvNo(cashStmt1.getCsTvNo());
										sTelMstr.setbCode(currentUser.getUserBranchCode());
										sTelMstr.setUserCode(currentUser.getUserCode());
										telephoneMstrDAO.updateTMBySTM(session, telMstr.getTmId(), sTelMstr);
										
										cashStmt1.setCsSFId(sTelMstr.getStmId());
										cashStmtStatusDAO.updateCSSBySFId(session, cashStmt1.getCsId() , sTelMstr.getStmId());
										
										CashStmtTemp cashStmt2 = new CashStmtTemp();
										cashStmt2.setbCode(currentUser.getUserBranchCode());
										cashStmt2.setUserCode(currentUser.getUserCode());
										cashStmt2.setCsDescription(voucherService.getDesc());
										cashStmt2.setCsDrCr('C');
										cashStmt2.setCsAmt(sTelMstr.getStmTotDedAmt());
										cashStmt2.setCsType(voucherService.getVoucherType());
										cashStmt2.setCsVouchType("contra");
										cashStmt2.setCsChequeType(chequeType);
										cashStmt2.setCsPayTo(voucherService.getPayTo());
										cashStmt2.setCsTvNo(phoneNo+" ("+tvNo+")");
										cashStmt2.setCsFaCode(voucherService.getBranch().getBranchFaCode());
										cashStmt2.setCsBrhFaCode(telMstr.getBranch().getBranchFaCode());
										cashStmt2.setCsDt(sqlDate);
										cashStmt2.setPayMode(payMode);
										
										cashStmtStatusDAO.saveCsTemp(session, cashStmt2);
										CashStmtTemp cashStmt3 = new CashStmtTemp();
										cashStmt3.setbCode(currentUser.getUserBranchCode());
										cashStmt3.setUserCode(currentUser.getUserCode());
										cashStmt3.setCsDescription(voucherService.getDesc());
										cashStmt3.setCsDrCr('D');
										cashStmt3.setCsAmt(sTelMstr.getStmPayAmt());
										cashStmt3.setCsType(voucherService.getVoucherType());
										cashStmt3.setCsVouchType("contra");
										cashStmt3.setCsChequeType(chequeType);
										cashStmt3.setCsPayTo(voucherService.getPayTo());
										cashStmt3.setCsFaCode(telFaCode);
										cashStmt3.setCsBrhFaCode(telMstr.getBranch().getBranchFaCode());
										cashStmt3.setCsTvNo(phoneNo+" ("+tvNo+")");									
										cashStmt3.setCsDt(sqlDate);
										cashStmt3.setPayMode(payMode);
										
										cashStmtStatusDAO.saveCsTemp(session, cashStmt3);
										double extraAmt = sTelMstr.getStmTotDedAmt() - sTelMstr.getStmPayAmt();
										if(extraAmt > 0){
											CashStmtTemp exCashStmt = new CashStmtTemp();
											exCashStmt.setbCode(currentUser.getUserBranchCode());
											exCashStmt.setUserCode(currentUser.getUserCode());
											exCashStmt.setCsDescription(voucherService.getDesc());
											exCashStmt.setCsDrCr('D');
											exCashStmt.setCsAmt(extraAmt);
											exCashStmt.setCsType(voucherService.getVoucherType());
											exCashStmt.setCsVouchType("contra");
											exCashStmt.setCsChequeType(chequeType);
											exCashStmt.setCsPayTo(voucherService.getPayTo());
											exCashStmt.setCsFaCode(telMstr.getEmployee().getEmpFaCode());
											exCashStmt.setCsTvNo(phoneNo+" ("+tvNo+")");
											exCashStmt.setCsBrhFaCode(telMstr.getBranch().getBranchFaCode());
											exCashStmt.setCsDt(sqlDate);
											exCashStmt.setPayMode(payMode);
											
											cashStmtStatusDAO.saveCsTemp(session, exCashStmt);
										}
									}
									
								}
								
								telVoucherService.deleteAllTmAndSTm();
								map.put("vhNo",voucherNo);
							  }else{
								  logger.info("tm_stmList list is empty");
								  map.put("vhNo",0);
							  }
						}else{
							logger.info("chequeLeaves not updated");
							map.put("vhNo",0);
						}
					}else{
						logger.info("invalid bank code");
						map.put("vhNo",0);
					}
			}else if(payBy == 'O'){
				payMode = "O";
				CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
				List<TelM_SubTelMService> tm_stmList = telVoucherService.getAllTmAndSTm();
				int voucherNo = 0;
				if(!tm_stmList.isEmpty()){
					
					String phoneNo="";
					// InterBranch Case Main Cash Stmt Start
					double amount = 0.0;
					int count = 0;
					for(int i=0;i<tm_stmList.size();i++){
		
						TelM_SubTelMService telM_SubTelM = tm_stmList.get(i);
						TelephoneMstr telMstr = telM_SubTelM.gettMstr();
						SubTelephoneMstr sTelMstr = telM_SubTelM.getStMstr();
							
						amount = amount + sTelMstr.getStmTotDedAmt();
						
						phoneNo = phoneNo+telMstr.getTmPhNo()+",";
					}
					
					logger.info(count+" phone no are from other branches--");
					phoneNo = phoneNo.substring(0, phoneNo.length()-1);
					
					
					CashStmt mainCashStmt = new CashStmt();
					
						Map<String,Object> newMap = new HashMap<String,Object>();
						
						mainCashStmt.setbCode(currentUser.getUserBranchCode());
						mainCashStmt.setUserCode(currentUser.getUserCode());
						mainCashStmt.setCsDescription(voucherService.getDesc());
						mainCashStmt.setCsDrCr('C');
						mainCashStmt.setCsAmt(amount);
						mainCashStmt.setCsType(voucherService.getVoucherType());
						mainCashStmt.setCsVouchType("bank");
						mainCashStmt.setCsPayTo(voucherService.getPayTo());
						mainCashStmt.setCsFaCode(voucherService.getBankCode());
						mainCashStmt.setCsTvNo(phoneNo);					
						java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
						mainCashStmt.setCsDt(sqlDate);
						mainCashStmt.setPayMode(payMode);
						
						newMap = cashStmtStatusDAO.updateCSSForTelV(session, cashStmtStatus.getCssId(), mainCashStmt);
						voucherNo = (int) newMap.get("vhNo");
						List<BankMstr> bnkList= bankMstrDAO.getBankByBankCode(session, voucherService.getBankCode());
						if(!bnkList.isEmpty()){
							bankMstrDAO.decBankBal(session, bnkList.get(0).getBnkId(), amount);
						}
					// InterBranch Case Main Cash Stmt End
					
					for(int i=0;i<tm_stmList.size();i++){
						Map<String,Object> newMap11 = new HashMap<String,Object>();
						
						TelM_SubTelMService telM_SubTelM = tm_stmList.get(i);
						TelephoneMstr telMstr = telM_SubTelM.gettMstr();
						SubTelephoneMstr sTelMstr = telM_SubTelM.getStMstr();
						
						String tvNo = telMstr.getTmPhNo();					
						
						String telBrFaCode = telMstr.getBranch().getBranchFaCode(); 
						
						if(brFaCode.equalsIgnoreCase(telBrFaCode)){
							
							CashStmt cashStmt = new CashStmt();
							cashStmt.setbCode(currentUser.getUserBranchCode());
							cashStmt.setUserCode(currentUser.getUserCode());
							cashStmt.setCsDescription(voucherService.getDesc());
							cashStmt.setCsDrCr('D');
							cashStmt.setCsAmt(sTelMstr.getStmPayAmt());
							cashStmt.setCsType(voucherService.getVoucherType());
							cashStmt.setCsVouchType("bank");
							cashStmt.setCsPayTo(voucherService.getPayTo());
							cashStmt.setCsFaCode(telFaCode);
							cashStmt.setCsVouchNo(voucherNo);
							cashStmt.setCsTvNo(tvNo);						
							sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
							cashStmt.setCsDt(sqlDate);
							cashStmt.setPayMode(payMode);
							
							newMap11 = cashStmtStatusDAO.updateCSSForTelV(session, cashStmtStatus.getCssId(), cashStmt);
							voucherNo = (int) newMap11.get("vhNo");
							logger.info("cashStmt id = "+cashStmt.getCsId());
							
							sTelMstr.setStmCsId(cashStmt.getCsId());
							sTelMstr.setStmTvNo(cashStmt.getCsTvNo());
							sTelMstr.setbCode(currentUser.getUserBranchCode());
							sTelMstr.setUserCode(currentUser.getUserCode());
							telephoneMstrDAO.updateTMBySTM(session, telMstr.getTmId(), sTelMstr);
							
							cashStmt.setCsSFId(sTelMstr.getStmId());
							cashStmtStatusDAO.updateCSSBySFId(session, cashStmt.getCsId() , sTelMstr.getStmId());
							
							double extraAmt = sTelMstr.getStmTotDedAmt() - sTelMstr.getStmPayAmt();
							if(extraAmt > 0){
								CashStmt exCashStmt = new CashStmt();
								exCashStmt.setbCode(currentUser.getUserBranchCode());
								exCashStmt.setUserCode(currentUser.getUserCode());
								exCashStmt.setCsDescription(voucherService.getDesc());
								exCashStmt.setCsDrCr('D');
								exCashStmt.setCsAmt(extraAmt);
								exCashStmt.setCsType(voucherService.getVoucherType());
								exCashStmt.setCsVouchType("bank");
								exCashStmt.setCsPayTo(voucherService.getPayTo());
								exCashStmt.setCsFaCode(telMstr.getEmployee().getEmpFaCode());
								exCashStmt.setCsTvNo(tvNo);
								exCashStmt.setCsVouchNo(voucherNo);							
								exCashStmt.setCsDt(sqlDate);
								exCashStmt.setPayMode(payMode);
								
								newMap11 = cashStmtStatusDAO.updateCSSForTelV(session, cashStmtStatus.getCssId(), exCashStmt);
								voucherNo = (int) newMap11.get("vhNo");
							}
							
						}else{
							
							CashStmt cashStmt1 = new CashStmt();
							cashStmt1.setbCode(currentUser.getUserBranchCode());
							cashStmt1.setUserCode(currentUser.getUserCode());
							cashStmt1.setCsDescription(voucherService.getDesc());
							cashStmt1.setCsDrCr('D');
							cashStmt1.setCsAmt(sTelMstr.getStmTotDedAmt());
							cashStmt1.setCsType(voucherService.getVoucherType());
							cashStmt1.setCsVouchType("bank");
							cashStmt1.setCsPayTo(voucherService.getPayTo());
							cashStmt1.setCsFaCode(telMstr.getBranch().getBranchFaCode());
							cashStmt1.setCsVouchNo(voucherNo);
							cashStmt1.setCsTvNo(tvNo);						
							sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
							cashStmt1.setCsDt(sqlDate);
							cashStmt1.setPayMode(payMode);
							
							logger.info("cashStmt1 is going to save in DB");
							newMap = cashStmtStatusDAO.updateCSSForTelV(session, cashStmtStatus.getCssId(), cashStmt1);
							voucherNo = (int) newMap.get("vhNo");
							logger.info("cashStmt id = "+cashStmt1.getCsId());
							
							sTelMstr.setStmCsId(cashStmt1.getCsId());
							sTelMstr.setStmTvNo(cashStmt1.getCsTvNo());
							sTelMstr.setbCode(currentUser.getUserBranchCode());
							sTelMstr.setUserCode(currentUser.getUserCode());
							telephoneMstrDAO.updateTMBySTM(session, telMstr.getTmId(), sTelMstr);
							
							cashStmt1.setCsSFId(sTelMstr.getStmId());
							cashStmtStatusDAO.updateCSSBySFId(session, cashStmt1.getCsId() , sTelMstr.getStmId());
							
							CashStmtTemp cashStmt2 = new CashStmtTemp();
							cashStmt2.setbCode(currentUser.getUserBranchCode());
							cashStmt2.setUserCode(currentUser.getUserCode());
							cashStmt2.setCsDescription(voucherService.getDesc());
							cashStmt2.setCsDrCr('C');
							cashStmt2.setCsAmt(sTelMstr.getStmTotDedAmt());
							cashStmt2.setCsType(voucherService.getVoucherType());
							cashStmt2.setCsVouchType("contra");
							cashStmt2.setCsPayTo(voucherService.getPayTo());
							cashStmt2.setCsVouchNo(voucherNo);
							cashStmt2.setCsTvNo(tvNo);
							cashStmt2.setCsFaCode(voucherService.getBranch().getBranchFaCode());
							cashStmt2.setCsBrhFaCode(telMstr.getBranch().getBranchFaCode());
							cashStmt2.setCsDt(sqlDate);
							cashStmt2.setPayMode(payMode);
							
							cashStmtStatusDAO.saveCsTemp(session, cashStmt2);
							CashStmtTemp cashStmt3 = new CashStmtTemp();
							cashStmt3.setbCode(currentUser.getUserBranchCode());
							cashStmt3.setUserCode(currentUser.getUserCode());
							cashStmt3.setCsDescription(voucherService.getDesc());
							cashStmt3.setCsDrCr('D');
							cashStmt3.setCsAmt(sTelMstr.getStmTotDedAmt());
							cashStmt3.setCsType(voucherService.getVoucherType());
							cashStmt3.setCsVouchType("contra");
							cashStmt3.setCsFaCode(telFaCode);
							cashStmt3.setCsTvNo(tvNo);
							cashStmt3.setCsPayTo(voucherService.getPayTo());
							cashStmt3.setCsBrhFaCode(telMstr.getBranch().getBranchFaCode());
							cashStmt3.setCsDt(sqlDate);
							cashStmt3.setPayMode(payMode);
							
							cashStmtStatusDAO.saveCsTemp(session, cashStmt3);
							double extraAmt = sTelMstr.getStmTotDedAmt() - sTelMstr.getStmPayAmt();
							if(extraAmt > 0){
								CashStmtTemp exCashStmt = new CashStmtTemp();
								exCashStmt.setbCode(currentUser.getUserBranchCode());
								exCashStmt.setUserCode(currentUser.getUserCode());
								exCashStmt.setCsDescription(voucherService.getDesc());
								exCashStmt.setCsDrCr('D');
								exCashStmt.setCsAmt(extraAmt);
								exCashStmt.setCsType(voucherService.getVoucherType());
								exCashStmt.setCsVouchType("contra");
								exCashStmt.setCsPayTo(voucherService.getPayTo());
								exCashStmt.setCsFaCode(telMstr.getEmployee().getEmpFaCode());
								exCashStmt.setCsTvNo(tvNo);
								exCashStmt.setCsBrhFaCode(telMstr.getBranch().getBranchFaCode());
								exCashStmt.setCsDt(sqlDate);
								exCashStmt.setPayMode(payMode);
								
								cashStmtStatusDAO.saveCsTemp(session, exCashStmt);
							}
							
						}
						
					}
					
					telVoucherService.deleteAllTmAndSTm();
					map.put("vhNo",voucherNo);
				  }
			}else{
				map.put("vhNo",0);
				map.put("tvNo","0");
				logger.info("invalid payment method");
			}
			transaction.commit();
		}
		catch(Exception e) {
			logger.error(e.getMessage(), e);
			transaction.rollback();
			map.put("vhNo", 0);
		} finally {
			session.close();
		}
		return map;
	}	
	
	@RequestMapping(value = "/getVDetFrTelVBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrTelVBack() {  
		logger.info("Enter into getVDetFrTelVBack---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				logger.info("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				logger.info("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					logger.info("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
}
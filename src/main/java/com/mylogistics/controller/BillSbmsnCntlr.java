package com.mylogistics.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mylogistics.DAO.BillForwardingDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.model.Bill;
import com.mylogistics.model.BillForwarding;
import com.mylogistics.model.CnmtImage;
import com.mylogistics.model.Customer;
import com.mylogistics.services.ConstantsValues;

@Controller
public class BillSbmsnCntlr {

	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private BillForwardingDAO billForwardingDAO;
	
	private List<Map<String,Object>> bfList = new ArrayList<>();
	
	private String bfImgPath = "/var/www/html/Erp_Image/BillSubmission";

	
	@RequestMapping(value = "/getCustFrBlSbmsn", method = RequestMethod.POST)
	public @ResponseBody Object getCustFrBlSbmsn() {
		System.out.println("enter into getCustFrBlSbmsn function");
		Map<String,Object> map = new HashMap<>();
		List<Customer> custList = new ArrayList<>();
		custList = customerDAO.getCustFrBillSbmsn();
		if(!custList.isEmpty()){
			map.put("list",custList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getBlFrwdFrSbmsn", method = RequestMethod.POST)
	public @ResponseBody Object getBlFrwdFrSbmsn(@RequestBody Map<String,Object> clientMap) {
		System.out.println("enter into getBlFrwdFrSbmsn function");
		Map<String,Object> map = new HashMap<>();
		int custId = Integer.parseInt(String.valueOf(clientMap.get("custId")));
		if(custId > 0){
			List<BillForwarding> bfList = new ArrayList<>();
			bfList = billForwardingDAO.getBfFrBillSbmsn(custId);
			if(!bfList.isEmpty()){
				map.put("list",bfList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put("msg","There is no BillForwarding for submission");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put("msg","invalid customer");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getBillFrSbmsn", method = RequestMethod.POST)
	public @ResponseBody Object getBillFrSbmsn(@RequestBody Map<String,Object> clientMap) {
		System.out.println("enter into getBillFrSbmsn function");
		Map<String,Object> map = new HashMap<>();

		int bfId = Integer.parseInt(String.valueOf(clientMap.get("bfId"))); 
		if(bfId > 0){
			List<Bill> blList = new ArrayList<>();
			blList = billForwardingDAO.getBillListById(bfId);
			if(!blList.isEmpty()){
				System.out.println("size of blList = "+blList.size());
				map.put("list",blList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put("msg","There is no bill for this BillForwarding no.");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put("msg","invalid BillForwarding No");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/submitBillSbmsn", method = RequestMethod.POST)
	public @ResponseBody Object submitBillSbmsn(@RequestBody BillForwarding billForwarding) {
		System.out.println("enter into submitBillSbmsn function");
		Map<String,Object> map = new HashMap<>();
		
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			
			if(billForwarding.getBfRecDt() != null){
				
				
				
				int cnRes = 0;
				
				try{
					Path path = Paths.get(bfImgPath);
					if(! Files.exists(path))
						Files.createDirectories(path);
				}catch(Exception e){
					System.out.println("Error in creating direcotry : "+e);
					//logger.error("Error in creating directory : "+e);
				}
				if(!bfList.isEmpty()){
					for(int i=0;i<bfList.size();i++){
						int bfId =  (int)bfList.get(i).get("bfId");
						System.out.println("bfNo : "+bfId);
						Blob blob = (Blob) bfList.get(i).get("blob");
						if(bfId > 0 && blob!=null){
							//try{						
								String imagePath = bfImgPath+"/BF"+bfId+".pdf";
								System.out.println("imagePath="+imagePath);
								File file = new File(imagePath);
								if(file.exists()) {
									file.delete();
									System.out.println("imagePath= delete");
								}
									
								if(file.createNewFile()){
									System.out.println("new file create");
									byte imageInBytes[] = blob.getBytes(1, (int)blob.length());							
									OutputStream targetFile=  new FileOutputStream(file);
						            targetFile.write(imageInBytes);			            
						            targetFile.close();
						            System.out.println("image created");
						            billForwarding.setBfImagePath(imagePath);
						            bfList.clear();
									}
									//result = 1;
								/*}catch(Exception e){
									//logger.error("Error in addCnmtImg() : "+e);
									System.out.println("Error in addBfImg() : "+e);
									//result = -1;
								}*/
						}
					}
				}else{
					//logger.info("CNMT image is not uploaded due to Not selecting cnmt in arrivalreport : ");
				}
				
				
				
				
				cnRes = billForwardingDAO.saveBlSbmsnDt(billForwarding,session);
				session.flush();
				session.clear();
				transaction.commit();
				
				if(cnRes > 0){
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put("msg","SERVER ERROR");
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put("msg","invalid BillForwarding No");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
			transaction.rollback();
		}finally {
			bfList.clear();
			session.close();
		}
		
		return map;
	}
	
	
	
	
	
	
	@RequestMapping(value = "/uploadBfImage", method = RequestMethod.POST)
	public @ResponseBody Object uploadBfImage(@RequestParam("file") MultipartFile file, @RequestParam("bfId") int bfId)throws Exception {
		System.out.println("enter into uploadBfImage function === "+bfId);
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		Map<String,Object> resMap = new HashMap<>();
		Blob blob = null;
		try{
			//blob = Hibernate.createBlob(fileContent);
			//modelService.setBlob(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			resMap.put("bfId",bfId);
			resMap.put("blob",blob);
			bfList.add(resMap);
			//modelService.setBlob(blob); 
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	
	
	
}

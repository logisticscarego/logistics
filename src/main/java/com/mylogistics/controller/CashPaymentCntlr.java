package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;

@Controller
public class CashPaymentCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;

	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	public static Logger logger = Logger.getLogger(CashPaymentCntlr.class);	
	
	@RequestMapping(value = "/getCashPayDetails" , method = RequestMethod.POST)  
	public @ResponseBody Object getCashPayDetails(){
		logger.info("Enter into /getCashPayDetails()");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();			
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				logger.info("User Branch Found : Branch Code = "+branchCode);
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				if(cashStmtStatus != null){
					logger.info("CashStmtStatus found : CashStmtStatus ID = "+cashStmtStatus.getCssId());
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					logger.info("CashStmtStatus not found !");
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				logger.info("User Branch not found !");
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			logger.info("User is not found in httpSession !");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/getFaCodeFrCP" , method = RequestMethod.POST)  
	public @ResponseBody Object getFaCodeFrCP() {  
		System.out.println("Enter into getFaCodeFrCP---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user!=null){
			List<FAMaster> faMList = new ArrayList<FAMaster>();
			
			List<FAMaster> fmEmpList = faMasterDAO.getMultiFaM("employee");
			if(!fmEmpList.isEmpty()){
				for(int i=0;i<fmEmpList.size();i++){
					faMList.add(fmEmpList.get(i));
				}
			}
			
			List<FAMaster> fmOwnList = faMasterDAO.getMultiFaM("owner");
			if(!fmOwnList.isEmpty()){
				for(int i=0;i<fmOwnList.size();i++){
					faMList.add(fmOwnList.get(i));
				}
			}
			
			List<FAMaster> fmBrkList = faMasterDAO.getMultiFaM("broker");
			if(!fmBrkList.isEmpty()){
				for(int i=0;i<fmBrkList.size();i++){
					faMList.add(fmBrkList.get(i));
				}
			}
			
			List<FAMaster> fmMisList = faMasterDAO.getMultiFaM("miscfa");
			if(!fmMisList.isEmpty()){
				for(int i=0;i<fmMisList.size();i++){
					faMList.add(fmMisList.get(i));
				}
			}
			
			List<FAMaster> fmPALList = faMasterDAO.getMultiFaM("profitnloss");
			if(!fmPALList.isEmpty()){
				for(int i=0;i<fmPALList.size();i++){
					faMList.add(fmPALList.get(i));
				}
			}
			
			List<FAMaster> fmAALList = faMasterDAO.getMultiFaM("assestsnliab");
			if(!fmAALList.isEmpty()){
				for(int i=0;i<fmAALList.size();i++){
					faMList.add(fmAALList.get(i));
				}
			}
			
			
			if(!faMList.isEmpty()){
				map.put("faMList",faMList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getFaCodeFrCPByName" , method = RequestMethod.POST)  
	public @ResponseBody Object getFaCodeFrCPByName(@RequestBody String faNameCode) {  
		logger.info("Enter into getFaCodeFrCPByName() : faNameCode = "+faNameCode);
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user!=null){			
			List<Map<String,  Object>> faMList = faMasterDAO.getMultiFaMByNameOrCode(faNameCode);			
			if(!faMList.isEmpty()){				
				map.put("faMList",faMList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			logger.info("User not found in httpSession");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		logger.info("Exit from getFaCodeFrCPByName()");
		return map;
	}
	
	
	
	@RequestMapping(value = "/submitCashP" , method = RequestMethod.POST)  
	public @ResponseBody Object submitCashP(@RequestBody VoucherService voucherService) {  
		System.out.println("Enter into submitCashP---->");
		Map<String, Object> map = new HashMap<>();	
		if(voucherService.getVoucherType().equalsIgnoreCase("CashPayment")){
			int vhNo = cashStmtStatusDAO.saveCashPayment(voucherService);
			// int vhNo = (int) map.get("vhNo");
			if(vhNo>0){
				map.put("vhNo",vhNo);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}
		return map;
	}
	
	
	public Map<String,Object> saveCashPayment(VoucherService voucherService){
		System.out.println("Enter into saveCashPayment----->>>");
		User currentUser =	(User)httpSession.getAttribute("currentUser");
		Map<String,Object> map = new HashMap<String,Object>();
		//String tvNo = "0000000000";
		String faCode = voucherService.getFaCode();
		String tdsCode = voucherService.getTdsCode();
		double tdsAmt = voucherService.getTdsAmt();
		List<Map<String,Object>> subFList = voucherService.getSubFList();
		
		if(!subFList.isEmpty()){
			
		}else{
			
			
		}
		
		
		
		
		
		if(tdsCode != null && tdsAmt > 0){
			CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
			CashStmt cashStmt = new CashStmt();
			cashStmt.setbCode(currentUser.getUserBranchCode());
			cashStmt.setUserCode(currentUser.getUserCode());
			cashStmt.setCsDescription(voucherService.getDesc());
			cashStmt.setCsDrCr('D');
			cashStmt.setCsAmt(voucherService.getAmount());
			cashStmt.setCsType(voucherService.getVoucherType());
			//cashStmt.setCsTvNo(tvNo);
			cashStmt.setCsVouchType("cash");
			cashStmt.setCsFaCode(faCode);
			
			java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
			cashStmt.setCsDt(sqlDate);
			
			CashStmt csWithTds = new CashStmt();
			csWithTds.setbCode(currentUser.getUserBranchCode());
			csWithTds.setUserCode(currentUser.getUserCode());
			csWithTds.setCsDescription(voucherService.getDesc());
			csWithTds.setCsDrCr('C');
			csWithTds.setCsType(voucherService.getVoucherType());
			//csWithTds.setCsTvNo(tvNo);
			csWithTds.setCsVouchType("cash");
			csWithTds.setCsAmt(tdsAmt);
			csWithTds.setCsFaCode(tdsCode);
			csWithTds.setCsDt(sqlDate);
			
			map  = cashStmtStatusDAO.updateCSSByCSWTDS(cashStmtStatus.getCssId() , cashStmt , csWithTds);
			/*if(vhNo > 0){
				map.put("vhNo",vhNo);
				map.put("tvNo",tvNo);	
				return map;
			}else{
				map.put("vhNo",vhNo);
				map.put("tvNo","0");
			}*/
			return map;
		}else{
			CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
			CashStmt cashStmt = new CashStmt();
			cashStmt.setbCode(currentUser.getUserBranchCode());
			cashStmt.setUserCode(currentUser.getUserCode());
			cashStmt.setCsDescription(voucherService.getDesc());
			cashStmt.setCsDrCr('D');
			cashStmt.setCsAmt(voucherService.getAmount());
			cashStmt.setCsType(voucherService.getVoucherType());
			//cashStmt.setCsTvNo(tvNo);
			cashStmt.setCsVouchType("cash");
			cashStmt.setCsFaCode(faCode);
			
			java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
			cashStmt.setCsDt(sqlDate);
			
			map = cashStmtStatusDAO.updateCSSByCS(cashStmtStatus.getCssId() , cashStmt);
			/*if(vhNo > 0){
				map.put("vhNo",vhNo);
				map.put("tvNo",tvNo);
				return map;
			}else{
				map.put("vhNo",vhNo);
				map.put("tvNo","0");
			}*/
			return map;
		}
	}
	
	
	
	@RequestMapping(value = "/getTdsCode" , method = RequestMethod.POST)  
	public @ResponseBody Object getTdsCode() {  
		System.out.println("Enter into getTdsCode---->");
		Map<String, Object> map = new HashMap<>();	
		List<FAMaster> faMList = faMasterDAO.getAllTdsFAM();
		System.out.println("size of faMList = "+faMList.size());
		if(!faMList.isEmpty()){
			map.put("list", faMList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	//TODO Back Data
	@RequestMapping(value = "/getCashPayDetailsBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getCashPayDetailsBack(){
		logger.info("Enter into /getCashPayDetailsBack()");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();			
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				logger.info("User Branch Found : Branch Code = "+branchCode);
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(branchCode);
				if(cashStmtStatus != null){
					logger.info("CashStmtStatus found : CashStmtStatus ID = "+cashStmtStatus.getCssId());
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					logger.info("CashStmtStatus not found !");
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				logger.info("User Branch not found !");
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			logger.info("User is not found in httpSession !");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
}

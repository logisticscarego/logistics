package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.model.Customer;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class DisplayCustomerCntlr {


	@Autowired
	private HttpSession httpSession;

	@Autowired
	private CustomerDAO customerDAO;


	@RequestMapping(value="/getCustIsViewNo" ,method = RequestMethod.POST)
	public @ResponseBody Object getCustIsViewNo(){

		System.out.println("Controller called of getCustIsViewNo");
		Map<String,Object> map = new HashMap<String,Object>();

		User currentUser = (User)httpSession.getAttribute("currentUser");

		String branchCode = currentUser.getUserBranchCode();
		List<Customer> custList = customerDAO.getCustmerisViewFalse(branchCode);
		System.out.println("Controller called after retrieving list");

		for(int i=0;i<custList.size();i++){
			System.out.println("The values in the list is"+custList.get(i).getCreationTS());
		}

		if(!custList.isEmpty()){
			map.put("list",custList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}

	@RequestMapping(value = "/getAllCustCode" , method = RequestMethod.GET)  
	public @ResponseBody Object getAllCustCode(){

		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();
		List<String> custCodeList = customerDAO.getAllCustCode(branchCode);

		Map<String,Object> map = new HashMap<String,Object>();
		if (!(custCodeList.isEmpty())) {				
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("custCodeList",custCodeList);
		}else {
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;

	}

	@RequestMapping(value="/updateIsVerifyCustomer",method = RequestMethod.POST)
	public @ResponseBody Object updateIsVerifyCustomer(@RequestBody String selection){

		String custids[]=selection.split(",");
		int temp = customerDAO.updateCustomerisVerify(custids);
		Map<String, Object> map = new HashMap<String, Object>();
		if(temp>0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}
		else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
}

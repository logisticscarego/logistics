package com.mylogistics.controller.dataintegration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.AssestsNLiabDAO;
import com.mylogistics.DAO.CashStmtDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.ContToStnDAO;
import com.mylogistics.DAO.LhpvStatusDAO;
import com.mylogistics.DAO.MiscFaDAO;
import com.mylogistics.DAO.MoneyReceiptDAO;
import com.mylogistics.DAO.ProfitNLossDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.DAO.report.ReportDAO;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class DataIntegrationCntlr {
	
	@Autowired
	private AssestsNLiabDAO assestsNLiabDAO;
	
	@Autowired
	private ProfitNLossDAO profitNLossDAO;
	
	@Autowired
	private MiscFaDAO miscFaDAO; 
	
	@Autowired
	private ContToStnDAO contToStnDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private ReportDAO reportDAO;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private CashStmtDAO cashStmtDAO;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Autowired
	private MoneyReceiptDAO moneyReceiptDAO;
	
	@Autowired
	private LhpvStatusDAO lhpvStatusDAO;
	
	@RequestMapping(value="/saveAssestsNLiab", method=RequestMethod.POST)
	public @ResponseBody Object saveAssestsNLiab(){

		System.out.println("saveAssestsNLiab: ");
		Map<String,Object> map = new HashMap<>();
		/*int temp = assestsNLiabDAO.saveAssestsNLiab();
		if (temp == 1) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}*/
		
		/*int count = cashStmtStatusDAO.delServTaxCS();
		System.out.println("count = "+count);
		if(count > 1){
			map.put("count",count);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}*/
		
		return map;
	}

	@RequestMapping(value="/saveProfitNLoss", method=RequestMethod.POST)
	public @ResponseBody Object saveProfitNLoss(){

		System.out.println("saveProfitNLoss: ");
		Map<String, String> map = new HashMap<>();
		int temp = profitNLossDAO.saveProfitNLoss();
		if (temp == 1) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}

	@RequestMapping(value="/saveMiscFa", method=RequestMethod.POST)
	public @ResponseBody Object saveMiscFa(){

		System.out.println("saveMiscFa: ");
		Map<String, String> map = new HashMap<>();
		int temp = miscFaDAO.saveMiscFa();
		if (temp == 1) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value="/saveAssestsNLiabFaMstr", method=RequestMethod.POST)
	public @ResponseBody Object saveAssestsNLiabFaMstr(){

		System.out.println("saveAssestsNLiabFaMstr: ");
		Map<String,String> map = new HashMap<>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		int temp = assestsNLiabDAO.saveAssestsNLiabFaMstr(currentUser.getUserCode(), currentUser.getUserBranchCode());
		if (temp == 1) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}

	@RequestMapping(value="/saveProfitNLossFaMstr", method=RequestMethod.POST)
	public @ResponseBody Object saveProfitNLossFaMstr(){

		System.out.println("saveProfitNLoss: ");
		Map<String, String> map = new HashMap<>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		int temp = profitNLossDAO.saveProfitNLossMstr(currentUser.getUserCode(), currentUser.getUserBranchCode());
		if (temp == 1) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}

	@RequestMapping(value="/saveMiscFaMstr", method=RequestMethod.POST)
	public @ResponseBody Object saveMiscFaMstr(){

		System.out.println("saveMiscFa: ");
		Map<String, String> map = new HashMap<>();
		/*User currentUser = (User)httpSession.getAttribute("currentUser");
		int temp = miscFaDAO.saveMiscFaMstr(currentUser.getUserCode(), currentUser.getUserBranchCode());
		if (temp == 1) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}*/
		
		int res = moneyReceiptDAO.chkAccuracy();
		if(res > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	//getUnregisteredStn
	@RequestMapping(value="/getUnregisteredStn", method=RequestMethod.POST)
	public @ResponseBody Object getUnregisteredStn(){

		System.out.println("getUnregisteredStn: ");
		Map<String, Object> map = new HashMap<>();
		List<String> getUnregStnList = contToStnDAO.getUnregStnList();
		
		if (!getUnregStnList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("unregisteredStnList", getUnregStnList);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}

	//getCnmtView
	@RequestMapping(value="/getCnmtView", method=RequestMethod.POST)
	public @ResponseBody Object getCnmtView(){
		System.out.println("getCnmtView() called");
		Map<String,Object> map = new HashMap<String, Object>();
		
		int res = assestsNLiabDAO.getTest();
		
		/*int res = moneyReceiptDAO.setPdmrToOnacc();
		if(res > 0){
			int res1 = moneyReceiptDAO.setOnAccRemAmt();
			if(res1 > 0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}*/
		
		//lhpv issue
		//map = lhpvStatusDAO.chkBackDt();
		
		return map;
		
		//reportDAO.test();
		
	}
	
	/*@RequestMapping(value="/assignChildToBank", method=RequestMethod.POST)
	public @ResponseBody Object assignChildToBank(){
		Map<String, Object> map = new HashMap<>();
		
		int temp = bankMstrDAO.assignChildToBank();
		
		if (temp>0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}*/
	
	@RequestMapping(value="/submitBankCSDemo", method=RequestMethod.POST)
	public @ResponseBody Object submitBankCSDemo(@RequestBody Map<String,Object> clientMap){
		System.out.println("enter into submitBankCSDemo function");
		Map<String,Object> map = new HashMap<>();
		System.out.println("clientMap = "+clientMap.entrySet());
		
		int res = reportDAO.saveFirstBCS(clientMap);
		if(res == 1){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	public void writeXLS(HttpServletRequest request , HttpServletResponse response) throws Exception{
		//Create blank workbook
		XSSFWorkbook workbook = new XSSFWorkbook(); 
		//Create a blank sheet
		XSSFSheet spreadsheet = workbook.createSheet(" Employee Info ");
		//Create row object
		XSSFRow row;
		//This data needs to be written (Object[])
		Map < String, Object[] > empinfo = new TreeMap < String, Object[] >();
		empinfo.put( "1", new Object[] { "EMP ID", "EMP NAME", "DESIGNATION" });
		empinfo.put( "2", new Object[] { "tp01", "Gopal", "Technical Manager" });
		empinfo.put( "3", new Object[] { "tp02", "Manisha", "Proof Reader" });
		empinfo.put( "4", new Object[] { "tp03", "Masthan", "Technical Writer" });
		empinfo.put( "5", new Object[] { "tp04", "Satish", "Technical Writer" });
		empinfo.put( "6", new Object[] { "tp05", "Krishna", "Technical Writer" });
		//Iterate over data and write to sheet
		Set < String > keyid = empinfo.keySet();
		int rowid = 0;
		for (String key : keyid) {
			row = spreadsheet.createRow(rowid++);
			Object [] objectArr = empinfo.get(key);
			int cellid = 0;
			for (Object obj : objectArr) {
				Cell cell = row.createCell(cellid++);
		        cell.setCellValue((String)obj);
			}
		}
		response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=MyExcel.xlsx");
		ServletOutputStream out = response.getOutputStream();
		workbook.write(out);
        out.flush();
        out.close();

        
		System.out.println("Writesheet.xlsx written successfully" );
	}
	
	/*@RequestMapping(value="editCsDesc", method=RequestMethod.POST)
	public @ResponseBody Object editCsDesc(){
		System.out.println("DataIntegrationCntlr.editCsDesc()");
		Map<String, Object> map = new HashMap<>();
		
		int temp = cashStmtDAO.editCsDesc();
		
		if (temp > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("temp", temp);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}*/
}

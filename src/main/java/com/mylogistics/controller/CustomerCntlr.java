package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.AddressDAO;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.CustomerOldDAO;
import com.mylogistics.DAO.CustomerRepresentativeDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.FAParticularDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.constants.CustomerCNTS;
import com.mylogistics.model.Address;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Customer;
import com.mylogistics.model.CustomerGroup;
import com.mylogistics.model.CustomerOld;
import com.mylogistics.model.CustomerRepresentative;
import com.mylogistics.model.Employee;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.User;
import com.mylogistics.services.CodePatternService;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.Cust_CustRepService;
import com.mylogistics.services.ModelService;

@Controller
public class CustomerCntlr {

	private String custCode = null;

	@Autowired
	private CustomerDAO customerDAO;

	@Autowired
	private CustomerOldDAO customerOldDAO;

	@Autowired
	private BranchDAO branchDAO;

	@Autowired
	private HttpSession httpSession;

	@Autowired
	private EmployeeDAO employeeDAO;

	@Autowired
	private AddressDAO addressDAO;

	@Autowired
	private CustomerRepresentativeDAO customerRepresentativeDAO;
	
	@Autowired
	private FAParticularDAO faParticularDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;

	private ModelService modelService = new ModelService();
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public static Logger logger = Logger.getLogger(CustomerCntlr.class);

	@RequestMapping(value="/getCustomerCode",method = RequestMethod.POST)
	public @ResponseBody Object getCustomerCode(){
		return custCode;
	}



	@RequestMapping(value="/submitCustAndCustRep",method = RequestMethod.POST)
	public @ResponseBody Object submitCustAndCustRep(@RequestBody Cust_CustRepService cust_CustRepService){
		System.out.println("enter into submitCustAndCustRep function--->"+cust_CustRepService.getCustomer().getStateGST());

		Customer customer = cust_CustRepService.getCustomer();
		customer.setView(false);
		User currentUser = (User)httpSession.getAttribute("currentUser");
		customer.setUserCode(currentUser.getUserCode());
		customer.setbCode(currentUser.getUserBranchCode());

		String s=customer.getCustName();
		String custname=s.substring(0,3);
		String b=customer.getBranchCode();
		String faCode=null;
		
		List<Branch> bRList = branchDAO.retrieveBranch(b);
		String brTempCode = bRList.get(0).getBranchCodeTemp();
		
		System.out.println("brTempCode=======>"+brTempCode);
		String branchname = brTempCode.substring(4,6);
		String lastFiveChar;

		Random rnd = new Random();
		int n = 1000 + rnd.nextInt(8900);
		System.out.println("rand: "+n);

		Map<String,String> map = new HashMap<String,String>();
		long totalRows =customerDAO.totalCount();

		if (!(totalRows == -1)) {

			if(totalRows==0){
				lastFiveChar="00001";
				custCode="1";
			}else{
				int customerid = customerDAO.getLastCustomer() + 1;
				int end = 100000+customerid; 
				lastFiveChar = String.valueOf(end).substring(1,6);
				custCode =String.valueOf(customerid); 
			}

			String custCodeTemp = CodePatternService.custCodeGen(customer,lastFiveChar,brTempCode);

			/*custCode= custname+branchname+last;*/
			customer.setCustCodeTemp(custCodeTemp);
			customer.setCustCode(custCode);
			System.out.println(custCode);
			
			List<FAParticular> faParticulars = faParticularDAO.getFAParticular(ConstantsValues.FAP_CUST);
			FAParticular fAParticular = new FAParticular();
			fAParticular=faParticulars.get(0);
			int fapId=fAParticular.getFaPerId();
			String fapIdStr = String.valueOf(fapId); 
			System.out.println("the fap id is--------------------"+fapId+""+fapIdStr);
			if(fapId<10){
				fapIdStr="0"+fapIdStr;
				System.out.println("After adding zeroes--"+fapIdStr);
			}
			
			//customer.setfAParticular(fAParticular);
			
			
			List<Branch> brList = branchDAO.retrieveBranch(customer.getBranchCode());
			if(!brList.isEmpty()){
				/*Branch branch = brList.get(0);
				branch.getCustomers().add(customer);
				int brResult = branchDAO.updateBranchToDB(branch);*/

				//if(brResult > 0){
				Branch branch = brList.get(0);
				//customer.getBranches().add(brList.get(0));
				int temp = customerDAO.saveCustomerToDB(customer,branch);
				
				if (temp>=0) {
					int custID = temp;
					System.out.println("Owner saved with id------------------"+custID);
					custID=custID+100000;
					String custIdStr = String.valueOf(custID).substring(1,6);
					faCode=fapIdStr+custIdStr;
					System.out.println("faCode code is------------------"+faCode);
					customer.setCustFaCode(faCode);
					int updateCustomer= customerDAO.updateCustomerById(customer);
					System.out.println("After updating customer----------------"+updateCustomer);
					
					FAMaster famaster = new FAMaster();
					famaster.setFaMfaCode(faCode);
					famaster.setFaMfaType("customer");
					famaster.setFaMfaName(customer.getCustName());
					famaster.setbCode(currentUser.getUserBranchCode());
					famaster.setUserCode(currentUser.getUserCode());
					System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
					int saveFAMaster= faMasterDAO.saveFaMaster(famaster);
					
					Address address = new Address();
					address.setAddCity(customer.getCustCity());
					address.setAddPin(customer.getCustPin());
					address.setAddState(customer.getCustState());
					address.setCompleteAdd(customer.getCustAdd());
					address.setAddType("Current Address");
					address.setAddRefCode(customer.getCustCode());
					address.setbCode(customer.getbCode());
					address.setUserCode(customer.getUserCode());

					int results = addressDAO.saveAddress(address);
					if(results > 0){
						map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
						CustomerRepresentative custrep = cust_CustRepService.getCustomerRepresentative();
						System.out.println("custRep ==================>>>>"+custrep);
						if(custrep != null){
							custrep.setCustCode(custCode);
							Map<String,Object> custRepMap = (Map<String, Object>) saveCustomerRepresentative(custrep);
							String result = (String) custRepMap.get(ConstantsValues.RESULT);
							if(result.equals("success")){
								String cRepFaCode = (String) custRepMap.get("faCode");
								map.put("custFaCode",faCode);
								map.put("custRepFaCode",cRepFaCode);
								map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
							}else{
								map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
							}	
						}else{
							map.put("custFaCode",faCode);
							map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
						}
						
					}
					else{
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
				}else {
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}	
				//}	
			}
		}
		else {
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;    
	}


	@RequestMapping(value="/updateCustomer",method = RequestMethod.POST)
	public @ResponseBody Object updateCustomer(@RequestBody Customer customer){
		System.out.println("enter into updateCustomer function--->");
		Map<String,String> map = new HashMap<String,String>();
		List<Customer> custList = customerDAO.getCustomer(customer.getCustCode());
		if(!custList.isEmpty()){
			Customer cust = custList.get(0);
			CustomerOld customerOld = new CustomerOld();

			customerOld.setbCode(cust.getbCode());
			customerOld.setBranchCode(cust.getBranchCode());
			customerOld.setCustAdd(cust.getCustAdd());
			customerOld.setCustBillCycle(cust.getCustBillCycle());
			customerOld.setCustBillValue(cust.getCustBillValue());
			customerOld.setCustBrPerson(cust.getCustBrPerson());
			customerOld.setCustCity(cust.getCustCity());
			customerOld.setCustCode(cust.getCustCode());
			customerOld.setCustCorpExc(cust.getCustCorpExc());
			customerOld.setCustCrBase(cust.getCustCrBase());
			customerOld.setCustCrPeriod(cust.getCustCrPeriod());
			customerOld.setCustDirector(cust.getCustDirector());
			customerOld.setCustId(cust.getCustId());
			customerOld.setCustIsDailyContAllow(cust.getCustIsDailyContAllow());
			customerOld.setCustMktHead(cust.getCustMktHead());
			customerOld.setCustName(cust.getCustName());
			customerOld.setCustPanNo(cust.getCustPanNo());
			customerOld.setCustPin(cust.getCustPin());
			customerOld.setCustSrvTaxBy(cust.getCustSrvTaxBy());
			customerOld.setCustState(cust.getCustState());
			customerOld.setCustStatus(cust.getCustStatus());
			customerOld.setCustTdsCircle(cust.getCustTdsCircle());
			customerOld.setCustTdsCity(cust.getCustTdsCity());
			customerOld.setCustTinNo(cust.getCustTinNo());
			customerOld.setIsVerify(cust.getIsVerify());
			customerOld.setUserCode(cust.getUserCode());
			customerOld.setView(cust.isView());

			int result = customerOldDAO.saveCustOld(customerOld);

			if(result > 0){
				int temp = customerDAO.updateCustomer(customer);
				if(temp > 0){
					FAMaster famaster = new FAMaster();
					List<FAMaster>	famasterlist = faMasterDAO.retrieveFAMaster(customer.getCustFaCode());
					if (!famasterlist.isEmpty()){
						famaster = famasterlist.get(0);
						famaster.setFaMfaName(customer.getCustName());
						int updateFAMaster= faMasterDAO.updateFaMaster(famaster);
					}else {
						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					}
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}


	public Object saveCustomerRepresentative(CustomerRepresentative custrep){
		System.out.println("enter into saveCustomerRepresentative function");	
		custrep.setView(false);
		User currentUser = (User)httpSession.getAttribute("currentUser");
		custrep.setUserCode(currentUser.getUserCode());
		custrep.setbCode(currentUser.getUserBranchCode());

		String custcode="";
		/*String s=custrep.getCrName();
		String custname=s.substring(0,3);*/
		String lastFiveChar;
		String custRepCode=null;
		String faCode=null;

		/*Random rnd = new Random();
        int n = 1000 + rnd.nextInt(8900);*/
		Map<String,String> map = new HashMap<String,String>();

		long totalRows = customerRepresentativeDAO.totalCount();

		if (!(totalRows == -1)) {

			if(totalRows==0)
			{
				lastFiveChar="00001";
				custRepCode="1";
			}else{
				int customerid = customerRepresentativeDAO.getLastCustomerRep()+1;
				int end = 100000+customerid;
				lastFiveChar = String.valueOf(end).substring(1,6);
				custRepCode=String.valueOf(customerid);
			}

			String custRepCodeTemp = CodePatternService.custRepCodeGen(custrep, lastFiveChar);

			/*custcode= custname+n+lastFiveChar;*/
			custrep.setCrCode(custRepCode);
			custrep.setCrCodeTemp(custRepCodeTemp);
			String custCode=custrep.getCustCode();
			/*String custid=custCode.substring(5,10);*/
			int custidint=Integer.parseInt(custCode);
			custrep.setCustId(custidint);
			

			List<FAParticular> faParticulars = faParticularDAO.getFAParticular(ConstantsValues.FAP_CUST_REP);
			FAParticular fAParticular = new FAParticular();
			fAParticular=faParticulars.get(0);
			int fapId=fAParticular.getFaPerId();
			String fapIdStr = String.valueOf(fapId); 
			System.out.println("the fap id is--------------------"+fapId+""+fapIdStr);
			if(fapId<10){
				fapIdStr="0"+fapIdStr;
				System.out.println("After adding zeroes--"+fapIdStr);
			}
			custrep.setfAParticular(fAParticular);

			int temp=customerRepresentativeDAO.saveCustomerRepToDB(custrep);
			
			if (temp>=0) {
				int custRepId = temp;
				System.out.println("Owner saved with id------------------"+custRepId);
				custRepId=custRepId+100000;
				String custIdStr = String.valueOf(custRepId).substring(1,6);
				faCode=fapIdStr+custIdStr;
				System.out.println("faCode code is------------------"+faCode);
				custrep.setCrFaCode(faCode);
				String crCode = String.valueOf(temp);
				custrep.setCrCode(crCode);
				int updateCustomerRep= customerRepresentativeDAO.updateCustomerRepById(custrep);
				System.out.println("After updating owner----------------"+updateCustomerRep);
				
				/*FAMaster famaster = new FAMaster();
				famaster.setFaMfaCode(faCode);
				famaster.setFaMfaType("customerrepresentative");
				famaster.setFaMfaName(custrep.getCrName());
				famaster.setbCode(currentUser.getUserBranchCode());
				famaster.setUserCode(currentUser.getUserCode());
				System.out.println("*******************************");

				int saveFAMaster= faMasterDAO.saveFaMaster(famaster);*/
				
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				map.put("faCode",faCode);
				map.put("targetPage","customer");
			}else {
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}	
		} else {
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}  


	/*	@RequestMapping(value="/customerRepresentative",method = RequestMethod.POST)
	public @ResponseBody Object helloCustomerRepresentative(@RequestBody Customer customer){
		customer.setView(false);
		User currentUser = (User)httpSession.getAttribute("currentUser");
		customer.setUserCode(currentUser.getUserCode());
		customer.setbCode(currentUser.getUserBranchCode());

		String s=customer.getCustName();
		String custname=s.substring(0,3);
		String b=customer.getBranchCode();
		String branchname=b.substring(4,6);
		String lastFiveChar;

		Random rnd = new Random();
		int n = 1000 + rnd.nextInt(8900);
		System.out.println("rand: "+n);

		Map<String,String> map = new HashMap<String,String>();
		long totalRows =customerDAO.totalCount();

		if (!(totalRows == -1)) {

			if(totalRows==0){
				lastFiveChar="00001";
			}else{
				int customerid = customerDAO.getLastCustomer() + 1;
				int end = 100000+customerid; 
				lastFiveChar = String.valueOf(end).substring(1,6);		
			}

			String custCode = CodePatternService.custCodeGen(customer, lastFiveChar);

			custCode= custname+branchname+last;
			customer.setCustCode(custCode);
			System.out.println(custCode);
			int temp = customerDAO.saveCustomerToDB(customer);

			if (temp>=0) {
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				map.put("message", custCode);
				map.put("targetPage","addcustomerrepresentative");
			}else {
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}	
		}
		else {
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;    
	}*/


	@RequestMapping(value = "/submitcustomer", method = RequestMethod.POST)
	public @ResponseBody Object customerDetails(@RequestBody String custCode){

		Customer customer = new Customer();
		List<String> customerRepList = new ArrayList<String>();
		List<Customer> list = customerDAO.getCustomer(custCode);
		List<CustomerRepresentative> custRepList = customerRepresentativeDAO.getCustomerRepByCustCode(custCode);
		Map<String,Object> map = new HashMap<String , Object>();
		if (!list.isEmpty()){
			customer = list.get(0);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("customer",customer);
			if(!custRepList.isEmpty()){
				for(int i=0;i<custRepList.size();i++){
					customerRepList.add(custRepList.get(i).getCrCode());
				}
				map.put("customerRepList",customerRepList);
			}	
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	

	}

	@RequestMapping(value = "/submitcustomerrepresentative", method = RequestMethod.POST)
	public @ResponseBody Object crDetails(@RequestBody String crCode){

		Map<String,Object> map = customerDAO.getCustomerNCustomerRep(crCode);

		if ((Integer)(map.get("temp"))>0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;	
	}    

	@RequestMapping(value = "/editsubmitcustomercode", method = RequestMethod.POST)
	public @ResponseBody Object editcustomerDetails(@RequestBody String custCode){

		Customer customer = new Customer();
		List<Customer> list = customerDAO.getCustomer(custCode);

		Map<String,Object> map = new HashMap<String , Object>();
		if (!list.isEmpty()){
			customer = list.get(0);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("customer",customer);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}


	@RequestMapping(value="/saveeditcustomer", method = RequestMethod.POST)
	public @ResponseBody Object saveeditcontract(@RequestBody Customer customer){
		System.out.println("enter into saveeditcontract function");
		int temp=customerDAO.updateCustomerByCustCode(customer);
		Map<String,Object> map = new HashMap<String , Object>();
		if (temp>=0) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}
		else {
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}	
		return map;
	}


	@RequestMapping(value="/verifyCustomer",method = RequestMethod.POST)
	public @ResponseBody Object verifyRegularContract(){
		List<Customer> customerList = customerDAO.getCustomerisVerifyNo();
		Map<String,Object> map = new HashMap<String,Object>();
		if (!customerList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",customerList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}

	@RequestMapping(value="/getBranchDataForCustomer",method = RequestMethod.POST)
	public @ResponseBody Object getBranchData(){
		System.out.println("enter into getBranchData function");
		List<Branch> branchList = branchDAO.getAllActiveBranches();
		Map<String,Object> map = new HashMap<String,Object>();
		if (!branchList.isEmpty()){
			System.out.println("size of list----->"+branchList.size());
			//System.out.println("customers -----> "+branchList.get(0).getCustomers());
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			System.out.println("#############Rohit1");
			map.put("list",branchList);
			System.out.println("#############Rohit2");
		}else{
			System.out.println("********************ERROR******************");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			System.out.println("#############Rohit3");
		}
		System.out.println("#############Rohit4");
		return map;		
	}

	@RequestMapping(value="/getListOfEmpforCustomer",method = RequestMethod.POST)
	public @ResponseBody Object getListOfEmpforCustomer(){

		Map<String,Object> map = new HashMap<String,Object>();

		List<Employee> employeeList = employeeDAO.getAllActiveEmployees();

		List<Employee> empList = modelService.getEmployeeList();
		if (!(empList.isEmpty())) {
			employeeList.addAll(empList);
		}

		if (!(employeeList.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",employeeList);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	@RequestMapping(value="/getCustList", method = RequestMethod.POST)
	public @ResponseBody Object getCustList(){
		System.out.println("getCustList()");
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> custList = customerDAO.getCustNCI();
		
		if (!custList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("custList", custList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}

	@RequestMapping(value="/getCustListByBranch", method=RequestMethod.POST)
	public @ResponseBody Object getCustListByBranch(@RequestBody Integer branchId){
		System.out.println("CustomerCntlr.getCustListByBranch()");
		System.out.println("custId: "+branchId);
		Map<String, Object> map = new HashMap<>();
		
		List<Map<String, Object>> custList = customerDAO.getCustListByBranch(branchId);
		
		if (!custList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("custList", custList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}
		
		return map;
	}
	
	
	@RequestMapping(value="/getCustomerListByNameFa", method=RequestMethod.POST)
	public @ResponseBody Object getCustomerListByNameFa(@RequestBody String custNameFaCode){
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("UserID = "+user.getUserId()+ " : Enter into /getCustomerListByNaFa()");
		Session session = this.sessionFactory.openSession();
		try{
			List<String> proList = new ArrayList<String>();
			
			proList.add(CustomerCNTS.CUST_FA_CODE);
			proList.add(CustomerCNTS.CUST_CODE);
			proList.add(CustomerCNTS.CUST_NAME);
			proList.add(CustomerCNTS.CUST_IS_DAILY_CONT_ALLOW);
			proList.add(CustomerCNTS.BRANCH_CODE);
			
			List<Customer> customerList = customerDAO.getCustomerByName_FaCode(session, user, custNameFaCode, proList);
			if(customerList.isEmpty()){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "No such customer found !");
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				resultMap.put("customerList", customerList);
			}
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+ " : Exception = "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "No such customer record !");
		}finally{
			session.clear();
			session.close();
		}
		logger.info("Exit from /getCustomerListByNaFa()");
		return resultMap;
	}
	
	@RequestMapping(value="/takeCustomerForGroup", method=RequestMethod.POST)
	public @ResponseBody Object takeCustomerForGroup(@RequestBody CustomerGroup customerGroup){
		User user = (User) httpSession.getAttribute("currentUser");		
		logger.info("UserID = "+user.getUserId()+ " : Enter into /takeCustomerForGroup()");
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		try{
			List<Map<String, Object>> custList = customerDAO.getCustomerByGroup(session, user, customerGroup.getGroupId());
			
			if(custList.isEmpty()){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Customer not found in this group !");
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				resultMap.put("custList", custList);
			}
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+ " : Exception = "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "No such customer record !");
		}finally{
			session.clear();
			session.close();
		}
		logger.info("Exit from /takeCustomerForGroup()");
		return resultMap;
	}
	
	
	
	
	@RequestMapping(value="/updateCustGstNo", method=RequestMethod.POST)
	public @ResponseBody Object updateCustGstNo(@RequestBody Map<String, String> custData){
		User user = (User) httpSession.getAttribute("currentUser");		
		logger.info("UserID = "+user.getUserId()+ " : Enter into /updateCustGstNo()");
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		int result;
		
		
		
		Session session = null;
		session = this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		
		try{
			
			
			
			result = customerDAO.saveUpdateCustGstNo(session, user, custData);
			
			if(result==1){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			
			transaction.commit();
			
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+ " : Exception = "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			transaction.rollback();
			
		}finally{
			session.clear();
			session.close();
		}
		logger.info("Exit from /updateCustGstNo()");
		return resultMap;
	}

	
	
	@RequestMapping(value="/getRelConsignee", method=RequestMethod.POST)
	public @ResponseBody Object getRelConsignee(){
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("UserID = "+user.getUserId()+ " : Enter into /getRelConsignee()");
		Session session = this.sessionFactory.openSession();
		try{
			List<String> proList = new ArrayList<String>();
			
			proList.add(CustomerCNTS.CUST_FA_CODE);
			proList.add(CustomerCNTS.CUST_CODE);
			proList.add(CustomerCNTS.CUST_NAME);
			proList.add(CustomerCNTS.CUST_IS_DAILY_CONT_ALLOW);
			proList.add(CustomerCNTS.BRANCH_CODE);
			
			List<Customer> customerList = customerDAO.getRelConsignee(session, user,proList);
			if(customerList.isEmpty()){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "No such customer found !");
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				resultMap.put("customerList", customerList);
			}
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+ " : Exception = "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "No such customer record !");
		}finally{
			session.clear();
			session.close();
		}
		logger.info("Exit from /getCustomerListByNaFa()");
		return resultMap;
	}
	
	
}
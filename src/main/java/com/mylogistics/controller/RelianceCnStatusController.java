package com.mylogistics.controller;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.apache.poi.ss.format.CellFormatType;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.CnmtDAO;
import com.mylogistics.DAO.report.ReportDAO;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.Challan;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.Cnmt_Challan;
import com.mylogistics.model.RelianceCNStatus;
import com.mylogistics.model.Station;
import com.mylogistics.services.ConstantsValues;

@Controller
public class RelianceCnStatusController {
	
//	@Autowired
//	CnmtDAO cnmtDao;
	
	@Autowired
	ReportDAO reportDAO;
	
	Map<String, Object> resultMap=new HashMap<>();
	
	@RequestMapping(value="/getCnmtCodeByCodeN",method=RequestMethod.POST)
	public @ResponseBody Object getCnmtCOde(@RequestBody String cnmtCode)
	{
		System.out.println("I am in Reliance CN Status Controller");
		System.out.println("CNmt Code is "+cnmtCode);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			List<Cnmt> cnmtCodeList = reportDAO.getCnmtCodes(cnmtCode); 
			if(cnmtCodeList != null && !cnmtCodeList.isEmpty()){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				resultMap.put("cnmtCodeList", cnmtCodeList);
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "No such cnmt found !");
			}
		}catch(Exception e){
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "No such cnmt found !");

		}

		return resultMap;
		
	}
	
	@RequestMapping(value="/getStationByCode",method=RequestMethod.POST)
	public @ResponseBody Object getStation(@RequestBody Map<String, String> map)
	{
		System.out.println("I am in Reliance CN Status Controller");
	System.out.println("CNmt TO Station Code is "+map.get("toStation"));
	System.out.println("Cnmt From Station Code is "+map.get("fromStation"));
		
	Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			List<Station> stationList = reportDAO.getStation();
			   Map<String, String> map2=new HashMap<>();
			    for (Station station : stationList) {
					map2 .put(station.getStnCode(), station.getStnName());
				}
			  String toStationName=  map2.get(map.get("toStation"));
			  String fromStationName=  map2.get(map.get("fromStation"));
			   System.out.println("to Station Name "+toStationName); 
			   System.out.println("From Station Name  "+fromStationName);
				
			if(stationList != null && !stationList.isEmpty()){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				//resultMap.put("cnmtCodeList", stationList);
				resultMap.put("toStName", toStationName);
				resultMap.put("frStName", fromStationName);
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "No such cnmt found !");
			}
		}catch(Exception e){
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "No such cnmt found !");

		}

		return resultMap;
		//return null;
		
	}
	@RequestMapping(value="/getChallan",method=RequestMethod.POST)
	public @ResponseBody Object getChallan(@RequestBody String cnmtCode)
	{
		
		System.out.println("i am inside get challan Data");
		System.out.println("Cnmt COde is "+cnmtCode);
		Map<String, Object> resultMap=new HashMap<>();
	List<Integer> integers=	reportDAO.getChallanData(cnmtCode);
	List<Integer> cnmt_challans_Id=reportDAO.getChallanId(integers);
	List<Challan> challans=reportDAO.getChallanData(cnmt_challans_Id);
	System.out.println(cnmt_challans_Id);
	System.out.println("CHallan Id is "+challans.get(0).getChlnId());
	if(challans != null &&!challans.isEmpty()){
		resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		//resultMap.put("cnmtCodeList", stationList);
		resultMap.put("challanList", challans);
		
	}else{
		resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		resultMap.put("msg", "No such Challans Found !");
	}

		return resultMap;
	}
	
	@RequestMapping(value="/getArrival",method=RequestMethod.POST)
	public @ResponseBody Object getArrival(@RequestBody String challanno)
	{
		System.out.println(" I am in Arrival Report "+challanno);
		Map<String,Object> resultMap=new HashMap<>();
		List<ArrivalReport> arrivalReports=reportDAO.getArrivalReoprtData(challanno);
		if(arrivalReports != null &&!arrivalReports.isEmpty()){
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			//resultMap.put("cnmtCodeList", stationList);
			resultMap.put("arrivalReportList", arrivalReports);
			
		}else{
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "No such Arrival Report Found !");
		}
		
		return resultMap;
	}
	
	@RequestMapping(value="/saveRelianceCNStatus",method=RequestMethod.POST)
	public @ResponseBody Object saveRelianeCnStatus(@RequestBody RelianceCNStatus cnStatus)
	{
		reportDAO.saveRelianceCNStatus(cnStatus);
		return cnStatus;
		
	}
	
	@RequestMapping(value="/fetchRelianceCNStatus",method=RequestMethod.POST)
	public @ResponseBody Object fetchRelianeCnStatus(@RequestBody Map<String, Object> map)
	{
		//Map<String, Object> resultMap=new HashMap<>();
		Date fromdate=Date.valueOf(map.get("fromIndent").toString());
		Date todate=Date.valueOf(map.get("toIndent").toString());
		System.out.println("From Date "+fromdate);
		System.out.println("TO Date "+todate);
	List<RelianceCNStatus> cnStatus=	reportDAO.fetchCNStatus(fromdate,todate);
	if(cnStatus!=null && !cnStatus.isEmpty())
	{
		resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		
		resultMap.put("cnStatusList", cnStatus);
	}
	else{
		resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		resultMap.put("msg", "No such Arrival Report Found !");
	}
		return resultMap;	
	}
	
	@RequestMapping(value="/getDataFromRelianceCNStatus",method=RequestMethod.POST)
	public @ResponseBody Object getDataFromRelianceCNStatus(@RequestBody String cnmtCode)
	{
		Map<String,Object> map=new HashMap<>();
		System.out.println("CNmt COde in getting data from Reliance CN Status  "+cnmtCode);
	List<RelianceCNStatus> cnStatus=	reportDAO.getDataFromRelianceCNStatus(cnmtCode);
		System.out.println(cnStatus.size());
		if(cnStatus.size()>0)
		{
		map.put("cnList", cnStatus.get(0));
		
		}
		else
		{
			map.put("cnList", 0);
		}
		return map;
	}
	
	
	@RequestMapping(value="/getRelCNStatusXLSX",method=RequestMethod.POST)
	public void printRelianeCnStatus(HttpServletRequest request,HttpServletResponse response) 
	{
		List<RelianceCNStatus> relianceCNStatusData=(List<RelianceCNStatus>) resultMap.get("cnStatusList");
		System.out.println("In Print "+relianceCNStatusData.size());
		// creating the workbook to create a excel sheet
		for (RelianceCNStatus relianceCNStatus : relianceCNStatusData) {
			System.out.println(relianceCNStatus.getIndentDate());
		}
		XSSFWorkbook workbook=new XSSFWorkbook();
	
		
		// creating a excel sheet 
	    XSSFSheet sheet=workbook.createSheet("Reliance Outstanding Format");
	 
	    // creating the first row in the sheet
	    XSSFRow row=sheet.createRow(0);
	    
	    
	    XSSFFont font=workbook.createFont();
	    font.setBold(true);
	    
	    // getting the cell style instance from the workbook
	    XSSFCellStyle cellStyle= workbook.createCellStyle();
	    cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	    cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
	    cellStyle .setFont(font);
	    XSSFCreationHelper creationHelper = workbook.getCreationHelper();
		CellStyle style1 = workbook.createCellStyle();
		style1.setDataFormat(creationHelper.createDataFormat().getFormat(
				"dd-MMM-yyyy"));
	    
	    XSSFCellStyle cellStyle2= workbook.createCellStyle();
	    cellStyle2.setAlignment(CellStyle.ALIGN_CENTER);
	    cellStyle2.setWrapText(true);
	    cellStyle2 .setFont(font);
	    
	    XSSFCell cell=null;
	    SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MMM-yy");
	    
	    
/*     

							 
  cell=  row.createCell(12);
  cell.setCellValue("Reason for"+"\n"+"Deviations");*/
	    
	    // creating the cell from  the specified row

	/*  cell=  row.createCell(0);
	  cell.setCellStyle(cellStyle);
	  cell.setCellValue("Sr. No.");*/
	  
	  cell=  row.createCell(0);
	  cell.setCellStyle(cellStyle);
	  cell.setCellValue("LR No"+"\n"+"Order #");
	  
	  cell=  row.createCell(1);
	  cell.setCellStyle(cellStyle);
	  cell.setCellValue("Indent Date");
	  
	  cell=  row.createCell(2);
	  cell.setCellStyle(cellStyle);
	  cell.setCellValue("Placement"+"\n"+"Date");
	  
	  cell=  row.createCell(3);
	  cell.setCellStyle(cellStyle);
	  cell.setCellValue("Trip start"+"\n"+"Date");
	  
	  cell=  row.createCell(4);
	  cell.setCellStyle(cellStyle);
	  cell.setCellValue("Pick up"+"\n"+"Warehouse"+"\n"+"Location");
	  
	  cell=  row.createCell(5);
	  cell.setCellStyle(cellStyle);
	  cell.setCellValue("Delivery"+"\n"+"Warehouse"+"\n"+"Destination");
	  
	  cell=  row.createCell(6);
	  cell.setCellStyle(cellStyle);
	  cell.setCellValue("Truck"+"\n"+"Number");
	  
	  cell=  row.createCell(7);
	  cell.setCellStyle(cellStyle);
	  cell.setCellValue("Truck Type");
	  
	  cell=  row.createCell(8);
	  cell.setCellStyle(cellStyle);
	  cell.setCellValue("Current"+"\n"+"Status");
	  
	  cell=  row.createCell(9);
	  cell.setCellStyle(cellStyle2);
	  cell.setCellValue("ETA(Average"+"\n"+"running 350"+"\n"+"kms/day");
	  
	  cell=  row.createCell(10);
	  cell.setCellStyle(cellStyle);
	  cell.setCellValue("Actual"+"\n"+"Vehicle"+"\n"+"Reporting"+"\n"+"Date");
	  
	  cell=  row.createCell(11);
	  cell.setCellStyle(cellStyle);
	  cell.setCellValue("Actual"+"\n"+"Unloading"+"Date");
	  
	  cell=  row.createCell(12);
	  cell.setCellStyle(cellStyle);
	  cell.setCellValue("Reason for"+"\n"+"Deviations");
	  
     
  int createRow=1;
  XSSFRow xssfRow;
		for (RelianceCNStatus relianceCNStatus : relianceCNStatusData) {
		 xssfRow=	sheet.createRow(createRow);
	cell=	xssfRow.createCell(0);
	cell.setCellValue(relianceCNStatus.getCnmtCode());
	if(relianceCNStatus.getIndentDate()==null)
	{
		cell=	xssfRow.createCell(1);
		cell.setCellValue("");	
	}
	else
	{
		Date date=relianceCNStatus.getIndentDate();
	//String indentDate=	dateFormat.format(date);
		//Date date2=Date.valueOf(indentDate);
	cell=	xssfRow.createCell(1);
	cell.setCellStyle(style1);
	cell.setCellValue(date);
	}
		
	if(relianceCNStatus.getPlacementDate()==null)
	{
		cell=	xssfRow.createCell(2);
		cell.setCellValue("");	
	}
	else
	{
		Date date=relianceCNStatus.getPlacementDate();
	//String placemetDate=	dateFormat.format(date);
	cell=	xssfRow.createCell(2);
	cell.setCellStyle(style1);
	cell.setCellValue(date);
	}
	if(relianceCNStatus.getTripStartDate()==null)
	{
		cell=	xssfRow.createCell(3);
		cell.setCellValue("");	
	}
	else
	{
		Date  date=relianceCNStatus.getTripStartDate();
		//String tripDate=dateFormat.format(date);
	cell=	xssfRow.createCell(3);
	cell.setCellStyle(style1);
	cell.setCellValue(date);
	}
	
	cell=	xssfRow.createCell(4);
	cell.setCellValue(relianceCNStatus.getFromStation());
			
	cell=	xssfRow.createCell(5);
	cell.setCellValue(relianceCNStatus.getToStation());
	
	cell=	xssfRow.createCell(6);
	cell.setCellValue(relianceCNStatus.getLorryNo());
	
	cell=	xssfRow.createCell(7);
	cell.setCellValue(relianceCNStatus.getCarryType());
	
	cell=	xssfRow.createCell(8);
	cell.setCellValue(relianceCNStatus.getCurrentStatus());
	if(relianceCNStatus.getTripStartDate()==null)
	{
	cell=	xssfRow.createCell(9);
	cell.setCellValue("");
	}
	else
	{
		Date date=relianceCNStatus.getTripStartDate();
		Calendar calendar=Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, relianceCNStatus.getTrasittime());
		cell=	xssfRow.createCell(9);
		//String etaDate=dateFormat.format(calendar.getTime());
		cell.setCellStyle(style1);
		cell.setCellValue(calendar.getTime());	
	}
	
	if(relianceCNStatus.getReportingDate()==null)
	{
		cell=	xssfRow.createCell(10);
		cell.setCellValue("");	
	}
	else
	{
		Date date=relianceCNStatus.getReportingDate();
	//String rptDate=dateFormat.format(date);
	cell=	xssfRow.createCell(10);
	cell.setCellStyle(style1);
	cell.setCellValue(date);
	}
	
	if(relianceCNStatus.getUnloadingDate()==null)
	{
	cell=	xssfRow.createCell(11);
		cell.setCellValue("");	
	}
	else{
	Date date=	relianceCNStatus.getUnloadingDate();
	//String unloadingDate=dateFormat.format(date);
	cell=	xssfRow.createCell(11);
	cell.setCellStyle(style1);
	cell.setCellValue(date);
	}
	
	cell=	xssfRow.createCell(12);
	cell.setCellValue(relianceCNStatus.getDeviation());
	createRow++;
	}
		
		// Defining the size to a cell automatically
		 for (int i = 0; i < 13; i++) {
					sheet.autoSizeColumn(i);
				}
		 	response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=relCNStatus.xlsx");
			ServletOutputStream out;
			try {
				out = response.getOutputStream();
				workbook.write(out);
				out.flush();
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
}

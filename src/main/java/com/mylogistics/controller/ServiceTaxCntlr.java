package com.mylogistics.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.ServiceTaxDAO;
import com.mylogistics.model.ServiceTax;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class ServiceTaxCntlr {
	
	@Autowired
	private ServiceTaxDAO serviceTaxDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@RequestMapping(value = "/saveServiceTax", method = RequestMethod.POST)
    public @ResponseBody Object saveServiceTax(@RequestBody ServiceTax servicetax){
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		servicetax.setUserCode(currentUser.getUserCode());
		servicetax.setbCode(currentUser.getUserBranchCode());
		        
        Map<String,String> map = new HashMap<String,String>();
        int temp=serviceTaxDAO.saveServiceTaxToDB(servicetax);
        if (temp>=0) {
        	 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map; 	
	}

}

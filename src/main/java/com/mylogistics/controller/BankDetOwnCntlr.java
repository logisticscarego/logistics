package com.mylogistics.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mylogistics.DAO.BrokerDAO;
import com.mylogistics.DAO.OwnerDAO;
import com.mylogistics.model.Broker;
import com.mylogistics.model.BrokerImg;
import com.mylogistics.model.Owner;
import com.mylogistics.model.OwnerImg;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class BankDetOwnCntlr {
	
	
	@Autowired
	private OwnerDAO ownerDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Blob blob;

	private String ownerBankDetImgPath = "/var/www/html/Erp_Image/Owner/BankDetail";


	@RequestMapping(value = "/getOwnCodeByIsBnk", method = RequestMethod.POST)
	public @ResponseBody Object getOwnCodeByIsBnk(@RequestBody String ownCode) {
		System.out.println("Enter into getOwnCodeByIsBnk--->>>>>");
		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String, String>> ownerCodeList =  new ArrayList<>();

		ownerCodeList = ownerDAO.getOwnCodesIsBnkD(ownCode);
		if(!ownerCodeList.isEmpty()){

			map.put("ownerCodeList",ownerCodeList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);

		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}

	@RequestMapping(value = "/updateOwnBnkD", method = RequestMethod.POST)
	public @ResponseBody Object updateOwnBnkD(@RequestBody Owner ownerBnk) {
		System.out.println("Enter into updateOwnBnkD--->>>>>" +ownerBnk);
		Map<String, Object> map = new HashMap<String, Object>();
		List<Owner> ownerList = ownerDAO.getOwnerList(ownerBnk.getOwnCode());
		User user=(User) httpSession.getAttribute("currentUser");
		System.out.println("user.getUserBranchCode()="+user.getUserBranchCode());
		
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		
		try{
		
		
		if(!ownerList.isEmpty()){
			Owner owner = ownerList.get(0);  
			
			
			
			if(blob!=null) {
				
				Path path = Paths.get(ownerBankDetImgPath);
				if(! Files.exists(path))
					Files.createDirectories(path);//make new directory if not exist
				
				
				if(owner.getOwnImgId()==null) {
					OwnerImg ownImg=new OwnerImg();
					ownImg.setbCode(user.getUserBranchCode());
					ownImg.setOwnId(owner.getOwnId());
					ownImg.setUserCode(user.getUserCode());
				
					ownImg.setOwnBnkDetImgPath(ownerBankDetImgPath+"/"+"own"+owner.getOwnId()+".pdf");
					byte ownerBnkImgInBytes[] = blob.getBytes(1, (int)blob.length());
					File bnkFile = new File(ownImg.getOwnBnkDetImgPath());
					if(bnkFile.exists())
						bnkFile.delete();
					if(bnkFile.createNewFile()){
						OutputStream targetFile=  new FileOutputStream(bnkFile);
			            targetFile.write(ownerBnkImgInBytes);
			            targetFile.close();
					}
				
				int ownImgId=ownerDAO.saveOwnImg(session, ownImg);
				System.out.println("ownImgId="+ownImgId);
				owner.setOwnIsChqImg(true);
				owner.setOwnImgId(ownImgId);
					
				}else if(owner.getOwnImgId()<=0) {
					OwnerImg ownImg=new OwnerImg();
					ownImg.setbCode(user.getUserBranchCode());
					ownImg.setOwnId(owner.getOwnId());
					ownImg.setUserCode(user.getUserCode());
				
					ownImg.setOwnBnkDetImgPath(ownerBankDetImgPath+"/"+"own"+owner.getOwnId()+".pdf");
					byte ownerBnkImgInBytes[] = blob.getBytes(1, (int)blob.length());
					File bnkFile = new File(ownImg.getOwnBnkDetImgPath());
					if(bnkFile.exists())
						bnkFile.delete();
					if(bnkFile.createNewFile()){
						OutputStream targetFile=  new FileOutputStream(bnkFile);
			            targetFile.write(ownerBnkImgInBytes);
			            targetFile.close();
					}
					int ownImgId=ownerDAO.saveOwnImg(session, ownImg);
					System.out.println("ownImgId="+ownImgId);
					owner.setOwnImgId(ownImgId);
					owner.setOwnIsChqImg(true);
				}else {
					OwnerImg ownImg=(OwnerImg) session.get(OwnerImg.class, owner.getOwnImgId());
					ownImg.setbCode(user.getUserBranchCode());
					ownImg.setOwnId(owner.getOwnId());
					ownImg.setUserCode(user.getUserCode());
				
					ownImg.setOwnBnkDetImgPath(ownerBankDetImgPath+"/"+"own"+owner.getOwnId()+".pdf");
					byte ownerBnkImgInBytes[] = blob.getBytes(1, (int)blob.length());
					File bnkFile = new File(ownImg.getOwnBnkDetImgPath());
					if(bnkFile.exists())
						bnkFile.delete();
					if(bnkFile.createNewFile()){
						OutputStream targetFile=  new FileOutputStream(bnkFile);
			            targetFile.write(ownerBnkImgInBytes);
			            targetFile.close();
					}
					
					ownerDAO.updateOwnImg(session,ownImg);
					owner.setOwnIsChqImg(true);
				}
				
			
		}
			
			
			owner.setOwnAccntNo(ownerBnk.getOwnAccntNo());
			owner.setOwnIfsc(ownerBnk.getOwnIfsc());
			owner.setOwnMicr(ownerBnk.getOwnMicr());
			owner.setOwnBnkBranch(ownerBnk.getOwnBnkBranch());
			owner.setOwnAcntHldrName(ownerBnk.getOwnAcntHldrName());
			owner.setOwnBnkDet(true);
			
			
			
			int temp = ownerDAO.updateOwner(owner);
			if(temp > 0){
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		session.flush();
		session.clear();
		transaction.commit();
		
		}catch(Exception e){
			System.out.println("Exception : "+e);
			transaction.rollback();
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}finally {
			blob=null;
			session.close();
		}


		return map;
	}

	
	@RequestMapping(value = "/upldOwnChqImgBnkN", method = RequestMethod.POST)
	public @ResponseBody Object upldOwnChqImgN(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into upldOwnDecImg funciton");
		byte [] byteArr = file.getBytes();
		Map<String,Object> map = new HashMap<String, Object>();
		blob = null;
		try{
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	

}

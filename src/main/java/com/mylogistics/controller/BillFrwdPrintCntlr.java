package com.mylogistics.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BillForwardingDAO;
import com.mylogistics.services.ConstantsValues;

@Controller
public class BillFrwdPrintCntlr {

	@Autowired
	private BillForwardingDAO billForwardingDAO;
	
	@RequestMapping(value = "/getBFPrint", method = RequestMethod.POST)
	public @ResponseBody Object getBFPrint(@RequestBody String bfNo) {
		System.out.println("enter into getBFPrint function");
		Map<String,Object> map = new HashMap<>();
		Map<String,Object> bfMap = new HashMap<String, Object>();
		if(bfNo != null && bfNo != ""){
			bfMap = billForwardingDAO.getBillFrwdPrint(bfNo);
			if(bfMap != null){
				map.put("bfMap",bfMap);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
}

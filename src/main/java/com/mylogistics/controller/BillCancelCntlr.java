package com.mylogistics.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BillDAO;
import com.mylogistics.services.ConstantsValues;

@Controller
public class BillCancelCntlr {

	@Autowired
	private BillDAO billDAO;
	
	@RequestMapping(value = "/getBlFrCancel", method = RequestMethod.POST)
	public @ResponseBody Object getBlFrCancel(@RequestBody Map<String,Object> clientMap) {
		System.out.println("enter into getBlFrCancel function = ");
		Map<String,Object> map = new HashMap<>();
		String blNo = (String) clientMap.get("blNo");
		int brhId = (int) clientMap.get("brhId");
		if(blNo != null && blNo != "" && brhId > 0){
			Map<String,Object> blMap = billDAO.getBillByBlNo(blNo,brhId);
			System.out.println("blMap = "+blMap);;
			if(!blMap.isEmpty()){
				map.put("blMap",blMap);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/cancelBill", method = RequestMethod.POST)
	public @ResponseBody Object cancelBill(@RequestBody Map<String,Object> clientMap) {
		System.out.println("enter into cancelBill function = ");
		Map<String,Object> map = new HashMap<>();
		String blNo = (String) clientMap.get("blNo");
		int brhId = (int) clientMap.get("brhId");
				
		if(blNo != null && blNo != "" && brhId > 0){
			int res = billDAO.billCancel(blNo,brhId);
			if(res > 0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
}

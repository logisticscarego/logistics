package com.mylogistics.controller;

//import java.util.Date;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.DAO.bank.ChequeLeavesDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.NewVoucherService;
import com.mylogistics.services.VoucherService;


@Controller
public class NewVoucherCntlr {
	
	public static final Logger logger = Logger.getLogger(NewVoucherCntlr.class);

	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;

	@Autowired
	private BranchDAO branchDAO;

	@Autowired
	private HttpSession httpSession;

	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Autowired
	private ChequeLeavesDAO chequeLeavesDAO;
	
	@Autowired
	private CashStmtDAO cashStmtDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@Autowired
	private NewVoucherService newVoucherService;

	@RequestMapping(value = "/getVoucherDetails" , method = RequestMethod.POST)  
	public @ResponseBody Object getDataByCashW() {  
		System.out.println("Enter into getVoucherDetails---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			System.out.println("branchCode = "+branchCode);
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());
				System.out.println("cashStmtStatus ----->>> "+ cashStmtStatus);
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}

	
	
	@RequestMapping(value = "/getChequeNo" , method = RequestMethod.POST)  
	public @ResponseBody Object getChequeNo(@RequestBody Map<String,String> clientMap) {  
		System.out.println("Enter into getChequeNo---->");
		Map<String, Object> map = new HashMap<>();	
		String bankCode = clientMap.get("bankCode");
		String cType = clientMap.get("CType");
		char CType = cType.charAt(0);
		System.out.println("value of CType = "+CType);
		ChequeLeaves chequeLeaves= bankMstrDAO.getChqListByBnkC(bankCode,CType);		
		List<ChequeLeaves> chqList = bankMstrDAO.getChqLByBnk(bankCode,CType);
		if(!chqList.isEmpty()){
			System.out.println("***************** 1");
			map.put("list",chqList);
			map.put(ConstantsValues.CHEQUE_LEAVES,chequeLeaves);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			System.out.println("***************** 2");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}

	
	@RequestMapping(value = "/submitVoucher" , method = RequestMethod.POST)  
	public @ResponseBody Object submitVoucher(@RequestBody VoucherService voucherService) {  
		System.out.println("Enter into submitVoucher---->");
		Map<String, Object> map = new HashMap<>();	
		if(voucherService.getVoucherType().equalsIgnoreCase(ConstantsValues.CASH_WITHDRAW)){
			 map = saveCashWithdraw(voucherService);
			 System.out.println("bank name = "+map.get("bnkName"));
			 int vhNo = (int) map.get("vhNo");
			if(vhNo>0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}
		return map;
	}
	
	@RequestMapping(value = "/submitVoucherN" , method = RequestMethod.POST)  
	public @ResponseBody Object submitVoucherN(@RequestBody VoucherService voucherService) {  
		logger.info("Enter into submitVoucher---->");
		Map<String, Object> map = new HashMap<>();	
		if(voucherService.getVoucherType().equalsIgnoreCase(ConstantsValues.CASH_WITHDRAW)){
			 map = newVoucherService.saveNewVoucher(voucherService);
			 logger.info("bank name = "+map.get("bnkName"));
			 int vhNo = (int) map.get("vhNo");
			if(vhNo>0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}
		return map;
	}
	
	
	public Map<String,Object> saveCashWithdraw(VoucherService voucherService){
		System.out.println("Enter into saveCashWithdraw----->>>");
		//CashStmtStatus cashStmtStatus = new CashStmtStatus();
		User currentUser =	(User)httpSession.getAttribute("currentUser");
		Map<String,Object> map = new HashMap<String,Object>();
		List<FAMaster> faList = faMasterDAO.getFAMByFaName(ConstantsValues.CASH_IN_HAND);
		String cashFaCode = faList.get(0).getFaMfaCode();
		//List<ChequeLeaves> chequeLeavesList = chequeLeavesDAO.getChqLByChqNo();
		char chequeType = voucherService.getChequeType();
		String payMode = "Q";
		if(chequeType == 'C'){
			String bankCode = voucherService.getBankCode();
			List<BankMstr> bankMList = bankMstrDAO.getBankByBankCode(bankCode);
			if(!bankMList.isEmpty()){
				BankMstr bankMstr = bankMList.get(0);
				
				ChequeLeaves chequeLeaves = voucherService.getChequeLeaves();
				chequeLeaves.setBankMstr(bankMstr);
				chequeLeaves.setChqLChqAmt(voucherService.getAmount());
				chequeLeaves.setChqLUsedDt(new Date(new java.util.Date().getTime()));
				chequeLeaves.setChqLUsed(true);
				//String tvNo = chequeLeaves.getChqLChqNo() + "000000";
				String tvNo = chequeLeaves.getChqLChqNo();
				int temp = chequeLeavesDAO.updateChqLeaves(chequeLeaves);
				//int res = bankMstrDAO.decBankBal(bankMstr.getBnkId(),voucherService.getAmount());
				
				if(temp > 0){
					CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
					CashStmt cashStmt = new CashStmt();
					cashStmt.setbCode(currentUser.getUserBranchCode());
					cashStmt.setUserCode(currentUser.getUserCode());
					cashStmt.setCsDescription(voucherService.getDesc());
					cashStmt.setCsDrCr('C');
					cashStmt.setCsAmt(voucherService.getAmount());
					//cashStmt.setcLeaves(chequeLeaves);
					cashStmt.setCsType(voucherService.getVoucherType());
					cashStmt.setCsChequeType(voucherService.getChequeType());
					cashStmt.setCsPayTo(voucherService.getPayTo());
					cashStmt.setCsTvNo(tvNo);
					cashStmt.setCsVouchType("cash");
					cashStmt.setCsFaCode(bankCode);
					cashStmt.setPayMode(payMode);
					java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
					System.out.println("sqlDate ====>>"+sqlDate);
					
					cashStmt.setCsDt(sqlDate);
					//cashStmt.setCsNo(cashStmtStatus);
					//cashStmtStatus.getCashStmtList().add(cashStmt);
					map = cashStmtStatusDAO.updateCSSByCS(cashStmtStatus.getCssId() , cashStmt);
					/*int status = cashStmtDAO.saveCashStmt(cashStmt);*/
					/*if(vhNo > 0){
						map.put("vhNo",vhNo);
						map.put("tvNo",tvNo);
						return map;
					}*/
					
					
					//CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
					CashStmt cashStmt1 = new CashStmt();
					cashStmt1.setbCode(currentUser.getUserBranchCode());
					cashStmt1.setUserCode(currentUser.getUserCode());
					cashStmt1.setCsDescription(voucherService.getDesc());
					cashStmt1.setCsDrCr('D');
					cashStmt1.setCsAmt(voucherService.getAmount());
					//cashStmt.setcLeaves(chequeLeaves);
					cashStmt1.setCsType(voucherService.getVoucherType());
					cashStmt1.setCsChequeType(voucherService.getChequeType());
					cashStmt1.setCsPayTo(voucherService.getPayTo());
					cashStmt1.setCsTvNo(tvNo);
					cashStmt1.setCsVouchType("bank");
					cashStmt1.setCsFaCode(cashFaCode);
					cashStmt1.setCsVouchNo((int)map.get("vhNo"));
					cashStmt1.setPayMode(payMode);
					//java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
					System.out.println("sqlDate ====>>>"+sqlDate);
					
					cashStmt1.setCsDt(sqlDate);
					//cashStmt.setCsNo(cashStmtStatus);
					//cashStmtStatus.getCashStmtList().add(cashStmt);
					map = cashStmtStatusDAO.updateCSSByCS(cashStmtStatus.getCssId() , cashStmt1);
					/*int status = cashStmtDAO.saveCashStmt(cashStmt);*/
					/*if(vhNo > 0){
						map.put("vhNo",vhNo);
						map.put("tvNo",tvNo);
						return map;
					}*/
					
					
					/*//CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
					CashStmt cashStmt2 = new CashStmt();
					cashStmt2.setbCode(currentUser.getUserBranchCode());
					cashStmt2.setUserCode(currentUser.getUserCode());
					cashStmt2.setCsDescription(voucherService.getDesc());
					cashStmt2.setCsDrCr('C');
					cashStmt2.setCsAmt(voucherService.getAmount());
					//cashStmt.setcLeaves(chequeLeaves);
					cashStmt2.setCsType(voucherService.getVoucherType());
					cashStmt2.setCsChequeType(voucherService.getChequeType());
					cashStmt2.setCsPayTo(voucherService.getPayTo());
					cashStmt2.setCsTvNo(tvNo);
					cashStmt2.setCsVouchType("bank");
					cashStmt2.setCsFaCode(bankCode);
					cashStmt2.setCsVouchNo((int)map.get("vhNo"));
					//java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
					System.out.println("sqlDate ====>>>"+sqlDate);
					
					cashStmt2.setCsDt(sqlDate);
					//cashStmt.setCsNo(cashStmtStatus);
					//cashStmtStatus.getCashStmtList().add(cashStmt);
					map = cashStmtStatusDAO.updateCSSByCS(cashStmtStatus.getCssId() , cashStmt2);
					int status = cashStmtDAO.saveCashStmt(cashStmt);
					if(vhNo > 0){
						map.put("vhNo",vhNo);
						map.put("tvNo",tvNo);
						return map;
					}*/
					
					
					map.put("bnkName",bankMstr.getBnkName());
					map.put("tvNo",tvNo);
					return map;
				}
			}
		
		}else if(chequeType == 'M'){
			String bankCode = voucherService.getBankCode();
			List<BankMstr> bankMList = bankMstrDAO.getBankByBankCode(bankCode);
			if(!bankMList.isEmpty()){
				BankMstr bankMstr = bankMList.get(0);
				
				ChequeLeaves chequeLeaves = voucherService.getChequeLeaves();
				chequeLeaves.setBankMstr(bankMstr);
				chequeLeaves.setChqLChqAmt(voucherService.getAmount());
				chequeLeaves.setChqLUsedDt(new Date(new java.util.Date().getTime()));
				chequeLeaves.setChqLUsed(true);
				//String tvNo = chequeLeaves.getChqLChqNo() + "000000";
				String tvNo = chequeLeaves.getChqLChqNo();
				System.out.println("TV NO.... + "+tvNo);
				int temp = chequeLeavesDAO.updateChqLeaves(chequeLeaves);
				
				
				/*ChequeLeaves chequeLeaves = new ChequeLeaves();
				chequeLeaves.setbCode(currentUser.getUserBranchCode());
				chequeLeaves.setUserCode(currentUser.getUserCode());
				chequeLeaves.setBankMstr(bankMstr);
				chequeLeaves.setChqLChqNo("0000000");
				chequeLeaves.setChqLChqAmt(voucherService.getAmount());
				chequeLeaves.setChqLUsedDt(new Date(new java.util.Date().getTime()));
				chequeLeaves.setChqLUsed(true);
				String tvNo = "0000000000000";
				int saveCL = chequeLeavesDAO.saveChqLeaves(chequeLeaves);*/
				//int res = bankMstrDAO.decBankBal(bankMstr.getBnkId(),voucherService.getAmount());
				if(temp > 0){
					CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
					CashStmt cashStmt = new CashStmt();
					cashStmt.setbCode(currentUser.getUserBranchCode());
					cashStmt.setUserCode(currentUser.getUserCode());
					cashStmt.setCsDescription(voucherService.getDesc());
					cashStmt.setCsDrCr('C');
					cashStmt.setCsAmt(voucherService.getAmount());
					//cashStmt.setcLeaves(chequeLeaves);
					cashStmt.setCsType(voucherService.getVoucherType());
					cashStmt.setCsChequeType(voucherService.getChequeType());
					cashStmt.setCsTvNo(tvNo);
					cashStmt.setCsVouchType("cash");
					cashStmt.setCsFaCode(bankCode);
					cashStmt.setPayMode(payMode);
					java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
					System.out.println("sqlDate ====>>"+sqlDate);
					cashStmt.setCsDt(sqlDate);
					//cashStmt.setCsNo(cashStmtStatus);
					//cashStmtStatus.getCashStmtList().add(cashStmt);
					//map = cashStmtStatusDAO.updateCSSByCS(cashStmtStatus.getCssId() , cashStmt);
					
					map = cashStmtStatusDAO.updateCSSByCS(cashStmtStatus.getCssId() , cashStmt);
					
					//int status = cashStmtDAO.saveCashStmt(cashStmt);
					/*if(vhNo > 0){
						map.put("vhNo",vhNo);
						map.put("tvNo",tvNo);
						return map;
					}*/
					
					CashStmt cashStmt1 = new CashStmt();
					cashStmt1.setbCode(currentUser.getUserBranchCode());
					cashStmt1.setUserCode(currentUser.getUserCode());
					cashStmt1.setCsDescription(voucherService.getDesc());
					cashStmt1.setCsDrCr('D');
					cashStmt1.setCsAmt(voucherService.getAmount());
					//cashStmt.setcLeaves(chequeLeaves);
					cashStmt1.setCsType(voucherService.getVoucherType());
					cashStmt1.setCsChequeType(voucherService.getChequeType());
					cashStmt1.setCsPayTo(voucherService.getPayTo());
					cashStmt1.setCsTvNo(tvNo);
					cashStmt1.setCsVouchType("bank");
					cashStmt1.setCsFaCode(cashFaCode);
					cashStmt1.setCsVouchNo((int)map.get("vhNo"));
					cashStmt1.setPayMode(payMode);
					//java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
					System.out.println("sqlDate ====>>>"+sqlDate);
					
					cashStmt1.setCsDt(sqlDate);
					//cashStmt.setCsNo(cashStmtStatus);
					//cashStmtStatus.getCashStmtList().add(cashStmt);
					map = cashStmtStatusDAO.updateCSSByCS(cashStmtStatus.getCssId() , cashStmt1);
					/*int status = cashStmtDAO.saveCashStmt(cashStmt);*/
					/*if(vhNo > 0){
						map.put("vhNo",vhNo);
						map.put("tvNo",tvNo);
						return map;
					}*/
					
					
					/*//CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
					CashStmt cashStmt2 = new CashStmt();
					cashStmt2.setbCode(currentUser.getUserBranchCode());
					cashStmt2.setUserCode(currentUser.getUserCode());
					cashStmt2.setCsDescription(voucherService.getDesc());
					cashStmt2.setCsDrCr('C');
					cashStmt2.setCsAmt(voucherService.getAmount());
					//cashStmt.setcLeaves(chequeLeaves);
					cashStmt2.setCsType(voucherService.getVoucherType());
					cashStmt2.setCsChequeType(voucherService.getChequeType());
					cashStmt2.setCsPayTo(voucherService.getPayTo());
					cashStmt2.setCsTvNo(tvNo);
					cashStmt2.setCsVouchType("bank");
					cashStmt2.setCsFaCode(bankCode);
					cashStmt2.setCsVouchNo((int)map.get("vhNo"));
					//java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
					System.out.println("sqlDate ====>>>"+sqlDate);
					
					cashStmt2.setCsDt(sqlDate);
					//cashStmt.setCsNo(cashStmtStatus);
					//cashStmtStatus.getCashStmtList().add(cashStmt);
					map = cashStmtStatusDAO.updateCSSByCS(cashStmtStatus.getCssId() , cashStmt2);
					int status = cashStmtDAO.saveCashStmt(cashStmt);
					if(vhNo > 0){
						map.put("vhNo",vhNo);
						map.put("tvNo",tvNo);
						return map;
					}*/
					
					map.put("bnkName",bankMstr.getBnkName());
					map.put("tvNo",tvNo);
					return map;
				}
			}
		}else{
			System.out.println("wrong check Type");
		}
		
		return map;
		
	}
	

	// TODO for back date data
	@RequestMapping(value = "/getVoucherDetailsBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getDataByCashWBack() {  
		System.out.println("Enter into getVoucherDetails---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			System.out.println("branchCode = "+branchCode);
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());
				System.out.println("cashStmtStatus ----->>> "+ cashStmtStatus);
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}

	
	
}

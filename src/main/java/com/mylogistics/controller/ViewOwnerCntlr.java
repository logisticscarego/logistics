package com.mylogistics.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.AddressDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.OwnerDAO;
import com.mylogistics.DAO.OwnerOldDAO;
import com.mylogistics.model.Address;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.Owner;
import com.mylogistics.model.OwnerOld;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.ModelService;

@Controller
public class ViewOwnerCntlr {
	
	
	@Autowired
	private AddressDAO addressDAO;
	
	@Autowired
	private OwnerDAO ownerDAO;
	
	@Autowired
    private HttpSession httpSession;
	
	@Autowired
	private OwnerOldDAO ownerOldDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@RequestMapping(value = "/submitEditOwner", method = RequestMethod.POST)
	public @ResponseBody Object submitEditOwner(@RequestBody ModelService modelService) {
		System.out.println("Enter into SubmitEditOwner function-----------------");
		Map<String, Object> map = new HashMap<String, Object>();
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			Address currentAddress = modelService.getCurrentAddress();
			
			Owner owner = modelService.getOwner();
			System.out.println("The last owner id is ---------------"+owner.getOwnId());
			Owner owners = ownerDAO.getOwnerData(owner.getOwnId());
			 
			OwnerOld ownerOld = new OwnerOld();
			
			ownerOld.setbCode(owners.getbCode());
			ownerOld.setBranchCode(owners.getBranchCode());
			ownerOld.setOwnActiveDt(owners.getOwnActiveDt());
			ownerOld.setOwnBsnCard(owners.getOwnBsnCard());
			ownerOld.setOwnCIN(owners.getOwnCIN());
			ownerOld.setOwnCode(owners.getOwnCode());
			ownerOld.setOwnComStbDt(owners.getOwnComStbDt());
			ownerOld.setOwnEmailId(owners.getOwnEmailId());
			ownerOld.setOwnFirmRegNo(owners.getOwnFirmRegNo());
			ownerOld.setOwnFirmType(owners.getOwnFirmType());
			ownerOld.setOwnName(owners.getOwnName());
			ownerOld.setOwnPanNo(owners.getOwnPanNo());
			ownerOld.setOwnPanDOB(owners.getOwnPanDOB());
			ownerOld.setOwnPanDt(owners.getOwnPanDt());
			ownerOld.setOwnPanName(owners.getOwnPanName());
			ownerOld.setOwnPPNo(owners.getOwnPPNo());
			ownerOld.setOwnRegPlace(owners.getOwnRegPlace());
			ownerOld.setOwnSrvTaxNo(owners.getOwnSrvTaxNo());
			ownerOld.setOwnUnion(owners.getOwnUnion());
			ownerOld.setOwnVehicleType(owners.getOwnVehicleType());
			ownerOld.setOwnVoterId(owners.getOwnVoterId());
			ownerOld.setUserCode(owners.getUserCode());
			ownerOld.setView(false);
			
			int saveOldOwner = ownerOldDAO.saveOwnerOld(ownerOld,session);
			
			
			System.out.println("After saving into old owner---------------"+saveOldOwner);	
			
			User currentUser = (User)httpSession.getAttribute("currentUser");
			owners.setVehicleVendorMstrList(owners.getVehicleVendorMstrList());
			owners.setUserCode(currentUser.getUserCode());
			owners.setOwnName(owner.getOwnName());
			owners.setOwnFather(owner.getOwnFather());
			owners.setOwnPanName(owner.getOwnPanName());
			owners.setOwnPhNoList(owner.getOwnPhNoList());
			owners.setOwnPanNo(owner.getOwnPanNo());
			owners.setOwnEmailId(owner.getOwnEmailId());
			if(owner.getOwnCode()!= null){
				Date date = new Date();
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(date);		
					
				owner.setCreationTS(calendar);
				ownerDAO.updateOwner(session,owners);
			}
			
			if(currentAddress!=null){

				currentAddress.setAddRefCode(owner.getOwnCode());
				currentAddress.setAddType("Current Address");
				currentAddress.setUserCode(currentUser.getUserCode());
				currentAddress.setbCode(currentUser.getUserBranchCode());
				 addressDAO.updateAddress(currentAddress,session);
			}
			
			
			List<FAMaster> fList = faMasterDAO.retrieveFAMaster(owner.getOwnFaCode(),session);
			FAMaster faMaster = new FAMaster();
			faMaster=fList.get(0);
			faMaster.setFaMfaName(owner.getOwnName());
			faMasterDAO.updateFaMaster(faMaster,session);
			
			session.flush();
			session.clear();
			transaction.commit();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}catch(Exception e) {
			e.printStackTrace();
			transaction.rollback();
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}finally {
			session.close();
		}
		
			return map;
	}
	
	
	
	
	@RequestMapping(value = "/ownerDetFrVO", method = RequestMethod.POST)
	public @ResponseBody Object ownerDetFrVO(@RequestBody String ownCode) {
		System.out.println("Enter into ownerDetFrVO function");
		Map<String, Object> map = new HashMap<String, Object>();
		Owner owner = ownerDAO.getOwnByCode(ownCode);
		if(owner != null){
			map.put("owner",owner);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
}

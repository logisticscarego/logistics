package com.mylogistics.controller;

import java.sql.Blob;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.soap.InitParam;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mylogistics.DAO.AllImgDao;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class IMGCntlr {

	public static Logger logger = Logger.getLogger(IMGCntlr.class);
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private AllImgDao allImgDao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Blob blob = null;
	
	@RequestMapping(value = "/getAllImg", method = RequestMethod.POST)
	public @ResponseBody Object getAllImg(@RequestBody Map<String, String> initParam){
		logger.info("Enter into /getAllImg() : BranchCode = "+initParam.get("branchCode")+ " : ImgType = "+initParam.get("imgType")+" : Code = "+initParam.get("code"));
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			if(httpSession.getAttribute("currentUser") == null){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Please login !");
				logger.info("User is not login !");
			}else{
				String branchCode = initParam.get("branchCode");
				String imgType = initParam.get("imgType");
				String code = initParam.get("code");
				
				if(imgType.equalsIgnoreCase("cnmt")){
					List<Map<String, Object>> cnmtList = allImgDao.getUnsavedCnmtImg(branchCode, code);
					if(cnmtList.isEmpty()){
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
						resultMap.put("msg", "IMG not found !");
					}else{
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
						resultMap.put("list", cnmtList);
					}
				}
				
			}
		}catch(Exception e){
			logger.error("Exception : "+e);
		}
		logger.info("Exit from /getAllImg()");
		return resultMap;
	}
	
	@RequestMapping(value = "/uploadAllImg", method = RequestMethod.POST)
	public @ResponseBody Object uploadAllImg(@RequestParam("file") MultipartFile file, @RequestParam("imgId") Integer imgId){	
		logger.info("Enter into /uploadAllImg() : File = "+file+" : Img ID = "+imgId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Blob blob = null;
		try{
			byte [] fileInBytes = file.getBytes();
			resultMap = allImgDao.updateImage(fileInBytes, imgId);
		}catch(Exception e){
			logger.error("Exception : "+e);
		}
		logger.info("Exit from /uploadAllImg()");
		return resultMap;
	}

	@RequestMapping(value = "/downloadImg", method = RequestMethod.POST)
	public void downloadImg(HttpServletRequest request, HttpServletResponse response){	
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if(user != null){
			logger.info("UserID = "+user.getUserId()+" : Enter into /downloadImg()");		
			try{
				if(blob != null){
					ServletOutputStream out = response.getOutputStream();
					out.write(blob.getBytes(1, (int) blob.length()));
					out.close();
					blob = null;
				}					
			}catch(Exception e){
				logger.error("Exception : "+e);
			}
		}		
		logger.info("UserID = "+user.getUserId()+" : Exit from /downloadImg()");
	}
	
	@RequestMapping(value = "/isDownloadImg", method = RequestMethod.POST)
	public @ResponseBody Object isDownloadImg(@RequestBody Map<String, String> initParam){	
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if(user != null){
			logger.info("UserID = "+user.getUserId()+" : Enter into /isDownloadImg() : InitParam = "+initParam);		
			Session session = null;
			try{
				session = this.sessionFactory.openSession();
				blob = allImgDao.downloadImg(session, user, initParam);				
				if(blob == null){	
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Image not found !");					
				}else
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}catch(Exception e){
				logger.error("Exception : "+e);
			}finally{
				session.clear();
				session.close();
			}
		}		
		logger.info("UserID = "+user.getUserId()+" : Exit from /isDownloadImg()");
		return resultMap;
	}
	
	@RequestMapping(value = "getOwnBrkForImgRead", method = RequestMethod.POST)
	public @ResponseBody Object getOwnBrkForImgRead(@RequestBody Map<String, String> initParam){
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		logger.info("UserID = "+user.getUserId()+" : Enter into getOwnBrkForImgRead()");
		Session session = this.sessionFactory.openSession();
		try{
			resultMap = allImgDao.getOwnBrkForImgRead(session, user, initParam, resultMap);
		}catch(Exception e){
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Please try again !");
		}finally{
			session.clear();
			session.close();
		}
		logger.info("UserID = "+user.getUserId()+": getOwnBrkForImgRead()");
		return resultMap;
	}
	
	
	
	@RequestMapping(value = "/upldExcelFile", method = RequestMethod.POST)
	public @ResponseBody Object upldExcelFile(@RequestParam("file") MultipartFile file){	
		logger.info("Enter into /upldExcelFile() : FileName = "+file.getName());
		System.out.println("Enter into /upldExcelFile() : File = "+file.getName());
		System.out.println("Enter into /upldExcelFile() : File = "+file.getOriginalFilename());
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			byte [] fileInBytes = file.getBytes();
			resultMap = allImgDao.upldExcelFile(fileInBytes,file.getOriginalFilename());
		}catch(Exception e){
			logger.error("Exception : "+e);
		}
		logger.info("Exit from /upldExcelFile()");
		return resultMap;
	}
	
	
	@RequestMapping(value = "/getFileByDate", method = RequestMethod.POST)
	public @ResponseBody Object getFileByDate(@RequestBody Map<String,String> dateMap){	
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Date date=Date.valueOf(dateMap.get("date"));
		System.out.println("Date="+date);
			resultMap = allImgDao.getFileNames(date);

			return resultMap;
	}
	
	
	@RequestMapping(value = "/dwnldExcelFile", method = RequestMethod.POST)
	public @ResponseBody Object dwnldExcelFile(HttpServletRequest request, HttpServletResponse response){	
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, String> resultMap = new HashMap<String, String>();
		Blob blob;
		Date date=Date.valueOf(request.getParameter("date"));
		String fileName=request.getParameter("fileName");
		int bsuId=Integer.parseInt(request.getParameter("bsuIdName"));
		System.out.println("date="+date+fileName+bsuId);
		Workbook workbook=new XSSFWorkbook(); 
		if(user != null){
			logger.info("UserID = "+user.getUserId()+" : Enter into /isDownloadImg() : InitParam = ");		
			Session session = null;
			try{
				session = this.sessionFactory.openSession();
				blob = allImgDao.downloadExcel(session, bsuId);				
				if(blob == null){	
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "File not found !");					
				}else {
					response.setContentType("application/vnd.ms-excel");
			        response.setHeader("Content-Disposition", "attachment; filename="+fileName);
					ServletOutputStream out = response.getOutputStream();
					byte[] file=blob.getBytes(1, (int) blob.length());
					out.write(file, 0, file.length);
			           
					workbook.write(out);
			        out.flush();
			        out.close();
			        resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);

				}
					
			}catch(Exception e){
				logger.error("Exception : "+e);
			}finally{
				session.clear();
				session.close();
			}
		}		
		logger.info("UserID = "+user.getUserId()+" : Exit from /isDownloadImg()");
		return resultMap;
	}
	
	
	
	
	@RequestMapping(value = "/downloadBrkBnkImg", method = RequestMethod.POST)
	public void downloadBrkBnkImg(HttpServletRequest request, HttpServletResponse response){	
		User user = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session=this.sessionFactory.openSession();
		if(user != null){
			logger.info("UserID = "+user.getUserId()+" : Enter into /downloadBrkBnkImg()");		
			Blob blob=null;
			try{
				String imageType=String.valueOf(request.getParameter("typeName"));
				String brkCode=String.valueOf(request.getParameter("brkCodeName"));
				String ownType=String.valueOf(request.getParameter("ownCodeName"));
				//request.get
				Map<String, String> initParam = new HashMap<String, String>();
				initParam.put("imageType", "BRK");
				initParam.put("id", brkCode);
				initParam.put("ownBrkImgType", "CHQ");
				
				System.out.println("imgTyp="+imageType+"  brkCode"+brkCode+" ownType "+ownType);
				blob=allImgDao.downloadImg(session, user, initParam);
				
				if(blob != null){
					ServletOutputStream out = response.getOutputStream();
					out.write(blob.getBytes(1, (int) blob.length()));
					out.close();
					blob = null;
				}					
			}catch(Exception e){
				logger.error("Exception : "+e);
			}
		}	
		session.clear();
		session.close();
		logger.info("UserID = "+user.getUserId()+" : Exit from /downloadBrkBnkImg()");
	}
	
	
}
package com.mylogistics.controller;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.UserDAO;
import com.mylogistics.DAO.UserRightsDAO;
import com.mylogistics.model.User;
import com.mylogistics.model.UserRights;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.SuperAdminBehave;

@Controller
public class UserCntlr {
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
	private UserRightsDAO userRightsDAO;
	
	//private SessionService sessionService = new SessionService();
	
	private SuperAdminBehave superAdminBehave = new SuperAdminBehave();
	//private HttpSession httpSession;
	
	
	private static final String TAG = "UserCntlr: ";
	
	
		
	@RequestMapping(value = "/userRetrieve" , method = RequestMethod.GET)  
	public ModelAndView retrieveUser(HttpServletRequest request,HttpServletResponse res) {   
		
		String userName = request.getParameter("userName");
		List<User> userList = userDAO.retrieveUser(userName);
		
		return new ModelAndView("home", "user", userList);
	}


	/*@RequestMapping(value = "/userUpdate" , method = RequestMethod.POST)  
	public ModelAndView updateUserByUserCode(HttpServletRequest request,HttpServletResponse res) {   
		String userCode = request.getParameter("userCode");
		String userName = request.getParameter("userName");
		
		User user = userDAO.updateUserByUserCode(userCode , userName);
		
		System.out.println("------"+user.getUserCode()+"-----");
		return new ModelAndView("home", "user", user);
	}*/
	

	@RequestMapping(value = "/roleOfUser" , method = RequestMethod.GET)  
	public @ResponseBody Object roleOfUser(){
		
		System.out.println("+++++++++++enter into roleOfUser++++++++++");
		Map<String,Object> map = new HashMap<String,Object>();
		
		try {
			//User currentUser = sessionService.getCurrentUser();
			User currentUser = (User)httpSession.getAttribute("currentUser");
			if(currentUser == null){
				System.out.println("currentUser = "+currentUser);
				map.put("role","unauthorized");
				return map;
			}else{
				if (currentUser.getUserRole() == 11 || currentUser.getUserRole() == 22 || currentUser.getUserRole() == 33) {
					String brhName = branchDAO.getBrNameByBrCode(currentUser.getUserBranchCode());
					String empName = employeeDAO.getEmpNameByEmpCode(currentUser.getUserCode());
					UserRights userRights = userRightsDAO.getUserRights(currentUser.getUserId());
					
					map.put("empName",empName);
					map.put("brhName",brhName);
					map.put("userRights",userRights);
					map.put("role",currentUser.getUserRole());
					map.put("userCode",currentUser.getUserCode());
					return map;
				}else {
					map.put("role","unauthorized");
					return map;
				}
			}
		} catch (Exception e) {
			map.put("role","unauthorized");
			e.printStackTrace();
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/user" , method = RequestMethod.POST)  
	public @ResponseBody Object loginUser(@RequestBody User user)throws Exception {  
		System.out.println("enter into loginUser function---->"+user.getUserName());
		//String userName = user.getUserName();
		User cUser = (User) httpSession.getAttribute("currentUser");
		System.out.println("cUser****************"+cUser);
		Map<String,Object> map = new HashMap<String,Object>();
		if(cUser == null){
			String userName = user.getUserName();
	        String pass = user.getPassword();
	        
	        MessageDigest md = MessageDigest.getInstance("MD5");
		    md.update(pass.getBytes());
		    byte[] digest = md.digest();
		    StringBuffer sb = new StringBuffer();
		    for (byte b : digest) {
	            sb.append(String.format("%02x", b & 0xff));
	        }
		    System.out.println("original:" + pass);
	        System.out.println("digested(hex):" + sb.toString());
	        String password = sb.toString();
	        
	        System.out.println("inside loginUser ----->"+userName);
	        List<User> userList = userDAO.getUserByUN(userName , password);
	       
	         
	        if (userList.isEmpty()) {
				System.out.println(TAG+" User not exist ");
				map.put("targetPage","errorpage");
				map.put("message","User not exist");
				System.out.println("------exit from helloUser-------");
				return map;
			}else {
				
				if (userList.get(0).getPassword().equals(password)) {
					String empName = employeeDAO.getEmpNameByEmpCode(userList.get(0).getUserCode());
					String brhName = branchDAO.getBrNameByBrCode(userList.get(0).getUserBranchCode());
					UserRights userRights = userRightsDAO.getUserRights(userList.get(0).getUserId());
					
					
					map.put("userCode",userList.get(0).getUserCode());
					if (userList.get(0).getUserRole() == 11) {
						if(userList.get(0).getUserStatus().equalsIgnoreCase("inactive")){
							map.put("targetPage","errorpage");
							map.put("message","this user has been inactivate by Super-Admin");
							return map;
						}
						
						//sessionService = new SessionService();
						//sessionService.setCurrentUser(userList.get(0));
						User currentUser = userList.get(0);
						currentUser.setUserAuthToken(httpSession.getId());
						String result = userDAO.updateUser(currentUser);
						if(result.equalsIgnoreCase("success")){
							httpSession.setAttribute("currentUser",currentUser);
							httpSession.setAttribute("currentUserRights",userRights);
							httpSession.setMaxInactiveInterval(990000);
							System.out.println(TAG+" Administrator");
							superAdminBehave.setAdminLogin(false);
							superAdminBehave.setOperatorLogin(false);
							superAdminBehave.setSuperAdminLogin(false);
							map.put("targetPage","superAdmin");
							map.put("currentUser",currentUser);
							map.put("brhName",brhName);
							map.put("empName",empName);
							map.put("userRights",userRights);
							map.put("message","Welcome "+userList.get(0).getUserName());
							System.out.println("------exit from helloUser-------");
							return map;
						}else{
							System.out.println(TAG+" Administrator");
							map.put("targetPage","login");
							map.put("message","User Authentication fail");
							System.out.println("------exit from helloUser-------");
							return map;
						}
						
					}else if (userList.get(0).getUserRole() == 22) {
						if(userList.get(0).getUserStatus().equalsIgnoreCase("inactive")){
							map.put("targetPage","errorpage");
							map.put("message","this user has been inactivate by Super-Admin");
							return map;
						}
						//sessionService = new SessionService();
						//sessionService.setCurrentUser(userList.get(0));
						User currentUser = userList.get(0);
						currentUser.setUserAuthToken(httpSession.getId());
						String result = userDAO.updateUser(currentUser);
						if(result.equalsIgnoreCase("success")){
							httpSession.setAttribute("currentUser",currentUser);
							httpSession.setAttribute("currentUserRights",userRights);
							httpSession.setMaxInactiveInterval(99000);
							System.out.println(TAG+" Administrator");
							map.put("targetPage","admin");
							map.put("currentUser",currentUser);
							map.put("empName",empName);
							map.put("brhName",brhName);
							map.put("userRights",userRights);
							map.put("message","Welcome "+userList.get(0).getUserName());
							System.out.println("------exit from helloUser-------");
							return map;
						}else{
							System.out.println(TAG+" Administrator");
							map.put("targetPage","login");
							map.put("message","User Authentication fail");
							System.out.println("------exit from helloUser-------");
							return map;
						}
					}else {
						if(userList.get(0).getUserStatus().equalsIgnoreCase("inactive")){
							map.put("targetPage","errorpage");
							map.put("message","this user has been inactivate by Super-Admin");
							return map;
						}
						//sessionService = new SessionService();
						//sessionService.setCurrentUser(userList.get(0));
						User currentUser = userList.get(0);
						currentUser.setUserAuthToken(httpSession.getId());
						//List<String> userRights = currentUser.getUserRights();
						map.put("list",userRights);
						String result = userDAO.updateUser(currentUser);
						if(result.equalsIgnoreCase("success")){
							httpSession.setAttribute("currentUser",currentUser);
							httpSession.setAttribute("currentUserRights",userRights);
							httpSession.setMaxInactiveInterval(99000);
							System.out.println(TAG+" OperatorCntrl");
							map.put("targetPage","operator");
							map.put("currentUser",currentUser);
							map.put("empName",empName);
							map.put("brhName",brhName);
							map.put("userRights",userRights);
							map.put("message","Welcome "+userList.get(0).getUserName());
							System.out.println("------exit from helloUser-------");
							return map;
						}else{
							System.out.println(TAG+" Administrator");
							map.put("targetPage","login");
							map.put("message","User Authentication fail");
							System.out.println("------exit from helloUser-------");
							return map;
						}
					}
				} else {
					System.out.println(TAG+" incorrect password");
					map.put("targetPage","errorpage");
					map.put("message","incorrect password");
					System.out.println("------exit from helloUser-------");
					return map;
				}			
			}
		}else{
			map.put("message","already logged in");
			System.out.println("cUser.getUserRole() ======> "+cUser.getUserRole());
			String brhName = branchDAO.getBrNameByBrCode(cUser.getUserBranchCode());
			String empName = employeeDAO.getEmpNameByEmpCode(cUser.getUserCode());
			UserRights userRights = userRightsDAO.getUserRights(cUser.getUserId());
			map.put("userCode",cUser.getUserCode());
			map.put("empName",empName);
			map.put("brhName",brhName);
			map.put("userRights",userRights);
			if(cUser.getUserRole() == 11){
				map.put("targetPage","superAdmin");
			}else if(cUser.getUserRole() == 22){
				map.put("targetPage","admin");
			}else if(cUser.getUserRole() == 33){
				map.put("targetPage","operator");
			}else{
				map.put("message","ERROR");
			}
			
			return map;
		}
		

    }
	
	
	@RequestMapping(value = "/superAdminBecomes" , method = RequestMethod.POST)  
	public @ResponseBody Object superAdminBecomes(@RequestBody SuperAdminBehave newSuperAdminBehave) {  
		
		System.out.println("enter into SuperAdminBecomes function");
		Map<String,String> map = new HashMap<String,String>();
		//superAdminBehave = new SuperAdminBehave();
		superAdminBehave = newSuperAdminBehave;
		
		if(superAdminBehave.isSuperAdminLogin() == true){
			superAdminBehave.setAdminLogin(false);
			superAdminBehave.setOperatorLogin(false);
			System.out.println("--------->superAdmin behave as a superAdmin");
			//map.put("behaveAs","superAdmin");
		}else if(superAdminBehave.isAdminLogin() == true){
			superAdminBehave.setSuperAdminLogin(false);
			superAdminBehave.setOperatorLogin(false);
			System.out.println("--------->superAdmin behave as a Admin");
			//map.put("behaveAs","admin");
		}else if(superAdminBehave.isOperatorLogin() == true){
			superAdminBehave.setSuperAdminLogin(false);
			superAdminBehave.setAdminLogin(false);
			System.out.println("--------->superAdmin behave as a Operator");
			//map.put("behaveAs","operator");
		}else{
			superAdminBehave.setSuperAdminLogin(false);
			superAdminBehave.setAdminLogin(false);
			superAdminBehave.setOperatorLogin(false);
			System.out.println("--------->superAdmin behave as default");
		}
		map.put("result","success");
		return map;
	}
	
	
	@RequestMapping(value = "/getSuperAdminBehaviour" , method = RequestMethod.GET)  
	public @ResponseBody Object getSuperAdminBehaviour() {  
		
		System.out.println("enter into getSuperAdminBehaviour function");
		Map<String,String> map = new HashMap<String,String>();
		if(superAdminBehave != null){
			if(superAdminBehave.isSuperAdminLogin() == true){
				superAdminBehave.setAdminLogin(false);
				superAdminBehave.setOperatorLogin(false);
				System.out.println("--------->superAdmin behave as a superAdmin");
				map.put("behaveAs","superAdmin");
			}else if(superAdminBehave.isAdminLogin() == true){
				superAdminBehave.setSuperAdminLogin(false);
				superAdminBehave.setOperatorLogin(false);
				System.out.println("--------->superAdmin behave as a Admin");
				map.put("behaveAs","admin");
			}else if(superAdminBehave.isOperatorLogin() == true){
				superAdminBehave.setSuperAdminLogin(false);
				superAdminBehave.setAdminLogin(false);
				System.out.println("--------->superAdmin behave as a Operator");
				map.put("behaveAs","operator");
			}else{
				superAdminBehave.setSuperAdminLogin(false);
				superAdminBehave.setAdminLogin(false);
				superAdminBehave.setOperatorLogin(false);
				map.put("behaveAs","default");
			}
		}else{
			map.put("behaveAs","default");
		}
		return map;
	}
	
	@RequestMapping(value = "/logout" , method = RequestMethod.POST)  
	public @ResponseBody Object logout() {  
		
		System.out.println("enter into logout function");
		Map<String,String> map = new HashMap<String,String>();
		httpSession.removeAttribute("currentUser");
		httpSession.removeAttribute("currentUserRights");
		httpSession.invalidate();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}

}
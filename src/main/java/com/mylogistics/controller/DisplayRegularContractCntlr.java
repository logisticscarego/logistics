package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.ContToStnDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.PenaltyBonusDetentionDAO;
import com.mylogistics.DAO.RateByKmDAO;
import com.mylogistics.DAO.RegularContractDAO;
import com.mylogistics.DAO.RegularContractOldDAO;
import com.mylogistics.DAO.TermNConditionDAO;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.Customer;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.PenaltyBonusDetention;
import com.mylogistics.model.RateByKm;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.RegularContractOld;
import com.mylogistics.model.TermNCondition;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.ContToStnService;
import com.mylogistics.services.ContToStnServiceImpl;
import com.mylogistics.services.PBDService;
import com.mylogistics.services.PBDServiceImpl;
import com.mylogistics.services.RBKMService;
import com.mylogistics.services.RBKMServiceImpl;

@Controller
public class DisplayRegularContractCntlr {
	
	@Autowired
	RegularContractDAO regularContractDAO;
	
	@Autowired
    private HttpSession httpSession;
	
	@Autowired
	private RateByKmDAO rateByKmDAO;
	
	@Autowired
	private PenaltyBonusDetentionDAO penaltyBonusDetentionDAO;
	
	@Autowired
	private RegularContractOldDAO regularContractOldDAO;
	
	@Autowired
	private TermNConditionDAO termNConditionDAO;
	
	@Autowired
	private ContToStnDAO contToStnDAO;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	private RBKMService rbkmService = new RBKMServiceImpl();

	private PBDService pbdService = new PBDServiceImpl();
	
	private ContToStnService contToStnService = new ContToStnServiceImpl();

	String contractCode=null;
	
	String metric=null;

	@RequestMapping(value="/getRegContIsViewNo	",method = RequestMethod.POST)
	public @ResponseBody Object getRegContIsViewNo(){
		
		System.out.println("Controller called of getRegContIsViewNo");
		Map<String,Object> map = new HashMap<String,Object>();
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		String branchCode = currentUser.getUserBranchCode();
		List<RegularContract> regularContractsLists = regularContractDAO.getRegContractisViewFalse(branchCode);
		System.out.println("Controller called after retrieving list");
		
		for(int i=0;i<regularContractsLists.size();i++){
			System.out.println("The values in the list is"+regularContractsLists.get(i).getCreationTS());
		}
		
		if(!regularContractsLists.isEmpty()){
			map.put("list",regularContractsLists);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT,"No more Contracts to show");
		}
		return map;
	}
	
	@RequestMapping(value = "/getRegularContCode" , method = RequestMethod.GET)  
	  public @ResponseBody Object getRegularContCode(){
	
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();
		List<String> regularContractsLists = regularContractDAO.getRegularContractCodes(branchCode);
		String regularContCode [] = new String[regularContractsLists.size()];
		
		for(int i=0;i<regularContractsLists.size();i++){
			System.out.println("--------------------"+regularContractsLists.get(i));
			regularContCode[i]=regularContractsLists.get(i);
		}
		
		return regularContCode;
		} 
	
	@RequestMapping(value = "/updateIsViewRegularContract", method = RequestMethod.POST)
	public @ResponseBody Object updateIsViewContract(@RequestBody String selection) {

		String contids[] = selection.split(",");
		int temp=regularContractDAO.updateRegContisViewTrue(contids);
		Map<String, Object> map = new HashMap<String, Object>();
		if(temp>0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}
		else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/regularcontractdetails", method = RequestMethod.POST)
	public @ResponseBody Object regularcontractdetails(@RequestBody String regContCode){
    	   	 
		RegularContract regularContract =new RegularContract();
		
		List<RegularContract> list = regularContractDAO.getRegularContract(regContCode);
		List<RegularContractOld> list2 = regularContractOldDAO.getOldRegularContract(regContCode);
		
		Map<String,Object> map = new HashMap<String , Object>();
		if(!list.isEmpty() || !list2.isEmpty()){
			regularContract = list.get(0);
			map.put("regularContractOld", list2);
			map.put("regularContract",regularContract);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			regularContract = list.get(0);
			map.put("regularContract",regularContract);
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
  		}
		return map;
	}
	
	/*@RequestMapping(value="/EditRegularContractSubmit", method = RequestMethod.POST)
	public @ResponseBody Object EditRegularContractSubmit(@RequestBody RegularContract regCnt){
		
		Map<String,Object> map = new HashMap<String , Object>();
		int temp =	regularContractDAO.updateRegularContract(regCnt);
		if(temp>0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}
		else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}*/
	
	@RequestMapping(value = "/getRegContCodesList", method = RequestMethod.POST)
	public @ResponseBody Object getRegContCodesList() {
		System.out.println("Enter into getRegContCodesList============================");
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();
		List<String> bList = regularContractDAO.getRegularContractCodes(branchCode);
		Map<String, Object> map = new HashMap<String, Object>();
		if(!bList.isEmpty()){
			map.put("list", bList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/EditRegularContractSubmit", method = RequestMethod.POST)
	public @ResponseBody Object EditRegularContractSubmit(@RequestBody RegularContract regularContract) {

		System.out.println("Enter into EditRegularContractSubmit function------");
		Map<String, Object> map = new HashMap<String, Object>();
		
		int id = regularContract.getRegContId();
		contractCode = regularContract.getRegContCode();
		
		List<RegularContract> list = regularContractDAO.getRegularContractData(id);
		
		if(!list.isEmpty()){
			RegularContractOld regularContractOld = new RegularContractOld();
			regularContractOld.setRegContId(list.get(0).getRegContId());
			regularContractOld.setbCode(list.get(0).getbCode());
			regularContractOld.setBranchCode(list.get(0).getBranchCode());
			regularContractOld.setCreationTS(list.get(0).getCreationTS());
			regularContractOld.setRegContBLPMCode(list.get(0).getRegContBLPMCode());
			regularContractOld.setRegContCngrCode(list.get(0).getRegContCngrCode());
			regularContractOld.setRegContCode(list.get(0).getRegContCode());
			regularContractOld.setRegContCostGrade(list.get(0).getRegContCostGrade());
			regularContractOld.setRegContCrName(list.get(0).getRegContCrName());
			regularContractOld.setRegContDc(list.get(0).getRegContDc());
			regularContractOld.setRegContDdl(list.get(0).getRegContDdl());
			regularContractOld.setRegContFromDt(list.get(0).getRegContFromDt());
			regularContractOld.setRegContFromStation(list.get(0).getRegContFromStation());
			regularContractOld.setRegContInsureComp(list.get(0).getRegContInsureComp());
			regularContractOld.setRegContInsuredBy(list.get(0).getRegContInsuredBy());
			regularContractOld.setRegContInsurePolicyNo(list.get(0).getRegContInsurePolicyNo());
			regularContractOld.setRegContInsureUnitNo(list.get(0).getRegContInsureUnitNo());
			regularContractOld.setRegContIsVerify(list.get(0).getRegContIsVerify());
			//regularContractOld.setRegContLoad(list.get(0).getRegContLoad());
			regularContractOld.setRegContProportionate(list.get(0).getRegContProportionate());
			regularContractOld.setRegContRate(list.get(0).getRegContRate());
			regularContractOld.setRegContRemark(list.get(0).getRegContRemark());
			regularContractOld.setRegContRenew(list.get(0).getRegContRenew());
			regularContractOld.setRegContRenewDt(list.get(0).getRegContRenewDt());
			//regularContractOld.setRegContStatisticalCharge(list.get(0).getRegContStatisticalCharge());
			regularContractOld.setRegContToDt(list.get(0).getRegContToDt());
			regularContractOld.setRegContToStation(list.get(0).getRegContToStation());
			//regularContractOld.setRegContTransitDay(list.get(0).getRegContTransitDay());
			regularContractOld.setRegContType(list.get(0).getRegContType());
			//regularContractOld.setRegContUnLoad(list.get(0).getRegContUnLoad());
			regularContractOld.setRegContValue(list.get(0).getRegContValue());
			regularContractOld.setRegContWt(list.get(0).getRegContWt());
			regularContractOld.setUserCode(list.get(0).getUserCode());
			regularContractOld.setView(false);
			
			int saveOldContract = regularContractOldDAO.saveRegContOld(regularContractOld);
			
			if(saveOldContract>0){
				List<RateByKm> rbkmList = rbkmService.getAllRBKM();
				List<PenaltyBonusDetention> pbdList = pbdService.getAllPBD();
				List<ContToStn> cList = contToStnService.getAllCTS();
				
				User currentUser = (User)httpSession.getAttribute("currentUser");
				
				if(!rbkmList.isEmpty()){
					for (int i = 0; i < rbkmList.size(); i++) {
						double rbkmRate=0;
						if(metric.equals("Ton")){
							rbkmRate = rbkmList.get(i).getRbkmRate();
							rbkmRate = (rbkmRate/ConstantsValues.kgInTon);
							//rbkmRate=Double.parseDouble((df.format(rbkmRate)));
							  rbkmRate = (double)Math.round(rbkmRate*100.0)/100.0;
							rbkmList.get(i).setRbkmRate(rbkmRate);
						}
						System.out.println("Rbkm rate is-----"+rbkmRate);
						rbkmList.get(i).setRbkmContCode(regularContract.getRegContCode());
						rbkmList.get(i).setbCode(currentUser.getUserBranchCode());
						rbkmList.get(i).setUserCode(currentUser.getUserCode());
					int temp =rateByKmDAO.saveRBKM(rbkmList.get(i));
					System.out.println("Saved Rbkm------"+temp);
					}
					}
				
				if(!pbdList.isEmpty()){
				for (int i = 0; i < pbdList.size(); i++) {
					pbdList.get(i).setPbdContCode(regularContract.getRegContCode());
					pbdList.get(i).setbCode(currentUser.getUserBranchCode());
					pbdList.get(i).setUserCode(currentUser.getUserCode());
					int savePBD = penaltyBonusDetentionDAO.savePBD(pbdList.get(i));
					System.out.println("after saving pbd---------------"+savePBD);
				}
				}
				
				if(!cList.isEmpty()){
					for (int i = 0; i < cList.size(); i++) {
						double ctsRate=0;
						double ctsFromWt=0;
						double ctsToWt=0;
						if(metric.equals("Ton") && (regularContract.getRegContProportionate().equals("P"))){
							ctsRate = cList.get(i).getCtsRate();
							ctsRate = ctsRate/ConstantsValues.kgInTon;
							ctsRate = (double)Math.round(ctsRate*100.0)/100.0;
							cList.get(i).setCtsRate(ctsRate);
							
							ctsFromWt = cList.get(i).getCtsFromWt();
							ctsFromWt = ctsFromWt*ConstantsValues.kgInTon;
							ctsFromWt = (double)Math.round(ctsFromWt*100.0)/100.0;
							cList.get(i).setCtsFromWt(ctsFromWt);
							
							ctsToWt = cList.get(i).getCtsToWt();
							ctsToWt = ctsToWt*ConstantsValues.kgInTon;
							ctsToWt = (double)Math.round(ctsToWt*100.0)/100.0;
							cList.get(i).setCtsToWt(ctsToWt);
						}else if(metric.equals("Ton") && (regularContract.getRegContProportionate().equals("F"))){
							ctsRate = cList.get(i).getCtsRate();
							cList.get(i).setCtsRate(ctsRate);
							
							ctsFromWt = cList.get(i).getCtsFromWt();
							ctsFromWt = ctsFromWt*ConstantsValues.kgInTon;
							ctsFromWt = (double)Math.round(ctsFromWt*100.0)/100.0;
							cList.get(i).setCtsFromWt(ctsFromWt);
							
							ctsToWt = cList.get(i).getCtsToWt();
							ctsToWt = ctsToWt*ConstantsValues.kgInTon;
							ctsToWt = (double)Math.round(ctsToWt*100.0)/100.0;
							cList.get(i).setCtsToWt(ctsToWt);
						}
						
						System.out.println("cts rate is ----"+ctsRate);
						System.out.println("ctsFromWt is ----"+ctsFromWt);
						System.out.println("ctsToWt is ----"+ctsToWt);
						
						cList.get(i).setCtsContCode(regularContract.getRegContCode());
						cList.get(i).setbCode(currentUser.getUserBranchCode());
						cList.get(i).setUserCode(currentUser.getUserCode());
						
						int temp=contToStnDAO.saveContToStn(cList.get(i));
						System.out.println("Saved Cont to Stations-------"+temp);
					}
					}
				
				int temp =	regularContractDAO.updateRegularContract(regularContract);
				System.out.println("After updating----"+temp);
				
				List<Customer> customerList= customerDAO.getCustomer(regularContract.getRegContBLPMCode());
				String custName=customerList.get(0).getCustName();
				System.out.println("customer name is-----------"+custName);
				List<FAMaster> fList = faMasterDAO.retrieveFAMaster(regularContract.getRegContFaCode());
				FAMaster faMaster = new FAMaster();
				faMaster=fList.get(0);
				faMaster.setFaMfaName(custName);
				int updateFaMstr=faMasterDAO.updateFaMaster(faMaster);
				System.out.println("Updated faMaster------------"+updateFaMstr);
				
				if(temp>0){
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else {
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}	
			}
			}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/saveUpdatedTncForRC", method = RequestMethod.POST)
	public @ResponseBody Object saveUpdatedTncForRC(@RequestBody TermNCondition termNCondition) {

		System.out.println("bonus"+termNCondition.getTncBonus());
		System.out.println("detention"+termNCondition.getTncDetention());
		System.out.println("loading"+termNCondition.getTncLoading());
		System.out.println("penalty"+termNCondition.getTncPenalty());
		System.out.println("SC"+termNCondition.getTncStatisticalCharge());
		System.out.println("transit day"+termNCondition.getTncTransitDay());
		System.out.println("unloading"+termNCondition.getTncUnLoading());
		System.out.println("id"+termNCondition.getTncId());
		
		Map<String, String> map = new HashMap<String, String>();
		termNCondition.setTncContCode(contractCode);
		
		int temp = termNConditionDAO.updateTnC(termNCondition);

		if (temp >= 0) {
			map.put(ConstantsValues.RESULT+"Tnc", ConstantsValues.SUCCESS+"Tnc");
		} else {
			map.put(ConstantsValues.RESULT+"Tnc", ConstantsValues.ERROR+"Tnc");
		}
	return map;
}
	
	@RequestMapping(value = "/deleteRbkmForRC", method = RequestMethod.POST)
	public @ResponseBody Object deleteRbkmForRC(@RequestBody RateByKm rbkm) {

		System.out.println("enter into deleteRbkmForRC function----"+rbkm.getRbkmContCode());
		rateByKmDAO.deleteRBKM(rbkm.getRbkmContCode());

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/deletePbdForRC", method = RequestMethod.POST)
	public @ResponseBody Object deletePbdForRC(@RequestBody PenaltyBonusDetention penaltyBonusDetention) {

		System.out.println("enter into deletePbdForRC function----"+penaltyBonusDetention.getPbdContCode());
		penaltyBonusDetentionDAO.deletePBD(penaltyBonusDetention.getPbdContCode());

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/addNewRbkmForRC", method = RequestMethod.POST)
	public @ResponseBody Object addNewRbkmForRC(@RequestBody RateByKm rbkm) {

		System.out.println("enter into addNewRbkmForRC function");
		rbkmService.addRBKM(rbkm);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/fetchRbkmListForEditRC", method = RequestMethod.GET)
	public @ResponseBody Object fetchRbkmListForEditRC() {
		System.out.println("enter into fetchRbkmListForEditRC function");

		List<RateByKm> rateByKmList = rbkmService.getAllRBKM();

		Map<String, Object> map = new HashMap<String, Object>();
		if(!rateByKmList.isEmpty()){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("list", rateByKmList);	
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/removeNewRbkmForRC", method = RequestMethod.POST)
	public @ResponseBody Object removeNewRbkmForRC(@RequestBody RateByKm rbkm) {

		System.out.println("enter into removeNewRbkmForRC function");
		rbkmService.deleteRBKM(rbkm);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/fetchPbdListForEditRC", method = RequestMethod.GET)
	public @ResponseBody Object fetchPbdListForEditRC() {
		System.out.println("enter into fetchPbdListForEditRC function");

		List<PenaltyBonusDetention> pList = pbdService.getAllPBD();

		Map<String, Object> map = new HashMap<String, Object>();
		
		if(!pList.isEmpty()){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("list", pList);	
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/addNewPbdForRC", method = RequestMethod.POST)
	public @ResponseBody Object addNewPbdForRC(@RequestBody PenaltyBonusDetention penaltyBonusDetention) {

		System.out.println("enter into addNewPbdForRC function");
		pbdService.addPBD(penaltyBonusDetention);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeNewPbdForRC", method = RequestMethod.POST)
	public @ResponseBody Object removeNewPbdForRC(@RequestBody PenaltyBonusDetention penaltyBonusDetention) {

		System.out.println("enter into removeNewPbdForRC function");
		pbdService.deletePBD(penaltyBonusDetention);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeAllPbdForEditRC", method = RequestMethod.POST)
	public @ResponseBody Object removeAllPbdForEditRC() {

		System.out.println("enter into removeAllPbdForEditRC function");
		pbdService.deleteAllPBD();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeAllRbkmForEditRC", method = RequestMethod.POST)
	public @ResponseBody Object removeAllRbkmForEditRC() {

		System.out.println("enter into removeAllRbkmForEditRC function");
		rbkmService.deleteAllRBKM();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/getCTSDataForRC", method = RequestMethod.POST)
	public @ResponseBody Object getCTSDataForRC(@RequestBody String code) {

		System.out.println("Enter into getCTSDataForRC function-------"+code);
		List<ContToStn> contToStns = new ArrayList<ContToStn>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		contToStns = contToStnDAO.getContToStn(code);
		System.out.println("The size of cts list is ----"+contToStns.size());	
		
		if(!contToStns.isEmpty()){
			map.put("list", contToStns);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/deleteCTSForRC", method = RequestMethod.POST)
	public @ResponseBody Object deleteCTSForRC(@RequestBody ContToStn contToStn) {

		System.out.println("enter into deleteCTSForRC function----"+contToStn.getCtsContCode());
		contToStnDAO.deleteCTS(contToStn.getCtsContCode());
		System.out.println("Deleted successfully CTS");

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/fetchCTSListForEditRC", method = RequestMethod.GET)
	public @ResponseBody Object fetchCTSListForEditRC() {
		System.out.println("enter into fetchCTSListForEditRC function");

		List<ContToStn> cList = contToStnService.getAllCTS();

		Map<String, Object> map = new HashMap<String, Object>();
		if(!cList.isEmpty()){
			map.put("list", cList);	
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put("list", cList);	
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/addNewCTSForRC", method = RequestMethod.POST)
	public @ResponseBody Object addNewCTSForRC(@RequestBody ContToStn contToStn) {

		System.out.println("enter into addNewCTSForRC function");
		contToStnService.addCTS(contToStn);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeNewCTSForRC", method = RequestMethod.POST)
	public @ResponseBody Object removeNewCTSForRC(@RequestBody ContToStn contToStn) {

		System.out.println("enter into removeNewCTSForRC function");
		contToStnService.deleteCTS(contToStn);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/removeAllCTSForEditRC", method = RequestMethod.POST)
	public @ResponseBody Object removeAllCTSForEditRC() {

		System.out.println("enter into removeAllCTSForEditRC function");
		contToStnService.deleteAllCTS();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
	
	@RequestMapping(value = "/sendMetricTypeForEditRC", method = RequestMethod.POST)
	public @ResponseBody Object sendMetricTypeForEditRC(@RequestBody String metricType) {

		System.out.println("enter into sendMetricTypeForEditRC function");
		 metric = metricType;
		System.out.println("Metric type is ----"+metric);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}
}

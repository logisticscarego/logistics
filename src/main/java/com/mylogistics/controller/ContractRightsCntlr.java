package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.ContractRightsDAO;
import com.mylogistics.DAO.DailyContractDAO;
import com.mylogistics.DAO.RegularContractDAO;
import com.mylogistics.model.ContractRights;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class ContractRightsCntlr {
	
	@Autowired
	private DailyContractDAO dailyContractDAO;
	
	@Autowired
    private HttpSession httpSession;
	
	@Autowired
	private ContractRightsDAO contractRightsDAO;
	
	@Autowired
	private RegularContractDAO regularContractDAO;
	
		@RequestMapping(value = "/getContCodeForRights" , method = RequestMethod.POST)  
		public @ResponseBody Object getContCodeForRights(){
			
		System.out.println("Enter into getContCodeForRights function");	
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();
		List<String> dailyContractsList = dailyContractDAO.getDailyContractCodes(branchCode);
		List<String> rList = regularContractDAO.getRegularContractCodes(branchCode);
		List<String> strings = new ArrayList<String>();
		
		strings.addAll(dailyContractsList);
		strings.addAll(rList);
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		if(!strings.isEmpty()){
			map.put("list", strings);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
		} 
		
		@RequestMapping(value = "/contRightsDetails", method = RequestMethod.POST)
		public @ResponseBody Object contRightsDetails(@RequestBody String contCode) {
			System.out.println("Enter into contRightsDetails function");	
			ContractRights contractRights = new ContractRights();
			List<ContractRights> list = contractRightsDAO.getContRight(contCode);
			Map<String, Object> map = new HashMap<String, Object>();
			
			if (!list.isEmpty()) {
				contractRights = list.get(0);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				map.put("contractRights", contractRights);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;
		}	
		
		@RequestMapping(value = "/EditContRights", method = RequestMethod.POST)
		public @ResponseBody Object EditContRights(@RequestBody ContractRights contractRights) {
			System.out.println("Enter into EditContRights function"+contractRights.getCrContBillBasis());	
			
			int temp = contractRightsDAO.updateContractRights(contractRights);
			Map<String, Object> map = new HashMap<String, Object>();
			
			if (temp>0) {
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
			return map;
		}	
}

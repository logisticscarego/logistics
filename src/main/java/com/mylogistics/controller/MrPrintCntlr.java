package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.MoneyReceiptDAO;
import com.mylogistics.services.ConstantsValues;

@Controller
public class MrPrintCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private MoneyReceiptDAO moneyReceiptDAO;
	

	@RequestMapping(value = "/getMrBranch", method = RequestMethod.POST)
	public @ResponseBody Object getMrBranch() {
		System.out.println("enter into getMrBranch function");
		Map<String,Object> map = new HashMap<>();
		List<Map<String,Object>> brhList = branchDAO.getNameAndCode();
		if(!brhList.isEmpty()){
			map.put("list",brhList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/mrPrintSubmit", method = RequestMethod.POST)
	public @ResponseBody Object mrPrintSubmit(@RequestBody Map<String,Object> clientMap) {
		System.out.println("enter into mrPrintSubmit function");
		Map<String,Object> map = new HashMap<>();
		int brhId = Integer.parseInt(String.valueOf(clientMap.get("brhId")));
		String frMrNo = String.valueOf(clientMap.get("frMrNo"));
		String toMrNo = String.valueOf(clientMap.get("toMrNo"));
		
		if(brhId > 0){
			if(Integer.parseInt(frMrNo.substring(2)) > Integer.parseInt(toMrNo.substring(3))){
				map.put("msg","From M.R. No must be less than To M.R. No");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}else if(Integer.parseInt(frMrNo.substring(2)) > 0 && Integer.parseInt(toMrNo.substring(2)) > 0){
				List<Map<String,Object>> mrList = new ArrayList<>();
				mrList = moneyReceiptDAO.getMrFrPrint(brhId,frMrNo,toMrNo);
				if(!mrList.isEmpty()){
					System.out.println("size of blList = "+mrList.size());
					map.put("list",mrList);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put("msg","Branch Does not have any M.R.");
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put("msg","Invalid M.R. No");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put("msg","Invalid Branch");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
}

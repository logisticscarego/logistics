package com.mylogistics.pdf;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mylogistics.services.CodePatternService;

public class LedgerReportPDFBuilder extends AbstractITextPdfView{

	@SuppressWarnings({ "unused", "unchecked" })
	@Override
	protected void buildPdfDocument(Map<String, Object> map, Document doc,
			PdfWriter writer, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// get data model which is passed by the Spring container
		List<Map<String, Object>> ledgerMapList = (List<Map<String, Object>>) map.get("ledgerMapList");
		List<Map<String, String>> faCodeNameList = (List<Map<String, String>>) map.get("faCodeNameList");
		Map<String, Object> ledgerReportMap = (Map<String, Object>) map.get("ledgerReportMap");
		
		Paragraph paragraph = new Paragraph();
		
		Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		font.setSize(25);
		
		Chunk sigUnderline = new Chunk("LEDGER REPORT");
		sigUnderline.setUnderline(0.1f, -2f);
		    
		paragraph.setAlignment(Element.ALIGN_CENTER);
		paragraph.setFont(font);
		paragraph.setSpacingAfter(10);
		paragraph.add(sigUnderline);
		
		doc.add(paragraph);
		
		PdfPTable table = new PdfPTable(8);
		table.setWidthPercentage(100.0f);
		table.setWidths(new float[] {0.4f, 0.7f, 0.4f, 1.2f, 0.5f, 3.2f, 0.8f, 0.8f});
		table.setSpacingBefore(8);
		
		// define font for table header row
		Font headerfont = FontFactory.getFont(FontFactory.HELVETICA);
		headerfont.setSize(10);
		headerfont.setColor(BaseColor.WHITE);
		
		// define table header cell
		PdfPCell headerCell1 = new PdfPCell();
		headerCell1.setBackgroundColor(BaseColor.DARK_GRAY);
		headerCell1.setColspan(3);
		headerCell1.setPaddingTop(5);
		headerCell1.setPaddingBottom(5);
		headerCell1.setPaddingLeft(2);
		headerCell1.setPaddingRight(2);
		headerCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		PdfPCell headerCell2 = new PdfPCell();
		headerCell2.setBackgroundColor(BaseColor.DARK_GRAY);
		headerCell2.setColspan(2);
		headerCell2.setPaddingTop(5);
		headerCell2.setPaddingBottom(5);
		headerCell2.setPaddingLeft(2);
		headerCell2.setPaddingRight(2);
		headerCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell headerCell3 = new PdfPCell();
		headerCell3.setBackgroundColor(BaseColor.DARK_GRAY);
		headerCell3.setColspan(1);
		headerCell3.setPaddingTop(5);
		headerCell3.setPaddingBottom(5);
		headerCell3.setPaddingLeft(2);
		headerCell3.setPaddingRight(2);
		headerCell3.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell headerCell4 = new PdfPCell();
		headerCell4.setBackgroundColor(BaseColor.DARK_GRAY);
		headerCell4.setColspan(2);
		headerCell4.setPaddingTop(5);
		headerCell4.setPaddingBottom(5);
		headerCell4.setPaddingLeft(2);
		headerCell4.setPaddingRight(2);
		headerCell4.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		
		// write table header 
		headerCell1.setPhrase(new Phrase("FROM DT: "+CodePatternService.getFormatedDateString(String.valueOf(ledgerReportMap.get("fromDt"))), headerfont));
		table.addCell(headerCell1);
		headerCell2.setPhrase(new Phrase("TO DT: "+CodePatternService.getFormatedDateString((String.valueOf(ledgerReportMap.get("toDt")))), headerfont));
		table.addCell(headerCell2);
		headerCell3.setPhrase(new Phrase("FROM FA: "+ledgerReportMap.get("fromFaCode"), headerfont));
		table.addCell(headerCell3);
		headerCell4.setPhrase(new Phrase("TO FA: "+ledgerReportMap.get("toFaCode"), headerfont));
		table.addCell(headerCell4);
		
		//define cell for faCode
		PdfPCell cellFaCode = new PdfPCell();
		cellFaCode.setBackgroundColor(new BaseColor(38, 166, 154));
		cellFaCode.setColspan(4);
		cellFaCode.setPaddingTop(5);
		cellFaCode.setPaddingBottom(5);
		cellFaCode.setPaddingLeft(5);
		cellFaCode.setPaddingRight(5);
		cellFaCode.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		//define cell for faName
		PdfPCell cellFaName = new PdfPCell();
		cellFaName.setBackgroundColor(new BaseColor(38, 166, 154));
		cellFaName.setColspan(4);
		cellFaName.setPaddingTop(5);
		cellFaName.setPaddingBottom(5);
		cellFaName.setPaddingLeft(5);
		cellFaName.setPaddingRight(5);
		cellFaName.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		// define font for table header row
		Font faCodeNamefont = FontFactory.getFont(FontFactory.COURIER_BOLD);
		faCodeNamefont.setSize(12);
		faCodeNamefont.setColor(BaseColor.WHITE);
		
		PdfPCell cellCSHeader = new PdfPCell();
		cellCSHeader.setColspan(1);
		cellCSHeader.setBackgroundColor(BaseColor.LIGHT_GRAY);
		cellCSHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Font cellCSHeaderFont = FontFactory.getFont(FontFactory.TIMES);
		cellCSHeaderFont.setSize(9);
		cellCSHeaderFont.setColor(BaseColor.BLUE);
		
		Font cellCSFont = FontFactory.getFont(FontFactory.TIMES);
		cellCSFont.setSize(9);
		cellCSFont.setColor(BaseColor.BLACK);
		
		PdfPCell cellColSpan1 = new PdfPCell();
		cellColSpan1.setColspan(1);
		
		PdfPCell cellColSpan6 = new PdfPCell();
		cellColSpan6.setColspan(6);
		cellColSpan6.setBackgroundColor(BaseColor.LIGHT_GRAY);
		cellColSpan6.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Font cellFont8 = FontFactory.getFont(FontFactory.TIMES);
		cellFont8.setSize(10);
		cellFont8.setColor(new BaseColor(128,0,128));
		
		PdfPCell cellColSpan8 = new PdfPCell();
		cellColSpan8.setColspan(8);
		cellColSpan8.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		//TODO
		// write table row for FaCodeName
		for (Map<String, String> faCodeNameMap : faCodeNameList) {
			cellFaCode.setPhrase(new Phrase("FaCode: "+faCodeNameMap.get("csFaCode"), faCodeNamefont));
			table.addCell(cellFaCode);
			cellFaName.setPhrase(new Phrase("FaName: "+faCodeNameMap.get("csFaName"), faCodeNamefont));
			table.addCell(cellFaName);

			cellCSHeader.setPhrase(new Phrase("Bcd", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("Date", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("Vno", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("Tv No.", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("V Type", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("Perticular", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("Debit", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("Credit", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			
			for (Map<String,Object> ledgerMap : ledgerMapList) {
				if (String.valueOf(faCodeNameMap.get("csFaCode")).equalsIgnoreCase(String.valueOf(ledgerMap.get("csFaCode")))) {
					cellColSpan1.setPhrase(new Phrase(String.valueOf(ledgerMap.get("branchId")), cellCSFont));
					table.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(CodePatternService.getFormatedDateString(String.valueOf(ledgerMap.get("csDt"))), cellCSFont));
					table.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(ledgerMap.get("csVouchNo")), cellCSFont));
					table.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(ledgerMap.get("csTvNo")), cellCSFont));
					table.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(ledgerMap.get("csVouchType")), cellCSFont));
					table.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(ledgerMap.get("csDescription")), cellCSFont));
					table.addCell(cellColSpan1);
					
					if (String.valueOf(ledgerMap.get("csDrCr")).equalsIgnoreCase("D")) {
						cellColSpan1.setPhrase(new Phrase(String.valueOf(ledgerMap.get("csAmt")), cellCSFont));
						table.addCell(cellColSpan1);
						
						cellColSpan1.setPhrase(new Phrase("", cellCSFont));
						table.addCell(cellColSpan1);
					}else if (String.valueOf(ledgerMap.get("csDrCr")).equalsIgnoreCase("C")) {
						cellColSpan1.setPhrase(new Phrase("", cellCSFont));
						table.addCell(cellColSpan1);
						
						cellColSpan1.setPhrase(new Phrase(String.valueOf(ledgerMap.get("csAmt")), cellCSFont));
						table.addCell(cellColSpan1);
					}else {
						cellColSpan1.setPhrase(new Phrase("", cellCSFont));
						table.addCell(cellColSpan1);
						
						cellColSpan1.setPhrase(new Phrase("", cellCSFont));
						table.addCell(cellColSpan1);
					}
				}
			}
			
			cellColSpan6.setPhrase(new Phrase("Balance", cellCSFont));
			table.addCell(cellColSpan6);
			if (String.valueOf(faCodeNameMap.get("csDrCrBalance")).equalsIgnoreCase("D")) {
				cellCSHeader.setPhrase(new Phrase(String.valueOf(faCodeNameMap.get("csAmtBalance")), cellCSFont));
				table.addCell(cellCSHeader);
				
				cellCSHeader.setPhrase(new Phrase("", cellCSFont));
				table.addCell(cellCSHeader);
			}else if (String.valueOf(faCodeNameMap.get("csDrCrBalance")).equalsIgnoreCase("C")) {
				cellCSHeader.setPhrase(new Phrase("", cellCSFont));
				table.addCell(cellCSHeader);
				
				cellCSHeader.setPhrase(new Phrase(String.valueOf(faCodeNameMap.get("csAmtBalance")), cellCSFont));
				table.addCell(cellCSHeader);
			}else {
				cellCSHeader.setPhrase(new Phrase("", cellCSFont));
				table.addCell(cellCSHeader);
				
				cellCSHeader.setPhrase(new Phrase("", cellCSFont));
				table.addCell(cellCSHeader);
			}
			
			cellColSpan6.setPhrase(new Phrase("Total", cellCSHeaderFont));
			table.addCell(cellColSpan6);
			
			cellCSHeader.setPhrase(new Phrase(String.valueOf(faCodeNameMap.get("csAmtTotal")), cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase(String.valueOf(faCodeNameMap.get("csAmtTotal")), cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellColSpan8.setPhrase(new Phrase("********************************************", cellFont8));
			table.addCell(cellColSpan8);
			
		}
		doc.add(table);
	}

}

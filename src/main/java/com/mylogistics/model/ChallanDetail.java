package com.mylogistics.model;

import java.util.Calendar;
//import java.util.Date;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "challandetail")

public class ChallanDetail {

	@Id
	@Column(name="chdId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int chdId;
	
	@Column(name="chdCode")
	private String chdCode;
	
	@Column(name="chdBrCode")
	private String chdBrCode;
	
	@Column(name="chdOwnCode")
	private String chdOwnCode;
	
	@Column(name="chdBrMobNo")
	private String chdBrMobNo;
	
	@Column(name="chdOwnMobNo")
	private String chdOwnMobNo;
	
	@Column(name="chdDvrName")
	private String chdDvrName;
	
	@Column(name="chdDvrMobNo")
	private String chdDvrMobNo;
	
	@Column(name="chdRcNo")
	private String chdRcNo;
	
	@Column(name="chdRcIssueDt")
	private Date chdRcIssueDt;
	
	@Column(name="chdRcValidDt")
	private Date chdRcValidDt;
	
	@Column(name="chdDlNo")
	private String chdDlNo;
	
	@Column(name="chdDlIssueDt")
	private Date chdDlIssueDt;
	
	@Column(name="chdDlValidDt")
	private Date chdDlValidDt;
	
	@Column(name="chdPerNo")
	private String chdPerNo;
	
	@Column(name="chdPerIssueDt")
	private Date chdPerIssueDt;
	
	@Column(name="chdPerValidDt")
	private Date chdPerValidDt;
	
	@Column(name="chdPerState")
	private String chdPerState;
	
	@Column(name="chdFitDocNo")
	private String chdFitDocNo;
	
	@Column(name="chdFitDocIssueDt")
	private Date chdFitDocIssueDt;
	
	@Column(name="chdFitDocValidDt")
	private Date chdFitDocValidDt;
	
	@Column(name="chdFitDocPlace")
	private String chdFitDocPlace;
	
	@Column(name="chdBankFinance")
	private String chdBankFinance;
	
	@Column(name="chdPolicyNo")
	private String chdPolicyNo;
	
	@Column(name="chdPolicyCom")
	private String chdPolicyCom;
	
	@Column(name="chdTransitPassNo")
	private String chdTransitPassNo;
	
	@Column(name="chdPanHdrType")
	private String chdPanHdrType;
	
	@Column(name="chdPanNo")
	private String chdPanNo;
	
	@Column(name="chdPanHdrName")
	private String chdPanHdrName;
	
	@Column(name="chdPanIssueSt")
	private String chdPanIssueSt;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="isView")
	private boolean isView;
	
	@Column(name="chdChlnCode")
	private String chdChlnCode;
	
	@Column(name="chdValidPan",nullable = false,columnDefinition = "bit default false")
	private boolean chdValidPan;
	
	@Column(name="chdInvalidPan",nullable = false,columnDefinition = "bit default false")
	private boolean chdInvalidPan;
	
	@Column(name="chdEdit", columnDefinition="boolean default false")
	private boolean chdEdit;
	
	@Column(name="chdType", columnDefinition="varchar(255) default 'normal'")
	private String chdType;
	
	@Column(name="chdEngineNo")
	private String chdEngineNo;
	
	@Column(name="chdChassisNo")
	private String chdChassisNo;
	
	@Column(name="chdModel", columnDefinition="int(5) default '0000'")
	private int chdModel;
	
	@Transient
	private Date chdChlnDt;
	
	
	
	/*@Column(name="chdValidDec",nullable = false,columnDefinition = "bit default false")
	private boolean chdValidDec;
	
	@Column(name="chdInvalidDec",nullable = false,columnDefinition = "bit default false")
	private boolean chdInvalidDec;*/
	
	public String getChdChlnCode() {
		return chdChlnCode;
	}

	public void setChdChlnCode(String chdChlnCode) {
		this.chdChlnCode = chdChlnCode;
	}

	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}
	
	
	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
	
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public int getChdId() {
		return chdId;
	}

	public void setChdId(int chdId) {
		this.chdId = chdId;
	}

	public String getChdCode() {
		return chdCode;
	}

	public void setChdCode(String chdCode) {
		this.chdCode = chdCode;
	}

	public String getChdBrCode() {
		return chdBrCode;
	}

	public void setChdBrCode(String chdBrCode) {
		this.chdBrCode = chdBrCode;
	}

	public String getChdOwnCode() {
		return chdOwnCode;
	}

	public void setChdOwnCode(String chdOwnCode) {
		this.chdOwnCode = chdOwnCode;
	}

	public String getChdBrMobNo() {
		return chdBrMobNo;
	}

	public void setChdBrMobNo(String chdBrMobNo) {
		this.chdBrMobNo = chdBrMobNo;
	}

	public String getChdOwnMobNo() {
		return chdOwnMobNo;
	}

	public void setChdOwnMobNo(String chdOwnMobNo) {
		this.chdOwnMobNo = chdOwnMobNo;
	}

	public String getChdDvrName() {
		return chdDvrName;
	}

	public void setChdDvrName(String chdDvrName) {
		this.chdDvrName = chdDvrName;
	}

	public String getChdDvrMobNo() {
		return chdDvrMobNo;
	}

	public void setChdDvrMobNo(String chdDvrMobNo) {
		this.chdDvrMobNo = chdDvrMobNo;
	}

	public String getChdRcNo() {
		return chdRcNo;
	}

	public void setChdRcNo(String chdRcNo) {
		this.chdRcNo = chdRcNo;
	}

	public Date getChdRcIssueDt() {
		return chdRcIssueDt;
	}

	public void setChdRcIssueDt(Date chdRcIssueDt) {
		this.chdRcIssueDt = chdRcIssueDt;
	}

	public Date getChdRcValidDt() {
		return chdRcValidDt;
	}

	public void setChdRcValidDt(Date chdRcValidDt) {
		this.chdRcValidDt = chdRcValidDt;
	}

	public String getChdDlNo() {
		return chdDlNo;
	}

	public void setChdDlNo(String chdDlNo) {
		this.chdDlNo = chdDlNo;
	}

	public Date getChdDlIssueDt() {
		return chdDlIssueDt;
	}

	public void setChdDlIssueDt(Date chdDlIssueDt) {
		this.chdDlIssueDt = chdDlIssueDt;
	}

	public Date getChdDlValidDt() {
		return chdDlValidDt;
	}

	public void setChdDlValidDt(Date chdDlValidDt) {
		this.chdDlValidDt = chdDlValidDt;
	}

	public String getChdPerNo() {
		return chdPerNo;
	}

	public void setChdPerNo(String chdPerNo) {
		this.chdPerNo = chdPerNo;
	}

	public Date getChdPerIssueDt() {
		return chdPerIssueDt;
	}

	public void setChdPerIssueDt(Date chdPerIssueDt) {
		this.chdPerIssueDt = chdPerIssueDt;
	}

	public Date getChdPerValidDt() {
		return chdPerValidDt;
	}

	public void setChdPerValidDt(Date chdPerValidDt) {
		this.chdPerValidDt = chdPerValidDt;
	}

	public String getChdPerState() {
		return chdPerState;
	}

	public void setChdPerState(String chdPerState) {
		this.chdPerState = chdPerState;
	}

	public String getChdFitDocNo() {
		return chdFitDocNo;
	}

	public void setChdFitDocNo(String chdFitDocNo) {
		this.chdFitDocNo = chdFitDocNo;
	}

	public Date getChdFitDocIssueDt() {
		return chdFitDocIssueDt;
	}

	public void setChdFitDocIssueDt(Date chdFitDocIssueDt) {
		this.chdFitDocIssueDt = chdFitDocIssueDt;
	}

	public Date getChdFitDocValidDt() {
		return chdFitDocValidDt;
	}

	public void setChdFitDocValidDt(Date chdFitDocValidDt) {
		this.chdFitDocValidDt = chdFitDocValidDt;
	}

	public String getChdFitDocPlace() {
		return chdFitDocPlace;
	}

	public void setChdFitDocPlace(String chdFitDocPlace) {
		this.chdFitDocPlace = chdFitDocPlace;
	}

	public String getChdBankFinance() {
		return chdBankFinance;
	}

	public void setChdBankFinance(String chdBankFinance) {
		this.chdBankFinance = chdBankFinance;
	}

	public String getChdPolicyNo() {
		return chdPolicyNo;
	}

	public void setChdPolicyNo(String chdPolicyNo) {
		this.chdPolicyNo = chdPolicyNo;
	}

	public String getChdPolicyCom() {
		return chdPolicyCom;
	}

	public void setChdPolicyCom(String chdPolicyCom) {
		this.chdPolicyCom = chdPolicyCom;
	}

	public String getChdTransitPassNo() {
		return chdTransitPassNo;
	}

	public void setChdTransitPassNo(String chdTransitPassNo) {
		this.chdTransitPassNo = chdTransitPassNo;
	}

	public String getChdPanHdrType() {
		return chdPanHdrType;
	}

	public void setChdPanHdrType(String chdPanHdrType) {
		this.chdPanHdrType = chdPanHdrType;
	}

	public String getChdPanNo() {
		return chdPanNo;
	}

	public void setChdPanNo(String chdPanNo) {
		this.chdPanNo = chdPanNo;
	}

	public String getChdPanHdrName() {
		return chdPanHdrName;
	}

	public void setChdPanHdrName(String chdPanHdrName) {
		this.chdPanHdrName = chdPanHdrName;
	}

	public String getChdPanIssueSt() {
		return chdPanIssueSt;
	}

	public void setChdPanIssueSt(String chdPanIssueSt) {
		this.chdPanIssueSt = chdPanIssueSt;
	}

	public boolean isChdValidPan() {
		return chdValidPan;
	}

	public void setChdValidPan(boolean chdValidPan) {
		this.chdValidPan = chdValidPan;
	}

	public boolean isChdInvalidPan() {
		return chdInvalidPan;
	}

	public void setChdInvalidPan(boolean chdInvalidPan) {
		this.chdInvalidPan = chdInvalidPan;
	}

	public boolean isChdEdit() {
		return chdEdit;
	}

	public void setChdEdit(boolean chdEdit) {
		this.chdEdit = chdEdit;
	}

	public Date getChdChlnDt() {
		return chdChlnDt;
	}

	public void setChdChlnDt(Date chdChlnDt) {
		this.chdChlnDt = chdChlnDt;
	}

	public String getChdType() {
		return chdType;
	}

	public void setChdType(String chdType) {
		this.chdType = chdType;
	}

	public String getChdEngineNo() {
		return chdEngineNo;
	}

	public void setChdEngineNo(String chdEngineNo) {
		this.chdEngineNo = chdEngineNo;
	}

	public String getChdChassisNo() {
		return chdChassisNo;
	}

	public void setChdChassisNo(String chdChassisNo) {
		this.chdChassisNo = chdChassisNo;
	}

	public int getChdModel() {
		return chdModel;
	}

	public void setChdModel(int chdModel) {
		this.chdModel = chdModel;
	}
	
	
	
}

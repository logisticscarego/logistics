package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "facardivision")
public class FaCarDivision {

	@Id
	@Column(name="fcdId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int fcdId;
	
	@Column(name="fcdSvmId")
	private int fcdSvmId;
	
	@Column(name="fcdExpType")
	private String fcdExpType;
	
	@Column(name="fcdExpAmt")
	private double fcdExpAmt;
	
	@Column(name="fcdReading")
	private double fcdReading;
	
	@Column(name="fcdFuel")
	private double fcdFuel;
	
	@Column(name="fcdInsFrDt")
	private Date fcdInsFrDt;
	
	@Column(name="fcdInsToDt")
	private Date fcdInsToDt;
	
	@Column(name="fcdDesc" , columnDefinition="TEXT")
	private String fcdDesc;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getFcdId() {
		return fcdId;
	}

	public void setFcdId(int fcdId) {
		this.fcdId = fcdId;
	}

	public int getFcdSvmId() {
		return fcdSvmId;
	}

	public void setFcdSvmId(int fcdSvmId) {
		this.fcdSvmId = fcdSvmId;
	}

	public String getFcdExpType() {
		return fcdExpType;
	}

	public void setFcdExpType(String fcdExpType) {
		this.fcdExpType = fcdExpType;
	}

	public double getFcdExpAmt() {
		return fcdExpAmt;
	}

	public void setFcdExpAmt(double fcdExpAmt) {
		this.fcdExpAmt = fcdExpAmt;
	}

	public double getFcdReading() {
		return fcdReading;
	}

	public void setFcdReading(double fcdReading) {
		this.fcdReading = fcdReading;
	}

	public double getFcdFuel() {
		return fcdFuel;
	}

	public void setFcdFuel(double fcdFuel) {
		this.fcdFuel = fcdFuel;
	}

	public Date getFcdInsFrDt() {
		return fcdInsFrDt;
	}

	public void setFcdInsFrDt(Date fcdInsFrDt) {
		this.fcdInsFrDt = fcdInsFrDt;
	}

	public Date getFcdInsToDt() {
		return fcdInsToDt;
	}

	public void setFcdInsToDt(Date fcdInsToDt) {
		this.fcdInsToDt = fcdInsToDt;
	}

	public String getFcdDesc() {
		return fcdDesc;
	}

	public void setFcdDesc(String fcdDesc) {
		this.fcdDesc = fcdDesc;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
	
}

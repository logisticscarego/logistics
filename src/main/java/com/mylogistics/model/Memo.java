package com.mylogistics.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "memo")
public class Memo {
	
	@Id@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "memoId")
	public Integer memoId;
	
	@Column(name = "memoNo", length = 8)
	public String memoNo;
	
	@Column(name = "memoDate")
	public Date memoDate;
	
	@Column(name = "fromBranch", length = 25)
	public String fromBranch;
	
	@Column(name = "toBranch", length = 25)
	public String toBranch;
	
	@Column(name = "branchCode", length = 2)
	public String branchCode;
	
	@Column(name = "userCode", length = 3)
	public String userCode;
	
	@Column(name = "remark", length = 100)
	public String remark;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonIgnore
	List<ArrivalReport> arrivalReports = new ArrayList<ArrivalReport>();
	
	@Column(name = "creationTS", insertable = false, updatable = true)
	public Calendar creationTS;

	public Integer getMemoId() {
		return memoId;
	}

	public void setMemoId(Integer memoId) {
		this.memoId = memoId;
	}

	public String getMemoNo() {
		return memoNo;
	}

	public void setMemoNo(String memoNo) {
		this.memoNo = memoNo;
	}

	public Date getMemoDate() {
		return memoDate;
	}

	public void setMemoDate(Date memoDate) {
		this.memoDate = memoDate;
	}

	public String getFromBranch() {
		return fromBranch;
	}

	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}

	public String getToBranch() {
		return toBranch;
	}

	public void setToBranch(String toBranch) {
		this.toBranch = toBranch;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public List<ArrivalReport> getArrivalReports() {
		return arrivalReports;
	}

	public void setArrivalReports(List<ArrivalReport> arrivalReports) {
		this.arrivalReports = arrivalReports;
	}
	
}
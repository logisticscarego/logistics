package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "stationarysupplier")
public class StationarySupplier{
	
	@Id
	@Column(name="stSupID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int stSupID;
	
	@Column(name="stSupCode")
	private String stSupCode;
	
	@Column(name="stSupName")
	private String stSupName;
	
	@Column(name="stSupAdd")
	private String stSupAdd;
	
	@Column(name="stSupCity")
	private String stSupCity;
	
	@Column(name="stSupState")
	private String stSupState;
	
	@Column(name="stSupPin")
	private String stSupPin;
	
	@Column(name="stSupPanNo")
	private String stSupPanNo;
	
	@Column(name="stSupTdsCode")
	private String stSupTdsCode;
	
	@Column(name="stSupSubGroup")
	private String stSupSubGroup;
	
	@Column(name="stSupGroup")
	private String stSupGroup;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="isView")
	private boolean isView;
	
	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}


	public int getStSupID() {
		return stSupID;
	}

	public void setStSupID(int stSupID) {
		this.stSupID = stSupID;
	}

	public String getStSupCode() {
		return stSupCode;
	}

	public void setStSupCode(String stSupCode) {
		this.stSupCode = stSupCode;
	}

	public String getStSupName() {
		return stSupName;
	}

	public void setStSupName(String stSupName) {
		this.stSupName = stSupName;
	}

	public String getStSupAdd() {
		return stSupAdd;
	}

	public void setStSupAdd(String stSupAdd) {
		this.stSupAdd = stSupAdd;
	}

	public String getStSupCity() {
		return stSupCity;
	}

	public void setStSupCity(String stSupCity) {
		this.stSupCity = stSupCity;
	}

	public String getStSupState() {
		return stSupState;
	}

	public void setStSupState(String stSupState) {
		this.stSupState = stSupState;
	}

	public String getStSupPin() {
		return stSupPin;
	}

	public void setStSupPin(String stSupPin) {
		this.stSupPin = stSupPin;
	}

	public String getStSupPanNo() {
		return stSupPanNo;
	}

	public void setStSupPanNo(String stSupPanNo) {
		this.stSupPanNo = stSupPanNo;
	}

	public String getStSupTdsCode() {
		return stSupTdsCode;
	}

	public void setStSupTdsCode(String stSupTdsCode) {
		this.stSupTdsCode = stSupTdsCode;
	}

	public String getStSupSubGroup() {
		return stSupSubGroup;
	}

	public void setStSupSubGroup(String stSupSubGroup) {
		this.stSupSubGroup = stSupSubGroup;
	}

	public String getStSupGroup() {
		return stSupGroup;
	}

	public void setStSupGroup(String stSupGroup) {
		this.stSupGroup = stSupGroup;
	}

}

package com.mylogistics.model;

import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="acknowledgevalidation")
public class Acknowledge_Validation {
	
	@Id
	@GeneratedValue
	@Column(name="AckId")
	private int ackId;
	

	private int bcode;
    
	private String cnmtCode;
	
	private String cnmtPdfName;
	
	private Date scandate;
	
	@Column(name="isChacked")
	private String ischeck;
	
	private String remarks;
	
	private String reason;
	
	private Date cnmtDate;
	
	private int cngeCode;
	
	private int cngrCode;
	
	private int daysdiff;
	
	private String sedrNo;
	
	private Date arDate;
	
	private String chlnNo;
	
	private Time lrArrivalTime;
	
	private Date lrArrivalDate;
	
	private Time unloadingTime;
	
	private double unloadingAmt;
	
	private Date lrUnloadingDt;
	
	private double detention;
	
	private double otherAmt;
	
	private double recWeight;
	
	private int recPackage;
	
	private String dmOrShrtg;
	
	private String weightOrPkg;
	
	private int claimPkg;
	
	private double claimWeight;
	
	private double claimAmtVOG;
	
	private String newRemarks;
	
	private String newReason;
	
	public void setNewReason(String newReason) {
		this.newReason = newReason;
	}
	public String getNewReason() {
		return newReason;
	}
	
	public void setNewRemarks(String newRemarks) {
		this.newRemarks = newRemarks;
	}
	
	public String getNewRemarks() {
		return newRemarks;
	}
	
	public Date getScandate() {
		return scandate;
	}

	public void setScandate(Date scandate) {
		this.scandate = scandate;
	}

	public Date getCnmtDate() {
		return cnmtDate;
	}

	public void setCnmtDate(Date cnmtDate) {
		this.cnmtDate = cnmtDate;
	}

	public int getCngeCode() {
		return cngeCode;
	}

	public void setCngeCode(int cngeCode) {
		this.cngeCode = cngeCode;
	}

	public int getCngrCode() {
		return cngrCode;
	}

	public void setCngrCode(int cngrCode) {
		this.cngrCode = cngrCode;
	}

	public int getDaysdiff() {
		return daysdiff;
	}

	public void setDaysdiff(int daysdiff) {
		this.daysdiff = daysdiff;
	}

	public String getSedrNo() {
		return sedrNo;
	}

	public void setSedrNo(String sedrNo) {
		this.sedrNo = sedrNo;
	}

	public Date getArDate() {
		return arDate;
	}

	public void setArDate(Date arDate) {
		this.arDate = arDate;
	}

	public String getChlnNo() {
		return chlnNo;
	}

	public void setChlnNo(String chlnNo) {
		this.chlnNo = chlnNo;
	}

	public Time getLrArrivalTime() {
		return lrArrivalTime;
	}

	public void setLrArrivalTime(Time lrArrivalTime) {
		this.lrArrivalTime = lrArrivalTime;
	}

	public Date getLrArrivalDate() {
		return lrArrivalDate;
	}

	public void setLrArrivalDate(Date lrArrivalDate) {
		this.lrArrivalDate = lrArrivalDate;
	}

	public Time getUnloadingTime() {
		return unloadingTime;
	}

	public void setUnloadingTime(Time unloadingTime) {
		this.unloadingTime = unloadingTime;
	}

	public double getUnloadingAmt() {
		return unloadingAmt;
	}

	public void setUnloadingAmt(double unloadingAmt) {
		this.unloadingAmt = unloadingAmt;
	}

	public Date getLrUnloadingDt() {
		return lrUnloadingDt;
	}

	public void setLrUnloadingDt(Date lrUnloadingDt) {
		this.lrUnloadingDt = lrUnloadingDt;
	}

	public double getDetention() {
		return detention;
	}

	public void setDetention(double detention) {
		this.detention = detention;
	}

	public double getOtherAmt() {
		return otherAmt;
	}

	public void setOtherAmt(double otherAmt) {
		this.otherAmt = otherAmt;
	}

	public double getRecWeight() {
		return recWeight;
	}

	public void setRecWeight(double recWeight) {
		this.recWeight = recWeight;
	}

	public int getRecPackage() {
		return recPackage;
	}

	public void setRecPackage(int recPackage) {
		this.recPackage = recPackage;
	}

	public String getDmOrShrtg() {
		return dmOrShrtg;
	}

	public void setDmOrShrtg(String dmOrShrtg) {
		this.dmOrShrtg = dmOrShrtg;
	}

	public String getWeightOrPkg() {
		return weightOrPkg;
	}

	public void setWeightOrPkg(String weightOrPkg) {
		this.weightOrPkg = weightOrPkg;
	}

	public int getClaimPkg() {
		return claimPkg;
	}

	public void setClaimPkg(int claimPkg) {
		this.claimPkg = claimPkg;
	}

	public double getClaimWeight() {
		return claimWeight;
	}

	public void setClaimWeight(double claimWeight) {
		this.claimWeight = claimWeight;
	}

	public double getClaimAmtVOG() {
		return claimAmtVOG;
	}

	public void setClaimAmtVOG(double claimAmtVOG) {
		this.claimAmtVOG = claimAmtVOG;
	}

	public int getAckId() {
		return ackId;
	}

	public void setAckId(int ackId) {
		this.ackId = ackId;
	}

	public int getBcode() {
		return bcode;
	}

	public void setBcode(int bcode) {
		this.bcode = bcode;
	}

	public String getCnmtCode() {
		return cnmtCode;
	}

	public void setCnmtCode(String cnmtCode) {
		this.cnmtCode = cnmtCode;
	}

	public String getCnmtPdfName() {
		return cnmtPdfName;
	}

	public void setCnmtPdfName(String cnmtPdfName) {
		this.cnmtPdfName = cnmtPdfName;
	}

	public Date getScanDate() {
		return scandate;
	}

	public void setScanDate(Date scandate) {
		this.scandate = scandate;
	}

	public String getCheck() {
		return ischeck;
	}

	public void setCheck(String check) {
		this.ischeck = check;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
}

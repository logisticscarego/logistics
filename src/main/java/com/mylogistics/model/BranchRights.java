package com.mylogistics.model;

import java.util.Calendar;
//import java.util.Date;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "branchrights")
public class BranchRights {

	@Id
	@Column(name="brhId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int brhId;
	
	@Column(name="brhBranchCode")
	private String brhBranchCode;
	
	@Column(name="brhAdvAllow")
	private String brhAdvAllow;
	
	@Column(name="brhAdvValidDt")
	private Date brhAdvValidDt;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getBrhId() {
		return brhId;
	}

	public void setBrhId(int brhId) {
		this.brhId = brhId;
	}

	public String getBrhBranchCode() {
		return brhBranchCode;
	}

	public void setBrhBranchCode(String brhBranchCode) {
		this.brhBranchCode = brhBranchCode;
	}

	public String getBrhAdvAllow() {
		return brhAdvAllow;
	}

	public void setBrhAdvAllow(String brhAdvAllow) {
		this.brhAdvAllow = brhAdvAllow;
	}

	public Date getBrhAdvValidDt() {
		return brhAdvValidDt;
	}

	public void setBrhAdvValidDt(Date brhAdvValidDt) {
		this.brhAdvValidDt = brhAdvValidDt;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
	
	
}

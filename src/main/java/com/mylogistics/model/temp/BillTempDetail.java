package com.mylogistics.model.temp;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mylogistics.model.Branch;

@Entity
@Table(name="billtempdetail")
public class BillTempDetail {

	@Id
	@Column(name="btdId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int btdId;
	
	@Column(name="btdDate")
	private Date btdDate;
	
	@Column(name="btdCnmtNo")
	private String btdCnmtNo;
	
	@Column(name="btdFromStn")
	private String btdFromStn;
	
	@Column(name="btdToStn")
	private String btdToStn;
	
	@Column(name="btdWeightMt")
	private double btdWeightMt;
	
	@Column(name="btdRate")
	private double btdRate;
	
	@Column(name="btdFreight")
	private double btdFreight;
	
	@Column(name="btdDetention")
	private double btdDetention;
	
	@Column(name="btdBonus")
	private double btdBonus;
	
	@Column(name="btdLdngNUnldng")
	private double btdLdngNUnldng;
	
	@Column(name="btdTotal")
	private double btdTotal;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@ManyToOne
	@JoinColumn(name = "btmId") 
	private BillTempMain billTempMain;
	
	public double getBtdTotal() {
		return btdTotal;
	}

	public void setBtdTotal(double btdTotal) {
		this.btdTotal = btdTotal;
	}

	public int getBtdId() {
		return btdId;
	}

	public void setBtdId(int btdId) {
		this.btdId = btdId;
	}

	public Date getBtdDate() {
		return btdDate;
	}

	public void setBtdDate(Date btdDate) {
		this.btdDate = btdDate;
	}

	public String getBtdCnmtNo() {
		return btdCnmtNo;
	}

	public void setBtdCnmtNo(String btdCnmtNo) {
		this.btdCnmtNo = btdCnmtNo;
	}

	public String getBtdFromStn() {
		return btdFromStn;
	}

	public void setBtdFromStn(String btdFromStn) {
		this.btdFromStn = btdFromStn;
	}

	public String getBtdToStn() {
		return btdToStn;
	}

	public void setBtdToStn(String btdToStn) {
		this.btdToStn = btdToStn;
	}

	public double getBtdWeightMt() {
		return btdWeightMt;
	}

	public void setBtdWeightMt(double btdWeightMt) {
		this.btdWeightMt = btdWeightMt;
	}

	public double getBtdRate() {
		return btdRate;
	}

	public void setBtdRate(double btdRate) {
		this.btdRate = btdRate;
	}

	public double getBtdFreight() {
		return btdFreight;
	}

	public void setBtdFreight(double btdFreight) {
		this.btdFreight = btdFreight;
	}

	public double getBtdDetention() {
		return btdDetention;
	}

	public void setBtdDetention(double btdDetention) {
		this.btdDetention = btdDetention;
	}

	public double getBtdBonus() {
		return btdBonus;
	}

	public void setBtdBonus(double btdBonus) {
		this.btdBonus = btdBonus;
	}

	public double getBtdLdngNUnldng() {
		return btdLdngNUnldng;
	}

	public void setBtdLdngNUnldng(double btdLdngNUnldng) {
		this.btdLdngNUnldng = btdLdngNUnldng;
	}

	
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public BillTempMain getBillTempMain() {
		return billTempMain;
	}

	public void setBillTempMain(BillTempMain billTempMain) {
		this.billTempMain = billTempMain;
	}
	
	
	
}

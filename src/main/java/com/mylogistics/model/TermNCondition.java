package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "termncondition")
public class TermNCondition {

	@Id
	@Column(name="tncId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int tncId;
	
	@Column(name="tncContCode")
	private String tncContCode;
	
	@Column(name="tncPenalty")
	private String tncPenalty;
	
	@Column(name="tncLoading")
	private String tncLoading;
	
	@Column(name="tncUnLoading")
	private String tncUnLoading;
	
	@Column(name="tncDetention")
	private String tncDetention;
	
	@Column(name="tncBonus")
	private String tncBonus;
	
	@Column(name="tncTransitDay")
	private String tncTransitDay;
	
	@Column(name="tncStatisticalCharge")
	private String tncStatisticalCharge;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	@Column(name="bCode")
	private String bCode;
	
	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}


	public int getTncId() {
		return tncId;
	}

	public void setTncId(int tncId) {
		this.tncId = tncId;
	}

	public String getTncContCode() {
		return tncContCode;
	}

	public void setTncContCode(String tncContCode) {
		this.tncContCode = tncContCode;
	}

	public String getTncPenalty() {
		return tncPenalty;
	}

	public void setTncPenalty(String tncPenalty) {
		this.tncPenalty = tncPenalty;
	}

	public String getTncLoading() {
		return tncLoading;
	}

	public void setTncLoading(String tncLoading) {
		this.tncLoading = tncLoading;
	}

	public String getTncUnLoading() {
		return tncUnLoading;
	}

	public void setTncUnLoading(String tncUnLoading) {
		this.tncUnLoading = tncUnLoading;
	}

	public String getTncDetention() {
		return tncDetention;
	}

	public void setTncDetention(String tncDetention) {
		this.tncDetention = tncDetention;
	}

	public String getTncBonus() {
		return tncBonus;
	}

	public void setTncBonus(String tncBonus) {
		this.tncBonus = tncBonus;
	}

	public String getTncTransitDay() {
		return tncTransitDay;
	}

	public void setTncTransitDay(String tncTransitDay) {
		this.tncTransitDay = tncTransitDay;
	}

	public String getTncStatisticalCharge() {
		return tncStatisticalCharge;
	}

	public void setTncStatisticalCharge(String tncStatisticalCharge) {
		this.tncStatisticalCharge = tncStatisticalCharge;
	}
}

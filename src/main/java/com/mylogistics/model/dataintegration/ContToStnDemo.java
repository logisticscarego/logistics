package com.mylogistics.model.dataintegration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="conttostndemo")
public class ContToStnDemo {

	
	@Id
	@Column(name="ctsId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ctsId;
	
	@Column(name="ctsStnName")
	private String ctsStnName;
	
	@Column(name="ctsStnPinNo")
	private String ctsStnPinNo;
	
	@Column(name="ctsRate")
	private double ctsRate;

	public int getCtsId() {
		return ctsId;
	}

	public void setCtsId(int ctsId) {
		this.ctsId = ctsId;
	}

	public String getCtsStnName() {
		return ctsStnName;
	}

	public void setCtsStnName(String ctsStnName) {
		this.ctsStnName = ctsStnName;
	}

	public String getCtsStnPinNo() {
		return ctsStnPinNo;
	}

	public void setCtsStnPinNo(String ctsStnPinNo) {
		this.ctsStnPinNo = ctsStnPinNo;
	}

	public double getCtsRate() {
		return ctsRate;
	}

	public void setCtsRate(double ctsRate) {
		this.ctsRate = ctsRate;
	}
	
}

package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "chblardetail")
public class ChBlArDetail {
	
	@Id
	@Column(name="chBlArId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int chBlArId;
	
	@Column(name="chBlArCode")
	private String chBlArCode;
	
	@Column(name="chBlArstOdrCode")
	private String chBlArstOdrCode;
	
	@Column(name="chBlArStartNo")
	private int chBlArStartNo;
	
	@Column(name="chBlArNOB")
	private int chBlArNOB;
	
	@Column(name="chBlArEndNo")
	private int chBlArEndNo;
	
	@Column(name="chBlArStatus")
	private int chBlArStatus;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;

	public int getChBlArId() {
		return chBlArId;
	}

	public void setChBlArId(int chBlArId) {
		this.chBlArId = chBlArId;
	}

	public String getChBlArCode() {
		return chBlArCode;
	}

	public void setChBlArCode(String chBlArCode) {
		this.chBlArCode = chBlArCode;
	}

	public String getChBlArstOdrCode() {
		return chBlArstOdrCode;
	}

	public void setChBlArstOdrCode(String chBlArstOdrCode) {
		this.chBlArstOdrCode = chBlArstOdrCode;
	}

	public int getChBlArStartNo() {
		return chBlArStartNo;
	}

	public void setChBlArStartNo(int chBlArStartNo) {
		this.chBlArStartNo = chBlArStartNo;
	}

	public int getChBlArNOB() {
		return chBlArNOB;
	}

	public void setChBlArNOB(int chBlArNOB) {
		this.chBlArNOB = chBlArNOB;
	}

	public int getChBlArEndNo() {
		return chBlArEndNo;
	}

	public void setChBlArEndNo(int chBlArEndNo) {
		this.chBlArEndNo = chBlArEndNo;
	}

	public int getChBlArStatus() {
		return chBlArStatus;
	}

	public void setChBlArStatus(int chBlArStatus) {
		this.chBlArStatus = chBlArStatus;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}
	
	
}

package com.mylogistics.model;

import java.util.ArrayList;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="lhpvtemp")
public class LhpvTemp {

	@Id
	@Column(name="ltId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ltId;
	
	@Column(name="ltLhpvNo")
	private String ltLhpvNo;
	
	@Column(name="ltTransType")
	private String ltTransType;
	
	@Column(name="ltBankCode")
	private String ltBankCode;
	
	@Column(name="ltChqNo")
	private String ltChqNo;
	
	@Column(name="ltAdv")
	private double ltAdv;

	@Column(name="ltBal")
	private double ltBal;
	
	@Column(name="ltDet")
	private double ltDet;

	@Column(name="ltUnload")
	private double ltUnload;
	
	@Column(name="ltOth")
	private double ltOth;
	
	@Column(name="ltTds")
	private double ltTds;
	
	@Column(name="ltCashDis")
	private double ltCashDis;
	
	@Column(name="ltMuns")
	private double ltMuns;
	
	@Column(name="ltClaim")
	private double ltClaim;
	
	@Column(name="ltFinalAmt")
	private double ltFinalAmt;
	
	@Column(name="ltStart", columnDefinition="boolean default false")
	private boolean ltStart;
	
	@Column(name="ltEnd", columnDefinition="boolean default false")
	private boolean ltEnd;
	
	@Column(name="ltChlnList")
	private ArrayList<String> ltChlnList = new ArrayList<>() ;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;

	public int getLtId() {
		return ltId;
	}

	public void setLtId(int ltId) {
		this.ltId = ltId;
	}

	public String getLtLhpvNo() {
		return ltLhpvNo;
	}

	public void setLtLhpvNo(String ltLhpvNo) {
		this.ltLhpvNo = ltLhpvNo;
	}

	public String getLtTransType() {
		return ltTransType;
	}

	public void setLtTransType(String ltTransType) {
		this.ltTransType = ltTransType;
	}

	public String getLtChqNo() {
		return ltChqNo;
	}

	public void setLtChqNo(String ltChqNo) {
		this.ltChqNo = ltChqNo;
	}

	public double getLtAdv() {
		return ltAdv;
	}

	public void setLtAdv(double ltAdv) {
		this.ltAdv = ltAdv;
	}

	public double getLtBal() {
		return ltBal;
	}

	public void setLtBal(double ltBal) {
		this.ltBal = ltBal;
	}

	public double getLtDet() {
		return ltDet;
	}

	public void setLtDet(double ltDet) {
		this.ltDet = ltDet;
	}

	public double getLtUnload() {
		return ltUnload;
	}

	public void setLtUnload(double ltUnload) {
		this.ltUnload = ltUnload;
	}

	public double getLtOth() {
		return ltOth;
	}

	public void setLtOth(double ltOth) {
		this.ltOth = ltOth;
	}

	public double getLtTds() {
		return ltTds;
	}

	public void setLtTds(double ltTds) {
		this.ltTds = ltTds;
	}

	public double getLtCashDis() {
		return ltCashDis;
	}

	public void setLtCashDis(double ltCashDis) {
		this.ltCashDis = ltCashDis;
	}

	public double getLtMuns() {
		return ltMuns;
	}

	public void setLtMuns(double ltMuns) {
		this.ltMuns = ltMuns;
	}

	public double getLtClaim() {
		return ltClaim;
	}

	public void setLtClaim(double ltClaim) {
		this.ltClaim = ltClaim;
	}

	public double getLtFinalAmt() {
		return ltFinalAmt;
	}

	public void setLtFinalAmt(double ltFinalAmt) {
		this.ltFinalAmt = ltFinalAmt;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public ArrayList<String> getLtChlnList() {
		return ltChlnList;
	}

	public void setLtChlnList(ArrayList<String> ltChlnList) {
		this.ltChlnList = ltChlnList;
	}

	public String getLtBankCode() {
		return ltBankCode;
	}

	public void setLtBankCode(String ltBankCode) {
		this.ltBankCode = ltBankCode;
	}

	public boolean isLtStart() {
		return ltStart;
	}

	public void setLtStart(boolean ltStart) {
		this.ltStart = ltStart;
	}

	public boolean isLtEnd() {
		return ltEnd;
	}

	public void setLtEnd(boolean ltEnd) {
		this.ltEnd = ltEnd;
	}
	
	
}

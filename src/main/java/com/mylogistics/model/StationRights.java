package com.mylogistics.model;

import java.util.Calendar;
//import java.util.Date;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stationrights")
public class StationRights {
	
	@Id
	@Column(name="strhId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int strhId;
	
	@Column(name="strhStCode")
	private String strhStCode;
	
	@Column(name="strhAdvAllow")
	private String strhAdvAllow;
	
	@Column(name="strhIsFromSt")
	private String strhIsFromSt;
	
	@Column(name="strhIsToSt")
	private String strhIsToSt;
	
	@Column(name="strhAdvValidDt")
	private Date strhAdvValidDt;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getStrhId() {
		return strhId;
	}

	public void setStrhId(int strhId) {
		this.strhId = strhId;
	}

	public String getStrhStCode() {
		return strhStCode;
	}

	public void setStrhStCode(String strhStCode) {
		this.strhStCode = strhStCode;
	}

	public String getStrhAdvAllow() {
		return strhAdvAllow;
	}

	public void setStrhAdvAllow(String strhAdvAllow) {
		this.strhAdvAllow = strhAdvAllow;
	}

	public String getStrhIsFromSt() {
		return strhIsFromSt;
	}

	public void setStrhIsFromSt(String strhIsFromSt) {
		this.strhIsFromSt = strhIsFromSt;
	}

	public String getStrhIsToSt() {
		return strhIsToSt;
	}

	public void setStrhIsToSt(String strhIsToSt) {
		this.strhIsToSt = strhIsToSt;
	}

	public Date getStrhAdvValidDt() {
		return strhAdvValidDt;
	}

	public void setStrhAdvValidDt(Date strhAdvValidDt) {
		this.strhAdvValidDt = strhAdvValidDt;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
}

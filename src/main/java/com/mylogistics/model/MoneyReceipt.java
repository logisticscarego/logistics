package com.mylogistics.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "moneyreceipt")
public class MoneyReceipt implements Serializable{

	@Id
	@Column(name="mrId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int mrId;
	
	@Column(name="mrNo")
	private String mrNo;
	
	@Column(name="mrCancel", columnDefinition="boolean default false")
	private boolean mrCancel;
	
	@Column(name="mrSrtExsDis", columnDefinition="boolean default false")
	private boolean mrSrtExsDis;
	
	@Column(name="mrFrPD" , columnDefinition="longblob")
	private ArrayList<Map<String,Object>> mrFrPDList = new ArrayList<>();
	
	@Column(name="mrPayBy")
	private char mrPayBy;
	
	@Column(name="mrRtgsRefNo")
	private String mrRtgsRefNo;
	
	@Column(name="mrCustBankName")
	private String mrCustBankName;
	
	@Column(name="mrChqNo")
	private String mrChqNo;
	
	@Column(name="mrDate")
	private Date mrDate;
	
	@Column(name="mrType")
	private String mrType;
	
	@Column(name="mrDedRsn")
	private String mrDedRsn;
	
	@Column(name="mrIsPending" , columnDefinition="boolean default false")
	private boolean mrIsPending;
	
	@Column(name="mrOthMrNo")
	private String mrOthMrNo;
	
	@Column(name="mrOthMrAmt" , columnDefinition="double default 0.0")
	private double mrOthMrAmt;
	
	@Column(name="mrBnkId" , columnDefinition="int default -1")
	private int mrBnkId;
	
	@Column(name="mrCustId" , columnDefinition="int default -1")
	private int mrCustId;
	
	@Column(name="mrBrhId" , columnDefinition="int default -1")
	private int mrBrhId;
	
	@Column(name="mrNetAmt" , columnDefinition="double default 0.0")
	private double mrNetAmt;
	
	@Column(name="mrRemAmt" , columnDefinition="double default 0.0")
	private double mrRemAmt;
	
	@Column(name="mrTdsAmt" , columnDefinition="double default 0.0")
	private double mrTdsAmt;
	
	@Column(name="mrSrvTaxAmt" , columnDefinition="double default 0.0")
	private double mrSrvTaxAmt;
	
	@Column(name="mrDedAmt" , columnDefinition="double default 0.0")
	private double mrDedAmt;
	
	@Column(name="mrAccessAmt" , columnDefinition="double default 0.0")
	private double mrAccessAmt;
	
	@Column(name="mrFreight" , columnDefinition="double default 0.0")
	private double mrFreight;
	
	//One To One
	@JsonIgnore
	@JsonManagedReference(value = "mrbill")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinTable(name="mr_bill",
	joinColumns=@JoinColumn(name="mrId"),
	inverseJoinColumns=@JoinColumn(name="blId"))
	private Bill bill;
	
	@Column(name="mrDesc")
	private String mrDesc;
	
	@Column(name="mrDedDetList", columnDefinition="longblob")
	private ArrayList<Map<String,Object>> mrDedDetList = new ArrayList<>();
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="userCode")
	private String userCode;
	
	@Transient
	private double mrNetAmtForRpt;
	
	@Transient
	private String mrBankName;
	
	@Transient
	private String mrCustName;

	public int getMrId() {
		return mrId;
	}

	public void setMrId(int mrId) {
		this.mrId = mrId;
	}

	public String getMrNo() {
		return mrNo;
	}

	public void setMrNo(String mrNo) {
		this.mrNo = mrNo;
	}

	public char getMrPayBy() {
		return mrPayBy;
	}

	public void setMrPayBy(char mrPayBy) {
		this.mrPayBy = mrPayBy;
	}

	public String getMrChqNo() {
		return mrChqNo;
	}

	public void setMrChqNo(String mrChqNo) {
		this.mrChqNo = mrChqNo;
	}

	public Date getMrDate() {
		return mrDate;
	}

	public void setMrDate(Date mrDate) {
		this.mrDate = mrDate;
	}

	public String getMrType() {
		return mrType;
	}

	public void setMrType(String mrType) {
		this.mrType = mrType;
	}

	public String getMrDedRsn() {
		return mrDedRsn;
	}

	public void setMrDedRsn(String mrDedRsn) {
		this.mrDedRsn = mrDedRsn;
	}

	public boolean isMrIsPending() {
		return mrIsPending;
	}

	public void setMrIsPending(boolean mrIsPending) {
		this.mrIsPending = mrIsPending;
	}

	public int getMrBnkId() {
		return mrBnkId;
	}

	public void setMrBnkId(int mrBnkId) {
		this.mrBnkId = mrBnkId;
	}

	public int getMrCustId() {
		return mrCustId;
	}

	public void setMrCustId(int mrCustId) {
		this.mrCustId = mrCustId;
	}

	public int getMrBrhId() {
		return mrBrhId;
	}

	public void setMrBrhId(int mrBrhId) {
		this.mrBrhId = mrBrhId;
	}

	public double getMrNetAmt() {
		return mrNetAmt;
	}

	public void setMrNetAmt(double mrNetAmt) {
		this.mrNetAmt = mrNetAmt;
	}

	public double getMrTdsAmt() {
		return mrTdsAmt;
	}

	public void setMrTdsAmt(double mrTdsAmt) {
		this.mrTdsAmt = mrTdsAmt;
	}

	public double getMrSrvTaxAmt() {
		return mrSrvTaxAmt;
	}

	public void setMrSrvTaxAmt(double mrSrvTaxAmt) {
		this.mrSrvTaxAmt = mrSrvTaxAmt;
	}

	

	public double getMrDedAmt() {
		return mrDedAmt;
	}

	public void setMrDedAmt(double mrDedAmt) {
		this.mrDedAmt = mrDedAmt;
	}

	public double getMrAccessAmt() {
		return mrAccessAmt;
	}

	public void setMrAccessAmt(double mrAccessAmt) {
		this.mrAccessAmt = mrAccessAmt;
	}

	public double getMrFreight() {
		return mrFreight;
	}

	public void setMrFreight(double mrFreight) {
		this.mrFreight = mrFreight;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getMrDesc() {
		return mrDesc;
	}

	public void setMrDesc(String mrDesc) {
		this.mrDesc = mrDesc;
	}

	public String getMrRtgsRefNo() {
		return mrRtgsRefNo;
	}

	public void setMrRtgsRefNo(String mrRtgsRefNo) {
		this.mrRtgsRefNo = mrRtgsRefNo;
	}

	public String getMrCustBankName() {
		return mrCustBankName;
	}

	public void setMrCustBankName(String mrCustBankName) {
		this.mrCustBankName = mrCustBankName;
	}

	/*public String getMrFrPD() {
		return mrFrPD;
	}

	public void setMrFrPD(String mrFrPD) {
		this.mrFrPD = mrFrPD;
	}*/
	
	

	public ArrayList<Map<String, Object>> getMrDedDetList() {
		return mrDedDetList;
	}

	public ArrayList<Map<String, Object>> getMrFrPDList() {
		return mrFrPDList;
	}

	public void setMrFrPDList(ArrayList<Map<String, Object>> mrFrPDList) {
		this.mrFrPDList = mrFrPDList;
	}

	public void setMrDedDetList(ArrayList<Map<String, Object>> mrDedDetList) {
		this.mrDedDetList = mrDedDetList;
	}

	public double getMrRemAmt() {
		return mrRemAmt;
	}

	public void setMrRemAmt(double mrRemAmt) {
		this.mrRemAmt = mrRemAmt;
	}

	public String getMrOthMrNo() {
		return mrOthMrNo;
	}

	public void setMrOthMrNo(String mrOthMrNo) {
		this.mrOthMrNo = mrOthMrNo;
	}

	public double getMrOthMrAmt() {
		return mrOthMrAmt;
	}

	public void setMrOthMrAmt(double mrOthMrAmt) {
		this.mrOthMrAmt = mrOthMrAmt;
	}

	public boolean isMrCancel() {
		return mrCancel;
	}

	public void setMrCancel(boolean mrCancel) {
		this.mrCancel = mrCancel;
	}

	public double getMrNetAmtForRpt() {
		return mrNetAmtForRpt;
	}

	public void setMrNetAmtForRpt(double mrNetAmtForRpt) {
		this.mrNetAmtForRpt = mrNetAmtForRpt;
	}

	public String getMrBankName() {
		return mrBankName;
	}

	public void setMrBankName(String mrBankName) {
		this.mrBankName = mrBankName;
	}

	public String getMrCustName() {
		return mrCustName;
	}

	public void setMrCustName(String mrCustName) {
		this.mrCustName = mrCustName;
	}

	public boolean isMrSrtExsDis() {
		return mrSrtExsDis;
	}

	public void setMrSrtExsDis(boolean mrSrtExsDis) {
		this.mrSrtExsDis = mrSrtExsDis;
	}
	
	
}
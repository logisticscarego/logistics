package com.mylogistics.model.lhpv;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mylogistics.model.Challan;

@Entity
@Table(name = "lhpvadv")
public class LhpvAdv {
	
	@Id
	@Column(name="laId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int laId;
	
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name = "lsId") 
	private LhpvStatus lhpvStatus;
	
	
	//One To One
	@JsonIgnore
	@JsonManagedReference(value = "lachln")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinTable(name="lhpvadv_challan",
	joinColumns=@JoinColumn(name="laId"),
	inverseJoinColumns=@JoinColumn(name="chlnId"))
	private Challan challan;
	
	@Column(name="laNo")
	private int laNo;
	
	@Column(name="laDt")
	private Date laDt;
	
	@Column(name="laIsClose" , columnDefinition="boolean default false")
	private boolean laIsClose;
	
	@Column(name="laBrhCode")
	private String laBrhCode;
	
	@Column(name="laBrkOwn")
	private String laBrkOwn;
	
	@Column(name="laLryAdvP")
	private double laLryAdvP;
	
	@Column(name="laLoadingP")
	private double laLoadingP;
	
	@Column(name="laLdpDetP")
	private double laLdpDetP;
	
	@Column(name="laOthExtKmP")
	private double laOthExtKmP;
	
	@Column(name="laOthOvrHgtP")
	private double laOthOvrHgtP;
	
	@Column(name="laOthPnltyP")
	private double laOthPnltyP;
	
	@Column(name="laOthMisctP")
	private double laOthMiscP;
	
	@Column(name="laCashDiscR")
	private double laCashDiscR;
	
	@Column(name="laMunsR")
	private double laMunsR;
	
	@Column(name="laTdsR")
	private double laTdsR;
	
	@Column(name="laPayMethod")
	private char laPayMethod;
	
	@Column(name="laBankCode")
	private String laBankCode;
	
	@Column(name="laChqType")
	private char laChqType;
	
	@Column(name="laChqNo")
	private String laChqNo;
	
	@Column(name="laTotPayAmt")
	private double laTotPayAmt;
	
	@Column(name="laTotRcvrAmt")
	private double laTotRcvrAmt;
	
	@Column(name="laFinalTot")
	private double laFinalTot;
	
	@Column(name="laDesc")
	private String laDesc;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="stafCode")
	private String stafCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column (name="payToStf")
	private String payToStf;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Transient
	private String sheetNo;

	public int getLaId() {
		return laId;
	}

	public void setLaId(int laId) {
		this.laId = laId;
	}

	

	public LhpvStatus getLhpvStatus() {
		return lhpvStatus;
	}

	public void setLhpvStatus(LhpvStatus lhpvStatus) {
		this.lhpvStatus = lhpvStatus;
	}

	public double getLaLryAdvP() {
		return laLryAdvP;
	}

	public void setLaLryAdvP(double laLryAdvP) {
		this.laLryAdvP = laLryAdvP;
	}

	public double getLaLoadingP() {
		return laLoadingP;
	}

	public void setLaLoadingP(double laLoadingP) {
		this.laLoadingP = laLoadingP;
	}

	public double getLaLdpDetP() {
		return laLdpDetP;
	}

	public void setLaLdpDetP(double laLdpDetP) {
		this.laLdpDetP = laLdpDetP;
	}

	public double getLaOthExtKmP() {
		return laOthExtKmP;
	}

	public void setLaOthExtKmP(double laOthExtKmP) {
		this.laOthExtKmP = laOthExtKmP;
	}

	public double getLaOthOvrHgtP() {
		return laOthOvrHgtP;
	}

	public void setLaOthOvrHgtP(double laOthOvrHgtP) {
		this.laOthOvrHgtP = laOthOvrHgtP;
	}

	public double getLaOthPnltyP() {
		return laOthPnltyP;
	}

	public void setLaOthPnltyP(double laOthPnltyP) {
		this.laOthPnltyP = laOthPnltyP;
	}

	public double getLaOthMiscP() {
		return laOthMiscP;
	}

	public void setLaOthMiscP(double laOthMiscP) {
		this.laOthMiscP = laOthMiscP;
	}

	public double getLaCashDiscR() {
		return laCashDiscR;
	}

	public void setLaCashDiscR(double laCashDiscR) {
		this.laCashDiscR = laCashDiscR;
	}

	public double getLaMunsR() {
		return laMunsR;
	}

	public void setLaMunsR(double laMunsR) {
		this.laMunsR = laMunsR;
	}

	public double getLaTdsR() {
		return laTdsR;
	}

	public void setLaTdsR(double laTdsR) {
		this.laTdsR = laTdsR;
	}

	public char getLaPayMethod() {
		return laPayMethod;
	}

	public void setLaPayMethod(char laPayMethod) {
		this.laPayMethod = laPayMethod;
	}

	public String getLaBankCode() {
		return laBankCode;
	}

	public void setLaBankCode(String laBankCode) {
		this.laBankCode = laBankCode;
	}

	public char getLaChqType() {
		return laChqType;
	}

	public void setLaChqType(char laChqType) {
		this.laChqType = laChqType;
	}

	public String getLaChqNo() {
		return laChqNo;
	}

	public void setLaChqNo(String laChqNo) {
		this.laChqNo = laChqNo;
	}

	public double getLaTotPayAmt() {
		return laTotPayAmt;
	}

	public void setLaTotPayAmt(double laTotPayAmt) {
		this.laTotPayAmt = laTotPayAmt;
	}

	public double getLaTotRcvrAmt() {
		return laTotRcvrAmt;
	}

	public void setLaTotRcvrAmt(double laTotRcvrAmt) {
		this.laTotRcvrAmt = laTotRcvrAmt;
	}

	public double getLaFinalTot() {
		return laFinalTot;
	}

	public void setLaFinalTot(double laFinalTot) {
		this.laFinalTot = laFinalTot;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public Challan getChallan() {
		return challan;
	}

	public void setChallan(Challan challan) {
		this.challan = challan;
	}

	public String getLaDesc() {
		return laDesc;
	}

	public void setLaDesc(String laDesc) {
		this.laDesc = laDesc;
	}

	public String getLaBrkOwn() {
		return laBrkOwn;
	}

	public void setLaBrkOwn(String laBrkOwn) {
		this.laBrkOwn = laBrkOwn;
	}

	public int getLaNo() {
		return laNo;
	}

	public void setLaNo(int laNo) {
		this.laNo = laNo;
	}

	public boolean isLaIsClose() {
		return laIsClose;
	}

	public void setLaIsClose(boolean laIsClose) {
		this.laIsClose = laIsClose;
	}

	public Date getLaDt() {
		return laDt;
	}

	public void setLaDt(Date laDt) {
		this.laDt = laDt;
	}

	public String getLaBrhCode() {
		return laBrhCode;
	}

	public void setLaBrhCode(String laBrhCode) {
		this.laBrhCode = laBrhCode;
	}

	public String getSheetNo() {
		return sheetNo;
	}

	public void setSheetNo(String sheetNo) {
		this.sheetNo = sheetNo;
	}

	public String getPayToStf() {
		return payToStf;
	}

	public void setPayToStf(String payToStf) {
		this.payToStf = payToStf;
	}

	public String getStafCode() {
		return stafCode;
	}

	public void setStafCode(String stafCode) {
		this.stafCode = stafCode;
	}
	
	
	
}

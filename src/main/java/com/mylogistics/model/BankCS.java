package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bankcs")
public class BankCS {

	@Id
	@Column(name="bcsId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int bcsId;
	
	@Column(name="bcsDt")
	private Date bcsDt;
	
	@Column(name="bcsBrhId")
	private int bcsBrhId;
	
	@Column(name="bcsBankId")
	private int bcsBankId;
	
	@Column(name="bcsBankCode")
	private String bcsBankCode;
	
	@Column(name="bcsOpenBal" , columnDefinition="double default 0.0")
	private double bcsOpenBal;
	
	@Column(name="bcsCloseBal" , columnDefinition="double default 0.0")
	private double bcsCloseBal;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="userCode")
	private String userCode;

	public int getBcsId() {
		return bcsId;
	}

	public void setBcsId(int bcsId) {
		this.bcsId = bcsId;
	}

	public Date getBcsDt() {
		return bcsDt;
	}

	public void setBcsDt(Date bcsDt) {
		this.bcsDt = bcsDt;
	}

	public int getBcsBrhId() {
		return bcsBrhId;
	}

	public void setBcsBrhId(int bcsBrhId) {
		this.bcsBrhId = bcsBrhId;
	}

	public int getBcsBankId() {
		return bcsBankId;
	}

	public void setBcsBankId(int bcsBankId) {
		this.bcsBankId = bcsBankId;
	}

	public String getBcsBankCode() {
		return bcsBankCode;
	}

	public void setBcsBankCode(String bcsBankCode) {
		this.bcsBankCode = bcsBankCode;
	}

	public double getBcsOpenBal() {
		return bcsOpenBal;
	}

	public void setBcsOpenBal(double bcsOpenBal) {
		this.bcsOpenBal = bcsOpenBal;
	}

	public double getBcsCloseBal() {
		return bcsCloseBal;
	}

	public void setBcsCloseBal(double bcsCloseBal) {
		this.bcsCloseBal = bcsCloseBal;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	
	
}

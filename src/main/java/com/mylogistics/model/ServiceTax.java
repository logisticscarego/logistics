package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "servicetax")
public class ServiceTax {
	
	@Id
	@Column(name="stId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int stId;
	
	@Column(name="stTaxableRt" , columnDefinition="double default 0.0")
	private double stTaxableRt;
	
	@Column(name="stSerTaxRt" , columnDefinition="double default 0.0")
	private double stSerTaxRt;
	
	@Column(name="stEduCessRt" , columnDefinition="double default 0.0")
	private double stEduCessRt;
	
	@Column(name="stHscCessRt" , columnDefinition="double default 0.0")
	private double stHscCessRt;
	
	@Column(name="stSwhBHCessRt" , columnDefinition="double default 0.0") 
	private double stSwhBHCessRt;
	
	@Column(name="stKissanCessRt" , columnDefinition="double default 0.0")
	private double stKissanCessRt;

	@Column(name="stFrmDt")
	private Date stFrmDt;

	@Column(name="stToDt")
	private Date stToDt;

	@Column(name="stCGST" , columnDefinition="double default 0.0")
	private Float stCGST;
	
	@Column(name="stSGST" , columnDefinition="double default 0.0")
	private Float stSGST;
	
	@Column(name="stIGST" , columnDefinition="double default 0.0")
	private Float stIGST;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
    private Calendar creationTS;
	
	public int getStId() {
		return stId;
	}

	public void setStId(int stId) {
		this.stId = stId;
	}

	public double getStTaxableRt() {
		return stTaxableRt;
	}

	public void setStTaxableRt(double stTaxableRt) {
		this.stTaxableRt = stTaxableRt;
	}

	public double getStSerTaxRt() {
		return stSerTaxRt;
	}

	public void setStSerTaxRt(double stSerTaxRt) {
		this.stSerTaxRt = stSerTaxRt;
	}

	public double getStEduCessRt() {
		return stEduCessRt;
	}

	public void setStEduCessRt(double stEduCessRt) {
		this.stEduCessRt = stEduCessRt;
	}

	public double getStHscCessRt() {
		return stHscCessRt;
	}

	public void setStHscCessRt(double stHscCessRt) {
		this.stHscCessRt = stHscCessRt;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public double getStSwhBHCessRt() {
		return stSwhBHCessRt;
	}

	public void setStSwhBHCessRt(double stSwhBHCessRt) {
		this.stSwhBHCessRt = stSwhBHCessRt;
	}

	public double getStKissanCessRt() {
		return stKissanCessRt;
	}

	public void setStKissanCessRt(double stKissanCessRt) {
		this.stKissanCessRt = stKissanCessRt;
	}

	public Date getStFrmDt() {
		return stFrmDt;
	}

	public void setStFrmDt(Date stFrmDt) {
		this.stFrmDt = stFrmDt;
	}

	public Date getStToDt() {
		return stToDt;
	}

	public void setStToDt(Date stToDt) {
		this.stToDt = stToDt;
	}

	public Float getStCGST() {
		return stCGST;
	}

	public void setStCGST(Float stCGST) {
		this.stCGST = stCGST;
	}

	public Float getStSGST() {
		return stSGST;
	}

	public void setStSGST(Float stSGST) {
		this.stSGST = stSGST;
	}

	public Float getStIGST() {
		return stIGST;
	}

	public void setStIGST(Float stIGST) {
		this.stIGST = stIGST;
	}

	
	
}

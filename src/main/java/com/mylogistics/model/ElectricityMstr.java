package com.mylogistics.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "electricitymstr")
public class ElectricityMstr {

	@Id
	@Column(name="emId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int emId;
	
	// Many To One
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	//@JsonBackReference(value = "telBrh")
	@JoinColumn(name = "branchId") 
	private Branch branch;
	
	// Many To One
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	//@JsonBackReference(value = "eleEmp")
	@JoinColumn(name = "empId") 
	private Employee employee;
	
	//One To Many
	@JsonIgnore
	@JsonManagedReference(value = "semEM")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="ElectricityMstr_subelectricitymstr",
	joinColumns=@JoinColumn(name="emId"),
	inverseJoinColumns=@JoinColumn(name="semId"))
	private List<SubElectricityMstr> subElectricityMstr = new ArrayList<SubElectricityMstr>();
		
	@Column(name="emCrnNo")
	private String emCrnNo;
	
	@Column(name="emSPName")
	private String emSPName;
	
	@Column(name="emParticular")
	private String emParticular;
	
	@Column(name="emInstDt")
	private Date emInstDt;
	
	@Column(name="emDiscDt")
	private Date emDiscDt;
	
	@Column(name="emAdvDepDt")
	private Date emAdvDepDt;

	@Column(name="emRefDt")
	private Date emRefDt;
	
	@Column(name="emAdvDepAmt")
	private double emAdvDepAmt;
	
	@Column(name="emRefAmt")
	private double emRefAmt;
	
	@Column(name="emBillDay")
	private int emBillDay;
	
	@Column(name="emDueDay")
	private int emDueDay;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	
	
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public List<SubElectricityMstr> getSubElectricityMstr() {
		return subElectricityMstr;
	}

	public void setSubElectricityMstr(List<SubElectricityMstr> subElectricityMstr) {
		this.subElectricityMstr = subElectricityMstr;
	}

	public int getEmId() {
		return emId;
	}

	public void setEmId(int emId) {
		this.emId = emId;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public String getEmCrnNo() {
		return emCrnNo;
	}

	public void setEmCrnNo(String emCrnNo) {
		this.emCrnNo = emCrnNo;
	}

	public String getEmSPName() {
		return emSPName;
	}

	public void setEmSPName(String emSPName) {
		this.emSPName = emSPName;
	}

	public String getEmParticular() {
		return emParticular;
	}

	public void setEmParticular(String emParticular) {
		this.emParticular = emParticular;
	}

	public Date getEmInstDt() {
		return emInstDt;
	}

	public void setEmInstDt(Date emInstDt) {
		this.emInstDt = emInstDt;
	}

	public Date getEmDiscDt() {
		return emDiscDt;
	}

	public void setEmDiscDt(Date emDiscDt) {
		this.emDiscDt = emDiscDt;
	}

	public Date getEmAdvDepDt() {
		return emAdvDepDt;
	}

	public void setEmAdvDepDt(Date emAdvDepDt) {
		this.emAdvDepDt = emAdvDepDt;
	}

	public Date getEmRefDt() {
		return emRefDt;
	}

	public void setEmRefDt(Date emRefDt) {
		this.emRefDt = emRefDt;
	}

	public double getEmAdvDepAmt() {
		return emAdvDepAmt;
	}

	public void setEmAdvDepAmt(double emAdvDepAmt) {
		this.emAdvDepAmt = emAdvDepAmt;
	}

	public double getEmRefAmt() {
		return emRefAmt;
	}

	public void setEmRefAmt(double emRefAmt) {
		this.emRefAmt = emRefAmt;
	}

	public int getEmBillDay() {
		return emBillDay;
	}

	public void setEmBillDay(int emBillDay) {
		this.emBillDay = emBillDay;
	}

	public int getEmDueDay() {
		return emDueDay;
	}

	public void setEmDueDay(int emDueDay) {
		this.emDueDay = emDueDay;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
}

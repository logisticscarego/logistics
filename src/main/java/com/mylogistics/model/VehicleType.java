package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vehicletype")
public class VehicleType {
	

	@Id
	@Column(name="vtId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int vtId;
	
	@Column(name="vtServiceType")
	private String vtServiceType;
	
	@Column(name="vtVehicleType")
	private String vtVehicleType;

	@Column(name="vtCode")
	private String vtCode;
	
	@Column(name="vtLoadLimit")
	private double vtLoadLimit;
	
	@Column(name="vtGuaranteeWt")
	private double vtGuaranteeWt;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;	

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
    private Calendar creationTS;
	
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

    public Calendar getCreationTS() {
        return creationTS;
    }

    public void setCreationTS(Calendar creationTS) {
        this.creationTS = creationTS;
    }
	
	public int getVtId() {
		return vtId;
	}

	public void setVtId(int vtId) {
		this.vtId = vtId;
	}

	public String getVtServiceType() {
		return vtServiceType;
	}

	public void setVtServiceType(String vtServiceType) {
		this.vtServiceType = vtServiceType;
	}

	public String getVtVehicleType() {
		return vtVehicleType;
	}

	public void setVtVehicleType(String vtVehicleType) {
		this.vtVehicleType = vtVehicleType;
	}

	public String getVtCode() {
		return vtCode;
	}

	public void setVtCode(String vtCode) {
		this.vtCode = vtCode;
	}

	public double getVtLoadLimit() {
		return vtLoadLimit;
	}

	public void setVtLoadLimit(double vtLoadLimit) {
		this.vtLoadLimit = vtLoadLimit;
	}

	public double getVtGuaranteeWt() {
		return vtGuaranteeWt;
	}

	public void setVtGuaranteeWt(double vtGuaranteeWt) {
		this.vtGuaranteeWt = vtGuaranteeWt;
	}

}

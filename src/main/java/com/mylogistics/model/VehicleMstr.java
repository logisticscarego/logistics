package com.mylogistics.model;

import java.util.ArrayList;
import java.util.Calendar;
//import java.util.Date;
import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "vehiclemstr")
public class VehicleMstr {

	@Id
	@Column(name="vehId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int vehId;
	
	// Many To One
	@ManyToOne
	@LazyCollection(LazyCollectionOption.FALSE)
	//@JsonBackReference(value = "vehBrh")
	@JoinColumn(name = "branchId") 
	private Branch branch;
	
	@Column(name="vehNo")
	private String vehNo;
	
	@Column(name = "vehModel")
	private int vehModel;
	
	@Column(name="vehType")
	private char vehType;
	
	@Column(name="vehOwnName")
	private String vehOwnName;
	
	/*@OneToOne(mappedBy = "vehiclemstr")
	private Address vehOwnAdd;*/
	
	//One to One
	@OneToOne(cascade = CascadeType.ALL)
	private Address vehOwnAdd;
	
	@Column(name="vehPolicyNo")
	private String vehPolicyNo;
	
	@Column(name="vehInsUpto")
	private Date vehInsUpto;
	
	@Column(name="vehInsPrem") 
	private double vehInsPrem;
	
	@Column(name="vehTaxUpto")
	private Date vehTaxUpto;
	
	@Column(name="vehTaxPlace")
	private String vehTaxPlace;
	
	@Column(name="vehTaxAmt")
	private double vehTaxAmt;
	
	@Column(name="vehSaleVal")
	private double vehSaleVal;
	
	@Column(name="vehSoldTo")
	private String vehSoldTo;
	
	@Column(name="vehSoldDt")
	private Date vehSoldDt;
	
	//One To One
	@OneToOne(cascade = CascadeType.ALL)
	private Address vehRtoAdd;
	
	@Column(name="vehFitUpto")
	private Date vehFitUpto;
	
	@Column(name="vehFitPlace")
	private String vehFitPlace;
	
	@Column(name="vehStatus")
	private boolean vehStatus;
	
	//text
	@Column(name="vehComment", columnDefinition="TEXT")
	private String vehComment;
	
	@Column(name="vehEngineNo")
	private String vehEngineNo;
	
	@Column(name="vehChesisNo")
	private String vehChesisNo;
	
	//Many To One
	@ManyToOne
	@LazyCollection(LazyCollectionOption.FALSE)
	//@JsonBackReference(value = "vehEmp")
	@JoinColumn(name = "empId") 
	private Employee employee;
	
	//One To Many
    @JsonIgnore
  	@JsonManagedReference(value = "vmSvm")
  	@LazyCollection(LazyCollectionOption.TRUE)
  	@OneToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="vehiclemstr_subvehiclemstr",
      		joinColumns=@JoinColumn(name="vehId"),
              inverseJoinColumns=@JoinColumn(name="svmId"))
  	private List<SubVehicleMstr> svmList = new ArrayList<SubVehicleMstr>();
	
	@Column(name="vehWEFDt")
	private Date vehWEFDt;
	
	@Column(name="vehEmpLNo")
	private String vehEmpLNo;
	
	@Column(name="vehEmpLUpto")
	private Date vehEmpLUpto;

	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getVehId() {
		return vehId;
	}

	public void setVehId(int vehId) {
		this.vehId = vehId;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public String getVehNo() {
		return vehNo;
	}

	public void setVehNo(String vehNo) {
		this.vehNo = vehNo;
	}

	public int getVehModel() {
		return vehModel;
	}

	public void setVehModel(int vehModel) {
		this.vehModel = vehModel;
	}

	public char getVehType() {
		return vehType;
	}

	public void setVehType(char vehType) {
		this.vehType = vehType;
	}

	public String getVehOwnName() {
		return vehOwnName;
	}

	public void setVehOwnName(String vehOwnName) {
		this.vehOwnName = vehOwnName;
	}

	public Address getVehOwnAdd() {
		return vehOwnAdd;
	}

	public void setVehOwnAdd(Address vehOwnAdd) {
		this.vehOwnAdd = vehOwnAdd;
	}

	public String getVehPolicyNo() {
		return vehPolicyNo;
	}

	public void setVehPolicyNo(String vehPolicyNo) {
		this.vehPolicyNo = vehPolicyNo;
	}

	public Date getVehInsUpto() {
		return vehInsUpto;
	}

	public void setVehInsUpto(Date vehInsUpto) {
		this.vehInsUpto = vehInsUpto;
	}

	public double getVehInsPrem() {
		return vehInsPrem;
	}

	public void setVehInsPrem(double vehInsPrem) {
		this.vehInsPrem = vehInsPrem;
	}

	public Date getVehTaxUpto() {
		return vehTaxUpto;
	}

	public void setVehTaxUpto(Date vehTaxUpto) {
		this.vehTaxUpto = vehTaxUpto;
	}

	public String getVehTaxPlace() {
		return vehTaxPlace;
	}

	public void setVehTaxPlace(String vehTaxPlace) {
		this.vehTaxPlace = vehTaxPlace;
	}

	public double getVehTaxAmt() {
		return vehTaxAmt;
	}

	public void setVehTaxAmt(double vehTaxAmt) {
		this.vehTaxAmt = vehTaxAmt;
	}

	public double getVehSaleVal() {
		return vehSaleVal;
	}

	public void setVehSaleVal(double vehSaleVal) {
		this.vehSaleVal = vehSaleVal;
	}

	public String getVehSoldTo() {
		return vehSoldTo;
	}

	public void setVehSoldTo(String vehSoldTo) {
		this.vehSoldTo = vehSoldTo;
	}

	public Date getVehSoldDt() {
		return vehSoldDt;
	}

	public void setVehSoldDt(Date vehSoldDt) {
		this.vehSoldDt = vehSoldDt;
	}

	public Address getVehRtoAdd() {
		return vehRtoAdd;
	}

	public void setVehRtoAdd(Address vehRtoAdd) {
		this.vehRtoAdd = vehRtoAdd;
	}

	public Date getVehFitUpto() {
		return vehFitUpto;
	}

	public void setVehFitUpto(Date vehFitUpto) {
		this.vehFitUpto = vehFitUpto;
	}

	public String getVehFitPlace() {
		return vehFitPlace;
	}

	public void setVehFitPlace(String vehFitPlace) {
		this.vehFitPlace = vehFitPlace;
	}

	public boolean isVehStatus() {
		return vehStatus;
	}

	public void setVehStatus(boolean vehStatus) {
		this.vehStatus = vehStatus;
	}

	public String getVehComment() {
		return vehComment;
	}

	public void setVehComment(String vehComment) {
		this.vehComment = vehComment;
	}

	public String getVehEngineNo() {
		return vehEngineNo;
	}

	public void setVehEngineNo(String vehEngineNo) {
		this.vehEngineNo = vehEngineNo;
	}

	public String getVehChesisNo() {
		return vehChesisNo;
	}

	public void setVehChesisNo(String vehChesisNo) {
		this.vehChesisNo = vehChesisNo;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Date getVehWEFDt() {
		return vehWEFDt;
	}

	public void setVehWEFDt(Date vehWEFDt) {
		this.vehWEFDt = vehWEFDt;
	}

	public String getVehEmpLNo() {
		return vehEmpLNo;
	}

	public void setVehEmpLNo(String vehEmpLNo) {
		this.vehEmpLNo = vehEmpLNo;
	}

	public Date getVehEmpLUpto() {
		return vehEmpLUpto;
	}

	public void setVehEmpLUpto(Date vehEmpLUpto) {
		this.vehEmpLUpto = vehEmpLUpto;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public List<SubVehicleMstr> getSvmList() {
		return svmList;
	}

	public void setSvmList(List<SubVehicleMstr> svmList) {
		this.svmList = svmList;
	}

	
}

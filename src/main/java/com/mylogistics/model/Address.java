package com.mylogistics.model;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "address")
public class Address {
	
	@Id
	@Column(name="addId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int addId;
	
	@Column(name="completeAdd")
	private String completeAdd;
	
	@Column(name="addCity")
	private String addCity;
	
	@Column(name="addState")
	private String addState;
	
	@Column(name="addPin")
	private String addPin;
	
	@Column(name="addType")
	private String addType;
	
	@Column(name="addDist")
	private String addDist;
	
	@Column(name="addPost")
	private String addPost;
	
	@Column(name="addRefCode")
	private String addRefCode;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public int getAddId() {
		return addId;
	}

	public void setAddId(int addId) {
		this.addId = addId;
	}

	public String getCompleteAdd() {
		return completeAdd;
	}

	public void setCompleteAdd(String completeAdd) {
		this.completeAdd = completeAdd;
	}

	public String getAddCity() {
		return addCity;
	}

	public void setAddCity(String addCity) {
		this.addCity = addCity;
	}

	public String getAddState() {
		return addState;
	}

	public void setAddState(String addState) {
		this.addState = addState;
	}

	public String getAddPin() {
		return addPin;
	}

	public void setAddPin(String addPin) {
		this.addPin = addPin;
	}

	public String getAddType() {
		return addType;
	}

	public void setAddType(String addType) {
		this.addType = addType;
	}

	public String getAddRefCode() {
		return addRefCode;
	}

	public void setAddRefCode(String addRefCode) {
		this.addRefCode = addRefCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getAddDist() {
		return addDist;
	}

	public void setAddDist(String addDist) {
		this.addDist = addDist;
	}

	public String getAddPost() {
		return addPost;
	}

	public void setAddPost(String addPost) {
		this.addPost = addPost;
	}
	
	
	

}

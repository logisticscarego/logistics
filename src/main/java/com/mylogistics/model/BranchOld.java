package com.mylogistics.model;

import java.util.Calendar;
//import java.util.Date;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "branchold")
public class BranchOld {
	
	@Id
	@Column(name="branchOldId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int branchOldId;
	
	@Column(name="branchId")
	private int branchId;
	
	@Column(name="branchCode")
	private String branchCode;
	
	@Column(name="branchName")
	private String branchName;
	
	@Column(name="branchAdd")
	private String branchAdd;
	
	@Column(name="branchCity")
	private String branchCity;
	
	@Column(name="branchState")
	private String branchState;
	
	@Column(name="branchPin")
	private String branchPin;
	
	@Column(name="branchOpenDt")
	private Date branchOpenDt;
	
	@Column(name="branchCloseDt")
	private Date branchCloseDt;
	
	@Column(name="branchStationCode")
	private String branchStationCode;
	
	@Column(name="branchDirector")
	private String branchDirector;
	
	@Column(name="branchMarketingHD")
	private String branchMarketingHD;
	
	@Column(name="branchOutStandingHD")
	private String branchOutStandingHD;
	
	@Column(name="branchMarketing")
	private String branchMarketing;
	
	@Column(name="branchMngr")
	private String branchMngr;
	
	@Column(name="branchCashier")
	private String branchCashier;
	
	@Column(name="branchTraffic")
	private String branchTraffic;
	
	@Column(name="branchRegionalMngr")
	private String branchRegionalMngr;
	
	@Column(name="branchAreaMngr")
	private String branchAreaMngr;
	
	@Column(name="branchPhone")
	private String branchPhone;
	
	@Column(name="branchFax")
	private String branchFax;
	
	@Column(name="branchEmailId")
	private String branchEmailId;
	
	@Column(name="branchWebsite")
	private String branchWebsite;
	
	@Column(name="isOpen")
	private String isOpen;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="isNCR")
	private String isNCR;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="isView")
	private boolean isView;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
    private Calendar creationTS;

    public Calendar getCreationTS() {
        return creationTS;
    }

    public void setCreationTS(Calendar creationTS) {
        this.creationTS = creationTS;
    }
	

    public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getIsNCR() {
		return isNCR;
	}

	public void setIsNCR(String isNCR) {
		this.isNCR = isNCR;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchAdd() {
		return branchAdd;
	}

	public void setBranchAdd(String branchAdd) {
		this.branchAdd = branchAdd;
	}

	public String getBranchCity() {
		return branchCity;
	}

	public void setBranchCity(String branchCity) {
		this.branchCity = branchCity;
	}

	public String getBranchState() {
		return branchState;
	}

	public void setBranchState(String branchState) {
		this.branchState = branchState;
	}

	public String getBranchPin() {
		return branchPin;
	}

	public void setBranchPin(String branchPin) {
		this.branchPin = branchPin;
	}

	public String getBranchStationCode() {
		return branchStationCode;
	}

	public void setBranchStationCode(String branchStationCode) {
		this.branchStationCode = branchStationCode;
	}

	public String getBranchDirector() {
		return branchDirector;
	}

	public void setBranchDirector(String branchDirector) {
		this.branchDirector = branchDirector;
	}

	public String getBranchMarketingHD() {
		return branchMarketingHD;
	}

	public void setBranchMarketingHD(String branchMarketingHD) {
		this.branchMarketingHD = branchMarketingHD;
	}

	public String getBranchOutStandingHD() {
		return branchOutStandingHD;
	}

	public void setBranchOutStandingHD(String branchOutStandingHD) {
		this.branchOutStandingHD = branchOutStandingHD;
	}

	public String getBranchMarketing() {
		return branchMarketing;
	}

	public void setBranchMarketing(String branchMarketing) {
		this.branchMarketing = branchMarketing;
	}

	public String getBranchMngr() {
		return branchMngr;
	}

	public void setBranchMngr(String branchMngr) {
		this.branchMngr = branchMngr;
	}

	public String getBranchCashier() {
		return branchCashier;
	}

	public void setBranchCashier(String branchCashier) {
		this.branchCashier = branchCashier;
	}

	public String getBranchTraffic() {
		return branchTraffic;
	}

	public void setBranchTraffic(String branchTraffic) {
		this.branchTraffic = branchTraffic;
	}

	public String getBranchRegionalMngr() {
		return branchRegionalMngr;
	}

	public void setBranchRegionalMngr(String branchRegionalMngr) {
		this.branchRegionalMngr = branchRegionalMngr;
	}

	public String getBranchAreaMngr() {
		return branchAreaMngr;
	}

	public void setBranchAreaMngr(String branchAreaMngr) {
		this.branchAreaMngr = branchAreaMngr;
	}

	public String getBranchPhone() {
		return branchPhone;
	}

	public void setBranchPhone(String branchPhone) {
		this.branchPhone = branchPhone;
	}

	public String getBranchFax() {
		return branchFax;
	}

	public void setBranchFax(String branchFax) {
		this.branchFax = branchFax;
	}

	public String getBranchEmailId() {
		return branchEmailId;
	}

	public void setBranchEmailId(String branchEmailId) {
		this.branchEmailId = branchEmailId;
	}

	public String getBranchWebsite() {
		return branchWebsite;
	}

	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}

	public String getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(String isOpen) {
		this.isOpen = isOpen;
	}
	
	public Date getBranchOpenDt() {
		return branchOpenDt;
	}

	public void setBranchOpenDt(Date branchOpenDt) {
		this.branchOpenDt = branchOpenDt;
	}

	public Date getBranchCloseDt() {
		return branchCloseDt;
	}

	public void setBranchCloseDt(Date branchCloseDt) {
		this.branchCloseDt = branchCloseDt;
	}
	
	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}

}

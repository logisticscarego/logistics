package com.mylogistics.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
//import java.util.Date;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;


@Entity
@Table(name = "cnmt")
public class Cnmt {
	
	@Id
	@Column(name="cnmtId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int cnmtId;
	
	@Column(name="cnmtCode")
	private String cnmtCode;
	
	@Column(name="branchCode")
	private String branchCode;
	
	@Column(name="cnmtDt")
	private Date cnmtDt;
	
	@Column(name="custCode")
	private String custCode;

    @Column(name="cnmtConsignor")
	private String cnmtConsignor;
    
    @Column(name="cnmtConsignee")
	private String cnmtConsignee;
    
    @Column(name="contractCode")
   	private String contractCode;
    
    @Column(name="cnmtNoOfPkg")
	private int cnmtNoOfPkg;
    
    @Column(name="cnmtActualWt")
	private double cnmtActualWt;
    
    @Column(name="cnmtGuaranteeWt")
   	private double cnmtGuaranteeWt;

	@Column(name="cnmtPayAt")
	private String cnmtPayAt;
    
    @Column(name="cnmtBillAt")
	private String cnmtBillAt;
    
    @Column(name="cnmtFreight")
	private double cnmtFreight;
    
    @Column(name="cnmtVOG")
	private double cnmtVOG;
    
    @Column(name="cnmtExtraExp")
	private int cnmtExtraExp;
    
    @Column(name="cnmtDtOfDly")
	private Date cnmtDtOfDly;
    
    @Column(name="cnmtEmpCode")
	private String cnmtEmpCode;
    
    @Column(name="cnmtKm")
	private double cnmtKm;
    
    @Column(name="cnmtState")
	private String cnmtState;
    
    @Column(name="cnmtSrtg" , columnDefinition="double default 0")
	private double cnmtSrtg;
    
    @Column(name="cnmtInvoiceNo" , columnDefinition="longblob")
	private ArrayList<Map<String,Object>> cnmtInvoiceNo;
    
    @Column(name="userCode")
	private String userCode;
    
    @Column(name="isDone")
	private int isDone;
  
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="isView")
	private boolean isView;
	
	@Column(name="isTrans" , columnDefinition="boolean default false")
	private boolean isTrans;
	
	@Column(name="cnmtBillNo")
	@NotNull(message = "CNMT Bill no. can not be null !", groups = CnmtBillInsert.class)
	private String cnmtBillNo;
	
	@Column(name="cnmtBillDetId", columnDefinition="int default -1")
	@Min(value = 1, message = "Cnmt Bill Detail Id should be greater than 1", groups = CnmtBillInsert.class)
	private int cnmtBillDetId;
	
	@Column(name="cnmtSupBillNo" , columnDefinition="longblob")
	private ArrayList<String> cnmtSupBillNo;
	
	@Column(name="cnmtSupBillDet" , columnDefinition="longblob")
	private ArrayList<String> cnmtSupBillDet;
	
	/*@Column(name="cnmtImage")
	private Blob cnmtImage;
	
	@Column(name="cnmtConfirmImage")
	private Blob cnmtConfirmImage;*/
	
	@Column(name="cmId" , columnDefinition = "int default -1")
	private int cmId;
	
	@Column(name="cnmtRate")
	private Double cnmtRate;
	 
	 @Column(name="cnmtFromSt")
	private String cnmtFromSt;
		
	@Column(name="cnmtToSt")
	private String cnmtToSt;
	
	@Column(name="cnmtDC")
	private String cnmtDC;
	
	@Column(name="cnmtVehicleType")
	private String cnmtVehicleType;
	
	@Column(name="cnmtProductType")
	private String cnmtProductType;
	
	@Column(name="cnmtTOT")
	private double cnmtTOT;
	
	@Column(name="cnmtBillPermission")
	private String cnmtBillPermission;
	
	@Column(name="cnmtTpNo")
	private String cnmtTpNo;
	
	@Column(name="relType")
	private String relType;
	
	@Column(name="whCode")
	private Integer whCode;
	
	@Column(name="relChln")
	private String relChln;
	
	@Column(name="prBlCode")
	private String prBlCode;
	
	@Column(name="cnmtGtOutDt")
	private Date cnmtGtOutDt;
	
	@Column(name="cnmtEdit", columnDefinition="boolean default false")
	private boolean cnmtEdit;
	
	@Column(name="cnmtType", columnDefinition="varchar(255) default 'normal'")
	private String cnmtType;
	
	@Column(name="CCA" )
	private String isCCA;
	

	@Column(name="isCancel" , columnDefinition="boolean default false")
	private boolean isCancel;
	
	@Column(name="cnmtCost" , columnDefinition="double default 0")
	private double cnmtCost;
	
	@Column(name="cnmtPL" , columnDefinition="double default 0")
	private double cnmtPL;
	
	@Column(name="bilAmt" , columnDefinition="double default 0")
	private double bilAmt;
	
	@Column(name="costPer" , columnDefinition="float default 0")
	private float costPer;
	
	@Column(name="lhpvAmt" , columnDefinition="double default 0")
   	private double lhpvAmt;
	
	@Column(name="lhpvRcvry" , columnDefinition="double default 0")
   	private double lhpvRcvry;
	
	@Column(name="arRemark", length = 100)
	@NotBlank(message = "Ar Remark can not be null !", groups = ArInsert.class)
	@NotEmpty(message = "Ar Remark can not be empty !", groups = ArInsert.class)
   	private String arRemark;
	
	@Transient
	private Boolean dirty;
	
	public String getCnmtBillPermission() {
		return cnmtBillPermission;
	}

	public void setCnmtBillPermission(String cnmtBillPermission) {
		this.cnmtBillPermission = cnmtBillPermission;
	}

	public String getCnmtProductType() {
		return cnmtProductType;
	}

	public void setCnmtProductType(String cnmtProductType) {
		this.cnmtProductType = cnmtProductType;
	}

	public double getCnmtTOT() {
		return cnmtTOT;
	}

	public void setCnmtTOT(double cnmtTOT) {
		this.cnmtTOT = cnmtTOT;
	}

/*	public String getCnmtCG() {
		return cnmtCG;
	}

	public void setCnmtCG(String cnmtCG) {
		this.cnmtCG = cnmtCG;
	}*/

	public String getCnmtDC() {
		return cnmtDC;
	}

	public void setCnmtDC(String cnmtDC) {
		this.cnmtDC = cnmtDC;
	}

	public String getCnmtVehicleType() {
		return cnmtVehicleType;
	}

	public void setCnmtVehicleType(String cnmtVehicleType) {
		this.cnmtVehicleType = cnmtVehicleType;
	}

	public String getCnmtFromSt() {
		return cnmtFromSt;
	}

	public void setCnmtFromSt(String cnmtFromSt) {
		this.cnmtFromSt = cnmtFromSt;
	}

	public String getCnmtToSt() {
		return cnmtToSt;
	}

	public void setCnmtToSt(String cnmtToSt) {
		this.cnmtToSt = cnmtToSt;
	}

	public Double getCnmtRate() {
		return cnmtRate;
	}

	public void setCnmtRate(Double cnmtRate) {
		this.cnmtRate = cnmtRate;
	}

	/*public Blob getCnmtConfirmImage() {
		return cnmtConfirmImage;
	}

	public void setCnmtConfirmImage(Blob cnmtConfirmImage) {
		this.cnmtConfirmImage = cnmtConfirmImage;
	}

	public Blob getCnmtImage() {
		return cnmtImage;
	}

	public void setCnmtImage(Blob cnmtImage) {
		this.cnmtImage = cnmtImage;
	}*/

	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}
	
	
	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

    public int getIsDone() {
		return isDone;
	}

	public void setIsDone(int isDone) {
		this.isDone = isDone;
	}
	
	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
	
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public int getCnmtId() {
		return cnmtId;
	}

	public void setCnmtId(int cnmtId) {
		this.cnmtId = cnmtId;
	}

	public String getCnmtCode() {
		return cnmtCode;
	}

	public void setCnmtCode(String cnmtCode) {
		this.cnmtCode = cnmtCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public Date getCnmtDt() {
		return cnmtDt;
	}

	public void setCnmtDt(Date cnmtDt) {
		this.cnmtDt = cnmtDt;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getCnmtConsignor() {
		return cnmtConsignor;
	}

	public void setCnmtConsignor(String cnmtConsignor) {
		this.cnmtConsignor = cnmtConsignor;
	}

	public String getCnmtConsignee() {
		return cnmtConsignee;
	}

	public void setCnmtConsignee(String cnmtConsignee) {
		this.cnmtConsignee = cnmtConsignee;
	}

	public String getContractCode() {
		return contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	public int getCnmtNoOfPkg() {
		return cnmtNoOfPkg;
	}

	public void setCnmtNoOfPkg(int cnmtNoOfPkg) {
		this.cnmtNoOfPkg = cnmtNoOfPkg;
	}

	public double getCnmtActualWt() {
		return cnmtActualWt;
	}

	public void setCnmtActualWt(double cnmtActualWt) {
		this.cnmtActualWt = cnmtActualWt;
	}
	
	public double getCnmtGuaranteeWt() {
			return cnmtGuaranteeWt;
	}

	public void setCnmtGuaranteeWt(double cnmtGuaranteeWt) {
			this.cnmtGuaranteeWt = cnmtGuaranteeWt;
	}

	public String getCnmtPayAt() {
		return cnmtPayAt;
	}

	public void setCnmtPayAt(String cnmtPayAt) {
		this.cnmtPayAt = cnmtPayAt;
	}

	public String getCnmtBillAt() {
		return cnmtBillAt;
	}

	public void setCnmtBillAt(String cnmtBillAt) {
		this.cnmtBillAt = cnmtBillAt;
	}

	public double getCnmtFreight() {
		return cnmtFreight;
	}

	public void setCnmtFreight(double cnmtFreight) {
		this.cnmtFreight = cnmtFreight;
	}

	public double getCnmtVOG() {
		return cnmtVOG;
	}

	public void setCnmtVOG(double cnmtVOG) {
		this.cnmtVOG = cnmtVOG;
	}

	public int getCnmtExtraExp() {
		return cnmtExtraExp;
	}

	public void setCnmtExtraExp(int cnmtExtraExp) {
		this.cnmtExtraExp = cnmtExtraExp;
	}

	public Date getCnmtDtOfDly() {
		return cnmtDtOfDly;
	}

	public void setCnmtDtOfDly(Date cnmtDtOfDly) {
		this.cnmtDtOfDly = cnmtDtOfDly;
	}

	public String getCnmtEmpCode() {
		return cnmtEmpCode;
	}

	public void setCnmtEmpCode(String cnmtEmpCode) {
		this.cnmtEmpCode = cnmtEmpCode;
	}

	public double getCnmtKm() {
		return cnmtKm;
	}

	public void setCnmtKm(double cnmtKm) {
		this.cnmtKm = cnmtKm;
	}

	public String getCnmtState() {
		return cnmtState;
	}

	public void setCnmtState(String cnmtState) {
		this.cnmtState = cnmtState;
	}

	public ArrayList<Map<String,Object>> getCnmtInvoiceNo() {
	return cnmtInvoiceNo;
	}

	public void setCnmtInvoiceNo(ArrayList<Map<String,Object>> cnmtInvoiceNo) {
	this.cnmtInvoiceNo = cnmtInvoiceNo;
	}

	public int getCmId() {
		return cmId;
	}

	public void setCmId(int cmId) {
		this.cmId = cmId;
	}

	public double getCnmtSrtg() {
		return cnmtSrtg;
	}

	public void setCnmtSrtg(double cnmtSrtg) {
		this.cnmtSrtg = cnmtSrtg;
	}

	public boolean isTrans() {
		return isTrans;
	}

	public void setTrans(boolean isTrans) {
		this.isTrans = isTrans;
	}

	public String getCnmtBillNo() {
		return cnmtBillNo;
	}

	public void setCnmtBillNo(String cnmtBillNo) {
		this.cnmtBillNo = cnmtBillNo;
	}

	public int getCnmtBillDetId() {
		return cnmtBillDetId;
	}

	public void setCnmtBillDetId(int cnmtBillDetId) {
		this.cnmtBillDetId = cnmtBillDetId;
	}

	public ArrayList<String> getCnmtSupBillNo() {
		return cnmtSupBillNo;
	}

	public void setCnmtSupBillNo(ArrayList<String> cnmtSupBillNo) {
		this.cnmtSupBillNo = cnmtSupBillNo;
	}

	public ArrayList<String> getCnmtSupBillDet() {
		return cnmtSupBillDet;
	}

	public void setCnmtSupBillDet(ArrayList<String> cnmtSupBillDet) {
		this.cnmtSupBillDet = cnmtSupBillDet;
	}

	public boolean isCnmtEdit() {
		return cnmtEdit;
	}

	public void setCnmtEdit(boolean cnmtEdit) {
		this.cnmtEdit = cnmtEdit;
	}

	public String getCnmtType() {
		return cnmtType;
	}

	public void setCnmtType(String cnmtType) {
		this.cnmtType = cnmtType;
	}

	public String getCnmtTpNo() {
		return cnmtTpNo;
	}

	public void setCnmtTpNo(String cnmtTpNo) {
		this.cnmtTpNo = cnmtTpNo;
	}

	public Boolean getDirty() {
		return dirty;
	}

	public void setDirty(Boolean dirty) {
		this.dirty = dirty;
	}

	public String getIsCCA() {
		return isCCA;
	}

	public void setIsCCA(String isCCA) {
		this.isCCA = isCCA;
	}

	public boolean isCancel() {
		return isCancel;
	}

	public void setCancel(boolean isCancel) {
		this.isCancel = isCancel;
	}

	public double getCnmtCost() {
		return cnmtCost;
	}

	public void setCnmtCost(double cnmtCost) {
		this.cnmtCost = cnmtCost;
	}

	public double getCnmtPL() {
		return cnmtPL;
	}

	public void setCnmtPL(double cnmtPL) {
		this.cnmtPL = cnmtPL;
	}

	public double getBilAmt() {
		return bilAmt;
	}

	public void setBilAmt(double bilAmt) {
		this.bilAmt = bilAmt;
	}

	public float getCostPer() {
		return costPer;
	}

	public void setCostPer(float costPer) {
		this.costPer = costPer;
	}


	public double getLhpvAmt() {
		return lhpvAmt;
	}

	public void setLhpvAmt(double lhpvAmt) {
		this.lhpvAmt = lhpvAmt;
	}

	public double getLhpvRcvry() {
		return lhpvRcvry;
	}

	public void setLhpvRcvry(double lhpvRcvry) {
		this.lhpvRcvry = lhpvRcvry;
	}
	
	public interface CnmtBillInsert{		
	}

	
	
	public String getRelType() {
		return relType;
	}

	public void setRelType(String relType) {
		this.relType = relType;
	}

	public Integer getWhCode() {
		return whCode;
	}

	public void setWhCode(Integer whCode) {
		this.whCode = whCode;
	}

	public String getRelChln() {
		return relChln;
	}

	public void setRelChln(String relChln) {
		this.relChln = relChln;
	}

	public String getPrBlCode() {
		return prBlCode;
	}

	public void setPrBlCode(String prBlCode) {
		this.prBlCode = prBlCode;
	}

	public Date getCnmtGtOutDt() {
		return cnmtGtOutDt;
	}

	public void setCnmtGtOutDt(Date cnmtGtOutDt) {
		this.cnmtGtOutDt = cnmtGtOutDt;
	}

	public String getArRemark() {
		return arRemark;
	}

	public void setArRemark(String arRemark) {
		this.arRemark = arRemark;
	}
	
	public interface ArInsert{}
	
}

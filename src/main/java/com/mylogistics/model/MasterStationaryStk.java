package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CollectionId;

@Entity
@Table(name = "master_stationary_stk")
public class MasterStationaryStk {
	
	public interface ValidationForInsert{
	}
	public interface ValidationForIssueing{
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "mstrStnStkId", length = 11)		
	private Integer mstrStnStkId;
	
	@NotNull(groups = ValidationForInsert.class)	
	@Column(name = "mstrStnStkStartNo", length = 100)	
	private Long mstrStnStkStartNo;
		
	@NotNull(groups = ValidationForInsert.class)
	@Column(name = "mstrStnStkbCode", length = 11)	
	private Integer mstrStnStkbCode;
	
	@NotNull(groups = ValidationForInsert.class)
	@Column(name = "mstrStnStkStatus", length = 4)	
	private String mstrStnStkStatus;
	
	@NotNull(groups = ValidationForInsert.class)
	@Column(name = "isIssued", length = 3)	
	private String isIssued;
	
	@Column(name = "issuedBranchCode", length = 11)
	@NotNull(groups = ValidationForIssueing.class)
	private Integer issuedBranchCode;
	
	@Column(name = "userCode", length = 11)
	@NotNull(groups = ValidationForInsert.class)
	private Integer userCode;
	
	@Column(name = "isReceived", length = 3)	
	private String isReceived;
	
	@Column(name = "isDirty", length = 3)
	private String isDirty;
	
	@Column(name = "creationTS", insertable = false, updatable = true)
	private Calendar creationTS;
	
	@ManyToOne
	@JoinColumn(name = "stnId")
	private Stationary stationary;
	
	
	
	public Integer getMstrStnStkId() {
		return mstrStnStkId;
	}
	public void setMstrStnStkId(Integer mstrStnStkId) {
		this.mstrStnStkId = mstrStnStkId;
	}
	public Long getMstrStnStkStartNo() {
		return mstrStnStkStartNo;
	}
	public void setMstrStnStkStartNo(Long mstrStnStkStartNo) {
		this.mstrStnStkStartNo = mstrStnStkStartNo;
	}
	
	public Integer getMstrStnStkbCode() {
		return mstrStnStkbCode;
	}
	public void setMstrStnStkbCode(Integer mstrStnStkbCode) {
		this.mstrStnStkbCode = mstrStnStkbCode;
	}
	public String getMstrStnStkStatus() {
		return mstrStnStkStatus;
	}
	public void setMstrStnStkStatus(String mstrStnStkStatus) {
		this.mstrStnStkStatus = mstrStnStkStatus;
	}
	public String getIsIssued() {
		return isIssued;
	}
	public void setIsIssued(String isIssued) {
		this.isIssued = isIssued;
	}
	public Integer getIssuedBranchCode() {
		return issuedBranchCode;
	}
	public void setIssuedBranchCode(Integer issuedBranchCode) {
		this.issuedBranchCode = issuedBranchCode;
	}
	public Integer getUserCode() {
		return userCode;
	}
	public void setUserCode(Integer userCode) {
		this.userCode = userCode;
	}
	public Calendar getCreationTS() {
		return creationTS;
	}
	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
	public Stationary getStationary() {
		return stationary;
	}
	public void setStationary(Stationary stationary) {
		this.stationary = stationary;
	}
	public String getIsReceived() {
		return isReceived;
	}
	public void setIsReceived(String isReceived) {
		this.isReceived = isReceived;
	}
	public String getIsDirty() {
		return isDirty;
	}
	public void setIsDirty(String isDirty) {
		this.isDirty = isDirty;
	}
	
	
}
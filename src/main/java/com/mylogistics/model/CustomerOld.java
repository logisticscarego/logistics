package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="customerold")
public class CustomerOld {
	
	@Id
	@Column(name="custOldId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int custOldId;
	
	@Column(name="custId")
	private int custId;
	 
	@Column(name="custCode")
	private String custCode;
	
	@Column(name="custName")
	private String custName;
	
	@Column(name="custAdd")
	private String custAdd;
	
	@Column(name="custCity")
	private String custCity;
	
	@Column(name="custState")
	private String custState;
	
	@Column(name="custPin")
	private String custPin;
	
	@Column(name="custTdsCircle")
	private String custTdsCircle;
	
	@Column(name="custTdsCity")
	private String custTdsCity;
	
	@Column(name="custTinNo")
	private String custTinNo;
	
	@Column(name="custStatus")
	private String custStatus;
	
	@Column(name="custPanNo")
	private String custPanNo;
	
	@Column(name="custIsDailyContAllow")
	private String custIsDailyContAllow;

	@Column(name="userCode")
	private String userCode;
	
	@Column(name="isVerify")
	private String isVerify;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="isView")
	private boolean isView;
	
	@Column(name="branchCode")
	private String branchCode;
	
	@Column(name="custDirector")
	private String custDirector;
	
	@Column(name="custMktHead")
	private String custMktHead;
	
	@Column(name="custCorpExc")
	private String custCorpExc;
	
	@Column(name="custBrPerson")
	private String custBrPerson;
	
	@Column(name="custCrBase")
	private String custCrBase;
	
	@Column(name="custCrPeriod")
	private int custCrPeriod;
	
	@Column(name="custSrvTaxBy")
	private char custSrvTaxBy;
	
	@Column(name="custBillCycle")
	private char custBillCycle;
	
	@Column(name="custBillValue")
	private int custBillValue;

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustAdd() {
		return custAdd;
	}

	public void setCustAdd(String custAdd) {
		this.custAdd = custAdd;
	}

	public String getCustCity() {
		return custCity;
	}

	public void setCustCity(String custCity) {
		this.custCity = custCity;
	}

	public String getCustState() {
		return custState;
	}

	public void setCustState(String custState) {
		this.custState = custState;
	}

	public String getCustPin() {
		return custPin;
	}

	public void setCustPin(String custPin) {
		this.custPin = custPin;
	}

	public String getCustTdsCircle() {
		return custTdsCircle;
	}

	public void setCustTdsCircle(String custTdsCircle) {
		this.custTdsCircle = custTdsCircle;
	}

	public String getCustTdsCity() {
		return custTdsCity;
	}

	public void setCustTdsCity(String custTdsCity) {
		this.custTdsCity = custTdsCity;
	}

	public String getCustTinNo() {
		return custTinNo;
	}

	public void setCustTinNo(String custTinNo) {
		this.custTinNo = custTinNo;
	}

	public String getCustStatus() {
		return custStatus;
	}

	public void setCustStatus(String custStatus) {
		this.custStatus = custStatus;
	}

	public String getCustPanNo() {
		return custPanNo;
	}

	public void setCustPanNo(String custPanNo) {
		this.custPanNo = custPanNo;
	}

	public String getCustIsDailyContAllow() {
		return custIsDailyContAllow;
	}

	public void setCustIsDailyContAllow(String custIsDailyContAllow) {
		this.custIsDailyContAllow = custIsDailyContAllow;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getIsVerify() {
		return isVerify;
	}

	public void setIsVerify(String isVerify) {
		this.isVerify = isVerify;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getCustDirector() {
		return custDirector;
	}

	public void setCustDirector(String custDirector) {
		this.custDirector = custDirector;
	}

	public String getCustMktHead() {
		return custMktHead;
	}

	public void setCustMktHead(String custMktHead) {
		this.custMktHead = custMktHead;
	}

	public String getCustCorpExc() {
		return custCorpExc;
	}

	public void setCustCorpExc(String custCorpExc) {
		this.custCorpExc = custCorpExc;
	}

	public String getCustBrPerson() {
		return custBrPerson;
	}

	public void setCustBrPerson(String custBrPerson) {
		this.custBrPerson = custBrPerson;
	}

	public String getCustCrBase() {
		return custCrBase;
	}

	public void setCustCrBase(String custCrBase) {
		this.custCrBase = custCrBase;
	}

	public int getCustCrPeriod() {
		return custCrPeriod;
	}

	public void setCustCrPeriod(int custCrPeriod) {
		this.custCrPeriod = custCrPeriod;
	}

	public char getCustSrvTaxBy() {
		return custSrvTaxBy;
	}

	public void setCustSrvTaxBy(char custSrvTaxBy) {
		this.custSrvTaxBy = custSrvTaxBy;
	}

	public char getCustBillCycle() {
		return custBillCycle;
	}

	public void setCustBillCycle(char custBillCycle) {
		this.custBillCycle = custBillCycle;
	}

	public int getCustBillValue() {
		return custBillValue;
	}

	public void setCustBillValue(int custBillValue) {
		this.custBillValue = custBillValue;
	}
	
	
}

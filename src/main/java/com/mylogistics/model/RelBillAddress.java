package com.mylogistics.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="relbilladdress")
public class RelBillAddress {
	
	@Id
	@Column(name="baId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int baId;

	@Column(name="custName")
	private String custName;//cmpltAddress
		
		@Column(name="stateCode")
		private Integer stateCode;
		
		@Column(name="gstType")
		private String gstType;
		
		@Column(name="gstNo")
		private String gstNo;
		
		@Column(name="arnNo")
		private String arnNo;
		
		@Column(name="buildingNo")
		private String buildingNo;
		
		@Column(name="floor")
		private String floor;
		
		@Column(name="buildingName")
		private String buildingName;
		
		@Column(name="galiNo")
		private String galiNo;
		
		@Column(name="locality")
		private String locality;
		
		@Column(name="city")
		private String city;
		
		@Column(name="pin")
		private Integer pin;

		@Column(name="stateRelPreFix")
		private String stateRelPreFix;
		
		@Column(name="cmpltAddress")
		private String cmpltAddress;
		
		
		public int getBaId() {
			return baId;
		}

		public void setBaId(int baId) {
			this.baId = baId;
		}

		public String getCustName() {
			return custName;
		}

		public void setCustName(String custName) {
			this.custName = custName;
		}

		public Integer getStateCode() {
			return stateCode;
		}

		public void setStateCode(Integer stateCode) {
			this.stateCode = stateCode;
		}

		public String getGstType() {
			return gstType;
		}

		public void setGstType(String gstType) {
			this.gstType = gstType;
		}

		public String getGstNo() {
			return gstNo;
		}

		public void setGstNo(String gstNo) {
			this.gstNo = gstNo;
		}

		public String getArnNo() {
			return arnNo;
		}

		public void setArnNo(String arnNo) {
			this.arnNo = arnNo;
		}

		public String getBuildingNo() {
			return buildingNo;
		}

		public void setBuildingNo(String buildingNo) {
			this.buildingNo = buildingNo;
		}

		public String getFloor() {
			return floor;
		}

		public void setFloor(String floor) {
			this.floor = floor;
		}

		public String getBuildingName() {
			return buildingName;
		}

		public void setBuildingName(String buildingName) {
			this.buildingName = buildingName;
		}

		public String getGaliNo() {
			return galiNo;
		}

		public void setGaliNo(String galiNo) {
			this.galiNo = galiNo;
		}

		public String getLocality() {
			return locality;
		}

		public void setLocality(String locality) {
			this.locality = locality;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public Integer getPin() {
			return pin;
		}

		public void setPin(Integer pin) {
			this.pin = pin;
		}

		public String getStateRelPreFix() {
			return stateRelPreFix;
		}

		public void setStateRelPreFix(String stateRelPreFix) {
			this.stateRelPreFix = stateRelPreFix;
		}

		public String getCmpltAddress() {
			return cmpltAddress;
		}

		public void setCmpltAddress(String cmpltAddress) {
			this.cmpltAddress = cmpltAddress;
		}
		
		
}

package com.mylogistics.model;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cnmtimage")
public class CnmtImage {

	@Id
	@Column(name="cmId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int cmId;
	
	@Column(name="cnmtId")
	private int cnmtId;
	
	@Column(name="bCode")
	private String bCode;

	@Column(name="userCode")
	private String userCode;
	
	@Column(name="cnmtImgPath")
	private String cnmtImgPath;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="arImgPath" , columnDefinition="longblob")
	private HashMap<Integer,String> arImgPath ;
	

	public HashMap<Integer, String> getArImgPath() {
		return arImgPath;
	}

	public void setArImgPath(HashMap<Integer, String> arImgPath) {
		this.arImgPath = arImgPath;
	}

	public int getCmId() {
		return cmId;
	}

	public void setCmId(int cmId) {
		this.cmId = cmId;
	}

	public int getCnmtId() {
		return cnmtId;
	}

	public void setCnmtId(int cnmtId) {
		this.cnmtId = cnmtId;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getCnmtImgPath() {
		return cnmtImgPath;
	}

	public void setCnmtImgPath(String cnmtImgPath) {
		this.cnmtImgPath = cnmtImgPath;
	}

	
	
	
}

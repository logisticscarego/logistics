package com.mylogistics.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "dlycontauth")
public class DlyContAuth {

	@Id
	@Column(name="dcaId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int dcaId;
	
	@Column(name="dcaCustCode")
	private String dcaCustCode;
	
	@Column(name="dcaFDt")
	private Date dcaFDt;
	
	@Column(name="dcaTDt")
	private Date dcaTDt;
	
	@Column(name="dcaStatList")
	private ArrayList<String> dcaStatList;
	
	@Column(name="dcaIsActive")
	private boolean dcaIsActive;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getDcaId() {
		return dcaId;
	}

	public void setDcaId(int dcaId) {
		this.dcaId = dcaId;
	}

	public String getDcaCustCode() {
		return dcaCustCode;
	}

	public void setDcaCustCode(String dcaCustCode) {
		this.dcaCustCode = dcaCustCode;
	}

	public Date getDcaFDt() {
		return dcaFDt;
	}

	public void setDcaFDt(Date dcaFDt) {
		this.dcaFDt = dcaFDt;
	}

	public Date getDcaTDt() {
		return dcaTDt;
	}

	public void setDcaTDt(Date dcaTDt) {
		this.dcaTDt = dcaTDt;
	}

	
	public ArrayList<String> getDcaStatList() {
		return dcaStatList;
	}

	public void setDcaStatList(ArrayList<String> dcaStatList) {
		this.dcaStatList = dcaStatList;
	}

	public boolean isDcaIsActive() {
		return dcaIsActive;
	}

	public void setDcaIsActive(boolean dcaIsActive) {
		this.dcaIsActive = dcaIsActive;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
	
}

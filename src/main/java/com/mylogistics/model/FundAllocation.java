package com.mylogistics.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@SuppressWarnings("serial")
@Entity
@Table(name="fundallocation")
public class FundAllocation implements Serializable {
	
	@Id
	@GeneratedValue
	@Column(name="faId")
	private int faId;
	
	@Column(name="bCode", length=3,nullable=false)
	private String bCode;
	
	@Column(name="isGenerated", length=3)
	private String isGenerated;
	
	@Column(name="amount", length=128)
	private double amount;
	
	@Column(name="tdsAmt", columnDefinition="double default 0")
	private double tdsAmt;
	
	@Column(name="transactionType",length=128)
	private String transactionType;
	
	@Column(name="beneficiaryCode",length=128)
	private String beneficiaryCode;
	
	@Column(name="beneficiaryAccountNo",length=128)
	private String beneficiaryAccountNo;
	
	@Column(name="instrumentAmount",length=128)
	private double instrumentAmount;
	
	@Column(name="beneficiaryName",length=128)
	private String beneficiaryName;
	
	@Column(name="draweeLocation",length=128)
	private String draweeLocation;
	
	@Column(name="printLocation",length=128)
	private String printLocation;
	
	@Column(name="beneAddress1",length=128)
	private String beneAddress1;
	
	@Column(name="beneAddress2",length=128)
	private String beneAddress2;
	
	@Column(name="beneAddress3",length=128)
	private String beneAddress3;
	
	@Column(name="beneAddress4",length=128)
	private String beneAddress4;
	
	@Column(name="beneAddress5",length=128)
	private String beneAddress5;
	
	@Column(name="instructionRefNo",length=128)
	private String instructionRefNo;
	
	@Column(name="customerRefNo",length=128)
	private String customerRefNo;
	
	@Column(name="paymentDetail1",length=128)
	private String paymentDetail1;
	
	@Column(name="paymentDetail2",length=128)
	private String paymentDetail2;
	
	@Column(name="paymentDetail3",length=128)
	private String paymentDetail3;
	
	@Column(name="paymentDetail4",length=128)
	private String paymentDetail4;
	
	@Column(name="paymentDetail5",length=128)
	private String paymentDetail5;
	
	@Column(name="paymentDetail6",length=128)
	private String paymentDetail6;
	
	@Column(name="paymentDetail7",length=128)
	private String paymentDetail7;
	
	@Column(name="chequeNo",length=128)
	private String chequeNo;
	
	@Column(name="chequeTmDate")
	private Calendar chequeTmDate;
	
	@Column(name="micrNo",length=128)
	private String micrNo;
	
	@Column(name="ifscCode",length=128)
	private String ifscCode;
	
	@Column(name="beneBnkName",length=128)
	private String beneBnkName;
	
	@Column(name="beneBnkBranchName",length=128)
	private String beneBnkBranchName;
	
	@Column(name="beneficiaryEmailId",length=128)
	private String beneficiaryEmailId;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="userCode",length=5)
	private String userCode;
	
	@Column(name="staffCode",length=10)
	private String staffCode;
	
	@Column(name="payTo",length=10)
	private String payTo;
	
	@Column(name="rejected", columnDefinition="boolean default false")
	private boolean rejected;
	
	@Column(name="approved", columnDefinition="boolean default false")
	private boolean approved;
	
	@Column(name="allowed", columnDefinition="boolean default false")
	private boolean allowed;
	
	@JsonIgnore
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,targetEntity=FundAllocationDetails.class)
    @JoinColumn(name="fa_Id",referencedColumnName="faId")
    private List<FundAllocationDetails> fundAllocationDetails;
    
    public void setFundAllocationDetails(List<FundAllocationDetails> fundAllocationDetails) {
		this.fundAllocationDetails = fundAllocationDetails;
	}
    
    public List<FundAllocationDetails> getFundAllocationDetails() {
		return fundAllocationDetails;
	}

	public int getFaId() {
		return faId;
	}

	public void setFaId(int faId) {
		this.faId = faId;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getIsGenerated() {
		return isGenerated;
	}

	public void setIsGenerated(String isGenerated) {
		this.isGenerated = isGenerated;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getBeneficiaryCode() {
		return beneficiaryCode;
	}

	public void setBeneficiaryCode(String beneficiaryCode) {
		this.beneficiaryCode = beneficiaryCode;
	}

	public String getBeneficiaryAccountNo() {
		return beneficiaryAccountNo;
	}

	public void setBeneficiaryAccountNo(String beneficiaryAccountNo) {
		this.beneficiaryAccountNo = beneficiaryAccountNo;
	}

	public double getInstrumentAmount() {
		return instrumentAmount;
	}

	public void setInstrumentAmount(double instrumentAmount) {
		this.instrumentAmount = instrumentAmount;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getDraweeLocation() {
		return draweeLocation;
	}

	public void setDraweeLocation(String draweeLocation) {
		this.draweeLocation = draweeLocation;
	}

	public String getPrintLocation() {
		return printLocation;
	}

	public void setPrintLocation(String printLocation) {
		this.printLocation = printLocation;
	}

	public String getBeneAddress1() {
		return beneAddress1;
	}

	public void setBeneAddress1(String beneAddress1) {
		this.beneAddress1 = beneAddress1;
	}

	public String getBeneAddress2() {
		return beneAddress2;
	}

	public void setBeneAddress2(String beneAddress2) {
		this.beneAddress2 = beneAddress2;
	}

	public String getBeneAddress3() {
		return beneAddress3;
	}

	public void setBeneAddress3(String beneAddress3) {
		this.beneAddress3 = beneAddress3;
	}

	public String getBeneAddress4() {
		return beneAddress4;
	}

	public void setBeneAddress4(String beneAddress4) {
		this.beneAddress4 = beneAddress4;
	}

	public String getBeneAddress5() {
		return beneAddress5;
	}

	public void setBeneAddress5(String beneAddress5) {
		this.beneAddress5 = beneAddress5;
	}


	public String getInstructionRefNo() {
		return instructionRefNo;
	}

	public void setInstructionRefNo(String instructionRefNo) {
		this.instructionRefNo = instructionRefNo;
	}

	public String getCustomerRefNo() {
		return customerRefNo;
	}

	public void setCustomerRefNo(String customerRefNo) {
		this.customerRefNo = customerRefNo;
	}

	public String getPaymentDetail1() {
		return paymentDetail1;
	}

	public void setPaymentDetail1(String paymentDetail1) {
		this.paymentDetail1 = paymentDetail1;
	}

	public String getPaymentDetail2() {
		return paymentDetail2;
	}

	public void setPaymentDetail2(String paymentDetail2) {
		this.paymentDetail2 = paymentDetail2;
	}

	public String getPaymentDetail3() {
		return paymentDetail3;
	}

	public void setPaymentDetail3(String paymentDetail3) {
		this.paymentDetail3 = paymentDetail3;
	}

	public String getPaymentDetail4() {
		return paymentDetail4;
	}

	public void setPaymentDetail4(String paymentDetail4) {
		this.paymentDetail4 = paymentDetail4;
	}

	public String getPaymentDetail5() {
		return paymentDetail5;
	}

	public void setPaymentDetail5(String paymentDetail5) {
		this.paymentDetail5 = paymentDetail5;
	}

	public String getPaymentDetail6() {
		return paymentDetail6;
	}

	public void setPaymentDetail6(String paymentDetail6) {
		this.paymentDetail6 = paymentDetail6;
	}

	public String getPaymentDetail7() {
		return paymentDetail7;
	}

	public void setPaymentDetail7(String paymentDetail7) {
		this.paymentDetail7 = paymentDetail7;
	}

	public String getChequeNo() {
		return chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public Calendar getChequeTmDate() {
		return chequeTmDate;
	}

	public void setChequeTmDate(Calendar chequeTmDate) {
		this.chequeTmDate = chequeTmDate;
	}

	public String getMicrNo() {
		return micrNo;
	}

	public void setMicrNo(String micrNo) {
		this.micrNo = micrNo;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getBeneBnkName() {
		return beneBnkName;
	}

	public void setBeneBnkName(String beneBnkName) {
		this.beneBnkName = beneBnkName;
	}

	public String getBeneBnkBranchName() {
		return beneBnkBranchName;
	}

	public void setBeneBnkBranchName(String beneBnkBranchName) {
		this.beneBnkBranchName = beneBnkBranchName;
	}

	public String getBeneficiaryEmailId() {
		return beneficiaryEmailId;
	}

	public void setBeneficiaryEmailId(String beneficiaryEmailId) {
		this.beneficiaryEmailId = beneficiaryEmailId;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	
	public String getPayTo() {
		return payTo;
	}

	public void setPayTo(String payTo) {
		this.payTo = payTo;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isAllowed() {
		return allowed;
	}

	public void setAllowed(boolean allowed) {
		this.allowed = allowed;
	}

	public double getTdsAmt() {
		return tdsAmt;
	}

	public void setTdsAmt(double tdsAmt) {
		this.tdsAmt = tdsAmt;
	}
	
	
}

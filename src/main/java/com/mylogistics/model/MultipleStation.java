package com.mylogistics.model;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "multiplestation")

public class MultipleStation {
	@Id
	@Column(name="mulStnId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int mulStnId;
	
	@Column(name="mulStnStnCode")
	private String mulStnStnCode;
	
	@Column(name="mulStnRefCode")
	private String mulStnRefCode;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	@Column(name="bCode")
	private String bCode;
	
	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}


	public int getMulStnId() {
		return mulStnId;
	}

	public void setMulStnId(int mulStnId) {
		this.mulStnId = mulStnId;
	}

	public String getMulStnStnCode() {
		return mulStnStnCode;
	}

	public void setMulStnStnCode(String mulStnStnCode) {
		this.mulStnStnCode = mulStnStnCode;
	}

	public String getMulStnRefCode() {
		return mulStnRefCode;
	}

	public void setMulStnRefCode(String mulStnRefCode) {
		this.mulStnRefCode = mulStnRefCode;
	}

}

package com.mylogistics.model;

//import java.util.Date;
import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "hboptr_notification")
public class HBOptr_Notification {
	
	@Id
	@Column(name="hbonId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int hbonId;
	
	@Column(name="noCnmt")
	private String noCnmt;
	
	@Column(name="noChln")
	private String noChln;
	
	@Column(name="noSedr")
	private String noSedr;
	
	@Column(name="operatorCode")
	private String operatorCode;
	
	@Column(name="disBranchCode")
	private String disBranchCode;

	@Column(name="dispatchDate")
	private Date dispatchDate;
	
	@Column(name="dispatchCode")
	private String dispatchCode;


	public String getDispatchCode() {
		return dispatchCode;
	}

	public void setDispatchCode(String dispatchCode) {
		this.dispatchCode = dispatchCode;
	}

	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="isView")
	private boolean isView;
	
	public Date getDispatchDate() {
		return dispatchDate;
	}

	public void setDispatchDate(Date dispatchDate) {
		this.dispatchDate = dispatchDate;
	}
	
	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}

	/*public int getBrsGenId() {
		return brsGenId;
	}

	public void setBrsGenId(int brsGenId) {
		this.brsGenId = brsGenId;
	}*/

	public int getHbonId() {
		return hbonId;
	}

	public void setHbonId(int hbonId) {
		this.hbonId = hbonId;
	}

	public String getNoCnmt() {
		return noCnmt;
	}

	public void setNoCnmt(String noCnmt) {
		this.noCnmt = noCnmt;
	}

	public String getNoChln() {
		return noChln;
	}

	public void setNoChln(String noChln) {
		this.noChln = noChln;
	}

	public String getNoSedr() {
		return noSedr;
	}

	public void setNoSedr(String noSedr) {
		this.noSedr = noSedr;
	}

	public String getOperatorCode() {
		return operatorCode;
	}

	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}
	
	public String getDisBranchCode() {
		return disBranchCode;
	}

	public void setDisBranchCode(String disBranchCode) {
		this.disBranchCode = disBranchCode;
	}
	
}


package com.mylogistics.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "cashstmt")
public class CashStmt {
	
	@Id
	@Column(name="csId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int csId;
	
	//Many To One with CSS table
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JsonBackReference(value = "cssCs")
	@JoinColumn(name = "cssId") 
	@NotNull(message = "CashStmt csNo can not be null !", groups = CashStmtBillInsert.class)
	private CashStmtStatus csNo;
	
	@Column(name="csDescription")
	@NotNull(message = "CashStmt description can not be null !", groups = CashStmtBillInsert.class)
	@NotEmpty(message = "CashStmt description can not be empty !", groups = CashStmtBillInsert.class)
	private String csDescription;
	
	@Column(name="csTvNo")
	@NotNull(message = "CashStmt csTvNo can not be null !", groups = CashStmtBillInsert.class)
	@NotEmpty(message = "CashStmt csTvNo can not be empty !", groups = CashStmtBillInsert.class)
	private String csTvNo;
	
	@Column(name="csDrCr")
	@NotNull(message = "CashStmt CsDrCr description can not be null !", groups = CashStmtBillInsert.class)
	private char csDrCr;
	
	//Common field (chequeLeaves)
	@Column(name="csAmt")
	@Min(value = 1, message = "CashStmt CsAmt should be greater than 0 !",  groups = CashStmtBillInsert.class)
	private double csAmt;
	
	@Column(name="csVouchNo")
	@Min(value = 1, message = "CashStmt csVouchNo should be greater than 0 !", groups = CashStmtBillInsert.class)
	private int csVouchNo;
	
	@Column(name="csVouchType")
	@NotNull(message = "CashStmt csVoucherType can not be null !", groups = CashStmtBillInsert.class)
	@NotEmpty(message = "CashStmt csVoucherType can not be empty !", groups = CashStmtBillInsert.class)
	private String csVouchType;
	
	@Column(name="csPayTo")
	private String csPayTo;
	
	@Column(name="csDt")
	@NotNull(message = "CashStmt csDt can not be null !", groups = CashStmtBillInsert.class)
	private Date csDt;

	@Column(name="userCode")
	@NotNull(message = "CashStmt userCode can not be null !", groups = CashStmtBillInsert.class)
	@NotEmpty(message = "CashStmt userCode can not be empty !", groups = CashStmtBillInsert.class)
	private String userCode;
	
	@Column(name="bCode")
	@NotNull(message = "CashStmt bCode can not be null !", groups = CashStmtBillInsert.class)
	@NotEmpty(message = "CashStmt bCode can not be empty !", groups = CashStmtBillInsert.class)
	private String bCode;
			
	@Column(name ="csFaCode")
	@NotNull(message = "CashStmt csFaCode can not be null !", groups = CashStmtBillInsert.class)
	@NotEmpty(message = "CashStmt csFaCode can not be empty !", groups = CashStmtBillInsert.class)
	private String csFaCode;
	
	
	@Column(name ="csType")
	@NotNull(message = "CashStmt csType can not be null !", groups = CashStmtBillInsert.class)
	@NotEmpty(message = "CashStmt csType can not be empty !", groups = CashStmtBillInsert.class)
	private String csType;
	
	//Specific for Cash Withdrawal
	@Column(name="csChequeType")
	private char csChequeType;
	
	//Specific for Cash Receipt
	@Column(name="csMrNo")
	private String csMrNo;
	
	@Column(name="csLhpvTempNo")
	private String csLhpvTempNo;
	
	@Column(name = "csIsClose")
	private boolean csIsClose;
	
	@Column(name = "csIsVRev" , columnDefinition = "boolean default false")
	private boolean csIsVRev;
	
	@Column(name = "csSFId" , columnDefinition = "int default -1")
	private int csSFId; 
	
	@Column(name = "csUpdate" , columnDefinition = "boolean default false")
	private boolean csUpdate;
	
	
	@Column(name = "csTravIdList")	
	private ArrayList<Integer> csTravIdList = new ArrayList<Integer>();

	
	@Column(name="payMode")	
	private String payMode;
	
	@Transient
	private String fromBrName;
	
	@Transient
	private String toBrName;
	
	@Transient
	private Date toBrCsCloseDt;
	
	@Transient
	private String csChqNo;

	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	
	public int getCsSFId() {
		return csSFId;
	}


	public void setCsSFId(int csSFId) {
		this.csSFId = csSFId;
	}

	
	public ArrayList<Integer> getCsTravIdList() {
		return csTravIdList;
	}


	public void setCsTravIdList(ArrayList<Integer> csTravIdList) {
		this.csTravIdList = csTravIdList;
	}
	

	public boolean isCsIsVRev() {
		return csIsVRev;
	}


	public void setCsIsVRev(boolean csIsVRev) {
		this.csIsVRev = csIsVRev;
	}
	
	public Date getCsDt() {
		return csDt;
	}


	public void setCsDt(java.sql.Date date) {
		this.csDt = date;
	}


	public int getCsId() {
		return csId;
	}


	public void setCsId(int csId) {
		this.csId = csId;
	}


	public CashStmtStatus getCsNo() {
		return csNo;
	}


	public void setCsNo(CashStmtStatus csNo) {
		this.csNo = csNo;
	}


	public String getCsDescription() {
		return csDescription;
	}


	public void setCsDescription(String csDescription) {
		this.csDescription = csDescription;
	}


	public String getCsTvNo() {
		return csTvNo;
	}


	public void setCsTvNo(String csTvNo) {
		this.csTvNo = csTvNo;
	}


	public char getCsDrCr() {
		return csDrCr;
	}


	public void setCsDrCr(char csDrCr) {
		this.csDrCr = csDrCr;
	}


	public double getCsAmt() {
		return csAmt;
	}


	public void setCsAmt(double csAmt) {
		this.csAmt = csAmt;
	}


	/*public ChequeLeaves getcLeaves() {
		return cLeaves;
	}


	public void setcLeaves(ChequeLeaves cLeaves) {
		this.cLeaves = cLeaves;
	}*/


	public int getCsVouchNo() {
		return csVouchNo;
	}


	public void setCsVouchNo(int csVouchNo) {
		this.csVouchNo = csVouchNo;
	}


	public String getCsVouchType() {
		return csVouchType;
	}


	public void setCsVouchType(String csVouchType) {
		this.csVouchType = csVouchType;
	}


	public String getCsPayTo() {
		return csPayTo;
	}


	public void setCsPayTo(String csPayTo) {
		this.csPayTo = csPayTo;
	}


	public String getUserCode() {
		return userCode;
	}


	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}


	public String getbCode() {
		return bCode;
	}


	public void setbCode(String bCode) {
		this.bCode = bCode;
	}


	/*public FAMaster getFaMaster() {
		return faMaster;
	}


	public void setFaMaster(FAMaster faMaster) {
		this.faMaster = faMaster;
	}*/


	public String getCsType() {
		return csType;
	}


	public void setCsType(String csType) {
		this.csType = csType;
	}


	public char getCsChequeType() {
		return csChequeType;
	}


	public void setCsChequeType(char csChequeType) {
		this.csChequeType = csChequeType;
	}


	public String getCsMrNo() {
		return csMrNo;
	}


	public void setCsMrNo(String csMrNo) {
		this.csMrNo = csMrNo;
	}


	public Calendar getCreationTS() {
		return creationTS;
	}


	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}


	public boolean isCsIsClose() {
		return csIsClose;
	}


	public void setCsIsClose(boolean csIsClose) {
		this.csIsClose = csIsClose;
	}


	public String getCsFaCode() {
		return csFaCode;
	}


	public void setCsFaCode(String csFaCode) {
		this.csFaCode = csFaCode;
	}


	public String getCsLhpvTempNo() {
		return csLhpvTempNo;
	}


	public void setCsLhpvTempNo(String csLhpvTempNo) {
		this.csLhpvTempNo = csLhpvTempNo;
	}

	public boolean isCsUpdate() {
		return csUpdate;
	}

	public void setCsUpdate(boolean csUpdate) {
		this.csUpdate = csUpdate;
	}


	public String getFromBrName() {
		return fromBrName;
	}


	public void setFromBrName(String fromBrName) {
		this.fromBrName = fromBrName;
	}


	public String getToBrName() {
		return toBrName;
	}


	public void setToBrName(String toBrName) {
		this.toBrName = toBrName;
	}


	public Date getToBrCsCloseDt() {
		return toBrCsCloseDt;
	}


	public void setToBrCsCloseDt(Date toBrCsCloseDt) {
		this.toBrCsCloseDt = toBrCsCloseDt;
	}


	public String getPayMode() {
		return payMode;
	}


	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}


	public String getCsChqNo() {
		return csChqNo;
	}


	public void setCsChqNo(String csChqNo) {
		this.csChqNo = csChqNo;
	}

	public interface CashStmtBillInsert{
	}
	
	
}

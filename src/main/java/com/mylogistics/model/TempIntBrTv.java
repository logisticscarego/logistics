package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "tempintbrtv")
public class TempIntBrTv {

	@Id
	@Column(name="tibId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int tibId;
	
	@Column(name="tibBranchCode")
	private String tibBranchCode;
	
	@Column(name="tibCsId")
	private int tibCsId;
	
	@Column(name="tibFaCode")
	private String tibFaCode;
	
	@Column(name="tibBnkFaCode")
	private String tibBnkFaCode;
	
	@Column(name="tibAmt")
	private double tibAmt;
	
	@Column(name="tibDOrC")
	private char tibDOrC;
	
	@Column(name="tibDesc" , columnDefinition="TEXT")
	private String tibDesc;
	
	@Column(name="tibIsClear")
	private boolean tibIsClear;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Transient
	private Date tibDate;
	
	@Transient
	private String tibDrBrName;
	
	@Transient
	private String tibCrBrName;

	public int getTibId() {
		return tibId;
	}

	public void setTibId(int tibId) {
		this.tibId = tibId;
	}

	public String getTibBranchCode() {
		return tibBranchCode;
	}

	public void setTibBranchCode(String tibBranchCode) {
		this.tibBranchCode = tibBranchCode;
	}

	public int getTibCsId() {
		return tibCsId;
	}

	public void setTibCsId(int tibCsId) {
		this.tibCsId = tibCsId;
	}

	public String getTibFaCode() {
		return tibFaCode;
	}

	public void setTibFaCode(String tibFaCode) {
		this.tibFaCode = tibFaCode;
	}

	public double getTibAmt() {
		return tibAmt;
	}

	public void setTibAmt(double tibAmt) {
		this.tibAmt = tibAmt;
	}

	public char getTibDOrC() {
		return tibDOrC;
	}

	public void setTibDOrC(char tibDOrC) {
		this.tibDOrC = tibDOrC;
	}

	public String getTibDesc() {
		return tibDesc;
	}

	public void setTibDesc(String tibDesc) {
		this.tibDesc = tibDesc;
	}

	public boolean isTibIsClear() {
		return tibIsClear;
	}

	public void setTibIsClear(boolean tibIsClear) {
		this.tibIsClear = tibIsClear;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getTibBnkFaCode() {
		return tibBnkFaCode;
	}

	public void setTibBnkFaCode(String tibBnkFaCode) {
		this.tibBnkFaCode = tibBnkFaCode;
	}

	public Date getTibDate() {
		return tibDate;
	}

	public void setTibDate(Date tibDate) {
		this.tibDate = tibDate;
	}

	public String getTibDrBrName() {
		return tibDrBrName;
	}

	public void setTibDrBrName(String tibDrBrName) {
		this.tibDrBrName = tibDrBrName;
	}

	public String getTibCrBrName() {
		return tibCrBrName;
	}

	public void setTibCrBrName(String tibCrBrName) {
		this.tibCrBrName = tibCrBrName;
	}
	
}

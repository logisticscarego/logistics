package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "branchstockleafdet")
public class BranchStockLeafDet {

	@Id
	@Column(name="brsLeafDetId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int brsLeafDetId;
	
	@Column(name="brsLeafDetBrCode")
	private String brsLeafDetBrCode;
	
	@Column(name="brsLeafDetStatus")
	private String brsLeafDetStatus;
	
	@Column(name="brsLeafDetSNo" , unique=true)
	private String brsLeafDetSNo;
	
	@Column(name="brsLeafDetDlyRt" , columnDefinition="boolean default false")
	private boolean brsLeafDetDlyRt;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;

	public int getBrsLeafDetId() {
		return brsLeafDetId;
	}

	public void setBrsLeafDetId(int brsLeafDetId) {
		this.brsLeafDetId = brsLeafDetId;
	}

	public String getBrsLeafDetBrCode() {
		return brsLeafDetBrCode;
	}

	public void setBrsLeafDetBrCode(String brsLeafDetBrCode) {
		this.brsLeafDetBrCode = brsLeafDetBrCode;
	}

	public String getBrsLeafDetStatus() {
		return brsLeafDetStatus;
	}

	public void setBrsLeafDetStatus(String brsLeafDetStatus) {
		this.brsLeafDetStatus = brsLeafDetStatus;
	}

	public String getBrsLeafDetSNo() {
		return brsLeafDetSNo;
	}

	public void setBrsLeafDetSNo(String brsLeafDetSNo) {
		this.brsLeafDetSNo = brsLeafDetSNo;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public boolean isBrsLeafDetDlyRt() {
		return brsLeafDetDlyRt;
	}

	public void setBrsLeafDetDlyRt(boolean brsLeafDetDlyRt) {
		this.brsLeafDetDlyRt = brsLeafDetDlyRt;
	}
	
	
}

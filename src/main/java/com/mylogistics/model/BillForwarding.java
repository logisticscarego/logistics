package com.mylogistics.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "billforwarding")
public class BillForwarding {

	
	@Id
	@Column(name="bfId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int bfId;
	
	@Column(name="bfNo")
	private String bfNo;
	
	@Column(name="bfBrhCode")
	private String bfBrhCode;
	
	@Column(name="bfImagePath")
	private String bfImagePath;
	
	@Column(name="bfCustId" , columnDefinition="int default -1")
	private int bfCustId;
	
	@Column(name="bfRecDt")
	private Date bfRecDt;
	
	@Column(name="bfTotAmt" , columnDefinition="double default 0.0")
	private double bfTotAmt;
	
	//One To Many with Bill
	@JsonIgnore
	@JsonManagedReference(value = "bfBill")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="billforwarding_bill",
	joinColumns=@JoinColumn(name="bfId"),
	inverseJoinColumns=@JoinColumn(name="blId"))
	private List<Bill> billList = new ArrayList<Bill>();
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="userCode")
	private String userCode;

	public int getBfId() {
		return bfId;
	}

	public void setBfId(int bfId) {
		this.bfId = bfId;
	}

	public String getBfNo() {
		return bfNo;
	}

	public void setBfNo(String bfNo) {
		this.bfNo = bfNo;
	}

	public String getBfBrhCode() {
		return bfBrhCode;
	}

	public void setBfBrhCode(String bfBrhCode) {
		this.bfBrhCode = bfBrhCode;
	}

	public int getBfCustId() {
		return bfCustId;
	}

	public void setBfCustId(int bfCustId) {
		this.bfCustId = bfCustId;
	}

	

	public List<Bill> getBillList() {
		return billList;
	}

	public void setBillList(List<Bill> billList) {
		this.billList = billList;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public double getBfTotAmt() {
		return bfTotAmt;
	}

	public void setBfTotAmt(double bfTotAmt) {
		this.bfTotAmt = bfTotAmt;
	}

	public Date getBfRecDt() {
		return bfRecDt;
	}

	public void setBfRecDt(Date bfRecDt) {
		this.bfRecDt = bfRecDt;
	}

	public String getBfImagePath() {
		return bfImagePath;
	}

	public void setBfImagePath(String bfImagePath) {
		this.bfImagePath = bfImagePath;
	}
	
	
}

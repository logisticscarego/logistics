package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "subtelephonemstr")
public class SubTelephoneMstr {

	@Id
	@Column(name="stmId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int stmId;
	
	//One To One
	//@OneToOne(cascade = CascadeType.ALL)
	@Column(name="stmCsId" ,  columnDefinition = "int default -1")
	private int stmCsId;
	
	// Many To One
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	//@JsonBackReference(value = "stmTelM")
	@JoinColumn(name = "tmId") 
	private TelephoneMstr telephoneMstr;
	
	@Column(name="stmPayAmt")
	private double stmPayAmt;
	
	@Column(name="stmBillFrDt")
	private Date stmBillFrDt;
	
	@Column(name="stmBillToDt")
	private Date stmBillToDt;
	
	@Column(name="stmTotDedAmt")
	private double stmTotDedAmt;
	
	@Column(name="stmSmsDedAmt")
	private double stmSmsDedAmt;
	
	@Column(name="stmCTDedAmt")
	private double stmCTDedAmt;
	
	@Column(name="stmDDedAmt")
	private double stmDDedAmt;
	
	@Column(name="stmGDedAmt")
	private double stmGDedAmt;
	
	@Column(name="stmPCDedAmt")
	private double stmPCDedAmt;
	
	@Column(name="stmRDedAmt")
	private double stmRDedAmt;
	
	@Column(name="stmIsdDedAmt")
	private double stmIsdDedAmt;
	
	@Column(name="stmTvNo")
	private String stmTvNo;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public String getStmTvNo() {
		return stmTvNo;
	}

	public void setStmTvNo(String stmTvNo) {
		this.stmTvNo = stmTvNo;
	}

	public int getStmId() {
		return stmId;
	}

	public void setStmId(int stmId) {
		this.stmId = stmId;
	}

	/*public CashStmt getCashStmt() {
		return cashStmt;
	}

	public void setCashStmt(CashStmt cashStmt) {
		this.cashStmt = cashStmt;
	}*/

	public TelephoneMstr getTelephoneMstr() {
		return telephoneMstr;
	}

	
	public int getStmCsId() {
		return stmCsId;
	}

	public void setStmCsId(int stmCsId) {
		this.stmCsId = stmCsId;
	}

	public void setTelephoneMstr(TelephoneMstr telephoneMstr) {
		this.telephoneMstr = telephoneMstr;
	}

	public double getStmPayAmt() {
		return stmPayAmt;
	}

	public void setStmPayAmt(double stmPayAmt) {
		this.stmPayAmt = stmPayAmt;
	}

	public Date getStmBillFrDt() {
		return stmBillFrDt;
	}

	public void setStmBillFrDt(Date stmBillFrDt) {
		this.stmBillFrDt = stmBillFrDt;
	}

	public Date getStmBillToDt() {
		return stmBillToDt;
	}

	public void setStmBillToDt(Date stmBillToDt) {
		this.stmBillToDt = stmBillToDt;
	}

	public double getStmTotDedAmt() {
		return stmTotDedAmt;
	}

	public void setStmTotDedAmt(double stmTotDedAmt) {
		this.stmTotDedAmt = stmTotDedAmt;
	}

	public double getStmSmsDedAmt() {
		return stmSmsDedAmt;
	}

	public void setStmSmsDedAmt(double stmSmsDedAmt) {
		this.stmSmsDedAmt = stmSmsDedAmt;
	}

	public double getStmCTDedAmt() {
		return stmCTDedAmt;
	}

	public void setStmCTDedAmt(double stmCTDedAmt) {
		this.stmCTDedAmt = stmCTDedAmt;
	}

	public double getStmDDedAmt() {
		return stmDDedAmt;
	}

	public void setStmDDedAmt(double stmDDedAmt) {
		this.stmDDedAmt = stmDDedAmt;
	}

	public double getStmGDedAmt() {
		return stmGDedAmt;
	}

	public void setStmGDedAmt(double stmGDedAmt) {
		this.stmGDedAmt = stmGDedAmt;
	}

	public double getStmPCDedAmt() {
		return stmPCDedAmt;
	}

	public void setStmPCDedAmt(double stmPCDedAmt) {
		this.stmPCDedAmt = stmPCDedAmt;
	}

	public double getStmRDedAmt() {
		return stmRDedAmt;
	}

	public void setStmRDedAmt(double stmRDedAmt) {
		this.stmRDedAmt = stmRDedAmt;
	}

	public double getStmIsdDedAmt() {
		return stmIsdDedAmt;
	}

	public void setStmIsdDedAmt(double stmIsdDedAmt) {
		this.stmIsdDedAmt = stmIsdDedAmt;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	
}

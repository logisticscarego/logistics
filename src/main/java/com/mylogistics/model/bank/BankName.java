package com.mylogistics.model.bank;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="bankname")
public class BankName {

	@Id
	@Column(name="bnkNameId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int bnkNameId;
	
	@Column(name="bnkName")
	private String bnkName;

	public int getBnkNameId() {
		return bnkNameId;
	}

	public void setBnkNameId(int bnkNameId) {
		this.bnkNameId = bnkNameId;
	}

	public String getBnkName() {
		return bnkName;
	}

	public void setBnkName(String bnkName) {
		this.bnkName = bnkName;
	}
}

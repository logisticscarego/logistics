package com.mylogistics.model.bank;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="branch_atm")
public class Branch_Atm {

	@Id
	@Column(name="branch_atmId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int branch_atmId;
	
	@Column(name = "branchId")
	private int branchId;
	
	@Column(name = "atmCardId")
	private int atmCardId;

	public int getBranch_atmId() {
		return branch_atmId;
	}

	public void setBranch_atmId(int branch_atmId) {
		this.branch_atmId = branch_atmId;
	}

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public int getAtmCardId() {
		return atmCardId;
	}

	public void setAtmCardId(int atmCardId) {
		this.atmCardId = atmCardId;
	}
	
	
	
}

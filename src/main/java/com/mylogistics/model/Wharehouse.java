package com.mylogistics.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "wharehouse")
public class Wharehouse {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="whSrNo")
	private int whSrNo;
	
	@Column(name="whCount")
	private int whCount;
	
	@Column(name="state")
	private String state;
	
	@Column(name="location")
	private String location;
	
	@Column(name="address1")
	private String address1;
	
	@Column(name="address2")
	private String address2;
	
	@Column(name="pinCode")
	private String pinCode;
	
	@Column(name="inchName")
	private String inchName;
	
	@Column(name="inchContactNo")
	private String inchContactNo;
	
	@Column(name="inchEmail")
	private String inchEmail;
	
	@Column(name="plant")
	private String plant;
	
	@Column(name="sloc")
	private String sloc;
	
	@Column(name="plant_sloc")
	private String plant_sloc;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getWhSrNo() {
		return whSrNo;
	}

	public void setWhSrNo(int whSrNo) {
		this.whSrNo = whSrNo;
	}

	public int getWhCount() {
		return whCount;
	}

	public void setWhCount(int whCount) {
		this.whCount = whCount;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getInchName() {
		return inchName;
	}

	public void setInchName(String inchName) {
		this.inchName = inchName;
	}

	public String getInchContactNo() {
		return inchContactNo;
	}

	public void setInchContactNo(String inchContactNo) {
		this.inchContactNo = inchContactNo;
	}

	public String getInchEmail() {
		return inchEmail;
	}

	public void setInchEmail(String inchEmail) {
		this.inchEmail = inchEmail;
	}

	public String getPlant() {
		return plant;
	}

	public void setPlant(String plant) {
		this.plant = plant;
	}

	public String getSloc() {
		return sloc;
	}

	public void setSloc(String sloc) {
		this.sloc = sloc;
	}

	public String getPlant_sloc() {
		return plant_sloc;
	}

	public void setPlant_sloc(String plant_sloc) {
		this.plant_sloc = plant_sloc;
	}
	
	
	
}

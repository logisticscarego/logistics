package com.mylogistics.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name = "state")
public class State {

	
	@Id
	@Column(name="stateId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int stateId;
	
	@Column(name="stateCode")
	private String stateCode;
	
	@Column(name="stateName" , unique=true)
	private String stateName;
	
	@Column(name="stateLryPrefix")
	private String stateLryPrefix;
	
	@Column(name="stateSTD")
	private String stateSTD;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
    private Calendar creationTS;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="stateGST")
	private String stateGST;
	
	@Column(name="stateRelPrefix")
	private String stateRelPrefix;//for reliance bill state prifix code

	
	//One To Many
	@JsonIgnore
	@JsonManagedReference(value = "stStn")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="state_station",
	joinColumns=@JoinColumn(name="stateId"),
	inverseJoinColumns=@JoinColumn(name="stnId"))
	private List<Station> stnList = new ArrayList<Station>();

    public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}


    public Calendar getCreationTS() {
        return creationTS;
    }

    public void setCreationTS(Calendar creationTS) {
        this.creationTS = creationTS;
    }
	
	public String getStateSTD() {
		return stateSTD;
	}

	public void setStateSTD(String stateSTD) {
		this.stateSTD = stateSTD;
	}

	public int getStateId() {
		return stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStateLryPrefix() {
		return stateLryPrefix;
	}

	public void setStateLryPrefix(String stateLryPrefix) {
		this.stateLryPrefix = stateLryPrefix;
	}

	public List<Station> getStnList() {
		return stnList;
	}

	public void setStnList(List<Station> stnList) {
		this.stnList = stnList;
	}

	public String getStateGST() {
		return stateGST;
	}

	public void setStateGST(String stateGST) {
		this.stateGST = stateGST;
	}

	public String getStateRelPrefix() {
		return stateRelPrefix;
	}

	public void setStateRelPrefix(String stateRelPrefix) {
		this.stateRelPrefix = stateRelPrefix;
	}
	
	
	
}

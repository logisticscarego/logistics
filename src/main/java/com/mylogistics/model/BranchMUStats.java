package com.mylogistics.model;

import java.util.Calendar;
//import java.util.Date;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "branchmustats")
public class BranchMUStats {

	@Id
	@Column(name="bmusId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int bmusId;
	

	@Column(name="bmusBranchCode")
	private String bmusBranchCode;
	

	@Column(name="bmusAvgMonthUsage")
	private double bmusAvgMonthUsage;
	
	@Column(name="bmusDt")
	private Date bmusDt;
	
	@Column(name="bmusDaysLast")
	private double bmusDaysLast;
	
	@Column(name="bmusStType")
	private String bmusStType;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
    private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="userCode")
	private String userCode;

	public String getBmusStType() {
		return bmusStType;
	}

	public void setBmusStType(String stationaryType) {
		this.bmusStType = stationaryType;
	}

	public int getBmusId() {
		return bmusId;
	}

	public void setBmusId(int branchMonthlyUsageStatsId) {
		this.bmusId = branchMonthlyUsageStatsId;
	}

	public String getBmusBranchCode() {
		return bmusBranchCode;
	}

	public void setBmusBranchCode(String branchCode) {
		this.bmusBranchCode = branchCode;
	}


	public double getBmusAvgMonthUsage() {
		return bmusAvgMonthUsage;
	}

	public void setBmusAvgMonthUsage(double avgMonthUsage) {
		this.bmusAvgMonthUsage = avgMonthUsage;
	}

	public Date getBmusDt() {
		return bmusDt;
	}

	public void setBmusDt(Date date) {
		this.bmusDt = date;
	}

	

	public double getBmusDaysLast() {
		return bmusDaysLast;
	}

	public void setBmusDaysLast(double bmusDaysLast) {
		this.bmusDaysLast = bmusDaysLast;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
}

package com.mylogistics.model.webservice;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "challandtapp")
public class ChallanDTApp {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "chdId")
	private Integer chdId;	
	@Column(name = "chdLorryNo")
	private String chdLorryNo;
	@Column(name = "chdBrkCode")
	private String chdBrkCode;
	@Column(name = "chdBrkName")
	private String chdBrkName;
	@Column(name = "chdOwnCode")
	private String chdOwnCode;
	@Column(name = "chdOwnName")
	private String chdOwnName;
	@Column(name = "chdBrkMob")
	private String chdBrkMob;
	@Column(name = "chdOwnMob")
	private String chdOwnMob;
	@Column(name = "chdDvrName")
	private String chdDvrName;
	@Column(name = "chdDvrMob")
	private String chdDvrMob;
	@Column(name = "chdRcNo")
	private String chdRcNo;
	@Column(name = "chdRcIssueDt")
	private String chdRcIssueDt;
	@Column(name = "chdRcValidDt")
	private String chdRcValidDt;
	@Column(name = "chdDlNo")
	private String chdDlNo;
	@Column(name = "chdDlIssueDt")
	private String chdDlIssueDt;
	@Column(name = "chdDlValidDt")
	private String chdDlValidDt;
	@Column(name = "chdPerNo")
	private String chdPerNo;
	@Column(name = "chdPerIssueDt")
	private String chdPerIssueDt;
	@Column(name = "chdPerValidDt")
	private String chdPerValidDt;
	@Column(name = "chdPerState")
	private String chdPerState;
	@Column(name = "chdFitDocNo")
	private String chdFitDocNo;
	@Column(name = "chdFitDocIssueDt")
	private String chdFitDocIssueDt;
	@Column(name = "chdFitDocValidDt")
	private String chdFitDocValidDt;
	@Column(name = "chdFitDocPlace")
	private String chdFitDocPlace;
	@Column(name = "chdBankFinance")
	private String chdBankFinance;
	@Column(name = "chdPolicyNo")
	private String chdPolicyNo;
	@Column(name = "chdPolicyCom")
	private String chdPolicyCom;
	@Column(name = "chdTransitPassNo")
	private String chdTransitPassNo;
	@Column(name = "chdPassHDRType")
	private String chdPassHDRType;
	@Column(name = "chdPanIssueSt")
	private String chdPanIssueSt;	
	@Column(name = "chlnCode")
	private String chlnCode;	
	@Column(name = "branchCode")
	private String branchCode;	
	@Column(name = "userCode")
	private String userCode;	
	@Column(name = "creationTS", insertable = false, updatable = true)
	private Calendar creationTS;
	
	public Integer getChdId() {
		return chdId;
	}
	public void setChdId(Integer chdId) {
		this.chdId = chdId;
	}
	public String getChdLorryNo() {
		return chdLorryNo;
	}
	public void setChdLorryNo(String chdLorryNo) {
		this.chdLorryNo = chdLorryNo;
	}
	public String getChdBrkCode() {
		return chdBrkCode;
	}
	public void setChdBrkCode(String chdBrkCode) {
		this.chdBrkCode = chdBrkCode;
	}
	public String getChdBrkName() {
		return chdBrkName;
	}
	public void setChdBrkName(String chdBrkName) {
		this.chdBrkName = chdBrkName;
	}
	public String getChdOwnCode() {
		return chdOwnCode;
	}
	public void setChdOwnCode(String chdOwnCode) {
		this.chdOwnCode = chdOwnCode;
	}
	public String getChdOwnName() {
		return chdOwnName;
	}
	public void setChdOwnName(String chdOwnName) {
		this.chdOwnName = chdOwnName;
	}
	public String getChdBrkMob() {
		return chdBrkMob;
	}
	public void setChdBrkMob(String chdBrkMob) {
		this.chdBrkMob = chdBrkMob;
	}
	public String getChdOwnMob() {
		return chdOwnMob;
	}
	public void setChdOwnMob(String chdOwnMob) {
		this.chdOwnMob = chdOwnMob;
	}
	public String getChdDvrName() {
		return chdDvrName;
	}
	public void setChdDvrName(String chdDvrName) {
		this.chdDvrName = chdDvrName;
	}
	public String getChdDvrMob() {
		return chdDvrMob;
	}
	public void setChdDvrMob(String chdDvrMob) {
		this.chdDvrMob = chdDvrMob;
	}
	public String getChdRcNo() {
		return chdRcNo;
	}
	public void setChdRcNo(String chdRcNo) {
		this.chdRcNo = chdRcNo;
	}
	public String getChdRcIssueDt() {
		return chdRcIssueDt;
	}
	public void setChdRcIssueDt(String chdRcIssueDt) {
		this.chdRcIssueDt = chdRcIssueDt;
	}
	public String getChdRcValidDt() {
		return chdRcValidDt;
	}
	public void setChdRcValidDt(String chdRcValidDt) {
		this.chdRcValidDt = chdRcValidDt;
	}
	public String getChdDlNo() {
		return chdDlNo;
	}
	public void setChdDlNo(String chdDlNo) {
		this.chdDlNo = chdDlNo;
	}
	public String getChdDlIssueDt() {
		return chdDlIssueDt;
	}
	public void setChdDlIssueDt(String chdDlIssueDt) {
		this.chdDlIssueDt = chdDlIssueDt;
	}
	public String getChdDlValidDt() {
		return chdDlValidDt;
	}
	public void setChdDlValidDt(String chdDlValidDt) {
		this.chdDlValidDt = chdDlValidDt;
	}
	public String getChdPerNo() {
		return chdPerNo;
	}
	public void setChdPerNo(String chdPerNo) {
		this.chdPerNo = chdPerNo;
	}
	public String getChdPerIssueDt() {
		return chdPerIssueDt;
	}
	public void setChdPerIssueDt(String chdPerIssueDt) {
		this.chdPerIssueDt = chdPerIssueDt;
	}
	public String getChdPerValidDt() {
		return chdPerValidDt;
	}
	public void setChdPerValidDt(String chdPerValidDt) {
		this.chdPerValidDt = chdPerValidDt;
	}
	public String getChdPerState() {
		return chdPerState;
	}
	public void setChdPerState(String chdPerState) {
		this.chdPerState = chdPerState;
	}
	public String getChdFitDocNo() {
		return chdFitDocNo;
	}
	public void setChdFitDocNo(String chdFitDocNo) {
		this.chdFitDocNo = chdFitDocNo;
	}
	public String getChdFitDocIssueDt() {
		return chdFitDocIssueDt;
	}
	public void setChdFitDocIssueDt(String chdFitDocIssueDt) {
		this.chdFitDocIssueDt = chdFitDocIssueDt;
	}
	public String getChdFitDocValidDt() {
		return chdFitDocValidDt;
	}
	public void setChdFitDocValidDt(String chdFitDocValidDt) {
		this.chdFitDocValidDt = chdFitDocValidDt;
	}
	public String getChdFitDocPlace() {
		return chdFitDocPlace;
	}
	public void setChdFitDocPlace(String chdFitDocPlace) {
		this.chdFitDocPlace = chdFitDocPlace;
	}
	public String getChdBankFinance() {
		return chdBankFinance;
	}
	public void setChdBankFinance(String chdBankFinance) {
		this.chdBankFinance = chdBankFinance;
	}
	public String getChdPolicyNo() {
		return chdPolicyNo;
	}
	public void setChdPolicyNo(String chdPolicyNo) {
		this.chdPolicyNo = chdPolicyNo;
	}
	public String getChdPolicyCom() {
		return chdPolicyCom;
	}
	public void setChdPolicyCom(String chdPolicyCom) {
		this.chdPolicyCom = chdPolicyCom;
	}
	public String getChdTransitPassNo() {
		return chdTransitPassNo;
	}
	public void setChdTransitPassNo(String chdTransitPassNo) {
		this.chdTransitPassNo = chdTransitPassNo;
	}
	public String getChdPassHDRType() {
		return chdPassHDRType;
	}
	public void setChdPassHDRType(String chdPassHDRType) {
		this.chdPassHDRType = chdPassHDRType;
	}
	public String getChdPanIssueSt() {
		return chdPanIssueSt;
	}
	public void setChdPanIssueSt(String chdPanIssueSt) {
		this.chdPanIssueSt = chdPanIssueSt;
	}
	public Calendar getCreationTS() {
		return creationTS;
	}
	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
	public String getChlnCode() {
		return chlnCode;
	}
	public void setChlnCode(String chlnCode) {
		this.chlnCode = chlnCode;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}	
			
}
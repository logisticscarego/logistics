package com.mylogistics.model;


import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.validator.constraints.NotBlank;

import com.sun.istack.NotNull;

@Entity
@Table(name = "fyear", uniqueConstraints = {
			@UniqueConstraint(columnNames = {"fyId", "fyFrmDt", "fyToDt"})
		}
)	
public class FYear {
	
	@Id
	@Column(name = "fyId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer fyId;
	
	@Column(name = "fyFrmDt", nullable = false)
	private Date fyFrmDt;
	
	@Column(name = "fyToDt", nullable = false)
	private Date fyToDt;
	
	@Column(name = "isActive", nullable = false)
	private Character isActive;

	public Integer getFyId() {
		return fyId;
	}

	public void setFyId(Integer fyId) {
		this.fyId = fyId;
	}

	public Date getFyFrmDt() {
		return fyFrmDt;
	}

	public void setFyFrmDt(Date fyFrmDt) {
		this.fyFrmDt = fyFrmDt;
	}

	public Date getFyToDt() {
		return fyToDt;
	}

	public void setFyToDt(Date fyToDt) {
		this.fyToDt = fyToDt;
	}

	public Character getIsActive() {
		return isActive;
	}

	public void setIsActive(Character isActive) {
		this.isActive = isActive;
	}
	
}

package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "assestsnliab")
public class AssestsNLiab {

	@Id
	@Column(name="alId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int alId;
	
	@Column(name="alName")
	private String alName;
	
	@Column(name="alFaCode")
	private String alFaCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getAlId() {
		return alId;
	}

	public void setAlId(int alId) {
		this.alId = alId;
	}

	public String getAlName() {
		return alName;
	}

	public void setAlName(String alName) {
		this.alName = alName;
	}

	public String getAlFaCode() {
		return alFaCode;
	}

	public void setAlFaCode(String alFaCode) {
		this.alFaCode = alFaCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
}

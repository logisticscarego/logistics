package com.mylogistics.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="petrocard")
public class PetroCard {
	
	@Id
	@GeneratedValue
	@Column(name="cardId")
	private int cardId;
	
	@Column(name="branch")
	private String branch;
	
	@Column(name="cardType")
	private String cardType;
	
	@Column(name="cardNo")
	private String cardNo;
	
	@Column(name="cardFaCode")
	private String cardFaCode;

	public int getCardId() {
		return cardId;
	}

	public void setCardId(int cardId) {
		this.cardId = cardId;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCardFaCode() {
		return cardFaCode;
	}

	public void setCardFaCode(String cardFaCode) {
		this.cardFaCode = cardFaCode;
	}
	

}

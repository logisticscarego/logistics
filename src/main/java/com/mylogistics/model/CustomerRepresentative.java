package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "custrepresentative")


public class CustomerRepresentative {
	
	@Id
	@Column(name="crId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int crId;
	 
	@Column(name="crCode")
	private String crCode;
	
	@Column(name="crName")
	private String crName;
	
	@Column(name="crDesignation")
	private String crDesignation;
	
	@Column(name="crMobileNo")
	private String crMobileNo;
	
	@Column(name="crEmailId")
	private String crEmailId;
	
	@Column(name="custId")
	private int custId;
	
	@Column(name="custRefNo")
	private String custRefNo;
	 
	@Column(name="custCode",unique=false,nullable=true)
	private String custCode;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="crCodeTemp")
	private String crCodeTemp;
	
	@Column(name="crFaCode")
	private String crFaCode;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToOne(cascade = CascadeType.ALL)
    private FAParticular fAParticular;
	
	public FAParticular getfAParticular() {
		return fAParticular;
	}

	public void setfAParticular(FAParticular fAParticular) {
		this.fAParticular = fAParticular;
	}
	
	public String getCrFaCode() {
		return crFaCode;
	}

	public void setCrFaCode(String crFaCode) {
		this.crFaCode = crFaCode;
	}

	public String getCrCodeTemp() {
		return crCodeTemp;
	}

	public void setCrCodeTemp(String crCodeTemp) {
		this.crCodeTemp = crCodeTemp;
	}

	@Column(name="isView")
	private boolean isView;
	
	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}
	
	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
	
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public int getCrId() {
		return crId;
	}

	public void setCrId(int crId) {
		this.crId = crId;
	}

	public String getCrCode() {
		return crCode;
	}

	public void setCrCode(String crCode) {
		this.crCode = crCode;
	}

	public String getCrName() {
		return crName;
	}

	public void setCrName(String crName) {
		this.crName = crName;
	}

	public String getCrDesignation() {
		return crDesignation;
	}

	public void setCrDesignation(String crDesignation) {
		this.crDesignation = crDesignation;
	}

	public String getCrMobileNo() {
		return crMobileNo;
	}

	public void setCrMobileNo(String crMobileNo) {
		this.crMobileNo = crMobileNo;
	}

	public String getCrEmailId() {
		return crEmailId;
	}

	public void setCrEmailId(String crEmailId) {
		this.crEmailId = crEmailId;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getCustRefNo() {
		return custRefNo;
	}

	public void setCustRefNo(String custRefNo) {
		this.custRefNo = custRefNo;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
}

package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "subvehiclemstr")
public class SubVehicleMstr {


	@Id
	@Column(name="svmId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int svmId;
	
	//One to One
	//@OneToOne(cascade = CascadeType.ALL)
	@Column(name="svmCsId" , columnDefinition = "int default -1")
	private int svmCsId;
	
	//Many To One
	@ManyToOne
	@LazyCollection(LazyCollectionOption.FALSE)
	//@JsonBackReference(value = "vmSvm")
	@JoinColumn(name = "vehId") 
	private VehicleMstr vehicleMstr;
	
	@Column(name="svmTotAmt")
	private double svmTotAmt;
	
	@Column(name="svmFaCode")
	private String svmFaCode;
	
	@Column(name="svmTvNo")
	private String svmTvNo;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getSvmId() {
		return svmId;
	}

	public void setSvmId(int svmId) {
		this.svmId = svmId;
	}

	/*public CashStmt getCashStmt() {
		return cashStmt;
	}

	public void setCashStmt(CashStmt cashStmt) {
		this.cashStmt = cashStmt;
	}*/

	public double getSvmTotAmt() {
		return svmTotAmt;
	}

	public int getSvmCsId() {
		return svmCsId;
	}

	public void setSvmCsId(int svmCsId) {
		this.svmCsId = svmCsId;
	}

	public void setSvmTotAmt(double svmTotAmt) {
		this.svmTotAmt = svmTotAmt;
	}

	public String getSvmFaCode() {
		return svmFaCode;
	}

	public void setSvmFaCode(String svmFaCode) {
		this.svmFaCode = svmFaCode;
	}

	public String getSvmTvNo() {
		return svmTvNo;
	}

	public void setSvmTvNo(String svmTvNo) {
		this.svmTvNo = svmTvNo;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public VehicleMstr getVehicleMstr() {
		return vehicleMstr;
	}

	public void setVehicleMstr(VehicleMstr vehicleMstr) {
		this.vehicleMstr = vehicleMstr;
	}

}

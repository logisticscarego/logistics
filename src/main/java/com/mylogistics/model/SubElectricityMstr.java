package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "subelectricitymstr")
public class SubElectricityMstr {

	@Id
	@Column(name="semId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int semId;
	
	//One To One
	//@OneToOne(cascade = CascadeType.ALL)
	@Column(name="semCsId" , columnDefinition = "int default -1")
	private int semCsId;
	
	// Many To One
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	//@JsonBackReference(value = "semEM")
	@JoinColumn(name = "emId") 
	private ElectricityMstr electricityMstr;
	
	@Column(name="semPayAmt")
	private double semPayAmt;
	
	@Column(name="semBillFrDt")
	private Date semBillFrDt;
	
	@Column(name="semBillToDt")
	private Date semBillToDt;
	
	/*@Column(name="semStaffCode")
	private String semStaffCode;*/
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getSemId() {
		return semId;
	}

	public void setSemId(int semId) {
		this.semId = semId;
	}

	/*public CashStmt getCashStmt() {
		return cashStmt;
	}

	public void setCashStmt(CashStmt cashStmt) {
		this.cashStmt = cashStmt;
	}*/

	public ElectricityMstr getElectricityMstr() {
		return electricityMstr;
	}

	public void setElectricityMstr(ElectricityMstr electricityMstr) {
		this.electricityMstr = electricityMstr;
	}

	public double getSemPayAmt() {
		return semPayAmt;
	}

	public void setSemPayAmt(double semPayAmt) {
		this.semPayAmt = semPayAmt;
	}

	public Date getSemBillFrDt() {
		return semBillFrDt;
	}

	public void setSemBillFrDt(Date semBillFrDt) {
		this.semBillFrDt = semBillFrDt;
	}

	public Date getSemBillToDt() {
		return semBillToDt;
	}

	public void setSemBillToDt(Date semBillToDt) {
		this.semBillToDt = semBillToDt;
	}

	/*public String getSemStaffCode() {
		return semStaffCode;
	}

	public void setSemStaffCode(String semStaffCode) {
		this.semStaffCode = semStaffCode;
	}*/

	public String getUserCode() {
		return userCode;
	}

	public int getSemCsId() {
		return semCsId;
	}

	public void setSemCsId(int semCsId) {
		this.semCsId = semCsId;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
}

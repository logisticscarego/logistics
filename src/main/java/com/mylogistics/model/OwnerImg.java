package com.mylogistics.model;

import java.sql.Blob;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ownerimg")
public class OwnerImg {

	@Id
	@Column(name="ownImgId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ownImgId;
	
	@Column(name="ownId" , columnDefinition = "int default -1")
	private int ownId;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="ownPanImgPath")
	private String ownPanImgPath;
	
	@Column(name="ownDecImgPath")
	private String ownDecImgPath;
	
	@Column(name="ownBnkDetImgPath")
	private String ownBnkDetImgPath;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getOwnImgId() {
		return ownImgId;
	}

	public void setOwnImgId(int ownImgId) {
		this.ownImgId = ownImgId;
	}

	public int getOwnId() {
		return ownId;
	}

	public void setOwnId(int ownId) {
		this.ownId = ownId;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getOwnPanImgPath() {
		return ownPanImgPath;
	}

	public void setOwnPanImgPath(String ownPanImgPath) {
		this.ownPanImgPath = ownPanImgPath;
	}

	public String getOwnDecImgPath() {
		return ownDecImgPath;
	}

	public void setOwnDecImgPath(String ownDecImgPath) {
		this.ownDecImgPath = ownDecImgPath;
	}

	public String getOwnBnkDetImgPath() {
		return ownBnkDetImgPath;
	}

	public void setOwnBnkDetImgPath(String ownBnkDetImgPath) {
		this.ownBnkDetImgPath = ownBnkDetImgPath;
	}
	
	
}

package com.mylogistics.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "telephonemstr")
public class TelephoneMstr {

	@Id
	@Column(name="tmId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int tmId;
	
	// Many To One
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	//@JsonBackReference(value = "telBrh")
	@JoinColumn(name = "branchId") 
	private Branch branch;
	
	// Many To One
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	//@JsonBackReference(value = "telEmp")
	@JoinColumn(name = "empId") 
	private Employee employee;
	
	
	//One To Many
	@JsonIgnore
	@JsonManagedReference(value = "stmTelM")
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="telephonemstr_subtelephonemstr",
	joinColumns=@JoinColumn(name="tmId"),
	inverseJoinColumns=@JoinColumn(name="stmId"))
	private List<SubTelephoneMstr> subTelephoneMstrList = new ArrayList<SubTelephoneMstr>();
		
	
	
	//One To One
	@OneToOne(cascade = CascadeType.ALL)
	private Address address;
	
	@Column(name="tmPhNo")
	private String tmPhNo;
	
	@Column(name="tmType")
	private String tmType;
	
	@Column(name="tmLocType")
	private String tmLocType;
	
	@Column(name="tmSPName")
	private String tmSPName;
	
	@Column(name="tmInstDt")
	private Date tmInstDt;
	
	@Column(name="tmDiscDt")
	private Date tmDiscDt;
	
	@Column(name="tmAdvDepDt")
	private Date tmAdvDepDt;

	@Column(name="tmRefDt")
	private Date tmRefDt;
	
	@Column(name="tmAdvDepAmt")
	private double tmAdvDepAmt;
	
	@Column(name="tmRefAmt")
	private double tmRefAmt;
	
	@Column(name="tmPlan")
	private String tmPlan;
	
	@Column(name="tmPayMod")
	private String tmPayMod;
	
	@Column(name="tmLimitAmt")
	private double tmLimitAmt;
	
	@Column(name="tmAccNo")
	private String tmAccNo;
	
	@Column(name="tmBillDay")
	private int tmBillDay;
	
	@Column(name="tmDueDay")
	private int tmDueDay;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public List<SubTelephoneMstr> getSubTelephoneMstrList() {
		return subTelephoneMstrList;
	}

	public void setSubTelephoneMstrList(List<SubTelephoneMstr> subTelephoneMstrList) {
		this.subTelephoneMstrList = subTelephoneMstrList;
	}

	public int getTmId() {
		return tmId;
	}

	public void setTmId(int tmId) {
		this.tmId = tmId;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getTmPhNo() {
		return tmPhNo;
	}

	public void setTmPhNo(String tmPhNo) {
		this.tmPhNo = tmPhNo;
	}

	public String getTmType() {
		return tmType;
	}

	public void setTmType(String tmType) {
		this.tmType = tmType;
	}

	public String getTmLocType() {
		return tmLocType;
	}

	public void setTmLocType(String tmLocType) {
		this.tmLocType = tmLocType;
	}

	public String getTmSPName() {
		return tmSPName;
	}

	public void setTmSPName(String tmSPName) {
		this.tmSPName = tmSPName;
	}

	public Date getTmInstDt() {
		return tmInstDt;
	}

	public void setTmInstDt(Date tmInstDt) {
		this.tmInstDt = tmInstDt;
	}

	public Date getTmDiscDt() {
		return tmDiscDt;
	}

	public void setTmDiscDt(Date tmDiscDt) {
		this.tmDiscDt = tmDiscDt;
	}

	public Date getTmAdvDepDt() {
		return tmAdvDepDt;
	}

	public void setTmAdvDepDt(Date tmAdvDepDt) {
		this.tmAdvDepDt = tmAdvDepDt;
	}

	public Date getTmRefDt() {
		return tmRefDt;
	}

	public void setTmRefDt(Date tmRefDt) {
		this.tmRefDt = tmRefDt;
	}

	public double getTmAdvDepAmt() {
		return tmAdvDepAmt;
	}

	public void setTmAdvDepAmt(double tmAdvDepAmt) {
		this.tmAdvDepAmt = tmAdvDepAmt;
	}

	public double getTmRefAmt() {
		return tmRefAmt;
	}

	public void setTmRefAmt(double tmRefAmt) {
		this.tmRefAmt = tmRefAmt;
	}

	public String getTmPlan() {
		return tmPlan;
	}

	public void setTmPlan(String tmPlan) {
		this.tmPlan = tmPlan;
	}

	public String getTmPayMod() {
		return tmPayMod;
	}

	public void setTmPayMod(String tmPayMod) {
		this.tmPayMod = tmPayMod;
	}

	public double getTmLimitAmt() {
		return tmLimitAmt;
	}

	public void setTmLimitAmt(double tmLimitAmt) {
		this.tmLimitAmt = tmLimitAmt;
	}

	public String getTmAccNo() {
		return tmAccNo;
	}

	public void setTmAccNo(String tmAccNo) {
		this.tmAccNo = tmAccNo;
	}

	public int getTmBillDay() {
		return tmBillDay;
	}

	public void setTmBillDay(int tmBillDay) {
		this.tmBillDay = tmBillDay;
	}

	public int getTmDueDay() {
		return tmDueDay;
	}

	public void setTmDueDay(int tmDueDay) {
		this.tmDueDay = tmDueDay;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
	
	
}

package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "btbfundtrans")
public class BTBFundTrans {

	@Id
	@Column(name="btbId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int btbId;
	
	@Column(name="btbFrBrCode")
	private String btbFrBrCode;
	
	@Column(name="btbFrBnkCode")
	private String btbFrBnkCode;
	
	@Column(name="btbChqType")
	private char btbChqType;
	
	@Column(name="btbChqNo")
	private String btbChqNo;
	
	@Column(name="btbToBrCode")
	private String btbToBrCode;
	
	@Column(name="btbToBnkCode")
	private String btbToBnkCode;
	
	@Column(name="btbVouchNo")
	private int btbVouchNo;
	
	@Column(name="btbCssDt")
	private Date btbCssDt;
	
	@Column(name="btbAmt" , columnDefinition = "double default 0")
	private double btbAmt;
	
	@Column(name="btbIsView" , columnDefinition = "boolean default false")
	private boolean btbIsView;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getBtbId() {
		return btbId;
	}

	public void setBtbId(int btbId) {
		this.btbId = btbId;
	}

	public String getBtbFrBrCode() {
		return btbFrBrCode;
	}

	public void setBtbFrBrCode(String btbFrBrCode) {
		this.btbFrBrCode = btbFrBrCode;
	}

	public String getBtbFrBnkCode() {
		return btbFrBnkCode;
	}

	public void setBtbFrBnkCode(String btbFrBnkCode) {
		this.btbFrBnkCode = btbFrBnkCode;
	}

	public String getBtbToBrCode() {
		return btbToBrCode;
	}

	public void setBtbToBrCode(String btbToBrCode) {
		this.btbToBrCode = btbToBrCode;
	}

	public String getBtbToBnkCode() {
		return btbToBnkCode;
	}

	public void setBtbToBnkCode(String btbToBnkCode) {
		this.btbToBnkCode = btbToBnkCode;
	}

	public double getBtbAmt() {
		return btbAmt;
	}

	public void setBtbAmt(double btbAmt) {
		this.btbAmt = btbAmt;
	}

	public boolean isBtbIsView() {
		return btbIsView;
	}

	public void setBtbIsView(boolean btbIsView) {
		this.btbIsView = btbIsView;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public int getBtbVouchNo() {
		return btbVouchNo;
	}

	public void setBtbVouchNo(int btbVouchNo) {
		this.btbVouchNo = btbVouchNo;
	}

	public Date getBtbCssDt() {
		return btbCssDt;
	}

	public void setBtbCssDt(Date btbCssDt) {
		this.btbCssDt = btbCssDt;
	}

	public char getBtbChqType() {
		return btbChqType;
	}

	public void setBtbChqType(char btbChqType) {
		this.btbChqType = btbChqType;
	}

	public String getBtbChqNo() {
		return btbChqNo;
	}

	public void setBtbChqNo(String btbChqNo) {
		this.btbChqNo = btbChqNo;
	}

	
}

package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "othercharge")
public class OtherCharge {

	@Id
	@Column(name="otChgId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int otChgId;
	
	@Column(name="otChgBillNo")
	private String otChgBillNo;
	
	@Column(name="otChgContCode")
	private String otChgContCode;
	
	@Column(name="otChgCnmtCode")
	private String otChgCnmtCode;
	
	@Column(name="otChgType")
	private String otChgType;
	
	@Column(name="otChgValue")
	private double otChgValue;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getOtChgId() {
		return otChgId;
	}

	public void setOtChgId(int otChgId) {
		this.otChgId = otChgId;
	}

	public String getOtChgBillNo() {
		return otChgBillNo;
	}

	public void setOtChgBillNo(String otChgBillNo) {
		this.otChgBillNo = otChgBillNo;
	}

	public String getOtChgContCode() {
		return otChgContCode;
	}

	public void setOtChgContCode(String otChgContCode) {
		this.otChgContCode = otChgContCode;
	}

	public String getOtChgCnmtCode() {
		return otChgCnmtCode;
	}

	public void setOtChgCnmtCode(String otChgCnmtCode) {
		this.otChgCnmtCode = otChgCnmtCode;
	}

	public String getOtChgType() {
		return otChgType;
	}

	public void setOtChgType(String otChgType) {
		this.otChgType = otChgType;
	}

	public double getOtChgValue() {
		return otChgValue;
	}

	public void setOtChgValue(double otChgValue) {
		this.otChgValue = otChgValue;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
	
	
}

package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stationarystock")
public class StationaryStock {

	@Id
	@Column(name="stStkId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int stStkId;
	
	@Column(name="stStkCode")
	private String stStkCode;

	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public int getStStkId() {
		return stStkId;
	}

	public void setStStkId(int stStkId) {
		this.stStkId = stStkId;
	}

	public String getStStkCode() {
		return stStkCode;
	}

	public void setStStkCode(String stStkCode) {
		this.stStkCode = stStkCode;
	}
}

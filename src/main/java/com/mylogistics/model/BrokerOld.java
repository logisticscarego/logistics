package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "brokerold")
public class BrokerOld {
	
	@Id
	@Column(name="brkOldId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int brkOldId;
	

	@Column(name="brkId")
	private int brkId;
	
	@Column(name="brkCode")
	private String brkCode;
	
	@Column(name="branchCode")
	private String branchCode;
	
	@Column(name="brkName")
	private String brkName;
	
	@Column(name="brkComStbDt")
	private Date brkComStbDt;
	
	@Column(name="brkPanDOB")
	private Date brkPanDOB;
	
	@Column(name="brkPanDt")
	private Date brkPanDt;
	
	@Column(name="brkPanName")
	private String brkPanName;
	
	@Column(name="brkPanNo")
	private String brkPanNo;
	
	@Column(name="brkVehicleType")
	private String brkVehicleType;
	
	@Column(name="brkPPNo")
	private String brkPPNo;
	
	@Column(name="brkVoterId")
	private String brkVoterId;
	
	@Column(name="brkSrvTaxNo")
	private String brkSrvTaxNo;
	
	@Column(name="brkFirmRegNo")
	private String brkFirmRegNo;
	
	@Column(name="brkRegPlace")
	private String brkRegPlace;
	
	@Column(name="brkFirmType")
	private String brkFirmType;
	
	@Column(name="brkCIN")
	private String brkCIN;
	
	@Column(name="brkBsnCard")
	private String brkBsnCard;
	
	@Column(name="brkUnion")
	private String brkUnion;
	
	@Column(name="brkEmailId")
	private String brkEmailId;
	
	@Column(name="brkActiveDt")
	private Date brkActiveDt;
	
	@Column(name="brkDeactiveDt")
	private Date brkDeactiveDt;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;

	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="isView")
	private boolean isView;
	
	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}

	public Date getBrkComStbDt() {
		return brkComStbDt;
	}

	public void setBrkComStbDt(Date brkComStbDt) {
		this.brkComStbDt = brkComStbDt;
	}

	public Date getBrkPanDOB() {
		return brkPanDOB;
	}

	public void setBrkPanDOB(Date brkPanDOB) {
		this.brkPanDOB = brkPanDOB;
	}

	public Date getBrkPanDt() {
		return brkPanDt;
	}

	public void setBrkPanDt(Date brkPanDt) {
		this.brkPanDt = brkPanDt;
	}

	public Date getBrkActiveDt() {
		return brkActiveDt;
	}

	public void setBrkActiveDt(Date brkActiveDt) {
		this.brkActiveDt = brkActiveDt;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}


	public int getBrkId() {
		return brkId;
	}

	public void setBrkId(int brkId) {
		this.brkId = brkId;
	}

	public String getBrkCode() {
		return brkCode;
	}

	public void setBrkCode(String brkCode) {
		this.brkCode = brkCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBrkName() {
		return brkName;
	}

	public void setBrkName(String brkName) {
		this.brkName = brkName;
	}

	public String getBrkPanName() {
		return brkPanName;
	}

	public void setBrkPanName(String brkPanName) {
		this.brkPanName = brkPanName;
	}

	public String getBrkVehicleType() {
		return brkVehicleType;
	}

	public void setBrkVehicleType(String brkVehicleType) {
		this.brkVehicleType = brkVehicleType;
	}

	public String getBrkPPNo() {
		return brkPPNo;
	}

	public void setBrkPPNo(String brkPPNo) {
		this.brkPPNo = brkPPNo;
	}

	public String getBrkVoterId() {
		return brkVoterId;
	}

	public void setBrkVoterId(String brkVoterId) {
		this.brkVoterId = brkVoterId;
	}

	public String getBrkSrvTaxNo() {
		return brkSrvTaxNo;
	}

	public void setBrkSrvTaxNo(String brkSrvTaxNo) {
		this.brkSrvTaxNo = brkSrvTaxNo;
	}

	public String getBrkFirmRegNo() {
		return brkFirmRegNo;
	}

	public void setBrkFirmRegNo(String brkFirmRegNo) {
		this.brkFirmRegNo = brkFirmRegNo;
	}

	public String getBrkRegPlace() {
		return brkRegPlace;
	}

	public void setBrkRegPlace(String brkRegPlace) {
		this.brkRegPlace = brkRegPlace;
	}

	public String getBrkFirmType() {
		return brkFirmType;
	}

	public void setBrkFirmType(String brkFirmType) {
		this.brkFirmType = brkFirmType;
	}

	public String getBrkCIN() {
		return brkCIN;
	}

	public void setBrkCIN(String brkCIN) {
		this.brkCIN = brkCIN;
	}

	public String getBrkBsnCard() {
		return brkBsnCard;
	}

	public void setBrkBsnCard(String brkBsnCard) {
		this.brkBsnCard = brkBsnCard;
	}

	public String getBrkUnion() {
		return brkUnion;
	}

	public void setBrkUnion(String brkUnion) {
		this.brkUnion = brkUnion;
	}

	public String getBrkEmailId() {
		return brkEmailId;
	}

	public void setBrkEmailId(String brkEmailId) {
		this.brkEmailId = brkEmailId;
	}

	public Date getBrkDeactiveDt() {
		return brkDeactiveDt;
	}

	public void setBrkDeactiveDt(Date brkDeactiveDt) {
		this.brkDeactiveDt = brkDeactiveDt;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public int getBrkOldId() {
		return brkOldId;
	}

	public void setBrkOldId(int brkOldId) {
		this.brkOldId = brkOldId;
	}

	public String getBrkPanNo() {
		return brkPanNo;
	}

	public void setBrkPanNo(String brkPanNo) {
		this.brkPanNo = brkPanNo;
	}

}

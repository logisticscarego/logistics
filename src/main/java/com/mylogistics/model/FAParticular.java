package com.mylogistics.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "faparticular")
public class FAParticular {
	
	@Id
	@Column(name="faPerId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int faPerId;
	
	@Column(name="faPerType")
	private String faPerType;

	public int getFaPerId() {
		return faPerId;
	}

	public void setFaPerId(int faPerId) {
		this.faPerId = faPerId;
	}

	public String getFaPerType() {
		return faPerType;
	}

	public void setFaPerType(String faPerType) {
		this.faPerType = faPerType;
	}

}

package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="vehiclevendormstr")
public class VehicleVendorMstr {
	
	@Id
	@Column(name="vvId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int vvId;
	
	@Column(name="vvRcNo")
	private String vvRcNo;
	
	@Column(name="vvRcIssueDt")
	private Date vvRcIssueDt;
	
	@Column(name="vvRcValidDt")
	private Date vvRcValidDt;
	
	@Column(name="vvDriverName")
	private String vvDriverName;
	
	@Column(name="vvDriverMobNo")
	private String vvDriverMobNo;
	
	@Column(name="vvDriverDLNo")
	private String vvDriverDLNo;
	
	@Column(name="vvDriverDLIssueDt")
	private Date vvDriverDLIssueDt;
	
	@Column(name="vvDriverDLValidDt")
	private Date vvDriverDLValidDt;
	
	@Column(name="vvPerNo")
	private String vvPerNo;
	
	@Column(name="vvPerIssueDt")
	private Date vvPerIssueDt;
	
	@Column(name="vvPerValidDt")
	private Date vvPerValidDt;
	
	@Column(name="vvPerState")
	private String vvPerState;//from state table
	
	@Column(name="vvFitNo")
	private String vvFitNo;
	
	@Column(name="vvFitIssueDt")
	private Date vvFitIssueDt;
	
	@Column(name="vvFitValidDt")
	private Date vvFitValidDt;
	
	@Column(name="vvFitState")
	private String vvFitState;//from state table
	
	@Column(name="vvFinanceBankName")
	private String vvFinanceBankName;
	
	@Column(name="vvPolicyNo")
	private String vvPolicyNo;
	
	@Column(name="vvPolicyValidDt")
	private Date vvPolicyValidDt;
	
	@Column(name="vvPolicyIssueDt")
	private Date vvPolicyIssueDt;
	
	@Column(name="vvPolicyComp")
	private String vvPolicyComp;
	
	@Column(name="vvTransitPassNo")
	private String vvTransitPassNo;
	
	@Column(name="vvType")
	private String vvType;//this from vehicle type
	
	@ManyToOne
	@JoinColumn(name = "ownId") 
	private Owner owner;
	
	@ManyToOne
	@JoinColumn(name = "brkId") 
	private Broker broker;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="vvEngineNo")
	private String vvEngineNo;
	
	@Column(name="vvTransferId")
	private String vvTransferId;
	
	@Column(name="vvTransferDt")
	private Date vvTransferDt;
	
	@Column(name="vvChassisNo")
	private String vvChassisNo;
	
	@Column(name="vvModel", columnDefinition="int(5) default '0000'")
	private int vvModel;
	
	@Column(name="vvModelNo")
	private String vvModelNo;
	
	@Column(name = "vvHypoTo", length=128)
	private String vvHypoTo;
	
	@Column(name="vvRcImgPath")
	private String vvRcImgPath;
	
	/*@Column(name="vvTcImgPath")
	private String vvTcImgPath;*/
	
	/*public String getVvTcImgPath() {
		return vvTcImgPath;
	}

	public void setVvTcImgPath(String vvTcImgPath) {
		this.vvTcImgPath = vvTcImgPath;
	}*/

	@Column(name="vvPerSlpImgPath")
	private String vvPerSlpImgPath;
	
	@Column(name="vvPolicyImgPath")
	private String vvPolicyImgPath;
	
	public int getVvId() {
		return vvId;
	}

	public void setVvId(int vvId) {
		this.vvId = vvId;
	}

	public String getVvRcNo() {
		return vvRcNo;
	}

	public void setVvRcNo(String vvRcNo) {
		this.vvRcNo = vvRcNo;
	}

	public Date getVvRcIssueDt() {
		return vvRcIssueDt;
	}

	public void setVvRcIssueDt(Date vvRcIssueDt) {
		this.vvRcIssueDt = vvRcIssueDt;
	}

	public Date getVvRcValidDt() {
		return vvRcValidDt;
	}

	public void setVvRcValidDt(Date vvRcValidDt) {
		this.vvRcValidDt = vvRcValidDt;
	}

	public String getVvDriverName() {
		return vvDriverName;
	}

	public void setVvDriverName(String vvDriverName) {
		this.vvDriverName = vvDriverName;
	}

	public String getVvDriverMobNo() {
		return vvDriverMobNo;
	}

	public void setVvDriverMobNo(String vvDriverMobNo) {
		this.vvDriverMobNo = vvDriverMobNo;
	}

	public String getVvDriverDLNo() {
		return vvDriverDLNo;
	}

	public void setVvDriverDLNo(String vvDriverDLNo) {
		this.vvDriverDLNo = vvDriverDLNo;
	}

	public Date getVvDriverDLIssueDt() {
		return vvDriverDLIssueDt;
	}

	public void setVvDriverDLIssueDt(Date vvDriverDLIssueDt) {
		this.vvDriverDLIssueDt = vvDriverDLIssueDt;
	}

	public Date getVvDriverDLValidDt() {
		return vvDriverDLValidDt;
	}

	public void setVvDriverDLValidDt(Date vvDriverDLValidDt) {
		this.vvDriverDLValidDt = vvDriverDLValidDt;
	}

	public String getVvPerNo() {
		return vvPerNo;
	}

	public void setVvPerNo(String vvPerNo) {
		this.vvPerNo = vvPerNo;
	}

	public Date getVvPerIssueDt() {
		return vvPerIssueDt;
	}

	public void setVvPerIssueDt(Date vvPerIssueDt) {
		this.vvPerIssueDt = vvPerIssueDt;
	}

	public Date getVvPerValidDt() {
		return vvPerValidDt;
	}

	public void setVvPerValidDt(Date vvPerValidDt) {
		this.vvPerValidDt = vvPerValidDt;
	}

	public String getVvPerState() {
		return vvPerState;
	}

	public void setVvPerState(String vvPerState) {
		this.vvPerState = vvPerState;
	}

	public String getVvFitNo() {
		return vvFitNo;
	}

	public void setVvFitNo(String vvFitNo) {
		this.vvFitNo = vvFitNo;
	}

	public Date getVvFitIssueDt() {
		return vvFitIssueDt;
	}

	public void setVvFitIssueDt(Date vvFitIssueDt) {
		this.vvFitIssueDt = vvFitIssueDt;
	}

	public Date getVvFitValidDt() {
		return vvFitValidDt;
	}

	public void setVvFitValidDt(Date vvFitValidDt) {
		this.vvFitValidDt = vvFitValidDt;
	}

	public String getVvFitState() {
		return vvFitState;
	}

	public void setVvFitState(String vvFitState) {
		this.vvFitState = vvFitState;
	}

	public String getVvFinanceBankName() {
		return vvFinanceBankName;
	}

	public void setVvFinanceBankName(String vvFinanceBankName) {
		this.vvFinanceBankName = vvFinanceBankName;
	}

	public String getVvPolicyNo() {
		return vvPolicyNo;
	}

	public void setVvPolicyNo(String vvPolicyNo) {
		this.vvPolicyNo = vvPolicyNo;
	}

	public String getVvPolicyComp() {
		return vvPolicyComp;
	}

	public void setVvPolicyComp(String vvPolicyComp) {
		this.vvPolicyComp = vvPolicyComp;
	}

	public String getVvTransitPassNo() {
		return vvTransitPassNo;
	}

	public void setVvTransitPassNo(String vvTransitPassNo) {
		this.vvTransitPassNo = vvTransitPassNo;
	}

	public String getVvType() {
		return vvType;
	}

	public void setVvType(String vvType) {
		this.vvType = vvType;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public Broker getBroker() {
		return broker;
	}

	public void setBroker(Broker broker) {
		this.broker = broker;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getVvEngineNo() {
		return vvEngineNo;
	}

	public void setVvEngineNo(String vvEngineNo) {
		this.vvEngineNo = vvEngineNo;
	}

	public String getVvChassisNo() {
		return vvChassisNo;
	}

	public void setVvChassisNo(String vvChassisNo) {
		this.vvChassisNo = vvChassisNo;
	}

	public int getVvModel() {
		return vvModel;
	}

	public void setVvModel(int vvModel) {
		this.vvModel = vvModel;
	}

	public Date getVvPolicyValidDt() {
		return vvPolicyValidDt;
	}

	public void setVvPolicyValidDt(Date vvPolicyValidDt) {
		this.vvPolicyValidDt = vvPolicyValidDt;
	}

	public Date getVvPolicyIssueDt() {
		return vvPolicyIssueDt;
	}

	public void setVvPolicyIssueDt(Date vvPolicyIssueDt) {
		this.vvPolicyIssueDt = vvPolicyIssueDt;
	}

	public String getVvHypoTo() {
		return vvHypoTo;
	}

	public void setVvHypoTo(String vvHypoTo) {
		this.vvHypoTo = vvHypoTo;
	}

	public String getVvRcImgPath() {
		return vvRcImgPath;
	}

	public void setVvRcImgPath(String vvRcImgPath) {
		this.vvRcImgPath = vvRcImgPath;
	}

	public String getVvPerSlpImgPath() {
		return vvPerSlpImgPath;
	}

	public void setVvPerSlpImgPath(String vvPerSlpImgPath) {
		this.vvPerSlpImgPath = vvPerSlpImgPath;
	}

	public String getVvPolicyImgPath() {
		return vvPolicyImgPath;
	}

	public void setVvPolicyImgPath(String vvPolicyImgPath) {
		this.vvPolicyImgPath = vvPolicyImgPath;
	}

	public String getVvModelNo() {
		return vvModelNo;
	}

	public void setVvModelNo(String vvModelNo) {
		this.vvModelNo = vvModelNo;
	}

	public String getVvTransferId() {
		return vvTransferId;
	}

	public void setVvTransferId(String vvTransferId) {
		this.vvTransferId = vvTransferId;
	}

	public Date getVvTransferDt() {
		return vvTransferDt;
	}

	public void setVvTransferDt(Date vvTransferDt) {
		this.vvTransferDt = vvTransferDt;
	}

	
	
}

package com.mylogistics.constants;

public class TempIntBrTvCNTS {

	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String TIBT_BR_CODE = "tibBranchCode";
	
	public static final String TIBT_FA_CODE = "tibFaCode";
	
	public static final String TIBT_IS_CLEAR = "tibIsClear";
	
	public static final String TIBT_CS_ID = "tibCsId";
}

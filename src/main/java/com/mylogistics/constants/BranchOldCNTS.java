package com.mylogistics.constants;

public class BranchOldCNTS {
	
	public static final String BRANCH_OLD_ID = "branchOldId";

	public static final String BRANCH_ID = "branchId";
	
	public static final String BRANCH_CODE = "branchCode";
	
	public static final String BRANCH_NAME = "branchName";
	
	public static final String BRANCH_ADD = "branchAdd";
	
	public static final String BRANCH_CITY = "branchCity";
	
	public static final String BRANCH_STATE = "branchState";
	
	public static final String BRANCH_PIN = "branchPin";
	
	public static final String BRANCH_OPEN_DT = "branchOpenDt";
	
	public static final String BRANCH_CLOSE_DT = "branchCloseDt";
	
	public static final String BRANCH_STATION_CODE = "branchStationCode";
	
	public static final String BRANCH_DIRECTOR = "branchDirector";
	
	public static final String BRANCH_MARKETING_HD = "branchMarketingHD";
	
	public static final String BRANCH_OUT_STANDING_HD = "branchOutStandingHD";
	
	public static final String BRANCH_MARKETING = "branchMarketing";
	
	public static final String BRANCH_MNGR = "branchMngr";
	
	public static final String BRANCH_CASHIER= "branchCashier";
	
	public static final String BRANCH_TRAFFIC = "branchTraffic";
	
	public static final String BRANCH_REGIONAL_MNGR = "branchRegionalMngr";
	
	public static final String BRANCH_AREA_MNGR = "branchAreaMngr";
	
	public static final String BRANCH_PHONE = "branchPhone";
	
	public static final String BRANCH_FAX = "branchFax";
	
	public static final String BRANCH_EMAIL_ID = "branchEmailId";
	
	public static final String BRANCH_WEBSITE = "branchWebsite";
		
	public static final String IS_OPEN = "isOpen";
	
	public static final String IS_NCR = "isNCR";
	
	public static final String USER_CODE = "userCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String B_CODE = "bCode";
	
	public static final String IS_VIEW = "isView";
}

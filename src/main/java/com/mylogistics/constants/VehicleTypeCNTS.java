package com.mylogistics.constants;

public class VehicleTypeCNTS {
	
	public static final String VT_ID = "vtId";
	
	public static final String VT_SERVICE_TYPE = "vtServiceType";
	
	public static final String VT_VEHICLE_TYPE = "vtVehicleType";
	
	public static final String VT_CODE = "vtCode";
	
	public static final String VT_LOAD_LIMIT = "vtLoadLimit";
	
	public static final String VT_GUARANTEE_WT = "vtGuaranteeWt";
	
	public static final String USER_CODE = "userCode";

	public static final String CREATION_TS = "creationTS";
	
	public static final String B_CODE = "bCode";
}

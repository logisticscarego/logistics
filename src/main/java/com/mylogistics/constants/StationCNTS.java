package com.mylogistics.constants;

public class StationCNTS {

    public static final String STN_ID = "stnId";
	
	public static final String STN_CODE = "stnCode";
	
	public static final String STN_NAME = "stnName";
	
	public static final String STATE_CODE = "stateCode";
	
	public static final String STN_DISTRICT = "stnDistrict";
	
	public static final String STN_PIN = "stnPin";
	
	public static final String STN_CITY = "stnCity";
	
	public static final String USER_CODE = "userCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String B_CODE = "bCode";
	
}

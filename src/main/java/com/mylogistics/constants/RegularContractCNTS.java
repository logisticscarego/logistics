package com.mylogistics.constants;

public class RegularContractCNTS {
	
		public static final String REG_CONT_ID="regContId";
	
		public static final String REG_CONT_CODE="regContCode";
	
		public static final String BRANCH_CODE="branchCode";
	
		public static final String REG_CONT_BLPM_CODE="regContBLPMCode";
	
		public static final String REG_CONT_CNGR_CODE="regContCngrCode";
	
		public static final String REG_CONT_CP_NAME="regContCpName";
	
		public static final String REG_CONT_FROM_STATION="regContFromStation";
	
		public static final String REG_CONT_TO_STATION="regContToStation";
	
		public static final String REG_CONT_RATE="regContRate";
	
		public static final String REG_CONT_VEHICLE_TYPE="regContVehicleType";
	
		public static final String REG_CONT_FROM_DT="regContFromDt";
	
		public static final String REG_CONT_TO_DT="regContToDt";
	
		public static final String REG_CONT_TYPE="regContType";
	
		public static final String REG_CONT_FROM_WT="regContFromWt";
	
		public static final String REG_CONT_TO_WT="regContToWt";
	
		public static final String REG_CONT_PRODUCT_TYPE="regContProductType";
	
		public static final String REG_CONT_TRANSIT_DAY="regContTransitDay";
	
		public static final String REG_CONT_STATISTICAL_CHANGE="regContStatisticalChange";
		
		public static final String REG_CONT_LOAD="regContLoad";
	
		public static final String REG_CONT_UNLOAD="regContUnLoad";
	
		public static final String REG_CONT_PENALTY="regContPenalty";
	
		public static final String REG_CONT_BONUS="regContBonus";
	
		public static final String REG_CONT_DETENTION="regContDetention";
	
		public static final String REG_CONT_PROPERTIONATE="regContProportionate";
		
		public static final String REG_CONT_ADDITIONAL_RATE="regContAdditionalRate";
	
		public static final String REG_CONT_VALUE="regContValue";
			
		public static final String REG_CONT_DC="regContDc";
			
		public static final String REG_CONT_COST_GRADE="regContCostGrade";
	
		public static final String REG_CONT_WT="regContWt";
			
		public static final String REG_CONT_DDL="regContDdl";
			
		public static final String REG_CONT_RENEW="regContRenew";
			
		public static final String REG_CONT_RENEW_DT="regContRenewDt";
			
		public static final String REG_CONT_INSURED_BY="regContInsuredBy";
			
		public static final String REG_CONT_INSURE_COMP="regContInsureComp";
			
		public static final String REG_CONT_INSURE_UNIT_NO="regContInsureUnitNo";
			
		public static final String REG_CONT_INSURE_POLICY_NO="regContInsurePolicyNo";
			
		public static final String REG_CONT_REMARK="regContRemark";
		
		public static final String REG_CONT_IS_VERIFY="regContIsVerify";
		
		public static final String REG_CONT_FA_CODE="regContFaCode";
	
		public static final String USER_BRANCH_CODE="bCode";
		
		public static final String USER_CODE="userCode";
		
		public static final String IS_VIEW="isView";
}

package com.mylogistics.constants;

public class CustomerBillStatusCNTS {
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String USER_CODE = "userCode";
	
	public static final String CUST_BS_ID = "custBSId";
	
	public static final String CUST_BS_BILL_NO = "custBSBillNo";
	
	public static final String CUST_BS_CUST_CODE = "custBSCustCode";
	
	public static final String CUST_BS_CONT_CODE = "custBSContCode";
	
	public static final String CUST_BS_BILL_CURRENCY = "custBSbillCurrency";
	
	public static final String CUST_BS_BILL_TYPE = "custBSBillType";
	
	public static final String CUST_BS_BILL_ADD_ID = "custBSBillAdd_Id";
	
	public static final String CUST_BS_STATUS = "custBS_Status";
	
	public static final String CUST_BS_BILL_TOTAL = "custBSBillTotal";

}

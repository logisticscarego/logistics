package com.mylogistics.constants;

public class BillFormateNameCNTS {
	
	public static final String SIM_BI_FORMAT="simBillFormat";
	public static final String HND_BI_FORMAT="hndlBillFormat";
	public static final String REL_BI_FORMAT="relBillFormat";
	public static final String REN_BI_FORMAT="renBillFormat";
	public static final String BHU_BI_FORMAT="bhuBillFormat";
	public static final String HIL_BI_FORMAT="hilBillFormat";
	public static final String ADA_BI_FORMAT="adaniBillFormat";
	public static final String JIN_ST_BI_FORMAT="jindalSteelBillFormat";
	public static final String MSP_ST_BI_FORMAT="mspSteelBillFormat";
	public static final String SKS_PO_BI_FORMAT="sksPowerGenBillFormat";
	public static final String HID_MA_BI_FORMAT="hidalcoMahanBillFormat";
	public static final String HID_BI_FORMAT="hindalcoBillFormat";
	public static final String SKI_CA_BI_FORMAT="skiCarbonBillFormat";
	public static final String GRA_IN_BI_FORMAT="grasimIndusBillFormat";
	public static final String RUC_SO_BI_FORMAT="ruchiSoyaBillFormat";
	public static final String HIM_FR_BI_FORMAT="hmfBillFormat";
	public static final String BHI_BI_FORMAT="bhilaiBillFormat";
	public static final String CHE_SKI_CA_BI_FORMAT="SkiCArbonnChennaiBillFormat";
	public static final String KWA_BI_FORMAT="kwalityBillFormat";
	public static final String HIL_MUM_BI_FORMAT="HillLTDMUMBAIBillFormat";
	public static final String DAM_SHIP_BI_FORMAT="damanishipBillFormat";
	public static final String MUM_SKI_CA_BI_FORMAT="SkiMUMBAIBillFormat";
	public static final String ADANI_WIL_JAI_LUC_ALW_BI_FORMAT="AdaniWilmarJaiLucAlwBillFormat";
	public static final String PASSIVE_BI_FORMAT="PassiveInfraBillFormat";
	//public static final String HIN_UN_MA_BI_FORMAT="hindcoUniMahBillFormat";
			
	public static final String foramteName[]={
			SIM_BI_FORMAT,HND_BI_FORMAT,REL_BI_FORMAT,REN_BI_FORMAT,
			BHU_BI_FORMAT,HIL_BI_FORMAT,ADA_BI_FORMAT,JIN_ST_BI_FORMAT,
			MSP_ST_BI_FORMAT,SKS_PO_BI_FORMAT,HID_MA_BI_FORMAT,
			HID_BI_FORMAT,SKI_CA_BI_FORMAT,GRA_IN_BI_FORMAT,RUC_SO_BI_FORMAT,
			HIM_FR_BI_FORMAT,BHI_BI_FORMAT,CHE_SKI_CA_BI_FORMAT,KWA_BI_FORMAT,
			HIL_MUM_BI_FORMAT,DAM_SHIP_BI_FORMAT,MUM_SKI_CA_BI_FORMAT,
			ADANI_WIL_JAI_LUC_ALW_BI_FORMAT,PASSIVE_BI_FORMAT//,HIN_UN_MA_BI_FORMAT
														};

}

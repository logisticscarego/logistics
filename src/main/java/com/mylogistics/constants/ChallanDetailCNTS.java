package com.mylogistics.constants;

public class ChallanDetailCNTS {
	
	public static final String CHD_CHALLAN_CODE = "chdChlnCode";

	public static final String CREATION_TS = "creationTS";
	
	public static final String USER_CODE = "userCode";
	
	public static final String CHD_VALID_PAN = "chdValidPan";
	
	public static final String CHD_INVALID_PAN = "chdInvalidPan";
	
	public static final String CHD_OWN_CODE = "chdOwnCode";
	
	public static final String CHD_BR_CODE = "chdBrCode";
	
	/*public static final String CHD_VALID_DEC = "chdValidDec";
	
	public static final String CHD_INVALID_DEC = "chdInvalidDec";*/
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String BRANCH_CODE = "branchCode";
	
	public static final String IS_VIEW = "isView";
}

package com.mylogistics.constants;

public class CustomerCNTS {
	
	public static final String CUST_ID = "custId";
	
	public static final String CUST_CODE = "custCode";
	
	public static final String CUST_NAME = "custName";
	
	public static final String CUST_ADD = "custAdd";
	
	public static final String CUST_CITY = "custCity";
	
	public static final String CUST_STATE = "custState";
	
	public static final String CUST_PIN = "custPin";
	
	public static final String CUST_TDS_CIRCLE = "custTdsCircle";
	
	public static final String CUST_TDS_CITY = "custTdsCity";
	
	public static final String CUST_TIN_NO = "custTinNo";
	
	public static final String CUST_STATUS = "custStatus";
	
	public static final String CUST_PAN_NO = "custPanNo";
		
	public static final String CUST_IS_DAILY_CONT_ALLOW = "custIsDailyContAllow";
	
	public static final String IS_VERIFY = "isVerify";
	
	public static final String CUST_FA_CODE = "custFaCode";
	
    public static final String IS_VIEW="isView";
	
	public static final String USER_BRANCH_CODE="bCode";
	
	public static final String USER_CODE="userCode";
	
	public static final String BRANCH_CODE = "branchCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String CUST_GROUP_ID = "custGroupId";
	
	public static final String CUST_GST_NO = "custGstNo";
	
	public static final String NOT_FOUND = "Customer is not found.";

}

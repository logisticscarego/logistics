package com.mylogistics.constants;

import javax.persistence.Column;

public class ChallanCNTS {
	
	public static final String CHLN_ID = "chlnId";
	
	public static final String USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String IS_VIEW = "isView";
	
	public static final String CHALLAN_CODE = "chlnCode";
	
	public static final String CHALLAN_LHADV = "lhpvAdv";
	
	public static final String CHALLAN_LHBAL = "lhpvBal";
	
	public static final String BRANCH_CODE = "branchCode";
	
	public static final String CHLN_AR_ID = "chlnArId";
	
	public static final String CHLN_LRY_NO = "chlnLryNo";
	
	public static final String CHLN_FROM_STN = "chlnFromStn";
	
	public static final String CHLN_TO_STN = "chlnToStn";
	
	public static final String CHLN_EMP_CODE = "chlnEmpCode";
	
	public static final String CHLN_LRY_RATE = "chlnLryRate";
	
	public static final String CHLN_CHG_WT = "chlnChgWt";
	
	public static final String CHLN_NO_OF_PKG = "chlnNoOfPkg";
	
	public static final String CHLN_TOTAL_WT = "chlnTotalWt";
	
	public static final String CHLN_FREIGHT = "chlnFreight";
	
	public static final String CHLN_LOADING_AMT = "chlnLoadingAmt";
	
	public static final String CHLN_EXTRA = "chlnExtra";
	
	public static final String CHLN_TOTAL_FREIGHT = "chlnTotalFreight";
	
	public static final String CHLN_ADVANCE = "chlnAdvance";
	
	public static final String CHLN_BALANCE = "chlnBalance";
	
	public static final String CHLN_PAY_AT = "chlnPayAt";
	
	public static final String CHLN_TIME_ALLOW = "chlnTimeAllow";
	
	public static final String CHLN_DT = "chlnDt";
	
	public static final String CHLN_BR_RATE = "chlnBrRate";
	
	public static final String CHLN_WT_SLIP = "chlnWtSlip";
	
	public static final String CHLN_VEHICLE_TYPE = "chlnVehicleType";
	
	public static final String CHLN_STATISTICAL_CHG = "chlnStatisticalChg";
	
	public static final String CHLN_TDS_AMOUNT = "chlnTdsAmt";
		
	public static final String CHLN_RR_NO = "chlnRrNo";
	
	public static final String CHLN_TRAIN_NO = "chlnTrainNo";
	
	public static final String CHLN_LRY_LOAD_TIME = "chlnLryLoadTime";
	
	public static final String CHLN_LRY_REP_DT = "chlnLryRepDT";
	
	public static final String CHLN_LRY_RPT_TIME = "chlnLryRptTime";
	
	public static final String CHLN_IMAGE = "challanImage";
	
	public static final String CHLN_REM_ADV = "chlnRemAdv";
	
	public static final String CHLN_REM_BAL = "chlnRemBal";
	
	public static final String CHLN_CANCEL = "isCancel";
	
	public static final String CHLN_DETECTION = "chlnDetection";
	
	public static final String CHLN_TOOL_TAX = "chlnToolTax";
	
	public static final String CHLN_HEIGHT = "chlnHeight";
	
	public static final String CHLN_UNION = "chlnUnion";
	
	public static final String CHLN_TWO_POINT = "chlnTwoPoint";
	
	public static final String CHLN_WEIGHTMENT_CHG = "chlnWeightmentChg";
	
	public static final String CHLN_CRANE_CHG = "chlnCraneChg";
	
	public static final String CHLN_OTHERS = "chlnOthers";
	
	public static final String BANK_NAME = "bankName";
	
	public static final String IFSC_CODE = "ifscCode";
	
	public static final String ACCOUNT_NO = "accountNo";
	
	public static final String PAYEE_NAME = "payeeName";
	
	public static final String IS_PAID = "isPaid";
}

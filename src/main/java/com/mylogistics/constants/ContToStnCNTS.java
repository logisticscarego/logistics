package com.mylogistics.constants;

public class ContToStnCNTS {
	
	public static final String USER_BRANCH_CODE="bCode";
	
	public static final String USER_CODE="userCode";
	
	public static final String CONT_TO_STN="ctsToStn";
	
	public static final String CTS_RATE="ctsRate";

	public static final String CTS_CONT_CODE="ctsContCode";
	
	public static final String CTS_VEHICLE_TYPE="ctsVehicleType";

	public static final String CTS_FROM_WT="ctsFromWt";
	
	public static final String CTS_TO_WT="ctsToWt";
	
	public static final String CTS_FR_DT="ctsFrDt";
	
	public static final String CTS_TO_DT="ctsToDt";
	
	public static final String CTS_PRODUCT_TYPE="ctsProductType";
	
	public static final String CTS_ID="ctsId";
}

package com.mylogistics.constants;

public class MultipleStationCNTS {
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String IS_VIEW = "isView";

	public static final String STATION_CODE = "mulStnStnCode";
	
	public static final String STATION_REF_CODE = "mulStnRefCode";

}

package com.mylogistics.constants;

public class BranchMUStatsCNTS {

	public static final String BMUS_ID = "bmusId";
	
	public static final String BMUS_BRANCH_CODE = "bmusBranchCode";
	
	public static final String BMUS_AVG_MONTH_USAGE = "bmusAvgMonthUsage";
	
	public static final String BMUS_DT = "bmusDt";
	
	public static final String BMUS_DAYS_LAST = "bmusDaysLast";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String B_CODE = "bCode";
	
	public static final String USER_CODE = "userCode";
	
	public static final String BMUS_ST_TYPE="bmusStType";
}

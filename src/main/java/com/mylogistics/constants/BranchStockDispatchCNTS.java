package com.mylogistics.constants;

public class BranchStockDispatchCNTS {
	
	public static final String BSD_ID = "brsDisId";
	
	public static final String BSD_CODE = "brsDisCode";
	
	public static final String BSD_BRCODE = "brsDisBranchCode";
	
	public static final String BSD_CNMT= "brsDisCnmt";
	
	public static final String BSD_CHLN= "brsDisChln";
	
	public static final String BSD_SEDR= "brsDisSedr";
	
	public static final String BSD_DT= "brsDisDt";
	
	public static final String BSD_RDT= "brsDisRcvDt";
	
	public static final String BSD_INO= "brsDisInvNo";
	
	public static final String BSD_OPCODE = "operatorCode";
	
	public static final String BSD_IS_STOCK_RECEIVED ="isStockRecieved";
	
	public static final String USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String IS_ADMIN_VIEW="isAdminView";
	
	public static final String DISPATCH_MS="dispatchMsg";
}

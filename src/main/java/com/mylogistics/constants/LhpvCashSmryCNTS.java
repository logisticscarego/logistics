package com.mylogistics.constants;

public class LhpvCashSmryCNTS {

	public static final String LCS_LHPV_DT = "lcsLhpvDt";
	
	public static final String LCS_BRK_OWN = "lcsBrkOwn";
	
	public static final String LCS_BRH_CODE = "lcsBrhCode";
	
	public static final String LA_ID = "laId";
	
	public static final String LB_ID = "lbId";
	
	public static final String LSP_ID = "lspId";
	
	public static final String B_CODE = "bCode";
}

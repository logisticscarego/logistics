package com.mylogistics.constants;

public class EmployeeCNTS {

	public static final String EMP_ID = "empId";
	
	public static final String USER_CODE = "userCode";
	
	public static final String EMP_CODE = "empCode";
	
	public static final String EMP_FA_CODE = "empFaCode";
	
	public static final String EMP_NAME = "empName";
	
	public static final String EMP_ADD = "empAdd";
	
	public static final String EMP_CITY = "empCity";
	
	public static final String EMP_STATE = "empState";
	
	public static final String EMP_PIN = "empPin";
	
	public static final String EMP_DOB = "empDOB";
	
	public static final String EMP_DO_APPOINTMENT = "empDOAppointment";
	
	public static final String EMP_DESIGNATION = "empDesignation";
	
	public static final String EMP_BASIC = "empBasic";
	
	public static final String EMP_HRA = "empHRA";
	
	public static final String EMP_OTHER_ALLOWANCE = "empOtherAllowance";
	
	public static final String EMP_PF_NO = "empPFNo";
	
	public static final String EMP_ESI_NO = "empESINo";
	
	public static final String EMP_ESI_DISPENSARY = "empESIDispensary";
	
	public static final String EMP_NOMINEE = "empNominee";
	
	public static final String EMP_EDU_QUALI = "empEduQuali";
	
	public static final String EMP_DO_RESIG = "empDOResig";
	
	public static final String EMP_PF = "empPF";
	
	public static final String EMP_ESI = "empESI";
	
	public static final String EMP_LOAN_BAL = "empLoanBal";
	
	public static final String EMP_LOAN_PAYMENT = "empLoanPayment";
	
	public static final String EMP_PRESENT_ADD = "empPresentAdd";
	
	public static final String EMP_PRESENT_CITY = "empPresentCity";
	
	public static final String EMP_PRESENT_STATE = "empPresentState";
	
	public static final String EMP_PRESENT_PIN = "empPresentPin";
	
	public static final String EMP_SEX = "empSex";
	
	public static final String EMP_FATHER_NAME = "empFatherName";
	
	public static final String EMP_LICENSE_NO = "empLicenseNo";
	
	public static final String EMP_LICENSE_EXP = "empLicenseExp";
	
	public static final String EMP_TELEPHONE_AMT = "empTelephoneAmt";
	
	public static final String EMP_MOB_AMT = "empMobAmt";
	
	public static final String EMP_BANK_AC_NO = "empBankAcNo";
	
	public static final String EMP_PAN_NO = "empPanNo";
	
	public static final String EMP_MAIL_ID = "empMailId";
	
	public static final String EMP_PH_NO = "empPhNo";
	
	public static final String EMP_BANK_IFS = "empBankIFS";
	
	public static final String EMP_BANK_NAME = "empBankName";
	
	public static final String EMP_BANK_BRANCH = "empBankBranch";
	
	public static final String BRANCH_CODE = "branchCode";
	
	public static final String EMP_GROSS = "empGross";
	
	public static final String 	NET_SALARY = "netSalary";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String B_CODE = "bCode";
	
	public static final String IS_VIEW = "isView";
	
	public static final String IS_TERMINATE = "isTerminate";
	
	public static final String EMP_CODE_TEMP = "empCodeTemp";
}

package com.mylogistics.constants;

public class BillForwardingCNTS {

	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String USER_CODE = "userCode";
	
	public static final String BF_ID = "bfId";
	
	public static final String BF_BRH_CODE = "bfBrhCode";
	
	public static final String BF_REC_DT = "bfRecDt";
	
	public static final String BF_CUST_ID = "bfCustId";
	
	public static final String BF_NO = "bfNo";
	
}

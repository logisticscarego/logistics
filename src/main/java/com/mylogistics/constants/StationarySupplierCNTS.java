package com.mylogistics.constants;

public class StationarySupplierCNTS {
	
	public static final String 	ST_SUP_ID = "stSupID";
	
	public static final String 	ST_SUP_CODE = "stSupCode";
	
	public static final String 	ST_SUP_NAME = "stSupName";
	
	public static final String 	ST_SUP_ADDRESS= "stSupAdd";
	
	public static final String 	ST_SUP_CITY = "stSupCity";
	
	public static final String 	ST_SUP_STATE = "stSupState";
	
	public static final String 	ST_SUP_PIN = "stSupPin";
	
	public static final String 	ST_SUP_PAN_NO = "stSupPanNo";
	
	public static final String 	ST_SUP_TDSCODE = "stSupTdsCode";
	
	public static final String 	ST_SUP_SUBGROUP = "stSupSubGroup";
	
	public static final String 	ST_SUP_GROUP = "stSupGroup";
	
	public static final String 	USER_CODE = "userCode";
	
	public static final String 	B_CODE = "bCode";
	
	public static final String 	CREATION_TS = "creationTS";
	
	public static final String IS_VIEW = "isView";
}

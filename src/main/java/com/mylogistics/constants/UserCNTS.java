package com.mylogistics.constants;

public class UserCNTS {
	
	public static final String USER_ID = "userId";
	
	public static final String USER_CODE = "userCode";
	
	public static final String USER_NAME = "userName";
	
	public static final String PASSWORD = "password";
	
	public static final String USER_ROLE = "userRole";
	
	public static final String USER_STATUS = "userStatus";
	
	public static final String USER_AUTH_TOKEN = "userAuthToken";
	
	public static final String USER_BRANCH_CODE = "userBranchCode";

}

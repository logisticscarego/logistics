package com.mylogistics.constants;

public class StateCNTS {
	
	public static final String STATE_ID = "stateId";
	
	public static final String STATE_CODE = "stateCode";
	
	public static final String STATE_NAME = "stateName";
	
	public static final String STATE_LRY_PREFIX = "stateLryPrefix";
	
	public static final String STATE_STD = "stateSTD";
	
	public static final String USER_CODE = "userCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String B_CODE = "bCode";
	
	public static final String STATE_GST_CODE = "stateGST";

}

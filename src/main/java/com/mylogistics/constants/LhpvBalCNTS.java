package com.mylogistics.constants;

public class LhpvBalCNTS {

	public static final String LHPV_BAL_BRKOWN = "lbBrkOwn";
	
	public static final String LHPV_BAL_DT = "lbDt";
	
	public static final String LHPV_BAL_STATUS="lhpvStatus";
	
	public static final String LHPV_BAL_BCODE = "bCode";
	
	public static final String LHPV_BAL_ID = "lbId";
	
	public static final String LHPV_BAL_FINAL_TOT = "lbFinalTot";
	
	public static final String LHPV_BAL_PAY = "lbLryBalP";
	
	public static final String LHPV_BAL_CASH_DISC = "lbCashDiscR";
	
	public static final String LHPV_BAL_MUNS = "lbMunsR";
	
	public static final String LHPV_BAL_TDS = "lbTdsR";
	
	public static final String LHPV_BAL_WT_SHRTG = "lbWtShrtgCR";
	
	public static final String LHPV_BAL_DR_RCVR_WT = "lbDrRcvrWtCR";
	
	public static final String LHPV_BAL_LATE_DEL = "lbLateDelCR";
	
	public static final String LHPV_BAL_LATE_ACK = "lbLateAckCR";
			
	public static final String LHPV_BAL_OTH_EXT_KM = "lbOthExtKmP";
	
	public static final String LHPV_BAL_OTH_OVR_HGT = "lbOthOvrHgtP";
	
	public static final String LHPV_BAL_OTH_PNLTY = "lbOthPnltyP";
	
	public static final String LHPV_BAL_OTH_MISC = "lbOthMiscP";
	
	public static final String LHPV_BAL_UNLOADING = "lbUnLoadingP";
	
	public static final String LHPV_BAL_UNP_DET = "lbUnpDetP";
	
	public static final String LHPV_BAL_CHQ_NO = "lbChqNo";
	
	public static final String LHPV_BAL_TOT_RCVR_AMT = "lbTotRcvrAmt";
	
	public static final String LHPV_BAL_PAY_METHOD = "lbPayMethod";
	
	public static final String LHPV_BAL_IS_CLOSE = "lbIsClose";
	
	public static final String LHPV_BAL_BANK_CODE = "lbBankCode";
	
	public static final String LHPV_BAL_TOTAL_PAY_AMT = "lbTotPayAmt";
	
	public static final String LHPV_BAL_BANK_NAME = "lbBankName";
	
	public static final String LHPV_BAL_IFSC_CODE = "lbIfscCode";
	
	public static final String LHPV_BAL_ACCOUNT_NO = "lbAccountNo";
	
	public static final String LHPV_BAL_PAYEE_NAME = "lbPayeeName";
	
	public static final String LHPV_BAL_IS_PAID = "lbIsPaid";
	
	public static final String LHPV_BAL_BRH_FACODE = "lbBrhCode";
	
}

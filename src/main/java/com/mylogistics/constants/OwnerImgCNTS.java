package com.mylogistics.constants;

public class OwnerImgCNTS {

	public static final String OWN_ID = "ownId";
	
	public static final String USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
}

package com.mylogistics.constants;

public class HBOptr_NotificationCNTS {

	public static final String HBON_ID = "hbonId";
	
	public static final String HBON_NCNMT = "noCnmt";
	
	public static final String HBON_NCHLN = "noChln";
	
	public static final String HBON_NSEDR = "noSedr";
	
	public static final String HBON_OPCODE = "operatorCode";
	
	public static final String HBON_BRANCH_CODE = "disBranchCode";
	
	public static final String HBON_DISDT = "dispatchDate";
	
	public static final String HBON_DISCODE = "dispatchCode";
	
	public static final String USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String HBON_ISVIEW = "isView";
}

package com.mylogistics.constants;

public class CustomerRepresentativeCNTS {

	
	public static final String CR_ID = "crId";
	
	public static final String CR_CODE = "crCode";
	
	public static final String CR_NAME = "crName";
		
	public static final String CR_DESIGNATION = "crDesignation";
	
	public static final String CR_MOBILE_NO = "crMobileNo";
	
	public static final String CR_EMAIL_ID = "crEmailId";
	
	public static final String CR_FA_CODE = "crFaCode";
	
	public static final String CUST_ID = "custId";
	
	public static final String CUST_REF_NO = "custRefNo";
	
	public static final String CUST_CODE = "custCode";
	
	public static final String IS_VIEW="isView";
	
	public static final String USER_BRANCH_CODE="bCode";
		
	public static final String USER_CODE="userCode";
		
	public static final String BRANCH_CODE = "branchCode";
		
	public static final String CREATION_TS = "creationTS";

}

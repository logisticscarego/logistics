package com.mylogistics.constants;

public class ServiceTaxCNTS {
	
	public static final String ST_ID = "stId";
	
	public static final String ST_TAXABLE_RT = "stTaxableRt";
	
	public static final String ST_SER_TAX_RT = "stSerTaxRt";
	
	public static final String ST_EDU_CESS_RT = "stEduCessRt";
	
	public static final String ST_HSC_CESS_RT = "stHscCessRt";
	
	public static final String USER_CODE = "userCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String B_CODE = "bCode";

}

package com.mylogistics.constants;

public class RateByKmCNTS {

	public static final String RBKM_ID = "rbkmId";
	
	public static final String RBKM_CONT_CODE = "rbkmContCode";
	
	public static final String RBKM_FROM_STATION = "rbkmFromStation";
	
	public static final String RBKM_TO_STATION = "rbkmToStation";
	
	public static final String RBKM_START_DATE = "rbkmStartDate";
	
	public static final String RBKM_END_DATE = "rbkmEndDate";
	
	public static final String RBKM_STATE_CODE = "rbkmStateCode";
	
	public static final String RBKM_FROM_KM = "rbkmFromKm";
	
	public static final String RBKM_TO_KM = "rbkmToKm";
	
	public static final String RBKM_VEHICLE_TYPE = "rbkmVehicleType";
	
	public static final String RBKM_RATE = "rbkmRate";
}

package com.mylogistics.constants;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class NarrationCNTS {

	public static final String NARR_ID = "narrId";
	
	public static final String NARR_TYPE = "narrType";
	
	public static final String NARR_CODE = "narrCode";
	
	public static final String NARR_DESC= "narrDesc";
	
	public static final String NARR_USER_CODE = "narrUserCode";
	
	public static final String NARR_USER_BRANCH_CODE = "narrUserBCode";
	
	public static final String NARR_CREATION_TS= "creationTS";
	
}
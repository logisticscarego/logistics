package com.mylogistics.constants;

public class StationRightsCNTS {
	
	public static final String STRH_ID = "strhId";
	
	public static final String STRH_ST_CODE = "strhStCode";
	
	public static final String STRH_ADV_ALLOW = "strhAdvAllow";
	
	public static final String STRH_IS_FROM_ST = "strhIsFromSt";
	
	public static final String STRH_IS_TO_ST = "strhIsToSt";
	
	public static final String STRH_ADV_VALID_DT = "strhAdvValidDt";
	
	public static final String USER_CODE = "userCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String B_CODE = "bCode";

}

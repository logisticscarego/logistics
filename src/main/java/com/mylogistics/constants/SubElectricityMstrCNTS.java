package com.mylogistics.constants;

public class SubElectricityMstrCNTS {

	public static final String SEM_ID = "semId";
	
	public static final String SEM_PAY_AMT = "semPayAmt";
	
	public static final String SEM_BILL_FR_DT = "semBillFrDt";
	
	public static final String SEM_BILL_TO_DT = "semBillToDt";
	
	public static final String SEM_STAFF_CODE = "semStaffCode";
	
	public static final String USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
}

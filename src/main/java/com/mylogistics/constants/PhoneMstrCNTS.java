package com.mylogistics.constants;

public class PhoneMstrCNTS {

	public static final String PH_ID = "phId";
	
	public static final String PH_NO = "phNo";
	
	public static final String PH_TYPE = "phType";
	
	public static final String PH_TYPE_ALLOW = "phTypeAllow";
	
	public static final String PH_HLDR_NAME = "phHldrName";
	
	public static final String PH_WEF = "phWEF";
	
	public static final String PH_WET = "phWET";
	
	public static final String PH_INST_DT = "phInstDt";
	
	public static final String PH_DISC_DT = "phDiscDt";
	
	public static final String PH_DEP_AMT = "phDepAmt";
	
	public static final String PH_DEP_DT = "phDepDt";
	
	public static final String PH_REFUND_AMT = "phRefundAmt";
	
	public static final String PH_REFUND_DT = "phRefundDt";
	
	public static final String PH_SER_PRO = "phSerPro";

	public static final String PH_PLAN = "phPlan";
	
	public static final String PH_MOD_PAY = "phModPay";
	
	public static final String PH_LMT_AMT = "phLmtAmt";
	
	public static final String PH_AC_NO = "phAcNo";
	
	public static final String PH_BILL_DAY = "phBillDay";
	
	public static final String PH_DUE_DAY = "phDueDay";
	
	public static final String EMPLOYEE = "employee";
	
	public static final String BRANCH = "branch";
	
	public static final String PH_BILLING_ADD = "phBillingAdd";
	
	
	
}

package com.mylogistics.constants;

public class DailyContractCNTS {
	
	public static final String DLY_CONT_ID="dlyContId";
	
	public static final String DLY_CONT_CODE="dlyContCode";
	
	public static final String BRANCH_CODE="branchCode";
	
	public static final String DLY_CONT_BLPM_CODE="dlyContBLPMCode";
	
	public static final String DLY_CONT_CNGR_CODE="dlyContCngrCode";
	
	public static final String DLY_CONT_CP_NAME="dlyContCpName";
	
	public static final String DLY_CONT_START_DT="dlyContStartDt";
	
	public static final String DLY_CONT_END_DT="dlyContEndDt";
	
	public static final String DLY_CONT_FROM_STATION="dlyContFromStation";
	
	public static final String DLY_CONT_TO_STATION="dlyContToStation";
	
	public static final String DLY_CONT_RATE="dlyContRate";
	
	public static final String DLY_CONT_FROM_WT="dlyContFromWt";
	
	public static final String DLY_CONT_TO_WT="dlyContToWt";
	
	public static final String DLY_CONT_TYPE="dlyContType";
	
	public static final String DLY_CONT_PRODUCT_TYPE="dlyContProductType";
	
	public static final String DLY_CONT_RBKMID="dlyContRbkmId";
	
	public static final String DLY_CONT_VEHICLE_TYPE="dlyContVehicleType";
	
	public static final String DLY_CONT_DC="dlyContDc";
	
	public static final String DLY_CONT_COST_GRADE="dlyContCostGrade";
	
	public static final String DLY_CONT_TRANSIT_DAY="dlyContTransitDay";
	
	public static final String DLY_CONT_STATISTICAL_CHANGE="dlyContStatisticalChange";
	
	public static final String DLY_CONT_LOAD="dlyContLoad";
	
	public static final String DLY_CONT_UNLOAD="dlyContUnLoad";
	
	public static final String DLY_CONT_PENALTY="dlyContPenalty";
	
	public static final String DLY_CONT_BONUS="dlyContBonus";
	
	public static final String DLY_CONT_DETENTION="dlyContDetention";
	
	public static final String DLY_CONT_HOUR_OR_DAY="dlyContHourOrDay";
	
	public static final String DLY_CONT_PROPERTIONATE="dlyContProportionate";
	
	public static final String DLY_CONT_ADDITIONAL_RATE="dlyContAdditionalRate";
	
	public static final String DLY_CONT_IS_VERIFY="dlyContIsVerify";
	
	public static final String IS_VIEW="isView";
	
	public static final String DLY_CONT_FA_CODE="dlyContFaCode";
	
	public static final String USER_BRANCH_CODE="bCode";
	
	public static final String USER_CODE="userCode";
	
}

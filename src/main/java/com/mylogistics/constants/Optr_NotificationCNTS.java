package com.mylogistics.constants;

public class Optr_NotificationCNTS {

	public static final String OPN_ID = "onId";
	
	public static final String OPN_DISCODE = "dispatchCode";
	
	public static final String OPN_DISDT = "dispatchDate";
	
	public static final String OPN_OPCODE = "operatorCode";
	
	public static final String OPN_ISREC = "isRecieved";
	
	public static final String USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
}

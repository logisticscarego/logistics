package com.mylogistics.constants;

public class BrokerCNTS {

	public static final String BRK_CODE = "brkCode";
	
	public static final String BRK_FA_CODE = "brkFaCode";
	
	public static final String BRK_ID = "brkId";
	
	public static final String BRANCH_CODE = "branchCode";
	
	public static final String BRK_NAME = "brkName";
	
	public static final String BRK_COM_START_DATE = "brkComStbDt";
	
	public static final String BRK_PAN_DOB = "brkPanDOB";
	
	public static final String BRK_PAN_No = "brkPanNo";
	
	public static final String BRK_PAN_DATE = "brkPanDt";
	
	public static final String BRK_PAN_NAME = "brkPanName";
	
	public static final String BRK_VEHICLE_TYPE = "brkVehicleType";
	
	public static final String BRK_PP_NO = "brkPPNo";
	
	public static final String BRK_VOTER_ID = "brkVoterId";
	
	public static final String BRK_SRV_TAX_NO = "brkSrvTaxNo";
	
	public static final String BRK_FIRM_REG_NO = "brkFirmRegNo";
	
	public static final String BRK_REG_PLACE = "brkRegPlace";
	
	public static final String BRK_FIRM_TYPE = "brkFirmType";

	public static final String BRK_CIN = "brkCIN";
	
	public static final String BRK_BSN_CARD = "brkBsnCard";
	
	public static final String BRK_UNION = "brkUnion";
	
	public static final String BRK_EMAIL_ID = "brkEmailId";
	
	public static final String BRK_ACTIVE_DT = "brkActiveDt";
	
	public static final String BRK_DEACTIVE_DT = "brkDeactiveDt";
	
	public static final String BRK_USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String IS_VIEW = "isView";
	
	public static final String IS_BRK_BNK_DET = "isBrkBnkDet";
	
	public static final String BRK_ACCNT_NO = "brkAccntNo";
	
	public static final String BRK_IFSC = "brkIfsc";
	
	public static final String BRK_MICR = "brkMicr";
	
	public static final String BRK_BNK_BRANCH = "brkBnkBranch";

	public static final String BRK_VALID_PAN = "brkValidPan";
	
	public static final String BRK_INVALID_PAN = "brkInvalidPan";
	
	public static final String BRK_VALID_BNK_DET = "brkBnkDetValid";
	
	public static final String BRK_INVALID_BNK_DET = "brkBnkDetInValid";
	
	
}

package com.mylogistics.constants;

public class HOSLastCNTS {

	public static final String HOSL_ID = "hOSLastId";
	
	public static final String HOSL_CNMT = "hOSCnmt";
	
	public static final String HOSL_CNMT_MLAST = "hOSCnmtMonthLast";
	
	public static final String HOSL_CHLN = "hOSChln";
	
	public static final String HOSL_CHLN_MLAST = "hOSChlnMonthLast";
	
	public static final String HOSL_SEDR = "hOSSedr";
	
	public static final String HOSL_SEDR_MLAST = "hOSSedrMonthLast";
	
	public static final String HOSL_TYPE = "hOSType";
	
	public static final String HOSL_DATE = "hOSDate";
	
	public static final String USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
}

package com.mylogistics.constants.bank;

public class ChequeLeavesCNTS {

	public static final String CHQ_L_ID = "chqLId";
	
	public static final String CHQ_L_BR_CODE = "chqLBrCode";
	
	public static final String BANK_MSTR = "bankMstr";
	
	public static final String CHQ_L_CHQ_NO = "chqLChqNo";
	
	public static final String CHQ_L_CHQ_TYPE = "chqLChqType";
	
	public static final String CHQ_L_C_TYPE = "chqLCType";
	
	public static final String CHQ_L_CHQ_AMT = "chqLChqAmt";
	
	public static final String CHQ_L_CHQ_MAX_LT = "chqLChqMaxLt";
	
	public static final String CHQ_L_ISSUE_DT = "chqLIssueDt";
	
	public static final String CHQ_L_USED_DT = "chqLUsedDt";
	
	public static final String CASH_STMT = "cashStmt";
	
	public static final String CHQ_L_CHQ_CANCEL_DT = "chqLChqCancelDt";
	
	public static final String CHQ_L_CHQ_CANCEL = "chqLCancel";
	
	public static final String CHQ_USED = "chqUsed";
	public static final String CHQ_L_USED = "chqLUsed";
	
	public static final String CASH_WITHDRAW = "cashWithdraw";
	
	public static final String BNK_ID = "bnkId";
	
	
}

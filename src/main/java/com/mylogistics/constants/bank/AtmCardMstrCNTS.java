package com.mylogistics.constants.bank;


public class AtmCardMstrCNTS {

	public static final String ATM_CARD_ID = "atmCardId";
	
	public static final String ATM_CARD_NO = "atmCardNo";
	
	public static final String ATM_CARD_BRH_ID = "branchId";
	
}

package com.mylogistics.constants.bank;


public class ChequeRequestCNTS {

	public static final String C_REQ_ID = "cReqId";
	
	public static final String C_REQ_REF_NO = "cReqRefNo";
	
	public static final String C_REQ_DT = "cReqDt";
	
	public static final String C_REQ_NO_BOOK = "cReqNOBook";
	
	public static final String C_REQ_NO_CHQ_PER_BOOK = "cReqNOChqPerBook";
	
	public static final String C_REQ_TYPE = "cReqType";
	
	public static final String C_REQ_PRINT_TYPE = "cReqPrintType";
	
	public static final String C_REQ_IS_RECEIVED = "cReqIsReceived";
	
	public static final String C_REQ_IS_CANCELLED = "cReqIsCancelled";
	
	public static final String USER_CODE = "userCode";
	
	public static final String B_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
}

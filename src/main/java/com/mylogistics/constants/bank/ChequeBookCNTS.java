package com.mylogistics.constants.bank;


public class ChequeBookCNTS {

	public static final String CHQ_BK_ID = "chqBkId";
	
	public static final String CHQ_BK_FROM_CHQ_NO = "chqBkFromChqNo";
	
	public static final String CHQ_BK_TO_CHQ_NO = "chqBkToChqNo";
	
	public static final String CHQ_BK_NO_CHQ = "chqBkNOChq";
	
	public static final String CHQ_BK_IS_ISSUED = "chqBkIsIssued";
	
	public static final String CHQ_BK_IS_RECEIVED = "chqBkIsReceived";
	
	public static final String CHQ_BK_ISSUE_DT = "chqBkIssueDt";
	
	public static final String CHQ_BK_RECEIVE_DT = "chqBkReceiveDt";
	
	public static final String USER_CODE = "userCode";
	
	public static final String B_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
}

package com.mylogistics.constants.bank;

public class BankMstrCNTS {

	public static final String BNK_ID = "bnkId";
	
	public static final String BNK_BRH_ID = "branchId";

	public static final String BNK_FA_CODE = "bnkFaCode";

	public static final String ADDRESS = "address";

	public static final String BNK_AC_NO = "bnkAcNo";

	public static final String BNK_OPEN_DT = "bnkOpenDt";

	public static final String BNK_CLOSE_DT = "bnkCloseDt";

	public static final String BNK_LOCAL = "bnkLocal";

	public static final String BNK_BR_MIN_CHQ = "bnkBrMinChq";

	public static final String BNK_BR_MAX_CHQ = "bnkBrMaxChq";

	public static final String BNK_HO_MIN_CHQ = "bnkHoMinChq";

	public static final String BNK_HO_MAX_CHQ = "bnkHoMaxChq";

	public static final String BNK_NAME = "bnkName";

	public static final String BNK_IFSC_CODE = "bnkIfscCode";
	
	public static final String BNK_MICR_CODE = "bnkMicrCode";

	public static final String BRANCH = "branch";

	public static final String CHEQUE_LEAVES_LIST = "chequeLeavesList";
	
	public static final String BNK_SINGLE_SIGNATORY_LIST = "bnkSingleSignatoryList";
	
	public static final String BNK_SINGLE_JOINT_LIST = "bnkJointSignatoryList";

	public static final String USER_CODE = "userCode";

	public static final String CREATION_TS = "creationTS";

	public static final String B_CODE = "bCode";
	
	public static final String ADDRESS_TYPE = "BankBranchAddress";
	
	public static final String BNK_ALLOTED = "bnkAlloted";
	
	public static final String NOT_FOUND = "Bank is not found.";

}

package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.Optr_Notification;

@Repository
public interface Optr_NotificationDAO {
	
	 public int saveOptr_Notification(Optr_Notification optr_Notification);
	 
	 public List<Optr_Notification> getOptr_NotificationData(String operatorCode);
	 
	 public int updateIsReceiveYes(String dispatchcode);
}

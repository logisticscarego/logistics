package com.mylogistics.DAO;

import java.sql.Date;
import java.util.List;

import com.mylogistics.model.Acknowledge_Validation;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.CnmtImage;

public interface Acknowledge_ValidationDao {
	
	public int saveAckValidation(Acknowledge_Validation acknowledge_Validation);
	
	public List<CnmtImage> getCnmtImage(int cmId);
	
	//public List<Acknowledge_Validation> getAckValidatorFromTODate(Date fromDate,Date toDate);
	
	public List<Acknowledge_Validation> getAckByCnmtCode(String cnmtCode);
	
	public void updateAckValidation(int ackId,Acknowledge_Validation acknowledge_Validation,ArrivalReport arrivalReport);

	public List<Acknowledge_Validation> getAckValidatorFromTODate(Date fromDate,
			Date toDate, String remarkType);

}

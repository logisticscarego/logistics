package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.Stationary;
import com.mylogistics.model.User;
import com.mylogistics.services.StnTransferService;

@Repository
public interface StationaryDAO {
	
	//Sanjana: saves stationaryOrder to database
	public int saveStationary(Stationary stationary);
	
	//Sanjana: retrieves the last row of stationaryOrder table
	public int getLastStationaryId();
	
	//Sanjana: returns the total number of rows in  stationaryOrder table
	public long totalCount();

	public int updateStationary(Stationary stationary);
	
	public Stationary getStnById(int stnId);
	
	public int saveCustStnry(Map<String, Object> custStnryService);
	
	public List<Map<String, Object>> getBrhStkDetail(String brhCode);
	
	public List<String> fetchStnaryDetail(Map<String, String> initParam);
	
	public List<Map<String, Object>> getMstrStock(Session session);
	
	public List<Map<String, Object>> findByStk(Session session, Map<String, String> initParam);
	
	public Map<String, Object> doStnTransfer(Session session, User user, StnTransferService stnTransferService);
	
	public Map<String, Object> isStnAvailable(Session session, User user, StnTransferService stnService);
	
	public int reIssueStsnry(Map<String, String> stnryData);

}

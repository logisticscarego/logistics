package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import com.mylogistics.model.CashStmt;
import com.mylogistics.services.TravDivService;
import com.mylogistics.services.TravVoucherService;
import com.mylogistics.services.VoucherService;

public interface FaTravDivisionDAO {

	public Map<String,Object> saveTrvlVoucByCash(VoucherService voucherService , TravVoucherService travVoucherService);
	
	public Map<String,Object> saveTrvlVouchByChq(VoucherService voucherService , TravVoucherService travVoucherService);
	
	public Map<String,Object> saveTrvlVouchByOnline(VoucherService voucherService , TravVoucherService travVoucherService);
	
	public List<TravDivService> getSubFileDetails(List<CashStmt> reqCsList);
}

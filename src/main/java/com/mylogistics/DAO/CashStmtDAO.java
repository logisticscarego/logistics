package com.mylogistics.DAO;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.services.BTBFundTransService;
import com.mylogistics.services.CashStmtService;

@Repository
public interface CashStmtDAO {

	public int saveCashStmt(CashStmt cashStmt);
	
	public int updateCashStmt(int csId);
	
	public int saveFundTrans(BTBFundTransService btbFundTransService , List<Map<String,Object>> toBrhList);
	
	public int saveEditCS(CashStmtService cashStmtService);
	
	public Integer checkCsIsClose(Date date);
	
	public CashStmt getLastCashStmt(int cssId);
	/*public int editCsDesc();*/
}

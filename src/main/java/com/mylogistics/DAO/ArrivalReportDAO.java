package com.mylogistics.DAO;

import java.sql.Blob;
import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.CnmtImage;
import com.mylogistics.model.Memo;
import com.mylogistics.model.User;
import com.mylogistics.model.lhpv.LhpvBal;
import com.mylogistics.services.MemoService;
import com.mylogistics.services.VoucherService;

public interface ArrivalReportDAO {
	
	//Shikha: saves arrival report to database
	public int saveArrivalReportToDB(ArrivalReport ar);
	
	public int saveArrivalReportToDB(ArrivalReport ar,Session session);
	
	//Shikha: retrieves the list of arrival report whose isView is false
	public List<ArrivalReport> getARisViewFalse(String branchCode);
	
	//Shikha: updates the arrival report isView to true
	public int updateIsViewARTrue(String code[]);
	
	//Shikha: retrieves arrival report from database on the basis of entered arrival report code
	public List<ArrivalReport> getARList(String arCode);

	//Shikha: updates the arrival report
	public int updateAR(ArrivalReport arrivalReport);
	
	public Blob getArImageByCode(String arCode);
	
	public List<ArrivalReport> getArrivalReport(String arCode);
	
	public List<String> getArCode();
	
	public List<ArrivalReport> getAllArCodes(String branchCode);
	
	public int updateAr(ArrivalReport ar);
	
	public List<ArrivalReport> getARByChlnCode(String chlnCode);
	
	public int saveCompARToDB(ArrivalReport arrivalReport);
	
	public int saveCompARToDB(ArrivalReport arrivalReport,Session session);
	
	public ArrivalReport getArById(int arId);

	public int saveArRemBal(VoucherService voucherService);
	
	public int saveArRemBal(VoucherService voucherService, Session session);

	public int saveRemBalSup(VoucherService voucherService);

	public int sedrDtlFrCncl(Map<String, String> sedrDtl);
	
	public List getArChlnCodeFrSrtgDmgAlw();
	
	public Memo getLastMemoByBranch(Session session, User user, String branchCode);
	
	public List<ArrivalReport> getArDetailForMemo(Session session, User user, String arNo);
	
	public Map<String, Object> saveArMemo(Session session, User user, MemoService memoService, Map<String, Object> resultMap);
	
	public Memo getMemoWithChild(Session session, User user, String memoNo);

	public int doValidate(Map<String, Object> map);

	public List<Integer> getChlnId(String chlnCode);

	public List<Integer> getCnmtId(List<Integer> chlnId);

	public List<CnmtImage> getCNmtImageData(List<Integer> cnmtId);

	public List<ArrivalReport> getArByDate(Date startDate, Date endDate);

	public int updateRemAr(Map<String, Object> map);
	
}

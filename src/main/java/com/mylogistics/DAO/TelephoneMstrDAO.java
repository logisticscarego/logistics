package com.mylogistics.DAO;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.SubTelephoneMstr;
import com.mylogistics.model.TelephoneMstr;

@Repository
public interface TelephoneMstrDAO {

	public int saveTelephoneMstr(Branch branch , Employee employee , TelephoneMstr telephoneMstr);
	
	public List<TelephoneMstr> getAllTelMstr();
	
	public int updateTMBySTM(int tmId , SubTelephoneMstr subTelephoneMstr);

	public int updateTMBySTM(Session session, int tmId,
			SubTelephoneMstr subTelephoneMstr);
}

package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.HBO_StnNotification;


@Repository
public interface HBO_StnNotificationDAO {

	public int saveHBO_StnNot(HBO_StnNotification hBO_StnNotification);
	
	public List<HBO_StnNotification> getHBOSNData(String opCode);
	
	public int updateHBOSN(HBO_StnNotification HBOSN);
	
	public List<HBO_StnNotification> getHBOSNForSA();
	
	public int confirmOrder(int id);
	
	public int updateIsCreate(HBO_StnNotification hBO_StnNotification);
	
	public int updateHBO(Map<String , Object> clientMap);
}

package com.mylogistics.DAO;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.OtherCharge;

@Repository
public interface OtherChargeDAO {
	
	public int saveOtChg(OtherCharge otherCharge);
}

package com.mylogistics.DAO;

import java.sql.Blob;
import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import com.mylogistics.model.User;

public interface AllImgDao {

	public List<Map<String, Object>> getUnsavedCnmtImg(String branchCode, String code);
	
	public Map<String, Object> updateImage(byte fileInBytes[], Integer imgId);
	
	public Blob downloadImg(Session session, User user, Map<String, String> initParam);
	
	public Map<String, Object> getOwnBrkForImgRead(Session session, User user, Map<String, String> initParam, Map<String, Object> resultMap);
	
	public Map<String, Object> upldExcelFile(byte fileInBytes[],String fileName);
	
	public Blob downloadExcel(Session session, int bsuId);
	
	public Map<String, Object> getFileNames(Date date);

	public Blob downloadImgByCode(Session session, User user, Map<String, String> initParam);
	
	
	
	
}
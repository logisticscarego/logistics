package com.mylogistics.DAO;

import java.sql.Date;
import java.util.List;

import com.mylogistics.model.FYear;


public interface FYearDAO {
	
	public List<FYear> getAllFYear();
	
	public FYear getCurrentFYear();
	
	public FYear getFYearByDate(Date date);	
	
}
package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.Remarks;

@Repository
public interface RemarksDAO {
	
	public int getVerify(String chlnCode);
	
	public int holdChallan(Map<String , Object> clientMap,Session session);
	
	public List verifyFrBal(String docNo);
	
	public Map<String, Object> getHoldChln();
	
	public List getObjFrUnhldDocs(String chlnCode);
	
	public int allowDocsFrHold(Remarks remarks,Session session);

}

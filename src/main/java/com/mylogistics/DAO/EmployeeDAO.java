package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.Employee;

@Repository
public interface EmployeeDAO {

	//Satarupa: saves employee to database
	public int saveEmployeeToDB(Employee employee);
	
	//Satarupa: retrieve the details of employee table on the basis of empcode
	public List<Employee> retrieveEmployee(String empCode);
	
	//Satarupa: retrieve the details of employee table on the basis of empCodeTemp
	public List<Employee> empDataByEmpCodeTemp(String empCodeTemp);

	//Satarupa: updates the employee table on the basis of employeeCode and branchCode
	public int updateEmployee(String employeeCode,String branchCode);
	
	//Satarupa: returns the total number of rows in employee table
	public long totalCount();
	
	//Satarupa: retrieves the last row of employee table
	public List<Employee> getLastEmployeeRow();
	
	//Satarupa: retrieve the list of all employees where isView=no and bCode is same as current user.
	public List<Employee> getAllEmployeeForAdmin(String branchCode);
	
	public List<Employee> getAllEmployee();
	
	//Satarupa: updates the employee table for the employees whose details have been checked. 
	public int updateEmployeeisViewTrue(String code[]);
	
	//Satarupa: updates the employee table on editing by admin
	 public int updateEmployeeToDB(Employee employee);
	 
	//Satarupa: get all employees who are not terminated.
	 public List<Employee> getAllActiveEmployees();
	 
	 public List<Employee> getAllActiveEmployeesByName(String empName);
	 
	/*//Satarupa: get list of all employee codes.
	 public List<String> getEmployeeCodes();*/
	 
	 public List<Employee> getAll_HB_Employee(String branchCode);
	 
	//Satarupa: get the list of all employee codes.
	 public List<Employee> getAllEmpForAdmin(String branchCode);
	 
	//Satarupa: retrieve the details of employee table on the basis of employee Id
	 public List<Employee> getOldEmployee(int oldEmployeeId);
	 
	 public String getEmpNameByCode(String empCode);
	 
	 
	 public List<Employee> getNewEmp();
	 
	 public int assignPrBranch(int empId , int branchId);
	 
	 public String getEmpNameByEmpCode(String empCode);
	 
	 public List<String> getAllEmpBrh(String empFaCode);
	 
	 public Employee getCmpltEmp(String empCode);
	 
	 public int updateEmpPart(Employee employee);
	 
	 public List<Map<String, Object>> getEmpList();
}

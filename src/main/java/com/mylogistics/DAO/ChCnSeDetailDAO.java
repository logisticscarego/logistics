package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;
import com.mylogistics.model.ChCnSeDetail;

@Repository
public interface ChCnSeDetailDAO {
	
	//Sanjana: saves the chcnse to database
	public int saveChCnSE(ChCnSeDetail chCnSeDetail);
	
//	//Sanjana: retrieves the last entered row with status='cnmt'
//	public List<ChCnSeDetail> getLastCnmt();
//	
//	//Sanjana: retrieves the last entered row with status='chlm'
//	public List<ChCnSeDetail> getLastChln();
//	
//	//Sanjana: retrieves the last entered row with status='sedr'
//	public List<ChCnSeDetail> getLastSedr();
	
	public List<ChCnSeDetail> getLastStationary(String status);

}

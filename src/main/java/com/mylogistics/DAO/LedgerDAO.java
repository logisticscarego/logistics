package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import com.mylogistics.model.MoneyReceipt;

public interface LedgerDAO {

	public List<Map<String, Object>> getBrLedger(Map<String, Object> ledgerReportMap);
	public List<Map<String, Object>> getBrLedgerFinalList(List<Map<String, Object>> ledgerMapListTemp);
	
	public Map<String, MoneyReceipt> getMoneyReceiptByBill(List<Map<String, Object>> ledgerMapListTemp);

	public List<Map<String, Object>> getConsolidateLedger(Map<String, Object> ledgerReportMap);
	public List<Map<String, Object>> getConsolidateLedgerFinalList(List<Map<String, Object>> ledgerMapListTemp);
}
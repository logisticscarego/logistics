package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.StationarySupplier;

@Repository
public interface StationarySupplierDAO {
	
	//Sanjana: saves stationarySupplier to database
	public int saveStationarySupplier(StationarySupplier stationarySupplier);
	
	//Sanjana: retrieves the list of stationarySupplier codes from database
	public List<StationarySupplier> getStationarySupplierCode();

	//public int updateStationarySupplier1(StationarySupplier stationarySupplier);
	
	//Satarupa: returns the total number of rows in stationary supplier table
	public long totalCount();
		
	//Satarupa: get last row of stationary supplier
	public List<StationarySupplier> getLastStationarySupplier();
		
	/*//Satarupa: get all the stationary supplier codes
	public List<String> getSupplierCodes();*/
		
	//Satarupa: retrieve the details of stationary supplier table on the basis of supplier code
	public List<StationarySupplier> retrieveSupplier(String stSupplierCode);
	
	//Satarupa: retrieve the details of stationary supplier table on the basis of supplier Id
	 public List<StationarySupplier> getOldStSupplier(int stSupID);
	 
	 //Satarupa: updates the stationary supplier table for the suppliers whose details have been checked.
	 public int updateStSupplierIsViewTrue(String code[]);
	 
	 //Satarupa: retrieve the list of all suppliers where isView=no
	 public List<StationarySupplier> getSupplierListForAdmin();
}

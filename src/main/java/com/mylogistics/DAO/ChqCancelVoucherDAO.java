package com.mylogistics.DAO;

import org.springframework.stereotype.Repository;

import com.mylogistics.services.VoucherService;

@Repository
public interface ChqCancelVoucherDAO {

	public int saveChqCancelVoucher(VoucherService voucherService);
	
}

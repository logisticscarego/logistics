package com.mylogistics.DAO;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.TermNCondition;

@Repository
public interface TermNConditionDAO {
	
	//Sanjana: saves termNCondition to database
	 public int saveTnc(TermNCondition termNCondition);
	 
	 //Sanjana: updates TNC
	 public int updateTnC(TermNCondition termNCondition);

}

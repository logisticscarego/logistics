package com.mylogistics.DAO;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.ProfitNLoss;

@Repository
public interface ProfitNLossDAO {

	public int saveProfitNLoss();
	
	public int saveProfitNLossMstr(String userCode, String bCode);
	
}

package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.ProductType;

@Repository
	public interface ProductTypeDAO {
	
	//Sanjana: retrieves the list of productname from product table 
	public List<String> getProductName();

	//Satarupa: saves product type to database
	public int saveProductTypeToDB(ProductType producttype);
	
	//Satarupa: retrieves the complete database of product type table
	public List<ProductType> getProductTypeData();
	
	//Satarupa: updates product type
	public int updateProductType(ProductType producttype);
	
}

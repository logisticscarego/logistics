package com.mylogistics.DAO.temp;

import org.springframework.stereotype.Repository;

import com.mylogistics.services.temp.BillTempService;

@Repository
public interface BillTempDAO {

	public int saveBillTemp(BillTempService billTempService);
	
}

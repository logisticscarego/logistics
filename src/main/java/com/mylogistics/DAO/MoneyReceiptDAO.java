package com.mylogistics.DAO;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.MoneyReceipt;

@Repository
public interface MoneyReceiptDAO {

	public String saveOAMR(MoneyReceipt moneyReceipt);
	
	public String savePDMR(List<MoneyReceipt> mrList);
	
	public String saveDPMR(List<MoneyReceipt> mrList);
	
	public List<MoneyReceipt> getPendingMR(int custId);
	
	public List<MoneyReceipt> getOnAccFrRev(int custId, int brhId);
	
	public Map<String, Object> saveOnAccRev(Map<String,Object> clientMap);
	
	public List<Map<String,Object>> getMrFrPrint(int brhId, String frMrNo, String toMrNo);
	
	public List<Map<String,Object>> getMrrFrPrint(int brhId, Date mrDt);
	
	//public int setPdmrToOnacc();
	
	public int setOnAccRemAmt();
	
	public int duplicateMR(MoneyReceipt moneyReceipt);
	
	public int duplicateDPMR(List<MoneyReceipt> mrList);
	
	public Map<String,Object> cancelOnAcc(int brhId, String mrNo);
	
	public int chkAccuracy();
	
	/**
	 * @author Mohd Furkan
	 * @param userBranchCode
	 * @return Moneyreceipt or null
	 */
	public MoneyReceipt getLastMoenyReceipt(int userBranchCode);
	public void saveOAMRN(MoneyReceipt moneyReceipt);
	
	public Map removeBillFromSrtExs(int custId,String billNo);
}

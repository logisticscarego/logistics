package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.CashStmt;
import com.mylogistics.services.VM_SubVMService;

@Repository
public interface SubVehicleMstrDAO {

	public List<VM_SubVMService> getSubFileDetails(List<CashStmt> reqCsList);
}

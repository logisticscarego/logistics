package com.mylogistics.DAO;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.BankCS;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.model.User;
import com.mylogistics.services.VoucherService;

@Repository
public interface CashStmtStatusDAO {
	
	public CashStmtStatus getLastCssByBranch(String bCode);
	
	public CashStmtStatus getLastCssByBranch(String bCode,Session session);
	
	public CashStmtStatus getLastCssByBranchBack(String bCode,Session session);//for back date data
	
	public CashStmtStatus getLastCssByBranchBack(String bCode);
	
	public Map<String,Object> updateCSSByCS(int cssId , CashStmt cashStmt);
	
	public Map<String,Object> updateCSSForTelV(int cssId , CashStmt cashStmt);
	
	public Map<String,Object> updateCSSForEleV(int cssId , CashStmt cashStmt);
	
	public Map<String,Object> updateCSSByCSWTDS(int cssId , CashStmt cashStmt , CashStmt csWithTds);
	
	public List<CashStmt> getAllCashStmt(int cssId);
	
	public int closeCSS(CashStmtStatus cashStmtStatus);
	
	public int closeCSS(CashStmtStatus cashStmtStatus,Session session);
	
	public List<CashStmtStatus> getCSSById(int id);
	
	public List<CashStmtStatus> getCSSById(int id,Session session);
	
	public Map<String,Object> updateCSSForV(int cssId , CashStmt cashStmt);
	
	public int updateCSSBySFId(int csId ,int csfId);
	
	public int saveCashPayment(VoucherService voucherService);
	
	public int closePenCs(List<Integer> penCsIdList, int cssId );
	
	public int closePenCs(List<Integer> penCsIdList, int cssId, Session session );
	
	public int saveCsTemp(CashStmtTemp cashStmtTemp);
	
	public int saveRentVoucher(VoucherService voucherService);
	
	public Map<String,Object> saveAtmVoucher(VoucherService voucherService);
	
	public CashStmtStatus getCssByDt(String brCode , Date cssDt);
	
	public CashStmtStatus getCssByDt(String brCode , Date cssDt, Session session);
	/////
	public List<CashStmtStatus> getCssByDt(String brCode , Date cssDt, Date toDate);
	
	public int delServTaxCS();
	
	public List<BankCS> getBankCsByDt(Date date, int brhId);
	
	public List<BankCS> getBankCsByDt(Date date, int brhId,Session session);
	
	// Transaction Management Methods
	public CashStmtStatus getCssByCssDtWithoutCs(Session session, Integer userId, String brCode , Date cssDt);
	
	public CashStmtStatus getLastCssByBranch(Session session, Integer userId, String bCode);
	
	public CashStmtStatus getCssByDt(Session session, Integer userId, String brCode , Date cssDt);
	
	public List<BankCS> getBankCsByDt(Session session, Integer userId,  Date date, int brhId);
	
	public List<CashStmtStatus> getCss(Session session, User user, String branchId, Date fromDate, Date toDate);
	
	public List<BankCS> getBankCs(Session session, User user,  Integer branchId, Date date);	
	
	public CashStmtStatus getCashStmtStatus(String branchCode, Date cssDate);
	
	public void merge(CashStmtStatus css);

	public Map<String, Object> updateCSSByCSN(Session session, int cssId,
			CashStmt cashStmt);

	public Map<String, Object> saveRentVoucherN(VoucherService voucherService);

	public int saveCsTemp(Session session, CashStmtTemp cashStmtTemp);

	public int updateCSSBySFId(Session session, int csId, int csfId);

	public Map<String, Object> updateCSSForTelV(Session session, int cssId,
			CashStmt cashStmt);

	public List<BankCS> getBankCsByDt(Session session, Integer userId, Date date,
			ArrayList<Integer> brhId);

	public List<BankCS> getBankCsForGurgaon();
}

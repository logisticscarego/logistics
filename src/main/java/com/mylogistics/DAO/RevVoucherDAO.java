package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.CashStmt;

@Repository
public interface RevVoucherDAO {

	public int revVoucher(List<CashStmt> reqCsList);
	
	public int revTelVoucher(List<CashStmt> reqCsList);
	
	public int revEleVoucher(List<CashStmt> reqCsList);
	
	public int revBPVoucher(List<CashStmt> reqCsList);
	
	public int revVehVoucher(List<CashStmt> reqCsList);
	
	public int revTrvVoucher(List<CashStmt> reqCsList);
}

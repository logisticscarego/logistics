package com.mylogistics.DAO.werservice;

import java.util.Map;

import com.mylogistics.model.webservice.VehicleApp;
import com.mylogistics.services.VehVendorService;

public interface VehicleWebServiceDao {
	
	public Map<String, Object> getOwnsBrksWs();
	public Map<String, Object> vehicleCheck(String rcNo);
	public Map<String, Object> addNewVehicle(VehicleApp vehicleApp);
	public Boolean findAlreadyVehicle(String vRcNo);
	public Map<String, Object> getPendingVehDt(String vvRcNo);
	public Map<String, Object> getPendingVehicle();
	public Map<String, Object> verifyVehicle(VehVendorService vehVendorService);
	
}
package com.mylogistics.DAO.werservice;

import java.sql.Blob;
import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.RateByKm;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.User;
import com.mylogistics.model.app.AppUser;
import com.mylogistics.model.webservice.CnmtApp;
import com.mylogistics.services.InvAndDateService;

public interface WebServiceDAO {

	// For WebService
	public List<Map<String, Object>> getCodeList(String branchCode,String status);
	public List<Map<String, Object>> getConsigneeList();
	public List<Map<String, Object>> getCustomerList();
	public List<Map<String, Object>> getAllEmployeeList();
	public List<Map<String, Object>> getStateList();
	public List<Map<String, Object>> getStationDataList();
	public List<String> getProductTypeList();
	public List<Map<String, Object>> getVehicleTypeList();
	public Map<String, Object> getRegContList(String custCode, Date cnmtDate, String cnmtFromSt);
	public Map<String, Object> getDailyContList(String custCode, Date cnmtDate, String cnmtFromSt);
	public List<RegularContract> getRegContByContCode(String contCode);
	public List<ContToStn> getContToStnRateCnmtW(String contCode, String cnmtToSta, String cnmtVehicleType, Date cnmtDt);
	public List<ContToStn> getContToStnRateCnmtQ(String contCode, String cnmtToSta, String cnmtProductType, Date cnmtDt);
	public List<RegularContract> getRegContractData(String contCode);
	public List<DailyContract> getDailyContractData(String contCode);
	public List<RateByKm> getRbkmRate(String cnmtState, String contCode);
	public double getLastMonthPerDayUsed(Date startDate,Date endDate,String type);	
	public Date getFirstEntryDate(String type);
	public double getNoOfStationary(Date startDate,Date endDate,String type);
	public Map<String, Object> saveCnmtToDB(CnmtApp cnmtApp);
	public List<Map<String, Object>> getBranchList();
	public Map<String, Object> checkAvaCnmtCode(String cnmtCode);
	
	// For Validation
	public Map<String, Object> getCnmtPendingCodeList(String branchCode);
	public Map<String, Object> getPendingCnmtDetail(String cnmtCode, String branchCode);	
	public String updatePendingCnmt(String cnmtCode);
	public void updatePendingCnmtChange(Cnmt cnmt, User user);
	
	public Boolean isCnmtAvailable(String cnmtCode);
	
	public Map<String, Object> getEntryCount(Map<String, Object> initParam);
	
	public Map<String, Object> saveOrUpdateOTP(String phoneNo, String otp);
	public Map<String, Object> verifyOTP(String phoneNo, String otp);
	public Map<String, Object> saveNewAppUser(AppUser appUser);
	
	
}
package com.mylogistics.DAO;

import org.hibernate.Session;

import com.mylogistics.model.BrokerOld;

public interface BrokerOldDAO {

	public int saveBrOld(BrokerOld brokerOld);
	
	public int saveBrOld(BrokerOld brokerOld,Session session);
}

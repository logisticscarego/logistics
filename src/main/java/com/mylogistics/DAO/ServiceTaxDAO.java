package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.ServiceTax;

@Repository
public interface ServiceTaxDAO {

	//Satarupa: saves service tax to database
	public int saveServiceTaxToDB(ServiceTax servicetax);
	
	//Sanjana: retrieves the last row of serviceTax table
	public List<ServiceTax> getLastSrvTaxRow();
	
	public ServiceTax getLastSerTax();
}

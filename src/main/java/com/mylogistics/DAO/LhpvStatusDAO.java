package com.mylogistics.DAO;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.lhpv.LhpvAdv;
import com.mylogistics.model.lhpv.LhpvBal;
import com.mylogistics.model.lhpv.LhpvCashSmry;
import com.mylogistics.model.lhpv.LhpvStatus;
import com.mylogistics.model.lhpv.LhpvSup;
import com.mylogistics.services.VoucherService;

@Repository
public interface LhpvStatusDAO {

	public LhpvStatus getLastLhpvStatus(String bCode);
	
	public LhpvStatus getLastLhpvStatus(String bCode,Session session);
	
	public LhpvStatus getLastLhpvStatusBack(String bCode);
	
	public int saveLhpvAdv(VoucherService voucherService);
	
	public int saveLhpvAdvTemp(VoucherService voucherService);
	
	public int saveLhpvBal(VoucherService voucherService);
	
	public int saveLhpvBal(VoucherService voucherService,Session session);
	
	public int saveLhpvBalTemp(VoucherService voucherService);
	
	public int saveLhpvSup(VoucherService voucherService);
	
	public List<LhpvAdv> getLhpvAdvList(int lsId);
	
	public List<LhpvBal> getLhpvBalList(int lsId);
	
	public List<LhpvSup> getLhpvSupList(int lsId);
	
	public List<LhpvAdv> getLhpvAdv(int lsId);
	
	public List<LhpvBal> getLhpvBal(int lsId);
	
	public List<LhpvSup> getLhpvSup(int lsId);
	
	public List<LhpvAdv> getLhpvAdv(int lsId,Session session);
	
	public List<LhpvBal> getLhpvBal(int lsId,Session session);
	
	public List<LhpvSup> getLhpvSup(int lsId,Session session);
	
	
//	public int closeLhpvIntoCS(LhpvStatus lhpvStatus);
	
	public int closeLhpvIntoCS(LhpvStatus lhpvStatus,Session session);
	
	public int closeLhpvSmry(LhpvStatus lhpvStatus);
	
	public int closeLhpvSmry(LhpvStatus lhpvStatus,Session session);
	
	public List<LhpvCashSmry> getLhpvCash(Date date,String bCode);
	
	public List<LhpvCashSmry> getLhpvCash(Date date,String bCode,Session session);

	
	public LhpvStatus getLssByDt(String bCode, Date date);
	
	public Map<String,Object> chkBackDt();
	
	public Map<String,Object> checkBackDtLhpv(Date date);
	
	public int closeLhpvTemp(Date date, int brhId);
	
	public Map<String,Object> chkTempLhpv(Date date, int brhId);

	public int saveLhpvBalByBrh(VoucherService voucherService,Session session);

	public int saveLhpvBalHo(VoucherService voucherService, Session session);

	public List<LhpvBal> getLhpvBal(Date fromDt, Date toDt);

	public List<LhpvBal> getLhpvBalFrPtro(Date date);
	
	public List<LhpvBal> getLhpvBalFrBank(Date date);

	public int updateLhpvBal(List<LhpvBal> lhpvBals);

	public List<LhpvBal> getLhpvBal(Date date, String bCode);
	
	public List<LhpvBal> getLhpvBal(Date date, String bCode,Session session);

	public List<LhpvBal> setLhpvBal(LhpvStatus ls, List<LhpvBal> lbList);
	
	public int saveLhpvAdv(LhpvAdv la,Session session);

	public List<LhpvAdv> getLhpvAdv(Date date, String bCode);

	public List<LhpvAdv> setLhpvAdv(LhpvStatus ls, List<LhpvAdv> laList);

}

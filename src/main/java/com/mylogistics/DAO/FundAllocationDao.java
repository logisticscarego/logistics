package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import com.mylogistics.model.AllowAdvance;
import com.mylogistics.model.FundAllocation;
import com.mylogistics.model.PetroCard;
import com.mylogistics.model.PetroCardAdv;

public interface FundAllocationDao {
	
	public int saveFundAllocation(FundAllocation fundAllocation);
	
	public List<FundAllocation> getCurrentDateFundAllocationList();
	
	//public Map<String, Object> getCurrentFundAllocationForPay();
	
	public int updateFundAllocation(FundAllocation fundAllocation);
	
	public void setGenerated(List<FundAllocation> fundAllocationList);
	
	public int savePetroAdv(PetroCardAdv adv);
	
	public List<PetroCardAdv> getCurrentDatePetroAllocationList();
	
	public int updatePetroAllocation(PetroCardAdv adv);
	
	public void setPetroGenerated(List<PetroCardAdv> petroAllocationList);
	
	public boolean checkChallanNo(String chlnCode);

	public boolean checkChallanNoInPetro(String chlnCode);
	
	public PetroCard getCardByCardNo(String cardNo);

	public void updatePetroAllocation(PetroCardAdv adv, Session session);

	public List<PetroCardAdv> getPetroAllocationListFrExl();

	public int rejectFundAllocation(FundAllocation fundAllocation);

	public void updateFundAllocation(FundAllocation fa, Session session);

	public List<FundAllocation> getCurrentDateFundAllocationListFrExcel();
	
	public Map<String,String> alwChlnFrPetroAdvanceMT(String chlnCode);
	
	public Map<String,String> alwChlnFrFundAdvanceMT(String chlnCode);
	
	/*public int saveVehclAdvAlwReq(Map<String, String> map);
	
	public AllowAdvance getAlwdVehclFrAdv(Map<String, String> map);*/
	

}

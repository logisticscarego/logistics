package com.mylogistics.DAO.bank;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;

@Repository
public interface ChequeLeavesDAO {

	
	public List<ChequeLeaves> getChqLByChqNo(String chqLChqNo);
	
	public int updateChqLeaves(ChequeLeaves chequeLeaves);
	
	public int saveChqLeaves(ChequeLeaves chequeLeaves);
	
	public List<ChequeLeaves> getUnUsedChqLeave(BankMstr bankMstr);
	
	public int cancelChqLeave(ChequeLeaves chequeLeaves);
	
	public List<ChequeLeaves> getChqLeavesForUpdate(Map<String, Object> chqStatusService);
	
	public int updateChqMaxLtNType(ChequeLeaves chequeLeaves);
	
	public int removeChqLeave(String chqLId);
	
	public List<String> getChqNoListByBankId(int bankId);

	public int updateChqLeavesN(Session session, ChequeLeaves chequeLeaves);

	public int updateChqLeaves(Session session, ChequeLeaves chequeLeaves);
}

package com.mylogistics.DAO.bank;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.Employee;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeBook;
import com.mylogistics.model.bank.ChequeRequest;

@Repository
public interface ChequeBookDAO {

	public String saveChequeRequest(ChequeRequest chequeRequest);
	
	public List<ChequeRequest> getChqReqList(BankMstr bankMstr);
	
	public int receiveChequeBook(List<ChequeBook> chequeBookList, 
			ChequeRequest chequeRequest, BankMstr bankMstr, User user);
	
	public List<ChequeBook> getChqBook(BankMstr bankMstr);
	
	public int issueChequeBook(List<ChequeBook> chequeBookList, Employee authEmp1, Employee authEmp2);
	
	public List<ChequeBook> getIssuedChqBook(BankMstr bankMstr);
	
	public int updateChqBookRecvdBr(List<ChequeBook> chequeBookList, User currentUser);
}

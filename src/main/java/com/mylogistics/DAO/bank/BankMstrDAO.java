package com.mylogistics.DAO.bank;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.Branch;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.BankName;
import com.mylogistics.model.bank.ChequeLeaves;

@Repository
public interface BankMstrDAO {

	public List<String> getBankCodeList(String branchCode);
	
	public List<Map<String,Object>> getBnkNameAndCode(String branchCode);
	
	public ChequeLeaves getChqListByBnkC(String bankCode,char CType);
	
	public List<ChequeLeaves> getChqLByBnk(String bankCode,char CType);
	
	public List<ChequeLeaves> getAPChqLByBnkC(String bankCode,char CType);
	
	public List<ChequeLeaves> getAPChqLByBnkC(String bankCode,char CType, String chqNo);
	
	public ChequeLeaves getAccPayChqByBnkC(String bankCode,char CType);
	
	public List<BankMstr> getBankByBankCode(String bankCode);
	
	public ChequeLeaves getChqListByBnkC(String bankCode);
	
	public List<BankName> getBankNameList();
	
	public String saveBankMstr(BankMstr bankMstr);
	
	public int saveBankMstr(BankMstr bankMstr, Branch branch);
	
	public List<BankMstr> getUnAssignBank();
	
	public List<BankMstr> getBankMstr();
	
	public int dissBMFromBranch(BankMstr bankMstr, Branch branch);
	
	public List<BankMstr> getAssignedBank();
	
	public int dissBankMstr(BankMstr bankMstr);
	
	public int decBankBal(int bnkId, double amt);
	
	public int incBnkBal(int bnkId, double amt);
	
	public BankMstr getBankByAtm(String atmCard);
	
	public String getBankName(String bankFaCode);
	
	public String getBankName(String bankFaCode,Session session);
	
	public List<ChequeLeaves> getChqLByBnkId(int bnkId);
	
	public List<Map<String, Object>> getBankNCAIList(String branchId);

	ChequeLeaves getChqListByBnkCRvrs(String bankCode, char CType);
	
	public BankMstr getBankMstr(int bankMstrId);
	public void update(BankMstr bankMstr);

	List<BankMstr> getBankByBankCodeN(Session session, String bankCode);

	public List<BankMstr> getBankByBankCode(Session session, String bankCode);
	
	public int decBankBal(Session session, int bnkId, double amt);
//	public List<ChequeLeaves> getChqLByBnkRvrs(String bankCode,char CType);
	/*public int assignChildToBank();*/
	
}
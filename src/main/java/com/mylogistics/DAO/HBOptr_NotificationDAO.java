package com.mylogistics.DAO;

import java.sql.Date;
import java.util.List;

import com.mylogistics.model.HBOptr_Notification;

public interface HBOptr_NotificationDAO {

	public int saveHBON(HBOptr_Notification hBON);
	
	public List<HBOptr_Notification> getHBOptr_NotificationData(String operatorCode);
	
	public List<HBOptr_Notification> getTotalHBON_Data(String operatorCode);
	
	public List<HBOptr_Notification> getHBOptr_NotificationDataWithDate(String userCode,Date date);
	
	public int updateHBON_Data(String dispatchCode);
	
	public int updateIsViewYes(int id);
}

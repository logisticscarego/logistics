package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.User;

@Repository
public interface UserDAO {

	//save
	public void saveUser(User user);
	//retrieve
	public List<User> retrieveUser(String userCode);
	
	public List<User> getUserByUN(String userName , String Password);
	//update
	//public User updateUserByUserCode(String userCode , String userName);
	//add
	public String addNewUser(User user);
	//getAllUser
	public List<User> getAllUser();
	//updateUserList
	public String updateUserList(List<User> userList);
	//update User
	public String updateUser(User user);
	
	public List<User> getHBOperator(String branchCode);
	
	public List<User> getSubBranchOperator(String branchCode);
	
	//Satarupa:update user by employee code and branch code
	public int updateUser(String employeeCode,String branchCode);
	
	public int setUserCode(int userId, int empId);
	//change user password 
	public int changePass(User user);
}

package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.services.JourVouchDRCR;
import com.mylogistics.services.VoucherService;

@Repository
public interface JVoucherDAO {

	public Map<String,Object> saveJVoucher(VoucherService voucherService , List<JourVouchDRCR> jvList);
	public Map<String,Object> saveJVoucherN(VoucherService voucherService , List<JourVouchDRCR> jvList);
}

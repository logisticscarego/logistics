package com.mylogistics.DAO;
import java.sql.Blob;
import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.Cnmt;
import com.mylogistics.model.CnmtImage;
import com.mylogistics.model.Narration;
import com.mylogistics.model.User;
import com.mylogistics.services.InvAndDateService;

@Repository
public interface CnmtDAO {  
	
	//Shikha: retrieves contract code from daily and regular contract using customer code
	public Map<String,Object> getContractCode(String custCode);
	
	//Shikha: saves cnmt to database
	//public int saveCnmtToDB(Cnmt cnmt);
	public int saveCnmtToDB(Cnmt cnmt, String serialNo, String brsLeafDetStatus, double daysLeft, double average, Blob blob, Blob confirmBlob, List<InvAndDateService> invList);

	//Shikha: fetches a single cnmt row using cnmt code 
	public List<Cnmt> getCnmt(String cnmtCode);
	
	public List<Cnmt> getCnmt(String cnmtCode,Session session);
	
	//Shikha: fetches cnmt code,date and contract
	public List<Cnmt> getCnmtCodeDtCon();
	
	//Shikha: fetches state lorry prefix
	public List<String> getStateLryPrefix();
	
	//Shikha: updates cnmt
	public int updateCnmt(Cnmt cnmt);
	
	public int updateCnmt(Cnmt cnmt,Session session);
	
	public int updateCnmts(Cnmt cnmt);
	
	//Shikha: retrieves multiple rows of cnmt code
	public List<Cnmt> getAllCnmtByCode(List<String> cnmtCodeList);
	
	public List<Cnmt> getAllCnmtByCode(List<String> cnmtCodeList,Session session);
	
	
	//Sanjana: retrieves the list of cnmt whose isView is false
	public List<Cnmt> getCnmtisViewFalse(String branchCode);
	
	//Sanjana: updates the cnmt isView to true
	public int updateCnmtisViewTrue(String code[]);
	
	//Sanjana: retrieves cnmt from database on the basis of entered cnmt code
	public List<Cnmt> getCnmtList(String cnmtCode);

	//Shikha: get cnmt image by using cnmt code
	public Blob getCnmtImageByCode(String cnmtCode);
	
	public Blob getCnmtConfImageByCode(String cnmtCode);
	
	public List<String> getCnmtCode(String cnmtCode);
	
	public List<Cnmt> getAllBiltyCodes(String branchCode);
	
	public List<Cnmt> getCnmtById(int cnmtId);
	
	public List<Cnmt> getCnmtByCustCode(String custCode);
	
	public List<Cnmt> getCnmtByContCode(String contCode);
	
	public List<Cnmt> getAllCnmt();
	
	public int addCnmtImg(int cnmtId , Blob blob, String cnmtImgPath);
	
	public int addCnmtImg(int cnmtId , Blob blob, String cnmtImgPath,Session session);
	
	public CnmtImage getCnmtImg(int cnmtId);
	
	public int updateCnImg(CnmtImage cnmtImage, Blob blob);
	
	public int saveCnImg(CnmtImage cnmtImage, Blob blob);
	
	public List<String> getAllCnCode(String brhCode); 
	
	//public List<Cnmt> getAllCnCodeByCnmt(String brhCode, List<String> cnmtCodeList);
	public List<Map<String, Object>> getAllCnCodeByCnmt(String brhCode, List<String> cnmtCodeList);
	
	public List<String> getTransCnmt();
	
	//public List<Cnmt> getTransCnmtByCnmt(List<String> cnmtList);
	public List<Map<String, Object>> getTransCnmtByCnmt(List<String> cnmtList);
	
	public int updateCnWS(Map<String,Object> map);
	
	public int updateCnWS(Map<String,Object> map,Session session);
	
	public List<String> getCnmtFrNBill(String custCode, Date frDt, Date toDt);
	
	public List<String> getCnmtFrSBill(String custCode, Date frDt, Date toDt);
	
	public List<String> getCnmtFrNBillR(String custCode, Date frDt, Date toDt);
	
	public List<String> getCnmtFrSBillR(String custCode, Date frDt, Date toDt);
	
	
	public Blob getCnmtImage(int cnmtImgId);
	
	public List<Map<String, Object>> getChlnNSedrByCnmt(String cnmtCode);
	
	public Map<String, Object> saveEditCnmt(Cnmt cnmt, Blob cnmtImgBlob, String cnmtImgPath);
	
	public Map<String, Object> getCnmtNCnmtImg(String cnmtCode);
	
	public List<String> getCnmtNoByBill(String blNo);
	
	public Map<String, Object> getUsrBrCnmtCust();
	
	public int saveCnmtBbl(Map<String, Object> cnmtBblService);
	
	public Object getBblCnmtCode(String  cnmtBblCode);

	public int cnmtDtlFrCncl(Map<String, String> cnmtDtl);

	public List<Map<String, Object>> getCostingReport(Map<String, Object> map);
	
	public List<Map<String, Object>> getHoCostingReport(Session session, User user, Map<String, Object> map);

	public int cnmtDtlFrCstng(Map<String, Object> cnmtDtl);
	
	public List getCnmtFrmToCwise(Map<String, Object> cnmtDtl);
	
	public List<Map<String, Object>> getCnmtByChlnCodeForMemo(Session session, User user, String chlnCode);
	
	public Boolean isCodeValid(Session session, User user, Narration narr);
	
	public Boolean isAlreadyNarrtion(Session session, User user, Narration narr);
	
	public Integer saveNarration(Session session, User user, Narration narr);
	
	public List<String> getCnmtByCustCodes(Session session, User user, List<String> custCodes, String billType);

	public Map<String, String> getCustomerMap(List<String> customerCodes);	
	
	public Map<String, String> getStationMap(List<String> stationCodes);

	public List<Map<String, Object>> getRelCnData(Map<String, Object> clientMap);

	public int addCnmtImgN(int cnmtId,int chlnId, Blob blob, String imagePath);
	
	public Cnmt getCnmtFrFndAlctn(String cnmtCode);
	
}

package com.mylogistics.DAO;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.AssestsNLiab;

@Repository
public interface AssestsNLiabDAO {

	public int saveAssestsNLiab();
	
	public int saveAssestsNLiabFaMstr(String userCode, String bCode);
	
	public int getTest();
	
}

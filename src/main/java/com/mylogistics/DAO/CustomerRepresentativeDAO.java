package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.Customer;
import com.mylogistics.model.CustomerRepresentative;

@Repository

public interface CustomerRepresentativeDAO {
	
	//Shikha: retrieves customer representative
	public List<CustomerRepresentative> getCustomerRep(int custRepId);
	
	//Shikha: retrieves last customer representative
	public int getLastCustomerRep();
	
	//Shikha: saves customer representative to database
	public int saveCustomerRepToDB(CustomerRepresentative custrep);
	
	//Shikha: calculate total count
	public long totalCount();
	
	public List<String> getCustomerRepCode();

    public List<Object[]> getCustomerRepresentativeCodeNameDesig();
	
    //Sanjana: get crList for daily & regular Contract on basis of entered custCode
	public List<Object[]> getCRCodeNameDesigCustName(String custCode);

	public List<CustomerRepresentative> getCustomerRepByCustCode(String custCode);
	
	public List<CustomerRepresentative> getCustRepIsViewNo(String custCode);
	
	public List<String> getAllCustRepCode(String custCode);
	
	public int updateCustRepIsView(String[] code);
	
	public  CustomerRepresentative getCustRepByCode(String custRepCode);
	
	public int updateCustomerRepByCrCode(CustomerRepresentative custrep);
	
	public int updateCustomerRepById(CustomerRepresentative custrep);
	
	public List<CustomerRepresentative> getAllCustRepByBrCode(String brCode);
}

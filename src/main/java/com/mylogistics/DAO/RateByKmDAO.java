package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.ContToStn;
import com.mylogistics.model.RateByKm;

@Repository
public interface RateByKmDAO {
	
	//Sanjana: saves rateByKm to database
	public int saveRBKM(RateByKm rateByKm);

	//Sanjana: gets ratebykm from database on basis of entered contract code
	public List<RateByKm> getRateByKm(String code);

	//Sanjana :delete RateByKm from database
	public RateByKm deleteRBKM(String code);
	
	public List<RateByKm> getRbkmRate(String stateCode , double cnmtKm , String contCode);
	
	public List<RateByKm> getRegContForView(String stnCode, String contCode);
	
	public List<RateByKm> getRbkmForChln(String contractCode , String fromStation , String toStation , String stateCode);
	
	/*public List<RateByKm> getContForView(String contCode , String stnCode);*/
}

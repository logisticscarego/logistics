package com.mylogistics.DAO;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.EmployeeOld;

@Repository
public interface EmployeeOldDAO {

	//Satarupa: saves row of employee table to employeeOld table that is to be modified.
	public int saveEmployeeOldToDB(EmployeeOld employeeOld);
	
	//Satarupa: retrieves the details of employeeOld table on the basis of branchCode 
	 public List<EmployeeOld> getModifiedEmp(String empCode);
}

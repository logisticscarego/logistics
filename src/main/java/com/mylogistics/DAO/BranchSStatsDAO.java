package com.mylogistics.DAO;

//import java.util.Date;
import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.BranchSStats;
import com.mylogistics.model.User;

@Repository
public interface BranchSStatsDAO {
	
	public int saveBranchSStats(BranchSStats branchSStats);
	
	public int saveBranchSStats(BranchSStats branchSStats,Session session);
	
	/*public double getLastMonthPerDayUsedCnmt(Date startDate,Date endDate);
	
	public double getLastMonthPerDayUsedChln(Date startDate,Date endDate);
	
	public double getLastMonthPerDayUsedSedr(Date startDate,Date endDate);*/
	
	public double getLastMonthPerDayUsed(Date startDate,Date endDate,String type);
	
	public double getLastMonthPerDayUsed(Date startDate,Date endDate,String type,Session session);
	
	//public int getLastMnthDataOfCnmt(Date startDate,Date endDate);
	
	/*public Date getFirstEntryDateOfCnmt();
	
	public Date getFirstEntryDateOfChln();
	
	public Date getFirstEntryDateOfSedr();*/
	
	public Date getFirstEntryDate(String type);
	
	public Date getFirstEntryDate(String type,Session session);
	
	public double getNoOfStationary(Date startDate,Date endDate,String type);
	
	public double getNoOfStationary(Date startDate,Date endDate,String type,Session session);
	
	public List<String> getBranchSStats(Session session, User user, String branchCode, Long min, Long max, String stnType, String custSta);
	
	public List<Map<String, String>> getMinMax(Session session, User user, String branchCode, String fromDate, String toDate, String stnType, String custSta);
	
//	public double getNoOfChln(Date startDate,Date endDate);
//	
//	public double getNoOfSedr(Date startDate,Date endDate);
}

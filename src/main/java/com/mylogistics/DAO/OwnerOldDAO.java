package com.mylogistics.DAO;

import org.hibernate.Session;

import com.mylogistics.model.OwnerOld;

public interface OwnerOldDAO {

	public int saveOwnerOld(OwnerOld ownerOld);
	
	public int saveOwnerOld(OwnerOld ownerOld,Session session);
}

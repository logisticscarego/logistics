package com.mylogistics.DAO;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.HOSLast;

@Repository
public interface HOSLastDAO {
	
	//Satarupa: gets the last row of HOSLast table
	 public List<HOSLast> getLastArrival();
	 
	//Satarupa: saves HOSLast to database.
	 public int saveHosLastToDB(HOSLast hosLast);
	 
	 public Date getFirstEntryDate();
	 	 
	/* public double getNoOfCnmt(Date startDate,Date endDate);*/
	 
	 public Map<String,Object> getNoOfStationary(Date startDate,Date endDate);
	 
	/* //Satarupa: No. of challans between two dates.
	 public double getNoOfChln(Date startDate,Date endDate);
	 
	 //Satarupa: No. of sedrs between two dates.
	 public double getNoOfSedr(Date startDate,Date endDate);*/
	 
	 public int saveHosLast(HOSLast hosLast);
	 
}

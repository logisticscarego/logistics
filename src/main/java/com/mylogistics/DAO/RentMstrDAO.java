package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.RentDetail;
import com.mylogistics.model.RentMstr;
import com.mylogistics.model.User;
import com.mylogistics.services.RentService;

@Repository
public interface RentMstrDAO {

	public int saveRentMstr(RentService rentService, User currentUser);
	
	public List<Map<String,Object>> getRentMstrInfo();
	
	public List<Map<String,Object>> getRentMstrForN();
	
	public List<RentMstr>  getAllRentMstrByBranchId(int branchId);
	
	public boolean updateRentMstr(RentMstr mstr);

	public int saveRentDetail(RentDetail rentDetail);
	
	public List<RentDetail> getRentDetailFrExl();
	
	public List<RentMstr> getRentMstr();

	public int updateRentDetail(List<RentDetail> rentDetaillist);
	
}

package com.mylogistics.DAO;

import java.util.List;

import org.hibernate.Session;

import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.User;

public interface BranchStockLeafDetDAO {
	
	public int saveBranchStockLeafDet(BranchStockLeafDet branchStockLeafDet);
	
	public List<BranchStockLeafDet> getBranchStockLeafDetData(String branchCode);
	
	public BranchStockLeafDet deleteBSLD(String serialNo,String brsLeafDetStatus);
	
	public BranchStockLeafDet deleteBSLD(String serialNo,String brsLeafDetStatus,Session session);
	
/*	public List<String> getCnmtCodeList(String branhCode);
	
	public List<String> getARCodeList(String branchCode);*/
	
	public List<BranchStockLeafDet> getCodeList(String branhCode,String status);
	
	public int getLeftStationary(String branchCode,String type);
	
	public int getLeftStationary(String branchCode,String type,Session session);
	
	public int updateCnmt(String brhCode , String serialNo , boolean allow);
	
	public List<String> getAvlChln(String brhCode);
	public List<String> getAvlChln(String brhCode, String chlnCode);
	/*public int getLeftChlns(String branchCode);
	
	public int getLeftSedrs(String branchCode);*/
	
	
	// With Session Management
	
	
	public List<BranchStockLeafDet> getCodeList(Session session, User user, String cnmtCode, String status);

	public BranchStockLeafDet getCodeByCode(String code, String status);

	public BranchStockLeafDet getCodeByCodeFrLa(String code, String status);
}

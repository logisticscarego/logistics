package com.mylogistics.DAO;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.Address;

@Repository
public interface AddressDAO {

	//Sanjana: saves the address to database
	public int saveAddress(Address address);
	
	public List<Address> getAddress(String code,String type);
	
	 public int updateAddress(Address address);
	 
	 public List<Address> getAddressData(String custCode);
	 
	 public List<Address> getAddressDataById(int id);
	 
	 public List<Address> getRTOAddress();
	 
	 public List<Address> getAddByType(String addType);

	public int saveAddress(Address address, Session session);
	
	public void updateAddress(Address address, Session session);
	
	public List<Address> getAddByTypeRef(Session session,String addType, String refCode);

}

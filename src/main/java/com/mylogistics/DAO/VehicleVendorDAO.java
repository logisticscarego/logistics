package com.mylogistics.DAO;

import java.sql.Blob;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.TransferVehicle;
import com.mylogistics.model.VehicleVendorMstr;
import com.mylogistics.services.VehVendorService;

@Repository
public interface VehicleVendorDAO {

	//this function find out that at a given owner and broker has scan Pan image and validate that given vehicle no not exist for the current branch 
	public int validateVehicle(VehVendorService vehVendorService);
	  
	public int saveVehVenMstr(VehVendorService vehVendorService);
	
	public List<VehicleVendorMstr> getVehicleVendorList();
	
	public VehicleVendorMstr getVehVendor(String vvId);
	
	public VehicleVendorMstr getVehVendorByRc(String rcNo);
	
	public int updateVehVenMstr(VehVendorService vehVendorService);
	
	public List<String> getAllVehNo();
	
	public List<String> getAllVehNoLike(String vehNo);
	
	public List<String> getAllVehNoByLryNo(String lryNo);
	
	public List<String> getAllVehNoForBbl();
	
	public VehicleVendorMstr getVehByVehNo(String vehNo);
	
	public List<Map<String, String>> getRcOwnBrkList();
	
	public Map<String, Object> getOwnBrkFrVehicle(String rcNo);
	
	public int saveVehVenMstr(Session session,VehicleVendorMstr mstr,int ownId,Blob rcImg,Blob policyImg,Blob psImg);
	
	public Map<String,String> checkVehicleExist(Map<String,String> map);
	
	public boolean checkVehicleExist(String vehilceNo,Session session);

	public void updateVehVenMstr(Session session, VehicleVendorMstr vehicleVendorMstr, int ownId, Blob rcImg, Blob policyImg,
			Blob psImg);

	public VehicleVendorMstr getVehicleByRc(String rcNo, Session session);
	
	public int saveTransferVehicle(Session session,TransferVehicle tv);
}

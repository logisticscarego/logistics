package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.LhpvTemp;
import com.mylogistics.services.VoucherService;

@Repository
public interface LhpvTempDAO {

	public int saveLhpvTemp(VoucherService voucherService);
	
	public List<LhpvTemp> getLhpvList(String bCode);
 }

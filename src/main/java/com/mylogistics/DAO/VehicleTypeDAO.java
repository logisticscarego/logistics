package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.VehicleType;

@Repository
public interface VehicleTypeDAO {
	
	//Satarupa: saves vehicle type to database
	public int saveVehicleTypeToDB(VehicleType vehicletype);
	
	//Satarupa: retrieves the complete database of vehicle type table
	public List<VehicleType> getVehicleType();
	
	public List<String> getHypo();
	
	public List<VehicleType> getVehicleTypeByVNameCode(String vNameCode);
	
	public List<VehicleType> getVehicleTypeByName(String vhType);
	
	//Satarupa: updates vehicle type 
	public int updateVehicleType(VehicleType vehicleType);
	
	public List<Map<String, String>> getVehTypeList();
	
	public String getVehType(String vtCode);

}

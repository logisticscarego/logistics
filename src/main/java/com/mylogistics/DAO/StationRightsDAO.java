package com.mylogistics.DAO;

import java.util.List;

import com.mylogistics.model.StationRights;

public interface StationRightsDAO {

	public int saveStRights(StationRights stationRights);
	
	//Satarupa: get the list of all stationrights where isAllow=yes.
	public List<StationRights> getStationRightsIsAllow();
	
	//Satarupa: updating the isAllow  of stationrights table field to "no".
	public int updateStnRightsIsAllowNo(int contIdsInt[]);
}

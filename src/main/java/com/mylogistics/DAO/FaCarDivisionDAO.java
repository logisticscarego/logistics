package com.mylogistics.DAO;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.FaCarDivision;

@Repository
public interface FaCarDivisionDAO {

	public int saveFaCarD(FaCarDivision faCarDivision);
}

package com.mylogistics.config;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

@WebListener
public class ServletContextListenerImpl implements ServletContextListener {

	public static final Logger logger = Logger.getLogger(ServletContextListenerImpl.class);
	
	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		logger.info("ServletContext is destroyed......");
	}

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		logger.info("ServletContext is initialized......");
		ServletContext 		servletContext 	= 	servletContextEvent.getServletContext();
		Map<String, Long> 	filesMap 		= 	new HashMap<>();
		
		String 	path 					= servletContext.getRealPath("/resources/js/controllers");
		File 	jsControllerDirective 	= new File(path);
		
		if(jsControllerDirective.isDirectory()) {
			File []files = jsControllerDirective.listFiles();
			logger.info("AngularCntlr size = "+files.length);
			for (File file : files) {
				filesMap.put(file.getName(), file.lastModified());
			}
		}
		servletContext.setAttribute("filesMap", filesMap);
	}

}
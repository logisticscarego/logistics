package com.mylogistics.services;

import java.sql.Date;

public class NotificationDate {
	
	Date date;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
}

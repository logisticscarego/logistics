package com.mylogistics.services;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.LhpvTemp;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.model.lhpv.LhpvAdv;
import com.mylogistics.model.lhpv.LhpvBal;
import com.mylogistics.model.lhpv.LhpvStatus;
import com.mylogistics.model.lhpv.LhpvSup;

public class VoucherService {

	// For CashWithdrawal Voucher Type
	private Date dateTemp;
	
	private String voucherType;
	
	private Branch branch;
	
	private CashStmtStatus cashStmtStatus;
	
	private LhpvStatus lhpvStatus;
	
	private LhpvAdv lhpvAdv;
	
	private LhpvBal lhpvBal;
		
	private String crNo;
	
	private ChequeLeaves chequeLeaves;
	
	private LhpvTemp lhpvTemp;
	
	private char chequeType;
	
	private char payBy;
	
	private String payToStf;
	
	private String payToPetro;
	
	private String cardCode;
	
	private String faCode;

	private double amount;
	
	private String desc;
	
	private String bankCode;
	
	private String atmCode;
	
	private String payTo;
	
	private int vhNo;
	
	private String tvNo;
	
	private String mrNo;
	
	private String tdsCode;
	
	private String brkOwnFaCode;
	
	private String stafCode;
	
	private String actBankName;
	
	private String actIfscCode;
	
	private String actAccountNo;
	
	private String actPayeeName;
	
	private String loryCode;
	
	private double tdsAmt;
	
	private Date currentDate;
	
	private String acntHldr;
	
	private List<Integer> penCsId = new ArrayList();
	
	private List<Map<String,Object>> subFList = new ArrayList<>();
	
	private List<LhpvAdv> lhpvAdvList = new ArrayList<>();
	
	private List<LhpvBal> lhpvBalList = new ArrayList<>();
	
	private List<LhpvSup> lhpvSupList = new ArrayList<>();
	
	private ChequeLeaves usedChq;
	
	private ChequeLeaves unUsedChq;
	
	
	/*private int sheetNo;*/

	public String getFaCode() {
		return faCode;
	}

	public char getPayBy() {
		return payBy;
	}

	public void setPayBy(char payBy) {
		this.payBy = payBy;
	}

	public String getTdsCode() {
		return tdsCode;
	}

	public void setTdsCode(String tdsCode) {
		this.tdsCode = tdsCode;
	}

	public double getTdsAmt() {
		return tdsAmt;
	}

	public void setTdsAmt(double tdsAmt) {
		this.tdsAmt = tdsAmt;
	}

	public String getMrNo() {
		return mrNo;
	}

	public void setMrNo(String mrNo) {
		this.mrNo = mrNo;
	}

	public void setFaCode(String faCode) {
		this.faCode = faCode;
	}
	
	public String getVoucherType() {
		return voucherType;
	}

	public int getVhNo() {
		return vhNo;
	}

	public void setVhNo(int vhNo) {
		this.vhNo = vhNo;
	}

	public String getTvNo() {
		return tvNo;
	}

	public void setTvNo(String tvNo) {
		this.tvNo = tvNo;
	}

	public String getPayTo() {
		return payTo;
	}

	public void setPayTo(String payTo) {
		this.payTo = payTo;
	}

	public ChequeLeaves getChequeLeaves() {
		return chequeLeaves;
	}

	public void setChequeLeaves(ChequeLeaves chequeLeaves) {
		this.chequeLeaves = chequeLeaves;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public CashStmtStatus getCashStmtStatus() {
		return cashStmtStatus;
	}

	public void setCashStmtStatus(CashStmtStatus cashStmtStatus) {
		this.cashStmtStatus = cashStmtStatus;
	}

	public char getChequeType() {
		return chequeType;
	}

	public void setChequeType(char chequeType) {
		this.chequeType = chequeType;
	}

	/*public String getChequeNo() {
		return chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}*/

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public LhpvTemp getLhpvTemp() {
		return lhpvTemp;
	}

	public void setLhpvTemp(LhpvTemp lhpvTemp) {
		this.lhpvTemp = lhpvTemp;
	}

	public List<Map<String, Object>> getSubFList() {
		return subFList;
	}

	public void setSubFList(List<Map<String, Object>> subFList) {
		this.subFList = subFList;
	}

	public List<Integer> getPenCsId() {
		return penCsId;
	}

	public void setPenCsId(List<Integer> penCsId) {
		this.penCsId = penCsId;
	}

	public String getAtmCode() {
		return atmCode;
	}

	public void setAtmCode(String atmCode) {
		this.atmCode = atmCode;
	}

	public LhpvStatus getLhpvStatus() {
		return lhpvStatus;
	}

	public void setLhpvStatus(LhpvStatus lhpvStatus) {
		this.lhpvStatus = lhpvStatus;
	}

	public LhpvAdv getLhpvAdv() {
		return lhpvAdv;
	}

	public void setLhpvAdv(LhpvAdv lhpvAdv) {
		this.lhpvAdv = lhpvAdv;
	}

	public LhpvBal getLhpvBal() {
		return lhpvBal;
	}

	public void setLhpvBal(LhpvBal lhpvBal) {
		this.lhpvBal = lhpvBal;
	}

	public String getBrkOwnFaCode() {
		return brkOwnFaCode;
	}

	public void setBrkOwnFaCode(String brkOwnFaCode) {
		this.brkOwnFaCode = brkOwnFaCode;
	}

	public List<LhpvAdv> getLhpvAdvList() {
		return lhpvAdvList;
	}

	public void setLhpvAdvList(List<LhpvAdv> lhpvAdvList) {
		this.lhpvAdvList = lhpvAdvList;
	}

	public List<LhpvBal> getLhpvBalList() {
		return lhpvBalList;
	}

	public void setLhpvBalList(List<LhpvBal> lhpvBalList) {
		this.lhpvBalList = lhpvBalList;
	}

	public List<LhpvSup> getLhpvSupList() {
		return lhpvSupList;
	}

	public void setLhpvSupList(List<LhpvSup> lhpvSupList) {
		this.lhpvSupList = lhpvSupList;
	}

	public ChequeLeaves getUsedChq() {
		return usedChq;
	}

	public void setUsedChq(ChequeLeaves usedChq) {
		this.usedChq = usedChq;
	}

	public ChequeLeaves getUnUsedChq() {
		return unUsedChq;
	}

	public void setUnUsedChq(ChequeLeaves unUsedChq) {
		this.unUsedChq = unUsedChq;
	}

	public Date getDateTemp() {
		return dateTemp;
	}

	public void setDateTemp(Date dateTemp) {
		this.dateTemp = dateTemp;
	}

	public String getCrNo() {
		return crNo;
	}

	public void setCrNo(String crNo) {
		this.crNo = crNo;
	}

	public String getPayToStf() {
		return payToStf;
	}

	public void setPayToStf(String payToStf) {
		this.payToStf = payToStf;
	}

	public String getStafCode() {
		return stafCode;
	}

	public void setStafCode(String stafCode) {
		this.stafCode = stafCode;
	}

	public String getActBankName() {
		return actBankName;
	}

	public void setActBankName(String actBankName) {
		this.actBankName = actBankName;
	}

	public String getActIfscCode() {
		return actIfscCode;
	}

	public void setActIfscCode(String actIfscCode) {
		this.actIfscCode = actIfscCode;
	}

	public String getActAccountNo() {
		return actAccountNo;
	}

	public void setActAccountNo(String actAccountNo) {
		this.actAccountNo = actAccountNo;
	}

	public String getActPayeeName() {
		return actPayeeName;
	}

	public void setActPayeeName(String actPayeeName) {
		this.actPayeeName = actPayeeName;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public String getLoryCode() {
		return loryCode;
	}

	public void setLoryCode(String loryCode) {
		this.loryCode = loryCode;
	}

	public String getPayToPetro() {
		return payToPetro;
	}

	public void setPayToPetro(String payToPetro) {
		this.payToPetro = payToPetro;
	}

	public String getCardCode() {
		return cardCode;
	}

	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

	public String getAcntHldr() {
		return acntHldr;
	}

	public void setAcntHldr(String acntHldr) {
		this.acntHldr = acntHldr;
	}

	
}
package com.mylogistics.services;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class StnTransferService {

	@NotNull(message = "Branch code can not be bull !", groups=DoTransfer.class)
	public Integer branchCode;
	@NotNull(message = "Stationary type can not bull !", groups={DoTransfer.class, IsAvailable.class})
	@NotEmpty(message = "Stationary type can not be empty !", groups={DoTransfer.class, IsAvailable.class})
	public String transferType;
	@NotNull(message = "Start no can not be null !", groups={DoTransfer.class, IsAvailable.class})
	@Size(min = 1, message = "Start no can not blank",  groups={DoTransfer.class, IsAvailable.class})
	public List<Long> startList;
	
	public Integer getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(Integer branchCode) {
		this.branchCode = branchCode;
	}
	public String getTransferType() {
		return transferType;
	}
	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}
	public List<Long> getStartList() {
		return startList;
	}
	public void setStartList(List<Long> startList) {
		this.startList = startList;
	}
	
	public interface IsAvailable{		
	}
	public interface DoTransfer{		
	}	
	
	
}
package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.mylogistics.model.Challan;
import com.mylogistics.model.ChallanDetail;

public class Cnmt_ChallanService {

	private Challan challan;
	private List<Map<String,Object>> cnmtCodeList;
	private ChallanDetail challanDetail;
	private List<Map<String, Object>> otherChgList = new ArrayList<Map<String, Object>>();
	
	private int chlnId;
	private List<Integer> cnmtAddIdList;
	private List<Integer> cnmtRemoveIdList;
	
	public Challan getChallan() {
		return challan;
	}
	public void setChallan(Challan challan) {
		this.challan = challan;
	}
	/*public List<String> getCnmtCodeList() {
		return cnmtCodeList;
	}
	public void setCnmtCodeList(List<String> cnmtCodeList) {
		this.cnmtCodeList = cnmtCodeList;
	}*/
	
	public ChallanDetail getChallanDetail() {
		return challanDetail;
	}
	public List<Map<String, Object>> getCnmtCodeList() {
		return cnmtCodeList;
	}
	public void setCnmtCodeList(List<Map<String, Object>> cnmtCodeList) {
		this.cnmtCodeList = cnmtCodeList;
	}
	public void setChallanDetail(ChallanDetail challanDetail) {
		this.challanDetail = challanDetail;
	}
	public int getChlnId() {
		return chlnId;
	}
	public void setChlnId(int chlnId) {
		this.chlnId = chlnId;
	}
	public List<Integer> getCnmtAddIdList() {
		return cnmtAddIdList;
	}
	public void setCnmtAddIdList(List<Integer> cnmtAddIdList) {
		this.cnmtAddIdList = cnmtAddIdList;
	}
	public List<Integer> getCnmtRemoveIdList() {
		return cnmtRemoveIdList;
	}
	public void setCnmtRemoveIdList(List<Integer> cnmtRemoveIdList) {
		this.cnmtRemoveIdList = cnmtRemoveIdList;
	}
	public List<Map<String, Object>> getOtherChgList() {
		return otherChgList;
	}
	public void setOtherChgList(List<Map<String, Object>> otherChgList) {
		this.otherChgList = otherChgList;
	}
	
}

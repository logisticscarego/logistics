package com.mylogistics.services.bank;

import java.util.List;

import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeBook;
import com.mylogistics.model.bank.ChequeRequest;

public class ChequeService {
	
	private Branch branch;
	
	private BankMstr bankMstr;
	
	private ChequeRequest chqReq;
	
	private List<ChequeBook> chequeBookList;
	
	private Employee authEmp1;
	
	private Employee authEmp2;
	
	public List<ChequeBook> getChequeBookList() {
		return chequeBookList;
	}

	public void setChequeBookList(List<ChequeBook> chequeBookList) {
		this.chequeBookList = chequeBookList;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public BankMstr getBankMstr() {
		return bankMstr;
	}

	public void setBankMstr(BankMstr bankMstr) {
		this.bankMstr = bankMstr;
	}

	public ChequeRequest getChqReq() {
		return chqReq;
	}

	public void setChqReq(ChequeRequest chqReq) {
		this.chqReq = chqReq;
	}

	public Employee getAuthEmp1() {
		return authEmp1;
	}

	public void setAuthEmp1(Employee authEmp1) {
		this.authEmp1 = authEmp1;
	}

	public Employee getAuthEmp2() {
		return authEmp2;
	}

	public void setAuthEmp2(Employee authEmp2) {
		this.authEmp2 = authEmp2;
	}
	
	

}

package com.mylogistics.services.bank;

import com.mylogistics.model.bank.AtmCardMstr;

public class AtmCardService {

	private AtmCardMstr atmCardMstr;
	
	private int bankId;
	
	private int empId;
	
	private int branchId;

	public AtmCardMstr getAtmCardMstr() {
		return atmCardMstr;
	}

	public void setAtmCardMstr(AtmCardMstr atmCardMstr) {
		this.atmCardMstr = atmCardMstr;
	}

	public int getBankId() {
		return bankId;
	}

	public void setBankId(int bankId) {
		this.bankId = bankId;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	
}

package com.mylogistics.services.bank;

import com.mylogistics.model.Branch;
import com.mylogistics.model.bank.BankMstr;

public class BankBranchService {
	
	private BankMstr bankMstr;
	
	private Branch branch;

	public BankMstr getBankMstr() {
		return bankMstr;
	}

	public void setBankMstr(BankMstr bankMstr) {
		this.bankMstr = bankMstr;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	

}

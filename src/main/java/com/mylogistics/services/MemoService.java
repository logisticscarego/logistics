package com.mylogistics.services;

import java.util.List;
import java.util.Map;

import com.mylogistics.model.Memo;

public class MemoService {

	public Memo memo;
	public List<Map<String, Object>> arList;
	
	public Memo getMemo() {
		return memo;
	}
	public void setMemo(Memo memo) {
		this.memo = memo;
	}
	public List<Map<String, Object>> getArList() {
		return arList;
	}
	public void setArList(List<Map<String, Object>> arList) {
		this.arList = arList;
	}
	
	
	
}
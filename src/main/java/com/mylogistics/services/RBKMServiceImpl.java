package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;

import com.mylogistics.model.RateByKm;

public class RBKMServiceImpl implements RBKMService{
	
	 private static List<RateByKm> rbkmList = new ArrayList<RateByKm>();

	    public List<RateByKm> getAllRBKM() {
	        return rbkmList;
	    }

	    public void addRBKM(RateByKm rateByKm) {
	    	rbkmList.add(rateByKm);
	    }

	    public void deleteRBKM(RateByKm rateByKm) {
        	System.out.println("enter into deleteRBKM of RBKMServiceImpl");
        	String temp = rateByKm.getRbkmFromStation()
        			+rateByKm.getRbkmToStation()
        			+rateByKm.getRbkmStateCode()
        			+rateByKm.getRbkmFromKm()
        			+rateByKm.getRbkmToKm()
        			+rateByKm.getRbkmVehicleType()
        			+rateByKm.getRbkmRate();
        	
        	if (!rbkmList.isEmpty()) {
        		System.out.println("size of rbkmList = "+rbkmList.size());
				for (int i = 0; i < rbkmList.size(); i++) {
					String listTemp = rbkmList.get(i).getRbkmFromStation()
							+rbkmList.get(i).getRbkmToStation()
							+rbkmList.get(i).getRbkmStateCode()
							+rbkmList.get(i).getRbkmFromKm()
							+rbkmList.get(i).getRbkmToKm()
							+rbkmList.get(i).getRbkmVehicleType()
							+rbkmList.get(i).getRbkmRate();
					if (temp.equalsIgnoreCase(listTemp)) {
						rbkmList.remove(rbkmList.get(i));
					}
				}
				System.out.println("after deleting size of rbkmList = "+rbkmList.size());
			}
	    }

	    public void deleteAllRBKM() {
	    	rbkmList.clear();
	    }
}

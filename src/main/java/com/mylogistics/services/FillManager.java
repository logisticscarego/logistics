package com.mylogistics.services;


import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.CellStyle;

import com.mylogistics.model.User;
/*import org.krams.tutorial.domain.PowerSupply;*/

public class FillManager {

/**
 * Fills the report with content
 * 
 * @param worksheet
 * @param startRowIndex starting row offset
 * @param startColIndex starting column offset
 * @param datasource the data source
 */
public static void fillReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<User> datasource) {
 // Row offset
 startRowIndex += 2;
  
 // Create cell style for the body
 HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
 bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
 bodyCellStyle.setWrapText(true);
  
 // Create body
 for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
  // Create a new row
  HSSFRow row = worksheet.createRow((short) i+1);

  // Retrieve the id value
  HSSFCell cell1 = row.createCell(startColIndex+0);
  cell1.setCellValue(datasource.get(i-2).getUserId());
  cell1.setCellStyle(bodyCellStyle);

  // Retrieve the creationTS value
  HSSFCell cell2 = row.createCell(startColIndex+1);
  cell2.setCellValue(datasource.get(i-2).getCreationTS());
  cell2.setCellStyle(bodyCellStyle);

  // Retrieve the password value
  HSSFCell cell3 = row.createCell(startColIndex+2);
  cell3.setCellValue(datasource.get(i-2).getPassword());
  cell3.setCellStyle(bodyCellStyle);

  // Retrieve the userAuthToken value
  HSSFCell cell4 = row.createCell(startColIndex+3);
  cell4.setCellValue(datasource.get(i-2).getUserAuthToken());
  cell4.setCellStyle(bodyCellStyle);

  // Retrieve the userBranchCode value
  HSSFCell cell5 = row.createCell(startColIndex+4);
  cell5.setCellValue(datasource.get(i-2).getUserBranchCode());
  cell5.setCellStyle(bodyCellStyle);
  
  // Retrieve the userCode value
  HSSFCell cell6 = row.createCell(startColIndex+5);
  cell6.setCellValue(datasource.get(i-2).getUserCode());
  cell6.setCellStyle(bodyCellStyle);
  
  // Retrieve the userCodeTemp value
  HSSFCell cell7 = row.createCell(startColIndex+6);
  cell7.setCellValue(datasource.get(i-2).getUserCodeTemp());
  cell7.setCellStyle(bodyCellStyle);
  
  // Retrieve the userRights value
  HSSFCell cell8 = row.createCell(startColIndex+7);
  cell8.setCellValue("hidden");
  cell8.setCellStyle(bodyCellStyle);
  
  // Retrieve the userRole value
  HSSFCell cell9 = row.createCell(startColIndex+8);
  cell9.setCellValue(datasource.get(i-2).getUserRole());
  cell9.setCellStyle(bodyCellStyle);
  
  // Retrieve the userStatus value
  HSSFCell cell10 = row.createCell(startColIndex+9);
  cell10.setCellValue(datasource.get(i-2).getUserStatus());
  cell10.setCellStyle(bodyCellStyle);
  
  // Retrieve the userName value
  HSSFCell cell11 = row.createCell(startColIndex+10);
  cell11.setCellValue(datasource.get(i-2).getUserName());
  cell11.setCellStyle(bodyCellStyle);
 }
}
}

package com.mylogistics.services;

import com.mylogistics.model.Customer;

public interface CustomerService {

	public Customer getCustomer(Integer custId);
	
}
package com.mylogistics.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;

import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.opc.ContentTypes;

import com.fasterxml.jackson.databind.ObjectMapper;

public class EmailUsingPhpService {

	public static Logger logger = Logger.getLogger(EmailUsingPhpService.class);
	
	public static void SendMail(String to, String subject, String msg, String path, String fileName){
		  try {
			  System.err.println("Going to send Mail().....");
			  HttpPost httpPost = new HttpPost("http://latto.in/workspace/sendemail/javamail.php");
			  // Reading File
			  File payload = new File(path);
		      MultipartEntity entity = new MultipartEntity( HttpMultipartMode.BROWSER_COMPATIBLE );
		      // Creading Parameters For email
		      entity.addPart( "userPhoto", new FileBody(payload));
		      entity.addPart( "email_id", new StringBody(to));
		      entity.addPart( "subject", new StringBody(subject));
		      entity.addPart( "msg", new StringBody(msg));
		      // Set parameters
		      httpPost.setEntity( entity );
		      		  
			  HttpClient client = new DefaultHttpClient();			  
			  // Here we go!
			  String response = EntityUtils.toString( client.execute( httpPost ).getEntity(), "UTF-8" );
			  logger.info("Result = "+response);
			  client.getConnectionManager().shutdown();
			  }catch(Exception e) {
				e.printStackTrace();
			  } 
		  System.err.println("End......");
	}
	
}
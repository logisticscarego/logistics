package com.mylogistics.services;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

import com.mylogistics.model.Address;
import com.mylogistics.model.Broker;
import com.mylogistics.model.ContPerson;
import com.mylogistics.model.Employee;
import com.mylogistics.model.Owner;
import com.mylogistics.model.VehicleVendorMstr;

public class ModelService {
	
	private ContPerson contPerson;
	private Address otherAddress;
	private Address registerAddress;
	private Address currentAddress;
	private Owner owner;
	private Broker broker;
	private Blob blob;
	private Blob confirmBlob;
	
	private VehicleVendorMstr vehVenMstr;
	
	public Blob getConfirmBlob() {
		return confirmBlob;
	}

	public void setConfirmBlob(Blob confirmBlob) {
		this.confirmBlob = confirmBlob;
	}
	private List<Employee> employeeList = new ArrayList<Employee>();
	
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	public void removeAllEmployeeList(){
		employeeList.clear();
	}
	
	public void setEmployee(Employee employee){
		employeeList.add(employee);
	}
	
	public Blob getBlob() {
		return blob;
	}
	
	public void setBlob(Blob blob) {
		this.blob = blob;
	}
	public Broker getBroker() {
		return broker;
	}
	public void setBroker(Broker broker) {
		this.broker = broker;
	}
	public Owner getOwner() {
		return owner;
	}
	public void setOwner(Owner owner) {
		this.owner = owner;
	}
	public Address getOtherAddress() {
		return otherAddress;
	}
	public void setOtherAddress(Address otherAddress) {
		this.otherAddress = otherAddress;
	}
	public Address getRegisterAddress() {
		return registerAddress;
	}
	public void setRegisterAddress(Address registerAddress) {
		this.registerAddress = registerAddress;
	}
	public Address getCurrentAddress() {
		return currentAddress;
	}
	public void setCurrentAddress(Address currentAddress) {
		this.currentAddress = currentAddress;
	}
	public ContPerson getContPerson() {
		return contPerson;
	}
	public void setContPerson(ContPerson contPerson) {
		this.contPerson = contPerson;
	}

	public VehicleVendorMstr getVehVenMstr() {
		return vehVenMstr;
	}

	public void setVehVenMstr(VehicleVendorMstr vehVenMstr) {
		this.vehVenMstr = vehVenMstr;
	}


}

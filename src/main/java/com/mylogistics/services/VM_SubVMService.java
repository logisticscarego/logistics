package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;

import com.mylogistics.model.FaCarDivision;
import com.mylogistics.model.SubVehicleMstr;
import com.mylogistics.model.VehicleMstr;

public class VM_SubVMService {

	private VehicleMstr vehMstr;
	
	private SubVehicleMstr sVehMstr;
	
	private List<FaCarDivision> faCarList = new ArrayList<>();

	public VehicleMstr getVehMstr() {
		return vehMstr;
	}

	public void setVehMstr(VehicleMstr vehMstr) {
		this.vehMstr = vehMstr;
	}

	public SubVehicleMstr getsVehMstr() {
		return sVehMstr;
	}

	public void setsVehMstr(SubVehicleMstr sVehMstr) {
		this.sVehMstr = sVehMstr;
	}

	public List<FaCarDivision> getFaCarList() {
		return faCarList;
	}

	public void setFaCarList(List<FaCarDivision> faCarList) {
		this.faCarList = faCarList;
	}
	
}

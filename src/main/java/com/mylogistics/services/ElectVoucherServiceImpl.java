package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;

public class ElectVoucherServiceImpl implements ElectVoucherService{
	
	private List<EM_SEMService> emsemList = new ArrayList<EM_SEMService>();

	public void addEMAndSEM(EM_SEMService eM_SEMService){
		emsemList.add(eM_SEMService);
	}
	
	public List<EM_SEMService> getAllEMAndSEM(){
		return emsemList;
	}
	
	public void deleteAllEMAndSEM(){
		emsemList.clear();
	}
	
	public void remove(int index){
		emsemList.remove(index);
	}

}

package com.mylogistics.services;

import java.util.List;

public interface ElectVoucherService {

	public void addEMAndSEM(EM_SEMService em_SEMService);
	
	public List<EM_SEMService> getAllEMAndSEM();
	
	public void deleteAllEMAndSEM();
	
	public void remove(int index);
}

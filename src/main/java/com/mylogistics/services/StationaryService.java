package com.mylogistics.services;

import com.mylogistics.model.HBO_StnNotification;
import com.mylogistics.model.Stationary;

public class StationaryService {
	
	private HBO_StnNotification hBO_StnNotification;
	private Stationary stationary;
	
	public HBO_StnNotification gethBO_StnNotification() {
		return hBO_StnNotification;
	}
	public void sethBO_StnNotification(HBO_StnNotification hBO_StnNotification) {
		this.hBO_StnNotification = hBO_StnNotification;
	}
	public Stationary getStationary() {
		return stationary;
	}
	public void setStationary(Stationary stationary) {
		this.stationary = stationary;
	}
	
	
}

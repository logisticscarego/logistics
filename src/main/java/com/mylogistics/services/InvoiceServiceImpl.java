package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;


public class InvoiceServiceImpl implements InvoiceService {
	
	
	private static List<InvAndDateService> invoiceList = new  ArrayList<InvAndDateService>();
	
	 public List<InvAndDateService> getAllInvoice() {
	        return invoiceList;
	 }
	 
	 public void addInvoice(InvAndDateService invAndDateService){
		 invoiceList.add(invAndDateService);
	 }
	 
	 public void deleteInvoice(InvAndDateService invAndDateService){
		 String clientInv = invAndDateService.getInvoice();
		 if(!invoiceList.isEmpty()){
			 for(int i=0;i<invoiceList.size();i++){
				 if(invoiceList.get(i).getInvoice().equalsIgnoreCase(clientInv)){
					 invoiceList.remove(invoiceList.get(i));
				 }
			 }
		 }
	 }
	 
	 public void deleteAll(){
		 invoiceList.clear();
	 }
}

package com.mylogistics.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.services.BankMasterService;

@Service
public class BankMasterServiceImpl implements BankMasterService {

	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Transactional
	public BankMstr getBankMstr(int bankMstrId) {
		return bankMstrDAO.getBankMstr(bankMstrId);
	}
	
	@Transactional
	public void update(BankMstr bankMstr) {
		bankMstrDAO.update(bankMstr);
	}
	
}
package com.mylogistics.services;

import com.mylogistics.model.Branch;
import com.mylogistics.model.ElectricityMstr;
import com.mylogistics.model.Employee;

public class EMstrService {

	private ElectricityMstr electricityMstr;
	
	private Branch branch;
	
	private Employee employee;
	
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public ElectricityMstr getElectricityMstr() {
		return electricityMstr;
	}

	public void setElectricityMstr(ElectricityMstr electricityMstr) {
		this.electricityMstr = electricityMstr;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
}

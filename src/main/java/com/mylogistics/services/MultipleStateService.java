package com.mylogistics.services;

import java.util.List;
import com.mylogistics.model.MultipleState;

public interface MultipleStateService {
	
	public List<MultipleState> getAllState();

    public void addState(MultipleState multipleState);
    
    public void deleteState(MultipleState multipleState);

    public void deleteAllState();


}

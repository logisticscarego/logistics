package com.mylogistics.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.mylogistics.model.FundAllocation;
import com.mylogistics.model.FundAllocationDetails;

@Service
public class FundAllocationService {
	
	private FundAllocation allocation;
	private List<FundAllocationDetails> fadList;
	private FundAllocationDetails fd;
	
	public FundAllocationDetails getFd() {
		return fd;
	}

	public void setFd(FundAllocationDetails fd) {
		this.fd = fd;
	}

	public void setAllocation(FundAllocation allocation) {
		this.allocation = allocation;
	}
	
	public FundAllocation getAllocation() {
		return allocation;
	}
	
	public void setFadList(List<FundAllocationDetails> fadList) {
		this.fadList = fadList;
	}
	
	public List<FundAllocationDetails> getFadList() {
		return fadList;
	}
	
	

}

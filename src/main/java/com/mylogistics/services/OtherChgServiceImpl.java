package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;

import com.mylogistics.model.OtherCharge;

public class OtherChgServiceImpl implements OtherChgService{
	
	private List<List<OtherCharge>> otherChargeList = new ArrayList<List<OtherCharge>>();
	
	 public List<List<OtherCharge>> getAllOtherCharge() {
	        return otherChargeList;
	 }
	 
	 public void addOtherCharge(List<OtherCharge> otChgList){
		 otherChargeList.add(otChgList);
	 }
	 
	/* public void deleteOtherCharge(OtherCharge otherCharge){
		 if(!otChgList.isEmpty()){
			 for(int i=0;i<otChgList.size();i++){
				 if(otChgList.get(i).getOtChgType().equalsIgnoreCase(otherCharge.getOtChgType())){
					 otChgList.remove(otChgList.get(i));
				 }
			 }
		 }
	 }*/
	 
	 public void deleteAll(){
		 otherChargeList.clear();
	 }
}

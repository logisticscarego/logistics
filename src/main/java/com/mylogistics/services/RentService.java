package com.mylogistics.services;

import com.mylogistics.model.Address;
import com.mylogistics.model.RentMstr;

public class RentService {

	private RentMstr rentMstr;
	
	private Address address;
	
	private int branchId;
	
	private int empId;

	public RentMstr getRentMstr() {
		return rentMstr;
	}

	public void setRentMstr(RentMstr rentMstr) {
		this.rentMstr = rentMstr;
	}

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}
	
	
	
}

package com.mylogistics.services;

import java.util.List;

public interface TelVoucherService {

	public void addTmAndSTm(TelM_SubTelMService telM_SubTelMService);
	
	public List<TelM_SubTelMService> getAllTmAndSTm();
	
	public void deleteAllTmAndSTm();
	
	public void remove(int index);
	
}

package com.mylogistics.services;

import java.util.List;

public interface InvoiceService {
	
	public List<InvAndDateService> getAllInvoice();

	public void addInvoice(InvAndDateService invAndDateService);
	
	public void deleteInvoice(InvAndDateService invAndDateService);
	 
	public void deleteAll();

}

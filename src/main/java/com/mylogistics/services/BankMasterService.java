package com.mylogistics.services;

import com.mylogistics.model.bank.BankMstr;

public interface BankMasterService {
	
	public BankMstr getBankMstr(int bankMstrId);
	public void update(BankMstr bankMstr);
	
}
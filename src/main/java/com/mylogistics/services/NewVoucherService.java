package com.mylogistics.services;

import java.util.Map;

public interface NewVoucherService {

	public Map<String, Object> saveNewVoucher(VoucherService voucherService);
	
}
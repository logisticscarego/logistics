package com.mylogistics.services;

import com.mylogistics.model.Challan;
import com.mylogistics.model.ChallanDetail;

public class Chln_ChlnDetService {
	
	private Challan challan;
	private ChallanDetail challanDetail;
	
	public Challan getChallan() {
		return challan;
	}
	public void setChallan(Challan challan) {
		this.challan = challan;
	}
	public ChallanDetail getChallanDetail() {
		return challanDetail;
	}
	public void setChallanDetail(ChallanDetail challanDetail) {
		this.challanDetail = challanDetail;
	}
}

package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;

public class BPVoucherServiceImpl implements BPVoucherService{

	private List<BPromService> bpsList = new ArrayList<BPromService>();
	
	public void addBPromSer(BPromService bPromService){
		bpsList.add(bPromService);
	}
	
	public List<BPromService> getAllBPromSer(){
		return bpsList;
	}
	
	public void deleteAllBPromSer(){
		bpsList.clear();
	}
	
	public void remove(int index){
		bpsList.remove(index);
	}
}

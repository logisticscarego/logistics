package com.mylogistics.services;

import java.sql.Date;

public class InvAndDateService {

	private String invoice;
	private Date date;
	
	public String getInvoice() {
		return invoice;
	}
	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
}

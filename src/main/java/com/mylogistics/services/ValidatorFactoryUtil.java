package com.mylogistics.services;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public class ValidatorFactoryUtil {

	public static Validator validator = null;
	public static Validator getValidator(){
		if(validator == null){
			ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
			validator = factory.getValidator();
		}
		return validator;
	}
	
}
package com.mylogistics.services;

import java.util.List;

public interface TravVoucherService {
	
	public void addTDService(TravDivService travDivService);
	
	public List<TravDivService> getAllTDService();
	
	public void deleteAllTDService();
	
	public void remove(int index);
}

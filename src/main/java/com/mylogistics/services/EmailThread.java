
package com.mylogistics.services;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

public class EmailThread implements Runnable {

	private List<String> emailList;
	private String subject;
	private String msg;
	private String path;
	private String fileName;
	
	Logger logger = Logger.getLogger(EmailThread.class);
	
	public EmailThread(List<String> emailList, String subject, String msg, String path, String fileName) {
		super();
		this.emailList = emailList;
		this.subject = subject;
		this.msg = msg;
		this.path = path;
		this.fileName = fileName;
	}

	@Override
	public void run() {
		logger.info("Goin to send email !");
		if(emailList != null){
			Iterator<String> it = emailList.iterator();
			while(it.hasNext()){
				String email = it.next();
				EmailUsingPhpService.SendMail(email, subject, msg, path, fileName);
			}
			emailList = null;
			subject = null;
			msg = null;
		}
		logger.info("Email sent !");
	}
	
}
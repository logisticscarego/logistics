package com.mylogistics.services;

import com.mylogistics.model.Customer;
import com.mylogistics.model.CustomerRepresentative;

public class Cust_CustRepService {
	
	private Customer customer;
	private CustomerRepresentative customerRepresentative;
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public CustomerRepresentative getCustomerRepresentative() {
		return customerRepresentative;
	}
	public void setCustomerRepresentative(CustomerRepresentative customerRepresentative) {
		this.customerRepresentative = customerRepresentative;
	}
	
}

package com.mylogistics.services;

import com.mylogistics.model.VehicleVendorMstr;

public class VehVendorService {

	private int ownId;
	private String ownCode;	
	
	private int brkId;
	private String brkCode;
	
	private String vvRcNo;
	
	private VehicleVendorMstr vehVenMstr;

	public int getOwnId() {
		return ownId;
	}

	public void setOwnId(int ownId) {
		this.ownId = ownId;
	}

	public int getBrkId() {
		return brkId;
	}

	public void setBrkId(int brkId) {
		this.brkId = brkId;
	}

	public String getVvRcNo() {
		return vvRcNo;
	}

	public void setVvRcNo(String vvRcNo) {
		this.vvRcNo = vvRcNo;
	}

	public VehicleVendorMstr getVehVenMstr() {
		return vehVenMstr;
	}

	public void setVehVenMstr(VehicleVendorMstr vehVenMstr) {
		this.vehVenMstr = vehVenMstr;
	}

	public String getOwnCode() {
		return ownCode;
	}

	public void setOwnCode(String ownCode) {
		this.ownCode = ownCode;
	}

	public String getBrkCode() {
		return brkCode;
	}

	public void setBrkCode(String brkCode) {
		this.brkCode = brkCode;
	}

	
		
}

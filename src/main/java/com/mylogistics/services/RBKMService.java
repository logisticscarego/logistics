package com.mylogistics.services;

import java.util.List;

import com.mylogistics.model.RateByKm;

public interface RBKMService {

	 	public List<RateByKm> getAllRBKM();

	    public void addRBKM(RateByKm rateByKm);

	    public void deleteRBKM(RateByKm rateByKm);

	    public void deleteAllRBKM();
}

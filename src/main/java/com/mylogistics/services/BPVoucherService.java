package com.mylogistics.services;

import java.util.List;

public interface BPVoucherService {

	public void addBPromSer(BPromService bPromService);
	
	public List<BPromService> getAllBPromSer();
	
	public void deleteAllBPromSer();
	
	public void remove(int index);
}

package com.mylogistics.services;

import org.springframework.stereotype.Service;

@Service
public class SuperAdminBehave {

	private boolean operatorLogin;
	private boolean adminLogin;
	private boolean superAdminLogin;
	
	public boolean isOperatorLogin() {
		return operatorLogin;
	}
	public void setOperatorLogin(boolean operatorLogin) {
		this.operatorLogin = operatorLogin;
	}
	public boolean isAdminLogin() {
		return adminLogin;
	}
	public void setAdminLogin(boolean adminLogin) {
		this.adminLogin = adminLogin;
	}
	public boolean isSuperAdminLogin() {
		return superAdminLogin;
	}
	public void setSuperAdminLogin(boolean superAdminLogin) {
		this.superAdminLogin = superAdminLogin;
	}
	
	
	
}

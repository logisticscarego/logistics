package com.mylogistics.services;

import com.mylogistics.model.HBOptr_Notification;

public class HBO_NotificationService {
	
	private HBOptr_Notification hboN;

	public HBOptr_Notification getHboN() {
		return hboN;
	}

	public void setHboN(HBOptr_Notification hboN) {
		this.hboN = hboN;
	}
	
}

package com.mylogistics.DAOImpl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;

import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.OwnerDAO;
import com.mylogistics.constants.AddressCNTS;
import com.mylogistics.constants.BrokerCNTS;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.constants.OwnerCNTS;
import com.mylogistics.constants.OwnerImgCNTS;
import com.mylogistics.model.Address;
import com.mylogistics.model.Broker;
import com.mylogistics.model.BrokerImg;
import com.mylogistics.model.Challan;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.Owner;
import com.mylogistics.model.OwnerImg;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.PanService;

public class OwnerDAOImpl implements OwnerDAO {
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;	
	
	public static Logger logger = Logger.getLogger(OwnerDAOImpl.class);
	
	private String ownerPanImgPath = "/var/www/html/Erp_Image/Owner/Pan";
	private String ownerDecImgPath = "/var/www/html/Erp_Image/Owner/Dec";
	private String ownerBankDetImgPath = "/var/www/html/Erp_Image/Owner/BankDetail";

	@Autowired
	private HttpSession httSession;
	
	@Autowired
	public OwnerDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	


	@SuppressWarnings("unchecked")
	public List<Owner>  getOwner(){
		List<Owner> list = new ArrayList<Owner>();
		logger.info("Enter into getOwner()...");		
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Owner.class);		
			list = cr.list();	
		}catch (Exception e) {
			logger.error("Exception : "+e);
			e.printStackTrace();
		}
		session.clear();
		session.close();
		logger.info("Enter into getOwner() : Owner Size = "+list.size());
		return list;
	}

	


	@SuppressWarnings("unchecked")
	public List<Owner> getOwnerList(String ownCode){

		List<Owner> result = new ArrayList<Owner>();	 
		try{
			System.out.println("ownCode is"+ownCode);
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Owner.class);
			cr.add(Restrictions.eq(OwnerCNTS.OWN_CODE,ownCode));
			result =cr.setMaxResults(1).list();
		}

		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return  result; 
	}

	@Transactional
	public int updateOwner(Owner owner){
		logger.info("Enter into updateOwner() : OwnerId = "+owner.getOwnId());
		int temp;
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Owner own = (Owner) session.get(Owner.class, owner.getOwnId());
			owner.setCreationTS(calendar);
			session.merge(owner);
			transaction.commit();			
			temp= 1;
			logger.info("Onwer is updated !");
		}catch(Exception e){
			logger.error("Exception : "+e);
			temp= -1;
		}
		session.flush();
		session.clear();
		session.close();
		logger.info("Exit from updateOwner()");
		return temp;
	}

	@SuppressWarnings("unchecked")
	public List<String> getOwnerCode(){
		List<String> oList = new ArrayList<String>();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Owner.class);
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property("ownCode"));
			cr.setProjection(projList);
			oList = cr.list();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return oList;
	}

	

	
	
	
	public Owner getOwnerData(int id){
		Session session;
		session = this.sessionFactory.openSession();
			Owner own=(Owner) session.get(Owner.class, id);
			session.clear();
			session.close();
		return own;
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, String>> getOwnCodesIsBnkD(String ownCode){
		List<Map<String, String>> ownerCodeList = new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		try{
			
			Criteria cr=session.createCriteria(Owner.class);
			cr.add(Restrictions.eq(OwnerCNTS.IS_OWN_BNK_DET,false));
			cr.add(Restrictions.like(OwnerCNTS.OWN_NAME,"%"+ownCode+"%"));
			ProjectionList projectionList = Projections.projectionList();
			projectionList.add(Projections.property(OwnerCNTS.OWN_CODE),"ownCode");
			projectionList.add(Projections.property(OwnerCNTS.OWN_NAME),"ownName");
			cr.setProjection(projectionList);
			cr.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			ownerCodeList =cr.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return ownerCodeList;

	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Map<String,String>> getOwnNCF(){
		System.out.println("enter into getOwnNCF function");
		//Map<String,String> map = new HashMap<String,String>();
		List<Map<String,String>> ownList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Owner.class);
			List<Owner> ownerCodeList =cr.list();
			
			if(!ownerCodeList.isEmpty()){
				for(int i=0;i<ownerCodeList.size();i++){
						Map<String,String> map = new HashMap<String,String>();
						
						map.put("id",String.valueOf(ownerCodeList.get(i).getOwnId()));
						map.put("code",ownerCodeList.get(i).getOwnCode());
						map.put("faCode",ownerCodeList.get(i).getOwnFaCode());
						map.put("name",ownerCodeList.get(i).getOwnName());
						map.put("panNo",ownerCodeList.get(i).getOwnPanNo());
						map.put("panImgId",String.valueOf(ownerCodeList.get(i).getOwnImgId()));
						
						ownList.add(map);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return ownList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String,String>> getOwnNCFByName(String ownName){
		logger.info("Enter into getOwnNCFByName() : ownName = "+ownName);
		List<Map<String,String>> ownList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Owner.class);
			cr.add(Restrictions.like(OwnerCNTS.OWN_NAME, ownName+"%"));
			
			List<Owner> ownerCodeList =cr.list();			
			if(!ownerCodeList.isEmpty()){
				for(int i=0;i<ownerCodeList.size();i++){
						Map<String,String> map = new HashMap<String,String>();
						
						map.put("id",String.valueOf(ownerCodeList.get(i).getOwnId()));
						map.put("code",ownerCodeList.get(i).getOwnCode());
						map.put("faCode",ownerCodeList.get(i).getOwnFaCode());
						map.put("name",ownerCodeList.get(i).getOwnName());
						map.put("panNo",ownerCodeList.get(i).getOwnPanNo());
						map.put("panImgId",String.valueOf(ownerCodeList.get(i).getOwnImgId()));
						
						ownList.add(map);
				}
			}
			
		}catch(Exception e){
			logger.error("Exception : "+e);
			e.printStackTrace();
		}
		session.clear();
		session.close();
		logger.info("Exit from getOwnNCFByName()");
		return ownList;
	}
	
	@SuppressWarnings("unchecked")
	public Owner getOwnByCode(String ownCode){
		System.out.println("enter into getOwnByCode function");
		Owner owner = null;
		try{
			List<Owner> ownList = new ArrayList<>();
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Owner.class);
			cr.add(Restrictions.eq(OwnerCNTS.OWN_CODE,ownCode));
			ownList = cr.list();
			if(!ownList.isEmpty()){
				owner = ownList.get(0);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return owner;
	}
	
	@Override
	public Boolean isOwnerAvailable(String ownName, String branchCode){
		logger.info("Enter into isOwnerAvailable() : OwnerName = "+ownName+" : BranchCode = "+branchCode);
		Boolean isAvailable = false;		
		try{
			session = this.sessionFactory.openSession();
			List<String> ownList = session.createCriteria(Owner.class)
					.add(Restrictions.eq(OwnerCNTS.BRANCH_CODE, branchCode))
					.add(Restrictions.eq(OwnerCNTS.OWN_NAME, ownName))
					.setProjection(Projections.property(OwnerCNTS.OWN_NAME))
					.list();
			if(ownList.isEmpty())
				isAvailable = true;
			else
				isAvailable = false;
		}catch(Exception e){
			logger.info("Exception : "+e);
		}
		session.clear();
		session.close();
		logger.info("Exit from isOwnerAvailable() : IsAvailable = "+isAvailable);
		return isAvailable;
	}
	
	@Override
	public Boolean isOwnerAvailable(String panNo){
		logger.info("Enter into isOwnerAvailable() : OwnerName = "+panNo);
		Boolean isAvailable = false;		
		try{
			session = this.sessionFactory.openSession();
			List<String> ownList = session.createCriteria(Owner.class)
					.add(Restrictions.eq(OwnerCNTS.OWN_PAN_NO, panNo))
					.setProjection(Projections.property(OwnerCNTS.OWN_NAME))
					.list();
			if(ownList.isEmpty())
				isAvailable = true;
			else
				isAvailable = false;
		}catch(Exception e){
			logger.info("Exception : "+e);
		}
		session.clear();
		session.close();
		logger.info("Exit from isOwnerAvailable() : IsAvailable = "+isAvailable);
		return isAvailable;
	}
	
	
	@Override
	public Boolean isOwnerAvailable(Session session,String panNo){
		logger.info("Enter into isOwnerAvailable() : OwnerName = "+panNo);
		Boolean isAvailable = false;		
			List<String> ownList = session.createCriteria(Owner.class)
					.add(Restrictions.eq(OwnerCNTS.OWN_PAN_NO, panNo))
					.setProjection(Projections.property(OwnerCNTS.OWN_NAME))
					.list();
			if(ownList.isEmpty())
				isAvailable = true;
			else
				isAvailable = false;
		return isAvailable;
	}
	
	
	
	@Override
	public int saveOwnAndImg(Owner owner , OwnerImg ownerImg, Blob ownerPanImg, Blob ownerDecImg){
		logger.info("Enter into saveOwnAndImg()");
		User currentUser = (User) httSession.getAttribute("currentUser");
		int res = 0;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			int ownId = (Integer) session.save(owner);			
			String ownerCode = "own"+String.valueOf(ownId);
			owner.setOwnCode(ownerCode);
			if(ownId > 0){
				logger.info("Owner is saved !");
				if(ownerPanImg != null || ownerDecImg != null){
					// Writing PDF of Pan / Dec
					try{
						if(ownerPanImg != null){
							ownerImg.setOwnPanImgPath(ownerPanImgPath+"/"+ownerCode+".pdf");
							byte ownerPanImgInBytes[] = ownerPanImg.getBytes(1, (int)ownerPanImg.length());
							File panFile = new File(ownerImg.getOwnPanImgPath());
							if(panFile.exists())
								panFile.delete();
							if(panFile.createNewFile()){											    
								OutputStream targetFile=  new FileOutputStream(panFile);
					            targetFile.write(ownerPanImgInBytes);			            
					            targetFile.close();	
							}
						}
						if(ownerDecImg != null){
							ownerImg.setOwnDecImgPath(ownerDecImgPath+"/"+ownerCode+".pdf");
							byte ownerDecImgInBytes[] = ownerDecImg.getBytes(1, (int)ownerDecImg.length());
							File decFile = new File(ownerImg.getOwnDecImgPath());
							if(decFile.exists())
								decFile.delete();
							if(decFile.createNewFile()){											    
								OutputStream targetFile=  new FileOutputStream(decFile);
					            targetFile.write(ownerDecImgInBytes);			            
					            targetFile.close();	
							}
						}
					}catch(Exception e){
						logger.error("Exception : "+e);
						res = 0;
						return res;
					}					
					ownerImg.setOwnId(ownId);
					ownerImg.setbCode(currentUser.getUserBranchCode());
					ownerImg.setUserCode(currentUser.getUserCode());
					int ownImgId = (Integer) session.save(ownerImg);
					if(ownImgId > 0){
						logger.info("OwnerImg is saved !");
						owner.setOwnImgId(ownImgId);
						if(ownerPanImg != null){
							owner.setOwnIsPanImg(true);
						}
						if(ownerDecImg != null){
							owner.setOwnIsDecImg(true);
						}
						
					}else{
						logger.info("OwnerImg is not saved !");
					}
				}
			}
			session.update(owner);
			transaction.commit();
			res = ownId;
		}catch(Exception e){
			logger.error("Owner is not saved ! Exception = "+e);
		}
		session.clear();
		session.close();
		logger.info("Exit from saveOwnAndImg()....");
		return res;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> getOwnNameCodeId() {
		
		System.out.println("enter into getOwnNameCodeId function");
		User currentUser = (User) httSession.getAttribute("currentUser");
		List<Map<String, Object>> ownerListMap = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Owner.class);
			//cr.add(Restrictions.eq(OwnerCNTS.BRANCH_CODE, currentUser.getUserBranchCode()));
			List<Owner> ownList = cr.list();
			
			if(!ownList.isEmpty()){
				for (Owner owner : ownList) {
					/*System.out.println("owner branch code: "+owner.getBranchCode()+" user branch code: "+currentUser.getUserBranchCode());*/
					Map<String, Object> ownMap = new HashMap<>();
					ownMap.put(OwnerCNTS.OWN_ID, owner.getOwnId());
					ownMap.put(OwnerCNTS.OWN_NAME, owner.getOwnName());
					ownMap.put(OwnerCNTS.OWN_CODE, owner.getOwnCode());
					ownMap.put(OwnerCNTS.OWN_PAN_NO, owner.getOwnPanNo());
					ownerListMap.add(ownMap);
				}
			}		
		}catch(Exception e){
			e.printStackTrace();
		}
		session.close();
		
		return ownerListMap;
	}
	
@Override	
public List<Map<String, Object>> getOwnNameCodeIdByName(String ownName) {
		
		System.out.println("enter into getOwnNameCodeId function");
		User currentUser = (User) httSession.getAttribute("currentUser");
		List<Map<String, Object>> ownerListMap = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Owner.class);
			cr.add(Restrictions.like(OwnerCNTS.OWN_NAME, "%"+ownName+"%"));
			List<Owner> ownList = cr.list();
			
			if(!ownList.isEmpty()){
				for (Owner owner : ownList) {
					/*System.out.println("owner branch code: "+owner.getBranchCode()+" user branch code: "+currentUser.getUserBranchCode());*/
					Map<String, Object> ownMap = new HashMap<>();
					ownMap.put(OwnerCNTS.OWN_ID, owner.getOwnId());
					ownMap.put(OwnerCNTS.OWN_NAME, owner.getOwnName());
					ownMap.put(OwnerCNTS.OWN_CODE, owner.getOwnCode());
					ownMap.put(OwnerCNTS.OWN_PAN_NO, owner.getOwnPanNo());
					ownerListMap.add(ownMap);
				}
			}		
		}catch(Exception e){
			e.printStackTrace();
		}
		session.close();
		
		return ownerListMap;
	}
	
	
	@SuppressWarnings("unchecked")
	public Owner getOwnByFaCode(String faCode){
		System.out.println("enter into getOwnByFaCode function");
		Owner owner = null;
		List<Owner> ownList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Owner.class);
			cr.add(Restrictions.eq(OwnerCNTS.OWN_FA_CODE,faCode));
			ownList = cr.list();
			if(!ownList.isEmpty()){
				owner = ownList.get(0);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return owner;
	}

	
	@SuppressWarnings("unchecked")
	public int checkOwnPan(String ownCode){
		logger.info("Enter into checkOwnPan() : OwnCode = "+ownCode);
		List<Owner> ownList = new ArrayList<>();
		int res = 0;
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Owner.class);
			cr.add(Restrictions.eq(OwnerCNTS.OWN_CODE,ownCode));
			ownList = cr.list();
			if(!ownList.isEmpty()){
				logger.info("Owner found : OwnID = "+ownList.get(0).getOwnId());
				List<OwnerImg> ownImgList = new ArrayList<>();
				cr=session.createCriteria(OwnerImg.class);
				cr.add(Restrictions.eq(OwnerImgCNTS.OWN_ID,ownList.get(0).getOwnId()));
				ownImgList = cr.list();
				if(!ownImgList.isEmpty()){
					logger.info("OwnerImg found : OwnerImg ID = "+ownImgList.get(0).getOwnImgId());
					res = 1;
				}else{
					logger.info("OwnerImg not found :");
					res = 0;
				}
			}else
				logger.info("Owner not found !");
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception : "+e);
		}
		session.clear();
		session.close();
		logger.info("Exit from checkOwnPan()");
		return res;
	}
	
	
	
	@Override
	public Map<String, Object> saveOwnPan(PanService panService, OwnerImg ownerImg,
			float ownIntRate, boolean isPanImg, boolean isDecImg, Blob ownerPanImg, Blob ownerDecImg) {
		
		logger.info("Enter into saveOwnPan()...");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		int temp = 0;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			int ownImgId = -1;			
			Owner owner = (Owner) session.get(Owner.class, panService.getOwnId());			
			String ownerCode = owner.getOwnCode();
			logger.info("Owner found !");			
			// Writing Pan/Dec Image			
			try{
				if(ownerPanImg != null){
					logger.info("Going to write pan image !");
					ownerImg.setOwnPanImgPath(ownerImg.getOwnPanImgPath()+"/"+ownerCode+".pdf");
					byte ownerPanImgInBytes[] = ownerPanImg.getBytes(1, (int)ownerPanImg.length());
					File panFile = new File(ownerImg.getOwnPanImgPath());
					if(panFile.exists())
						panFile.delete();
					if(panFile.createNewFile()){											    
						OutputStream targetFile=  new FileOutputStream(panFile);
			            targetFile.write(ownerPanImgInBytes);			            
			            targetFile.close();	
			            logger.info("writing pan image done !");
					}
				}
				if(ownerDecImg != null){
					logger.info("Going to write dec image !");
					ownerImg.setOwnDecImgPath(ownerImg.getOwnDecImgPath()+"/"+ownerCode+".pdf");
					byte ownerDecImgInBytes[] = ownerDecImg.getBytes(1, (int)ownerDecImg.length());
					File decFile = new File(ownerImg.getOwnDecImgPath());			
					if(decFile.exists())
						decFile.delete();
					if(decFile.createNewFile()){											    
						OutputStream targetFile=  new FileOutputStream(decFile);
			            targetFile.write(ownerDecImgInBytes);			            
			            targetFile.close();
			            logger.info("Writing dec image done !");
					}
				}
			}catch(Exception e){
				logger.error("Error in writing pan image : "+e);
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Error in uploading pan image");
				return resultMap;
			}			
			Integer id = owner.getOwnImgId();
			logger.info("Owner Imgage ID : "+id);
			if (id != null && id > 0) {
				OwnerImg ownImg = (OwnerImg)session.get(OwnerImg.class, owner.getOwnImgId());
				ownerImg.setOwnImgId(owner.getOwnImgId());
				session.merge(ownerImg);
				ownImgId = owner.getOwnImgId();
				logger.info("Owner image is updated !");
			} else {
				ownImgId = (Integer) session.save(ownerImg);
				logger.info("Owner image is saved !");
			}			
			owner.setOwnImgId(ownImgId);
			owner.setOwnIsPanImg(isPanImg);
			owner.setOwnIsDecImg(isDecImg);
			owner.setOwnPanIntRt(ownIntRate);
			owner.setOwnPanDOB(panService.getPanDOB());
			owner.setOwnPanDt(panService.getPanDt());
			owner.setOwnPanName(panService.getPanName());
			owner.setOwnPanNo(panService.getPanNo());			
			
			session.update(owner);			
			transaction.commit();
			
			logger.info("Owner is updated !");
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			resultMap.put("msg", "Record is updated !");
			
		}catch(Exception e){
			logger.error("Error in saveOwnPan() : "+e);
			System.out.println("Error in saveOwnPan - OwnerDAOImpl : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");
		}finally{	
			session.clear();
			session.close();
		}
		logger.info("Exit from saveOwnPan()...");
		return resultMap;
	}

	@Override
	@Transactional
	public Blob getOwnPanImg(int ownImgId) {
		System.out.println("enter into getOwnPanImg function");
		logger.info("Enter into getOwnPanImg() : OwnImgID = "+ownImgId);
		Blob blob = null;
		try{
			session = this.sessionFactory.openSession();
			OwnerImg ownerImg = (OwnerImg) session.get(OwnerImg.class, ownImgId);
			String panImgPath = ownerImg.getOwnPanImgPath();
			if(! panImgPath.equalsIgnoreCase("")){
				File file = new File(panImgPath);
				if(file.exists()){
					Path path = Paths.get(panImgPath);
					byte imgInBytes[] = Files.readAllBytes(path);
					blob = new SerialBlob(imgInBytes);
				}					
			}			
		}catch(Exception e){
			logger.error("Exception : "+e);
			e.printStackTrace();
		}finally{
			session.clear();
			session.close();
		}
		logger.info("Exit from getOwnPanImg()");
		return blob;
	}

	@Override
	@Transactional
	public List<Map<String, Object>> getOwnNameCodeIdFa() {
		
		List<Map<String, Object>> ownNameCodeIdFaList = new ArrayList<>();
		
		try {
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("select new map(ownId as ownId, ownCode as ownCode, ownName as ownName, ownFaCode as ownFaCode) from Owner"); 
			ownNameCodeIdFaList = query.list();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return ownNameCodeIdFaList;
	}

	@Override
	@Transactional
	public boolean isOwnBrkHasPan(int ownId, int brkId) {
		System.out.println("isOwnBrkHasPan() DB");		
		logger.info("Owner ID : "+ownId +" :: Broker ID :"+brkId);
		boolean isPan = false;		
		try {
			session = this.sessionFactory.openSession();
			Owner owner = (Owner) session.get(Owner.class, ownId);			
			if (owner.isOwnIsPanImg()) {
				isPan = owner.isOwnIsPanImg();
				logger.info("Owner has pan img : "+isPan);
			} else {
				Broker broker = (Broker) session.get(Broker.class, brkId);
				isPan = broker.isBrkIsPanImg();
				logger.info("Broker has pan img : "+isPan);
			}			
			session.flush();
		} catch (Exception e) {
			
		}
		session.clear();
		session.close();
		return isPan;
	}
	
	@Override
	public List<Owner> getOwnerByNameCode(Session session, User user, String ownNameCode, List<String> proList){
		logger.info("UserID = "+user.getUserId()+" : Enter into getOwnerByNameCode() : OwnNameCode = "+ownNameCode);
		List<Owner> ownList = null;	
		try{
			Criteria cr = session.createCriteria(Owner.class);
			if(proList != null){
				ProjectionList projectionList = Projections.projectionList();
				for(int i=0; i<proList.size(); i++)
					projectionList.add(Projections.property(proList.get(i)), proList.get(i));
				cr.setProjection(projectionList);
				cr.setResultTransformer(Transformers.aliasToBean(Owner.class));
			}
			if(NumberUtils.isNumber(ownNameCode))
				cr.add(Restrictions.like(OwnerCNTS.OWN_FA_CODE, "%"+ownNameCode));
			else
				cr.add(Restrictions.like(OwnerCNTS.OWN_NAME, ownNameCode+"%"));
			
			ownList = cr.list();
			logger.info("UserID = "+user.getUserId()+" : Owner Size = "+ownList.size());
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId()+" : Exit from getOwnerByNameCode() : OwnNameCode = "+ownNameCode);
		return ownList;
	}
	
	@Override
	public List<Map<String, Object>> getOwnNamePhCodeIdFa() {
		
		List<Map<String, Object>> ownNameCodeIdFaList = new ArrayList<>();
		
		try {
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("select new map(ownId as ownId, ownCode as ownCode, ownName as ownName,ownPhNoList as ownPhNoList,ownFaCode as ownFaCode) from Owner"); 
			ownNameCodeIdFaList = query.list();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return ownNameCodeIdFaList;
	}
	
	
	@Override
	public List<Map<String, Object>> getOwnNameCodeByPanNo(String panNo) {
		
		List<Map<String, Object>> ownNameCodeList = new ArrayList<>();
		Session session=null;
		try {
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("select new map(ownId as ownId, ownCode as ownCode, ownName as ownName,ownPanName as ownPanName,ownFaCode as ownFaCode) from Owner where ownPanNo ='"+panNo+"' "); 
			ownNameCodeList = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return ownNameCodeList;
	}
	
	
	@Override
	public int saveOwner(Session session,Owner owner,Blob panImg,Blob decImg,Blob chqImg){
		User currentUser = (User) httSession.getAttribute("currentUser");
		//owner.setbCode(currentUser.getUserBranchCode());
		owner.setBranchCode(currentUser.getUserBranchCode());
		owner.setUserCode(currentUser.getUserCode());
		System.out.println("owner "+owner);
		
		FAParticular fAParticular=(FAParticular) session.get(FAParticular.class, 8);
		
		owner.setfAParticular(fAParticular);
		owner.setView(false);
		
		
		int temp=0;
			temp = (Integer) session.save(owner);
			int faCode=800000+temp;
			owner.setOwnCode("own"+temp);
			owner.setOwnFaCode("0"+faCode);
			
			
			
			
			try{
				Path path = Paths.get(ownerPanImgPath);
				if(! Files.exists(path))
					Files.createDirectories(path);
				
				Path decPath = Paths.get(ownerDecImgPath);
				if(! Files.exists(decPath))
					Files.createDirectories(decPath);
				
				
				Path bnkPath = Paths.get(ownerBankDetImgPath);
				if(! Files.exists(bnkPath))
					Files.createDirectories(bnkPath);
				
			}catch(Exception e){
				System.out.println("Error in creating direcotry : "+e);
				logger.error("Error in creating directory : "+e);
			}
			
			
			
			//image code
			OwnerImg ownImg = new OwnerImg();
			try {
				
			
			if (panImg != null && decImg != null) {
				
				
				ownImg.setOwnPanImgPath(ownerPanImgPath+"/"+"own"+temp+".pdf");
				byte ownerPanImgInBytes[] = panImg.getBytes(1, (int)panImg.length());
				File panFile = new File(ownImg.getOwnPanImgPath());
				if(panFile.exists())
					panFile.delete();
				if(panFile.createNewFile()){
					OutputStream targetFile=  new FileOutputStream(panFile);
		            targetFile.write(ownerPanImgInBytes);
		            targetFile.close();
				}
				
				
				ownImg.setOwnDecImgPath(ownerDecImgPath+"/"+"own"+temp+".pdf");
				byte ownerDecImgInBytes[] = decImg.getBytes(1, (int)decImg.length());
				File decFile = new File(ownImg.getOwnDecImgPath());
				if(decFile.exists())
					decFile.delete();
				if(decFile.createNewFile()){											    
					OutputStream targetFile=  new FileOutputStream(decFile);
		            targetFile.write(ownerDecImgInBytes);		            
		            targetFile.close();	
				}
				
				owner.setOwnPanIntRt(0);
			
				
			} else if (panImg != null ) {
				
				ownImg.setOwnPanImgPath(ownerPanImgPath+"/"+"own"+temp+".pdf");
				byte ownerPanImgInBytes[] = panImg.getBytes(1, (int)panImg.length());
				File panFile = new File(ownImg.getOwnPanImgPath());
				if(panFile.exists())
					panFile.delete();
				if(panFile.createNewFile()){											    
					OutputStream targetFile=  new FileOutputStream(panFile);
		            targetFile.write(ownerPanImgInBytes);			            
		            targetFile.close();	
				}
				
				
				if (owner.getOwnPanNo().charAt(3) == 'P' ||	owner.getOwnPanNo().charAt(3) == 'H' ||	owner.getOwnPanNo().charAt(3) == 'J') {
					owner.setOwnPanIntRt(1);
				}else {
					owner.setOwnPanIntRt(2);
				}
			}
			
			if(chqImg != null) {
				ownImg.setOwnBnkDetImgPath(ownerBankDetImgPath+"/"+"own"+temp+".pdf");
				byte ownerChqImgInBytes[] = chqImg.getBytes(1, (int)chqImg.length());
				File chqFile = new File(ownImg.getOwnBnkDetImgPath());
				if(chqFile.exists())
					chqFile.delete();
				if(chqFile.createNewFile()){											    
					OutputStream targetFile=  new FileOutputStream(chqFile);
		            targetFile.write(ownerChqImgInBytes);			            
		            targetFile.close();	
				}
			}
			
			}catch(Exception e) {
				e.printStackTrace();
				logger.error("exception in OwnerImg save"+e);
				System.out.println("exception in OwnerImg save"+e);
			}
			
							
				ownImg.setOwnId(temp);
				ownImg.setbCode(currentUser.getUserBranchCode());
				ownImg.setUserCode(currentUser.getUserCode());
				int ownImgId = (Integer) session.save(ownImg);
				if(ownImgId > 0){
					logger.info("OwnerImg is saved !");
					owner.setOwnImgId(ownImgId);
					if(panImg != null){
						owner.setOwnIsPanImg(true);
					}
					if(decImg != null){
						owner.setOwnIsDecImg(true);
					}
					
					if(chqImg != null){
						owner.setOwnIsChqImg(true);
					}
					
				}else{
					logger.info("OwnerImg is not saved !");
				}
			
			
				
			
			session.update(owner);
			//session.flush();
			//session.clear();
			System.out.println("owner Data is inserted successfully");
		return temp;
	}
	
	
	@Override
	public Map<String,Object> getOwnerDetail(int ownId){
		Map<String,Object> map=new HashMap<>();
		
		Session session=this.sessionFactory.openSession();
		try {
			
			Owner owner=(Owner) session.get(Owner.class, ownId);
			List<Address> addressList= session.createCriteria(Address.class)
					.add(Restrictions.eq(AddressCNTS.REF_CODE, owner.getOwnCode()))
					.add(Restrictions.eq(AddressCNTS.TYPE, "Current Address")).list();
			
			map.put("owner", owner);
			if(!addressList.isEmpty()) {
				Address address=addressList.get(0);
				map.put("address", address);
			}else {
				map.put("address", "");
			}
			
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}catch(Exception e) {
			e.printStackTrace();
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}finally {
			session.clear();
			session.close();
		}
		
		return map;
	}
	
	
	
	@Override
	public int updateOwner(Session session,Owner owner,Blob panImg,Blob decImg,Blob chqImg){
		User currentUser = (User) httSession.getAttribute("currentUser");
		//owner.setbCode(currentUser.getUserBranchCode());
		owner.setBranchCode(currentUser.getUserBranchCode());
		owner.setUserCode(currentUser.getUserCode());
		System.out.println("OwnerId="+owner.getOwnId());
		int temp=owner.getOwnId();
			/*temp = (Integer) session.save(owner);
			int faCode=800000+temp;
			owner.setOwnCode("own"+temp);
			owner.setOwnFaCode("0"+faCode);
			
			FAParticular fAParticular=(FAParticular) session.get(FAParticular.class, 8);
			
			owner.setfAParticular(fAParticular);
			owner.setView(false);
			*/
			
			
			try{
				Path path = Paths.get(ownerPanImgPath);
				if(! Files.exists(path))
					Files.createDirectories(path);
				
				Path decPath = Paths.get(ownerDecImgPath);
				if(! Files.exists(decPath))
					Files.createDirectories(decPath);
				
				
				Path bnkPath = Paths.get(ownerBankDetImgPath);
				if(! Files.exists(bnkPath))
					Files.createDirectories(bnkPath);
				
			}catch(Exception e){
				System.out.println("Error in creating direcotry : "+e);
				logger.error("Error in creating directory : "+e);
			}
			
			
			
			//image code
			if(owner.getOwnImgId()==null)
				owner.setOwnImgId(0);
			
			
			if(owner.getOwnImgId()>0) {
				
				OwnerImg ownImg = (OwnerImg) session.get(OwnerImg.class, owner.getOwnImgId());
				System.out.println("ownerImgId="+ownImg);
				try {
					
				
				if (panImg != null && decImg != null) {
					
					
					ownImg.setOwnPanImgPath(ownerPanImgPath+"/"+"own"+temp+".pdf");
					byte ownerPanImgInBytes[] = panImg.getBytes(1, (int)panImg.length());
					File panFile = new File(ownImg.getOwnPanImgPath());
					if(panFile.exists())
						panFile.delete();
					if(panFile.createNewFile()){
						OutputStream targetFile=  new FileOutputStream(panFile);
			            targetFile.write(ownerPanImgInBytes);
			            targetFile.close();
					}
					
					
					ownImg.setOwnDecImgPath(ownerDecImgPath+"/"+"own"+temp+".pdf");
					byte ownerDecImgInBytes[] = decImg.getBytes(1, (int)decImg.length());
					File decFile = new File(ownImg.getOwnDecImgPath());
					if(decFile.exists())
						decFile.delete();
					if(decFile.createNewFile()){											    
						OutputStream targetFile=  new FileOutputStream(decFile);
			            targetFile.write(ownerDecImgInBytes);		            
			            targetFile.close();	
					}
					
					owner.setOwnPanIntRt(0);
				
					
				} else if (panImg != null ) {
					
					ownImg.setOwnPanImgPath(ownerPanImgPath+"/"+"own"+temp+".pdf");
					byte ownerPanImgInBytes[] = panImg.getBytes(1, (int)panImg.length());
					File panFile = new File(ownImg.getOwnPanImgPath());
					if(panFile.exists())
						panFile.delete();
					if(panFile.createNewFile()){											    
						OutputStream targetFile=  new FileOutputStream(panFile);
			            targetFile.write(ownerPanImgInBytes);			            
			            targetFile.close();	
					}
					
					
					if (owner.getOwnPanNo().charAt(3) == 'P' ||	owner.getOwnPanNo().charAt(3) == 'H' ||	owner.getOwnPanNo().charAt(3) == 'J') {
						owner.setOwnPanIntRt(1);
					}else {
						owner.setOwnPanIntRt(2);
					}
				}
				
				if(chqImg != null) {
					ownImg.setOwnBnkDetImgPath(ownerBankDetImgPath+"/"+"own"+temp+".pdf");
					byte ownerChqImgInBytes[] = chqImg.getBytes(1, (int)chqImg.length());
					File chqFile = new File(ownImg.getOwnBnkDetImgPath());
					if(chqFile.exists())
						chqFile.delete();
					if(chqFile.createNewFile()){											    
						OutputStream targetFile=  new FileOutputStream(chqFile);
			            targetFile.write(ownerChqImgInBytes);			            
			            targetFile.close();	
					}
				}
				
				}catch(Exception e) {
					e.printStackTrace();
					logger.error("exception in OwnerImg save"+e);
					System.out.println("exception in OwnerImg save"+e);
				}
				
								
					//ownImg.setOwnId(temp);
					ownImg.setbCode(currentUser.getUserBranchCode());
					ownImg.setUserCode(currentUser.getUserCode());
					System.out.println("ownerImageId="+ownImg.getOwnImgId()+"ownerID="+owner.getOwnId());
					 session.update(ownImg);
					 /*if(ownImgId > 0){
						logger.info("OwnerImg is saved !");
						owner.setOwnImgId(ownImgId);
					*/	if(panImg != null){
							owner.setOwnIsPanImg(true);
						}
						if(decImg != null){
							owner.setOwnIsDecImg(true);
						}
						
						if(chqImg != null){
							owner.setOwnIsChqImg(true);
						}
					/*}else{
						logger.info("OwnerImg is not saved !");
					}*/
				
			}else {
				
				OwnerImg ownImg = new OwnerImg();
				try {
					
				
				if (panImg != null && decImg != null) {
					
					
					ownImg.setOwnPanImgPath(ownerPanImgPath+"/"+"own"+temp+".pdf");
					byte ownerPanImgInBytes[] = panImg.getBytes(1, (int)panImg.length());
					File panFile = new File(ownImg.getOwnPanImgPath());
					if(panFile.exists())
						panFile.delete();
					if(panFile.createNewFile()){
						OutputStream targetFile=  new FileOutputStream(panFile);
			            targetFile.write(ownerPanImgInBytes);
			            targetFile.close();
					}
					
					
					ownImg.setOwnDecImgPath(ownerDecImgPath+"/"+"own"+temp+".pdf");
					byte ownerDecImgInBytes[] = decImg.getBytes(1, (int)decImg.length());
					File decFile = new File(ownImg.getOwnDecImgPath());
					if(decFile.exists())
						decFile.delete();
					if(decFile.createNewFile()){											    
						OutputStream targetFile=  new FileOutputStream(decFile);
			            targetFile.write(ownerDecImgInBytes);		            
			            targetFile.close();	
					}
					
					owner.setOwnPanIntRt(0);
				
					
				} else if (panImg != null ) {
					
					ownImg.setOwnPanImgPath(ownerPanImgPath+"/"+"own"+temp+".pdf");
					byte ownerPanImgInBytes[] = panImg.getBytes(1, (int)panImg.length());
					File panFile = new File(ownImg.getOwnPanImgPath());
					if(panFile.exists())
						panFile.delete();
					if(panFile.createNewFile()){											    
						OutputStream targetFile=  new FileOutputStream(panFile);
			            targetFile.write(ownerPanImgInBytes);			            
			            targetFile.close();	
					}
					
					
					if (owner.getOwnPanNo().charAt(3) == 'P' ||	owner.getOwnPanNo().charAt(3) == 'H' ||	owner.getOwnPanNo().charAt(3) == 'J') {
						owner.setOwnPanIntRt(1);
					}else {
						owner.setOwnPanIntRt(2);
					}
				}
				
				if(chqImg != null) {
					ownImg.setOwnBnkDetImgPath(ownerBankDetImgPath+"/"+"own"+temp+".pdf");
					byte ownerChqImgInBytes[] = chqImg.getBytes(1, (int)chqImg.length());
					File chqFile = new File(ownImg.getOwnBnkDetImgPath());
					if(chqFile.exists())
						chqFile.delete();
					if(chqFile.createNewFile()){											    
						OutputStream targetFile=  new FileOutputStream(chqFile);
			            targetFile.write(ownerChqImgInBytes);			            
			            targetFile.close();	
					}
					owner.setOwnBnkDet(true);
				}
				
				}catch(Exception e) {
					e.printStackTrace();
					logger.error("exception in OwnerImg save"+e);
					System.out.println("exception in OwnerImg save"+e);
				}
				
								
					ownImg.setOwnId(owner.getOwnId());
					ownImg.setbCode(currentUser.getUserBranchCode());
					ownImg.setUserCode(currentUser.getUserCode());
					int ownImgId = (Integer) session.save(ownImg);
					if(ownImgId > 0){
						logger.info("OwnerImg is saved !");
						owner.setOwnImgId(ownImgId);
						if(panImg != null){
							owner.setOwnIsPanImg(true);
						}
						if(decImg != null){
							owner.setOwnIsDecImg(true);
						}
						
						if(chqImg != null){
							owner.setOwnIsChqImg(true);
						}
						
					}else{
						logger.info("OwnerImg is not saved !");
					}
				
			}
			
			session.merge(owner);
			//session.flush();
			//session.clear();
			System.out.println("owner Data is inserted successfully");
		return temp;
	}
	
	
	
	@Override
	public void updateOwner(Session session,Owner owner){
		logger.info("Enter into updateOwner()");
		
			session.update(owner);
			
		logger.info("Exit from updateOwner()");
	}
	
	@Override
	public void updateOwnImg(Session session,OwnerImg ownImg){
		logger.info("Enter into updateOwnerImg()");
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);		
			
			ownImg.setCreationTS(calendar);
			session.saveOrUpdate(ownImg);
			
		logger.info("Exit from updateOwnerImg()");
	}
	
	
	@Override
	public int saveOwnImg(Session session,OwnerImg ownImg){
		logger.info("Enter into updateOwnerImg()");
		int temp=0;
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);		
			
			ownImg.setCreationTS(calendar);
			temp=(Integer) session.save(ownImg);
			
		logger.info("Exit from updateOwnerImg()");
		return temp;
	}
	
	
	
	
	@Override
	public List<Owner> getOwnFrBnkVerify() {
		
		List<Owner> ownList = new ArrayList<>();
		Session session=null;
		try {
			session = this.sessionFactory.openSession();
			ownList = session.createCriteria(Owner.class)
					.add(Restrictions.eq(OwnerCNTS.IS_OWN_BNK_DET, true))
					.add(Restrictions.eq(OwnerCNTS.OWN_VALID_BNK_DET,false))
					.add(Restrictions.eq(OwnerCNTS.OWN_INVALID_BNK_DET,false)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return ownList;
	}
	
	
	@Override
	public List<Owner> getOwnerInvalidAcc() {
		
		List<Owner> ownList = new ArrayList<>();
		Session session=null;
		try {
			session = this.sessionFactory.openSession();
			ownList = session.createCriteria(Owner.class)
					//.add(Restrictions.eq(OwnerCNTS.IS_OWN_BNK_DET, true))
					.add(Restrictions.eq(OwnerCNTS.OWN_INVALID_BNK_DET,true)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return ownList;
	}
	
	@Override
	public Map<String,String> bnkInfoValid(Integer ownId){
		Map<String,String> map=new HashMap<>();
		User user=(User) httSession.getAttribute("currentUser");
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			Owner own=(Owner) session.get(Owner.class, ownId);
			own.setOwnBnkDetValid(true);
			own.setOwnBnkDetInValid(false);
			own.setOwnBnkDetVerifyBy(user.getUserCode());
			session.update(own);
			session.flush();
			session.clear();
			transaction.commit();
			
			map.put("result", "success");
		}catch (Exception e) {
			transaction.rollback();
			e.printStackTrace();
			map.put("result", "error");
			// TODO: handle exception
		}finally {
			session.close();
		}
		
		
		return map;
	}
	
	
	
	@Override
	public Map<String,String> bnkInfoInValid(int ownId){
		Map<String,String> map=new HashMap<>();
		User user=(User) httSession.getAttribute("currentUser");
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			Owner own=(Owner) session.get(Owner.class, ownId);
			own.setOwnBnkDetValid(false);
			own.setOwnBnkDetInValid(true);
			own.setOwnBnkDetVerifyBy(user.getUserCode());
			session.update(own);
			session.flush();
			session.clear();
			transaction.commit();
			
			map.put("result", "success");
		}catch (Exception e) {
			transaction.rollback();
			e.printStackTrace();
			map.put("result", "error");
			// TODO: handle exception
		}finally {
			session.close();
		}
		
		
		return map;
	}
	
	
	
	@Override
	public Map<String,String> bnkInfoUpdate(Owner newOwn){
		Map<String,String> map=new HashMap<>();
		User user=(User) httSession.getAttribute("currentUser");
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			Owner own=(Owner) session.get(Owner.class, newOwn.getOwnId());
			own.setOwnAccntNo(newOwn.getOwnAccntNo());
			own.setOwnAcntHldrName(newOwn.getOwnAcntHldrName());
			own.setOwnBnkBranch(newOwn.getOwnBnkBranch());
			own.setOwnIfsc(newOwn.getOwnIfsc());
			own.setOwnBnkDetValid(true);
			own.setOwnBnkDet(true);
			own.setOwnBnkDetInValid(false);
			own.setOwnBnkDetVerifyBy(user.getUserCode());
			session.update(own);
			session.flush();
			session.clear();
			transaction.commit();
			
			map.put("result", "success");
		}catch (Exception e) {
			transaction.rollback();
			e.printStackTrace();
			map.put("result", "error");
			// TODO: handle exception
		}finally {
			session.close();
		}
		
		return map;
	}
	
	
	
	@Override
	public Map<String,String> uploadChqImg(byte fileInBytes[],int ownId){
		Map<String, String> map=new HashMap<>();
		User crnUser=(User) httSession.getAttribute("currentUser");
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			
			
			Path path = Paths.get(ownerBankDetImgPath);
			if(! Files.exists(path))
				Files.createDirectories(path);//make new directory if not exist
			
			
			Owner own=(Owner) session.get(Owner.class, ownId);
			if(own != null) {
				OwnerImg ownImg=(OwnerImg) session.get(OwnerImg.class, own.getOwnImgId());
				
				if(ownImg!=null) {
					if(ownImg.getOwnBnkDetImgPath()!=null) {
						File chqFile = new File(ownImg.getOwnBnkDetImgPath());										
						if(chqFile.exists())
							chqFile.delete();
						
						FileOutputStream out = new FileOutputStream(chqFile);
						out.write(fileInBytes);
						out.close();
						//ownImg.setOwnBnkDetImgPath(ownerBankDetImgPath+"/own"+ownId+".pdf");
						ownImg.setUserCode(crnUser.getUserCode());
						session.update(ownImg);
					}else {
						File chqFile = new File(ownerBankDetImgPath+"/own"+ownId+".pdf");										
						if(chqFile.exists())
							chqFile.delete();
						
						FileOutputStream out = new FileOutputStream(chqFile);
						out.write(fileInBytes);
						out.close();
						
						ownImg.setOwnBnkDetImgPath(ownerBankDetImgPath+"/own"+ownId+".pdf");
						ownImg.setUserCode(crnUser.getUserCode());
						session.update(ownImg);
					}
					
					
				}else {
					
					OwnerImg ownImgN=new OwnerImg();
					File chqFile = new File(ownerBankDetImgPath+"/own"+ownId+".pdf");										
					if(chqFile.exists())
						chqFile.delete();
					
					FileOutputStream out = new FileOutputStream(chqFile);
					out.write(fileInBytes);
					out.close();
					ownImgN.setbCode(crnUser.getUserBranchCode());
					ownImgN.setOwnId(ownId);
					ownImgN.setUserCode(crnUser.getUserCode());
					
					ownImgN.setOwnBnkDetImgPath(ownerBankDetImgPath+"/own"+ownId+".pdf");
					int ownImgId=(int) session.save(ownImgN);
					
					own.setOwnImgId(ownImgId);
					
				}
				
			}
			own.setOwnIsChqImg(true);
			session.update(own);
			session.flush();
			session.clear();
			transaction.commit();
			
			map.put("result", "success");
			map.put("msg", "success");
		}catch(Exception e) {
			e.printStackTrace();
			transaction.rollback();
			map.put("result", "error");
			map.put("msg", "There is some problem to upload file");
			map.put("exp", "Exception="+e);
		}finally {
			session.close();
		}
		
		
		return map;
	}
	
	
	
	@Override
	public Map<String,String> uploadPanImg(byte fileInBytes[],int ownId){
		Map<String, String> map=new HashMap<>();
		User crnUser=(User) httSession.getAttribute("currentUser");
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			
			
			Path path = Paths.get(ownerPanImgPath);
			if(! Files.exists(path))
				Files.createDirectories(path);//make new directory if not exist
			
			
			Owner own=(Owner) session.get(Owner.class, ownId);
			if(own != null) {
				OwnerImg ownImg=(OwnerImg) session.get(OwnerImg.class, own.getOwnImgId());
				
				if(ownImg!=null) {
					if(ownImg.getOwnPanImgPath()!=null) {
						File panFile = new File(ownImg.getOwnPanImgPath());										
						if(panFile.exists())
							panFile.delete();
						
						FileOutputStream out = new FileOutputStream(panFile);
						out.write(fileInBytes);
						out.close();
						//ownImg.setOwnBnkDetImgPath(ownerBankDetImgPath+"/own"+ownId+".pdf");
						ownImg.setUserCode(crnUser.getUserCode());
						session.update(ownImg);
					}else {
						File panFile = new File(ownerPanImgPath+"/own"+ownId+".pdf");										
						if(panFile.exists())
							panFile.delete();
						
						FileOutputStream out = new FileOutputStream(panFile);
						out.write(fileInBytes);
						out.close();
						
						ownImg.setOwnPanImgPath(ownerPanImgPath+"/own"+ownId+".pdf");
						ownImg.setUserCode(crnUser.getUserCode());
						session.update(ownImg);
					}
					
					
				}else {
					
					OwnerImg ownImgN=new OwnerImg();
					File panFile = new File(ownerPanImgPath+"/own"+ownId+".pdf");										
					if(panFile.exists())
						panFile.delete();
					
					FileOutputStream out = new FileOutputStream(panFile);
					out.write(fileInBytes);
					out.close();
					ownImgN.setbCode(crnUser.getUserBranchCode());
					ownImgN.setOwnId(ownId);
					ownImgN.setUserCode(crnUser.getUserCode());
					
					ownImgN.setOwnPanImgPath(ownerPanImgPath+"/own"+ownId+".pdf");
					int ownImgId=(int) session.save(ownImgN);
					
					own.setOwnImgId(ownImgId);
					
				}
				
			}
			own.setOwnIsPanImg(true);
			session.update(own);
			session.flush();
			session.clear();
			transaction.commit();
			
			map.put("result", "success");
			map.put("msg", "success");
		}catch(Exception e) {
			e.printStackTrace();
			transaction.rollback();
			map.put("result", "error");
			map.put("msg", "There is some problem to upload file");
			map.put("exp", "Exception="+e);
		}finally {
			session.close();
		}
		
		return map;
	}
	
	
	@Override
	public Map<String,String> uploadDecImg(byte fileInBytes[],int ownId){
		Map<String, String> map=new HashMap<>();
		User crnUser=(User) httSession.getAttribute("currentUser");
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			
			
			Path path = Paths.get(ownerDecImgPath);
			if(! Files.exists(path))
				Files.createDirectories(path);//make new directory if not exist
			
			
			Owner own=(Owner) session.get(Owner.class, ownId);
			if(own != null) {
				OwnerImg ownImg=(OwnerImg) session.get(OwnerImg.class, own.getOwnImgId());
				
				if(ownImg!=null) {
					if(ownImg.getOwnDecImgPath()!=null) {
						File decFile = new File(ownImg.getOwnDecImgPath());										
						if(decFile.exists())
							decFile.delete();
						
						FileOutputStream out = new FileOutputStream(decFile);
						out.write(fileInBytes);
						out.close();
						//ownImg.setOwnBnkDetImgPath(ownerBankDetImgPath+"/own"+ownId+".pdf");
						ownImg.setUserCode(crnUser.getUserCode());
						session.update(ownImg);
					}else {
						File decFile = new File(ownerDecImgPath+"/own"+ownId+".pdf");										
						if(decFile.exists())
							decFile.delete();
						
						FileOutputStream out = new FileOutputStream(decFile);
						out.write(fileInBytes);
						out.close();
						
						ownImg.setOwnDecImgPath(ownerDecImgPath+"/own"+ownId+".pdf");
						ownImg.setUserCode(crnUser.getUserCode());
						session.update(ownImg);
					}
					
					
				}else {
					
					OwnerImg ownImgN=new OwnerImg();
					File decFile = new File(ownerDecImgPath+"/own"+ownId+".pdf");										
					if(decFile.exists())
						decFile.delete();
					
					FileOutputStream out = new FileOutputStream(decFile);
					out.write(fileInBytes);
					out.close();
					ownImgN.setbCode(crnUser.getUserBranchCode());
					ownImgN.setOwnId(ownId);
					ownImgN.setUserCode(crnUser.getUserCode());
					
					ownImgN.setOwnDecImgPath(ownerDecImgPath+"/own"+ownId+".pdf");
					int ownImgId=(int) session.save(ownImgN);
					
					own.setOwnImgId(ownImgId);
					
				}
				
			}
			own.setOwnIsDecImg(true);
			own.setOwnPanIntRt(0);
			session.update(own);
			session.flush();
			session.clear();
			transaction.commit();
			
			map.put("result", "success");
			map.put("msg", "success");
		}catch(Exception e) {
			e.printStackTrace();
			transaction.rollback();
			map.put("result", "error");
			map.put("msg", "There is some problem to upload file");
			map.put("exp", "Exception="+e);
		}finally {
			session.close();
		}
		
		return map;
	}


}

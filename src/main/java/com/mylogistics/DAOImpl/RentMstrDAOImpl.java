package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.xmlbeans.impl.xb.xsdschema.RestrictionDocument.Restriction;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.mylogistics.DAO.RentMstrDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.constants.EmployeeCNTS;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.model.Address;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.model.Employee;
import com.mylogistics.model.RentDetail;
import com.mylogistics.model.RentMstr;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.lhpv.LhpvBal;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.RentService;

public class RentMstrDAOImpl implements RentMstrDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	HttpSession httpSession;
	
	@Autowired
	public RentMstrDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public int saveRentMstr(RentService rentService, User currentUser) {
		
		int temp = 0;
		
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();

			Criteria criteria=session.createCriteria(Branch.class);
			criteria.add(Restrictions.eq(BranchCNTS.BRANCH_ID, rentService.getBranchId()));
			Branch branch  = (Branch) criteria.list().get(0);
			
			System.out.println("No Employee Selected: "+rentService.getEmpId());
			
			Employee employee = null;
			
			if (rentService.getEmpId() > 0) {
				criteria = session.createCriteria(Employee.class);
				criteria.add(Restrictions.eq(EmployeeCNTS.EMP_ID, rentService.getEmpId()));
				employee = (Employee) criteria.list().get(0);
			} 
			
			
			Address address = rentService.getAddress();
			address.setbCode(currentUser.getUserBranchCode());
			address.setUserCode(currentUser.getUserCode());
			address.setAddType("Rent Address");
			
			RentMstr rentMstr = rentService.getRentMstr();
			rentMstr.setbCode(currentUser.getUserBranchCode());
			rentMstr.setUserCode(currentUser.getUserCode());
			rentMstr.setAddress(address);
			rentMstr.setEmployee(employee);
			rentMstr.setBranch(branch);
			
			branch.getRentMstrList().add(rentMstr);
			
			session.update(branch);
			
			transaction.commit();

			temp = 1;
		}catch(Exception e){
			e.printStackTrace();
			temp = -1;

		}
		session.clear();
		session.close();
		
		return temp;
	}
	
	

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String,Object>> getRentMstrInfo(){
		System.out.println("enter into getRentMstrInfo function");
		List<Map<String,Object>> rentList = new ArrayList<>();
		List<RentMstr> rentMstrList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();

			Criteria cr=session.createCriteria(RentMstr.class);
			rentMstrList = cr.list();
			
			if(!rentMstrList.isEmpty()){
				for(int i=0;i<rentMstrList.size();i++){
					Map<String,Object> map = new HashMap<String, Object>();
					map.put(ConstantsValues.RM_ID,rentMstrList.get(i).getRentMId());
					map.put(ConstantsValues.RM_FOR,rentMstrList.get(i).getRentMFor());
					map.put(ConstantsValues.RM_LL,rentMstrList.get(i).getRentMLandLordName());
					map.put(ConstantsValues.BRH_ID,rentMstrList.get(i).getBranch().getBranchId());
					map.put(ConstantsValues.BRH_FA_CODE,rentMstrList.get(i).getBranch().getBranchFaCode());
					map.put(ConstantsValues.BRH_NAME,rentMstrList.get(i).getBranch().getBranchName());
					Employee emp = rentMstrList.get(i).getEmployee();
					if(emp != null){
						map.put(ConstantsValues.EMP_ID,emp.getEmpId());
						map.put(ConstantsValues.EMP_FA_CODE,emp.getEmpFaCode());
						map.put(ConstantsValues.EMP_NAME,emp.getEmpName());
					}
					rentList.add(map);
				}
			}
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return rentList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RentMstr> getAllRentMstrByBranchId(int branchId) {
		// TODO Auto-generated method stub
		Session session=null;
		List<RentMstr> rentMstrs=new ArrayList<>();
		try {
			session=this.sessionFactory.openSession();
			SQLQuery query=session.createSQLQuery("select * from rentmstr where branchId="+branchId);
			query.addEntity(RentMstr.class);
			rentMstrs=query.list();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		finally {
			session.close();
		}
		return rentMstrs;
	}
	
	
	@Override
	public boolean updateRentMstr(RentMstr mstr) {
		// TODO Auto-generated method stub
		Session session=null;
		Transaction transaction=null;
		boolean b=false;
		try {
			session=this.sessionFactory.openSession();
			transaction=session.beginTransaction();
			System.out.println(mstr.getUserCode());
			System.out.println(mstr.getAddress().getUserCode());
			session.update(mstr);
			transaction.commit();
			b=true;
		}catch (Exception e) {
			// TODO:handle exception
			b=false;
			transaction.rollback();
		
			e.printStackTrace();
		}finally {
			session.close();
		}
		return b;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String,Object>> getRentMstrForN(){
		System.out.println("enter into getRentMstrForN function");
		List<Map<String,Object>> rentList = new ArrayList<>();
		List<RentMstr> rentMstrList = new ArrayList<>();
		Session session=null;
		try{
			session = this.sessionFactory.openSession();
//			transaction = session.beginTransaction();
            Calendar calendar=Calendar.getInstance();
            calendar.set(Calendar.DATE, 01);
           Date startDate=new Date(calendar.getTimeInMillis());
           calendar.add(Calendar.MONTH, 1);
           Date afterDate=new Date(calendar.getTimeInMillis());
           System.out.println("Start Date  "+startDate);
           System.out.println("After Date  "+afterDate);
			Criteria criteria=session.createCriteria(RentDetail.class);
			//criteria.add(Restrictions.eq("isUploaded", false));
			criteria.add(Restrictions.ge("rdDate", startDate));
			criteria.add(Restrictions.lt("rdDate", afterDate));
			List<RentDetail> details=criteria.list();
			if(details.isEmpty()) {
				Criteria cr=session.createCriteria(RentMstr.class)
						.add(Restrictions.isNotNull("rentAcNo"));
				rentMstrList = cr.list();
			}else {
				List<Integer> rmIdList=new ArrayList<>();
				for(RentDetail rentDetail:details) {
					rmIdList.add(rentDetail.getRentMstrId());
				}
				Criteria cr=session.createCriteria(RentMstr.class);
				cr.add(Restrictions.not(Restrictions.in("rentMId", rmIdList)))
				.add(Restrictions.isNotNull("rentAcNo"));
				rentMstrList = cr.list();
			}
			
			
			if(!rentMstrList.isEmpty()){
				for(int i=0;i<rentMstrList.size();i++){
					Map<String,Object> map = new HashMap<String, Object>();
					map.put(ConstantsValues.RM_ID,rentMstrList.get(i).getRentMId());
					map.put(ConstantsValues.RM_FOR,rentMstrList.get(i).getRentMFor());
					map.put(ConstantsValues.RM_LL,rentMstrList.get(i).getRentMLandLordName());
					map.put(ConstantsValues.BRH_ID,rentMstrList.get(i).getBranch().getBranchId());
					map.put(ConstantsValues.BRH_FA_CODE,rentMstrList.get(i).getBranch().getBranchFaCode());
					map.put(ConstantsValues.BRH_NAME,rentMstrList.get(i).getBranch().getBranchName());
					map.put("rentPM", rentMstrList.get(i).getRentMRentPM());
					Employee emp = rentMstrList.get(i).getEmployee();
					if(emp != null){
						map.put(ConstantsValues.EMP_ID,emp.getEmpId());
						map.put(ConstantsValues.EMP_FA_CODE,emp.getEmpFaCode());
						map.put(ConstantsValues.EMP_NAME,emp.getEmpName());
					}
					rentList.add(map);
				}
			}
			
	//		session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return rentList;
	}





	@Override
	public int saveRentDetail(RentDetail rentDetail) {
		// TODO Auto-generated method stub
		Session session=null;
		Transaction transaction=null;
		User currentUser=(User) httpSession.getAttribute("currentUser");
		int i=0;
		try {
			session=this.sessionFactory.openSession();
			transaction =session.beginTransaction();
			i=(Integer)session.save(rentDetail);
			
			if(i>0){
				
				//TODO save rent into cashstmt
				//for csTvNo
				Date date = new Date(new java.util.Date().getTime());
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(date);
				//logger.info("unique id =====>" + calendar.getTimeInMillis());
				long milliSec = calendar.getTimeInMillis();
				String tvNo = String.valueOf(milliSec);
				
				//getFrom branch
				Branch fromBranch=(Branch) session.get(Branch.class,Integer.parseInt(rentDetail.getFromBranch()));
				//getRentMstr 
				RentMstr rentMstr=(RentMstr) session.get(RentMstr.class, rentDetail.getRentMstrId());
				//getToBranch
				Branch toBranch=rentMstr.getBranch();
				System.out.println("TOBranchCode"+toBranch.getBranchCode());
				
				String payMode = "O";
				//from branch css
				CashStmtStatus cashStmtStatus ;
				@SuppressWarnings("unchecked")
				List<CashStmtStatus> cssList=session.createCriteria(CashStmtStatus.class)
						.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT, rentDetail.getRdDate()))
						.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, rentDetail.getFromBranch())).list();
				
				System.out.println("csslistSize"+cssList.size());
				if(!cssList.isEmpty()){
					int vchrNo=0;
					cashStmtStatus=cssList.get(0);
					//voucher no.get
					if(!cashStmtStatus.getCashStmtList().isEmpty()){
						for(int p=0;p<cashStmtStatus.getCashStmtList().size();p++){
							if(vchrNo < cashStmtStatus.getCashStmtList().get(p).getCsVouchNo()){
								vchrNo=cashStmtStatus.getCashStmtList().get(p).getCsVouchNo();
							}
						}
						vchrNo=vchrNo+1;
					}else{
						vchrNo=1;
					}
					
					
					//bankFaCode
					String bankCode = rentDetail.getBankFaCode();
					Criteria cr=session.createCriteria(BankMstr.class);
					cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
					List<BankMstr> bankMstrList = cr.list();
					
					if(!bankMstrList.isEmpty()){
						BankMstr bankMstr = bankMstrList.get(0);
						
						cr=session.createCriteria(BankMstr.class);
						cr.add(Restrictions.eq(BankMstrCNTS.BNK_ID,bankMstr.getBnkId()));
						@SuppressWarnings("unchecked")
						List<BankMstr> bnkList = cr.list();
						
						BankMstr bank = bnkList.get(0);
						double balAmt = bank.getBnkBalanceAmt();
						double newBalAmt = balAmt - (rentDetail.getNetAmt());
						bank.setBnkBalanceAmt(newBalAmt);
						session.merge(bank);
						
						//main cashStmt entry for bank start
						CashStmt mainCashStmt = new CashStmt();
						mainCashStmt.setbCode(fromBranch.getBranchCode());
						mainCashStmt.setUserCode(currentUser.getUserCode());
						mainCashStmt.setCsDescription(rentDetail.getRentDesc());
						mainCashStmt.setCsDrCr('C');
						mainCashStmt.setCsAmt(rentDetail.getNetAmt());
						mainCashStmt.setCsType("Rent");
						mainCashStmt.setCsVouchType("bank");
						mainCashStmt.setCsVouchNo(vchrNo);
						//mainCashStmt.setCsPayTo(voucherService.getPayTo());
						mainCashStmt.setCsFaCode(bankCode);
						mainCashStmt.setCsTvNo(tvNo);
						mainCashStmt.setCsDt(rentDetail.getRdDate());
						mainCashStmt.setPayMode(payMode);
						
							mainCashStmt.setCsNo(cashStmtStatus);
							cashStmtStatus.getCashStmtList().add(mainCashStmt);
							double rentAmt = rentDetail.getRentAmt();
							String destBrFaCode = toBranch.getBranchFaCode();
							double tdsAmt = rentDetail.getTdsAmt();
							
							if(fromBranch.getBranchFaCode().equalsIgnoreCase(destBrFaCode)){
								//logger.info("*********Same Branch Case*********");
								
								CashStmt cashStmt = new CashStmt();
								cashStmt.setbCode(fromBranch.getBranchCode());
								cashStmt.setUserCode(currentUser.getUserCode());
								cashStmt.setCsDescription(rentDetail.getRentDesc());
								cashStmt.setCsDrCr('D');
								cashStmt.setCsAmt(rentAmt);
								cashStmt.setCsType("Rent");
								cashStmt.setCsVouchType("bank");
								cashStmt.setCsFaCode("1200301");
								cashStmt.setCsTvNo(tvNo);
								cashStmt.setCsVouchNo(vchrNo);
								//cashStmt.setCsSFId(rmId);								
								cashStmt.setCsDt(rentDetail.getRdDate());
								cashStmt.setPayMode(payMode);
								cashStmt.setCsNo(cashStmtStatus);
									cashStmtStatus.getCashStmtList().add(cashStmt);
								
								if( tdsAmt > 0){
									
									CashStmt cashStmt1 = new CashStmt();
									cashStmt1.setbCode(fromBranch.getBranchCode());
									cashStmt1.setUserCode(currentUser.getUserCode());
									cashStmt1.setCsDescription(rentDetail.getRentDesc());
									cashStmt1.setCsDrCr('C');
									cashStmt1.setCsAmt(tdsAmt);
									cashStmt1.setCsType("Rent");
									cashStmt1.setCsVouchType("bank");
									cashStmt1.setCsFaCode("1110062");
									cashStmt1.setCsTvNo(tvNo);
									cashStmt1.setCsVouchNo(vchrNo);
									//cashStmt1.setCsSFId(rmId);
									cashStmt1.setCsDt(rentDetail.getRdDate());
									cashStmt1.setPayMode(payMode);
									
									cashStmt1.setCsNo(cashStmtStatus);
										cashStmtStatus.getCashStmtList().add(cashStmt1);
								}
								session.update(cashStmtStatus);//new
								
							}else{
								//logger.info("*********Inter Branch Case********");
								
								CashStmt cashStmt = new CashStmt();
								cashStmt.setbCode(fromBranch.getBranchCode());
								cashStmt.setUserCode(currentUser.getUserCode());
								cashStmt.setCsDescription(rentDetail.getRentDesc());
								cashStmt.setCsDrCr('D');
								cashStmt.setCsAmt(rentDetail.getNetAmt());
								cashStmt.setCsType("Rent");
								cashStmt.setCsVouchType("bank");
								cashStmt.setCsFaCode(destBrFaCode);
								cashStmt.setCsTvNo(tvNo);
								cashStmt.setCsVouchNo(vchrNo);
								//cashStmt.setCsSFId(rmId);
								cashStmt.setCsDt(rentDetail.getRdDate());
								cashStmt.setPayMode(payMode);
								
									cashStmt.setCsNo(cashStmtStatus);
									cashStmtStatus.getCashStmtList().add(cashStmt);
									session.update(cashStmtStatus);
								
								// new code TODO
								List<Branch> list=session.createCriteria(Branch.class)
										.add(Restrictions.eq(BranchCNTS.BRANCH_FA_CODE, destBrFaCode)).list();
								String bCode=list.get(0).getBranchCode();
								
								List<CashStmtStatus> newCssList=session.createCriteria(CashStmtStatus.class)
										.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, bCode))
										.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT, rentDetail.getRdDate())).list();
								
								
								
								if(!newCssList.isEmpty()){
									 vchrNo=0;
									if(!newCssList.get(0).getCashStmtList().isEmpty()){
										for(int p=0;p<newCssList.get(0).getCashStmtList().size();p++){
											if(vchrNo < newCssList.get(0).getCashStmtList().get(p).getCsVouchNo()){
												vchrNo=newCssList.get(0).getCashStmtList().get(p).getCsVouchNo();
											}
										}
										vchrNo=vchrNo+1;
									}else{
										vchrNo=1;
									}
									
									
								
									CashStmt cashStmta = new CashStmt();
									cashStmta.setbCode(bCode);
									cashStmta.setUserCode(currentUser.getUserCode());
									cashStmta.setCsDescription(rentDetail.getRentDesc());
									cashStmta.setCsDrCr('C');
									cashStmta.setCsAmt(rentDetail.getNetAmt());
									cashStmta.setCsType("Rent");
									cashStmta.setCsVouchType("contra");
									cashStmta.setCsFaCode(rentDetail.getFromBranch());
									cashStmta.setCsTvNo(tvNo);
									cashStmta.setCsVouchNo(vchrNo);
									//cashStmta.setCsSFId(rmId);
									cashStmta.setCsDt(rentDetail.getRdDate());
									cashStmta.setPayMode(payMode);
									
									
									CashStmtStatus csStatus = newCssList.get(0);
									csStatus.getCashStmtList().add(cashStmta);
									cashStmta.setCsNo(csStatus);
									
									
									
									
									CashStmt cashStmtb = new CashStmt();
									cashStmtb.setbCode(bCode);
									cashStmtb.setUserCode(currentUser.getUserCode());
									cashStmtb.setCsDescription(rentDetail.getRentDesc());
									cashStmtb.setCsDrCr('D');
									cashStmtb.setCsAmt(rentDetail.getNetAmt());
									cashStmtb.setCsType("Rent");
									cashStmtb.setCsVouchType("contra");
									cashStmtb.setCsFaCode("1200301");
									cashStmtb.setCsTvNo(tvNo);
									cashStmtb.setCsVouchNo(vchrNo);
									//cashStmtb.setCsSFId(rmId);
									cashStmtb.setCsDt(rentDetail.getRdDate());
									cashStmtb.setPayMode(payMode);
									
									
									cashStmtb.setCsNo(csStatus);
									csStatus.getCashStmtList().add(cashStmtb);
									
									
									if(tdsAmt > 0){
										
										CashStmt cashStmt1 = new CashStmt();
										cashStmt1.setbCode(bCode);
										cashStmt1.setUserCode(currentUser.getUserCode());
										cashStmt1.setCsDescription(rentDetail.getRentDesc());
										cashStmt1.setCsDrCr('C');
										cashStmt1.setCsAmt(tdsAmt);
										cashStmt1.setCsType("Rent");
										cashStmt1.setCsVouchType("contra");
										cashStmt1.setCsFaCode("1110062");
										cashStmt1.setCsTvNo(tvNo);
										cashStmt1.setCsVouchNo(vchrNo);
										//cashStmt1.setCsSFId(rmId);
										cashStmt1.setCsDt(rentDetail.getRdDate());
										cashStmt1.setPayMode(payMode);
										
											cashStmt1.setCsNo(csStatus);
											csStatus.getCashStmtList().add(cashStmt1);
									}
									session.update(csStatus);
									
								}else{
									CashStmtTemp cashStmt1 = new CashStmtTemp();
									cashStmt1.setbCode(rentDetail.getFromBranch());
									cashStmt1.setUserCode(currentUser.getUserCode());
									cashStmt1.setCsDescription(rentDetail.getRentDesc());
									cashStmt1.setCsDrCr('C');
									cashStmt1.setCsType("Rent");
									cashStmt1.setCsTvNo(tvNo);
									cashStmt1.setCsVouchType("contra");
									cashStmt1.setCsAmt(rentDetail.getNetAmt());
									cashStmt1.setCsFaCode(toBranch.getBranchFaCode());
									cashStmt1.setCsDt(rentDetail.getRdDate());
									//cashStmt1.setCsPayTo(voucherService.getPayTo());
									cashStmt1.setCsBrhFaCode(destBrFaCode);
									cashStmt1.setPayMode(payMode);

									session.save(cashStmt1);
									
									CashStmtTemp cashStmt2 = new CashStmtTemp();
									cashStmt2.setbCode(fromBranch.getBranchCode());
									cashStmt2.setUserCode(currentUser.getUserCode());
									cashStmt2.setCsDescription(rentDetail.getRentDesc());
									cashStmt2.setCsDrCr('D');
									cashStmt2.setCsType("Rent");
									cashStmt2.setCsTvNo(tvNo);
									cashStmt2.setCsVouchType("contra");
									cashStmt2.setCsAmt(rentAmt);
									cashStmt2.setCsFaCode("1200301");
									cashStmt2.setCsDt(rentDetail.getRdDate());
									//cashStmt2.setCsPayTo(voucherService.getPayTo());
									cashStmt2.setCsBrhFaCode(destBrFaCode);
									cashStmt2.setPayMode(payMode);

									session.save(cashStmt2);
									
									if(tdsAmt > 0){

										CashStmtTemp cashStmt3 = new CashStmtTemp();
										cashStmt3.setbCode(rentDetail.getFromBranch());
										cashStmt3.setUserCode(currentUser.getUserCode());
										cashStmt3.setCsDescription(rentDetail.getRentDesc());
										cashStmt3.setCsDrCr('C');
										cashStmt3.setCsType("Rent");
										cashStmt3.setCsTvNo(tvNo);
										cashStmt3.setCsVouchType("contra");
										cashStmt3.setCsAmt(tdsAmt);
										cashStmt3.setCsFaCode("1110062");
										cashStmt3.setCsDt(rentDetail.getRdDate());
										//cashStmt3.setCsPayTo(voucherService.getPayTo());
										cashStmt3.setCsBrhFaCode(destBrFaCode);
										cashStmt3.setPayMode(payMode);

										session.save(cashStmt3);

								}
								
						
							  }
							}
						
						
					}else{
						i=0;
					}
					
				}else{
					
					//TODO If from branch cs is not found

					int vchrNo=0;
					
					//bankFaCode
					String bankCode = rentDetail.getBankFaCode();
					Criteria cr=session.createCriteria(BankMstr.class);
					cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
					List<BankMstr> bankMstrList = cr.list();
					
					if(!bankMstrList.isEmpty()){
						
						//main cashStmt entry for bank start
						CashStmtTemp mainCashStmt = new CashStmtTemp();
						mainCashStmt.setbCode(fromBranch.getBranchCode());
						mainCashStmt.setUserCode(currentUser.getUserCode());
						mainCashStmt.setCsDescription(rentDetail.getRentDesc());
						mainCashStmt.setCsDrCr('C');
						mainCashStmt.setCsAmt(rentDetail.getNetAmt());
						mainCashStmt.setCsType("Rent");
						mainCashStmt.setCsVouchType("bank");
						mainCashStmt.setCsVouchNo(vchrNo);
						//mainCashStmt.setCsPayTo(voucherService.getPayTo());
						mainCashStmt.setCsFaCode(bankCode);
						mainCashStmt.setCsTvNo(tvNo);
						mainCashStmt.setCsDt(rentDetail.getRdDate());
						mainCashStmt.setCsBrhFaCode(fromBranch.getBranchFaCode());
						mainCashStmt.setPayMode(payMode);
						session.save(mainCashStmt);
							//int rmId = (int) subFList.get(i).get(ConstantsValues.RM_ID);
							double rentAmt = rentDetail.getRentAmt();
							String destBrFaCode = toBranch.getBranchFaCode();
							//String tdsCode = "1110062";
							double tdsAmt = rentDetail.getTdsAmt();
							
							if(fromBranch.getBranchFaCode().equalsIgnoreCase(destBrFaCode)){
								//logger.info("*********Same Branch Case*********");
								
								CashStmtTemp cashStmt = new CashStmtTemp();
								cashStmt.setbCode(fromBranch.getBranchCode());
								cashStmt.setUserCode(currentUser.getUserCode());
								cashStmt.setCsDescription(rentDetail.getRentDesc());
								cashStmt.setCsDrCr('D');
								cashStmt.setCsAmt(rentAmt);
								cashStmt.setCsType("Rent");
								cashStmt.setCsVouchType("bank");
								cashStmt.setCsFaCode("1200301");
								cashStmt.setCsTvNo(tvNo);
								cashStmt.setCsVouchNo(vchrNo);
								//cashStmt.setCsSFId(rmId);								
								cashStmt.setCsBrhFaCode(destBrFaCode);
								cashStmt.setCsDt(rentDetail.getRdDate());
								cashStmt.setPayMode(payMode);
								session.save(cashStmt);
								
								
								if( tdsAmt > 0){
									
									CashStmtTemp cashStmt1 = new CashStmtTemp();
									cashStmt1.setbCode(fromBranch.getBranchCode());
									cashStmt1.setUserCode(currentUser.getUserCode());
									cashStmt1.setCsDescription(rentDetail.getRentDesc());
									cashStmt1.setCsDrCr('C');
									cashStmt1.setCsAmt(tdsAmt);
									cashStmt1.setCsType("Rent");
									cashStmt1.setCsVouchType("bank");
									cashStmt1.setCsFaCode("1110062");
									cashStmt1.setCsTvNo(tvNo);
									cashStmt1.setCsVouchNo(vchrNo);
									//cashStmt1.setCsSFId(rmId);
									cashStmt1.setCsDt(rentDetail.getRdDate());
									cashStmt1.setPayMode(payMode);
									cashStmt1.setCsBrhFaCode(destBrFaCode);
									session.save(cashStmt1);
								
								}
								
							}else{
								//logger.info("*********Inter Branch Case********");
								
								CashStmtTemp cashStmt = new CashStmtTemp();
								cashStmt.setbCode(fromBranch.getBranchCode());
								cashStmt.setUserCode(currentUser.getUserCode());
								cashStmt.setCsDescription(rentDetail.getRentDesc());
								cashStmt.setCsDrCr('D');
								cashStmt.setCsAmt(rentDetail.getNetAmt());
								cashStmt.setCsType("Rent");
								cashStmt.setCsVouchType("bank");
								cashStmt.setCsFaCode(destBrFaCode);
								cashStmt.setCsTvNo(tvNo);
								cashStmt.setCsVouchNo(vchrNo);
								//cashStmt.setCsSFId(rmId);
								cashStmt.setCsDt(rentDetail.getRdDate());
								cashStmt.setPayMode(payMode);
								cashStmt.setCsBrhFaCode(fromBranch.getBranchFaCode());
								session.save(cashStmt);
								
								
								// new code TODO
								List<Branch> list=session.createCriteria(Branch.class)
										.add(Restrictions.eq(BranchCNTS.BRANCH_FA_CODE, destBrFaCode)).list();
								String bCode=list.get(0).getBranchCode();
								
								List<CashStmtStatus> newCssList=session.createCriteria(CashStmtStatus.class)
										.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, bCode))
										.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT, rentDetail.getRdDate())).list();
								
								
								
								if(!newCssList.isEmpty()){
									 vchrNo=0;
									if(!newCssList.get(0).getCashStmtList().isEmpty()){
										for(int p=0;p<newCssList.get(0).getCashStmtList().size();p++){
											if(vchrNo < newCssList.get(0).getCashStmtList().get(p).getCsVouchNo()){
												vchrNo=newCssList.get(0).getCashStmtList().get(p).getCsVouchNo();
											}
										}
										vchrNo=vchrNo+1;
									}else{
										vchrNo=1;
									}
									
									
								
									CashStmt cashStmta = new CashStmt();
									cashStmta.setbCode(bCode);
									cashStmta.setUserCode(currentUser.getUserCode());
									cashStmta.setCsDescription(rentDetail.getRentDesc());
									cashStmta.setCsDrCr('C');
									cashStmta.setCsAmt(rentDetail.getNetAmt());
									cashStmta.setCsType("Rent");
									cashStmta.setCsVouchType("contra");
									cashStmta.setCsFaCode(rentDetail.getFromBranch());
									cashStmta.setCsTvNo(tvNo);
									cashStmta.setCsVouchNo(vchrNo);
									//cashStmta.setCsSFId(rmId);
									cashStmta.setCsDt(rentDetail.getRdDate());
									cashStmta.setPayMode(payMode);
									
									
									CashStmtStatus csStatus = newCssList.get(0);
									csStatus.getCashStmtList().add(cashStmta);
									cashStmta.setCsNo(csStatus);
									
									
									
									
									CashStmt cashStmtb = new CashStmt();
									cashStmtb.setbCode(bCode);
									cashStmtb.setUserCode(currentUser.getUserCode());
									cashStmtb.setCsDescription(rentDetail.getRentDesc());
									cashStmtb.setCsDrCr('D');
									cashStmtb.setCsAmt(rentDetail.getNetAmt());
									cashStmtb.setCsType("Rent");
									cashStmtb.setCsVouchType("contra");
									cashStmtb.setCsFaCode("1200301");
									cashStmtb.setCsTvNo(tvNo);
									cashStmtb.setCsVouchNo(vchrNo);
									//cashStmtb.setCsSFId(rmId);
									cashStmtb.setCsDt(rentDetail.getRdDate());
									cashStmtb.setPayMode(payMode);
									
									
									cashStmtb.setCsNo(csStatus);
									csStatus.getCashStmtList().add(cashStmtb);
									
									
									if(tdsAmt > 0){
										
										CashStmt cashStmt1 = new CashStmt();
										cashStmt1.setbCode(bCode);
										cashStmt1.setUserCode(currentUser.getUserCode());
										cashStmt1.setCsDescription(rentDetail.getRentDesc());
										cashStmt1.setCsDrCr('C');
										cashStmt1.setCsAmt(tdsAmt);
										cashStmt1.setCsType("Rent");
										cashStmt1.setCsVouchType("contra");
										cashStmt1.setCsFaCode("1110062");
										cashStmt1.setCsTvNo(tvNo);
										cashStmt1.setCsVouchNo(vchrNo);
										//cashStmt1.setCsSFId(rmId);
										cashStmt1.setCsDt(rentDetail.getRdDate());
										cashStmt1.setPayMode(payMode);
										
											cashStmt1.setCsNo(csStatus);
											csStatus.getCashStmtList().add(cashStmt1);
									}
									session.update(csStatus);
									
								}else{
									CashStmtTemp cashStmt1 = new CashStmtTemp();
									cashStmt1.setbCode(rentDetail.getFromBranch());
									cashStmt1.setUserCode(currentUser.getUserCode());
									cashStmt1.setCsDescription(rentDetail.getRentDesc());
									cashStmt1.setCsDrCr('C');
									cashStmt1.setCsType("Rent");
									cashStmt1.setCsTvNo(tvNo);
									cashStmt1.setCsVouchType("contra");
									cashStmt1.setCsAmt(rentDetail.getNetAmt());
									cashStmt1.setCsFaCode(toBranch.getBranchFaCode());
									cashStmt1.setCsDt(rentDetail.getRdDate());
									//cashStmt1.setCsPayTo(voucherService.getPayTo());
									cashStmt1.setCsBrhFaCode(destBrFaCode);
									cashStmt1.setPayMode(payMode);

									session.save(cashStmt1);
									
									CashStmtTemp cashStmt2 = new CashStmtTemp();
									cashStmt2.setbCode(fromBranch.getBranchCode());
									cashStmt2.setUserCode(currentUser.getUserCode());
									cashStmt2.setCsDescription(rentDetail.getRentDesc());
									cashStmt2.setCsDrCr('D');
									cashStmt2.setCsType("Rent");
									cashStmt2.setCsTvNo(tvNo);
									cashStmt2.setCsVouchType("contra");
									cashStmt2.setCsAmt(rentAmt);
									cashStmt2.setCsFaCode("1200301");
									cashStmt2.setCsDt(rentDetail.getRdDate());
									//cashStmt2.setCsPayTo(voucherService.getPayTo());
									cashStmt2.setCsBrhFaCode(destBrFaCode);
									cashStmt2.setPayMode(payMode);

									session.save(cashStmt2);
									
									if(tdsAmt > 0){

										CashStmtTemp cashStmt3 = new CashStmtTemp();
										cashStmt3.setbCode(rentDetail.getFromBranch());
										cashStmt3.setUserCode(currentUser.getUserCode());
										cashStmt3.setCsDescription(rentDetail.getRentDesc());
										cashStmt3.setCsDrCr('C');
										cashStmt3.setCsType("Rent");
										cashStmt3.setCsTvNo(tvNo);
										cashStmt3.setCsVouchType("contra");
										cashStmt3.setCsAmt(tdsAmt);
										cashStmt3.setCsFaCode("1110062");
										cashStmt3.setCsDt(rentDetail.getRdDate());
										//cashStmt3.setCsPayTo(voucherService.getPayTo());
										cashStmt3.setCsBrhFaCode(destBrFaCode);
										cashStmt3.setPayMode(payMode);

										session.save(cashStmt3);

								}
								
						
							  }
							}
						
						
					}else{
						i=0;
					}
					
				}
				
			
			}
			
			transaction.commit();
		}catch (Exception e) {
			// TODO: handle exception
			i=0;
			e.printStackTrace();
			transaction.rollback();
		}finally {
			session.close();
		}
		return i;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public List<RentDetail> getRentDetailFrExl(){
		
		List<RentDetail> rentDetailList=new ArrayList<>();
		Session session=sessionFactory.openSession();
		try{
			rentDetailList=session.createCriteria(RentDetail.class)
					.add(Restrictions.eq("isUploaded", false)).list();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return rentDetailList;
	}
	
	
	
	@Override
	@SuppressWarnings("unchecked")
	public List<RentMstr> getRentMstr(){
		
		List<RentMstr> rentMstrList=new ArrayList<>();
		Session session=sessionFactory.openSession();
		try{
			rentMstrList=session.createCriteria(RentMstr.class).list();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return rentMstrList;
	}
	
	
	
	@Override
	public int updateRentDetail(List<RentDetail> rentDetaillist)
	{
		int i=0;
	Session session=this.sessionFactory.openSession();
	Transaction tx = session.beginTransaction();
	int count = 0;
		try{
			for (RentDetail rd : rentDetaillist) {
				rd.setUploaded(true);
				  session.update(rd); 
				  /* if ( ++count % 50 == 0 ) {
				      session.flush();
				      session.clear();
				   }*/
			}	
			 session.flush();
		      session.clear();
			tx.commit();
			i=1;
		}
		catch(Exception e)
		{
			tx.rollback();
			e.printStackTrace();
			i=0;
		}
		finally
		{
			session.close();
		}
		return i;
		
	}
	
	
}

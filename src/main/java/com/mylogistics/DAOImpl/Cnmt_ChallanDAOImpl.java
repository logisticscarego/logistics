package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.Cnmt_ChallanDAO;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.constants.Cnmt_ChallanCNTS;
import com.mylogistics.constants.DailyContractCNTS;
import com.mylogistics.model.Cnmt_Challan;
import com.mylogistics.model.DailyContract;


public class Cnmt_ChallanDAOImpl implements Cnmt_ChallanDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public Cnmt_ChallanDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int saveCnmt_Challan(Cnmt_Challan cnmt_Challan){
		int id;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 id = (Integer) session.save(cnmt_Challan);
			 transaction.commit();
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
			 id = -1;
		 }
		 session.clear();
		 session.close();
		 return id;	
	}
	
	
	@Override
	public int saveCnmt_Challan(Cnmt_Challan cnmt_Challan,Session session){
		int id;
			 id = (Integer) session.save(cnmt_Challan);
		 return id;	
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Cnmt_Challan> getCnmt_ChallanByCnmtId(int cnmtId){
		List<Cnmt_Challan> cnmt_challanList = new ArrayList<Cnmt_Challan>();
		try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(Cnmt_Challan.class);
			 cr.add(Restrictions.eq("cnmtId",cnmtId));
			 cnmt_challanList =cr.list();
			 transaction.commit();
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		session.clear();
		session.close();
		return cnmt_challanList;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Cnmt_Challan> getCnmt_ChallanByChlnId(int chlnId){
		System.out.println("enter into getCnmt_ChallanByChlnId function");
		List<Cnmt_Challan> cnmt_challanList = new ArrayList<Cnmt_Challan>();
		try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(Cnmt_Challan.class);
			 cr.add(Restrictions.eq(Cnmt_ChallanCNTS.Cnmt_Chln_chlnId,chlnId));
			 cnmt_challanList =cr.list();
			 transaction.commit();
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		session.clear();
		session.close();
		return cnmt_challanList;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Integer> getCnmtIdByChlnId(int chlnId){
		System.out.println("enter into getCnmtIdByChlnId function");
		List<Integer> cnmtIdList = new ArrayList<Integer>();
		try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(Cnmt_Challan.class);
			 ProjectionList projList = Projections.projectionList();
			 cr.add(Restrictions.eq(Cnmt_ChallanCNTS.Cnmt_Chln_chlnId,chlnId));
			 projList.add(Projections.property(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId));
			 cr.setProjection(projList);
			 cnmtIdList =cr.list();
			 transaction.commit();
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		session.clear();
		session.close();
		return cnmtIdList;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Integer> getChlnIdByCnmtId(int cnmtId){
		System.out.println("enter into getChlnIdByCnmtId function = "+cnmtId);
		List<Integer> chlnIdList = new ArrayList<Integer>();
		try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(Cnmt_Challan.class);
			 cr.add(Restrictions.eq(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId,cnmtId));
			 ProjectionList projList = Projections.projectionList();
			 projList.add(Projections.property(Cnmt_ChallanCNTS.Cnmt_Chln_chlnId));
			 cr.setProjection(projList);
			 chlnIdList = cr.list();
			 System.out.println("size of chlnIdList = "+chlnIdList.size());
			 transaction.commit();
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		session.clear();
		session.close();
		return chlnIdList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Cnmt_Challan> getAllCnChln(){
		System.out.println("enter into getAllCnChln funciton");
		List<Cnmt_Challan> ccList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Cnmt_Challan.class);
			ccList = cr.list();
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return ccList;
	}
}

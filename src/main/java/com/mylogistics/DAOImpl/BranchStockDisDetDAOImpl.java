package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.BranchStockDisDetDAO;
import com.mylogistics.constants.BranchStockDisDetCNTS;
import com.mylogistics.model.BranchStockDisDet;
import com.mylogistics.model.MasterStationaryStk;

public class BranchStockDisDetDAOImpl implements BranchStockDisDetDAO{
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	public static Logger logger = Logger.getLogger(BranchStockDisDetDAOImpl.class);
	
	@Autowired
	public BranchStockDisDetDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	 public int saveBranchStockDisDet(BranchStockDisDet branchStockDisDet){
		 System.out.println("enter into saveBranchStockDisDet function --- >");
		 logger.info("Enter into saveBranchStockDisDet()");
		 int temp;
		 //int startNo = branchStockDisDet.getBrsDisDetStartNo();
		   try{
			   session = this.sessionFactory.openSession();
			   transaction =  session.beginTransaction();
			   session.save(branchStockDisDet);			 
			   transaction.commit();
			   temp=1;
			   session.flush();			
		   }
		   catch(Exception e){
			   logger.error("Exception : "+e);
				e.printStackTrace();
				temp=-1;
		   }
		   session.clear();
		   session.close();
		   logger.info("Exit from saveBranchStockDisDet()");
		   return temp;
	}
	 
	 	@Transactional
		@SuppressWarnings("unchecked")
	 	public List<BranchStockDisDet> getBranchStockDisDetData(String userCode){
			List<BranchStockDisDet> bList = new ArrayList<BranchStockDisDet>();
			try{
				session = this.sessionFactory.openSession();
				transaction =  session.beginTransaction();
				Criteria cr=session.createCriteria(BranchStockDisDet.class);
				bList=cr.list();
				transaction.commit();
				session.flush();
			
				}
			catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return bList;
		}
	 	
	 	@Transactional
	 	@SuppressWarnings("unchecked")
        public List<String> getStartNo(String dispatchCode,String status){
            List<BranchStockDisDet> list = new ArrayList<BranchStockDisDet>();
            List<String> cnmtStartNoList = new ArrayList<String>();
            try{
                session = this.sessionFactory.openSession();
                transaction = session.beginTransaction();
                Criteria cr=session.createCriteria(BranchStockDisDet.class);
                cr.add(Restrictions.eq(BranchStockDisDetCNTS.BRSDD_STATUS,status));
                cr.add(Restrictions.eq(BranchStockDisDetCNTS.BRSDD_DIS_CODE,dispatchCode));
                cr.add(Restrictions.eq(BranchStockDisDetCNTS.IS_VERIFY,"no"));
                //list = session.createQuery("from BranchStockDisDet where brsDisDetStatus='chln' and brsDisDetDisCode ='5555555' order by brsDisDetId ").list();
                list = cr.list();
                for(int i=0;i<list.size();i++){
                    cnmtStartNoList.add(list.get(i).getBrsDisDetStartNo());
                }
                session.flush();              
            }
            catch(Exception e){
                e.printStackTrace();
            }
            session.clear();
            session.close();
             return cnmtStartNoList;
        } 	
	 	
	 	/*@Transactional
	 	@SuppressWarnings("unchecked")
        public List<String> getChlnStartNo(String dispatchCode){
            List<BranchStockDisDet> list = new ArrayList<BranchStockDisDet>();
            List<String> chlnStartNoList = new ArrayList<String>();
            try{
                session = this.sessionFactory.openSession();
                transaction = session.beginTransaction();
                Criteria cr=session.createCriteria(BranchStockDisDet.class);
                cr.add(Restrictions.eq(BranchStockDisDetCNTS.BRSDD_STATUS,"chln"));
                cr.add(Restrictions.eq(BranchStockDisDetCNTS.BRSDD_DIS_CODE,dispatchCode));
                cr.add(Restrictions.eq(BranchStockDisDetCNTS.IS_VERIFY,"no"));
                //list = session.createQuery("from BranchStockDisDet where brsDisDetStatus='chln' and brsDisDetDisCode ='5555555' order by brsDisDetId ").list();
                list = cr.list();
                for(int i=0;i<list.size();i++){
                    chlnStartNoList.add(list.get(i).getBrsDisDetStartNo());
                }
                session.flush();
             
            }
            catch(Exception e){
                e.printStackTrace();
            }
           
            session.close();
             return chlnStartNoList;
        } 	
	 	
	 	
	 	@Transactional
	 	@SuppressWarnings("unchecked")
        public List<String> getSedrStartNo(String dispatchCode){
            List<BranchStockDisDet> list = new ArrayList<BranchStockDisDet>();
            List<String> sedrStartNoList = new ArrayList<String>();
            try{
                session = this.sessionFactory.openSession();
                transaction = session.beginTransaction();
                Criteria cr=session.createCriteria(BranchStockDisDet.class);
                cr.add(Restrictions.eq(BranchStockDisDetCNTS.BRSDD_STATUS,"sedr"));
                cr.add(Restrictions.eq(BranchStockDisDetCNTS.BRSDD_DIS_CODE,dispatchCode));
                cr.add(Restrictions.eq(BranchStockDisDetCNTS.IS_VERIFY,"no"));
                //list = session.createQuery("from BranchStockDisDet where brsDisDetStatus='chln' and brsDisDetDisCode ='5555555' order by brsDisDetId ").list();
                list = cr.list();
                for(int i=0;i<list.size();i++){
                    System.out.println("------------sanjnaa---------------"+list.get(i).getBrsDisDetStartNo());
                    sedrStartNoList.add(list.get(i).getBrsDisDetStartNo());
                }
                session.flush();
               
               
            }
            catch(Exception e){
                e.printStackTrace();
            }
            session.close();
             return sedrStartNoList;
        } */
	 	
	 	
	 	@SuppressWarnings("unchecked")
		 @Transactional
		 public int updateisVerify(String startNos[], List<Long> totalStartNo){
			System.out.println("Entr to update is verify in DAO---------------------");
			int temp;
			 try{
				 session = this.sessionFactory.openSession();
				 List<MasterStationaryStk> mstrList = session.createCriteria(MasterStationaryStk.class)
						 .add(Restrictions.in("mstrStnStkStartNo", totalStartNo))
						 .list();
				 if(! mstrList.isEmpty()){
					 Iterator<MasterStationaryStk> it = mstrList.iterator();
					 while(it.hasNext()){
						 MasterStationaryStk stk = (MasterStationaryStk) it.next();
						 stk.setIsReceived("yes");
						 session.update(stk);
					 }
				 }
				 
				 
				 for(int i=0;i<startNos.length;i++){
					 System.out.println("The values inside array is ------"+startNos[i]);
					 transaction = session.beginTransaction();
					 BranchStockDisDet branchStockDisDet = new BranchStockDisDet();
			 		 List<BranchStockDisDet> list = new ArrayList<BranchStockDisDet>();
			 		 Criteria cr=session.createCriteria(BranchStockDisDet.class);
					 cr.add(Restrictions.eq(BranchStockDisDetCNTS.BRSDD_ST_NO,startNos[i]));
					 list =cr.list();
					 branchStockDisDet = list.get(0);
					 branchStockDisDet.setIsVerify("yes");
					 session.update(branchStockDisDet);
					 transaction.commit();
				 	}
				 session.flush();
				
				 temp = 1;
				 }
			 
			 catch(Exception e){
				 e.printStackTrace();
				 temp = -1;
			 }
			 session.clear();
			 session.close();
			 return temp;
		 }

		@Override
		@Transactional
		public boolean isStAlreadyOrdered(List<Long> cnmtList, List<Long> chlnList, List<Long> sedrList) {
			System.out.println("isStAlreadyOrdered() DB");
			
			List<Long> stList = new ArrayList<>();
			
			if (!cnmtList.isEmpty()) {
				stList.addAll(cnmtList);
			}
			if (!chlnList.isEmpty()) {
				stList.addAll(chlnList);
			}
			if (!sedrList.isEmpty()) {
				stList.addAll(sedrList);
			}
			
			boolean stAlreadyOrdered = false;
			
			try {
				session = this.sessionFactory.openSession();
				if (!stList.isEmpty()) {
					for (Long stNo : stList) {
						Query query = session.createQuery("select brsDisDetId from BranchStockDisDet where brsDisDetStartNo = :brsDisDetStartNo");
						query.setString("brsDisDetStartNo", String.valueOf(stNo));
						List<Long> startNoList = query.list();
						if (!startNoList.isEmpty()) {
							stAlreadyOrdered = true;
							break;
						} else {
							stAlreadyOrdered = false;
						}
					}
				} else {
					stAlreadyOrdered = false;
				}
				
				session.flush();
			} catch (Exception e) {
				e.printStackTrace();
				stAlreadyOrdered = false;
			}
			
			session.clear();
			session.close();
			return stAlreadyOrdered;
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
}

package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.RegularContractDAO;
import com.mylogistics.constants.ContToStnCNTS;
import com.mylogistics.constants.RegularContractCNTS;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.Customer;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.Station;

public class RegularContractDAOImpl implements RegularContractDAO {
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public RegularContractDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
		@Transactional
		public int saveRegularContract(RegularContract regularContract){
		   int temp;
			try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 int id = (Integer) session.save(regularContract);
				 transaction.commit();
				 temp =  id;
				 session.flush();
			}catch(Exception e){
				e.printStackTrace();
				temp= -1;
			}
			session.clear();
			session.close();
			return temp;
		}
	
		@SuppressWarnings("unchecked")
		@Transactional
		public List<RegularContract> getRegularContract(String regContCode){
		
			List<RegularContract> result = new ArrayList<RegularContract>();
		
			try{
				session = this.sessionFactory.openSession();
				Criteria cr=session.createCriteria(RegularContract.class);
				cr.add(Restrictions.eq(RegularContractCNTS.REG_CONT_CODE,regContCode));
				result =cr.list();
				session.flush();
			}
		 
			catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return  result; 
		}
	
			@Transactional
			public int updateRegularContract(RegularContract regularContract){
				int temp;
				Date date = new java.sql.Date(new java.util.Date().getTime());
		 		Calendar calendar = Calendar.getInstance();
		 		calendar.setTime(date);
		 		System.out.println("updating-------"+regularContract.getRegContCode()+" "+regularContract.getRegContId());
				try{
					session = this.sessionFactory.openSession();
					Criteria cr=session.createCriteria(Customer.class);
					cr.add(Restrictions.eq(RegularContractCNTS.REG_CONT_CODE,regularContract.getRegContCode()));
					transaction = session.beginTransaction();
					regularContract.setCreationTS(calendar);
					session.saveOrUpdate(regularContract);
					transaction.commit();
					session.flush();
					temp= 1;
					}
				
				catch(Exception e){
					e.printStackTrace();
					temp= -1;
				}
				session.clear();
				session.close();
				return temp;
			}
	
		 @Transactional
		 @SuppressWarnings("unchecked")
		 public List<RegularContract> getRegContractisVerifyNo(){
			 List<RegularContract> result = new ArrayList<RegularContract>();
					 
			 try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria cr=session.createCriteria(RegularContract.class);
				 cr.add(Restrictions.eq(RegularContractCNTS.REG_CONT_IS_VERIFY,"no"));
				 result =cr.list();
				 transaction.commit();
				 session.flush();
			 	}
			
			 catch(Exception e){
				 e.printStackTrace();
			 }
			 session.clear();
			 session.close();
			 
			 return result; 
		 }
		 	@Transactional
	 		@SuppressWarnings("unchecked")
		 	public int getLastRegcontractId(){
	 			List<RegularContract> list = new ArrayList<RegularContract>();
	 			int id=0;
			 
	 			try{
	 				session = this.sessionFactory.openSession();
	 				list = session.createQuery("from RegularContract order by regContId DESC ").setMaxResults(1).list();
	 				id = list.get(0).getRegContId();
	 				session.flush();
			 		}
			 
	 			catch(Exception e){
	 				e.printStackTrace();
	 				}
	 			session.clear();
	 			 session.close();
	 			 return id;
	 		}
	 		
	 		 @Transactional
			 public long totalCount(){
		 		 
		 		 long tempCount = -1; 
		 		 try {
		 			session = this.sessionFactory.openSession();
		 			tempCount = (Long) session.createCriteria(RegularContract.class).setProjection(Projections.rowCount()).uniqueResult();
		 			session.flush();
		 		} catch (Exception e) {
					e.printStackTrace();
				}
		 		session.clear();
	 			session.close();
	 			return tempCount;
			 }
		 
	 		@SuppressWarnings("unchecked")
	 		@Transactional
	 		public List<RegularContract> getRegContractCode(String custCode){
			 
	 			List<RegularContract> regContList = new ArrayList<RegularContract>();
				 
	 			try{
	 				session = this.sessionFactory.openSession();
	 				transaction = session.beginTransaction();
	 				Criteria cr=session.createCriteria(RegularContract.class);
	 				cr.add(Restrictions.eq(RegularContractCNTS.REG_CONT_BLPM_CODE,custCode));
	 				regContList =cr.list();	 
	 				transaction.commit();
	 				 session.flush();
	 			}
			 	
	 			catch(Exception e){
				 e.printStackTrace();
			 	}
	 			session.clear();
				 session.close();
				 return regContList;
	 		}
	 		
	 		@SuppressWarnings("unchecked")
	 		@Transactional
	 		public List<RegularContract> getRegContForCnmt(String custCode ,String cnmtFromSt){
			 
	 			List<RegularContract> regularContractList = new ArrayList<RegularContract>();
				 
	 			try{
	 				session = this.sessionFactory.openSession();
	 				transaction = session.beginTransaction();
	 				Criteria cr=session.createCriteria(RegularContract.class);
	 				cr.add(Restrictions.eq(RegularContractCNTS.REG_CONT_BLPM_CODE,custCode));
	 				cr.add(Restrictions.eq(RegularContractCNTS.REG_CONT_FROM_STATION,cnmtFromSt));
	 				/*cr.add(Restrictions.ge(RegularContractCNTS.REG_CONT_FROM_DT,cnmtDt)); 
	 				cr.add(Restrictions.le(RegularContractCNTS.REG_CONT_TO_DT,cnmtDt));*/
	 				
	 				regularContractList =cr.list();	 
	 				System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%"+regularContractList.size());
	 				for(int i=0;i<regularContractList.size();i++){
	 					System.out.println("Branch code----->"+regularContractList.get(i).getBranchCode());
	 					System.out.println("BLPM code----->"+regularContractList.get(i).getRegContBLPMCode());
	 					System.out.println("from station code----->"+regularContractList.get(i).getRegContFromStation());
	 					System.out.println("To date----->"+regularContractList.get(i).getRegContToDt());
	 					System.out.println("from date----->"+regularContractList.get(i).getRegContFromDt());
	 				}
	 				
	 				transaction.commit();
	 				 session.flush();
	 			}
			 	
	 			catch(Exception e){
				 e.printStackTrace();
			 	}
	 			session.clear();
				 session.close();
				 return regularContractList;
	 		}
	 		
	 		@Transactional
			 @SuppressWarnings("unchecked")
			 public List<RegularContract> getRegularContractData(){
				 List<RegularContract> regularContractsList = new ArrayList<RegularContract>();
				 try{
					 session = this.sessionFactory.openSession();
			         transaction = session.beginTransaction();
			         regularContractsList = session.createCriteria(RegularContract.class).list();
			         transaction.commit();
			         session.flush();
				 }catch (Exception e) {
					 e.printStackTrace();
				 }
				 session.clear();
				 session.close();
				 return regularContractsList;
			}
	 		
	 		@Transactional
			 @SuppressWarnings("unchecked")
			 public List<RegularContract> getRegContractisViewFalse(String branchCode){
				 List<RegularContract> result = new ArrayList<RegularContract>();
						 
				 try{
					 session = this.sessionFactory.openSession();
					 transaction = session.beginTransaction();
					 Criteria cr=session.createCriteria(RegularContract.class);
					 cr.add(Restrictions.eq(RegularContractCNTS.IS_VIEW,false));
					 cr.add(Restrictions.eq(RegularContractCNTS.BRANCH_CODE,branchCode));
					 result =cr.list();
					 transaction.commit();
					 session.flush();
				 	}
				 catch(Exception e){
					 e.printStackTrace();
				 	}
				 session.clear();
				 session.close();
				 return  result; 
			 }
		 	
			 @SuppressWarnings("unchecked")
			 @Transactional
			 public int updateRegContisViewTrue(String code[]){
				int temp;
				Date date = new java.sql.Date(new java.util.Date().getTime());
		 		Calendar calendar = Calendar.getInstance();
		 		calendar.setTime(date);
				 try{
					 session = this.sessionFactory.openSession();
					 for(int i=0;i<code.length;i++){
						transaction = session.beginTransaction();
						RegularContract rc = new RegularContract();
				 		List<RegularContract> list = new ArrayList<RegularContract>();
				 		Criteria cr=session.createCriteria(RegularContract.class);
						cr.add(Restrictions.eq(RegularContractCNTS.REG_CONT_CODE,code[i]));
						list =cr.list();
						rc = list.get(0);
						rc.setView(true);
						rc.setCreationTS(calendar);
						 session.update(rc);
						 transaction.commit();
					 	}
					 session.flush();
					
					 temp = 1;
					 }
				 
				 catch(Exception e){
					 e.printStackTrace();
					 temp = -1;
				 }
				 session.clear();
				 session.close();
				 return temp;
			 }
			 
			 	@Transactional
				@SuppressWarnings("unchecked")
				 public List<String> getRegularContractCode(){
					List<String> regContList = new ArrayList<String>();
					try{
						session = this.sessionFactory.openSession();
						transaction = session.beginTransaction();
						Criteria cr=session.createCriteria(RegularContract.class);
						ProjectionList projList = Projections.projectionList();
						projList.add(Projections.property("regContCode"));
						cr.setProjection(projList);
						regContList = cr.list();
						transaction.commit();
						session.flush();
						}
						catch(Exception e){
							e.printStackTrace();
						}
					session.clear();
						session.close();
						return regContList;
					}
			 	
			 	@Transactional
				 @SuppressWarnings("unchecked")
				 public List<String> getRegularContractCodes(String branchCode){
					 List<String> result = new ArrayList<String>();
							 
					 try{
						 session = this.sessionFactory.openSession();
						 transaction = session.beginTransaction();
						 Criteria cr=session.createCriteria(RegularContract.class);
						 cr.add(Restrictions.eq(RegularContractCNTS.BRANCH_CODE,branchCode));
						 ProjectionList projList = Projections.projectionList();
							projList.add(Projections.property("regContCode"));
							cr.setProjection(projList);
						 result =cr.list();
						 transaction.commit();
						 session.flush();
					 }
					 catch(Exception e){
						 e.printStackTrace();
					 }
					 session.clear();
					 session.close();
					 return  result; 
				 }
			 	
			 	 @SuppressWarnings("unchecked")
				 @Transactional
				 public List<RegularContract> getRegularContractData(int id){
						 List<RegularContract> list = new ArrayList<RegularContract>();
						try{
							 session = this.sessionFactory.openSession();
							 transaction = session.beginTransaction();
							 Criteria cr=session.createCriteria(RegularContract.class);
							 cr.add(Restrictions.eq(RegularContractCNTS.REG_CONT_ID,id));
							 list =cr.list();
							 transaction.commit();
							 session.flush();
						}
						catch (Exception e) {
							   e.printStackTrace();
						}
						session.clear();
						session.close();
						return list;
					}
	 	 
	 	@SuppressWarnings("unchecked")
 		@Transactional
 		public List<RegularContract> getRegContractData(String contractCode){
		 System.out.println("enter into getRegContractData function = "+contractCode);
 			List<RegularContract> regularList = new ArrayList<RegularContract>();
			 
 			try{
 				session = this.sessionFactory.openSession();
 				Criteria cr=session.createCriteria(RegularContract.class);
 				cr.add(Restrictions.eq(RegularContractCNTS.REG_CONT_CODE,contractCode));
 				regularList =cr.list();	 
 			}
		 	
 			catch(Exception e){
			 e.printStackTrace();
		 	}finally{
	 			session.clear();
				 session.close();
		 	}
			 return regularList;
 		}
	 	
			 	
	 	@SuppressWarnings("unchecked")
 		@Transactional	 	
		public List<String> getRegFaCode(){
			System.out.println("enter into getRegFaCode function");
			List<String> regFaList = new ArrayList<String>();
			try{
				session = this.sessionFactory.openSession();
 				transaction = session.beginTransaction();
 				Criteria cr=session.createCriteria(RegularContract.class);
 				ProjectionList projList = Projections.projectionList();
				projList.add(Projections.property(RegularContractCNTS.REG_CONT_FA_CODE));
				cr.setProjection(projList);
				
				regFaList = cr.list();
				
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return regFaList;
		}
	 	
	 	@SuppressWarnings("unchecked")
 		@Transactional	
	 	public List<RegularContract> getRegContByFaCode(String faCode){
	 		System.out.println("enter into getRegContByFaCode function");
	 		List<RegularContract> regList = new ArrayList<>();
	 		try{
	 			session = this.sessionFactory.openSession();
 				transaction = session.beginTransaction();
 				Criteria cr=session.createCriteria(RegularContract.class);
 				cr.add(Restrictions.eq(RegularContractCNTS.REG_CONT_FA_CODE,faCode));
 				regList = cr.list();
 				
 				session.flush();
	 		}catch(Exception e){
	 			e.printStackTrace();
	 		}
	 		session.clear();
	 		session.close();
	 		return regList;
	 	}
	 	
	 	@SuppressWarnings("unchecked")
 		@Transactional	
	 	public int extDtOfCont(String contFaCode, Date date){
	 		System.out.println("enter into extDtOfCont function");
	 		int res = 0;
	 		List<RegularContract> regList = new ArrayList<>();
	 		try{
	 			session = this.sessionFactory.openSession();
	 			transaction = session.beginTransaction();
 				Criteria cr=session.createCriteria(RegularContract.class);
 				cr.add(Restrictions.eq(RegularContractCNTS.REG_CONT_FA_CODE,contFaCode));
 				regList = cr.list();
 				
 				if(!regList.isEmpty()){
 					RegularContract regCont = regList.get(0);
 					Date prevDt = regCont.getRegContToDt();
 					regCont.setRegContToDt(date);	
 					session.update(regCont);
 					
 					List<ContToStn> ctsList = new ArrayList<>();
 					cr=session.createCriteria(ContToStn.class);
 					cr.add(Restrictions.eq(ContToStnCNTS.CTS_CONT_CODE,regCont.getRegContCode()));
 	 				cr.add(Restrictions.eq(ContToStnCNTS.CTS_TO_DT,prevDt));
 	 				ctsList = cr.list();
 					
 					if(!ctsList.isEmpty()){
 						for(int i=0;i<ctsList.size();i++){
 							ContToStn cts = ctsList.get(i);
 							cts.setCtsToDt(date);
 							session.update(cts);
 						}
 					}
 					
 					transaction.commit();
 					res = 1;
 				}
 				
 				session.flush();
	 		}catch(Exception e){
	 			e.printStackTrace();
	 		}
	 		session.clear();
	 		session.close();
	 		return res;
	 	}
	 	
	 	
	 	@SuppressWarnings("unchecked")
 		@Transactional	
	 	public int updateBillBase(String contCode , String billBase){
	 		System.out.println("enter into updateBillBase function = "+contCode+" and billBase = "+billBase);
	 		int res = 0;
	 		List<RegularContract> regList = new ArrayList<>();
	 		try{
	 			session = this.sessionFactory.openSession();
	 			transaction = session.beginTransaction();
 				Criteria cr=session.createCriteria(RegularContract.class);
 				cr.add(Restrictions.eq(RegularContractCNTS.REG_CONT_CODE,contCode));
 				regList = cr.list();
 				
 				if(!regList.isEmpty()){
 					RegularContract regularContract = regList.get(0);
 					regularContract.setRegContBillBasis(billBase);
 					session.update(regularContract);
 					transaction.commit();
 					res = 1;
 				}
 				
 				session.flush();
	 		}catch(Exception e){
	 			e.printStackTrace();
	 		}
	 		session.clear();
	 		session.close();
	 		return res;
	 	}

		@Override
		@Transactional
		public List<Map<String, Object>> getContList(String custId) {
			System.out.println("getContList");
			List<Map<String, Object>> contList = new ArrayList<>();
			List<Map<String, Object>> regQryList = new ArrayList<>();
			List<Map<String, Object>> dlyQryList = new ArrayList<>();
			List<Station> stationList = new ArrayList<>();
			
			/*System.err.println("custCode: "+custId);*/
			
			try {
				session = this.sessionFactory.openSession();
				Query regQry = session.createQuery("select new map(regContId as contId, regContCode as contCode, regContFromDt as contFromDt, regContToDt as contToDt, regContFromStation as contFrmStnId, regContFromStation as contFrmStnName, regContDdl as contDdl, regContCostGrade as contCostGrade) from RegularContract where regContBLPMCode = :custId");
				regQry.setString("custId", custId);
				regQryList = regQry.list();
				
				Query dlyQry = session.createQuery("select new map(dlyContId as contId, dlyContCode as contCode, dlyContStartDt as contFromDt, dlyContEndDt as contToDt, dlyContFromStation as contFrmStnId, dlyContFromStation as contFrmStnName, dlyContDdl as contDdl, dlyContCostGrade as contCostGrade) from DailyContract where dlyContBLPMCode = :custId");
				dlyQry.setString("custId", custId);
				dlyQryList = dlyQry.list();
								
				contList.addAll(regQryList);
				contList.addAll(dlyQryList);
				
				Criteria cr = session.createCriteria(Station.class);
				stationList = cr.list();
				
				/*for (Map<String, Object> cont : contList) {
					System.out.print("contId: "+cont.get("contId")+"\t");
					System.out.print("contCode: "+cont.get("contCode")+"\t");
					System.out.print("contFromDt: "+cont.get("contFromDt")+"\t");
					System.out.print("contToDt: "+cont.get("contToDt")+"\t");
					System.out.print("contFromStnName: "+cont.get("contFrmStnName")+"\t");
					System.out.println("contFromStnId: "+cont.get("contFrmStnId"));
					System.out.println("contDdl: "+cont.get("contDdl"));
					System.out.println("contCostGrade: "+cont.get("contCostGrade"));
					
				}*/
				
				session.flush();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			session.clear();
			session.close();
			
			for (Map<String, Object> cont : contList) {
				for (Station station : stationList) {
					if (station.getStnId() == Integer.parseInt(String.valueOf(cont.get("contFrmStnId")))) {
						cont.put("contFrmStnName", station.getStnName());
						break;
					}
				}
			}
			
			return contList;
		}

		@Override
		@Transactional
		public List<Map<String, Object>> getRateList(String contCode) {
			System.out.println("getRateList()");
			List<Map<String, Object>> rateList = new ArrayList<>();
			List<Station> stationList = new ArrayList<>();
			
			try {
				session = this.sessionFactory.openSession();
				Query query = session.createQuery("select new map(ctsId as ctsId, ctsFromWt as ctsFromWt, ctsRate as ctsRate, ctsToStn as ctsToStn, ctsToStn as ctsToStnName, ctsToWt as ctsToWt, ctsVehicleType as ctsVehicleType, ctsFrDt as ctsFrDt, ctsToDt as ctsToDt) from ContToStn where ctsContCode = :contCode");
				query.setString("contCode", contCode);
				rateList = query.list();
				
				Criteria cr = session.createCriteria(Station.class);
				stationList = cr.list();
				
				session.flush();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			session.clear();
			session.close();
			
			for (Map<String, Object> rate : rateList) {
				for (Station station : stationList) {
					if (station.getStnId() == Integer.parseInt(String.valueOf(rate.get("ctsToStn")))) {
						rate.put("ctsToStnName", station.getStnName());
						break;
					}
				}
			}
			
			/*for (Map<String, Object> rate : rateList) {
				System.out.print("ctsId: "+rate.get("ctsId")+"\t");
				System.out.print("ctsFromWt: "+rate.get("ctsFromWt")+"\t");
				System.out.print("ctsRate: "+rate.get("ctsRate")+"\t");
				System.out.print("ctsToStn: "+rate.get("ctsToStn")+"\t");
				System.out.print("ctsToStnName: "+rate.get("ctsToStnName")+"\t");
				System.out.print("ctsToWt: "+rate.get("ctsToWt")+"\t");
				System.out.print("ctsVehicleType: "+rate.get("ctsVehicleType")+"\t");
				System.out.print("ctsFrDt: "+rate.get("ctsFrDt")+"\t");
				System.out.println("ctsToDt: "+rate.get("ctsToDt"));
			}*/
			
			return rateList;
		}
}

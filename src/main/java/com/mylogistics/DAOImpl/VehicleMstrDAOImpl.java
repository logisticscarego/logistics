package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.VehicleMstrDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.EmployeeCNTS;
import com.mylogistics.constants.OwnerCNTS;
import com.mylogistics.constants.VehicleMstrCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.SubVehicleMstr;
import com.mylogistics.model.VehicleMstr;

public class VehicleMstrDAOImpl implements VehicleMstrDAO {

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public VehicleMstrDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public int saveVehicleMstr(Branch branch , Employee employee , VehicleMstr vehicleMstr){
		 System.out.println("enter into saveVehicleMstr function");
		 List<Branch> brList = new ArrayList<Branch>();
		 List<Employee> empList = new ArrayList<Employee>();
		 int temp;
		 try{
			  session = this.sessionFactory.openSession();
			  transaction =  session.beginTransaction();
			  
			  Criteria cr = session.createCriteria(Branch.class);
			  cr.add(Restrictions.eq(BranchCNTS.BRANCH_ID,branch.getBranchId()));
			  brList = cr.list();
			  if(!brList.isEmpty()){
				  Branch actBranch = brList.get(0);
				  vehicleMstr.setBranch(actBranch);
			  
			  
			  Criteria cr1 = session.createCriteria(Employee.class);
			  cr1.add(Restrictions.eq(EmployeeCNTS.EMP_ID,employee.getEmpId()));
			  empList = cr1.list();
			  if(!empList.isEmpty()){
				  Employee actEmployee = empList.get(0);
				  vehicleMstr.setEmployee(actEmployee);
				  actEmployee.getVehicleMstrList().add(vehicleMstr) ;
				  session.update(actEmployee);
			  }
			  
			  
			  session.update(actBranch);
			  }
			  
			  transaction.commit();
			  session.flush();
			  temp = 1;
		 }catch(Exception e){
			 e.printStackTrace();
			 temp= -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	}
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<VehicleMstr> getAllVehicleMstr(){
		 System.out.println("enter into getAllVehicleMstr function");
		 List<VehicleMstr> vehList = new ArrayList<VehicleMstr>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction =  session.beginTransaction();
			 
			 Criteria cr = session.createCriteria(VehicleMstr.class);
			 vehList = cr.list();
					
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return vehList;
	 }
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public int updateVMBySVM(int vmId , SubVehicleMstr subVehicleMstr){
		 System.out.println("enter into updateVMBySVM function");
		 int temp = 1;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction =  session.beginTransaction();
			 Criteria cr = session.createCriteria(VehicleMstr.class);
			 cr.add(Restrictions.eq(VehicleMstrCNTS.VEH_ID,vmId));
			 List<VehicleMstr> vmList = cr.list();
			 VehicleMstr vehicleMstr = vmList.get(0);
			 	
			 subVehicleMstr.setVehicleMstr(vehicleMstr);
			 vehicleMstr.getSvmList().add(subVehicleMstr);
			 
			 session.update(vehicleMstr);
			 transaction.commit();
			 session.flush();
			 temp = 1;
			 
		 }catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<String> getAllVehNo(){
		 System.out.println("enter into getAllVehNo function");
		 List<String> vehList = new ArrayList<>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction =  session.beginTransaction();
			 Criteria cr = session.createCriteria(VehicleMstr.class);
			 ProjectionList projList = Projections.projectionList();
			 projList.add(Projections.property(VehicleMstrCNTS.VEH_NO));
			 cr.setProjection(projList);
			 vehList = cr.list();
			 
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return vehList;
	 }
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public VehicleMstr getVehByVehNo(String vehNo){
		 System.out.println("enter into getVehByVehNo function");
		 VehicleMstr veh = null;
		 List<VehicleMstr> vehList = new ArrayList<>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction =  session.beginTransaction();
			 Criteria cr = session.createCriteria(VehicleMstr.class);
			 cr.add(Restrictions.eq(VehicleMstrCNTS.VEH_NO,vehNo));
			 vehList = cr.list();
			 if(!vehList.isEmpty()){
				 veh = vehList.get(0);
			 }
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return veh;
	 }
}

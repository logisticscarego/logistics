package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.HOSLastDAO;
import com.mylogistics.constants.HOSLastCNTS;
import com.mylogistics.model.HOSLast;

public class HOSLastDAOImpl implements HOSLastDAO{
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	public static Logger logger = Logger.getLogger(HOSLastDAOImpl.class);
	
	@Autowired
	public HOSLastDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
		
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<HOSLast> getLastArrival(){
		 logger.info("Enter into getLastArrival()");
		 List<HOSLast> last = new ArrayList<HOSLast>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction =  session.beginTransaction();
			 last = session.createQuery("from HOSLast order by hOSLastId DESC ").setMaxResults(1).list();
			 session.flush();
		 }catch(Exception e){
			 logger.error("Exception : "+e);
		 }
		session.clear();
		session.close();
		logger.info("Exit from getLastArrival()");
		return last;
	}
	 
	 @Transactional
	 public int saveHosLastToDB(HOSLast hosLast){
		 System.out.println("Entered into saveHosLastToDB function in DAOImpl---- ");
		 int temp;
		 hosLast.sethOSType("dispatch");
			try{
			    session = this.sessionFactory.openSession();
				transaction =  session.beginTransaction();
				session.save(hosLast);
				transaction.commit();
				session.flush();
				temp= 1;
			}
			catch(Exception e){
				e.printStackTrace();
				temp=-1;
			}
			session.clear();
		    session.close();
		    return temp;
	}
	 
	 

	 @Transactional
	 public int saveHosLast(HOSLast hosLast){
		 System.out.println("Entered into saveHosLastToDB function in DAOImpl---- ");
		 int temp;
			try{
			    session = this.sessionFactory.openSession();
				transaction =  session.beginTransaction();
				session.save(hosLast);
				transaction.commit();
				session.flush();
				temp= 1;
			}
			catch(Exception e){
				e.printStackTrace();
				temp=-1;
			}	
			session.clear();
		    session.close();
		    return temp;
	}
	 
	
	 @SuppressWarnings("unchecked")
		@Transactional
		public Date getFirstEntryDate(){
			System.out.println("enter into getFirstEntryDateOfCnmt function --->");
			List<HOSLast> hOSLastList = new ArrayList<HOSLast>();
			 Date date = null;
			 try{
				 session = this.sessionFactory.openSession();
				 hOSLastList = session.createQuery("from HOSLast where hOSType='dispatch' order by hOSLastId ").setMaxResults(1).list();
				 if(!hOSLastList.isEmpty()){
					 date = hOSLastList.get(0).gethOSDate();
					 System.out.println("The first date is---------------"+date);
				 }else{
					 System.out.println("The first date is---------------"+date);
				 }
				 session.flush();
			 }
			 catch(Exception e){
				 date = null;
				 e.printStackTrace();
			 }
			 session.clear();
			 session.close();
			 return date;
		 }
	 	 
/*	    @SuppressWarnings("unchecked")
		@Transactional
		public double getNoOfCnmt(Date startDate,Date endDate){
			
			List<HOSLast> HOSLastList = new ArrayList<HOSLast>();
			double noOfCnmt=0;
			 try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria criteria = session.createCriteria(HOSLast.class);
				 criteria.add(Restrictions.ge(HOSLastCNTS.HOSL_DATE,endDate)); 
				 criteria.add(Restrictions.lt(HOSLastCNTS.HOSL_DATE,startDate));
				 criteria.add(Restrictions.eq(HOSLastCNTS.HOSL_TYPE,"dispatch")); 
				 HOSLastList = criteria.list();
				 System.out.println("-------->HOSLastList.size() = "+HOSLastList.size());
				// System.out.println("-------->HOSLastList.get(0).gethOSCnmtDisOrRec() = "+HOSLastList.get(0).gethOSCnmtDisOrRec());
				 for(int i=0;i<HOSLastList.size();i++){
					 noOfCnmt = noOfCnmt + HOSLastList.get(i).gethOSCnmtDisOrRec();
					 System.out.println("noOfCnmt--------------->"+noOfCnmt);
					 System.out.println("HOSLastList.get(i).gethOSCnmtDisOrRec()--------------->"+HOSLastList.get(i).gethOSCnmtDisOrRec());
				 }
				// listSize=HOSLastList.size();
				 transaction.commit();
				 session.flush();
			 }
			 catch(Exception e){
				 e.printStackTrace(); 
			 }
			
			 session.close();
			 return noOfCnmt;
		 }
	    
	    @SuppressWarnings("unchecked")
		@Transactional
		public double getNoOfChln(Date startDate,Date endDate){
			
			List<HOSLast> HOSLastList = new ArrayList<HOSLast>();
			double noOfChln=0;
			 try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria criteria = session.createCriteria(HOSLast.class);
				 criteria.add(Restrictions.ge(HOSLastCNTS.HOSL_DATE,endDate)); 
				 criteria.add(Restrictions.lt(HOSLastCNTS.HOSL_DATE,startDate));
				 criteria.add(Restrictions.eq(HOSLastCNTS.HOSL_TYPE,"dispatch")); 
				 HOSLastList = criteria.list();
				 System.out.println("-------->HOSLastList.size() = "+HOSLastList.size());
				 //System.out.println("-------->HOSLastList.get(0).gethOSChlnDisOrRec() = "+HOSLastList.get(0).gethOSChlnDisOrRec());
				 for(int i=0;i<HOSLastList.size();i++){
					 noOfChln = noOfChln + HOSLastList.get(i).gethOSChlnDisOrRec();
					 System.out.println("noOfChln--------------->"+noOfChln);
					 System.out.println("HOSLastList.get(i).gethOSChlnDisOrRec()--------------->"+HOSLastList.get(i).gethOSChlnDisOrRec());
				 }
				// listSize=HOSLastList.size();
				 transaction.commit();
				 session.flush();
			 }
			 catch(Exception e){
				 e.printStackTrace(); 
			 }
			
			 session.close();
			 return noOfChln;
		 }
	    
	    @SuppressWarnings("unchecked")
		@Transactional
		public double getNoOfSedr(Date startDate,Date endDate){
			
			List<HOSLast> HOSLastList = new ArrayList<HOSLast>();
			double noOfSedr=0;
			 try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria criteria = session.createCriteria(HOSLast.class);
				 criteria.add(Restrictions.ge(HOSLastCNTS.HOSL_DATE,endDate)); 
				 criteria.add(Restrictions.lt(HOSLastCNTS.HOSL_DATE,startDate));
				 criteria.add(Restrictions.eq(HOSLastCNTS.HOSL_TYPE,"dispatch")); 
				 HOSLastList = criteria.list();
				 System.out.println("-------->HOSLastList.size() = "+HOSLastList.size());
				 //System.out.println("-------->HOSLastList.get(0).gethOSSedrDisOrRec() = "+HOSLastList.get(0).gethOSSedrDisOrRec());
				 for(int i=0;i<HOSLastList.size();i++){
					 noOfSedr = noOfSedr + HOSLastList.get(i).gethOSSedrDisOrRec();
					 System.out.println("noOfSedr--------------->"+noOfSedr);
					 System.out.println("HOSLastList.get(i).gethOSSedrDisOrRec()--------------->"+HOSLastList.get(i).gethOSSedrDisOrRec());
				 }
				 transaction.commit();
				 session.flush();
			 }
			 catch(Exception e){
				 e.printStackTrace(); 
			 }
			
			 session.close();
			 return noOfSedr;
		 }*/
	 
	 @SuppressWarnings("unchecked")
		@Transactional
		public Map<String,Object> getNoOfStationary(Date startDate,Date endDate){
		 Map<String,Object> map = new HashMap<String,Object>();
			List<HOSLast> HOSLastList = new ArrayList<HOSLast>();
			double noOfCnmt=0;
			double noOfChln=0;
			double noOfSedr=0;
			 try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria criteria = session.createCriteria(HOSLast.class);
				 criteria.add(Restrictions.ge(HOSLastCNTS.HOSL_DATE,endDate)); 
				 criteria.add(Restrictions.lt(HOSLastCNTS.HOSL_DATE,startDate));
				 criteria.add(Restrictions.eq(HOSLastCNTS.HOSL_TYPE,"dispatch")); 
				 HOSLastList = criteria.list();
				 System.out.println("-------->HOSLastList.size() = "+HOSLastList.size());
				// System.out.println("-------->HOSLastList.get(0).gethOSCnmtDisOrRec() = "+HOSLastList.get(0).gethOSCnmtDisOrRec());
				 for(int i=0;i<HOSLastList.size();i++){
					 noOfCnmt = noOfCnmt + HOSLastList.get(i).gethOSCnmtDisOrRec();
					 noOfChln = noOfChln+HOSLastList.get(i).gethOSChlnDisOrRec();
					 noOfSedr = noOfSedr+HOSLastList.get(i).gethOSSedrDisOrRec();
					 System.out.println("noOfCnmt--------------->"+noOfCnmt);
					 System.out.println("HOSLastList.get(i).gethOSCnmtDisOrRec()--------------->"+HOSLastList.get(i).gethOSCnmtDisOrRec());
				 }
				 map.put("noOfCnmt", noOfCnmt);
				 map.put("noOfChln", noOfChln);
				 map.put("noOfSedr", noOfSedr);
				// listSize=HOSLastList.size();
				 transaction.commit();
				 session.flush();
			 }
			 catch(Exception e){
				 e.printStackTrace(); 
			 }
			 session.clear();
			 session.close();
			 return map;
		 }
}

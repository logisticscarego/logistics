package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.BranchStockDispatchDAO;
import com.mylogistics.constants.BranchStockDispatchCNTS;
import com.mylogistics.model.BranchStockDispatch;
import com.mylogistics.model.MasterStationaryStk;

public class BranchStockDispatchDAOImpl implements BranchStockDispatchDAO {
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	public static Logger logger = Logger.getLogger(BranchStockDispatchDAOImpl.class);

	@Autowired
	public BranchStockDispatchDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	 public int saveBranchStockDispatch(BranchStockDispatch branchStockDispatch){
		logger.info("Enter into saveBranchStockDispatch() ");
		  int temp;
		   try{
			   session = this.sessionFactory.openSession();
			   transaction =  session.beginTransaction();
			   session.save(branchStockDispatch);
			   transaction.commit();
			   temp=1;
			   System.out.println("saveBranchStockDispatch Data is inserted successfully");
			   session.flush();			 
		   }
		   catch(Exception e){
			   logger.error("Exception : "+e);
				e.printStackTrace();
				temp=-1;
		   }
		   session.clear();
		   session.close();
		   logger.info("Exit from saveBranchStockDispatch() ");
		   return temp;
	 }
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<BranchStockDispatch> getBranchStockDispatchData(String branchCode){
		logger.info("Enter into getBranchStockDispatchData() : BranchCode = "+branchCode);
		List<BranchStockDispatch> branchStockDispatchs = new ArrayList<BranchStockDispatch>();
		try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			Criteria cr=session.createCriteria(BranchStockDispatch.class);
			cr.add(Restrictions.eq(BranchStockDispatchCNTS.BSD_BRCODE, branchCode));
			cr.add(Restrictions.eq(BranchStockDispatchCNTS.BSD_IS_STOCK_RECEIVED,"no"));
			branchStockDispatchs=cr.list();
			if(branchStockDispatchs.isEmpty())
				logger.info("No BranchStockDispatch is found !");
			else
				logger.info("BranchStockDispatch found ! Size = "+branchStockDispatchs.size());
			transaction.commit();
			session.flush();
			
		}
		catch(Exception e){
			logger.error("Exception : "+e);
			e.printStackTrace();
		}
		session.clear();
		session.close();
		logger.info("Exit from getBranchStockDispatchData()");
		return branchStockDispatchs;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<BranchStockDispatch> getBSDataByDisCode(String dispatchCode){
		List<BranchStockDispatch> brSDList = new ArrayList<BranchStockDispatch>();
		try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			Criteria cr=session.createCriteria(BranchStockDispatch.class);
			cr.add(Restrictions.eq(BranchStockDispatchCNTS.BSD_CODE , dispatchCode));
			brSDList=cr.list();
			transaction.commit();
			session.flush();
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return brSDList;
	}
	
	
	@Transactional
	
	public int stockRecieved(String bSDCode){
		int temp;
		System.out.println("enter into stockRecieved function---"+bSDCode);
		BranchStockDispatch branchStockDispatch = new BranchStockDispatch();
		try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			Criteria cr=session.createCriteria(BranchStockDispatch.class);
			cr.add(Restrictions.eq(BranchStockDispatchCNTS.BSD_CODE , bSDCode));
			branchStockDispatch = (BranchStockDispatch) cr.list().get(0);
			branchStockDispatch.setIsStockRecieved("yes");
			session.update(branchStockDispatch);
			temp = 1;
			transaction.commit();
			session.flush();
			
		}
		catch(Exception e){
			temp = -1;
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return temp;
	}
	
	
	@Transactional
	
	public int updateIsAdminView(String bSDCode){
		logger.info("Enter into updateIsAdminView : DispatchCode = "+bSDCode);
		int temp;
		System.out.println("enter into updateIsAdminView function---"+bSDCode);
		BranchStockDispatch branchStockDispatch = new BranchStockDispatch();
		try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			Criteria cr=session.createCriteria(BranchStockDispatch.class);
			cr.add(Restrictions.eq(BranchStockDispatchCNTS.BSD_CODE , bSDCode));
			branchStockDispatch = (BranchStockDispatch) cr.list().get(0);
			branchStockDispatch.setIsAdminView("yes");
			session.update(branchStockDispatch);
			temp = 1;
			transaction.commit();
			session.flush();
			
		}
		catch(Exception e){
			logger.error("Exception : "+e);
			temp = -1;
			e.printStackTrace();
		}
		session.clear();
		session.close();
		logger.info("Exit from updateIsAdminView()");
		return temp;
	}
	
	
	@Transactional
	public int updateDispatchMsg(String code){
	 BranchStockDispatch branchStockDispatch = new BranchStockDispatch();
	 int temp;
	 try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			Criteria cr=session.createCriteria(BranchStockDispatch.class);
			cr.add(Restrictions.eq(BranchStockDispatchCNTS.BSD_CODE,code));
			branchStockDispatch = (BranchStockDispatch) cr.list().get(0);
			branchStockDispatch.setDispatchMsg("received");
			branchStockDispatch.setIsStockRecieved("yes");
			session.update(branchStockDispatch);
			transaction.commit();
			session.flush();
			
			 temp = 1;
		}
	
	 catch(Exception e){
		 e.printStackTrace();
		 temp = -1;
	 }
	 session.clear();
	 session.close();
	 return temp;
 }
	
	@Transactional
	public String getBrCodeByDisCode(String dispatchCode,Date recDt){
		System.out.println("enter into getBrCodeByDisCode function");
		 String branchCode = "";
		 try{
				session = this.sessionFactory.openSession();
				transaction =  session.beginTransaction();
				Criteria cr=session.createCriteria(BranchStockDispatch.class);
				cr.add(Restrictions.eq(BranchStockDispatchCNTS.BSD_CODE,dispatchCode));
				BranchStockDispatch branchStockDispatch = (BranchStockDispatch) cr.list().get(0);
				branchStockDispatch.setBrsDisRcvDt(recDt);
				session.update(branchStockDispatch);
				branchCode = branchStockDispatch.getBrsDisBranchCode();
				transaction.commit();
				session.flush();
			}
		
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
			
		 return branchCode;
	}
	
	@Transactional
	 public long totalCount(){
		 long tempCount = -1;
		 try {
			session = this.sessionFactory.openSession();
			tempCount = (Long) session.createCriteria(BranchStockDispatch.class).setProjection(Projections.rowCount()).uniqueResult();
			session.flush();
		 	} 
		 catch (Exception e) {
		 	e.printStackTrace();
		 }
		 session.clear();
		session.close();
		return tempCount;
	 }
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int getLastBrnchStckDisptchId(){
		 List<BranchStockDispatch> list = new ArrayList<BranchStockDispatch>();
		 int id=0;
		 try{
			 session = this.sessionFactory.openSession();
			 list = session.createQuery("from BranchStockDispatch order by brsDisId DESC").setMaxResults(1).list();
			 id = list.get(0).getBrsDisId();
			 session.flush();
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return id;
	}
	
	public void updateMasterStock(Integer branchCode, List<Long> cnmtList, List<Long> chlnList, List<Long> sedrList){
		logger.info("Enter into updateMasterStock() : BranchCode = "+branchCode);
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			if(! cnmtList.isEmpty()){
				List<MasterStationaryStk> cnmtMstrList = session.createCriteria(MasterStationaryStk.class)
						.add(Restrictions.eq("isIssued", "no"))
						.add(Restrictions.eq("mstrStnStkStatus", "cnmt"))
						.add(Restrictions.in("mstrStnStkStartNo", cnmtList))
						.list();
				Iterator<MasterStationaryStk> it = cnmtMstrList.iterator();
				while(it.hasNext()){
					MasterStationaryStk stk = (MasterStationaryStk) it.next();
					stk.setIsIssued("yes");
					stk.setIsReceived("no");
					stk.setIssuedBranchCode(branchCode);
					session.update(stk);
				}
			}
			if(! chlnList.isEmpty()){
				List<MasterStationaryStk> chlnMstrList = session.createCriteria(MasterStationaryStk.class)
						.add(Restrictions.eq("isIssued", "no"))
						.add(Restrictions.eq("mstrStnStkStatus", "chln"))
						.add(Restrictions.in("mstrStnStkStartNo", chlnList))
						.list();
				Iterator<MasterStationaryStk> it = chlnMstrList.iterator();
				while(it.hasNext()){
					MasterStationaryStk stk = (MasterStationaryStk) it.next();
					stk.setIsIssued("yes");
					stk.setIsReceived("no");
					stk.setIssuedBranchCode(branchCode);
					session.update(stk);
				}
			}
			if(! sedrList.isEmpty()){
				List<MasterStationaryStk> sedrMstrList = session.createCriteria(MasterStationaryStk.class)
						.add(Restrictions.eq("isIssued", "no"))
						.add(Restrictions.eq("mstrStnStkStatus", "sedr"))
						.add(Restrictions.in("mstrStnStkStartNo", sedrList))
						.list();
				Iterator<MasterStationaryStk> it = sedrMstrList.iterator();
				while(it.hasNext()){
					MasterStationaryStk stk = (MasterStationaryStk) it.next();
					stk.setIsIssued("yes");
					stk.setIsReceived("no");
					stk.setIssuedBranchCode(branchCode);
					session.update(stk);
				}
			}
			
			transaction.commit();
		}catch(Exception e){
			transaction.rollback();
			logger.error("Exception : "+e);
		}finally{
			if(session != null){
				session.flush();
				session.clear();
				session.close();
			}
		}
	}
	
}

package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.BillDAO;
import com.mylogistics.constants.BillCNTS;
import com.mylogistics.constants.BillFormateNameCNTS;
import com.mylogistics.constants.BillForwardingCNTS;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.constants.ChallanDetailCNTS;
import com.mylogistics.constants.Cnmt_ChallanCNTS;
import com.mylogistics.constants.CustomerCNTS;
import com.mylogistics.constants.FAMasterCNTS;
import com.mylogistics.constants.StateCNTS;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.Bill;
import com.mylogistics.model.BillDetail;
import com.mylogistics.model.BillDetail.BillDetailNormal;
import com.mylogistics.model.BillDetail.BillDetailSup;
import com.mylogistics.model.BillForwarding;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.model.MasterStationaryStk;
import com.mylogistics.model.CashStmt.CashStmtBillInsert;
import com.mylogistics.model.CashStmtTemp.CashStmtTempBillAdvInsert;
import com.mylogistics.model.Challan;
import com.mylogistics.model.ChallanDetail;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.Cnmt.CnmtBillInsert;
import com.mylogistics.model.Cnmt_Challan;
import com.mylogistics.model.Customer;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.HilAddress;
import com.mylogistics.model.RelBillAddress;
import com.mylogistics.model.State;
import com.mylogistics.model.Station;
import com.mylogistics.model.User;
import com.mylogistics.model.Bill.BillInsert;
import com.mylogistics.services.BillDetService;
import com.mylogistics.services.BillService;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.NumToWords;
import com.mylogistics.services.ValidatorFactoryUtil;

public class BillDAOImpl implements BillDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	java.util.Date dateForKishan = new java.util.Date(2016, 05, 01);
	
	@Autowired
	private HttpSession httpSession;
	
	public static Logger logger = Logger.getLogger(BillDAOImpl.class);
	
	@Autowired
	public BillDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	
	@Override
	public int getBillByBlNo(String blNo){
		
		int i=0;
		List list=new ArrayList<>();
		Session session=null;
		try{
			session=this.sessionFactory.openSession();
			Criteria criteria=session.createCriteria(Bill.class)
					.add(Restrictions.eq(BillCNTS.Bill_NO, blNo));
			list=criteria.list();
			if(!list.isEmpty())
				i=2;
			else
				i=1;
		}catch(Exception e){
			i=-1;
			e.printStackTrace();
		}finally{
			session.clear();
			session.close();
		}
		
		return i;
		
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public String saveAdvBill(BillService billService){
		System.out.println("enter into saveAdvBill function");
		String billNo = "";
		User currentUser = (User)httpSession.getAttribute("currentUser");
		List<Bill> billList = new ArrayList<>();
		try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 
			 Query query = session.createQuery("from Bill where blBrhId = :type order by blId DESC");
			 query.setParameter("type", Integer.parseInt(currentUser.getUserBranchCode()));
			 billList = query.setMaxResults(1).list();
			 System.out.println("billList="+billList+"\nsize="+billList.size());
			 System.out.println("billNo="+billList.get(0).getBlBillNo());
			 Criteria cr = session.createCriteria(FAMaster.class);
			 
			 String frghtCode = "";
			 String srvTaxCode = "";
			 List<FAMaster> famList = new ArrayList<>();
				
			famList = session.createCriteria(FAMaster.class)
					  .add(Restrictions.disjunction()
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_COLLECTION))
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_SRV_TAX))
				  ).list(); 
				
			if(!famList.isEmpty()){
					for(int i=0;i<famList.size();i++){
						if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_COLLECTION)){
							frghtCode = famList.get(i).getFaMfaCode();
						}else if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_SRV_TAX)){
							srvTaxCode = famList.get(i).getFaMfaCode();
						}else{
							System.out.println("invalid result");
						}
					}
				}
				
			 String brh = "";
			 Branch branch = (Branch) session.get(Branch.class,Integer.parseInt(currentUser.getUserBranchCode()));
			 if(branch != null){
				brh = branch.getBranchName().substring(0,3).toUpperCase();
				if(branch.getBranchName().equals("Raipur")){
					brh="RIP";
				}
			 }
			 
			 if(brh != null){
				 List<BillDetService> bdSerList = billService.getBdSerList();
				 Bill bill = billService.getBill();
				 
				 Customer customer = (Customer) session.get(Customer.class,bill.getBlCustId());
				 
				 if(bill.getBlCustId() > 0 && bill.getBlBrhId() > 0){
					 
					 if(!billList.isEmpty()){
						 String prevBillNo = billList.get(0).getBlBillNo();
						 System.out.println("prevBillNo="+prevBillNo);
						 billNo  = brh + String.valueOf(Integer.parseInt(prevBillNo.substring(3)) + 1 + 1000000).substring(1);
						 System.out.println("if billNo="+billNo);
					 }else{
						 billNo = brh+"000001";
					 }
					 
					 System.out.println("Bill No : "+billNo);
					 
					 if(billNo != null){
						 
						 if(bill.getBlType() == 'N'){
							 bill.setBlBillNo(billNo);
							 bill.setBlRemAmt(bill.getBlFinalTot());
							 bill.setUserCode(currentUser.getUserCode());
							 bill.setbCode(currentUser.getUserBranchCode());
							 int billId = (Integer) session.save(bill);
							 if(!bdSerList.isEmpty()){
								 for(int i=0;i<bdSerList.size();i++){
									 BillDetService billDetService = bdSerList.get(i);
									 BillDetail billDetail = billDetService.getBillDetail();
									 Cnmt cnmt = billDetService.getCnmt();
									 Cnmt actCnmt = (Cnmt) session.get(Cnmt.class,cnmt.getCnmtId());
									 
									 billDetail.setUserCode(currentUser.getUserCode());
									 billDetail.setbCode(currentUser.getUserBranchCode());
									 billDetail.setBdCnmtId(cnmt.getCnmtId());
									 
									 Bill freshBill = (Bill) session.get(Bill.class,billId);
									 billDetail.setBill(freshBill);
									 int bdId = (Integer) session.save(billDetail);
									 freshBill.getBillDetList().add(billDetail);
									 session.merge(freshBill);
									 
									 actCnmt.setCnmtBillDetId(bdId);
									 actCnmt.setCnmtBillNo(billNo);
									 session.merge(actCnmt);
								 }
							 }
							 
							
								 
							     CashStmtTemp cs1 = new CashStmtTemp();
								 cs1.setUserCode(currentUser.getUserCode());
								 cs1.setbCode(currentUser.getUserBranchCode());
								 //cs1.setCsDescription(bill.getBlDesc());
								 cs1.setCsDescription("As Per Bill File");
								 cs1.setCsDrCr('C');
								 cs1.setCsAmt(bill.getBlSubTot());
								 cs1.setCsType("BILL");
								 cs1.setCsTvNo(billNo);
								 cs1.setCsVouchType("CONTRA");
								 cs1.setCsFaCode(frghtCode);
								 cs1.setCsBrhFaCode(branch.getBranchFaCode());
								 //cs1.setCsVouchNo(voucherNo);
								 cs1.setCsDt(bill.getBlBillDt());
								 session.save(cs1);
								 
								 if(bill.getBlFinalTot() - bill.getBlSubTot() > 0){
									 CashStmtTemp cs = new CashStmtTemp();
									 cs.setUserCode(currentUser.getUserCode());
									 cs.setbCode(currentUser.getUserBranchCode());
									 //cs.setCsDescription(bill.getBlDesc());
									 cs.setCsDescription("As Per Bill File");
									 cs.setCsDrCr('C');
									 cs.setCsAmt(bill.getBlSerTax() + bill.getBlSwachBhCess()+bill.getBlKisanKalCess());
									 cs.setCsType("BILL");
									 cs.setCsTvNo(billNo);
									 cs.setCsVouchType("CONTRA");
									 cs.setCsFaCode(srvTaxCode);
									 cs.setCsBrhFaCode(branch.getBranchFaCode());
									 //cs.setCsVouchNo(voucherNo);
									 cs.setCsDt(bill.getBlBillDt());
									 session.save(cs);
								 }
								 
								 
								 
								 CashStmtTemp cs2 = new CashStmtTemp();
								 cs2.setUserCode(currentUser.getUserCode());
								 cs2.setbCode(currentUser.getUserBranchCode());
								 //cs2.setCsDescription(bill.getBlDesc());
								 cs2.setCsDescription("As Per Bill File");
								 cs2.setCsDrCr('D');
								 cs2.setCsAmt(bill.getBlFinalTot());
								 cs2.setCsType("BILL");
								 cs2.setCsTvNo(billNo);
								 cs2.setCsVouchType("CONTRA");
								 cs2.setCsFaCode(customer.getCustFaCode());
								 //cs2.setCsVouchNo(voucherNo);
								 cs2.setCsBrhFaCode(branch.getBranchFaCode());
								 cs2.setCsDt(bill.getBlBillDt());
								 session.save(cs2);
							 
						 }else{
							 bill.setBlBillNo(billNo);
							 bill.setBlRemAmt(bill.getBlFinalTot());
							 bill.setUserCode(currentUser.getUserCode());
							 bill.setbCode(currentUser.getUserBranchCode());
							 int billId = (Integer) session.save(bill);
							 
							 if(!bdSerList.isEmpty()){
								 for(int i=0;i<bdSerList.size();i++){
									 BillDetService billDetService = bdSerList.get(i);
									 BillDetail billDetail = billDetService.getBillDetail();
									 Cnmt cnmt = billDetService.getCnmt();
									 Cnmt actCnmt = (Cnmt) session.get(Cnmt.class,cnmt.getCnmtId());
									 
									 billDetail.setUserCode(currentUser.getUserCode());
									 billDetail.setbCode(currentUser.getUserBranchCode());
									 billDetail.setBdCnmtId(cnmt.getCnmtId());
									 
									 Bill freshBill = (Bill) session.get(Bill.class,billId);
									 billDetail.setBill(freshBill);
									 int bdId = (Integer) session.save(billDetail);
									 freshBill.getBillDetList().add(billDetail);
									 session.merge(freshBill);
									 
									 if(actCnmt.getCnmtSupBillNo() == null || !actCnmt.getCnmtSupBillNo().isEmpty()){
										 ArrayList<String> list = new ArrayList<>();
										 list.add(billNo);
										 actCnmt.setCnmtSupBillNo(list);
									 }else{
										 actCnmt.getCnmtSupBillNo().add(billNo);
									 }
									 
									 if(actCnmt.getCnmtSupBillDet() == null || !actCnmt.getCnmtSupBillDet().isEmpty()){
										 ArrayList<String> list = new ArrayList<>();
										 list.add(String.valueOf(bdId));
										 actCnmt.setCnmtSupBillDet(list);
									 }else{
										 actCnmt.getCnmtSupBillDet().add(String.valueOf(bdId));
									 }
									 
									 session.merge(actCnmt);
								 }
							 }
							 
							 
							  
								 CashStmtTemp cs1 = new CashStmtTemp();
								 cs1.setUserCode(currentUser.getUserCode());
								 cs1.setbCode(currentUser.getUserBranchCode());
								 //cs1.setCsDescription(bill.getBlDesc());
								 cs1.setCsDescription("As Per Bill File");
								 cs1.setCsDrCr('C');
								 cs1.setCsAmt(bill.getBlSubTot());
								 cs1.setCsType("BILL");
								 cs1.setCsTvNo(billNo);
								 cs1.setCsVouchType("CONTRA");
								 cs1.setCsFaCode(frghtCode);
								 //cs1.setCsVouchNo(voucherNo);
								 cs1.setCsBrhFaCode(branch.getBranchFaCode());
								 cs1.setCsDt(bill.getBlBillDt());
								  session.save(cs1);
								 
								 if(bill.getBlFinalTot() - bill.getBlSubTot() > 0){
									 CashStmtTemp cs = new CashStmtTemp();
									 cs.setUserCode(currentUser.getUserCode());
									 cs.setbCode(currentUser.getUserBranchCode());
									 //cs.setCsDescription(bill.getBlDesc());
									 cs.setCsDescription("As Per Bill File");
									 cs.setCsDrCr('C');
									 cs.setCsAmt(bill.getBlSerTax() + bill.getBlSwachBhCess()+bill.getBlKisanKalCess());
									 cs.setCsType("BILL");
									 cs.setCsTvNo(billNo);
									 cs.setCsVouchType("CONTRA");
									 cs.setCsFaCode(frghtCode);
									 //cs.setCsVouchNo(voucherNo);
									 cs.setCsBrhFaCode(branch.getBranchFaCode());
									 cs.setCsDt(bill.getBlBillDt());
									 
									 session.save(cs);
								 }
								
								 
						    	 CashStmtTemp cs2 = new CashStmtTemp();
								 cs2.setUserCode(currentUser.getUserCode());
								 cs2.setbCode(currentUser.getUserBranchCode());
								 //cs2.setCsDescription(bill.getBlDesc());
								 cs2.setCsDescription("As Per Bill File");
								 cs2.setCsDrCr('D');
								 cs2.setCsAmt(bill.getBlFinalTot());
								 cs2.setCsType("BILL");
								 cs2.setCsTvNo(billNo);
								 cs2.setCsVouchType("CONTRA");
								 cs2.setCsFaCode(customer.getCustFaCode());
								 //cs2.setCsVouchNo(voucherNo);
								 cs2.setCsBrhFaCode(branch.getBranchFaCode());
								 cs2.setCsDt(bill.getBlBillDt());
								 session.save(cs2);
						 }
					 }
					
				 }else{
					 System.out.println("invalid customer or branch for billing");
				 }
			 }else{
				 System.out.println("User doesn't have any branch");
			 }
			 transaction.commit();
			 session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		return billNo;
	}
	
	
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public String saveBill(BillService billService){
		 System.out.println("enter into saveBill function");
		 String billNo = "";
		 User currentUser = (User)httpSession.getAttribute("currentUser");
		 List<Bill> billList = new ArrayList<>();
		 
		 Date date = new Date(new java.util.Date().getTime());
		 Calendar calendar = Calendar.getInstance();
		 calendar.setTime(date);
		 System.out.println("unique id =====>" + calendar.getTimeInMillis());
		 long milliSec = calendar.getTimeInMillis();
		 String tvNo = String.valueOf(milliSec);
			
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Query query = session.createQuery("from Bill where blBrhId = :type order by blId DESC");
			 query.setParameter("type", Integer.parseInt(currentUser.getUserBranchCode()));
			 billList = query.setMaxResults(1).list();
			 
			 Criteria cr = session.createCriteria(FAMaster.class);
			 
			 String frghtCode = "";
			 String srvTaxCode = "";
			 List<FAMaster> famList = new ArrayList<>();
				
			famList = session.createCriteria(FAMaster.class)
					  .add(Restrictions.disjunction()
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_COLLECTION))
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_SRV_TAX))
				  ).list(); 
				
				
				if(!famList.isEmpty()){
					for(int i=0;i<famList.size();i++){
						if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_COLLECTION)){
							frghtCode = famList.get(i).getFaMfaCode();
						}else if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_SRV_TAX)){
							srvTaxCode = famList.get(i).getFaMfaCode();
						}else{
							System.out.println("invalid result");
						}
					}
				}
				
			 String brh = "";
			 Branch branch = (Branch) session.get(Branch.class,Integer.parseInt(currentUser.getUserBranchCode()));
			 if(branch != null){
				brh = branch.getBranchName().substring(0,3).toUpperCase();
				if(branch.getBranchName().equals("Raipur")){
					brh="RIP";
				}
			 }
			 
			 if(brh != null){
				 List<BillDetService> bdSerList = billService.getBdSerList();
				 Bill bill = billService.getBill();
				 
				 Customer customer = (Customer) session.get(Customer.class,bill.getBlCustId());
				 
				 if(bill.getBlCustId() > 0 && bill.getBlBrhId() > 0){
					 
					 if(!billList.isEmpty()){
						 String prevBillNo = billList.get(0).getBlBillNo();
						 billNo  = brh + String.valueOf(Integer.parseInt(prevBillNo.substring(3)) + 1 + 1000000).substring(1);
					 }else{
						 billNo = brh+"000001";
					 }
					 
					 System.out.println("Bill No : "+billNo);
					 
					 if(billNo != null){
						 
						 if(bill.getBlType() == 'N'){
							 bill.setBlBillNo(billNo);
							 bill.setBlRemAmt(bill.getBlFinalTot());
							 bill.setUserCode(currentUser.getUserCode());
							 bill.setbCode(currentUser.getUserBranchCode());
							 int billId = (Integer) session.save(bill);
							 
							 if(!bdSerList.isEmpty()){
								 for(int i=0;i<bdSerList.size();i++){
									 BillDetService billDetService = bdSerList.get(i);
									 BillDetail billDetail = billDetService.getBillDetail();
									 Cnmt cnmt = billDetService.getCnmt();
									 Cnmt actCnmt = (Cnmt) session.get(Cnmt.class,cnmt.getCnmtId());
									 
									 billDetail.setUserCode(currentUser.getUserCode());
									 billDetail.setbCode(currentUser.getUserBranchCode());
									 billDetail.setBdCnmtId(cnmt.getCnmtId());
									 
									 Bill freshBill = (Bill) session.get(Bill.class,billId);
									 billDetail.setBill(freshBill);
									 int bdId = (Integer) session.save(billDetail);
									 freshBill.getBillDetList().add(billDetail);
									 session.merge(freshBill);
									 
									 actCnmt.setCnmtBillDetId(bdId);
									 actCnmt.setCnmtBillNo(billNo);
									 session.merge(actCnmt);
								 }
							 }
							 
							 int voucherNo = 0;
							 
							 cr = session.createCriteria(CashStmt.class);
							 cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,bill.getBlBillDt()));
							 cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
							 ProjectionList proList = Projections.projectionList();
							 proList.add(Projections.property("csVouchNo"));
							 cr.setProjection(proList);
								
							 List<Integer>  voucherNoList = cr.list();
							 if(!voucherNoList.isEmpty()){
									int vNo = voucherNoList.get(voucherNoList.size() - 1);
									voucherNo = vNo + 1;
							 }else{
									voucherNo = 1;
							 }
							 
							 List<CashStmtStatus> cssList = new ArrayList<>();
							 cr = session.createCriteria(CashStmtStatus.class);
							 cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,currentUser.getUserBranchCode()));
							 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT,bill.getBlBillDt()));
							 cssList = cr.list();
							 
							 if(!cssList.isEmpty()){
								 CashStmtStatus css = cssList.get(0);
								 
								 CashStmt cs1 = new CashStmt();
								 cs1.setUserCode(currentUser.getUserCode());
								 cs1.setbCode(currentUser.getUserBranchCode());
								 //cs1.setCsDescription(bill.getBlDesc());
								 cs1.setCsDescription("As Per Bill File");
								 cs1.setCsDrCr('C');
								 cs1.setCsAmt(bill.getBlSubTot());
								 cs1.setCsType("BILL");
								 cs1.setCsTvNo(billNo);
								 cs1.setCsVouchType("CONTRA");
								 cs1.setCsFaCode(frghtCode);
								 cs1.setCsVouchNo(voucherNo);
								 cs1.setCsDt(css.getCssDt());
								 
								 cs1.setCsNo(css);
								 css.getCashStmtList().add(cs1);
								 session.merge(css);
								 
								 if(bill.getBlFinalTot() - bill.getBlSubTot() > 0){
									 CashStmt cs = new CashStmt();
									 cs.setUserCode(currentUser.getUserCode());
									 cs.setbCode(currentUser.getUserBranchCode());
									 //cs.setCsDescription(bill.getBlDesc());
									 cs.setCsDescription("As Per Bill File");
									 cs.setCsDrCr('C');
									 cs.setCsAmt(bill.getBlSerTax() + bill.getBlSwachBhCess()+bill.getBlKisanKalCess());
									 cs.setCsType("BILL");
									 cs.setCsTvNo(billNo);
									 cs.setCsVouchType("CONTRA");
									 cs.setCsFaCode(srvTaxCode);
									 cs.setCsVouchNo(voucherNo);
									 cs.setCsDt(css.getCssDt());
									 
									 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
									 cs.setCsNo(actCss);
									 actCss.getCashStmtList().add(cs);
									 session.merge(actCss);
								 }
								 
								 
								 
								 CashStmt cs2 = new CashStmt();
								 cs2.setUserCode(currentUser.getUserCode());
								 cs2.setbCode(currentUser.getUserBranchCode());
								 //cs2.setCsDescription(bill.getBlDesc());
								 cs2.setCsDescription("As Per Bill File");
								 cs2.setCsDrCr('D');
								 cs2.setCsAmt(bill.getBlFinalTot());
								 cs2.setCsType("BILL");
								 cs2.setCsTvNo(billNo);
								 cs2.setCsVouchType("CONTRA");
								 cs2.setCsFaCode(customer.getCustFaCode());
								 cs2.setCsVouchNo(voucherNo);
								 cs2.setCsDt(css.getCssDt());
								 
								 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
								 cs2.setCsNo(actCss);
								 actCss.getCashStmtList().add(cs2);
								 session.merge(actCss);
							 }
							 
						 }else{
							 bill.setBlBillNo(billNo);
							 bill.setBlRemAmt(bill.getBlFinalTot());
							 bill.setUserCode(currentUser.getUserCode());
							 bill.setbCode(currentUser.getUserBranchCode());
							 int billId = (Integer) session.save(bill);
							 
							 if(!bdSerList.isEmpty()){
								 for(int i=0;i<bdSerList.size();i++){
									 BillDetService billDetService = bdSerList.get(i);
									 BillDetail billDetail = billDetService.getBillDetail();
									 Cnmt cnmt = billDetService.getCnmt();
									 Cnmt actCnmt = (Cnmt) session.get(Cnmt.class,cnmt.getCnmtId());
									 
									 billDetail.setUserCode(currentUser.getUserCode());
									 billDetail.setbCode(currentUser.getUserBranchCode());
									 billDetail.setBdCnmtId(cnmt.getCnmtId());
									 
									 Bill freshBill = (Bill) session.get(Bill.class,billId);
									 billDetail.setBill(freshBill);
									 int bdId = (Integer) session.save(billDetail);
									 freshBill.getBillDetList().add(billDetail);
									 session.merge(freshBill);
									 
									 System.out.println("bdId = "+bdId);
									 
									 if(actCnmt.getCnmtSupBillNo() == null || !actCnmt.getCnmtSupBillNo().isEmpty()){
										 ArrayList<String> list = new ArrayList<>();
										 list.add(billNo);
										 actCnmt.setCnmtSupBillNo(list);
									 }else{
										 actCnmt.getCnmtSupBillNo().add(billNo);
									 }
									 
									 if(actCnmt.getCnmtSupBillDet() == null || !actCnmt.getCnmtSupBillDet().isEmpty()){
										 ArrayList<String> list = new ArrayList<>();
										 list.add(String.valueOf(bdId));
										 actCnmt.setCnmtSupBillDet(list);
									 }else{
										 actCnmt.getCnmtSupBillDet().add(String.valueOf(bdId));
									 }
									 
									 session.merge(actCnmt);
								 }
							 }
							 
							 
							 int voucherNo = 0;
							 
							 cr = session.createCriteria(CashStmt.class);
							 cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,bill.getBlBillDt()));
							 cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
							 ProjectionList proList = Projections.projectionList();
							 proList.add(Projections.property("csVouchNo"));
							 cr.setProjection(proList);
								
							 List<Integer>  voucherNoList = cr.list();
							 if(!voucherNoList.isEmpty()){
									int vNo = voucherNoList.get(voucherNoList.size() - 1);
									voucherNo = vNo + 1;
							 }else{
									voucherNo = 1;
							 }
							 
							 
							 List<CashStmtStatus> cssList = new ArrayList<>();
							 cr = session.createCriteria(CashStmtStatus.class);
							 cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,currentUser.getUserBranchCode()));
							 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT,bill.getBlBillDt()));
							 cssList = cr.list();
							 
							 							 
							 if(!cssList.isEmpty()){
								 CashStmtStatus css = cssList.get(0);
								 
								 CashStmt cs1 = new CashStmt();
								 
								 cs1.setUserCode(currentUser.getUserCode());
								 cs1.setbCode(currentUser.getUserBranchCode());
								 //cs1.setCsDescription(bill.getBlDesc());
								 cs1.setCsDescription("As Per Bill File");
								 cs1.setCsDrCr('C');
								 cs1.setCsAmt(bill.getBlSubTot());
								 cs1.setCsType("BILL");
								 cs1.setCsTvNo(billNo);
								 cs1.setCsVouchType("CONTRA");
								 cs1.setCsFaCode(frghtCode);
								 cs1.setCsVouchNo(voucherNo);
								 cs1.setCsDt(css.getCssDt());
								 cs1.setCsNo(css);
								 css.getCashStmtList().add(cs1);
								 session.merge(css);
								 
								 if(bill.getBlFinalTot() - bill.getBlSubTot() > 0){
									 CashStmt cs = new CashStmt();
									 cs.setUserCode(currentUser.getUserCode());
									 cs.setbCode(currentUser.getUserBranchCode());
									 //cs.setCsDescription(bill.getBlDesc());
									 cs.setCsDescription("As Per Bill File");
									 cs.setCsDrCr('C');
									 cs.setCsAmt(bill.getBlSerTax() + bill.getBlSwachBhCess()+bill.getBlKisanKalCess());
									 cs.setCsType("BILL");
									 cs.setCsTvNo(billNo);
									 cs.setCsVouchType("CONTRA");
									 cs.setCsFaCode(frghtCode);
									 cs.setCsVouchNo(voucherNo);
									 cs.setCsDt(css.getCssDt());
									 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
									 cs.setCsNo(actCss);
									 actCss.getCashStmtList().add(cs);
									 session.merge(actCss);
								 }
								
								 
								 
								 
								 CashStmt cs2 = new CashStmt();
								 cs2.setUserCode(currentUser.getUserCode());
								 cs2.setbCode(currentUser.getUserBranchCode());
								 //cs2.setCsDescription(bill.getBlDesc());
								 cs2.setCsDescription("As Per Bill File");
								 cs2.setCsDrCr('D');
								 cs2.setCsAmt(bill.getBlFinalTot());
								 cs2.setCsType("BILL");
								 cs2.setCsTvNo(billNo);
								 cs2.setCsVouchType("CONTRA");
								 cs2.setCsFaCode(customer.getCustFaCode());
								 cs2.setCsVouchNo(voucherNo);
								 cs2.setCsDt(css.getCssDt());
								 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
								 cs2.setCsNo(actCss);
								 actCss.getCashStmtList().add(cs2);
								 
								 session.merge(actCss);
							 }
							 
						 }
					 }
					
				 }else{
					 System.out.println("invalid customer or branch for billing");
				 }
			 }else{
				 System.out.println("User doesn't have any branch");
			 }
			 transaction.commit();
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
			 billNo = "";
		 }
		 session.clear();
		 session.close();
		 return billNo;
	 }
	 
	
	 
	 
	 
	 
	 
	 //TODO
	 @SuppressWarnings("unchecked")
	 public String saveBillM(BillService billService,Session session){
		 System.out.println("enter into saveBill function");
		 String billNo = "";
		 User currentUser = (User)httpSession.getAttribute("currentUser");
			
		 Criteria cr =null; //session.createCriteria(FAMaster.class);
			 
			 String frghtCode = "";
			 String srvTaxCode = "";
			 List<FAMaster> famList = new ArrayList<>();
				
			famList = session.createCriteria(FAMaster.class)
					  .add(Restrictions.disjunction()
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_COLLECTION))
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_SRV_TAX))
				  ).list(); 
				
				
				if(!famList.isEmpty()){
					for(int i=0;i<famList.size();i++){
						if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_COLLECTION)){
							frghtCode = famList.get(i).getFaMfaCode();
						}else if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_SRV_TAX)){
							srvTaxCode = famList.get(i).getFaMfaCode();
						}else{
							System.out.println("invalid result");
						}
					}
				}
				
				 List<BillDetService> bdSerList = billService.getBdSerList();
				 Bill bill = billService.getBill();
				// bill.setBlBillZ("Z");
				 
				 Customer customer = (Customer) session.get(Customer.class,bill.getBlCustId());
				 
				 if(bill.getBlCustId() > 0 && bill.getBlBrhId() > 0){
					 
					 billNo=bill.getBlBillNo();
					 System.out.println("Bill No : "+billNo);
					 
					 if(billNo != null){
						 
						 if(bill.getBlType() == 'N'){
							 bill.setBlBillNo(billNo);
							 if(billService.getBill().getBlGstNo() != null)
								 bill.setBlStateGST(billService.getBill().getBlGstNo().substring(0, 2));
							 bill.setBlRemAmt(bill.getBlFinalTot());
							 bill.setUserCode(currentUser.getUserCode());
							 bill.setbCode(currentUser.getUserBranchCode());
							 int billId = (Integer) session.save(bill);
							 
							 if(!bdSerList.isEmpty()){
								 for(int i=0;i<bdSerList.size();i++){
									 BillDetService billDetService = bdSerList.get(i);
									 BillDetail billDetail = billDetService.getBillDetail();
									 Cnmt cnmt = billDetService.getCnmt();
									 Cnmt actCnmt = (Cnmt) session.get(Cnmt.class,cnmt.getCnmtId());
									 
									 billDetail.setUserCode(currentUser.getUserCode());
									 billDetail.setbCode(currentUser.getUserBranchCode());
									 billDetail.setBdCnmtId(cnmt.getCnmtId());
									 
									 Bill freshBill = (Bill) session.get(Bill.class,billId);
									 billDetail.setBill(freshBill);
									 int bdId = (Integer) session.save(billDetail);
									 freshBill.getBillDetList().add(billDetail);
									 session.merge(freshBill);
									 
									 actCnmt.setCnmtBillDetId(bdId);
									 actCnmt.setCnmtBillNo(billNo);
									 session.merge(actCnmt);
								 }
							 }
							 
							 int voucherNo = 0;
							 
							 cr = session.createCriteria(CashStmt.class);
							 cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,bill.getBlBillDt()));
							 cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
							 ProjectionList proList = Projections.projectionList();
							 proList.add(Projections.property("csVouchNo"));
							 cr.setProjection(proList);
								
							 List<Integer>  voucherNoList = cr.list();
							 if(!voucherNoList.isEmpty()){
									int vNo = voucherNoList.get(voucherNoList.size() - 1);
									voucherNo = vNo + 1;
							 }else{
									voucherNo = 1;
							 }
							 
							 List<CashStmtStatus> cssList = new ArrayList<>();
							 cr = session.createCriteria(CashStmtStatus.class);
							 cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,currentUser.getUserBranchCode()));
							 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT,bill.getBlBillDt()));
							 cssList = cr.list();
							 
							 if(!cssList.isEmpty()){
								 CashStmtStatus css = cssList.get(0);
								 
								 CashStmt cs1 = new CashStmt();
								 cs1.setUserCode(currentUser.getUserCode());
								 cs1.setbCode(currentUser.getUserBranchCode());
								 //cs1.setCsDescription(bill.getBlDesc());
								 cs1.setCsDescription("As Per Bill File");
								 cs1.setCsDrCr('C');
								 cs1.setCsAmt(bill.getBlSubTot());
								 cs1.setCsType("BILL");
								 cs1.setCsTvNo(billNo);
								 cs1.setCsVouchType("CONTRA");
								 cs1.setCsFaCode(frghtCode);
								 cs1.setCsVouchNo(voucherNo);
								 cs1.setCsDt(css.getCssDt());
								 
								 cs1.setCsNo(css);
								 css.getCashStmtList().add(cs1);
								 session.merge(css);
								 
								 if(bill.getBlFinalTot() - bill.getBlSubTot() > 0){
									 CashStmt cs = new CashStmt();
									 cs.setUserCode(currentUser.getUserCode());
									 cs.setbCode(currentUser.getUserBranchCode());
									 //cs.setCsDescription(bill.getBlDesc());
									 cs.setCsDescription("As Per Bill File");
									 cs.setCsDrCr('C');
									 cs.setCsAmt(bill.getBlSerTax() + bill.getBlSwachBhCess()+bill.getBlKisanKalCess());
									 cs.setCsType("BILL");
									 cs.setCsTvNo(billNo);
									 cs.setCsVouchType("CONTRA");
									 cs.setCsFaCode(srvTaxCode);
									 cs.setCsVouchNo(voucherNo);
									 cs.setCsDt(css.getCssDt());
									 
									 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
									 cs.setCsNo(actCss);
									 actCss.getCashStmtList().add(cs);
									 session.merge(actCss);
								 }
								 
								 
								 
								 CashStmt cs2 = new CashStmt();
								 cs2.setUserCode(currentUser.getUserCode());
								 cs2.setbCode(currentUser.getUserBranchCode());
								 //cs2.setCsDescription(bill.getBlDesc());
								 cs2.setCsDescription("As Per Bill File");
								 cs2.setCsDrCr('D');
								 cs2.setCsAmt(bill.getBlFinalTot());
								 cs2.setCsType("BILL");
								 cs2.setCsTvNo(billNo);
								 cs2.setCsVouchType("CONTRA");
								 cs2.setCsFaCode(customer.getCustFaCode());
								 cs2.setCsVouchNo(voucherNo);
								 cs2.setCsDt(css.getCssDt());
								 
								 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
								 cs2.setCsNo(actCss);
								 actCss.getCashStmtList().add(cs2);
								 session.merge(actCss);
							 }
							 
						 }else{
							 bill.setBlBillNo(billNo);
							 if(billService.getBill().getBlGstNo() != null)	
								 bill.setBlStateGST(billService.getBill().getBlGstNo().substring(0, 2));
							 bill.setBlRemAmt(bill.getBlFinalTot());
							 bill.setUserCode(currentUser.getUserCode());
							 bill.setbCode(currentUser.getUserBranchCode());
							 int billId = (Integer) session.save(bill);
							 
							 if(!bdSerList.isEmpty()){
								 for(int i=0;i<bdSerList.size();i++){
									 BillDetService billDetService = bdSerList.get(i);
									 BillDetail billDetail = billDetService.getBillDetail();
									 Cnmt cnmt = billDetService.getCnmt();
									 Cnmt actCnmt = (Cnmt) session.get(Cnmt.class,cnmt.getCnmtId());
									 
									 billDetail.setUserCode(currentUser.getUserCode());
									 billDetail.setbCode(currentUser.getUserBranchCode());
									 billDetail.setBdCnmtId(cnmt.getCnmtId());
									 
									 Bill freshBill = (Bill) session.get(Bill.class,billId);
									 billDetail.setBill(freshBill);
									 int bdId = (Integer) session.save(billDetail);
									 freshBill.getBillDetList().add(billDetail);
									 session.merge(freshBill);
									 
									 System.out.println("bdId = "+bdId);
									 
									 if(actCnmt.getCnmtSupBillNo() == null || !actCnmt.getCnmtSupBillNo().isEmpty()){
										 ArrayList<String> list = new ArrayList<>();
										 list.add(billNo);
										 actCnmt.setCnmtSupBillNo(list);
									 }else{
										 actCnmt.getCnmtSupBillNo().add(billNo);
									 }
									 
									 if(actCnmt.getCnmtSupBillDet() == null || !actCnmt.getCnmtSupBillDet().isEmpty()){
										 ArrayList<String> list = new ArrayList<>();
										 list.add(String.valueOf(bdId));
										 actCnmt.setCnmtSupBillDet(list);
									 }else{
										 actCnmt.getCnmtSupBillDet().add(String.valueOf(bdId));
									 }
									 
									 session.merge(actCnmt);
								 }
							 }
							 
							 
							 int voucherNo = 0;
							 
							 cr = session.createCriteria(CashStmt.class);
							 cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,bill.getBlBillDt()));
							 cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
							 ProjectionList proList = Projections.projectionList();
							 proList.add(Projections.property("csVouchNo"));
							 cr.setProjection(proList);
								
							 List<Integer>  voucherNoList = cr.list();
							 if(!voucherNoList.isEmpty()){
									int vNo = voucherNoList.get(voucherNoList.size() - 1);
									voucherNo = vNo + 1;
							 }else{
									voucherNo = 1;
							 }
							 
							 
							 List<CashStmtStatus> cssList = new ArrayList<>();
							 cr = session.createCriteria(CashStmtStatus.class);
							 cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,currentUser.getUserBranchCode()));
							 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT,bill.getBlBillDt()));
							 cssList = cr.list();
							 
							 							 
							 if(!cssList.isEmpty()){
								 CashStmtStatus css = cssList.get(0);
								 
								 CashStmt cs1 = new CashStmt();
								 
								 cs1.setUserCode(currentUser.getUserCode());
								 cs1.setbCode(currentUser.getUserBranchCode());
								 //cs1.setCsDescription(bill.getBlDesc());
								 cs1.setCsDescription("As Per Bill File");
								 cs1.setCsDrCr('C');
								 cs1.setCsAmt(bill.getBlSubTot());
								 cs1.setCsType("BILL");
								 cs1.setCsTvNo(billNo);
								 cs1.setCsVouchType("CONTRA");
								 cs1.setCsFaCode(frghtCode);
								 cs1.setCsVouchNo(voucherNo);
								 cs1.setCsDt(css.getCssDt());
								 cs1.setCsNo(css);
								 css.getCashStmtList().add(cs1);
								 session.merge(css);
								 
								 if(bill.getBlFinalTot() - bill.getBlSubTot() > 0){
									 CashStmt cs = new CashStmt();
									 cs.setUserCode(currentUser.getUserCode());
									 cs.setbCode(currentUser.getUserBranchCode());
									 //cs.setCsDescription(bill.getBlDesc());
									 cs.setCsDescription("As Per Bill File");
									 cs.setCsDrCr('C');
									 cs.setCsAmt(bill.getBlSerTax() + bill.getBlSwachBhCess()+bill.getBlKisanKalCess());
									 cs.setCsType("BILL");
									 cs.setCsTvNo(billNo);
									 cs.setCsVouchType("CONTRA");
									 cs.setCsFaCode(frghtCode);
									 cs.setCsVouchNo(voucherNo);
									 cs.setCsDt(css.getCssDt());
									 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
									 cs.setCsNo(actCss);
									 actCss.getCashStmtList().add(cs);
									 session.merge(actCss);
								 }
								
								 
								 CashStmt cs2 = new CashStmt();
								 cs2.setUserCode(currentUser.getUserCode());
								 cs2.setbCode(currentUser.getUserBranchCode());
								 //cs2.setCsDescription(bill.getBlDesc());
								 cs2.setCsDescription("As Per Bill File");
								 cs2.setCsDrCr('D');
								 cs2.setCsAmt(bill.getBlFinalTot());
								 cs2.setCsType("BILL");
								 cs2.setCsTvNo(billNo);
								 cs2.setCsVouchType("CONTRA");
								 cs2.setCsFaCode(customer.getCustFaCode());
								 cs2.setCsVouchNo(voucherNo);
								 cs2.setCsDt(css.getCssDt());
								 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
								 cs2.setCsNo(actCss);
								 actCss.getCashStmtList().add(cs2);
								 
								 session.merge(actCss);
							 }
							 
						 }
					 }
					
				 }else{
					 System.out.println("invalid customer or branch for billing");
				 }

		 return billNo;
	 }
	
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<Bill> getCustBill(int custId){
		 System.out.println("enter into getCustBill function");
		 List<Bill> billList = new ArrayList<>();
		 try{
			 session = this.sessionFactory.openSession();
			 Criteria cr = session.createCriteria(Bill.class);
			 cr.add(Restrictions.eq(BillCNTS.BILL_CUST_ID,custId));
			 cr.add(Restrictions.eq(BillCNTS.BILL_CANCEL,false));
			 cr.add(Restrictions.isNull(BillCNTS.BILL_BF));
			 billList = cr.list();
		 }catch(Exception e){
			 e.printStackTrace();
		 }finally{
			 session.clear();
			 session.close();
		 }
		 return billList;
	 }
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<Bill> getPendingBillFrMr(int custId){
		 System.out.println("enter into getPendingBillFrMr function");
		 List<Bill> blList = new ArrayList<Bill>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr = session.createCriteria(Bill.class);
			 cr.add(Restrictions.eq(BillCNTS.BILL_CUST_ID,custId));
			 cr.add(Restrictions.eq(BillCNTS.BILL_CANCEL,false));
			 cr.add(Restrictions.gt(BillCNTS.BILL_REM_AMT,0.0));
			 blList = cr.list();
			 
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return blList;
	 }
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<Map<String,Object>> getBlFrPrint(int brhId, String frBlNo, String toBlNo){
		 System.out.println("enter into getBlFrPrint function");
		 List<Map<String,Object>> blList = new ArrayList<Map<String,Object>>();
		 List<Bill> billList = new ArrayList<Bill>();
		 int frBl = Integer.parseInt(frBlNo.substring(3));
		 int toBl = Integer.parseInt(toBlNo.substring(3));
		 System.out.println("frBl = "+frBl);
		 System.out.println("toBl = "+toBl);
		 System.out.println("brhId="+brhId);
		 try{
			 session = this.sessionFactory.openSession();
			 Criteria cr = session.createCriteria(Bill.class);
			 cr.add(Restrictions.eq(BillCNTS.BILL_BRH_ID,brhId));
			 cr.add(Restrictions.ge(BillCNTS.Bill_NO, frBlNo));
			 cr.add(Restrictions.le(BillCNTS.Bill_NO, toBlNo));
			 billList = cr.list();
			 System.out.println("billList.isEmpty()="+billList.isEmpty());
			 System.out.println(billList.size());
			 if(!billList.isEmpty()){
				 NumToWords w = new NumToWords();
				 for(int i=0;i<billList.size();i++){
					 System.out.println("billNo="+billList.get(i).getBlBillNo());
				//	 System.out.println(Integer.parseInt(billList.get(i).getBlBillNo().substring(3)));
					 //if(Integer.parseInt(billList.get(i).getBlBillNo().substring(3)) >= frBl && Integer.parseInt(billList.get(i).getBlBillNo().substring(3)) <= toBl){
						 Map<String,Object> map = new HashMap<String, Object>();
						 map.put("bill", billList.get(i));
						 System.out.println("billList.get(i).getBlCustId()="+billList.get(i).getBlCustId());
						 
						// String stateRelPrefix;
						 
						 
						 if(billList.get(i).getBlCustId() == 595 || billList.get(i).getBlCustId() == 597 || billList.get(i).getBlCustId() == 609 || billList.get(i).getBlCustId() == 601
									|| billList.get(i).getBlCustId() == 617 || billList.get(i).getBlCustId() == 623 || billList.get(i).getBlCustId() == 614 || billList.get(i).getBlCustId() == 697
									|| billList.get(i).getBlCustId() == 685 || billList.get(i).getBlCustId() == 678 || billList.get(i).getBlCustId() == 699 || billList.get(i).getBlCustId() == 668
									|| billList.get(i).getBlCustId() == 820 || billList.get(i).getBlCustId() == 859 || billList.get(i).getBlCustId() == 932 || billList.get(i).getBlCustId() == 983
								 	|| billList.get(i).getBlCustId() == 1034 || billList.get(i).getBlCustId() == 1381 || billList.get(i).getBlCustId() == 1754 || billList.get(i).getBlCustId() == 1755
								 	 || billList.get(i).getBlCustId() == 1756){
							 
							 Criteria criteria= session.createCriteria(RelBillAddress.class)
									 .add(Restrictions.eq("gstNo", billList.get(i).getBlGstNo()));
							 
							 RelBillAddress customer = (RelBillAddress) criteria.list().get(0);
							 map.put("cust",customer);
							 
//							State list=(State) session.createCriteria(State.class)
//									 .add(Restrictions.eq(StateCNTS.STATE_CODE, customer.getStateCode()))
//									 .list().get(0);
//							stateRelPrefix=list.getStateRelPrefix();
//							
//							map.put("toStatePre", stateRelPrefix);
							
						 }else if(billList.get(i).getBlCustId() == 13){
							 Criteria criteria= session.createCriteria(HilAddress.class)
									 .add(Restrictions.eq("gstNo", billList.get(i).getBlGstNo()));
							 
							 HilAddress customer = (HilAddress) criteria.list().get(0);
							 map.put("cust",customer);
							 
						 }else{
							 Customer  customer = (Customer) session.get(Customer.class,billList.get(i).getBlCustId());
							 map.put("cust",customer);
							 //map.put("toStatePre", "");
						 }
						 
						 
						 
						 
						 if(billList.get(i).getBillForwarding() != null){
							 System.out.println("*************************************");
							 map.put("bfNo",billList.get(i).getBillForwarding().getBfNo());
							 map.put("bfDt",billList.get(i).getBillForwarding().getCreationTS().getTime());
						 }
						 
						   int customerId=billList.get(i).getBlCustId();
						     List<String> billFormateName=Arrays.asList(BillFormateNameCNTS.foramteName);
						      
						 /*if(customer.getCustId() == 126 || customer.getCustId() == 477){
							 //Hindalco
							 map.put("simBillFormat",false);
							 map.put("hndlBillFormat",true);
							 map.put("relBillFormat",false);
							 map.put("renBillFormat",false);
							 map.put("bhuBillFormat",false);
						 }else */if(customerId == 595 || customerId == 597 || customerId == 609 || customerId == 601
								|| customerId == 617 || customerId == 623 || customerId == 614 || customerId == 697
								|| customerId == 685 || customerId == 678 || customerId == 699 || customerId == 668
								|| customerId == 820 || customerId == 859 || customerId == 932 || customerId == 983
							 	|| customerId == 1034 || customerId == 1381 || billList.get(i).getBlCustId() == 1754 || billList.get(i).getBlCustId() == 1755
							 	 || billList.get(i).getBlCustId() == 1756){
							 //Reliance 
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.REL_BI_FORMAT)){			//"relBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						
						 }else if(customerId == 1 || customerId == 477 || customerId == 126){
							 //Bhushan
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.BHU_BI_FORMAT)){					//"bhuBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
							 
						 }else if(customerId == 10){
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.HIL_BI_FORMAT)){						//"hilBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 1702){
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.CHE_SKI_CA_BI_FORMAT)){						//"hilBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 552){
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.MUM_SKI_CA_BI_FORMAT)){						//"hilBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 447){
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.DAM_SHIP_BI_FORMAT)){						//"hilBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 153 || customerId == 166 || customerId == 154){
							 //faridabad customer
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.KWA_BI_FORMAT)){						//"hilBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 466 || customerId == 882 || customerId == 958 ){//|| customerId == 2413
									 //Adani Wilmare lucknow jaipur alwar karnal
									 Iterator<String> iterator=billFormateName.iterator();
									  while(iterator.hasNext()){
										  String name=iterator.next();
										  if(name.equals(BillFormateNameCNTS.ADANI_WIL_JAI_LUC_ALW_BI_FORMAT)){
											  map.put(name,true);
											  System.err.println("********");
											  System.out.println("Bill Format = "+name);
											  System.err.println("********");
										  }else
											  map.put(name,false);
									  }
						 }else if(customerId == 550){ 
							 //HIL mumbai customer
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.HIL_MUM_BI_FORMAT)){
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 318){ 
							 //haldia customer
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.ADA_BI_FORMAT)){							//"adaniBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 18){ 
							 //Ruchi soya customer
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.RUC_SO_BI_FORMAT)){							//"adaniBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 43){ 
							 //Bhilai customer
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.BHI_BI_FORMAT)){							//"adaniBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 1852){
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.PASSIVE_BI_FORMAT)){						//"hilBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 1185){ 
							 //Himachal Futuristic customer
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.HIM_FR_BI_FORMAT)){							//"adaniBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 } else if(customerId==290 ||customerId==326){					//Singaruli's customer 
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if((name.equals(BillFormateNameCNTS.HID_MA_BI_FORMAT)&&customerId==290)||(name.equals(BillFormateNameCNTS.HID_BI_FORMAT)&&customerId==326)){			
									  map.put(name,true);			// "hidalcoMahanBillFormat"||"hindalcoBillFormat"
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");									  
								  }else
									  map.put(name,false);
							  }
							 }else if(customerId==150||customerId==46||customerId==659){ //Raigardh's customer
							   Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if((name.equals(BillFormateNameCNTS.JIN_ST_BI_FORMAT)&&customerId==150)||(name.equals(BillFormateNameCNTS.MSP_ST_BI_FORMAT)&&customerId==46)){
									  map.put(name,true);		//"jindalSteelBillFormat"||"mspSteelBillFormat"
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else if(name.equals(BillFormateNameCNTS.SKS_PO_BI_FORMAT)&&customerId==659){			
									  map.put(name, true);				//"sksPowerGenBillFormat"
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else{ 
							 if(brhId == 4 || brhId == 16){
								if(brhId==4&&customerId==7){ //Renukoot's customer..
									
									 Iterator<String> iterator=billFormateName.iterator();
									  while(iterator.hasNext()){
										  String name=iterator.next();
										  if(name.equals(BillFormateNameCNTS.SKI_CA_BI_FORMAT)){							//"skiCarbonBillFormate"
											  map.put(name,true);
											  System.err.println("********");
											  System.out.println("Bill Format = "+name);
											  System.err.println("********");
										  }else
											  map.put(name,false);
									  }
									
								}else if(brhId==4&&customerId==248){ //Renukoot's customer..
									
									 Iterator<String> iterator=billFormateName.iterator();
									  while(iterator.hasNext()){
										  String name=iterator.next();
										  if(name.equals(BillFormateNameCNTS.GRA_IN_BI_FORMAT)){							//"grasimIndusBillFormat"
											  map.put(name,true);
											  System.err.println("********");
											  System.out.println("Bill Format = "+name);
											  System.err.println("********");
										  }else
											  map.put(name,false);
									  }
									
								}else{
								 Iterator<String> iterator=billFormateName.iterator();
								  while(iterator.hasNext()){
									  String name=iterator.next();
									  if(name.equals(BillFormateNameCNTS.REN_BI_FORMAT)){				//"renBillFormat"
										  map.put(name,true);
										  System.err.println("********");
										  System.out.println("Bill Format = "+name);
										  System.err.println("********");
									  }else
										  map.put(name,false);
								  }
								}
								 }
							 	 else{
							 		Iterator<String> iterator=billFormateName.iterator();
									  while(iterator.hasNext()){
										  String name=iterator.next();
										  if(name.equals(BillFormateNameCNTS.SIM_BI_FORMAT)){			//"simBillFormat"
											  map.put(name,true);
											  System.err.println("********");
											  System.out.println("Bill Format = "+name);
											  System.err.println("********");
										  }else
											  map.put(name,false);
									  } 
							 		 
							 		 
							 /*		 
								 map.put("mspSteelBillFormat",false);
								 map.put("simBillFormat",true);
								 map.put("hndlBillFormat",false);
								 map.put("relBillFormat",false);
								 map.put("renBillFormat",false);
								 map.put("bhuBillFormat",false);
								 map.put("hilBillFormat",false);
								 map.put("adaniBillFormat",false);
								 map.put("jindalSteelBillFormat",false);
								 map.put("sksPowerGenBillFormat",false);
								 map.put("hidalcoMahanBillFormat",false); */
							 }
						 }
						 
						 Double bdTotalWt=0.0;
						 Double bdTotalRt=0.0;
						 Double bdTotalFrt=0.0;
						 Double bdTotalOthChrg=0.0;
						 Double totOvrHgt = 0.0;
						 Double totAdvance = 0.0;
						 
						 List<Map<String,Object>> blDetList = new ArrayList<Map<String,Object>>();
						 for(int j=0;j<billList.get(i).getBillDetList().size();j++){
							 Map<String,Object> billDetMap = new HashMap<String, Object>();
							 Cnmt cnmt = (Cnmt) session.get(Cnmt.class,billList.get(i).getBillDetList().get(j).getBdCnmtId());
							 billDetMap.put("billDet",billList.get(i).getBillDetList().get(j));
							 billDetMap.put("cnmt", cnmt);
							 
							 List<Cnmt_Challan> ccList = new ArrayList<Cnmt_Challan>();
							 cr = session.createCriteria(Cnmt_Challan.class);
							 cr.add(Restrictions.eq(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId,cnmt.getCnmtId()));
							 ccList = cr.list();
							 
							 if(!ccList.isEmpty()){
								 Challan challan = (Challan) session.get(Challan.class,ccList.get(ccList.size() - 1).getChlnId());
								 billDetMap.put("vehType",challan.getChlnVehicleType());
								 if(challan.getChlnArId() > 0){
									 System.out.println("*************************** 1");
									 ArrivalReport arRep = (ArrivalReport) session.get(ArrivalReport.class,challan.getChlnArId());
									 System.out.println("arRep.getArPkg() = "+arRep.getArPkg());
									 System.out.println("arRep.getArRepDt() = "+arRep.getArRepDt());
									 System.out.println("arRep.getArOvrHgt() = "+arRep.getArOvrHgt());
									 billDetMap.put("delQty",arRep.getArPkg());
									 billDetMap.put("delDt",arRep.getArRepDt());
									 billDetMap.put("ovrHgt",arRep.getArOvrHgt());
									 billDetMap.put("arDt",arRep.getArDt());
									 totOvrHgt=totOvrHgt+arRep.getArOvrHgt();
								 }
								 if(challan.getChlnLryNo() != null && !challan.getChlnLryNo().trim().equals("")){
									 billDetMap.put("truckNo",challan.getChlnLryNo());
								 }else{
									 List<ChallanDetail> chdList = new ArrayList<ChallanDetail>();
									 cr = session.createCriteria(ChallanDetail.class);
									 cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE,challan.getChlnCode()));
									 chdList = cr.list();
									 if(!chdList.isEmpty()){
										 billDetMap.put("truckNo",chdList.get(0).getChdRcNo());
									 }
								 }
							 }
							 
							 
							 Customer consignee = (Customer) session.get(Customer.class,Integer.parseInt(cnmt.getCnmtConsignee()));
							 Customer consignor = (Customer) session.get(Customer.class,Integer.parseInt(cnmt.getCnmtConsignor()));
							 billDetMap.put("consignee",consignee);
							 billDetMap.put("consignor",consignor);
							 
							 
							 
							 
							 Station frStn = (Station) session.get(Station.class,Integer.parseInt(cnmt.getCnmtFromSt()));
							 Station toStn = (Station) session.get(Station.class,Integer.parseInt(cnmt.getCnmtToSt()));
							 billDetMap.put("frmStn",frStn.getStnName());
							 billDetMap.put("toStn",toStn.getStnName());
							 billDetMap.put("toStnStatePre",toStn.getState().getStateRelPrefix());
							 
							 
							 // for Adani for jaipur and lucknow
							 if(customerId == 466 || customerId == 882 ){ // || customerId == 2413
								 if(j==0){
									 if(cnmt.getCnmtConsignee().equalsIgnoreCase("466") || cnmt.getCnmtConsignee().equalsIgnoreCase("882")){ // || cnmt.getCnmtConsignee().equalsIgnoreCase("2413")
										 map.put("adaniStn",toStn.getStnName());
									 }else{
										 map.put("adaniStn",frStn.getStnName());
									 }
								 }
							 }
							 // for alwar
							 if(customerId == 958){
								 map.put("adaniStn",frStn.getStnName());
//								 map.put("adaniStn",toStn.getStnName());
							 }

							 
							 bdTotalRt=bdTotalRt+billList.get(i).getBillDetList().get(j).getBdRate();
							 bdTotalWt=bdTotalWt+billList.get(i).getBillDetList().get(j).getBdChgWt();
							 bdTotalFrt=bdTotalFrt+billList.get(i).getBillDetList().get(j).getBdFreight();
							 bdTotalOthChrg=bdTotalOthChrg+billList.get(i).getBillDetList().get(j).getBdOthChgAmt();
							 totAdvance=totAdvance+billList.get(i).getBillDetList().get(j).getBdCnmtAdv();
							 
							 blDetList.add(billDetMap);
						 }
						 map.put("blDetList",blDetList);
						 map.put("totCnmtCount",blDetList.size());
						 System.out.println("billList.get(i).getBlFinalTot()="+billList.get(i).getBlFinalTot());
						// String amt = String.valueOf(billList.get(i).getBlFinalTot());
						 
						 map.put("totWt",bdTotalWt/1000);
						 map.put("totRt",bdTotalRt*1000);
						 map.put("totFrt",bdTotalFrt);
						 map.put("othChrg",bdTotalOthChrg);
						 map.put("totOvrHgt", totOvrHgt);
						 map.put("totAdvance", totAdvance);

						 if(billList.get(i).getBlGstNo() != null){
							 if(billList.get(i).getBlCgst()>0 || billList.get(i).getBlSgst() >0){
								 map.put("cGST", Math.round((billList.get(i).getBlFinalTot()*0.025)*100.0)/100.0);
								 map.put("sGST", Math.round((billList.get(i).getBlFinalTot()*0.025)*100.0)/100.0);
								 map.put("iGST", 0);
							 }else if(billList.get(i).getBlIgst()>0){
								 map.put("cGST", 0);
								 map.put("sGST", 0);
								 map.put("iGST", Math.round((billList.get(i).getBlFinalTot()*0.05)*100.0)/100.0);
							 }else{
								 map.put("cGST", 0);
								 map.put("sGST", 0);
								 map.put("iGST", 0);
							 }
						 }
						
						 
						 
						 
						 double d=billList.get(i).getBlFinalTot();
						
						 String amt=String.format("%.2f",billList.get(i).getBlFinalTot() - totAdvance);
						 System.out.println("amt="+amt);
						 int rupees = Integer.parseInt(amt.split("\\.")[0]);
						 System.out.println("rupees="+rupees);
						 String str1 = w.convert(rupees);
						 str1 += " Rupees ";
						 System.out.println("str1="+str1);
						 int paise = Integer.parseInt(amt.split("\\.")[1]);
						// int paise = new Double(Double.parseDouble(amt.split("\\.")[1])).intValue();
						System.out.println("paise="+paise);
						 String str2 = "";
						 if (paise != 0) {
							   str2 += " and";
							   str2 = w.convert(paise);
							   str2 += " Paise";
						 }
						 String str3 = str1 + str2 + "only";
						 System.out.println("str3="+str3);
						 map.put("bilAmt",str3);
						 
						 
						 
						 
						 String gstAmt = String.valueOf(billList.get(i).getBlFinalTot()* 0.05);
						 int rupee = Integer.parseInt(gstAmt.split("\\.")[0]);
						 String strg1 = w.convert(rupee);
						 strg1 += " Rupees ";
						 int paisa = new Double(Double.parseDouble(gstAmt.split("\\.")[1])).intValue();
						 String strg2 = "";
						 if (paise != 0) { 
							   strg2 += " and";
							   strg2 = w.convert(paisa);
							   strg2 += " Paise";
						 }
						 String strg3 = strg1 + strg2 + "only";
						 
						 map.put("gstAmt",strg3);
						 
						 
						 
						 blList.add(map);
						 
						
				 }
			 }
			 
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return blList;
	 }
	 
	 
	 @SuppressWarnings("unchecked")
	 public List<Map<String,Object>> getSingleBlPrint(int brhId, String blNo){
		 System.out.println("enter into getSingleBlPrint function");
		 List<Map<String,Object>> blList = new ArrayList<Map<String,Object>>();
		 List<Bill> billList = new ArrayList<Bill>();
		 try{
			 session = this.sessionFactory.openSession();
			 Criteria cr = session.createCriteria(Bill.class);
			 cr.add(Restrictions.eq(BillCNTS.BILL_BRH_ID,brhId));
			 cr.add(Restrictions.eq(BillCNTS.Bill_NO,blNo));
			 billList = cr.list();
			 
			 if(!billList.isEmpty()){
				 NumToWords w = new NumToWords();
				 for(int i=0;i<billList.size();i++){
					 System.out.println("billNo="+billList.get(i).getBlBillNo());
				//	 System.out.println(Integer.parseInt(billList.get(i).getBlBillNo().substring(3)));
					 //if(Integer.parseInt(billList.get(i).getBlBillNo().substring(3)) >= frBl && Integer.parseInt(billList.get(i).getBlBillNo().substring(3)) <= toBl){
						 Map<String,Object> map = new HashMap<String, Object>();
						 map.put("bill", billList.get(i));
						 System.out.println("billList.get(i).getBlCustId()="+billList.get(i).getBlCustId());
						 
						// String stateRelPrefix;
						 
						 
						 if(billList.get(i).getBlCustId() == 595 || billList.get(i).getBlCustId() == 597 || billList.get(i).getBlCustId() == 609 || billList.get(i).getBlCustId() == 601
									|| billList.get(i).getBlCustId() == 617 || billList.get(i).getBlCustId() == 623 || billList.get(i).getBlCustId() == 614 || billList.get(i).getBlCustId() == 697
									|| billList.get(i).getBlCustId() == 685 || billList.get(i).getBlCustId() == 678 || billList.get(i).getBlCustId() == 699 || billList.get(i).getBlCustId() == 668
									|| billList.get(i).getBlCustId() == 820 || billList.get(i).getBlCustId() == 859 || billList.get(i).getBlCustId() == 932 || billList.get(i).getBlCustId() == 983
								 	|| billList.get(i).getBlCustId() == 1034 || billList.get(i).getBlCustId() == 1381 || billList.get(i).getBlCustId() == 1754 || billList.get(i).getBlCustId() == 1755
								 	 || billList.get(i).getBlCustId() == 1756){
							 
							 Criteria criteria= session.createCriteria(RelBillAddress.class)
									 .add(Restrictions.eq("gstNo", billList.get(i).getBlGstNo()));
							 
							 RelBillAddress customer = (RelBillAddress) criteria.list().get(0);
							 map.put("cust",customer);
							 
//							State list=(State) session.createCriteria(State.class)
//									 .add(Restrictions.eq(StateCNTS.STATE_CODE, customer.getStateCode()))
//									 .list().get(0);
//							stateRelPrefix=list.getStateRelPrefix();
//							
//							map.put("toStatePre", stateRelPrefix);
							
						 }else if(billList.get(i).getBlCustId() == 13){
							 Criteria criteria= session.createCriteria(HilAddress.class)
									 .add(Restrictions.eq("gstNo", billList.get(i).getBlGstNo()));
							 
							 HilAddress customer = (HilAddress) criteria.list().get(0);
							 map.put("cust",customer);
							 
						 }else{
							 Customer  customer = (Customer) session.get(Customer.class,billList.get(i).getBlCustId());
							 map.put("cust",customer);
							 //map.put("toStatePre", "");
						 }
						 
						 
						 
						 
						 if(billList.get(i).getBillForwarding() != null){
							 System.out.println("*************************************");
							 map.put("bfNo",billList.get(i).getBillForwarding().getBfNo());
							 map.put("bfDt",billList.get(i).getBillForwarding().getCreationTS().getTime());
						 }
						 
						   int customerId=billList.get(i).getBlCustId();
						     List<String> billFormateName=Arrays.asList(BillFormateNameCNTS.foramteName);
						      
						 /*if(customer.getCustId() == 126 || customer.getCustId() == 477){
							 //Hindalco
							 map.put("simBillFormat",false);
							 map.put("hndlBillFormat",true);
							 map.put("relBillFormat",false);
							 map.put("renBillFormat",false);
							 map.put("bhuBillFormat",false);
						 }else */if(customerId == 595 || customerId == 597 || customerId == 609 || customerId == 601
								|| customerId == 617 || customerId == 623 || customerId == 614 || customerId == 697
								|| customerId == 685 || customerId == 678 || customerId == 699 || customerId == 668
								|| customerId == 820 || customerId == 859 || customerId == 932 || customerId == 983
							 	|| customerId == 1034 || customerId == 1381 || billList.get(i).getBlCustId() == 1754 || billList.get(i).getBlCustId() == 1755
							 	 || billList.get(i).getBlCustId() == 1756){
							 //Reliance 
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.REL_BI_FORMAT)){			//"relBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						
						 }else if(customerId == 1 || customerId == 477 || customerId == 126){
							 //Bhushan
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.BHU_BI_FORMAT)){					//"bhuBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
							 
						 }else if(customerId == 10){
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.HIL_BI_FORMAT)){						//"hilBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 1702){
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.CHE_SKI_CA_BI_FORMAT)){						//"hilBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 552){
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.MUM_SKI_CA_BI_FORMAT)){						//"hilBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 447){
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.DAM_SHIP_BI_FORMAT)){						//"hilBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 153 || customerId == 166 || customerId == 154){
							 //faridabad customer
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.KWA_BI_FORMAT)){						//"hilBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 466 || customerId == 882 || customerId == 958 ){ //|| customerId == 2413
									 //Adani Wilmare lucknow jaipur alwar karnal
									 Iterator<String> iterator=billFormateName.iterator();
									  while(iterator.hasNext()){
										  String name=iterator.next();
										  if(name.equals(BillFormateNameCNTS.ADANI_WIL_JAI_LUC_ALW_BI_FORMAT)){
											  map.put(name,true);
											  System.err.println("********");
											  System.out.println("Bill Format = "+name);
											  System.err.println("********");
										  }else
											  map.put(name,false);
									  }
						 }else if(customerId == 550){ 
							 //HIL mumbai customer
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.HIL_MUM_BI_FORMAT)){
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 318){ 
							 //haldia customer
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.ADA_BI_FORMAT)){							//"adaniBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 18){ 
							 //Ruchi soya customer
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.RUC_SO_BI_FORMAT)){							//"adaniBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 43){ 
							 //Bhilai customer
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.BHI_BI_FORMAT)){							//"adaniBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 1852){
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.PASSIVE_BI_FORMAT)){						//"hilBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else if(customerId == 1185){ 
							 //Himachal Futuristic customer
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if(name.equals(BillFormateNameCNTS.HIM_FR_BI_FORMAT)){							//"adaniBillFormat"
									  map.put(name,true);
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 } else if(customerId==290 ||customerId==326){					//Singaruli's customer 
							 Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if((name.equals(BillFormateNameCNTS.HID_MA_BI_FORMAT)&&customerId==290)||(name.equals(BillFormateNameCNTS.HID_BI_FORMAT)&&customerId==326)){			
									  map.put(name,true);			// "hidalcoMahanBillFormat"||"hindalcoBillFormat"
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");									  
								  }else
									  map.put(name,false);
							  }
							 }else if(customerId==150||customerId==46||customerId==659){ //Raigardh's customer
							   Iterator<String> iterator=billFormateName.iterator();
							  while(iterator.hasNext()){
								  String name=iterator.next();
								  if((name.equals(BillFormateNameCNTS.JIN_ST_BI_FORMAT)&&customerId==150)||(name.equals(BillFormateNameCNTS.MSP_ST_BI_FORMAT)&&customerId==46)){
									  map.put(name,true);		//"jindalSteelBillFormat"||"mspSteelBillFormat"
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else if(name.equals(BillFormateNameCNTS.SKS_PO_BI_FORMAT)&&customerId==659){			
									  map.put(name, true);				//"sksPowerGenBillFormat"
									  System.err.println("********");
									  System.out.println("Bill Format = "+name);
									  System.err.println("********");
								  }else
									  map.put(name,false);
							  }
						 }else{ 
							 if(brhId == 4 || brhId == 16){
								if(brhId==4&&customerId==7){ //Renukoot's customer..
									
									 Iterator<String> iterator=billFormateName.iterator();
									  while(iterator.hasNext()){
										  String name=iterator.next();
										  if(name.equals(BillFormateNameCNTS.SKI_CA_BI_FORMAT)){							//"skiCarbonBillFormate"
											  map.put(name,true);
											  System.err.println("********");
											  System.out.println("Bill Format = "+name);
											  System.err.println("********");
										  }else
											  map.put(name,false);
									  }
									
								}else if(brhId==4&&customerId==248){ //Renukoot's customer..
									
									 Iterator<String> iterator=billFormateName.iterator();
									  while(iterator.hasNext()){
										  String name=iterator.next();
										  if(name.equals(BillFormateNameCNTS.GRA_IN_BI_FORMAT)){							//"grasimIndusBillFormat"
											  map.put(name,true);
											  System.err.println("********");
											  System.out.println("Bill Format = "+name);
											  System.err.println("********");
										  }else
											  map.put(name,false);
									  }
									
								}else{
								 Iterator<String> iterator=billFormateName.iterator();
								  while(iterator.hasNext()){
									  String name=iterator.next();
									  if(name.equals(BillFormateNameCNTS.REN_BI_FORMAT)){				//"renBillFormat"
										  map.put(name,true);
										  System.err.println("********");
										  System.out.println("Bill Format = "+name);
										  System.err.println("********");
									  }else
										  map.put(name,false);
								  }
								}
								 }
							 	 else{
							 		Iterator<String> iterator=billFormateName.iterator();
									  while(iterator.hasNext()){
										  String name=iterator.next();
										  if(name.equals(BillFormateNameCNTS.SIM_BI_FORMAT)){			//"simBillFormat"
											  map.put(name,true);
											  System.err.println("********");
											  System.out.println("Bill Format = "+name);
											  System.err.println("********");
										  }else
											  map.put(name,false);
									  } 
							 		 
							 		 
							 /*		 
								 map.put("mspSteelBillFormat",false);
								 map.put("simBillFormat",true);
								 map.put("hndlBillFormat",false);
								 map.put("relBillFormat",false);
								 map.put("renBillFormat",false);
								 map.put("bhuBillFormat",false);
								 map.put("hilBillFormat",false);
								 map.put("adaniBillFormat",false);
								 map.put("jindalSteelBillFormat",false);
								 map.put("sksPowerGenBillFormat",false);
								 map.put("hidalcoMahanBillFormat",false); */
							 }
						 }
						 
						 Double bdTotalWt=0.0;
						 Double bdTotalRt=0.0;
						 Double bdTotalFrt=0.0;
						 Double bdTotalOthChrg=0.0;
						 Double totOvrHgt = 0.0;
						 Double totAdvance = 0.0;
						 
						 List<Map<String,Object>> blDetList = new ArrayList<Map<String,Object>>();
						 for(int j=0;j<billList.get(i).getBillDetList().size();j++){
							 Map<String,Object> billDetMap = new HashMap<String, Object>();
							 Cnmt cnmt = (Cnmt) session.get(Cnmt.class,billList.get(i).getBillDetList().get(j).getBdCnmtId());
							 billDetMap.put("billDet",billList.get(i).getBillDetList().get(j));
							 billDetMap.put("cnmt", cnmt);
							 
							 List<Cnmt_Challan> ccList = new ArrayList<Cnmt_Challan>();
							 cr = session.createCriteria(Cnmt_Challan.class);
							 cr.add(Restrictions.eq(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId,cnmt.getCnmtId()));
							 ccList = cr.list();
							 
							 if(!ccList.isEmpty()){
								 Challan challan = (Challan) session.get(Challan.class,ccList.get(ccList.size() - 1).getChlnId());
								 billDetMap.put("vehType",challan.getChlnVehicleType());
								 if(challan.getChlnArId() > 0){
									 System.out.println("*************************** 1");
									 ArrivalReport arRep = (ArrivalReport) session.get(ArrivalReport.class,challan.getChlnArId());
									 System.out.println("arRep.getArPkg() = "+arRep.getArPkg());
									 System.out.println("arRep.getArRepDt() = "+arRep.getArRepDt());
									 System.out.println("arRep.getArOvrHgt() = "+arRep.getArOvrHgt());
									 billDetMap.put("delQty",arRep.getArPkg());
									 billDetMap.put("delDt",arRep.getArRepDt());
									 billDetMap.put("ovrHgt",arRep.getArOvrHgt());
									 billDetMap.put("arDt",arRep.getArDt());
									 totOvrHgt=totOvrHgt+arRep.getArOvrHgt();
								 }
								 if(challan.getChlnLryNo() != null && !challan.getChlnLryNo().trim().equals("")){
									 billDetMap.put("truckNo",challan.getChlnLryNo());
								 }else{
									 List<ChallanDetail> chdList = new ArrayList<ChallanDetail>();
									 cr = session.createCriteria(ChallanDetail.class);
									 cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE,challan.getChlnCode()));
									 chdList = cr.list();
									 if(!chdList.isEmpty()){
										 billDetMap.put("truckNo",chdList.get(0).getChdRcNo());
									 }
								 }
							 }
							 
							 
							 Customer consignee = (Customer) session.get(Customer.class,Integer.parseInt(cnmt.getCnmtConsignee()));
							 Customer consignor = (Customer) session.get(Customer.class,Integer.parseInt(cnmt.getCnmtConsignor()));
							 billDetMap.put("consignee",consignee);
							 billDetMap.put("consignor",consignor);
							 
							 
							 
							 
							 Station frStn = (Station) session.get(Station.class,Integer.parseInt(cnmt.getCnmtFromSt()));
							 Station toStn = (Station) session.get(Station.class,Integer.parseInt(cnmt.getCnmtToSt()));
							 billDetMap.put("frmStn",frStn.getStnName());
							 billDetMap.put("toStn",toStn.getStnName());
							 billDetMap.put("toStnStatePre",toStn.getState().getStateRelPrefix());
							 
							 
							 // for Adani for jaipur , lucknow and karnal
							 if(customerId == 466 || customerId == 882 ){ //|| customerId == 2413 
								 if(j==0){
									 if(cnmt.getCnmtConsignee().equalsIgnoreCase("466") || cnmt.getCnmtConsignee().equalsIgnoreCase("882") ){//|| cnmt.getCnmtConsignee().equalsIgnoreCase("2413")
										 map.put("adaniStn",toStn.getStnName());
									 }else{
										 map.put("adaniStn",frStn.getStnName());
									 }
								 }
							 }
							 // for alwar
							 if(customerId == 958){
								 map.put("adaniStn",frStn.getStnName());
//								 map.put("adaniStn",toStn.getStnName());
							 }

							 
							 bdTotalRt=bdTotalRt+billList.get(i).getBillDetList().get(j).getBdRate();
							 bdTotalWt=bdTotalWt+billList.get(i).getBillDetList().get(j).getBdChgWt();
							 bdTotalFrt=bdTotalFrt+billList.get(i).getBillDetList().get(j).getBdFreight();
							 bdTotalOthChrg=bdTotalOthChrg+billList.get(i).getBillDetList().get(j).getBdOthChgAmt();
							 totAdvance=totAdvance+billList.get(i).getBillDetList().get(j).getBdCnmtAdv();
							 
							 blDetList.add(billDetMap);
						 }
						 map.put("blDetList",blDetList);
						 map.put("totCnmtCount",blDetList.size());
						 System.out.println("billList.get(i).getBlFinalTot()="+billList.get(i).getBlFinalTot());
						// String amt = String.valueOf(billList.get(i).getBlFinalTot());
						 
						 map.put("totWt",bdTotalWt/1000);
						 map.put("totRt",bdTotalRt*1000);
						 map.put("totFrt",bdTotalFrt);
						 map.put("othChrg",bdTotalOthChrg);
						 map.put("totOvrHgt", totOvrHgt);
						 map.put("totAdvance", totAdvance);

						 if(billList.get(i).getBlGstNo() != null){
							 if(billList.get(i).getBlCgst()>0 || billList.get(i).getBlSgst() >0){
								 map.put("cGST", Math.round((billList.get(i).getBlFinalTot()*0.025)*100.0)/100.0);
								 map.put("sGST", Math.round((billList.get(i).getBlFinalTot()*0.025)*100.0)/100.0);
								 map.put("iGST", 0);
							 }else if(billList.get(i).getBlIgst()>0){
								 map.put("cGST", 0);
								 map.put("sGST", 0);
								 map.put("iGST", Math.round((billList.get(i).getBlFinalTot()*0.05)*100.0)/100.0);
							 }else{
								 map.put("cGST", 0);
								 map.put("sGST", 0);
								 map.put("iGST", 0);
							 }
						 }
						
						 
						 
						 
						 double d=billList.get(i).getBlFinalTot();
						
						 String amt=String.format("%.2f",billList.get(i).getBlFinalTot() - totAdvance);
						 System.out.println("amt="+amt);
						 int rupees = Integer.parseInt(amt.split("\\.")[0]);
						 System.out.println("rupees="+rupees);
						 String str1 = w.convert(rupees);
						 str1 += " Rupees ";
						 System.out.println("str1="+str1);
						 int paise = Integer.parseInt(amt.split("\\.")[1]);
						// int paise = new Double(Double.parseDouble(amt.split("\\.")[1])).intValue();
						System.out.println("paise="+paise);
						 String str2 = "";
						 if (paise != 0) {
							   str2 += " and";
							   str2 = w.convert(paise);
							   str2 += " Paise";
						 }
						 String str3 = str1 + str2 + "only";
						 System.out.println("str3="+str3);
						 map.put("bilAmt",str3);
						 
						 
						 
						 
						 String gstAmt = String.valueOf(billList.get(i).getBlFinalTot()* 0.05);
						 int rupee = Integer.parseInt(gstAmt.split("\\.")[0]);
						 String strg1 = w.convert(rupee);
						 strg1 += " Rupees ";
						 int paisa = new Double(Double.parseDouble(gstAmt.split("\\.")[1])).intValue();
						 String strg2 = "";
						 if (paise != 0) { 
							   strg2 += " and";
							   strg2 = w.convert(paisa);
							   strg2 += " Paise";
						 }
						 String strg3 = strg1 + strg2 + "only";
						 
						 map.put("gstAmt",strg3);
						 
						 
						 
						 blList.add(map);
						 
						
				 }
			 }
			 
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.clear();
		 return blList;
	 }
	
	 
	 
	 
	 public List<Map<String,Object>> getSingleBlPrint(int brhId, String blNo,Session session){
		 System.out.println("enter into getSingleBlPrint function");
		 List<Map<String,Object>> blList = new ArrayList<Map<String,Object>>();
		 List<Bill> billList = new ArrayList<Bill>();
//		 try{
//			 session = this.sessionFactory.openSession();
			 Criteria cr = session.createCriteria(Bill.class);
			 cr.add(Restrictions.eq(BillCNTS.BILL_BRH_ID,brhId));
			 cr.add(Restrictions.eq(BillCNTS.Bill_NO,blNo));
			 billList = cr.list();
			 
			 if(!billList.isEmpty()){
				 NumToWords w = new NumToWords();
				 for(int i=0;i<billList.size();i++){
						 Map<String,Object> map = new HashMap<String, Object>();
						 map.put("bill", billList.get(i));
						 
						 Customer customer = (Customer) session.get(Customer.class,billList.get(i).getBlCustId());
						 map.put("cust",customer);
						 
						 List<Map<String,Object>> blDetList = new ArrayList<Map<String,Object>>();
						 for(int j=0;j<billList.get(i).getBillDetList().size();j++){
							 Map<String,Object> billDetMap = new HashMap<String, Object>();
							 Cnmt cnmt = (Cnmt) session.get(Cnmt.class,billList.get(i).getBillDetList().get(j).getBdCnmtId());
							 billDetMap.put("billDet",billList.get(i).getBillDetList().get(j));
							 billDetMap.put("cnmt", cnmt);
							 Station frStn = (Station) session.get(Station.class,Integer.parseInt(cnmt.getCnmtFromSt()));
							 Station toStn = (Station) session.get(Station.class,Integer.parseInt(cnmt.getCnmtToSt()));
							 billDetMap.put("frmStn",frStn.getStnName());
							 billDetMap.put("toStn",toStn.getStnName());
							 blDetList.add(billDetMap);
						 }
						 map.put("blDetList",blDetList);
						 
						 String amt = String.valueOf(billList.get(i).getBlFinalTot());
						 int rupees = Integer.parseInt(amt.split("\\.")[0]);
						 String str1 = w.convert(rupees);
						 str1 += " Rupees ";
						 int paise = Integer.parseInt(amt.split("\\.")[1]);
						 String str2 = "";
						 if (paise != 0) {
							   str2 += " and";
							   str2 = w.convert(paise);
							   str2 += " Paise";
						 }
						 String str3 = str1 + str2 + "only";
						 
						 map.put("bilAmt",str3);
						 
						 blList.add(map);
				 }
			 }
			 
//		 }catch(Exception e){
//			 e.printStackTrace();
//		 }
//		 session.clear();
//		 session.clear();
		 return blList;
	 }
	
	 
	 
	 
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public Map<String,Object> getBillByBlNo(String blNo,int brhId){
		 System.out.println("enter into getBillByBlNo function");
		 Map<String,Object> blMap = new HashMap<String, Object>();
		 try{
			 session = this.sessionFactory.openSession();
			 
			 List<Bill> blList = new ArrayList<Bill>();
			 Criteria cr = session.createCriteria(Bill.class);
			 cr.add(Restrictions.eq(BillCNTS.Bill_NO,blNo));
			 cr.add(Restrictions.eq(BillCNTS.BILL_BRH_ID,brhId));
			 blList = cr.list();
			 
			 if(!blList.isEmpty()){
				 NumToWords w = new NumToWords();
				 Bill bill = blList.get(0);
				 
				 if(bill.isBlCancel()){
					 blMap.put("msg","cancel");
				 }else{
					 SQLQuery sqlQuery = session.createSQLQuery("SELECT * From mr_bill where blId =:blId ");
			         sqlQuery.setInteger("blId", bill.getBlId());
			         List<Object> mrList = sqlQuery.list();
			         System.out.println("size: "+mrList.size());
					 if(!mrList.isEmpty()){
						 blMap.put("msg","MR");
					 }else{
						 
						 //if(bill.getBillForwarding() == null){
							 Customer customer = (Customer) session.get(Customer.class,bill.getBlCustId());
							 Branch branch = (Branch) session.get(Branch.class,bill.getBlBrhId()); 
							 
							 blMap.put("bill",bill);
							 blMap.put("custName",customer.getCustName());
							 blMap.put("custAdd",customer.getCustAdd());
							 blMap.put("custCity",customer.getCustCity());
							 blMap.put("custPin",customer.getCustPin());
							 blMap.put("brhName",branch.getBranchName());
							 blMap.put("brhAdd",branch.getBranchAdd());
							 blMap.put("brhPhNo",branch.getBranchPhone());
							 blMap.put("brhEmail",branch.getBranchEmailId());
							 
							 List<Map<String,Object>> blDetList = new ArrayList<Map<String,Object>>();
							 if(!bill.getBillDetList().isEmpty()){
								 for(int i=0;i<bill.getBillDetList().size();i++){
									 Map<String,Object> bdMap = new HashMap<String, Object>();
									 Cnmt cnmt = (Cnmt)session.get(Cnmt.class,bill.getBillDetList().get(i).getBdCnmtId());
									 Station frStation = (Station) session.get(Station.class,Integer.parseInt(cnmt.getCnmtFromSt()));
									 Station toStation = (Station) session.get(Station.class,Integer.parseInt(cnmt.getCnmtToSt()));
									 bdMap.put("billDet",bill.getBillDetList().get(i));
									 bdMap.put("cnmtCode",cnmt.getCnmtCode());
									 bdMap.put("cnmtDt",cnmt.getCnmtDt());
									 bdMap.put("frStn",frStation.getStnName());
									 bdMap.put("toStn",toStation.getStnName());
									 
									 blDetList.add(bdMap);
								 }
							 }
							 
							 blMap.put("blDetList",blDetList);
							 
							  
							 
							 String amt = String.valueOf(bill.getBlFinalTot());
							 int rupees = Integer.parseInt(amt.split("\\.")[0]);
							 String str1 = w.convert(rupees);
							 str1 += " Rupees ";
							 int paise = new Double(Double.parseDouble(amt.split("\\.")[1])).intValue();
							 String str2 = "";
							 if (paise != 0) { 
								   str2 += " and";
								   str2 = w.convert(paise);
								   str2 += " Paise";
							 }
							 String str3 = str1 + str2 + "only";
							 
							 blMap.put("bilAmt",str3);
							 
							 
							 
							
							 blMap.put("msg",ConstantsValues.SUCCESS);
						 /*}else{
							 blMap.put("msg","BF");
						 }*/
					 }
				 }
			 }
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return blMap;
	 }
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public int billCancel(String blNo, int brhId){
		 System.out.println("enter into billCancel function");
		 int res = 0;
		 User currentUser = (User) httpSession.getAttribute("currentUser");
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 
			 List<Bill> blList = new ArrayList<Bill>();
			 Criteria cr = session.createCriteria(Bill.class);
			 cr.add(Restrictions.eq(BillCNTS.Bill_NO,blNo));
			 cr.add(Restrictions.eq(BillCNTS.BILL_BRH_ID,brhId));
			 blList = cr.list();
			 
			 if(!blList.isEmpty()){
				 Bill bill = blList.get(0);
				 Customer customer = (Customer) session.get(Customer.class,bill.getBlCustId());
				 
				 BillForwarding billForwarding = bill.getBillForwarding();
					if(billForwarding != null){
						billForwarding.getBillList().remove(bill);
						billForwarding.setBfTotAmt(billForwarding.getBfTotAmt() - bill.getBlFinalTot());
						session.merge(billForwarding);
					}
					
					
					
					
				 if(!bill.getBillDetList().isEmpty()){
					 for(int i=0;i<bill.getBillDetList().size();i++){
						 Cnmt cnmt = (Cnmt) session.get(Cnmt.class,bill.getBillDetList().get(i).getBdCnmtId());
							
						 if(bill.getBlType()=='S'){
							 
							 if(cnmt.getCnmtSupBillNo() != null && !cnmt.getCnmtSupBillNo().isEmpty()){
								 
								//for(String billNo : cnmt.getCnmtSupBillNo()){
									for(int j=0;j<cnmt.getCnmtSupBillNo().size();j++){
										String billNo=cnmt.getCnmtSupBillNo().get(j);
									if(billNo.equalsIgnoreCase(bill.getBlBillNo())){
										//int index=Collections.binarySearch(cnmt.getCnmtSupBillNo(), billNo);
										cnmt.getCnmtSupBillNo().remove(j);
										j--;
									}
										
								}
				 
							 }
							 
							 if(cnmt.getCnmtSupBillDet() != null && !cnmt.getCnmtSupBillDet().isEmpty()){
//								 ArrayList<String> list = new ArrayList<>();
//								 list.add(String.valueOf(bdId));
//								 actCnmt.setCnmtSupBillDet(list);
								 
								 //for(String bdId : cnmt.getCnmtSupBillDet()){
								for(int j=0;j<cnmt.getCnmtSupBillDet().size();j++){	
									String bdId=cnmt.getCnmtSupBillDet().get(j);
									if(bdId.equalsIgnoreCase(bill.getBillDetList().get(i).getBdId().toString())){
											//int index=Collections.binarySearch(cnmt.getCnmtSupBillDet(), bdId);
											cnmt.getCnmtSupBillDet().remove(j);
											j--;
										}
											
									}
								 
							 }

							 cnmt.setCnmtSupBillNo(cnmt.getCnmtSupBillNo());
							 cnmt.setCnmtSupBillDet(cnmt.getCnmtSupBillDet());
							}else{
								cnmt.setCnmtBillNo(null);
								cnmt.setCnmtBillDetId(-1);
							}
							 
							 session.update(cnmt);
					 }
				 }
				 
				 bill.setBlCancel(true);
				 session.update(bill);
				 
				 
				 String frghtCode = "";
				 String srvTaxCode = "";
				 List<FAMaster> famList = new ArrayList<>();
					
				famList = session.createCriteria(FAMaster.class)
						  .add(Restrictions.disjunction()
					      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_COLLECTION))
					      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_SRV_TAX))
					  ).list(); 
					
					
					if(!famList.isEmpty()){
						for(int i=0;i<famList.size();i++){
							if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_COLLECTION)){
								frghtCode = famList.get(i).getFaMfaCode();
							}else if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_SRV_TAX)){
								srvTaxCode = famList.get(i).getFaMfaCode();
							}else{
								System.out.println("invalid result");
							}
						}
					}
				 
					Query query = session
							.createQuery("from CashStmtStatus where bCode= :type order by cssId DESC");
					query.setParameter("type", currentUser.getUserBranchCode());
					List<CashStmtStatus> cashStmtStatusList = query.setMaxResults(1).list();
					
					int voucherNo = 0;
					 
					 cr = session.createCriteria(CashStmt.class);
					 cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,cashStmtStatusList.get(0).getCssDt()));
					 cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
					 ProjectionList proList = Projections.projectionList();
					 proList.add(Projections.property("csVouchNo"));
					 cr.setProjection(proList);
						
					 List<Integer>  voucherNoList = cr.list();
					 if(!voucherNoList.isEmpty()){
							int vNo = voucherNoList.get(voucherNoList.size() - 1);
							voucherNo = vNo + 1;
					 }else{
							voucherNo = 1;
					 }
				 
					if(!cashStmtStatusList.isEmpty()){
						CashStmtStatus css = cashStmtStatusList.get(0);
						
						
						 CashStmt cs1 = new CashStmt();
						 cs1.setUserCode(currentUser.getUserCode());
						 cs1.setbCode(currentUser.getUserBranchCode());
						 cs1.setCsDescription("cancel Bill("+bill.getBlBillNo()+")..");
						 cs1.setCsDrCr('D');
						 cs1.setCsAmt(bill.getBlSubTot());
						 cs1.setCsType("BILL");
						 cs1.setCsTvNo(bill.getBlBillNo());
						 cs1.setCsVouchType("CONTRA");
						 cs1.setCsFaCode(frghtCode);
						 cs1.setCsVouchNo(voucherNo);
						 cs1.setCsDt(css.getCssDt());
						 
						 cs1.setCsNo(css);
						 css.getCashStmtList().add(cs1);
						 session.merge(css);
						 
						 
						 
						 if(bill.getBlFinalTot() - bill.getBlSubTot() > 0){
							 CashStmt cs = new CashStmt();
							 cs.setUserCode(currentUser.getUserCode());
							 cs.setbCode(currentUser.getUserBranchCode());
							 cs.setCsDescription("cancel Bill("+bill.getBlBillNo()+")..");
							 
							 cs.setCsDrCr('D');
							 cs.setCsAmt(bill.getBlSerTax() + bill.getBlSwachBhCess()+bill.getBlKisanKalCess());
							 cs.setCsType("BILL");
							 cs.setCsTvNo(bill.getBlBillNo());
							 cs.setCsVouchType("CONTRA");
							 cs.setCsFaCode(srvTaxCode);
							 cs.setCsVouchNo(voucherNo);
							 cs.setCsDt(css.getCssDt());
							 
							 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
							 cs.setCsNo(actCss);
							 actCss.getCashStmtList().add(cs);
							 session.merge(actCss);
						 }
						 
						 
						 CashStmt cs2 = new CashStmt();
						 cs2.setUserCode(currentUser.getUserCode());
						 cs2.setbCode(currentUser.getUserBranchCode());
						 cs2.setCsDescription("cancel Bill("+bill.getBlBillNo()+")..");
						 cs2.setCsDrCr('C');
						 cs2.setCsAmt(bill.getBlFinalTot());
						 cs2.setCsType("BILL");
						 cs2.setCsTvNo(bill.getBlBillNo());
						 cs2.setCsVouchType("CONTRA");
						 cs2.setCsFaCode(customer.getCustFaCode());
						 cs2.setCsVouchNo(voucherNo);
						 cs2.setCsDt(css.getCssDt());
						 
						 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						 cs2.setCsNo(actCss);
						 actCss.getCashStmtList().add(cs2);
						 session.merge(actCss);
						 
						 transaction.commit();
						 res = 1;
					}
			 }
			session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
			 res = 0;
		 }
		 session.clear();
		 session.close();
		 return res;
	 }
	 
	 // Transaction Management
	 @Override
	 public Map<String, Object> saveAdvBill(Session session, User user, BillService billService){
	 		logger.info("Enter into saveAdvBill() : UserID = "+user.getUserId());	 		
			String billNo = "";
			List<String> errMsg = new ArrayList<String>();
			Map<String, Object> resultMap = new HashMap<String, Object>();	
			
			try{
				// Find Last Bill made by Current Branch
				List<Bill> billList = session.createCriteria(Bill.class)
						.add(Restrictions.eq(BillCNTS.BILL_BRH_ID, Integer.parseInt(user.getUserBranchCode())))						
						.addOrder(Order.desc(BillCNTS.BILL_ID))
						.setMaxResults(1)
						.list();
				
				if(billList.isEmpty()){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					errMsg.add("Last Bill is not found for this Bill !");
					resultMap.put("msg", errMsg);
					logger.info("UserID = "+user.getUserId()+" : Last Bill is not found for this Bill !");
					return resultMap;
				}
				
				logger.info("UserID = "+user.getUserId()+" : Last BillNo = "+billList.get(0).getBlBillNo());

				String frghtCode = "";
				String srvTaxCode = "";
				
				// Find FAMaster For Frieght And Service Tax Code
				List<FAMaster> famList = session.createCriteria(FAMaster.class)
						  .add(Restrictions.disjunction()
					      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_COLLECTION))
					      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_SRV_TAX))
					  ).list(); 
				
				//	Find FrightCode AND ServiceTaxCode
				if(! famList.isEmpty()){
					for(int i=0;i<famList.size();i++){
						if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_COLLECTION))
							frghtCode = famList.get(i).getFaMfaCode();
						else if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_SRV_TAX))
							srvTaxCode = famList.get(i).getFaMfaCode();
						else
							System.out.println("invalid result");
					}
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					errMsg.add("FaMaster is not found for Frieght Collection and Frieght Service Tax !");
					resultMap.put("msg", errMsg);
					logger.info("UserID = "+user.getUserId()+" : FaMaster is not found !");
					return resultMap;
				}
				
				// Find Branch Name (Start Three Charaacter)	
				String brh = "";
				Branch branch = (Branch) session.get(Branch.class, Integer.parseInt(user.getUserBranchCode()));
				if(branch != null){
					brh = branch.getBranchName().substring(0,3).toUpperCase();
					if(branch.getBranchName().equals("Raipur")){
						brh="RIP";
					}
				}
				
				
				if(brh != null){			 
					
					List<BillDetService> bdSerList = billService.getBdSerList();
					Bill bill = billService.getBill();
					 
					if(bill.getBlCustId() > 0 && bill.getBlBrhId() > 0){						
						// Find Customer
						Customer customer = (Customer) session.get(Customer.class, bill.getBlCustId());
						if(customer == null){
							resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
							errMsg.add("Customer detail is not found !");
							resultMap.put("msg", errMsg);
							logger.info("UserID = "+user.getUserId()+" : Customer detail is not found !");
							return resultMap;
						}						
						// Create Bill No for new Record
						if(! billList.isEmpty()){
							 String prevBillNo = billList.get(0).getBlBillNo();
							 billNo  = brh + String.valueOf(Integer.parseInt(prevBillNo.substring(3)) + 1 + 1000000).substring(1);
						 }else{
							 billNo = brh+"000001";
						 }
						 
						 logger.info("UserID = "+user.getUserId()+" : NewBill No = "+billNo);
						
						 if(billNo != null){
							 
							 // If Bill Type is NORMAL
							 if(bill.getBlType() == 'N'){
								 logger.info("UserID = "+user.getUserId()+" : Normal bill is going on.... ");
								 
								 bill.setBlBillNo(billNo);
								 bill.setBlRemAmt(bill.getBlFinalTot());
								 bill.setUserCode(user.getUserCode());
								 bill.setbCode(user.getUserBranchCode());
								 
								 // Validating Bill									 
								 resultMap = BillDAOImpl.validateBill(user, resultMap, errMsg, bill);
								 Object errStatus = resultMap.get(ConstantsValues.RESULT);
								 if(errStatus != null)
									 if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.ERROR))
										 return resultMap;
								 
								 // Save Bill
								 int billId = (Integer) session.save(bill);							 
								 
								 if(billId > 0){
									 logger.info("UserID = "+user.getUserId()+" Bill ID = "+billId+" : Bill Detail Size = "+bdSerList.size());
									 // If BillDetailService is not Empty								
									 if(! bdSerList.isEmpty()){
										 for(int i=0;i<bdSerList.size();i++){											 
											 BillDetService billDetService = bdSerList.get(i);											 
											 BillDetail billDetail = billDetService.getBillDetail();
											 Cnmt cnmt = billDetService.getCnmt();
											 
											 billDetail.setUserCode(user.getUserCode());
											 billDetail.setbCode(user.getUserBranchCode());
											 billDetail.setBdCnmtId(cnmt.getCnmtId());				
											 
											 // Find Bill Which is saved Aboved And Set to BillDetail
											 Bill freshBill = (Bill) session.get(Bill.class, billId);
											 billDetail.setBill(freshBill);	
											 
											 // Validation For Normal Bill Detail											 
											 resultMap = BillDAOImpl.validateBillDetail(user, resultMap, errMsg, billDetail, "normal");								 
											 if(billDetail.getBdRate() <= 0){
												 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
												 errMsg.add("Rate sould be greater than 0 !");
												 resultMap.put("msg", errMsg);
												 return resultMap;
											 }
											 											 
											 // Check Validation For CNMTDate - Bill Date AND Kishan Kalwan Tax
											 if(bill.getBlBillDt().compareTo(cnmt.getCnmtDt()) < 0){
												 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
												 errMsg.add("Bill date should be greater than CNMT Date");
												 resultMap.put("msg", errMsg);
												 logger.info("UserID = "+user.getUserId()+" : Bill date should be greater than CNMT Date !");
											 }
											 if(cnmt.getCnmtDt().compareTo(new Date(dateForKishan.getTime())) >= 0){
												 if(bill.getBlKisanKalCess() < 0){
													 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
													 errMsg.add("Please enter Krishi Kishan Cess amount !");
													 resultMap.put("msg", errMsg);
													 logger.info("UserID = "+user.getUserId()+" : Please enter Krishi Kishan Cess amount !");
												 }
											 }	
											 
											 errStatus = resultMap.get(ConstantsValues.RESULT);
											 if(errStatus != null)
												 if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.ERROR))
													 return resultMap;
											 
											 // Save Bill Detail
											 int bdId = (Integer) session.save(billDetail);												
											 
											 freshBill.getBillDetList().add(billDetail);											 
											 session.merge(freshBill);
											 
											 
											 if(bdId > 0){
												 logger.info("UserID = "+user.getUserId()+" :BillDetail ID = "+bdId);
												 // Get Actual Cnmt From Database
												 Cnmt actCnmt = (Cnmt) session.get(Cnmt.class, cnmt.getCnmtId());
												 actCnmt.setCnmtBillDetId(bdId);
												 actCnmt.setCnmtBillNo(billNo);
												 
												 // Validate CNMT
												 resultMap = BillDAOImpl.validateCNMT(user, resultMap, errMsg, actCnmt);
												 errStatus = resultMap.get(ConstantsValues.RESULT);
												 if(errStatus != null)
													 if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.ERROR))
														 return resultMap;										 
												 //Final Updated Actual Cnmt
												 session.merge(actCnmt);
												 logger.info("UserID = "+user.getUserId()+" : CnmtID = "+actCnmt.getCnmtId());
											 }else{
												resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
												errMsg.add("Bill Detail is not saved !");
												resultMap.put("msg", errMsg);
												logger.info("UserID = "+user.getUserId()+" : Bill Detail is not saved !");
												return resultMap;
											 }
											 
										 }
									 }
									
									 logger.info("UserID = "+user.getUserId()+" : CashStmtTemp is going on....");
									 
									// Create CashStmtTemp For saving Amt for Temporarly
									// Doing Credit
									CashStmtTemp cs1 = new CashStmtTemp();
									cs1.setUserCode(user.getUserCode());
									cs1.setbCode(user.getUserBranchCode());
									cs1.setCsDescription("As Per Bill File");
									cs1.setCsDrCr('C');
									cs1.setCsAmt(bill.getBlSubTot());
									cs1.setCsType("BILL");
									cs1.setCsTvNo(billNo);
									cs1.setCsVouchType("CONTRA");
									cs1.setCsFaCode(frghtCode);
									cs1.setCsBrhFaCode(branch.getBranchFaCode());								
									cs1.setCsDt(bill.getBlBillDt());
									
									// Doing Debit
									CashStmtTemp cs2 = new CashStmtTemp();
									cs2.setUserCode(user.getUserCode());
									cs2.setbCode(user.getUserBranchCode());
									cs2.setCsDescription("As Per Bill File");
									cs2.setCsDrCr('D');
									cs2.setCsAmt(bill.getBlFinalTot());
									cs2.setCsType("BILL");
									cs2.setCsTvNo(billNo);
									cs2.setCsVouchType("CONTRA");
									cs2.setCsFaCode(customer.getCustFaCode());
									cs2.setCsBrhFaCode(branch.getBranchFaCode());
									cs2.setCsDt(bill.getBlBillDt());					
									
									CashStmtTemp cs3 = null;
									
									if(bill.getBlFinalTot() - bill.getBlSubTot() > 0){
										 cs3 = new CashStmtTemp();
										 cs3.setUserCode(user.getUserCode());
										 cs3.setbCode(user.getUserBranchCode());
										 cs3.setCsDescription("As Per Bill File");
										 cs3.setCsDrCr('C');
										 cs3.setCsAmt(bill.getBlSerTax() + bill.getBlSwachBhCess()+bill.getBlKisanKalCess());
										 cs3.setCsType("BILL");
										 cs3.setCsTvNo(billNo);
										 cs3.setCsVouchType("CONTRA");
										 cs3.setCsFaCode(srvTaxCode);
										 cs3.setCsBrhFaCode(branch.getBranchFaCode());
										 cs3.setCsDt(bill.getBlBillDt());								
									}
									
									// Valiadating CashStmtTemp
									resultMap = BillDAOImpl.validateCashStmtTemp(user, resultMap, errMsg, cs1);
									resultMap = BillDAOImpl.validateCashStmtTemp(user, resultMap, errMsg, cs2);
									if(cs3 != null)
										resultMap = BillDAOImpl.validateCashStmtTemp(user, resultMap, errMsg, cs3);
									
									errStatus = resultMap.get(ConstantsValues.RESULT);
									 if(errStatus != null)
										 if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.ERROR))
											 return resultMap;
									
									Integer cstId = (Integer) session.save(cs1);
									if(cstId < 1){
										resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
										errMsg.add("CashStmtTemp1 is not saved !");
										resultMap.put("msg", errMsg);
										logger.info("UserID = "+user.getUserId()+" : CashStmtTemp1 is not saved !");
										return resultMap;
									}else
										logger.info("UserID = "+user.getUserId()+" : CashStmtTemp1ID = "+cstId);
										
									cstId = (Integer) session.save(cs2);
									if(cstId < 1){
										resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
										errMsg.add("CashStmtTemp2 is not saved !");
										resultMap.put("msg", errMsg);
										logger.info("UserID = "+user.getUserId()+" : CashStmtTemp2 is not saved !");
										return resultMap;
									}else
										logger.info("UserID = "+user.getUserId()+" : CashStmtTemp2ID = "+cstId);
									if(cs3 != null){
										cstId = (Integer) session.save(cs3);
										if(cstId < 1){
											resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
											errMsg.add("CashStmtTemp3 is not saved !");
											resultMap.put("msg", errMsg);
											logger.info("UserID = "+user.getUserId()+" : CashStmtTemp3 is not saved !");
											return resultMap;
										}else
											logger.info("UserID = "+user.getUserId()+" : CashStmtTemp3ID = "+cstId);
									}
									
									resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
									resultMap.put("billNo", billNo);
									logger.info("UserID = "+user.getUserId()+" : Bill is saved !");
								 }else{
									resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
									errMsg.add("Bill is not saved !");
									resultMap.put("msg", errMsg);
									logger.info("UserID = "+user.getUserId()+" : Bill is not saved !");
									return resultMap;
								 }		 

							 }else{
								 // If Bill is Supplementary 
								 logger.info("UserID = "+user.getUserId()+" : Sup. bill is going on.....");								 
								 bill.setBlBillNo(billNo);
								 bill.setBlRemAmt(bill.getBlFinalTot());
								 bill.setUserCode(user.getUserCode());
								 bill.setbCode(user.getUserBranchCode());
								 
								// Validating Bill									 
								 resultMap = BillDAOImpl.validateBill(user, resultMap, errMsg, bill);
								 Object errStatus = resultMap.get(ConstantsValues.RESULT);
								 if(errStatus != null)
									 if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.ERROR))
										 return resultMap;

								 // Save Bill
								 int billId = (Integer) session.save(bill);
								 
								 if(billId > 0){
									 logger.error("UserID = "+user.getUserId()+" : BillID = "+billId+" : Bill Detail Size = "+bdSerList.size());								 
									 // If BillDetailService is not Empty
									 if(!bdSerList.isEmpty()){
										 for(int i=0;i<bdSerList.size();i++){
											 BillDetService billDetService = bdSerList.get(i);											
											 BillDetail billDetail = billDetService.getBillDetail();
											 Cnmt cnmt = billDetService.getCnmt();
											 
											 // Actual Cnmt From database
											 Cnmt actCnmt = (Cnmt) session.get(Cnmt.class,cnmt.getCnmtId());
											 
											 billDetail.setUserCode(user.getUserCode());
											 billDetail.setbCode(user.getUserBranchCode());
											 billDetail.setBdCnmtId(cnmt.getCnmtId());
											 
											 // Get Bill which is saved above
											 Bill freshBill = (Bill) session.get(Bill.class,billId);
											 billDetail.setBill(freshBill);
											 
											 // Validation For Sup Bill Detail											 
											 resultMap = BillDAOImpl.validateBillDetail(user, resultMap, errMsg, billDetail, "sup");
//											 if(billDetail.getBdRate() <= 0){
//												 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
//												 errMsg.add("Rate sould be greater than 0 !");
//												 resultMap.put("msg", errMsg);
//												 return resultMap;
//											 }
											 
											 // Validation for BillDate - CNMTDate AND Kishan 
											 if(bill.getBlBillDt().compareTo(cnmt.getCnmtDt()) < 0){
												 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
												 errMsg.add("Bill date should be greater than CNMT Date");
												 resultMap.put("msg", errMsg);
												 logger.info("UserID = "+user.getUserId()+" : Bill date should be greater than CNMT Date");												 
											 }
											 if(cnmt.getCnmtDt().compareTo(new Date(dateForKishan.getTime())) >= 0){
												 if(bill.getBlKisanKalCess() < 0){
													 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
													 errMsg.add("Please enter Kishan Kal Cess amount !");
													 resultMap.put("msg", errMsg);
													 logger.info("UserID = "+user.getUserId()+" : Please enter Kishan Kal Cess amount");													 
												 }
											 }		
											 errStatus = resultMap.get(ConstantsValues.RESULT);
											 if(errStatus != null)
												 if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.ERROR))
													 return resultMap;
											 // Save Bill Detail
											 int bdId = (Integer) session.save(billDetail);
											 
											 freshBill.getBillDetList().add(billDetail);
											 session.merge(freshBill);
											
											 
											 // Set CnmtSupBillNo And CnmtSupBillDet ID to CNMT										 
											 if(bdId > 0){
													logger.info("UserID = "+user.getUserId()+" : BillDetail ID = "+bdId);
												 if(actCnmt.getCnmtSupBillNo() == null || !actCnmt.getCnmtSupBillNo().isEmpty()){
													 ArrayList<String> list = new ArrayList<>();
													 list.add(billNo);
													 actCnmt.setCnmtSupBillNo(list);
												 }else{
													 actCnmt.getCnmtSupBillNo().add(billNo);
												 }
												 // Set Bill ID to CNMT
												 if(actCnmt.getCnmtSupBillDet() == null || !actCnmt.getCnmtSupBillDet().isEmpty()){
													 ArrayList<String> list = new ArrayList<>();
													 list.add(String.valueOf(bdId));
													 actCnmt.setCnmtSupBillDet(list);
												 }else{
													 actCnmt.getCnmtSupBillDet().add(String.valueOf(bdId));
												 }
												 // CNMT is updated 
												 session.merge(actCnmt);
													logger.info("UserID = "+user.getUserId()+" : CNMT ID = "+actCnmt.getCnmtId());
											}else{
												resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
												errMsg.add("Bill Detail is not saved !");
												resultMap.put("msg", errMsg);
												logger.info("UserID = "+user.getUserId()+" : Bill detail is not saved");
												return resultMap;
											}
										 }
									 }
									
									// Create CashStmtTemp
									// Credit Side
									logger.info("UserID = "+user.getUserId()+" : CashStmtTemp is going on.....");
									
									CashStmtTemp cs1 = new CashStmtTemp();
									cs1.setUserCode(user.getUserCode());
									cs1.setbCode(user.getUserBranchCode());
									cs1.setCsDescription("As Per Bill File");
									cs1.setCsDrCr('C');
									cs1.setCsAmt(bill.getBlSubTot());
									cs1.setCsType("BILL");
									cs1.setCsTvNo(billNo);
									cs1.setCsVouchType("CONTRA");
									cs1.setCsFaCode(frghtCode);
									cs1.setCsBrhFaCode(branch.getBranchFaCode());
									cs1.setCsDt(bill.getBlBillDt());
									
									// Create CashStmtTemp
									// Debit Side
									CashStmtTemp cs2 = new CashStmtTemp();
									cs2.setUserCode(user.getUserCode());
									cs2.setbCode(user.getUserBranchCode());
									cs2.setCsDescription("As Per Bill File");
									cs2.setCsDrCr('D');
									cs2.setCsAmt(bill.getBlFinalTot());
									cs2.setCsType("BILL");
									cs2.setCsTvNo(billNo);
									cs2.setCsVouchType("CONTRA");
									cs2.setCsFaCode(customer.getCustFaCode());
									cs2.setCsBrhFaCode(branch.getBranchFaCode());
									cs2.setCsDt(bill.getBlBillDt());
									
									
									CashStmtTemp cs3 = null; 
									if(bill.getBlFinalTot() - bill.getBlSubTot() > 0){
										 cs3 = new CashStmtTemp();
										 cs3.setUserCode(user.getUserCode());
										 cs3.setbCode(user.getUserBranchCode());
										 cs3.setCsDescription("As Per Bill File");
										 cs3.setCsDrCr('C');
										 cs3.setCsAmt(bill.getBlSerTax() + bill.getBlSwachBhCess()+bill.getBlKisanKalCess());
										 cs3.setCsType("BILL");
										 cs3.setCsTvNo(billNo);
										 cs3.setCsVouchType("CONTRA");
										 cs3.setCsFaCode(frghtCode);
										 cs3.setCsBrhFaCode(branch.getBranchFaCode());
										 cs3.setCsDt(bill.getBlBillDt());								 
										 
									}
									
									// Valiadating CashStmtTemp
									resultMap = BillDAOImpl.validateCashStmtTemp(user, resultMap, errMsg, cs1);
									resultMap = BillDAOImpl.validateCashStmtTemp(user, resultMap, errMsg, cs2);
									if(cs3 != null)
										resultMap = BillDAOImpl.validateCashStmtTemp(user, resultMap, errMsg, cs3);
									
									errStatus = resultMap.get(ConstantsValues.RESULT);
									 if(errStatus != null)
										 if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.ERROR))
											 return resultMap;	 
									
									 Integer cstId = (Integer) session.save(cs1);
										if(cstId < 1){
											resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
											errMsg.add("CashStmtTemp1 is not saved !");
											resultMap.put("msg", errMsg);
											logger.info("UserID = "+user.getUserId()+" : CashStmtTemp1 is not saved");
											return resultMap;
										}else
											logger.info("UserID = "+user.getUserId()+" : CashStmtTemp1ID = "+cstId);
										cstId = (Integer) session.save(cs2);
										if(cstId < 1){
											resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
											errMsg.add("CashStmtTemp2 is not saved !");
											resultMap.put("msg", errMsg);
											logger.info("UserID = "+user.getUserId()+" : CashStmtTemp2 is not saved");
											return resultMap;
										}else
											logger.info("UserID = "+user.getUserId()+" : CashStmtTemp2ID = "+cstId);
										if(cs3 != null){
											cstId = (Integer) session.save(cs3);
											if(cstId < 1){
												resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
												errMsg.add("CashStmtTemp3 is not saved !");
												resultMap.put("msg", errMsg);
												logger.info("UserID = "+user.getUserId()+" : CashStmtTemp3 is not saved");
												return resultMap;
											}	
											logger.info("UserID = "+user.getUserId()+" : CashStmtTemp3ID = "+cstId);
										}
									
									resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
									resultMap.put("billNo", billNo);
									logger.info("UserID = "+user.getUserId()+" : Bill is saved !");
								 }else{
									 resultMap.put(ConstantsValues.ERROR, ConstantsValues.ERROR);
									 errMsg.add("Bill is not saved !");
									 resultMap.put("msg", errMsg);
									 return resultMap;
								 }
							 }
						 }else{
							 logger.info("UserID : "+user.getUserId()+" : New Bill not generated !");
							 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
							 errMsg.add("New Bill not generated !");
							 resultMap.put("msg", errMsg);
							 logger.info("UserID = "+user.getUserId()+" : Bill is not generated");
							 return resultMap;
						 }						
					 }else{
						 logger.info("UserID : "+user.getUserId()+" : Invalid Customer Or Billing Branch");
						 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
						 errMsg.add("Invalid Customer Or Billing Branch !");
						 resultMap.put("msg", errMsg);
						 logger.info("UserID = "+user.getUserId()+" : Invalid Customer OR Billing Branch");
						 return resultMap;
					 }
				 }else{
					 logger.info("UserID : "+user.getUserId()+" : User Does not have Branch");
					 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					 errMsg.add("User does not have branch !");
					 resultMap.put("msg", errMsg);
					 logger.info("UserID = "+user.getUserId()+" : User does not have branch");
					 return resultMap;
				 }
			}catch(Exception e){
				logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				errMsg.add("Something is going wrong !");
				resultMap.put("msg", errMsg);
				return resultMap;
			}			
			return resultMap;
		}
	 
	@Override
	 public Map<String, Object> saveBill(Session session, User user, BillService billService){
		 logger.info("UserID = "+user.getUserId()+" : Enter into saveBill()");
		 Map<String, Object> resultMap = new HashMap<String, Object>();
		 List<String> errMsg = new ArrayList<String>();
		 
		 String billNo = "";
		 List<Bill> billList = new ArrayList<>();
				
		 try{
			 // Find last Bill 
			 billList = session.createCriteria(Bill.class)
					 .add(Restrictions.eq(BillCNTS.BILL_BRH_ID, Integer.parseInt(user.getUserBranchCode())))
					 .addOrder(Order.desc(BillCNTS.BILL_ID))
					 .setMaxResults(1)
					 .list();
			 
			 if(billList.isEmpty()){
				 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				 errMsg.add("Last Bill is not found !");
				 resultMap.put("msg", errMsg);
				 logger.info("UserID = "+user.getUserId()+" : Last bill is not found !");
				 return resultMap;
			 }
			 
			 // Find and set FrightCode and Service Code
			 String frghtCode = "";
			 String srvTaxCode = "";			 
			 List<FAMaster> famList = session.createCriteria(FAMaster.class)
					  .add(Restrictions.disjunction()
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_COLLECTION))
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_SRV_TAX))
				  ).list(); 
			 
			 if(!famList.isEmpty()){
				for(int i=0;i<famList.size();i++){
					if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_COLLECTION)){
						frghtCode = famList.get(i).getFaMfaCode();
					}else if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_SRV_TAX)){
						srvTaxCode = famList.get(i).getFaMfaCode();
					}else{
						System.out.println("invalid result");
					}
				}
			}else{
				 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				 errMsg.add("FaMaster is not found for Frieght Collection and Frieght Service Tax !");
				 resultMap.put("msg", errMsg);
				 logger.info("UserID = "+user.getUserId()+" : FaMaster is not found !");
				 return resultMap;			 
			}
			
			// Find Branch via logged in user
			String brh = "";
			Branch branch = (Branch) session.get(Branch.class,Integer.parseInt(user.getUserBranchCode()));
			if(branch != null){
				brh = branch.getBranchName().substring(0,3).toUpperCase();
				if(branch.getBranchName().equals("Raipur")){
					brh="RIP";
				}
			}
			 
			if(brh != null){
				 List<BillDetService> bdSerList = billService.getBdSerList();
				 Bill bill = billService.getBill();
				 // Find Customer for billing
				 Customer customer = (Customer) session.get(Customer.class, bill.getBlCustId());
				 
				 if(bill.getBlCustId() > 0 && bill.getBlBrhId() > 0){
					 // Generating new Bill
					 if(!billList.isEmpty()){
						 String prevBillNo = billList.get(0).getBlBillNo();
						 billNo  = brh + String.valueOf(Integer.parseInt(prevBillNo.substring(3)) + 1 + 1000000).substring(1);
					 }else{
						 billNo = brh+"000001";
					 }
					 
					 logger.info("UserID = "+user.getUserId()+" : New Generated Bill = "+billNo);
					 
					 if(billNo != null){
						 
						 // If bill type NORMAL
						 if(bill.getBlType() == 'N'){
							 logger.info("UserID = "+user.getUserId()+" : Normal bill is going on.....");
							 
							 bill.setBlBillNo(billNo);
							 bill.setBlRemAmt(bill.getBlFinalTot());
							 bill.setUserCode(user.getUserCode());
							 bill.setbCode(user.getUserBranchCode());
							 
							// Validating Bill									 
							 resultMap = BillDAOImpl.validateBill(user, resultMap, errMsg, bill);
							 Object errStatus = resultMap.get(ConstantsValues.RESULT);
							 if(errStatus != null)
								 if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.ERROR))
									 return resultMap;
							 
							 // Save Bill
							 int billId = (Integer) session.save(bill);
							 
							 if(billId < 1){
								 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
								 errMsg.add("Bill is not saved !");
								 resultMap.put("msg", errMsg);
								 logger.info("UserID = "+user.getUserId()+" : Bill is not saved !");
								 return resultMap;	
							 }
							 
							  logger.info("UserID = "+user.getUserId()+" Bill ID = "+billId+" : Bill Detail Size = "+bdSerList.size());
							 
							 // If BillDetailService is not Empty
							 if(!bdSerList.isEmpty()){
								 for(int i=0;i<bdSerList.size();i++){
									 
									 BillDetService billDetService = bdSerList.get(i);
									 BillDetail billDetail = billDetService.getBillDetail();
									 Cnmt cnmt = billDetService.getCnmt();
									
									 billDetail.setUserCode(user.getUserCode());
									 billDetail.setbCode(user.getUserBranchCode());								 
									 billDetail.setBdCnmtId(cnmt.getCnmtId());
									 
									 // Fetch Bill which is saved above
									 Bill freshBill = (Bill) session.get(Bill.class, billId);
									 billDetail.setBill(freshBill);
									 
									 // Validation For Normal Bill Detail											 
									  resultMap = BillDAOImpl.validateBillDetail(user, resultMap, errMsg, billDetail, "normal");
									 
									 if(billDetail.getBdRate() <= 0){
										 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
										 errMsg.add("Rate sould be greater than 0 !");
										 resultMap.put("msg", errMsg);
										 return resultMap;
									 }
									 
									 // Validation For BillDate - CNMTDate AND Kishan Kal Cess
									 if(bill.getBlBillDt().compareTo(cnmt.getCnmtDt()) < 0){
										 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
										 errMsg.add("Bill date should be greater than CNMT Date");
										 resultMap.put("msg", errMsg);
										 logger.info("UserID = "+user.getUserId()+" : Bill date should be greater than CNMT Date !");
									 }
									 if(cnmt.getCnmtDt().compareTo(new Date(dateForKishan.getTime())) >= 0){
										 if(bill.getBlKisanKalCess() < 0){
											 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
											 errMsg.add("Please enter Kishan Kal Cess amount !");
											 resultMap.put("msg", errMsg);
											 logger.info("UserID = "+user.getUserId()+" : Please enter Kishan Kal Cess amount !");
										 }
									 }		
									 
									 errStatus = resultMap.get(ConstantsValues.RESULT);
									 if(errStatus != null)
										 if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.ERROR))
											 return resultMap;
									 
									 // Save Bill Detail
									 int bdId = (Integer) session.save(billDetail);
									 
									freshBill.getBillDetList().add(billDetail);
									 session.merge(freshBill);
									
									 
									 if(bdId > 0){		
 										logger.info("UserID = "+user.getUserId()+" : BillDetail ID = "+bdId);							 
										// Fetch Actual CNMT And Set billId and bill No And Merge									 
										 Cnmt actCnmt = (Cnmt) session.get(Cnmt.class,cnmt.getCnmtId());
										 actCnmt.setCnmtBillDetId(bdId);
										 actCnmt.setCnmtBillNo(billNo);
										 
										 // Validate CNMT
										 resultMap = BillDAOImpl.validateCNMT(user, resultMap, errMsg, actCnmt);
										 errStatus = resultMap.get(ConstantsValues.RESULT);
										 if(errStatus != null)
											 if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.ERROR))
												 return resultMap;	
										 
										 session.merge(actCnmt);
										logger.info("UserID = "+user.getUserId()+" : CnmtID = "+actCnmt.getCnmtId());
									 }else{
										 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
										 errMsg.add("Bill Detail is not saved !");
										 resultMap.put("msg", errMsg);
										 logger.info("UserID = "+user.getUserId()+" : Bill Detail is not saved !");
										 return resultMap;	
									 }
								 }
							 }
							 
							 int voucherNo = 0;
							 // Fetch Last CashStmt For generating Voucher No.
							 Criteria cr = session.createCriteria(CashStmt.class);
							 cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT, bill.getBlBillDt()));
							 cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE, user.getUserBranchCode()));
							 cr.addOrder(Order.desc(CashStmtCNTS.CS_ID))
							 .setMaxResults(1);
							 ProjectionList proList = Projections.projectionList();
							 proList.add(Projections.property("csVouchNo"));
							 cr.setProjection(proList);
							 List<Integer>  voucherNoList = cr.list();
							 if(! voucherNoList.isEmpty()){
								int vNo = voucherNoList.get(0);
								voucherNo = vNo + 1;
							 }else
								voucherNo = 1;
							 
							 // Fetch CashStmtStatus
							 List<CashStmtStatus> cssList = new ArrayList<>();
							 cr = session.createCriteria(CashStmtStatus.class);
							 cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, user.getUserBranchCode()));
							 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT, bill.getBlBillDt()));
							 cssList = cr.list();
							 
							 if(!cssList.isEmpty()){
								 
								logger.info("UserID = "+user.getUserId()+" : CashStmtStatus is going on....");
								 CashStmtStatus css = cssList.get(0);	
								 // Creating CashStmt for Credit Side
								 CashStmt cs1 = new CashStmt();
								 cs1.setUserCode(user.getUserCode());
								 cs1.setbCode(user.getUserBranchCode());
								 cs1.setCsDescription("As Per Bill File");
								 cs1.setCsDrCr('C');
								 cs1.setCsAmt(bill.getBlSubTot());
								 cs1.setCsType("BILL");
								 cs1.setCsTvNo(billNo);
								 cs1.setCsVouchType("CONTRA");
								 cs1.setCsFaCode(frghtCode);
								 cs1.setCsVouchNo(voucherNo);
								 cs1.setCsDt(css.getCssDt());								 
								 cs1.setCsNo(css);
								 
								 // Creating CashStmt for Debit Side
								 CashStmt cs2 = new CashStmt();
								 cs2.setUserCode(user.getUserCode());
								 cs2.setbCode(user.getUserBranchCode());
								 cs2.setCsDescription("As Per Bill File");
								 cs2.setCsDrCr('D');
								 cs2.setCsAmt(bill.getBlFinalTot());
								 cs2.setCsType("BILL");
								 cs2.setCsTvNo(billNo);
								 cs2.setCsVouchType("CONTRA");
								 cs2.setCsFaCode(customer.getCustFaCode());
								 cs2.setCsVouchNo(voucherNo);
								 cs2.setCsDt(css.getCssDt());								 
								 cs2.setCsNo(css);
								 
								 CashStmt cs3 = null;
								 // If Bill final total is greater than bill sub total 
								 if(bill.getBlFinalTot() - bill.getBlSubTot() > 0){
									 cs3 = new CashStmt();
									 cs3.setUserCode(user.getUserCode());
									 cs3.setbCode(user.getUserBranchCode());
									 cs3.setCsDescription("As Per Bill File");
									 cs3.setCsDrCr('C');
									 cs3.setCsAmt(bill.getBlSerTax() + bill.getBlSwachBhCess()+bill.getBlKisanKalCess());
									 cs3.setCsType("BILL");
									 cs3.setCsTvNo(billNo);
									 cs3.setCsVouchType("CONTRA");
									 cs3.setCsFaCode(srvTaxCode);
									 cs3.setCsVouchNo(voucherNo);
									 cs3.setCsDt(css.getCssDt());									 
									 cs3.setCsNo(css);						 
									 
								 }
								 
								// Valiadating CashStmtTemp
									resultMap = BillDAOImpl.validateCashStmt(user, resultMap, errMsg, cs1);
									resultMap = BillDAOImpl.validateCashStmt(user, resultMap, errMsg, cs2);
									if(cs3 != null)
										resultMap = BillDAOImpl.validateCashStmt(user, resultMap, errMsg, cs3);
									
									errStatus = resultMap.get(ConstantsValues.RESULT);
									 if(errStatus != null)
										 if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.ERROR))
											 return resultMap;	 
									
								Integer csId = (Integer) session.save(cs1);
									if(csId < 1){
										resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
										errMsg.add("CashStmt1 is not saved !");
										resultMap.put("msg", errMsg);
										return resultMap;
									}else{
										logger.info("UserID = "+user.getUserId()+ " : CashStmt1ID = "+csId);
										css.getCashStmtList().add(cs1);
									}
										
									csId = (Integer) session.save(cs2);
									if(csId < 1){
										resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
										errMsg.add("CashStmt2 is not saved !");
										resultMap.put("msg", errMsg);
										return resultMap;
									}else{
										logger.info("UserID = "+user.getUserId()+ " : CashStmt2ID = "+csId);
										css.getCashStmtList().add(cs2);
									}
									
									if(cs3 != null){
										csId = (Integer) session.save(cs3);
										if(csId < 1){
											resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
											errMsg.add("CashStmt3 is not saved !");
											resultMap.put("msg", errMsg);
											return resultMap;
										}else{
											css.getCashStmtList().add(cs3);
											logger.info("UserID = "+user.getUserId()+ " : CashStmt3ID = "+csId);
										}
									}
									session.merge(css);
								resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
								resultMap.put("billNo", billNo);
							 }else{
								 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
								 errMsg.add("CashStmtStatus not found for this Bill !");
								 resultMap.put("msg", errMsg);								 
								 logger.info("UserID = "+user.getUserId()+" : CashStmtStatus not found for this Bill !");
							 }
						 }else{
							 // If bill is SUPLEMENTARY
							 bill.setBlBillNo(billNo);
							 bill.setBlRemAmt(bill.getBlFinalTot());
							 bill.setUserCode(user.getUserCode());
							 bill.setbCode(user.getUserBranchCode());
							 
							// Validating Bill									 
							 resultMap = BillDAOImpl.validateBill(user, resultMap, errMsg, bill);
							 Object errStatus = resultMap.get(ConstantsValues.RESULT);
							 if(errStatus != null)
								 if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.ERROR))
									 return resultMap;
							 // Save Bill
							 int billId = (Integer) session.save(bill);
							 
							 if(billId < 1){
								 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
								 errMsg.add("Bill is not saved !");
								 resultMap.put("msg", errMsg);
								 return resultMap;	
							 }
							 
							 logger.info("UserID = "+user.getUserId()+" : BillID = "+billId+"  Bill Detail Size = "+bdSerList.size());
							 // If Bill Detail is not empty
							 if(!bdSerList.isEmpty()){
								 for(int i=0;i<bdSerList.size();i++){
									 BillDetService billDetService = bdSerList.get(i);
									 BillDetail billDetail = billDetService.getBillDetail();
									 
									 billDetail.setUserCode(user.getUserCode());
									 billDetail.setbCode(user.getUserBranchCode());
									 Cnmt cnmt = billDetService.getCnmt();
									 billDetail.setBdCnmtId(cnmt.getCnmtId());

									 // Fetch Bill which is saved above									 
									 Bill freshBill = (Bill) session.get(Bill.class, billId);
									 billDetail.setBill(freshBill);
									 
									 // Validation For Sup. Bill Detail											 
									 resultMap = BillDAOImpl.validateBillDetail(user, resultMap, errMsg, billDetail, "sup");
									 
//									 if(billDetail.getBdRate() <= 0){
//										 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
//										 errMsg.add("Rate sould be greater than 0 !");
//										 resultMap.put("msg", errMsg);
//										 return resultMap;
//									 }									 
									 if(bill.getBlBillDt().compareTo(cnmt.getCnmtDt()) < 0){
										 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
										 errMsg.add("Bill date should be greater than CNMT Date");
										 resultMap.put("msg", errMsg);
										 return resultMap;
									 }
									 if(cnmt.getCnmtDt().compareTo(new Date(dateForKishan.getTime())) >= 0){
										 if(bill.getBlKisanKalCess() < 0){
											 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
											 errMsg.add("Please enter Kishan Kal Cess amount !");
											 resultMap.put("msg", errMsg);
											 return resultMap;
										 }
									 }		
									 errStatus = resultMap.get(ConstantsValues.RESULT);
									 if(errStatus != null)
										 if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.ERROR))
											 return resultMap; 
									 
									 // Save Bill Detail
									 int bdId = (Integer) session.save(billDetail);
									
									 freshBill.getBillDetList().add(billDetail);									 
									 session.merge(freshBill);
									
									 // Fetch Acutal CNMT from database And set Bill No and Bill Id
									 if(bdId > 0){
											logger.info("UserID = "+user.getUserId()+" : BillDetail ID = "+bdId);
										 Cnmt actCnmt = (Cnmt) session.get(Cnmt.class,cnmt.getCnmtId());
										 if(actCnmt.getCnmtSupBillNo() == null || !actCnmt.getCnmtSupBillNo().isEmpty()){
											 ArrayList<String> list = new ArrayList<>();
											 list.add(billNo);
											 actCnmt.setCnmtSupBillNo(list);
										 }else{
											 actCnmt.getCnmtSupBillNo().add(billNo);
										 }
										 if(actCnmt.getCnmtSupBillDet() == null || !actCnmt.getCnmtSupBillDet().isEmpty()){
											 ArrayList<String> list = new ArrayList<>();
											 list.add(String.valueOf(bdId));
											 actCnmt.setCnmtSupBillDet(list);
										 }else{
											 actCnmt.getCnmtSupBillDet().add(String.valueOf(bdId));
										 }
										 session.merge(actCnmt);
										 logger.info("UserID = "+user.getUserId()+" : CnmtID = "+actCnmt.getCnmtId());
									 }else{
										resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
										errMsg.add("Bill Detail is not saved !");
										resultMap.put("msg", errMsg);
										return resultMap;
									 }
								 }
							 }
							 
							 
							 int voucherNo = 0;
							 // Fetch Last CashStmt for generating Voucher No
							 Criteria cr = session.createCriteria(CashStmt.class);
							 cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT, bill.getBlBillDt()));
							 cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE, user.getUserBranchCode()));
							 cr.addOrder(Order.desc(CashStmtCNTS.CS_ID));						 
							 ProjectionList proList = Projections.projectionList();
							 proList.add(Projections.property("csVouchNo"));
							 cr.setProjection(proList);
							 cr.setMaxResults(1);								
							 List<Integer>  voucherNoList = cr.list();
							 if(!voucherNoList.isEmpty()){
								int vNo = voucherNoList.get(0);
								voucherNo = vNo + 1;
							 }else{
								voucherNo = 1;
							 }
							 
							 // Fetch CashStmtStatus
							 List<CashStmtStatus> cssList = new ArrayList<>();
							 cr = session.createCriteria(CashStmtStatus.class);
							 cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, user.getUserBranchCode()));
							 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT, bill.getBlBillDt()));
							 cssList = cr.list();
							 							 
							 if(!cssList.isEmpty()){
								 
								logger.info("UserID = "+user.getUserId()+" : CashStmt is going on.....");
								 CashStmtStatus css = cssList.get(0);
								 
								 CashStmt cs1 = new CashStmt();								 
								 cs1.setUserCode(user.getUserCode());
								 cs1.setbCode(user.getUserBranchCode());
								 cs1.setCsDescription("As Per Bill File");
								 cs1.setCsDrCr('C');
								 cs1.setCsAmt(bill.getBlSubTot());
								 cs1.setCsType("BILL");
								 cs1.setCsTvNo(billNo);
								 cs1.setCsVouchType("CONTRA");
								 cs1.setCsFaCode(frghtCode);
								 cs1.setCsVouchNo(voucherNo);
								 cs1.setCsDt(css.getCssDt());
								 cs1.setCsNo(css);	
								 
								 CashStmt cs2 = new CashStmt();
								 cs2.setUserCode(user.getUserCode());
								 cs2.setbCode(user.getUserBranchCode());
								 cs2.setCsDescription("As Per Bill File");
								 cs2.setCsDrCr('D');
								 cs2.setCsAmt(bill.getBlFinalTot());
								 cs2.setCsType("BILL");
								 cs2.setCsTvNo(billNo);
								 cs2.setCsVouchType("CONTRA");
								 cs2.setCsFaCode(customer.getCustFaCode());
								 cs2.setCsVouchNo(voucherNo);
								 cs2.setCsDt(css.getCssDt());
								 cs2.setCsNo(css);
								 
								 CashStmt cs3 = null;
								// Bill final total is greater than bill sub total
								 if(bill.getBlFinalTot() - bill.getBlSubTot() > 0){
									 CashStmt cs = new CashStmt();
									 cs.setUserCode(user.getUserCode());
									 cs.setbCode(user.getUserBranchCode());
									 cs.setCsDescription("As Per Bill File");
									 cs.setCsDrCr('C');
									 cs.setCsAmt(bill.getBlSerTax() + bill.getBlSwachBhCess()+bill.getBlKisanKalCess());
									 cs.setCsType("BILL");
									 cs.setCsTvNo(billNo);
									 cs.setCsVouchType("CONTRA");
									 cs.setCsFaCode(frghtCode);
									 cs.setCsVouchNo(voucherNo);
									 cs.setCsDt(css.getCssDt());
									 cs.setCsNo(css);								
								 }			
								 								 						 
								// Valiadating CashStmtTemp
								resultMap = BillDAOImpl.validateCashStmt(user, resultMap, errMsg, cs1);
								resultMap = BillDAOImpl.validateCashStmt(user, resultMap, errMsg, cs2);
								if(cs3 != null)
									resultMap = BillDAOImpl.validateCashStmt(user, resultMap, errMsg, cs3);
								
								errStatus = resultMap.get(ConstantsValues.RESULT);
								if(errStatus != null)
									if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.ERROR))
										 return resultMap;	 
									
								Integer csId = (Integer) session.save(cs1);
								if(csId < 1){
									resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
									errMsg.add("CashStmt1 is not saved !");
									resultMap.put("msg", errMsg);
									return resultMap;
								}else{
									logger.info("UserID = "+user.getUserId()+" : CashStmt1ID = "+csId);
									css.getCashStmtList().add(cs1);
								}
								
								csId = (Integer) session.save(cs2);
								if(csId < 1){
									resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
									errMsg.add("CashStmt2 is not saved !");
									resultMap.put("msg", errMsg);
									return resultMap;
								}else{
									logger.info("UserID = "+user.getUserId()+" : CashStmt2ID = "+csId);
									css.getCashStmtList().add(cs2);
								}
								
								if(cs3 != null){
									csId = (Integer) session.save(cs3);
									if(csId < 1){
										resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
										errMsg.add("CashStmt3 is not saved !");
										resultMap.put("msg", errMsg);
										return resultMap;
									}else{
										logger.info("UserID = "+user.getUserId()+" : CashStmt3ID = "+csId);
										css.getCashStmtList().add(cs3);
									}
								}	
								session.merge(css);
								 logger.info("UserID = "+user.getUserId()+" : Bill is saved !");					 
								 resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
								 resultMap.put("billNo", billNo);
							 }else{
								 logger.info("UserID = "+user.getUserId()+" : CashStmtStatus not found !");
								 errMsg.add("CashStmtStatus not found !");
								 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
								 resultMap.put("msg", errMsg);
							 }
							 
						 }
					 }
					
				 }else{
					 logger.info("UserID = "+user.getUserId()+" : Invalid customer or branch billing");
					 errMsg.add("Invalid customer or branch billing !");
					 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					 resultMap.put("msg", errMsg);
				 }
			 }else{
				 logger.info("UserID = "+user.getUserId()+" : User does not have any branch !");
				 errMsg.add("User does not have any branch !");
				 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				 resultMap.put("msg", errMsg);
			 }
		 }catch(Exception e){
			 logger.info("UserID = "+user.getUserId()+" : Exception = "+e);
			 errMsg.add("Bill is not saved !");
			 resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			 resultMap.put("msg", errMsg);
		 }
		 return resultMap;
	 }
	
	public static Map<String, Object> validateBill(User user, Map<String, Object> resultMap, List<String> errMsg, Bill bill){
		 Validator validator = ValidatorFactoryUtil.getValidator();
		 Set<ConstraintViolation<Bill>> billErrMsg = validator.validate(bill, BillInsert.class);
		 if(! billErrMsg.isEmpty()){
			Iterator<ConstraintViolation<Bill>> it = billErrMsg.iterator();
			while(it.hasNext()){
				ConstraintViolation<Bill> eMsg = it.next();
				errMsg.add(eMsg.getMessage());
				logger.error("UserID = "+user.getUserId()+" : Validation MSG = "+eMsg.getMessage());
			}
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", errMsg);
		 }
		return resultMap;
	}
	public static Map<String, Object> validateBillDetail(User user, Map<String, Object> resultMap, List<String> errMsg, BillDetail billDt, String type){
		 Validator validator = ValidatorFactoryUtil.getValidator();
		 Set<ConstraintViolation<BillDetail>> billErrMsg = null;
		 
		 if(type.equalsIgnoreCase("normal"))			 
			 billErrMsg = validator.validate(billDt, BillDetailNormal.class);
		 else if(type.equalsIgnoreCase("sup"))
			 billErrMsg = validator.validate(billDt, BillDetailSup.class);
		 
		 if(! billErrMsg.isEmpty()){
			Iterator<ConstraintViolation<BillDetail>> it = billErrMsg.iterator();
			while(it.hasNext()){
				ConstraintViolation<BillDetail> eMsg = it.next();
				errMsg.add(eMsg.getMessage());
				logger.error("UserID = "+user.getUserId()+" : Validation MSG = "+eMsg.getMessage());
			}
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", errMsg);
		 }
		return resultMap;
	}
	
	public static Map<String, Object> validateCashStmtTemp(User user, Map<String, Object> resultMap, List<String> errMsg, CashStmtTemp cs){
		 Validator validator = ValidatorFactoryUtil.getValidator();
		 Set<ConstraintViolation<CashStmtTemp>> billErrMsg = validator.validate(cs, CashStmtTempBillAdvInsert.class);
		 if(! billErrMsg.isEmpty()){
			Iterator<ConstraintViolation<CashStmtTemp>> it = billErrMsg.iterator();
			while(it.hasNext()){
				ConstraintViolation<CashStmtTemp> eMsg = it.next();
				errMsg.add(eMsg.getMessage());
				logger.error("UserID = "+user.getUserId()+" : Validation MSG = "+eMsg.getMessage());
			}
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", errMsg);
		 }
		return resultMap;
	}
	
	public static Map<String, Object> validateCashStmt(User user, Map<String, Object> resultMap, List<String> errMsg, CashStmt cs){
		 Validator validator = ValidatorFactoryUtil.getValidator();
		 Set<ConstraintViolation<CashStmt>> billErrMsg = validator.validate(cs, CashStmtBillInsert.class);
		 if(! billErrMsg.isEmpty()){
			Iterator<ConstraintViolation<CashStmt>> it = billErrMsg.iterator();
			while(it.hasNext()){
				ConstraintViolation<CashStmt> eMsg = it.next();
				errMsg.add(eMsg.getMessage());
				logger.error("UserID = "+user.getUserId()+" : Validation MSG = "+eMsg.getMessage());
			}
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", errMsg);
		 }
		return resultMap;
	}
	
	public static Map<String, Object> validateCNMT(User user, Map<String, Object> resultMap, List<String> errMsg, Cnmt cnmt){
		 Validator validator = ValidatorFactoryUtil.getValidator();
		 Set<ConstraintViolation<Cnmt>> billErrMsg = validator.validate(cnmt, CnmtBillInsert.class);
		 if(! billErrMsg.isEmpty()){
			Iterator<ConstraintViolation<Cnmt>> it = billErrMsg.iterator();
			while(it.hasNext()){
				ConstraintViolation<Cnmt> eMsg = it.next();
				errMsg.add(eMsg.getMessage());
				logger.error("UserID = "+user.getUserId()+" : Validation MSG = "+eMsg.getMessage());
			}
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", errMsg);
		 }		
		return resultMap;
	}
	
	
	@SuppressWarnings("unchecked")
	public String saveAdvBillM(BillService billService,Session session){
		System.out.println("enter into saveAdvBill function");
		String billNo = "";
		billNo=billService.getBill().getBlBillNo();
		User currentUser = (User)httpSession.getAttribute("currentUser");
			 
			 String frghtCode = "";
			 String srvTaxCode = "";
			 List<FAMaster> famList = new ArrayList<>();
				
			famList = session.createCriteria(FAMaster.class)
					  .add(Restrictions.disjunction()
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_COLLECTION))
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_SRV_TAX))
				  ).list(); 
				
			if(!famList.isEmpty()){
					for(int i=0;i<famList.size();i++){
						if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_COLLECTION)){
							frghtCode = famList.get(i).getFaMfaCode();
						}else if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_SRV_TAX)){
							srvTaxCode = famList.get(i).getFaMfaCode();
						}else{
							System.out.println("invalid result");
						}
					}
				}
				
			 Branch branch = (Branch) session.get(Branch.class,Integer.parseInt(currentUser.getUserBranchCode()));
				 List<BillDetService> bdSerList = billService.getBdSerList();
				 Bill bill = billService.getBill();
				 //bill.setBlBillZ("Z");
				 Customer customer = (Customer) session.get(Customer.class,bill.getBlCustId());
				 
				 if(bill.getBlCustId() > 0 && bill.getBlBrhId() > 0){
					 
					 System.out.println("Bill No : "+billNo);
					 
					 if(billNo != null){
						 
						 if(bill.getBlType() == 'N'){
							 bill.setBlBillNo(billNo);
							 if(billService.getBill().getBlGstNo() != null)
								 bill.setBlStateGST(billService.getBill().getBlGstNo().substring(0, 2));
							 bill.setBlRemAmt(bill.getBlFinalTot());
							 bill.setUserCode(currentUser.getUserCode());
							 bill.setbCode(currentUser.getUserBranchCode());
							 int billId = (Integer) session.save(bill);
							 if(!bdSerList.isEmpty()){
								 for(int i=0;i<bdSerList.size();i++){
									 BillDetService billDetService = bdSerList.get(i);
									 BillDetail billDetail = billDetService.getBillDetail();
									 Cnmt cnmt = billDetService.getCnmt();
									 Cnmt actCnmt = (Cnmt) session.get(Cnmt.class,cnmt.getCnmtId());
									 
									 billDetail.setUserCode(currentUser.getUserCode());
									 billDetail.setbCode(currentUser.getUserBranchCode());
									 billDetail.setBdCnmtId(cnmt.getCnmtId());
									 
									 Bill freshBill = (Bill) session.get(Bill.class,billId);
									 billDetail.setBill(freshBill);
									 int bdId = (Integer) session.save(billDetail);
									 freshBill.getBillDetList().add(billDetail);
									 session.merge(freshBill);
									 
									 actCnmt.setCnmtBillDetId(bdId);
									 actCnmt.setCnmtBillNo(billNo);
									 session.merge(actCnmt);
								 }
							 }
							 
							
								 
							     CashStmtTemp cs1 = new CashStmtTemp();
								 cs1.setUserCode(currentUser.getUserCode());
								 cs1.setbCode(currentUser.getUserBranchCode());
								 //cs1.setCsDescription(bill.getBlDesc());
								 cs1.setCsDescription("As Per Bill File");
								 cs1.setCsDrCr('C');
								 cs1.setCsAmt(bill.getBlSubTot());
								 cs1.setCsType("BILL");
								 cs1.setCsTvNo(billNo);
								 cs1.setCsVouchType("CONTRA");
								 cs1.setCsFaCode(frghtCode);
								 cs1.setCsBrhFaCode(branch.getBranchFaCode());
								 //cs1.setCsVouchNo(voucherNo);
								 cs1.setCsDt(bill.getBlBillDt());
								 session.save(cs1);
								 
								 if(bill.getBlFinalTot() - bill.getBlSubTot() > 0){
									 CashStmtTemp cs = new CashStmtTemp();
									 cs.setUserCode(currentUser.getUserCode());
									 cs.setbCode(currentUser.getUserBranchCode());
									 //cs.setCsDescription(bill.getBlDesc());
									 cs.setCsDescription("As Per Bill File");
									 cs.setCsDrCr('C');
									 cs.setCsAmt(bill.getBlSerTax() + bill.getBlSwachBhCess()+bill.getBlKisanKalCess());
									 cs.setCsType("BILL");
									 cs.setCsTvNo(billNo);
									 cs.setCsVouchType("CONTRA");
									 cs.setCsFaCode(srvTaxCode);
									 cs.setCsBrhFaCode(branch.getBranchFaCode());
									 //cs.setCsVouchNo(voucherNo);
									 cs.setCsDt(bill.getBlBillDt());
									 session.save(cs);
								 }
								 
								 
								 
								 CashStmtTemp cs2 = new CashStmtTemp();
								 cs2.setUserCode(currentUser.getUserCode());
								 cs2.setbCode(currentUser.getUserBranchCode());
								 //cs2.setCsDescription(bill.getBlDesc());
								 cs2.setCsDescription("As Per Bill File");
								 cs2.setCsDrCr('D');
								 cs2.setCsAmt(bill.getBlFinalTot());
								 cs2.setCsType("BILL");
								 cs2.setCsTvNo(billNo);
								 cs2.setCsVouchType("CONTRA");
								 cs2.setCsFaCode(customer.getCustFaCode());
								 //cs2.setCsVouchNo(voucherNo);
								 cs2.setCsBrhFaCode(branch.getBranchFaCode());
								 cs2.setCsDt(bill.getBlBillDt());
								 session.save(cs2);
							 
						 }else{
							 bill.setBlBillNo(billNo);
							 if(billService.getBill().getBlGstNo() != null)
								 bill.setBlStateGST(billService.getBill().getBlGstNo().substring(0, 2));
							 bill.setBlRemAmt(bill.getBlFinalTot());
							 bill.setUserCode(currentUser.getUserCode());
							 bill.setbCode(currentUser.getUserBranchCode());
							 int billId = (Integer) session.save(bill);
							 
							 if(!bdSerList.isEmpty()){
								 for(int i=0;i<bdSerList.size();i++){
									 BillDetService billDetService = bdSerList.get(i);
									 BillDetail billDetail = billDetService.getBillDetail();
									 Cnmt cnmt = billDetService.getCnmt();
									 Cnmt actCnmt = (Cnmt) session.get(Cnmt.class,cnmt.getCnmtId());
									 
									 billDetail.setUserCode(currentUser.getUserCode());
									 billDetail.setbCode(currentUser.getUserBranchCode());
									 billDetail.setBdCnmtId(cnmt.getCnmtId());
									 
									 Bill freshBill = (Bill) session.get(Bill.class,billId);
									 billDetail.setBill(freshBill);
									 int bdId = (Integer) session.save(billDetail);
									 freshBill.getBillDetList().add(billDetail);
									 session.merge(freshBill);
									 
									 if(actCnmt.getCnmtSupBillNo() == null || !actCnmt.getCnmtSupBillNo().isEmpty()){
										 ArrayList<String> list = new ArrayList<>();
										 list.add(billNo);
										 actCnmt.setCnmtSupBillNo(list);
									 }else{
										 actCnmt.getCnmtSupBillNo().add(billNo);
									 }
									 
									 if(actCnmt.getCnmtSupBillDet() == null || !actCnmt.getCnmtSupBillDet().isEmpty()){
										 ArrayList<String> list = new ArrayList<>();
										 list.add(String.valueOf(bdId));
										 actCnmt.setCnmtSupBillDet(list);
									 }else{
										 actCnmt.getCnmtSupBillDet().add(String.valueOf(bdId));
									 }
									 
									 session.merge(actCnmt);
								 }
							 }
							 
							 
							  
								 CashStmtTemp cs1 = new CashStmtTemp();
								 cs1.setUserCode(currentUser.getUserCode());
								 cs1.setbCode(currentUser.getUserBranchCode());
								 //cs1.setCsDescription(bill.getBlDesc());
								 cs1.setCsDescription("As Per Bill File");
								 cs1.setCsDrCr('C');
								 cs1.setCsAmt(bill.getBlSubTot());
								 cs1.setCsType("BILL");
								 cs1.setCsTvNo(billNo);
								 cs1.setCsVouchType("CONTRA");
								 cs1.setCsFaCode(frghtCode);
								 //cs1.setCsVouchNo(voucherNo);
								 cs1.setCsBrhFaCode(branch.getBranchFaCode());
								 cs1.setCsDt(bill.getBlBillDt());
								  session.save(cs1);
								 
								 if(bill.getBlFinalTot() - bill.getBlSubTot() > 0){
									 CashStmtTemp cs = new CashStmtTemp();
									 cs.setUserCode(currentUser.getUserCode());
									 cs.setbCode(currentUser.getUserBranchCode());
									 //cs.setCsDescription(bill.getBlDesc());
									 cs.setCsDescription("As Per Bill File");
									 cs.setCsDrCr('C');
									 cs.setCsAmt(bill.getBlSerTax() + bill.getBlSwachBhCess()+bill.getBlKisanKalCess());
									 cs.setCsType("BILL");
									 cs.setCsTvNo(billNo);
									 cs.setCsVouchType("CONTRA");
									 cs.setCsFaCode(frghtCode);
									 //cs.setCsVouchNo(voucherNo);
									 cs.setCsBrhFaCode(branch.getBranchFaCode());
									 cs.setCsDt(bill.getBlBillDt());
									 
									 session.save(cs);
								 }
								
								 
						    	 CashStmtTemp cs2 = new CashStmtTemp();
								 cs2.setUserCode(currentUser.getUserCode());
								 cs2.setbCode(currentUser.getUserBranchCode());
								 //cs2.setCsDescription(bill.getBlDesc());
								 cs2.setCsDescription("As Per Bill File");
								 cs2.setCsDrCr('D');
								 cs2.setCsAmt(bill.getBlFinalTot());
								 cs2.setCsType("BILL");
								 cs2.setCsTvNo(billNo);
								 cs2.setCsVouchType("CONTRA");
								 cs2.setCsFaCode(customer.getCustFaCode());
								 //cs2.setCsVouchNo(voucherNo);
								 cs2.setCsBrhFaCode(branch.getBranchFaCode());
								 cs2.setCsDt(bill.getBlBillDt());
								 session.save(cs2);
						 }
					 }else{
						 logger.info("Bill No. Not found");
					 }
				 }else{
					 System.out.println("invalid customer or branch for billing");
				 }
		return billNo;
	}

	
	
	@Override
	public String saveRelGstBill(BillService billService,Session session){
		System.out.println("enter into saveAdvBill function");
		String billNo = "";
		billNo=billService.getBill().getBlBillNo();
		User currentUser = (User)httpSession.getAttribute("currentUser");
			 
			// String frghtCode = "";
			// String srvTaxCode = "";
		//	 List<FAMaster> famList = new ArrayList<>();
				
//			famList = session.createCriteria(FAMaster.class)
//					  .add(Restrictions.disjunction()
//				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_COLLECTION))
//				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_SRV_TAX))
//				  ).list(); 
				
//			if(!famList.isEmpty()){
//					for(int i=0;i<famList.size();i++){
//						if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_COLLECTION)){
//							frghtCode = famList.get(i).getFaMfaCode();
//						}else if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_SRV_TAX)){
//							srvTaxCode = famList.get(i).getFaMfaCode();
//						}else{
//							System.out.println("invalid result");
//						}
//					}
//				}
				
			// Branch branch = (Branch) session.get(Branch.class,Integer.parseInt(currentUser.getUserBranchCode()));
				 List<BillDetService> bdSerList = billService.getBdSerList();
				 Bill bill = billService.getBill();
				 bill.setBlBillZ("Z");
			//	 Customer customer = (Customer) session.get(Customer.class,bill.getBlCustId());
				 
				 if(bill.getBlCustId() > 0 && bill.getBlBrhId() > 0){
					 
					 System.out.println("Bill No : "+billNo);
					 
					 if(billNo != null){
						 
						 if(bill.getBlType() == 'N'){
							 bill.setBlBillNo(billNo);
							 if(billService.getBill().getBlGstNo() != null)
								 bill.setBlStateGST(billService.getBill().getBlGstNo().substring(0, 2));
							 bill.setBlRemAmt(bill.getBlFinalTot());
							 bill.setUserCode(currentUser.getUserCode());
							 bill.setbCode(currentUser.getUserBranchCode());
							 int billId = (Integer) session.save(bill);
							 if(!bdSerList.isEmpty()){
								 for(int i=0;i<bdSerList.size();i++){
									 BillDetService billDetService = bdSerList.get(i);
									 BillDetail billDetail = billDetService.getBillDetail();
									 Cnmt cnmt = billDetService.getCnmt();
									 Cnmt actCnmt = (Cnmt) session.get(Cnmt.class,cnmt.getCnmtId());
									 
									 billDetail.setUserCode(currentUser.getUserCode());
									 billDetail.setbCode(currentUser.getUserBranchCode());
									 billDetail.setBdCnmtId(cnmt.getCnmtId());
									 
									 Bill freshBill = (Bill) session.get(Bill.class,billId);
									 billDetail.setBill(freshBill);
									 int bdId = (Integer) session.save(billDetail);
									 freshBill.getBillDetList().add(billDetail);
									 session.merge(freshBill);
									 
									 actCnmt.setCnmtBillDetId(bdId);
									 actCnmt.setCnmtBillNo(billNo);
									 session.merge(actCnmt);
								 }
							 }
							 
							
								 
//							     CashStmtTemp cs1 = new CashStmtTemp();
//								 cs1.setUserCode(currentUser.getUserCode());
//								 cs1.setbCode(currentUser.getUserBranchCode());
//								 //cs1.setCsDescription(bill.getBlDesc());
//								 cs1.setCsDescription("As Per Bill File");
//								 cs1.setCsDrCr('C');
//								 cs1.setCsAmt(bill.getBlSubTot());
//								 cs1.setCsType("BILL");
//								 cs1.setCsTvNo(billNo);
//								 cs1.setCsVouchType("CONTRA");
//								 cs1.setCsFaCode(frghtCode);
//								 cs1.setCsBrhFaCode(branch.getBranchFaCode());
//								 //cs1.setCsVouchNo(voucherNo);
//								 cs1.setCsDt(bill.getBlBillDt());
//								 session.save(cs1);
//								 
//								 if(bill.getBlFinalTot() - bill.getBlSubTot() > 0){
//									 CashStmtTemp cs = new CashStmtTemp();
//									 cs.setUserCode(currentUser.getUserCode());
//									 cs.setbCode(currentUser.getUserBranchCode());
//									 //cs.setCsDescription(bill.getBlDesc());
//									 cs.setCsDescription("As Per Bill File");
//									 cs.setCsDrCr('C');
//									 cs.setCsAmt(bill.getBlSerTax() + bill.getBlSwachBhCess()+bill.getBlKisanKalCess());
//									 cs.setCsType("BILL");
//									 cs.setCsTvNo(billNo);
//									 cs.setCsVouchType("CONTRA");
//									 cs.setCsFaCode(srvTaxCode);
//									 cs.setCsBrhFaCode(branch.getBranchFaCode());
//									 //cs.setCsVouchNo(voucherNo);
//									 cs.setCsDt(bill.getBlBillDt());
//									 session.save(cs);
//								 }
//								 
//								 
//								 
//								 CashStmtTemp cs2 = new CashStmtTemp();
//								 cs2.setUserCode(currentUser.getUserCode());
//								 cs2.setbCode(currentUser.getUserBranchCode());
//								 //cs2.setCsDescription(bill.getBlDesc());
//								 cs2.setCsDescription("As Per Bill File");
//								 cs2.setCsDrCr('D');
//								 cs2.setCsAmt(bill.getBlFinalTot());
//								 cs2.setCsType("BILL");
//								 cs2.setCsTvNo(billNo);
//								 cs2.setCsVouchType("CONTRA");
//								 cs2.setCsFaCode(customer.getCustFaCode());
//								 //cs2.setCsVouchNo(voucherNo);
//								 cs2.setCsBrhFaCode(branch.getBranchFaCode());
//								 cs2.setCsDt(bill.getBlBillDt());
//								 session.save(cs2);
							 
						 }else{
							 bill.setBlBillNo(billNo);
							 if(billService.getBill().getBlGstNo() != null)
								 bill.setBlStateGST(billService.getBill().getBlGstNo().substring(0, 2));
							 bill.setBlRemAmt(bill.getBlFinalTot());
							 bill.setUserCode(currentUser.getUserCode());
							 bill.setbCode(currentUser.getUserBranchCode());
							 int billId = (Integer) session.save(bill);
							 
							 if(!bdSerList.isEmpty()){
								 for(int i=0;i<bdSerList.size();i++){
									 BillDetService billDetService = bdSerList.get(i);
									 BillDetail billDetail = billDetService.getBillDetail();
									 Cnmt cnmt = billDetService.getCnmt();
									 Cnmt actCnmt = (Cnmt) session.get(Cnmt.class,cnmt.getCnmtId());
									 
									 billDetail.setUserCode(currentUser.getUserCode());
									 billDetail.setbCode(currentUser.getUserBranchCode());
									 billDetail.setBdCnmtId(cnmt.getCnmtId());
									 
									 Bill freshBill = (Bill) session.get(Bill.class,billId);
									 billDetail.setBill(freshBill);
									 int bdId = (Integer) session.save(billDetail);
									 freshBill.getBillDetList().add(billDetail);
									 session.merge(freshBill);
									 
									 if(actCnmt.getCnmtSupBillNo() == null || !actCnmt.getCnmtSupBillNo().isEmpty()){
										 ArrayList<String> list = new ArrayList<>();
										 list.add(billNo);
										 actCnmt.setCnmtSupBillNo(list);
									 }else{
										 actCnmt.getCnmtSupBillNo().add(billNo);
									 }
									 
									 if(actCnmt.getCnmtSupBillDet() == null || !actCnmt.getCnmtSupBillDet().isEmpty()){
										 ArrayList<String> list = new ArrayList<>();
										 list.add(String.valueOf(bdId));
										 actCnmt.setCnmtSupBillDet(list);
									 }else{
										 actCnmt.getCnmtSupBillDet().add(String.valueOf(bdId));
									 }
									 
									 session.merge(actCnmt);
								 }
							 }
							 
							 
//							  
//								 CashStmtTemp cs1 = new CashStmtTemp();
//								 cs1.setUserCode(currentUser.getUserCode());
//								 cs1.setbCode(currentUser.getUserBranchCode());
//								 //cs1.setCsDescription(bill.getBlDesc());
//								 cs1.setCsDescription("As Per Bill File");
//								 cs1.setCsDrCr('C');
//								 cs1.setCsAmt(bill.getBlSubTot());
//								 cs1.setCsType("BILL");
//								 cs1.setCsTvNo(billNo);
//								 cs1.setCsVouchType("CONTRA");
//								 cs1.setCsFaCode(frghtCode);
//								 //cs1.setCsVouchNo(voucherNo);
//								 cs1.setCsBrhFaCode(branch.getBranchFaCode());
//								 cs1.setCsDt(bill.getBlBillDt());
//								  session.save(cs1);
//								 
//								 if(bill.getBlFinalTot() - bill.getBlSubTot() > 0){
//									 CashStmtTemp cs = new CashStmtTemp();
//									 cs.setUserCode(currentUser.getUserCode());
//									 cs.setbCode(currentUser.getUserBranchCode());
//									 //cs.setCsDescription(bill.getBlDesc());
//									 cs.setCsDescription("As Per Bill File");
//									 cs.setCsDrCr('C');
//									 cs.setCsAmt(bill.getBlSerTax() + bill.getBlSwachBhCess()+bill.getBlKisanKalCess());
//									 cs.setCsType("BILL");
//									 cs.setCsTvNo(billNo);
//									 cs.setCsVouchType("CONTRA");
//									 cs.setCsFaCode(frghtCode);
//									 //cs.setCsVouchNo(voucherNo);
//									 cs.setCsBrhFaCode(branch.getBranchFaCode());
//									 cs.setCsDt(bill.getBlBillDt());
//									 
//									 session.save(cs);
//								 }
//								
//								 
//						    	 CashStmtTemp cs2 = new CashStmtTemp();
//								 cs2.setUserCode(currentUser.getUserCode());
//								 cs2.setbCode(currentUser.getUserBranchCode());
//								 //cs2.setCsDescription(bill.getBlDesc());
//								 cs2.setCsDescription("As Per Bill File");
//								 cs2.setCsDrCr('D');
//								 cs2.setCsAmt(bill.getBlFinalTot());
//								 cs2.setCsType("BILL");
//								 cs2.setCsTvNo(billNo);
//								 cs2.setCsVouchType("CONTRA");
//								 cs2.setCsFaCode(customer.getCustFaCode());
//								 //cs2.setCsVouchNo(voucherNo);
//								 cs2.setCsBrhFaCode(branch.getBranchFaCode());
//								 cs2.setCsDt(bill.getBlBillDt());
//								 session.save(cs2);
						 }
					 }else{
						 logger.info("Bill No. Not found");
					 }
				 }else{
					 System.out.println("invalid customer or branch for billing");
				 }
		return billNo;
	}

	
	
	
	
	
	@Override
	public String getNewBillByBr(int brhId){
		
		Session session=null;
		session=this.sessionFactory.openSession();
		//User currentUser = (User)httpSession.getAttribute("currentUser");
		String billNo="";
		try{
			
			
			 String brh = "";
			 Branch branch = (Branch) session.get(Branch.class,brhId);
			 if(branch != null){
				brh = branch.getBranchName().substring(0,3).toUpperCase();
				if(branch.getBranchName().equals("Raipur")){
					brh="RIP";
				}
			 }
			 
			 System.out.println("Branch==="+brh);
			SQLQuery query=session.createSQLQuery("select max(blBillNo) billNo from bill where  blBillNo like :brh");
			query.setString("brh", brh+"%");
			List list=query.list();
			
			if(!list.isEmpty()){
				 String prevBillNo = list.get(0).toString();
				 System.out.println("prevBillNo="+prevBillNo);
				 billNo  = brh + String.valueOf(Integer.parseInt(prevBillNo.substring(3)) + 1 + 1000000).substring(1);
				 System.out.println("if billNo="+billNo);
			 }else{
				 billNo = brh+"000001";
			 }
			
			
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.clear();
			session.close();
		}
		
		return billNo;
	}
	
	

	
	@Override
	public Map<String,Object> getNewBillByBrh(int brhId){
		
		Session session=null;
		session=this.sessionFactory.openSession();
		//User currentUser = (User)httpSession.getAttribute("currentUser");
		Map<String,Object> map=new HashMap<>();
		String billNo="";
		try{
			
			 String brh = "";
			 Branch branch = (Branch) session.get(Branch.class,brhId);
			 if(branch != null){
				brh = branch.getBranchName().substring(0,3).toUpperCase();
				if(branch.getBranchName().equals("Raipur")){
					brh="RIP";
				}
			 }
			 
			 System.out.println("Branch==="+brh);
			SQLQuery query=session.createSQLQuery("select blBillNo,blCancel,b.bfId,bfNo,bfRecDt from bill b " +
					"left join billforwarding_bill bfb on b.blId=bfb.blId " +
					"left join billforwarding bf on bfb.bfId=bf.bfId " +
					" where blBillNo =(select max(blBillNo) billNo from bill where blBillZ='Z' and blBillNo like :brh)");
			query.setString("brh", brh+"%");
			List list=query.list();
			
			if(!list.isEmpty()){
				
				Object[] object=(Object[]) list.get(0);
				 String prevBillNo = object[0].toString();
				 System.out.println("prevBillNo="+prevBillNo);
				 
				/* if(object[2] != null && object[4] == null && object[1].toString() == "false"){
					 map.put("msg","please enter submission date of "+prevBillNo);
				 }else{*/
						 billNo  = brh + String.valueOf(Integer.parseInt(prevBillNo.substring(3)) + 1 + 1000000).substring(1);
						 System.out.println("if billNo="+billNo);
						 map.put("billNo",billNo);
						 map.put("msg","ok");
				 //} 
			 }else{
				 billNo = brh+"000001";
				 map.put("billNo",billNo);
				 map.put("msg","ok");
			 }

		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception in getting new bill with submission date"+e);
		}finally{
			session.clear();
			session.close();
		}
		
		return map;
	}


	
	
	
}

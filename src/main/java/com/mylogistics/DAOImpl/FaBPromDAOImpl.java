package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.FaBPromDAO;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.constants.CustomerCNTS;
import com.mylogistics.constants.FAMasterCNTS;
import com.mylogistics.constants.FaBPromCNTS;
import com.mylogistics.constants.bank.ChequeLeavesCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.model.Customer;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FaBProm;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.services.BPVoucherService;
import com.mylogistics.services.BPromService;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;
//import java.util.Date;

public class FaBPromDAOImpl implements FaBPromDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	private HttpSession httpSession;
	
	
	@Autowired
	public FaBPromDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	 @SuppressWarnings("unchecked")
	public Map<String,Object> saveBPVoucByCash(VoucherService voucherService, BPVoucherService bpVoucherService){
		 System.out.println("enter into saveBPVoucByCash function");
		 Map<String,Object> map = new HashMap<String,Object>();
		 User currentUser = (User) httpSession.getAttribute("currentUser");
		 String brFaCode = voucherService.getBranch().getBranchFaCode();
		 CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
		 //java.sql.Date sqlDate = new java.sql.Date(new Date().getTime());
		 List<BPromService> bpsList = bpVoucherService.getAllBPromSer();
		 List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		 List<Customer> custList = new ArrayList<Customer>();
		 List<FAMaster> faMList = new ArrayList<FAMaster>();
		 int voucherNo = 0;
		 String payMode = "C";
		 if(!bpsList.isEmpty()){
			 try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 
				 java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
				 
				 Criteria crr = session.createCriteria(FAMaster.class);
				 crr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.B_PROM_FANAME));
				 faMList =crr.list();
				 String bpFaCode = faMList.get(0).getFaMfaCode();
				 
				 Criteria cr = session.createCriteria(CashStmt.class);
				 cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
				 cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
				 ProjectionList proList = Projections.projectionList();
				 proList.add(Projections.property("csVouchNo"));
				 cr.setProjection(proList);
					
				 List<Integer>  voucherNoList = cr.list();
				 if(!voucherNoList.isEmpty()){
						int vNo = voucherNoList.get(voucherNoList.size() - 1);
						voucherNo = vNo + 1;
				 }else{
						voucherNo = 1;
				 }
				 
				 String tdsCode = voucherService.getTdsCode();
					double tdsAmt = voucherService.getTdsAmt();
					System.out.println("tdsCode = "+tdsCode+" --- tdsAmt = "+tdsAmt);
					/*if(tdsCode != null && tdsAmt > 0){
						
						CashStmt csWTds = new CashStmt();
						csWTds.setbCode(currentUser.getUserBranchCode());
						csWTds.setUserCode(currentUser.getUserCode());
						csWTds.setCsDescription(voucherService.getDesc());
						csWTds.setCsDrCr('C');
						csWTds.setCsAmt(tdsAmt);
						csWTds.setCsType(voucherService.getVoucherType());
						csWTds.setCsVouchType("cash");
						csWTds.setCsPayTo(voucherService.getPayTo());
						csWTds.setCsTvNo(ConstantsValues.CONST_TVNO);
						csWTds.setCsFaCode(tdsCode);
						csWTds.setCsVouchNo(voucherNo);
						csWTds.setCsDt(sqlDate);
						
						Criteria crr1 = session.createCriteria(CashStmtStatus.class);
						crr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = crr1.list();
						
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							csWTds.setCsNo(csStatus);
							csStatus.getCashStmtList().add(csWTds);
							System.out.println("tds case successfully saved");
							session.update(csStatus);
							//transaction.commit();
						}	
					}*/
				 
				 
				 for(int i=0 ; i<bpsList.size() ; i++){
					 BPromService bPromService = bpsList.get(i);
					 Customer customer = bPromService.getCustomer();
					 FaBProm faBProm = bPromService.getFaBProm();
					 
					java.util.Date date = new java.util.Date();
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(date);
					long timeInMillis = calendar.getTimeInMillis();	
					 
					String tvNo = String.valueOf(timeInMillis);			 
					 
					 Criteria cr1 = session.createCriteria(Customer.class);
					 cr1.add(Restrictions.eq(CustomerCNTS.CUST_ID,customer.getCustId()));
					 custList = cr1.list();
					 
					 Customer actCust = custList.get(0);
					 Set<Branch> branchSet = new HashSet<Branch>();
					 branchSet = actCust.getBranches();
					 System.out.println("size of branchSet = "+branchSet.size());
					 boolean interBr = false;
					 
					 String destBrFaCode = "";
					 for (Iterator<Branch> it = branchSet.iterator(); it.hasNext(); ) {
						 Branch actBr = it.next();
						 System.out.println("bpFaCode =====>"+brFaCode);
						 System.out.println("actBr.getBranchFaCode() ===== >"+actBr.getBranchFaCode());
					     if(brFaCode.equalsIgnoreCase(actBr.getBranchFaCode())){
					    	 
					     }else{
					    	 interBr = true;
					    	 destBrFaCode = actBr.getBranchFaCode();
					     }
					 }
					 
					 
					 if(interBr == false){
						 
						CashStmt cashStmt = new CashStmt();
						cashStmt.setbCode(currentUser.getUserBranchCode());
						cashStmt.setUserCode(currentUser.getUserCode());
						cashStmt.setCsDescription(voucherService.getDesc());
						cashStmt.setCsDrCr('D');
						cashStmt.setCsAmt(faBProm.getFbpAmt());
						cashStmt.setCsType(voucherService.getVoucherType());
						cashStmt.setCsVouchType("cash");
						cashStmt.setCsPayTo(voucherService.getPayTo());
						cashStmt.setCsTvNo(tvNo);
						cashStmt.setCsFaCode(bpFaCode);
						cashStmt.setCsVouchNo(voucherNo);
						cashStmt.setCsDt(sqlDate);
						cashStmt.setPayMode(payMode);
						 
						Criteria cr2 = session.createCriteria(CashStmtStatus.class);
						cr2.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr2.list();
						int csId = -1;
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt.setCsNo(csStatus);
							csId = (Integer) session.save(cashStmt);
							csStatus.getCashStmtList().add(cashStmt);
							session.update(csStatus);
						}	
						
						
						faBProm.setbCode(currentUser.getUserBranchCode());
						faBProm.setUserCode(currentUser.getUserCode());
						faBProm.setFbpCsId(csId);
						faBProm.setCustomer(actCust);
						int fabId = (Integer) session.save(faBProm);
						actCust.getFbpList().add(faBProm);
						session.update(actCust);
						
						cashStmt.setCsSFId(fabId);
						session.update(cashStmt);
						
						
						if(tdsCode != null && tdsAmt > 0){
							
							CashStmt csWTds = new CashStmt();
							csWTds.setbCode(currentUser.getUserBranchCode());
							csWTds.setUserCode(currentUser.getUserCode());
							csWTds.setCsDescription(voucherService.getDesc());
							csWTds.setCsDrCr('C');
							csWTds.setCsAmt(tdsAmt);
							csWTds.setCsType(voucherService.getVoucherType());
							csWTds.setCsVouchType("cash");
							csWTds.setCsPayTo(voucherService.getPayTo());
							csWTds.setCsTvNo(tvNo);
							csWTds.setCsFaCode(tdsCode);
							csWTds.setCsVouchNo(voucherNo);
							csWTds.setCsDt(sqlDate);
							csWTds.setPayMode(payMode);
							
							Criteria crr1 = session.createCriteria(CashStmtStatus.class);
							crr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = crr1.list();
							
							if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								csWTds.setCsNo(csStatus);
								csStatus.getCashStmtList().add(csWTds);
								System.out.println("tds case successfully saved");
								session.update(csStatus);
								//transaction.commit();
							}	
						}
						
					 }else{
						 
						CashStmt cashStmt1 = new CashStmt();
						cashStmt1.setbCode(currentUser.getUserBranchCode());
						cashStmt1.setUserCode(currentUser.getUserCode());
						cashStmt1.setCsDescription(voucherService.getDesc());
						cashStmt1.setCsDrCr('D');
						cashStmt1.setCsAmt(faBProm.getFbpAmt() - tdsAmt);
						cashStmt1.setCsType(voucherService.getVoucherType());
						cashStmt1.setCsVouchType("cash");
						cashStmt1.setCsTvNo(tvNo);
						cashStmt1.setCsPayTo(voucherService.getPayTo());
						cashStmt1.setCsFaCode(destBrFaCode);
						cashStmt1.setCsVouchNo(voucherNo);
						cashStmt1.setCsDt(sqlDate);
						cashStmt1.setPayMode(payMode);
						
						Criteria cr3 = session.createCriteria(CashStmtStatus.class);
						cr3.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr3.list();
						
						int csId = -1;
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt1.setCsNo(csStatus);
							csId = (Integer) session.save(cashStmt1);
							csStatus.getCashStmtList().add(cashStmt1);
							session.update(csStatus);
						}
							
						 
						faBProm.setbCode(currentUser.getUserBranchCode());
						faBProm.setUserCode(currentUser.getUserCode());
						faBProm.setFbpCsId(csId);
						faBProm.setCustomer(actCust);
						int fabId = (Integer) session.save(faBProm);
						actCust.getFbpList().add(faBProm);
						session.update(actCust);
						 
						cashStmt1.setCsSFId(fabId);
						session.update(cashStmt1);
						
						CashStmtTemp cashStmt2 = new CashStmtTemp();
						cashStmt2.setbCode(currentUser.getUserBranchCode());
						cashStmt2.setUserCode(currentUser.getUserCode());
						cashStmt2.setCsDescription(voucherService.getDesc());
						cashStmt2.setCsDrCr('C');
						cashStmt2.setCsAmt(faBProm.getFbpAmt() - tdsAmt);
						cashStmt2.setCsType(voucherService.getVoucherType());
						cashStmt2.setCsVouchType("contra");
						cashStmt2.setCsPayTo(voucherService.getPayTo());
						cashStmt2.setCsTvNo(tvNo);
						cashStmt2.setCsFaCode(brFaCode);
						cashStmt2.setCsBrhFaCode(destBrFaCode);
						//cashStmt2.setCsVouchNo(voucherNo);
						cashStmt2.setCsDt(sqlDate);
						cashStmt2.setPayMode(payMode);
						
						session.save(cashStmt2);
						/*Criteria cr4 = session.createCriteria(CashStmtStatus.class);
						cr4.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr4.list();
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt2.setCsNo(csStatus);
							csStatus.getCashStmtList().add(cashStmt2);
							session.update(csStatus);
						}*/
						
						
						CashStmtTemp cashStmt3 = new CashStmtTemp();
						cashStmt3.setbCode(currentUser.getUserBranchCode());
						cashStmt3.setUserCode(currentUser.getUserCode());
						cashStmt3.setCsDescription(voucherService.getDesc());
						cashStmt3.setCsDrCr('D');
						cashStmt3.setCsAmt(faBProm.getFbpAmt());
						cashStmt3.setCsType(voucherService.getVoucherType());
						cashStmt3.setCsVouchType("contra");
						cashStmt3.setCsPayTo(voucherService.getPayTo());
						cashStmt3.setCsTvNo(tvNo);
						cashStmt3.setCsFaCode(bpFaCode);
						//cashStmt3.setCsVouchNo(voucherNo);
						cashStmt3.setCsDt(sqlDate);
						cashStmt3.setCsBrhFaCode(destBrFaCode);
						cashStmt3.setPayMode(payMode);
						
						session.save(cashStmt3);
						/*Criteria cr5 = session.createCriteria(CashStmtStatus.class);
						cr5.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr5.list();
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt3.setCsNo(csStatus);
							csStatus.getCashStmtList().add(cashStmt3);
							session.update(csStatus);
						}*/
						
						if(tdsCode != null && tdsAmt > 0){
							
							CashStmtTemp csWTds = new CashStmtTemp();
							csWTds.setbCode(currentUser.getUserBranchCode());
							csWTds.setUserCode(currentUser.getUserCode());
							csWTds.setCsDescription(voucherService.getDesc());
							csWTds.setCsDrCr('C');
							csWTds.setCsAmt(tdsAmt);
							csWTds.setCsType(voucherService.getVoucherType());
							csWTds.setCsVouchType("contra");
							csWTds.setCsPayTo(voucherService.getPayTo());
							csWTds.setCsTvNo(tvNo);
							csWTds.setCsFaCode(tdsCode);
							//csWTds.setCsVouchNo(voucherNo);
							csWTds.setCsDt(sqlDate);
							csWTds.setCsBrhFaCode(destBrFaCode);
							csWTds.setPayMode(payMode);
							
							session.save(csWTds);
							
							/*Criteria crr1 = session.createCriteria(CashStmtStatus.class);
							crr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = crr1.list();
							
							if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								csWTds.setCsNo(csStatus);
								csStatus.getCashStmtList().add(csWTds);
								System.out.println("tds case successfully saved");
								session.update(csStatus);
								//transaction.commit();
							}	*/
						}
					 } 
				 }
				 map.put("vhNo",voucherNo);
				 transaction.commit();
				 session.flush();
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			 session.clear();
			 session.close();
		 }else{
				System.out.println("bpsList is empty");
				map.put("vhNo",0);
		 }
		 
		 return map;
	 }
	
	 
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	public Map<String,Object> saveBPVoucByChq(VoucherService voucherService, BPVoucherService bpVoucherService){
		 System.out.println("enter into saveBPVoucByChq function");
		 Map<String,Object> map = new HashMap<String,Object>();
		 User currentUser = (User) httpSession.getAttribute("currentUser");
		 String brFaCode = voucherService.getBranch().getBranchFaCode();
		 CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
		 //java.sql.Date sqlDate = new java.sql.Date(new Date().getTime());
		 List<BPromService> bpsList = bpVoucherService.getAllBPromSer();
		 List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		 List<Customer> custList = new ArrayList<Customer>();
		 List<FAMaster> faMList = new ArrayList<FAMaster>();
		 char chequeType = voucherService.getChequeType();
		 int voucherNo = 0;
		 String payMode = "Q";
		 if(!bpsList.isEmpty()){
			 try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 
				 java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
				 
				 Criteria crr = session.createCriteria(FAMaster.class);
				 crr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.B_PROM_FANAME));
				 faMList =crr.list();
				 String bpFaCode = faMList.get(0).getFaMfaCode();
				 
				 Criteria cr = session.createCriteria(CashStmt.class);
				 cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
				 cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
				 ProjectionList proList = Projections.projectionList();
				 proList.add(Projections.property("csVouchNo"));
				 cr.setProjection(proList);
					
				 List<Integer>  voucherNoList = cr.list();
				 if(!voucherNoList.isEmpty()){
						int vNo = voucherNoList.get(voucherNoList.size() - 1);
						voucherNo = vNo + 1;
				 }else{
						voucherNo = 1;
				 }
				 
				 String tdsCode = voucherService.getTdsCode();
					double tdsAmt = voucherService.getTdsAmt();
					System.out.println("tdsCode = "+tdsCode+" --- tdsAmt = "+tdsAmt);
					/*if(tdsCode != null && tdsAmt > 0){
						
						CashStmt csWTds = new CashStmt();
						csWTds.setbCode(currentUser.getUserBranchCode());
						csWTds.setUserCode(currentUser.getUserCode());
						csWTds.setCsDescription(voucherService.getDesc());
						csWTds.setCsDrCr('C');
						csWTds.setCsAmt(tdsAmt);
						csWTds.setCsType(voucherService.getVoucherType());
						csWTds.setCsVouchType("bank");
						csWTds.setCsPayTo(voucherService.getPayTo());
						csWTds.setCsTvNo(ConstantsValues.CONST_TVNO);
						csWTds.setCsFaCode(tdsCode);
						csWTds.setCsVouchNo(voucherNo);
						csWTds.setCsDt(sqlDate);
						
						Criteria crr1 = session.createCriteria(CashStmtStatus.class);
						crr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = crr1.list();
						
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							csWTds.setCsNo(csStatus);
							csStatus.getCashStmtList().add(csWTds);
							System.out.println("tds case successfully saved");
							session.update(csStatus);
							//transaction.commit();
						}	
					}*/
				 
					
					
					List<ChequeLeaves> chqList = new ArrayList<ChequeLeaves>();
					ChequeLeaves chequeLeaves = voucherService.getChequeLeaves();
					System.out.println("chequeLeaves id = "+chequeLeaves.getChqLId());
					Criteria crr2 = session.createCriteria(ChequeLeaves.class);
					crr2.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_ID,chequeLeaves.getChqLId()));
					chqList = crr2.list();
					System.out.println("size of chqList = "+chqList.size());
					ChequeLeaves chql = chqList.get(0);
					double totAmtFQ = 0.0;
					
					for(int i=0;i<bpsList.size();i++){
						BPromService bPromService = bpsList.get(i);
						FaBProm faBProm = bPromService.getFaBProm();
						
						totAmtFQ = totAmtFQ + faBProm.getFbpAmt();
					}
					
					chql.setChqLChqAmt(totAmtFQ - tdsAmt);
					chql.setChqLUsedDt(new Date(new java.util.Date().getTime()));
					chql.setChqLUsed(true);
					
					String tvNo = chql.getChqLChqNo();
					
					session.update(chql);
					
					BankMstr bank = chql.getBankMstr();
					double balAmt = bank.getBnkBalanceAmt();
					double newBalAmt = balAmt - chql.getChqLChqAmt();
					bank.setBnkBalanceAmt(newBalAmt);
					session.merge(bank);
					
					
					// InterBranch Case Main Cash Stmt Start
					double amount = 0.0;
					int count = 0;
					for(int i=0;i<bpsList.size();i++){

						 BPromService bPromService = bpsList.get(i);
						 Customer customer = bPromService.getCustomer();
						 FaBProm faBProm = bPromService.getFaBProm();
							
						 Criteria cr1 = session.createCriteria(Customer.class);
						 cr1.add(Restrictions.eq(CustomerCNTS.CUST_ID,customer.getCustId()));
						 custList = cr1.list();
						 
						 Customer actCust = custList.get(0);
						 Set<Branch> branchSet = new HashSet<Branch>();
						 branchSet = actCust.getBranches();
						 
						 boolean interBr = false;
						 
						 for (Iterator<Branch> it = branchSet.iterator(); it.hasNext(); ) {
							 Branch actBr = it.next();
						     if(brFaCode.equalsIgnoreCase(actBr.getBranchFaCode())){
						    	 
						     }else{
						    	 interBr = true;
						     }
						 }
						 amount = amount + faBProm.getFbpAmt();
						/* if(interBr == true){
							 amount = amount + faBProm.getFbpAmt();
							 count = count + 1;
						 }*/
					}
					
					System.out.println(count+" custCode are from other branches--");
					CashStmt mainCashStmt = new CashStmt();
					
					//if(count > 0){
						
						mainCashStmt.setbCode(currentUser.getUserBranchCode());
						mainCashStmt.setUserCode(currentUser.getUserCode());
						mainCashStmt.setCsDescription(voucherService.getDesc());
						mainCashStmt.setCsDrCr('C');
						mainCashStmt.setCsAmt(amount - tdsAmt);
						mainCashStmt.setCsType(voucherService.getVoucherType());
						mainCashStmt.setCsVouchType("bank");
						mainCashStmt.setCsChequeType(chequeType);
						mainCashStmt.setCsPayTo(voucherService.getPayTo());
						mainCashStmt.setCsFaCode(voucherService.getBankCode());
						mainCashStmt.setCsTvNo(tvNo);
						mainCashStmt.setCsDt(sqlDate);
						mainCashStmt.setCsVouchNo(voucherNo);
						mainCashStmt.setPayMode(payMode);
						
						Criteria crr3 = session.createCriteria(CashStmtStatus.class);
						crr3.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = crr3.list();
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							mainCashStmt.setCsNo(csStatus);
							csStatus.getCashStmtList().add(mainCashStmt);
							session.update(csStatus);
							//transaction.commit();
						}
					//}
					
					// InterBranch Case Main Cash Stmt End
					
				 
				 for(int i=0 ; i<bpsList.size() ; i++){
					 BPromService bPromService = bpsList.get(i);
					 Customer customer = bPromService.getCustomer();
					 FaBProm faBProm = bPromService.getFaBProm();
					 
					 
					/*String tvNo = customer.getCustFaCode();
					int count = tvNo.length();
					if(count < 13){
						count = 13 - count;
						for(int j=0;j<count;j++){
							tvNo = "0" + tvNo;
						}
					}*/
					 
					 
					 
					 Criteria cr1 = session.createCriteria(Customer.class);
					 cr1.add(Restrictions.eq(CustomerCNTS.CUST_ID,customer.getCustId()));
					 custList = cr1.list();
					 
					 Customer actCust = custList.get(0);
					 Set<Branch> branchSet = new HashSet<Branch>();
					 branchSet = actCust.getBranches();
					 System.out.println("size of branchSet = "+branchSet.size());
					 boolean interBr = false;
					 String destBrFaCode = "";
					 for (Iterator<Branch> it = branchSet.iterator(); it.hasNext(); ) {
						 Branch actBr = it.next();
						 System.out.println("bpFaCode =====>"+bpFaCode);
						 System.out.println("actBr.getBranchFaCode() ===== >"+actBr.getBranchFaCode());
					     if(brFaCode.equalsIgnoreCase(actBr.getBranchFaCode())){
					    	 
					     }else{
					    	 interBr = true;
					    	 destBrFaCode = actBr.getBranchFaCode();
					     }
					 }
					 
					 
					 if(interBr == false){
						 
						CashStmt cashStmt = new CashStmt();
						cashStmt.setbCode(currentUser.getUserBranchCode());
						cashStmt.setUserCode(currentUser.getUserCode());
						cashStmt.setCsDescription(voucherService.getDesc());
						cashStmt.setCsDrCr('D');
						cashStmt.setCsAmt(faBProm.getFbpAmt());
						cashStmt.setCsType(voucherService.getVoucherType());
						cashStmt.setCsVouchType("bank");
						cashStmt.setCsPayTo(voucherService.getPayTo());
						cashStmt.setCsChequeType(chequeType);
						cashStmt.setCsTvNo(tvNo);
						cashStmt.setCsFaCode(bpFaCode);
						cashStmt.setCsVouchNo(voucherNo);
						cashStmt.setCsDt(sqlDate);
						cashStmt.setPayMode(payMode);
						 
						Criteria cr2 = session.createCriteria(CashStmtStatus.class);
						cr2.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr2.list();
						int csId = -1;
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt.setCsNo(csStatus);
							csId = (Integer) session.save(cashStmt);
							csStatus.getCashStmtList().add(cashStmt);
							session.update(csStatus);
						}	
						
						
						faBProm.setbCode(currentUser.getUserBranchCode());
						faBProm.setUserCode(currentUser.getUserCode());
						faBProm.setFbpCsId(csId);
						faBProm.setCustomer(actCust);
						int fabId = (Integer) session.save(faBProm);
						actCust.getFbpList().add(faBProm);
						session.update(actCust);
						
						cashStmt.setCsSFId(fabId);
						session.update(cashStmt);
						
						/*CashStmt cStmt = new CashStmt();
						cStmt.setbCode(currentUser.getUserBranchCode());
						cStmt.setUserCode(currentUser.getUserCode());
						cStmt.setCsDescription(voucherService.getDesc());
						cStmt.setCsDrCr('C');
						cStmt.setCsAmt(faBProm.getFbpAmt());
						cStmt.setCsType(voucherService.getVoucherType());
						cStmt.setCsVouchType("bank");
						cStmt.setCsChequeType(chequeType);
						cStmt.setCsPayTo(voucherService.getPayTo());
						cStmt.setCsVouchNo(voucherNo);
						cStmt.setCsTvNo(tvNo);
						cStmt.setCsFaCode(voucherService.getBankCode());
						cStmt.setCsDt(sqlDate);
						
						 
						Criteria cr3 = session.createCriteria(CashStmtStatus.class);
						cr3.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr3.list();
						
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cStmt.setCsNo(csStatus);
							csId = (Integer) session.save(cStmt);
							csStatus.getCashStmtList().add(cStmt);
							session.update(csStatus);
						}	*/
						
						
						if(tdsCode != null && tdsAmt > 0){
							
							CashStmt csWTds = new CashStmt();
							csWTds.setbCode(currentUser.getUserBranchCode());
							csWTds.setUserCode(currentUser.getUserCode());
							csWTds.setCsDescription(voucherService.getDesc());
							csWTds.setCsDrCr('C');
							csWTds.setCsAmt(tdsAmt);
							csWTds.setCsType(voucherService.getVoucherType());
							csWTds.setCsVouchType("bank");
							csWTds.setCsPayTo(voucherService.getPayTo());
							csWTds.setCsTvNo(tvNo);
							csWTds.setCsFaCode(tdsCode);
							csWTds.setCsVouchNo(voucherNo);
							csWTds.setCsDt(sqlDate);
							csWTds.setPayMode(payMode);
							
							Criteria crr1 = session.createCriteria(CashStmtStatus.class);
							crr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = crr1.list();
							
							if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								csWTds.setCsNo(csStatus);
								csStatus.getCashStmtList().add(csWTds);
								System.out.println("tds case successfully saved");
								session.update(csStatus);
								//transaction.commit();
							}	
						}
						
					 }else{
						 
						CashStmt cashStmt1 = new CashStmt();
						cashStmt1.setbCode(currentUser.getUserBranchCode());
						cashStmt1.setUserCode(currentUser.getUserCode());
						cashStmt1.setCsDescription(voucherService.getDesc());
						cashStmt1.setCsDrCr('D');
						cashStmt1.setCsAmt(faBProm.getFbpAmt() - tdsAmt);
						cashStmt1.setCsType(voucherService.getVoucherType());
						cashStmt1.setCsVouchType("bank");
						cashStmt1.setCsTvNo(tvNo);
						cashStmt1.setCsPayTo(voucherService.getPayTo());
						cashStmt1.setCsFaCode(destBrFaCode);
						cashStmt1.setCsVouchNo(voucherNo);
						cashStmt1.setCsDt(sqlDate);
						cashStmt1.setPayMode(payMode);
						
						Criteria cr3 = session.createCriteria(CashStmtStatus.class);
						cr3.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr3.list();
						
						int csId = -1;
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt1.setCsNo(csStatus);
							csId = (Integer) session.save(cashStmt1);
							csStatus.getCashStmtList().add(cashStmt1);
							session.update(csStatus);
						}
							
						 
						faBProm.setbCode(currentUser.getUserBranchCode());
						faBProm.setUserCode(currentUser.getUserCode());
						faBProm.setFbpCsId(csId);
						faBProm.setCustomer(actCust);
						int fabId = (Integer) session.save(faBProm);
						actCust.getFbpList().add(faBProm);
						session.update(actCust);
						 
						cashStmt1.setCsSFId(fabId);
						session.update(cashStmt1);
						
						CashStmtTemp cashStmt2 = new CashStmtTemp();
						cashStmt2.setbCode(currentUser.getUserBranchCode());
						cashStmt2.setUserCode(currentUser.getUserCode());
						cashStmt2.setCsDescription(voucherService.getDesc());
						cashStmt2.setCsDrCr('C');
						cashStmt2.setCsAmt(faBProm.getFbpAmt() - tdsAmt);
						cashStmt2.setCsType(voucherService.getVoucherType());
						cashStmt2.setCsVouchType("contra");
						cashStmt2.setCsPayTo(voucherService.getPayTo());
						cashStmt2.setCsTvNo(tvNo);
						cashStmt2.setCsFaCode(brFaCode);
						cashStmt2.setCsBrhFaCode(destBrFaCode);
						//cashStmt2.setCsVouchNo(voucherNo);
						cashStmt2.setCsDt(sqlDate);
						cashStmt2.setPayMode(payMode);
						
						session.save(cashStmt2);
						/*Criteria cr4 = session.createCriteria(CashStmtStatus.class);
						cr4.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr4.list();
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt2.setCsNo(csStatus);
							csStatus.getCashStmtList().add(cashStmt2);
							session.update(csStatus);
						}*/
						
						
						CashStmtTemp cashStmt3 = new CashStmtTemp();
						cashStmt3.setbCode(currentUser.getUserBranchCode());
						cashStmt3.setUserCode(currentUser.getUserCode());
						cashStmt3.setCsDescription(voucherService.getDesc());
						cashStmt3.setCsDrCr('D');
						cashStmt3.setCsAmt(faBProm.getFbpAmt());
						cashStmt3.setCsType(voucherService.getVoucherType());
						cashStmt3.setCsVouchType("contra");
						cashStmt3.setCsPayTo(voucherService.getPayTo());
						cashStmt3.setCsTvNo(tvNo);
						cashStmt3.setCsFaCode(bpFaCode);
						cashStmt3.setCsBrhFaCode(destBrFaCode);
						//cashStmt3.setCsVouchNo(voucherNo);
						cashStmt3.setCsDt(sqlDate);
						cashStmt3.setPayMode(payMode);
						
						session.save(cashStmt3);
						/*Criteria cr5 = session.createCriteria(CashStmtStatus.class);
						cr5.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr5.list();
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt3.setCsNo(csStatus);
							csStatus.getCashStmtList().add(cashStmt3);
							session.update(csStatus);
						}*/
						
						if(tdsCode != null && tdsAmt > 0){
							
							CashStmtTemp csWTds = new CashStmtTemp();
							csWTds.setbCode(currentUser.getUserBranchCode());
							csWTds.setUserCode(currentUser.getUserCode());
							csWTds.setCsDescription(voucherService.getDesc());
							csWTds.setCsDrCr('C');
							csWTds.setCsAmt(tdsAmt);
							csWTds.setCsType(voucherService.getVoucherType());
							csWTds.setCsVouchType("contra");
							csWTds.setCsPayTo(voucherService.getPayTo());
							csWTds.setCsTvNo(tvNo);
							csWTds.setCsFaCode(tdsCode);
							csWTds.setCsBrhFaCode(destBrFaCode);
							//csWTds.setCsVouchNo(voucherNo);
							csWTds.setCsDt(sqlDate);
							csWTds.setPayMode(payMode);
							
							session.save(csWTds);
							/*Criteria crr1 = session.createCriteria(CashStmtStatus.class);
							crr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = crr1.list();
							
							if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								csWTds.setCsNo(csStatus);
								csStatus.getCashStmtList().add(csWTds);
								System.out.println("tds case successfully saved");
								session.update(csStatus);
								//transaction.commit();
							}*/	
						}
					 } 
				 }
				 map.put("vhNo",voucherNo);
				 transaction.commit();
				 session.flush();
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			 session.clear();
			 session.close();
		 }else{
				System.out.println("bpsList is empty");
				map.put("vhNo",0);
		 }
		 
		 return map;
	 }
	
	
 	@SuppressWarnings("unchecked")
	@Transactional
	 public List<FaBProm> getSubFileDetails(List<CashStmt> reqCsList){
		System.out.println("enter into getSubFileDetails function");
		List<FaBProm> fbpList = new ArrayList<FaBProm>();
		if(!reqCsList.isEmpty()){
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				for(int i=0;i<reqCsList.size();i++){
					List<FaBProm> tempList = new ArrayList<FaBProm>();
					Criteria cr = session.createCriteria(FaBProm.class);
					cr.add(Restrictions.eq(FaBPromCNTS.FBP_ID , reqCsList.get(i).getCsSFId()));
					tempList = cr.list(); 
					if(!tempList.isEmpty()){
						fbpList.add(tempList.get(0));
					}
				} 
				transaction.commit();
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
		}else{
			System.out.println("reqCsList is empty");
			
		}
		return fbpList;
	}
}

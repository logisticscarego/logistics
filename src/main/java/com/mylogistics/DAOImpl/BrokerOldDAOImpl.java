package com.mylogistics.DAOImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.BrokerOldDAO;
import com.mylogistics.model.BrokerOld;

public class BrokerOldDAOImpl implements BrokerOldDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public BrokerOldDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	public int saveBrOld(BrokerOld brokerOld){
		System.out.println("enter into saveBrOld function of IMPL");
		int temp;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.save(brokerOld);
			transaction.commit();
			temp= 1;
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}
		session.clear();
		session.close();
		return temp;
	}
	
	@Override
	public int saveBrOld(BrokerOld brokerOld,Session session){
		System.out.println("enter into saveBrOld function of IMPL");
		int temp=0;
	
			temp=(int) session.save(brokerOld);
			
		return temp;
	}
	
}

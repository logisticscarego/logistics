package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.CustomerRightsDAO;
import com.mylogistics.constants.CustomerRightsCNTS;
import com.mylogistics.model.CustomerRights;

public class CustomerRightsDAOImpl implements CustomerRightsDAO{
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public CustomerRightsDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	   public int saveCustRights(CustomerRights customerRgt){
		 System.out.println("enter into saveCustRights function");
		 int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 session.save(customerRgt);
			 transaction.commit();
			 temp = 1;
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<CustomerRights> getCustRightsIsAllow(){
		 List<CustomerRights> list = new ArrayList<CustomerRights>();
		 try{
			   session = this.sessionFactory.openSession();
			   transaction =  session.beginTransaction();
			   Criteria cr=session.createCriteria(CustomerRights.class);
			   cr.add(Restrictions.eq(CustomerRightsCNTS.CRH_ADV_ALLOW,"yes"));
			   list =cr.list();
			   session.flush();
		 }catch(Exception e){
				e.printStackTrace();
		 }
		 session.clear();
         session.close();
		 return list;  
	 }
	 
	@SuppressWarnings("unchecked")
	@Transactional
	public int updateCustRightsIsAllowNo(int contIdsInt[]) {
		int temp;
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		try {
			session = this.sessionFactory.openSession();
			for (int i = 0; i < contIdsInt.length; i++) {
				transaction = session.beginTransaction();
				CustomerRights custRights = new CustomerRights();
				List<CustomerRights> list = new ArrayList<CustomerRights>();
				Criteria cr = session.createCriteria(CustomerRights.class);
				cr.add(Restrictions
						.eq(CustomerRightsCNTS.CRH_ID, contIdsInt[i]));
				list = cr.list();
				custRights = list.get(0);
				custRights.setCrhAdvAllow("no");
				custRights.setCreationTS(calendar);
				session.update(custRights);
				transaction.commit();
			}
			session.flush();
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}
}

package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.MultipleStationDAO;
import com.mylogistics.constants.MultipleStationCNTS;
import com.mylogistics.model.MultipleStation;

public class MultipleStationDAOImpl implements MultipleStationDAO{
	

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	public static Logger logger = Logger.getLogger(MultipleStationDAOImpl.class);
	
	@Autowired
	public MultipleStationDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	   public int saveMultipleStation(MultipleStation multipleStation){
		 logger.info("Enter into saveMultipleStation() : MultipleStation = "+multipleStation);
		 int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 session.save(multipleStation);
			 transaction.commit();
			 System.out.println("multipleStation Data is inserted successfully");
			 temp = 1;
			 logger.info("MultipleStation is saved !");
		 } catch(Exception e){
			 logger.error("Exception : "+e);
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 logger.info("Exit from saveMultipleStation()");
		 return temp;
	 }
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<MultipleStation> getMultipleStaion(String code){
		 List<MultipleStation> multipleStations = new ArrayList<MultipleStation>();
		 System.out.println("Enter into MultipleStation function--- "+code);
		 try{
			 session = this.sessionFactory.openSession();
	         transaction = session.beginTransaction();
	         Criteria cr = session.createCriteria(MultipleStation.class);   
	         cr.add(Restrictions.eq(MultipleStationCNTS.STATION_REF_CODE,code));
	         multipleStations =cr.list();
	         transaction.commit();
	         session.flush();
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return multipleStations;
	}
	
	 @Transactional
	 public int deleteStation(MultipleStation multipleStation){
			System.out.println("code in dao is-----------------------"+multipleStation.getMulStnRefCode());
			int temp ;
		    try{
		    	session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria cr=session.createCriteria(MultipleStation.class);
				 cr.add(Restrictions.eq(MultipleStationCNTS.STATION_REF_CODE,multipleStation.getMulStnRefCode()));
				 
				 multipleStation = (MultipleStation) cr.list().get(0);
				 session.delete(multipleStation);
				 transaction.commit();
				 session.flush();
				
				 System.out.println("station Data is deleted successfully");
				 temp=1;
		    }catch (Exception e){
				e.printStackTrace();
				temp=-1;
			}
		    session.clear();
		    session.close();
		    return temp;
	   }

}

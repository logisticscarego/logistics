package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.MoneyReceiptDAO;
import com.mylogistics.constants.BankCSCNTS;
import com.mylogistics.constants.BillCNTS;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.constants.FAMasterCNTS;
import com.mylogistics.constants.MoneyReceiptCNTS;
import com.mylogistics.model.BankCS;
import com.mylogistics.model.Bill;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.Customer;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.MoneyReceipt;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.NumToWords;

public class MoneyReceiptDAOImpl implements MoneyReceiptDAO{

	private SessionFactory sessionFactory;
	public static Logger logger = Logger.getLogger(MoneyReceiptDAOImpl.class);
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	public MoneyReceiptDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")   
	public String saveOAMR(MoneyReceipt moneyReceipt){
		 System.out.println("enter into saveOAMR function");
	 	 String mrNo = "";
		 User currentUser = (User) httpSession.getAttribute("currentUser");
		Session session=null;
		Transaction transaction=null;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			List<MoneyReceipt> mrList = new ArrayList<>();
			Query query = session.createQuery("from MoneyReceipt where mrBrhId = :type order by mrId DESC");
			query.setParameter("type", Integer.parseInt(currentUser.getUserBranchCode()));
			mrList = query.setMaxResults(1).list();
		
			if(!mrList.isEmpty()){
				String prevMrNo = mrList.get(0).getMrNo();
				mrNo  = "MR" + String.valueOf(Integer.parseInt(prevMrNo.substring(2)) + 1 + 1000000).substring(1);
			}else{
				mrNo = "MR000001";
			}
			
			moneyReceipt.setUserCode(currentUser.getUserCode());
			moneyReceipt.setbCode(currentUser.getUserBranchCode());
			moneyReceipt.setMrType(ConstantsValues.OA_MR);
			moneyReceipt.setMrBrhId(Integer.parseInt(currentUser.getUserBranchCode()));
			moneyReceipt.setMrIsPending(true);
			moneyReceipt.setMrNo(mrNo);
			moneyReceipt.setMrRemAmt(moneyReceipt.getMrNetAmt());
			session.save(moneyReceipt);
			
			BankMstr bankMstr = (BankMstr) session.get(BankMstr.class,moneyReceipt.getMrBnkId());
			
			int voucherNo = 0;
			 
			Customer customer = (Customer) session.get(Customer.class,moneyReceipt.getMrCustId());
			
			List<CashStmtStatus> cssList = new ArrayList<>();
			Criteria cr = session.createCriteria(CashStmtStatus.class);
			cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,currentUser.getUserBranchCode()));
			cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT,moneyReceipt.getMrDate()));
			cssList = cr.list();
			
			CashStmtStatus css = cssList.get(0);
			List<CashStmt> csVouchList = new ArrayList<CashStmt>();
			csVouchList = css.getCashStmtList();
			
			if(!csVouchList.isEmpty()){
				for(int i=0;i<csVouchList.size();i++){
					if(voucherNo < csVouchList.get(i).getCsVouchNo()){
						voucherNo = csVouchList.get(i).getCsVouchNo();
					}
				}
				voucherNo = voucherNo + 1;
			}else{
				voucherNo = 1;
			}
			 
			
			CashStmt cs1 = new CashStmt();
			cs1.setUserCode(currentUser.getUserCode());
			cs1.setbCode(currentUser.getUserBranchCode());
			cs1.setCsDescription(moneyReceipt.getMrDesc());
			cs1.setCsDrCr('C');
			cs1.setCsAmt(moneyReceipt.getMrNetAmt());
			cs1.setCsType(ConstantsValues.MNY_RCT);
			
			if(moneyReceipt.getMrPayBy() == 'C'){
				cs1.setCsTvNo(mrNo);
				cs1.setPayMode("C");
				cs1.setCsVouchType("CASH");
			}else if(moneyReceipt.getMrPayBy() == 'Q'){
				String mrChqNo = moneyReceipt.getMrChqNo();
				cs1.setCsTvNo(mrNo+" ("+mrChqNo+")");
				cs1.setPayMode("Q");
				cs1.setCsVouchType("BANK");
			}else if(moneyReceipt.getMrPayBy() == 'R'){				
				cs1.setCsTvNo(mrNo +" ("+moneyReceipt.getMrRtgsRefNo()+")");
				cs1.setPayMode("R");
				cs1.setCsVouchType("BANK");
			}
			
			cs1.setCsFaCode(customer.getCustFaCode());
			cs1.setCsVouchNo(voucherNo);
			cs1.setCsDt(css.getCssDt());
			 
			cs1.setCsNo(css);
			css.getCashStmtList().add(cs1);
			session.merge(css);
			
		if(moneyReceipt.getMrPayBy()!='C'){
			CashStmt cs2 = new CashStmt();
			cs2.setUserCode(currentUser.getUserCode());
			cs2.setbCode(currentUser.getUserBranchCode());
			cs2.setCsDescription(moneyReceipt.getMrDesc());
			cs2.setCsDrCr('D');
			cs2.setCsAmt(moneyReceipt.getMrNetAmt());
			cs2.setCsType(ConstantsValues.MNY_RCT);
			 if(moneyReceipt.getMrPayBy() == 'Q'){
				String mrChqNo = moneyReceipt.getMrChqNo();
				cs2.setCsTvNo(mrNo+" ("+mrChqNo+")");
				cs2.setPayMode("Q");
			}else if(moneyReceipt.getMrPayBy() == 'R'){
				cs2.setCsTvNo(mrNo +" ("+moneyReceipt.getMrRtgsRefNo()+")");
				cs2.setPayMode("R");
			}
			
			cs2.setCsVouchType("BANK");
			cs2.setCsFaCode(bankMstr.getBnkFaCode());
			
			cs2.setCsVouchNo(voucherNo);
			cs2.setCsDt(css.getCssDt());
			 
			CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
			cs2.setCsNo(actCss);
			actCss.getCashStmtList().add(cs2);
			session.merge(actCss);
			
			double newBalAmt = bankMstr.getBnkBalanceAmt() + moneyReceipt.getMrNetAmt();
			bankMstr.setBnkBalanceAmt(newBalAmt);
			session.update(bankMstr);
		}
			if(mrNo != null && mrNo != ""){
				transaction.commit();
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			transaction.rollback();
			logger.info("Exception in saveOAMR "+e);
			mrNo = null;
		}finally{
			session.clear();
			session.close();
		}
		
		return mrNo;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<MoneyReceipt> getPendingMR(int custId){
		System.out.println("enter into getPendingMR function");
		List<MoneyReceipt> mrList = new ArrayList<>();		
		User currentUser  = (User) httpSession.getAttribute("currentUser");
		
		System.out.println(MoneyReceiptCNTS.MR_CUST_ID+": "+custId);
		System.out.println(MoneyReceiptCNTS.MR_TYPE+": "+ConstantsValues.OA_MR);
		System.out.println(MoneyReceiptCNTS.MR_BRH_ID+": "+currentUser.getUserBranchCode());
		Session session=null;
		try{
			session = this.sessionFactory.openSession();
			
			Criteria cr = session.createCriteria(MoneyReceipt.class);
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID,custId));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE,ConstantsValues.OA_MR));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID,Integer.parseInt(currentUser.getUserBranchCode())));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_IS_PENDING,true));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_IS_CANCEL,false));
			mrList = cr.list();
			
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception in getPendingMR "+e);
		}finally{
			session.clear();
			session.close();
		}
		return mrList;			
	}
	
	
	@SuppressWarnings("unchecked")
	public String savePDMR(List<MoneyReceipt> mrList){
		System.out.println("enter into savePDMR function");
		String mrNo = "";
		String chqNo = "";
		String payMode = "";
		User currentUser = (User) httpSession.getAttribute("currentUser");
		
		Date date = new Date(new java.util.Date().getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		System.out.println("unique id =====>" + calendar.getTimeInMillis());
		Session session=null;
		Transaction transaction=null;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			String faCode = "";
			String tdsCode = "";
			String srvTaxCode = "";
			String claimCode = "";
			
			List<FAMaster> famList = session.createCriteria(FAMaster.class)
					  .add(Restrictions.disjunction()
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_COLLECTION))
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_TDS))
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_SRV_TAX))
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.CLAIM))
				  ).list(); 
			 
			
			if(!famList.isEmpty()){
				for(int i=0;i<famList.size();i++){
					if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_COLLECTION)){
						faCode = famList.get(i).getFaMfaCode();
					}else if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_TDS)){
						tdsCode = famList.get(i).getFaMfaCode();
					}else if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_SRV_TAX)){
						srvTaxCode = famList.get(i).getFaMfaCode();
					}else if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.CLAIM)){
						claimCode = famList.get(i).getFaMfaCode();
					}else{
						System.out.println("invalid result");
					}
				}
			}
			
			 
			Query query = session.createQuery("from MoneyReceipt where mrBrhId = :type order by mrId DESC");
			query.setParameter("type", Integer.parseInt(currentUser.getUserBranchCode()));
			List<MoneyReceipt> dbMrList = query.setMaxResults(1).list();
		
			if(!dbMrList.isEmpty()){
				String prevMrNo = dbMrList.get(0).getMrNo();
				mrNo  = "MR" + String.valueOf(Integer.parseInt(prevMrNo.substring(2)) + 1 + 1000000).substring(1);
			}
			
			if(!mrList.isEmpty()){
				
				double recAmt = 0.0;
				double freight = 0.0;
				double dedAmt = 0.0;
				double excAmt = 0.0;
				double srvTaxAmt = 0.0;
				double tdsAmt = 0.0;
				double claimAmt = 0.0;
				
				
				for(int i=0;i<mrList.size();i++){			
					MoneyReceipt moneyReceipt = mrList.get(i);				
					
					moneyReceipt.setUserCode(currentUser.getUserCode());
					moneyReceipt.setbCode(currentUser.getUserBranchCode());
					moneyReceipt.setMrBrhId(Integer.parseInt(currentUser.getUserBranchCode()));
					moneyReceipt.setMrNo(mrNo);
					moneyReceipt.setMrType(ConstantsValues.PD_MR);					
					
					recAmt = recAmt + moneyReceipt.getMrNetAmt();
					freight = freight + moneyReceipt.getMrFreight();
					dedAmt = dedAmt + moneyReceipt.getMrDedAmt();
					excAmt = excAmt + moneyReceipt.getMrAccessAmt();
					srvTaxAmt = srvTaxAmt + moneyReceipt.getMrSrvTaxAmt();
					tdsAmt = tdsAmt + moneyReceipt.getMrTdsAmt();
					
					if(moneyReceipt.getMrDedAmt() > 0){
						List<Map<String,Object>> dedList = moneyReceipt.getMrDedDetList(); 
						if(!dedList.isEmpty()){
							double claim = 0.0;
							for(int j=0;j<dedList.size();j++){
								Map<String,Object> map = dedList.get(j); 
								if(String.valueOf(map.get("dedType")).equalsIgnoreCase("CLAIM SHORTAGE")){
									if(Double.parseDouble(String.valueOf(map.get("dedAmt"))) > 0){
										claim = claim + Double.parseDouble(String.valueOf(map.get("dedAmt")));
									}
								}else if(String.valueOf(map.get("dedType")).equalsIgnoreCase("CLAIM DAMAGE")){
									if(Double.parseDouble(String.valueOf(map.get("dedAmt"))) > 0){
										claim = claim + Double.parseDouble(String.valueOf(map.get("dedAmt")));
									}
								}
							}
							claimAmt = claimAmt + claim;
						}
					}	
					
					Bill bill = (Bill) session.get(Bill.class,moneyReceipt.getBill().getBlId());
					
					double remAmt = bill.getBlRemAmt();
					if(remAmt > moneyReceipt.getMrFreight()){
						remAmt = remAmt - moneyReceipt.getMrFreight();
						bill.setBlRemAmt(Double.parseDouble(String.format("%.2f",remAmt)));  //mk
					}else{
						bill.setBlRemAmt(0.0);
					}
					
					session.merge(bill);
				
					moneyReceipt.setBill(bill);
					session.save(moneyReceipt);
				} 
				
				List<Map<String,Object>> mrFrPDList = mrList.get(0).getMrFrPDList();
				System.out.println("size of mrFrPDList = "+mrFrPDList.size());
					for(int k=0;k<mrFrPDList.size();k++){
						System.out.println("%%%%%%%%%%%%%%%%%%%% = "+String.valueOf(mrFrPDList.get(k).get("brhId")));
						Criteria cr = session.createCriteria(MoneyReceipt.class);
						cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_NO,mrFrPDList.get(k).get("onAccMr")));
						cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID,Integer.parseInt(String.valueOf(mrFrPDList.get(k).get("brhId")))));
						cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_IS_CANCEL,false));
						List<MoneyReceipt> oaMrList = cr.list();
						
							MoneyReceipt onMr = oaMrList.get(0);
							
							double remAmt = onMr.getMrRemAmt();
							double newAmt = remAmt - Double.parseDouble(String.valueOf(mrFrPDList.get(k).get("detAmt")));
							Map<String,Object> map = new HashMap<String, Object>();
							map.put("pdMr",mrNo);
							map.put("brhId",Integer.parseInt(String.valueOf(mrFrPDList.get(k).get("brhId"))));
							map.put("detAmt",mrFrPDList.get(k).get("detAmt"));
							onMr.getMrFrPDList().add(map);
							if(newAmt > 0){
								onMr.setMrRemAmt(newAmt);
							}else{
								onMr.setMrRemAmt(0);
								onMr.setMrIsPending(false);
							}							
							if(onMr.getMrPayBy() == 'C'){
								if(!payMode.contains("C"))
									payMode = payMode+"C";
							}else if(onMr.getMrPayBy() == 'Q'){
								if(!payMode.contains("Q"))
									payMode = payMode+"Q";
								if(chqNo.length() > 0)
									chqNo = chqNo+" / "+onMr.getMrChqNo();
								else
									chqNo = onMr.getMrChqNo();
							}else if(onMr.getMrPayBy() == 'R'){
								if(!payMode.contains("R"))
									payMode = payMode+"R";
							}
							session.merge(onMr);
					}
			
				Criteria cr = session.createCriteria(CashStmtStatus.class);
				cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,currentUser.getUserBranchCode()));
				cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT,mrList.get(0).getMrDate()));
				List<CashStmtStatus> cssList = cr.list();
				
				CashStmtStatus css = cssList.get(0);
				Customer customer = (Customer) session.get(Customer.class,mrList.get(0).getMrCustId());
				
					List<CashStmt> csList = css.getCashStmtList();
					
					int voucherNo = 0;
					if(!csList.isEmpty()){
						for(int i=0;i<csList.size();i++){
							if(voucherNo < csList.get(i).getCsVouchNo()){
								voucherNo = csList.get(i).getCsVouchNo();
							}
						}
						voucherNo = voucherNo + 1;
					}else{
						voucherNo = 1;
					}				
					
					if(recAmt > 0){
						CashStmt cs1 = new CashStmt();
						cs1.setUserCode(currentUser.getUserCode());
						cs1.setbCode(currentUser.getUserBranchCode());
						cs1.setCsDescription("(On A/C removal) "+mrList.get(0).getMrDesc());
						cs1.setCsDrCr('D');
						cs1.setCsAmt(recAmt);
						cs1.setCsType(ConstantsValues.MNY_RCT);	
						if(chqNo.length() > 0)
							cs1.setCsTvNo(mrNo+"("+chqNo+")");
						else
							cs1.setCsTvNo(mrNo);
						cs1.setPayMode(payMode);
						cs1.setCsVouchType("CONTRA");
						cs1.setCsFaCode(faCode);
						cs1.setCsVouchNo(voucherNo);
						cs1.setCsDt(css.getCssDt());		
						 
						cs1.setCsNo(css);
						css.getCashStmtList().add(cs1);
						session.merge(css);
						
						CashStmt cs = new CashStmt();
						cs.setUserCode(currentUser.getUserCode());
						cs.setbCode(currentUser.getUserBranchCode());
						cs.setCsDescription("(On A/C removal) "+mrList.get(0).getMrDesc());
						cs.setCsDrCr('C');
						cs.setCsAmt(recAmt);
						cs.setCsType(ConstantsValues.MNY_RCT);
						if(chqNo.length() > 0)
							cs.setCsTvNo(mrNo+"("+chqNo+")");
						else
							cs.setCsTvNo(mrNo);
						cs.setPayMode(payMode);
						cs.setCsVouchType("CONTRA");
						cs.setCsFaCode(faCode);
						cs.setCsVouchNo(voucherNo);
						cs.setCsDt(css.getCssDt());
						
						CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						cs.setCsNo(actCss);
						actCss.getCashStmtList().add(cs);
						session.merge(actCss);
					}
					
					if(claimAmt > 0){
						 CashStmt cs2 = new CashStmt();
						 cs2.setUserCode(currentUser.getUserCode());
						 cs2.setbCode(currentUser.getUserBranchCode());
						 cs2.setCsDescription("(Claim Deduction) "+mrList.get(0).getMrDesc());
						 cs2.setCsDrCr('D');
						 cs2.setCsAmt(claimAmt);
						 cs2.setCsType(ConstantsValues.MNY_RCT);
						 if(chqNo.length() > 0)
							cs2.setCsTvNo(mrNo+"("+chqNo+")");
						 else
							cs2.setCsTvNo(mrNo);
						 cs2.setPayMode(payMode);
						 cs2.setCsVouchType("CONTRA");
						 cs2.setCsFaCode(claimCode);
						 cs2.setCsVouchNo(voucherNo);
						 cs2.setCsDt(css.getCssDt());
						 
						 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						 cs2.setCsNo(actCss);
						 actCss.getCashStmtList().add(cs2);
						 session.merge(actCss);
					}
					
					dedAmt = dedAmt - claimAmt;
					
					if(dedAmt != 0){
						 CashStmt cs3 = new CashStmt();
						 cs3.setUserCode(currentUser.getUserCode());
						 cs3.setbCode(currentUser.getUserBranchCode());
						 cs3.setCsDescription("(Freight Deduction) "+mrList.get(0).getMrDesc());
						 cs3.setCsDrCr('D');
						 cs3.setCsAmt(dedAmt);
						 cs3.setCsType(ConstantsValues.MNY_RCT);
						 if(chqNo.length() > 0)
							cs3.setCsTvNo(mrNo+"("+chqNo+")");
						 else
							cs3.setCsTvNo(mrNo);
						 cs3.setPayMode(payMode);
						 cs3.setCsVouchType("CONTRA");
						 cs3.setCsFaCode(faCode);
						 cs3.setCsVouchNo(voucherNo);
						 cs3.setCsDt(css.getCssDt());
						 
						 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						 cs3.setCsNo(actCss);
						 actCss.getCashStmtList().add(cs3);
						 session.merge(actCss);
						 
					}
					
					
					if(srvTaxAmt > 0){
						 CashStmt cs5 = new CashStmt();
						 cs5.setUserCode(currentUser.getUserCode());
						 cs5.setbCode(currentUser.getUserBranchCode());
						 cs5.setCsDescription("(Service Tax Pay By Customer) "+mrList.get(0).getMrDesc());
						 cs5.setCsDrCr('C');
						 cs5.setCsAmt(srvTaxAmt);
						 cs5.setCsType(ConstantsValues.MNY_RCT);
						 if(chqNo.length() > 0)
							cs5.setCsTvNo(mrNo+"("+chqNo+")");
						 else
							cs5.setCsTvNo(mrNo);
						 cs5.setPayMode(payMode);
						 cs5.setCsVouchType("CONTRA");
						 cs5.setCsFaCode(srvTaxCode);
						 cs5.setCsVouchNo(voucherNo);
						 cs5.setCsDt(css.getCssDt());
						 
						 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						 cs5.setCsNo(actCss);
						 actCss.getCashStmtList().add(cs5);
						 session.merge(actCss);
					}
					
					
					if(tdsAmt > 0){
						 CashStmt cs6 = new CashStmt();
						 cs6.setUserCode(currentUser.getUserCode());
						 cs6.setbCode(currentUser.getUserBranchCode());
						 cs6.setCsDescription("(TDS Deduction By Customer) "+mrList.get(0).getMrDesc());
						 cs6.setCsDrCr('D');
						 cs6.setCsAmt(tdsAmt);
						 cs6.setCsType(ConstantsValues.MNY_RCT);
						 if(chqNo.length() > 0)
							cs6.setCsTvNo(mrNo+"("+chqNo+")");
						 else
							cs6.setCsTvNo(mrNo);
						 cs6.setPayMode(payMode);
						 cs6.setCsVouchType("CONTRA");
						 cs6.setCsFaCode(tdsCode);
						 cs6.setCsVouchNo(voucherNo);
						 cs6.setCsDt(css.getCssDt());
						 
						 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						 cs6.setCsNo(actCss);
						 actCss.getCashStmtList().add(cs6);
						 session.merge(actCss);
						 
						 
					}
					
					
					if(excAmt > 0){
						
						 CashStmt cs8 = new CashStmt();
						 cs8.setUserCode(currentUser.getUserCode());
						 cs8.setbCode(currentUser.getUserBranchCode());
						 cs8.setCsDescription("(Excess Amount) "+mrList.get(0).getMrDesc());
						 cs8.setCsDrCr('C');
						 cs8.setCsAmt(excAmt);
						 cs8.setCsType(ConstantsValues.MNY_RCT);
						 if(chqNo.length() > 0)
							cs8.setCsTvNo(mrNo+"("+chqNo+")");
						 else
							cs8.setCsTvNo(mrNo);
						 cs8.setPayMode(payMode);
						 cs8.setCsVouchType("CONTRA");
						 cs8.setCsFaCode(faCode);
						 cs8.setCsVouchNo(voucherNo);
						 cs8.setCsDt(css.getCssDt());
						 
						 CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						 cs8.setCsNo(actCss1);
						 actCss1.getCashStmtList().add(cs8);
						 session.merge(actCss1);
						 
						 
						 CashStmt cs9 = new CashStmt();
						 cs9.setUserCode(currentUser.getUserCode());
						 cs9.setbCode(currentUser.getUserBranchCode());
						 cs9.setCsDescription("(Final Amount From Customer) "+mrList.get(0).getMrDesc());
						 cs9.setCsDrCr('D');
						 cs9.setCsAmt(excAmt);
						 cs9.setCsType(ConstantsValues.MNY_RCT);
						 if(chqNo.length() > 0)
							cs9.setCsTvNo(mrNo+"("+chqNo+")");
						 else
							cs9.setCsTvNo(mrNo);
						 cs9.setPayMode(payMode);
						 cs9.setCsVouchType("CONTRA");
						 cs9.setCsFaCode(customer.getCustFaCode());
						 cs9.setCsVouchNo(voucherNo);
						 cs9.setCsDt(css.getCssDt());
						 
						 CashStmtStatus actCss2 = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						 cs9.setCsNo(actCss2);
						 actCss2.getCashStmtList().add(cs9);
						 session.merge(actCss1);
						 
					}
					
					double finalCustAmt = tdsAmt + dedAmt + claimAmt;
					
					if(finalCustAmt > 0){
						
						 CashStmt cs9 = new CashStmt();
						 cs9.setUserCode(currentUser.getUserCode());
						 cs9.setbCode(currentUser.getUserBranchCode());
						 cs9.setCsDescription("(Final Amount From Customer) "+mrList.get(0).getMrDesc());
						 cs9.setCsDrCr('C');
						 cs9.setCsAmt(finalCustAmt);
						 cs9.setCsType(ConstantsValues.MNY_RCT);
						 if(chqNo.length() > 0)
							cs9.setCsTvNo(mrNo+"("+chqNo+")");
						 else
							cs9.setCsTvNo(mrNo);
						 cs9.setPayMode(payMode);
						 cs9.setCsVouchType("CONTRA");
						 cs9.setCsFaCode(customer.getCustFaCode());
						 cs9.setCsVouchNo(voucherNo);
						 cs9.setCsDt(css.getCssDt());
						 
						 CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						 cs9.setCsNo(actCss1);
						 actCss1.getCashStmtList().add(cs9);
						 session.merge(actCss1);
						 
					}
			}
			transaction.commit();
		}catch(Exception e){
			e.printStackTrace();
			transaction.rollback();
			logger.info("Exception in savePDMR "+e);
			mrNo = null;
		}finally{
			session.clear();
			session.close();
		}
		return mrNo;
	}
	
	
	@SuppressWarnings("unchecked")
	public String saveDPMR(List<MoneyReceipt> mrList){
		System.out.println("enter into saveDPMR function");
		String mrNo = "";
		String othMrNo = "";
		String payBy = "";
		String cheqNo = ""; 
		String rtgs = "";
		User currentUser = (User) httpSession.getAttribute("currentUser");
		Session session=null;
		Transaction transaction=null;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			String faCode = "";
			String tdsCode = "";
			String srvTaxCode = "";
			String claimCode = "";
			
			
			List<FAMaster> famList = new ArrayList<>();
			famList = session.createCriteria(FAMaster.class)
					  .add(Restrictions.disjunction()
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_COLLECTION))
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_TDS))
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.FREIGHT_SRV_TAX))
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.CLAIM))
				      .add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME, ConstantsValues.CASH_IN_HAND))
				  ).list(); 
			 
			
			if(!famList.isEmpty()){
				for(int i=0;i<famList.size();i++){
					if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_COLLECTION)){
						faCode = famList.get(i).getFaMfaCode();
					}else if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_TDS)){
						tdsCode = famList.get(i).getFaMfaCode();
					}else if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.FREIGHT_SRV_TAX)){
						srvTaxCode = famList.get(i).getFaMfaCode();
					}else if(famList.get(i).getFaMfaName().equalsIgnoreCase(ConstantsValues.CLAIM)){
						claimCode = famList.get(i).getFaMfaCode();
					}else{
						System.out.println("invalid result");
					}
				}
			}
			
			
			
			// Fetch Last MR of Respective Branch
			List<MoneyReceipt> dbMrList = new ArrayList<>();
			Query query = session.createQuery("from MoneyReceipt where mrBrhId = :type order by mrId DESC");
			query.setParameter("type", Integer.parseInt(currentUser.getUserBranchCode()));
			dbMrList = query.setMaxResults(1).list();
		
			if(!dbMrList.isEmpty()){
				String prevMrNo = dbMrList.get(0).getMrNo();
				mrNo  = "MR" + String.valueOf(Integer.parseInt(prevMrNo.substring(2)) + 1 + 1000000).substring(1);
			}else{
				mrNo = "MR000001";
			}
			
			
			if(!mrList.isEmpty()){
				
				double recAmt = 0.0;
				double freight = 0.0;
				double dedAmt = 0.0;
				double excAmt = 0.0;
				double srvTaxAmt = 0.0;
				double tdsAmt = 0.0;
				double claimAmt = 0.0;
				
				
				for(int i=0;i<mrList.size();i++){
					MoneyReceipt moneyReceipt = mrList.get(i);					
					
					payBy = String.valueOf(moneyReceipt.getMrPayBy());					
					if(payBy.equals("Q"))
						cheqNo = moneyReceipt.getMrChqNo();					
					if(payBy.equalsIgnoreCase("R"))
						rtgs = moneyReceipt.getMrRtgsRefNo();
						
					moneyReceipt.setUserCode(currentUser.getUserCode());
					moneyReceipt.setbCode(currentUser.getUserBranchCode());
					moneyReceipt.setMrNo(mrNo);
					moneyReceipt.setMrType(ConstantsValues.DP_MR);
					moneyReceipt.setMrBrhId(Integer.parseInt(currentUser.getUserBranchCode()));
					
					if(mrList.get(i).getMrOthMrAmt() > 0){
						moneyReceipt.setMrOthMrNo(othMrNo);
					}
					
					recAmt = recAmt + moneyReceipt.getMrNetAmt();
					freight = freight + moneyReceipt.getMrFreight();
					dedAmt = dedAmt + moneyReceipt.getMrDedAmt();
					excAmt = excAmt + moneyReceipt.getMrAccessAmt();
					srvTaxAmt = srvTaxAmt + moneyReceipt.getMrSrvTaxAmt();
					tdsAmt = tdsAmt + moneyReceipt.getMrTdsAmt();
					
					if(moneyReceipt.getMrDedAmt() > 0){
						List<Map<String,Object>> dedList = new ArrayList<Map<String,Object>>();
						dedList = moneyReceipt.getMrDedDetList(); 
						if(!dedList.isEmpty()){
							double claim = 0.0;
							for(int j=0;j<dedList.size();j++){
								Map<String,Object> map = dedList.get(j); 
								if(String.valueOf(map.get("dedType")).equalsIgnoreCase("CLAIM SHORTAGE")){
									if(Double.parseDouble(String.valueOf(map.get("dedAmt"))) > 0){
										claim = claim + Double.parseDouble(String.valueOf(map.get("dedAmt")));
									}
								}else if(String.valueOf(map.get("dedType")).equalsIgnoreCase("CLAIM DAMAGE")){
									if(Double.parseDouble(String.valueOf(map.get("dedAmt"))) > 0){
										claim = claim + Double.parseDouble(String.valueOf(map.get("dedAmt")));
									}
								}
							}
							claimAmt = claimAmt + claim;
						}
					}	
					
					Bill bill = (Bill) session.get(Bill.class,moneyReceipt.getBill().getBlId());
					
					double remAmt = bill.getBlRemAmt();
					if(remAmt > moneyReceipt.getMrFreight()){
						remAmt = remAmt - moneyReceipt.getMrFreight();
						bill.setBlRemAmt(Double.parseDouble(String.format("%.2f",remAmt))); 
					}else{
						bill.setBlRemAmt(0.0);
					}
					
					session.merge(bill);
					
					moneyReceipt.setBill(bill);
					session.save(moneyReceipt);
				}
				
				
				if(mrList.get(0).getMrOthMrAmt() > 0){
					othMrNo = "MR" + String.valueOf(Integer.parseInt(mrNo.substring(2)) + 1 + 1000000).substring(1);
					MoneyReceipt moneyReceipt = new MoneyReceipt();
					
					moneyReceipt.setUserCode(currentUser.getUserCode());
					moneyReceipt.setbCode(currentUser.getUserBranchCode());
					moneyReceipt.setMrType(ConstantsValues.OA_MR);
					moneyReceipt.setMrBrhId(Integer.parseInt(currentUser.getUserBranchCode()));
					moneyReceipt.setMrIsPending(true);
					moneyReceipt.setMrNo(othMrNo);
					moneyReceipt.setMrNetAmt(mrList.get(0).getMrOthMrAmt());
					moneyReceipt.setMrRemAmt(mrList.get(0).getMrOthMrAmt());
					moneyReceipt.setMrBnkId(mrList.get(0).getMrBnkId());
					moneyReceipt.setMrChqNo(mrList.get(0).getMrChqNo());
					moneyReceipt.setMrCustBankName(mrList.get(0).getMrCustBankName());
					moneyReceipt.setMrCustId(mrList.get(0).getMrCustId());
					moneyReceipt.setMrDate(mrList.get(0).getMrDate());
					moneyReceipt.setMrDesc(mrList.get(0).getMrDesc());
					moneyReceipt.setMrPayBy(mrList.get(0).getMrPayBy());
					moneyReceipt.setMrRtgsRefNo(mrList.get(0).getMrRtgsRefNo());
					
					recAmt = recAmt + mrList.get(0).getMrOthMrAmt();
					
					session.save(moneyReceipt);
					
				}
				
				
				Criteria cr = session.createCriteria(CashStmtStatus.class);
				cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,currentUser.getUserBranchCode()));
				cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT,mrList.get(0).getMrDate()));
				List<CashStmtStatus> cssList = cr.list();
				
				CashStmtStatus css = cssList.get(0);
				Customer customer = (Customer) session.get(Customer.class,mrList.get(0).getMrCustId());
				BankMstr bank = (BankMstr) session.get(BankMstr.class,mrList.get(0).getMrBnkId());
				
				if(css != null){
					
					List<CashStmt> csList = css.getCashStmtList();
					
					int voucherNo = 0;
					if(!csList.isEmpty()){
						for(int i=0;i<csList.size();i++){
							if(voucherNo < csList.get(i).getCsVouchNo()){
								voucherNo = csList.get(i).getCsVouchNo();
							}
						}
						voucherNo = voucherNo + 1;
					}else{
						voucherNo = 1;
					}
					
					
					if(recAmt > 0){
						
						CashStmt cs1 = new CashStmt();
						cs1.setUserCode(currentUser.getUserCode());
						cs1.setbCode(currentUser.getUserBranchCode());
						cs1.setCsDescription(mrList.get(0).getMrDesc());
						cs1.setCsAmt(recAmt);
						cs1.setCsType(ConstantsValues.MNY_RCT);
						if(payBy.equalsIgnoreCase("C")){
							cs1.setCsTvNo(mrNo);
							cs1.setPayMode("C");
							cs1.setCsDrCr('C');
							cs1.setCsVouchType("CASH");
							cs1.setCsFaCode(customer.getCustFaCode());
						}else if(payBy.equalsIgnoreCase("R")){	
							cs1.setCsDrCr('C');
							cs1.setPayMode("R");
							cs1.setCsTvNo(mrNo+" ("+rtgs+")");
							cs1.setCsVouchType("BANK");
							cs1.setCsFaCode(customer.getCustFaCode());
						}else if(payBy.equals("Q")){
							cs1.setCsDrCr('C');
							cs1.setCsTvNo(mrNo+" ("+cheqNo+")");
							cs1.setPayMode("Q");
							cs1.setCsVouchType("BANK");
							cs1.setCsFaCode(customer.getCustFaCode());
						}
						
						cs1.setCsVouchNo(voucherNo);
						cs1.setCsDt(css.getCssDt());
						 
						cs1.setCsNo(css);
						css.getCashStmtList().add(cs1);
						session.merge(css);
						
						if(!payBy.equalsIgnoreCase("C")){
							
						CashStmt cs = new CashStmt();
						cs.setUserCode(currentUser.getUserCode());
						cs.setbCode(currentUser.getUserBranchCode());
						cs.setCsDescription(mrList.get(0).getMrDesc());
						cs.setCsAmt(recAmt);
						cs.setCsType(ConstantsValues.MNY_RCT);
						
						 if(payBy.equalsIgnoreCase("R")){
							cs.setPayMode("R");
							cs.setCsTvNo(mrNo+" ("+rtgs+")");
							cs.setCsDrCr('D');
							cs.setCsVouchType("BANK");
							cs.setCsFaCode(bank.getBnkFaCode());
						}else if(payBy.equals("Q")){
							cs.setCsTvNo(mrNo+" ("+cheqNo+")");
							cs.setPayMode("Q");
							cs.setCsDrCr('D');
							cs.setCsVouchType("BANK");
							cs.setCsFaCode(bank.getBnkFaCode());
						}
						
						cs.setCsVouchNo(voucherNo);
						cs.setCsDt(css.getCssDt());
						
						CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						cs.setCsNo(actCss);
						actCss.getCashStmtList().add(cs);
						session.merge(actCss);
						
						
						BankMstr bankMstr = (BankMstr) session.get(BankMstr.class,mrList.get(0).getMrBnkId());
						double newBalAmt = bankMstr.getBnkBalanceAmt() + recAmt;
						bankMstr.setBnkBalanceAmt(newBalAmt);
						session.merge(bankMstr);
						}
					
					}
					
					if(claimAmt > 0){
						 CashStmt cs2 = new CashStmt();
						 cs2.setUserCode(currentUser.getUserCode());
						 cs2.setbCode(currentUser.getUserBranchCode());
						 cs2.setCsDescription("(Claim Deduction) "+mrList.get(0).getMrDesc());
						 cs2.setCsAmt(claimAmt);
						 cs2.setCsType(ConstantsValues.MNY_RCT);
						 
						 if(payBy.equalsIgnoreCase("C")){
							 cs2.setCsTvNo(mrNo);
							 cs2.setPayMode("C");
							 cs2.setCsDrCr('D');
							 cs2.setCsVouchType("CASH");
							 cs2.setCsFaCode(claimCode);
						 }else if(payBy.equals("R")){							
							 cs2.setPayMode("R");
							 cs2.setCsTvNo(mrNo+ " ("+rtgs+")");
							 cs2.setCsDrCr('D');
							 cs2.setCsVouchType("BANK");
							 cs2.setCsFaCode(claimCode);
						 }else if(payBy.equals("Q")){
							cs2.setCsTvNo(mrNo+" ("+cheqNo+")");
							cs2.setPayMode("Q");
							cs2.setCsDrCr('D');
							cs2.setCsVouchType("BANK");
							 cs2.setCsFaCode(claimCode);
						 }
						 
						 cs2.setCsVouchNo(voucherNo);
						 cs2.setCsDt(css.getCssDt());
						 
						 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						 cs2.setCsNo(actCss);
						 actCss.getCashStmtList().add(cs2);
						 session.merge(actCss);
					}
					
					dedAmt = dedAmt - claimAmt;
					
					if(dedAmt != 0){
						 CashStmt cs3 = new CashStmt();
						 cs3.setUserCode(currentUser.getUserCode());
						 cs3.setbCode(currentUser.getUserBranchCode());
						 cs3.setCsDescription("(Freight Deduction) "+mrList.get(0).getMrDesc());
						 cs3.setCsAmt(dedAmt);
						 cs3.setCsType(ConstantsValues.MNY_RCT);
						 
						 if(payBy.equalsIgnoreCase("C")){
							 cs3.setCsTvNo(mrNo);
							 cs3.setPayMode("C");
							 cs3.setCsDrCr('D');
							 cs3.setCsVouchType("CASH");
							 cs3.setCsFaCode(faCode);
						 }else if(payBy.equals("R")){
							 cs3.setCsDrCr('D');
							 cs3.setPayMode("R");
							 cs3.setCsTvNo(mrNo+" ("+rtgs+")");
							 cs3.setCsVouchType("BANK");
							 cs3.setCsFaCode(faCode);
						 }else if(payBy.equals("Q")){
							 cs3.setCsDrCr('D');
							cs3.setCsTvNo(mrNo+" ("+cheqNo+")");
							cs3.setPayMode("Q");
							 cs3.setCsVouchType("BANK");
							 cs3.setCsFaCode(faCode);
						}
						 
						 cs3.setCsVouchNo(voucherNo);
						 cs3.setCsDt(css.getCssDt());
						 
						 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						 cs3.setCsNo(actCss);
						 actCss.getCashStmtList().add(cs3);
						 session.merge(actCss);
						 
					}
					
					
					if(srvTaxAmt > 0){
						 CashStmt cs5 = new CashStmt();
						 cs5.setUserCode(currentUser.getUserCode());
						 cs5.setbCode(currentUser.getUserBranchCode());
						 cs5.setCsDescription("(Service Tax Pay By Customer) "+mrList.get(0).getMrDesc());
						 cs5.setCsDrCr('C');
						 cs5.setCsAmt(srvTaxAmt);
						 cs5.setCsType(ConstantsValues.MNY_RCT);
						 
						if(payBy.equalsIgnoreCase("C")){
							cs5.setCsTvNo(mrNo);
							cs5.setPayMode("C");
							cs5.setCsVouchType("CONTRA");
							cs5.setCsFaCode(srvTaxCode);
						}else if(payBy.equals("R")){							
							cs5.setPayMode("R");
							cs5.setCsTvNo(mrNo+ "("+rtgs+")");
							cs5.setCsVouchType("CONTRA");
							 cs5.setCsFaCode(srvTaxCode);
						}else if(payBy.equals("Q")){
							cs5.setCsTvNo(mrNo+" ("+cheqNo+")");
							cs5.setPayMode("Q");
							cs5.setCsVouchType("CONTRA");
							 cs5.setCsFaCode(srvTaxCode);
						}
						 
						 cs5.setCsVouchNo(voucherNo);
						 cs5.setCsDt(css.getCssDt());
						 
						 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						 cs5.setCsNo(actCss);
						 actCss.getCashStmtList().add(cs5);
						 session.merge(actCss);
					}
					
					
					if(tdsAmt > 0){
						 CashStmt cs6 = new CashStmt();
						 cs6.setUserCode(currentUser.getUserCode());
						 cs6.setbCode(currentUser.getUserBranchCode());
						 cs6.setCsDescription("(TDS Deduction By Customer) "+mrList.get(0).getMrDesc());
						 cs6.setCsAmt(tdsAmt);
						 cs6.setCsType(ConstantsValues.MNY_RCT);
						 if(payBy.equalsIgnoreCase("C")){
							 cs6.setCsDrCr('D');
							 cs6.setCsTvNo(mrNo);
							 cs6.setPayMode("C");
							 cs6.setCsVouchType("CASH");
							 cs6.setCsFaCode(tdsCode);
						 }else if(payBy.equals("R")){
							 cs6.setCsDrCr('D');
							 cs6.setPayMode("R");
							 cs6.setCsTvNo(mrNo+" ("+rtgs+")");
							 cs6.setCsVouchType("BANK");
							 cs6.setCsFaCode(tdsCode);
						 }else if(payBy.equals("Q")){
							 cs6.setCsDrCr('D');
							cs6.setCsTvNo(mrNo+" ("+cheqNo+")");
							cs6.setPayMode("Q");
							cs6.setCsVouchType("BANK");
							 cs6.setCsFaCode(tdsCode);
						 }
						 cs6.setCsVouchNo(voucherNo);
						 cs6.setCsDt(css.getCssDt());
						 
						 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						 cs6.setCsNo(actCss);
						 actCss.getCashStmtList().add(cs6);
						 session.merge(actCss);
						 
						 
					}
					
					if(excAmt > 0){
						 
						 CashStmt cs8 = new CashStmt();
						 cs8.setUserCode(currentUser.getUserCode());
						 cs8.setbCode(currentUser.getUserBranchCode());
						 cs8.setCsDescription("(Excess Amount) "+mrList.get(0).getMrDesc());
						 cs8.setCsAmt(excAmt);
						 cs8.setCsType(ConstantsValues.MNY_RCT);
						 if(payBy.equalsIgnoreCase("C")){
							 cs8.setCsDrCr('C');
							 cs8.setCsTvNo(mrNo);
							 cs8.setPayMode("C");	
							 cs8.setCsVouchType("CASH");
							 cs8.setCsFaCode(faCode);
						 }else if(payBy.equalsIgnoreCase("R")){
							cs8.setPayMode("R");
							 cs8.setCsDrCr('C');
							cs8.setCsTvNo(mrNo+" ("+rtgs+")");
							cs8.setCsVouchType("BANK");
							 cs8.setCsFaCode(faCode);
						 }else if(payBy.equals("Q")){
							 cs8.setCsDrCr('C');
							cs8.setCsTvNo(mrNo+" ("+cheqNo+")");
							cs8.setPayMode("Q");
							cs8.setCsVouchType("BANK");
							 cs8.setCsFaCode(faCode);
						 }
						 cs8.setCsVouchNo(voucherNo);
						 cs8.setCsDt(css.getCssDt());
						 
						 CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						 cs8.setCsNo(actCss1);
						 actCss1.getCashStmtList().add(cs8);
						 session.merge(actCss1);
					
						 
						 CashStmt cs9 = new CashStmt();
						 cs9.setUserCode(currentUser.getUserCode());
						 cs9.setbCode(currentUser.getUserBranchCode());
						 cs9.setCsDescription("(Final Amount From Customer) "+mrList.get(0).getMrDesc());
						 cs9.setCsAmt(excAmt);
						 cs9.setCsType(ConstantsValues.MNY_RCT);
						 if(payBy.equalsIgnoreCase("C")){
							 cs9.setCsDrCr('D');
							 cs9.setCsTvNo(mrNo);
							 cs9.setPayMode("C");
							 cs9.setCsVouchType("CASH");
							 cs9.setCsFaCode(customer.getCustFaCode());
						 }else if(payBy.equalsIgnoreCase("R")){
							 cs9.setPayMode("R");
							 cs9.setCsDrCr('D');
							 cs9.setCsTvNo(mrNo+" ("+rtgs+")");
							 cs9.setCsVouchType("BANK");
							 cs9.setCsFaCode(customer.getCustFaCode());
						 }else if(payBy.equals("Q")){
							 cs9.setCsDrCr('D');
							cs9.setCsTvNo(mrNo+" ("+cheqNo+")");
							cs9.setPayMode("Q");
							cs9.setCsVouchType("BANK");
							 cs9.setCsFaCode(customer.getCustFaCode());
						 }
						 cs9.setCsVouchNo(voucherNo);
						 cs9.setCsDt(css.getCssDt());
						 
						 CashStmtStatus actCss2 = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						 cs9.setCsNo(actCss2);
						 actCss2.getCashStmtList().add(cs9);
						 session.merge(actCss1);
						 
					}
					
					double finalCustAmt = tdsAmt + dedAmt + claimAmt;
					
					if(finalCustAmt > 0){
						
						 CashStmt cs9 = new CashStmt();
						 cs9.setUserCode(currentUser.getUserCode());
						 cs9.setbCode(currentUser.getUserBranchCode());
						 cs9.setCsDescription("(Final Amount From Customer) "+mrList.get(0).getMrDesc());
						 cs9.setCsAmt(finalCustAmt);
						 cs9.setCsType(ConstantsValues.MNY_RCT);
						 if(payBy.equalsIgnoreCase("C")){
							 cs9.setCsDrCr('C');
							 cs9.setCsTvNo(mrNo);
							 cs9.setPayMode("C");
							 cs9.setCsVouchType("CASH");
							 cs9.setCsFaCode(customer.getCustFaCode());
						 }else if(payBy.equalsIgnoreCase("R")){
							cs9.setPayMode("R");
							cs9.setCsDrCr('C');
							cs9.setCsTvNo(mrNo+" ("+rtgs+")");
							 cs9.setCsVouchType("BANK");
							 cs9.setCsFaCode(customer.getCustFaCode());
						 }else if(payBy.equals("Q")){
							 cs9.setCsDrCr('C');
							 cs9.setCsVouchType("BANK");
							 cs9.setCsFaCode(customer.getCustFaCode());
							 cs9.setCsTvNo(mrNo+" ("+cheqNo+")");
							cs9.setPayMode("Q");
						 }
						 cs9.setCsVouchNo(voucherNo);
						 cs9.setCsDt(css.getCssDt());
						 
						 CashStmtStatus actCss1 = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						 cs9.setCsNo(actCss1);
						 actCss1.getCashStmtList().add(cs9);
						 session.merge(actCss1);
						 
					}	
				}	
			}
			transaction.commit();
		}catch(Exception e){
			transaction.rollback();
			logger.info("exception in saveDPMR"+e);
			mrNo = null;
		}finally{
			session.clear();
			session.close();
		}
		return mrNo;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<MoneyReceipt> getOnAccFrRev(int custId, int brhId){
		System.out.println("enter into getOnAccFrRev function");
		List<MoneyReceipt> mrList = new ArrayList<>();
		Session session=null;
		try{
			session = this.sessionFactory.openSession();
			
			Criteria cr = session.createCriteria(MoneyReceipt.class);
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID,custId));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE,ConstantsValues.OA_MR));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID,brhId));
			cr.add(Restrictions.gt(MoneyReceiptCNTS.MR_REM_AMT,0.0));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_IS_PENDING,true));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_CANCEL,false));
			mrList = cr.list();
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception in getOnAccFrRev"+e);
		}finally{
			session.clear();
			session.close();
		}
		return mrList;		
	}
	
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> saveOnAccRev(Map<String,Object> clientMap){
		System.out.println("enter into saveOnAccRev function");
		Map<String, Object> result = new HashMap<String, Object>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		int prevCustId = Integer.parseInt(String.valueOf(clientMap.get("prevCustId")));
		int prevBrhId = Integer.parseInt(String.valueOf(clientMap.get("prevBrhId")));
		String prevMrNo = String.valueOf(clientMap.get("prevMrNo"));
		String newDesc = String.valueOf(clientMap.get("newDesc"));
		int newCustId = Integer.parseInt(String.valueOf(clientMap.get("newCustId")));
		Date csDt = Date.valueOf((String) clientMap.get("csDt"));    
		Session session=this.sessionFactory.openSession();;
		Transaction transaction=session.beginTransaction();
		try{
			Criteria cr = session.createCriteria(MoneyReceipt.class);
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID,prevCustId));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE,ConstantsValues.OA_MR));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID,prevBrhId));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_NO,prevMrNo));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_IS_PENDING,true));
			List<MoneyReceipt> mrList = cr.list();
					
			if(!mrList.isEmpty()){
				MoneyReceipt prevMR = mrList.get(0); 
				
				Query query = session.createQuery("from MoneyReceipt where mrBrhId = :type order by mrId DESC");
				query.setParameter("type", Integer.parseInt(currentUser.getUserBranchCode()));
				List<MoneyReceipt> dbMrList = query.setMaxResults(1).list();
			
				String newMrNo = "";
				if(!dbMrList.isEmpty()){
					String preMrNo = dbMrList.get(0).getMrNo();
					System.out.println("preMrNo = "+preMrNo);
					newMrNo  = "MR" + String.valueOf(Integer.parseInt(preMrNo.substring(2)) + 1 + 1000000).substring(1);
					System.out.println("newMrNo = "+newMrNo);
				}else{
					newMrNo = "MR000001";
				}
				
				
				MoneyReceipt newMR = new MoneyReceipt();
				newMR.setbCode(currentUser.getUserBranchCode());
				newMR.setUserCode(currentUser.getUserCode());
				newMR.setMrBnkId(prevMR.getMrBnkId());
				newMR.setMrBrhId(Integer.parseInt(currentUser.getUserBranchCode()));
				newMR.setMrChqNo(prevMR.getMrChqNo());
				newMR.setMrCustBankName(prevMR.getMrCustBankName());
				newMR.setMrCustId(newCustId);
				newMR.setMrDate(csDt);
				newMR.setMrDesc(newDesc);
				newMR.setMrIsPending(true);
				newMR.setMrNetAmt(prevMR.getMrRemAmt());
				newMR.setMrNo(newMrNo);
				newMR.setMrPayBy(prevMR.getMrPayBy());
				newMR.setMrRemAmt(prevMR.getMrRemAmt());
				newMR.setMrRtgsRefNo(prevMR.getMrRtgsRefNo());
				newMR.setMrType(prevMR.getMrType());
				
				/****************************************entry cash statement***************************************************/
				Customer newCustomer=(Customer)session.get(Customer.class, newCustId);
				
				 Criteria criteria=session.createCriteria(CashStmtStatus.class);
				 			criteria.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,currentUser.getUserBranchCode()));
				 			criteria.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT,newMR.getMrDate()));
				 			List<CashStmtStatus> cssList=criteria.list();
				
				 			int voucherNo=0;		
				 	    if(!cssList.isEmpty()){
				 	    	List<CashStmt> csList= cssList.get(0).getCashStmtList();
				 		        System.out.println("csList Size="+csList.size());
				              
				                	session.save(newMR);
				                	
				                	prevMR.setMrOthMrNo(newMrNo);
				    				prevMR.setMrOthMrAmt(newMR.getMrNetAmt());
				    				prevMR.setMrRemAmt(0.0);
				    				prevMR.setMrIsPending(false);
				                	session.update(prevMR);
				                	
				 		        CashStmt cashStmtCredit=new CashStmt();
				            cashStmtCredit.setbCode(newCustomer.getBranchCode());
				            cashStmtCredit.setCsAmt(newMR.getMrNetAmt());
				            cashStmtCredit.setCsDescription(newMR.getMrDesc());
				            cashStmtCredit.setCsDrCr('C');
				            cashStmtCredit.setCsDt(csDt);
				            cashStmtCredit.setCsFaCode(newCustomer.getCustFaCode());
				            cashStmtCredit.setCsIsClose(csList.get(0).isCsIsClose());
				            cashStmtCredit.setCsIsVRev(csList.get(0).isCsIsVRev());
				            cashStmtCredit.setCsTvNo(newMrNo);
				            
				            if(newMR.getMrPayBy() == 'C'){
								cashStmtCredit.setCsTvNo(newMrNo);
								cashStmtCredit.setPayMode("C");
							}else if(newMR.getMrPayBy() == 'Q'){
								String mrChqNo = newMR.getMrChqNo();
								cashStmtCredit.setCsTvNo(newMrNo+" ("+mrChqNo+")");
								cashStmtCredit.setPayMode("Q");
							}else if(newMR.getMrPayBy() == 'R'){				
								cashStmtCredit.setCsTvNo(newMrNo +" ("+newMR.getMrRtgsRefNo()+")");
								cashStmtCredit.setPayMode("R");
							}
					
				            cashStmtCredit.setCsType("reverse MR");
				            cashStmtCredit.setCsVouchType("CONTRA");
				            List<CashStmt> csVouchList = cssList.get(0).getCashStmtList();
							
							if(!csVouchList.isEmpty()){
								for(int i=0;i<csVouchList.size();i++){
									if(voucherNo < csVouchList.get(i).getCsVouchNo()){
										voucherNo = csVouchList.get(i).getCsVouchNo();
									}
								}
								voucherNo = voucherNo + 1;
							}else{
								voucherNo = 1;
							}
							
						 cashStmtCredit.setCsVouchNo(voucherNo);
						 cashStmtCredit.setUserCode(currentUser.getUserCode());
						 CashStmtStatus css=cssList.get(0);
						 cashStmtCredit.setCsNo(css);
						 csList.add(cashStmtCredit);
						 session.merge(css);
						 
						 
					Customer preCustomer=(Customer)session.get(Customer.class,prevCustId);
					 CashStmt cashStmtDebit=new CashStmt();
			            cashStmtDebit.setbCode(currentUser.getUserBranchCode());
			            cashStmtDebit.setCsAmt(newMR.getMrNetAmt());
			            cashStmtDebit.setCsDescription(newMR.getMrDesc());
			            cashStmtDebit.setCsDrCr('D');
			            cashStmtDebit.setCsDt(csDt);
			            cashStmtDebit.setCsFaCode(preCustomer.getCustFaCode());
			            cashStmtDebit.setCsIsClose(csList.get(0).isCsIsClose());
			            cashStmtDebit.setCsIsVRev(csList.get(0).isCsIsVRev());
			     
			            
			            if(newMR.getMrPayBy() == 'C'){
							cashStmtDebit.setCsTvNo(newMrNo);
							cashStmtDebit.setPayMode("C");
						}else if(newMR.getMrPayBy() == 'Q'){
							String mrChqNo = newMR.getMrChqNo();
							cashStmtDebit.setCsTvNo(newMrNo+" ("+mrChqNo+")");
							cashStmtDebit.setPayMode("Q");
						}else if(newMR.getMrPayBy() == 'R'){				
							cashStmtDebit.setCsTvNo(newMrNo +" ("+newMR.getMrRtgsRefNo()+")");
							cashStmtDebit.setPayMode("R");
						}
				
			            cashStmtDebit.setCsType("Reverse MR");
			            cashStmtDebit.setCsVouchType("CONTRA");					
					 cashStmtDebit.setCsVouchNo(voucherNo);
					 cashStmtDebit.setUserCode(currentUser.getUserCode());
					 
					 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
						cashStmtDebit.setCsNo(actCss);
						
						actCss.getCashStmtList().add(cashStmtDebit);
						session.merge(actCss);
					 
						 result.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				 	    	result.put("mrNo", newMrNo);
				                
				 	    }
				 	    else{
				 	    	result.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				 	    	result.put("msg", "Cashstmtstatus not found.");
				 	    }
			}
			transaction.commit();
		}catch(Exception e){
			transaction.rollback();
			e.printStackTrace();
			logger.error("Exception in saveOnAccRev"+e);
			result.clear();
			result.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			result.put("msg", "Please contact to support team.");
		}finally{
			session.close();
		}
		return result;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> getMrFrPrint(int brhId, String frMrNo, String toMrNo){
		System.out.println("enter into getMrFrPrint function");
		List<Map<String,Object>> mrList = new ArrayList<>();
		List<MoneyReceipt> actMrList = new ArrayList<MoneyReceipt>();
		int frMr = Integer.parseInt(frMrNo.substring(3));
		int toMr = Integer.parseInt(toMrNo.substring(3));
		Session session=null;
		try{
			 session = this.sessionFactory.openSession();
			 Criteria cr = session.createCriteria(MoneyReceipt.class);
			 cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID,brhId));
			 actMrList = cr.list();
			 
			 if(!actMrList.isEmpty()){
				 NumToWords w = new NumToWords();
				 for(int i=0;i<actMrList.size();i++){
					 if(!mrList.isEmpty()){
						 if(Integer.parseInt(actMrList.get(i).getMrNo().substring(2)) >= frMr && Integer.parseInt(actMrList.get(i).getMrNo().substring(2)) <= toMr){
							 boolean mrUpdate = false;
							 for(int j=0;j<mrList.size();j++){
								 Map<String,Object> map = mrList.get(j);
								 MoneyReceipt moneyReceipt = (MoneyReceipt) map.get("mr");
								 if(actMrList.get(i).getMrNo().equalsIgnoreCase(moneyReceipt.getMrNo())){
									 
									 moneyReceipt.setMrNetAmt(moneyReceipt.getMrNetAmt() + actMrList.get(i).getMrNetAmt());
									 String amt = String.valueOf(moneyReceipt.getMrNetAmt());
									 int rupees = Integer.parseInt(amt.split("\\.")[0]);
									 String str1 = w.convert(rupees);
									 
									 str1 += " Rupees ";
									 String pasieRange=amt.split("\\.")[1];
									 int paise; 
									 if(pasieRange.length()<3){
										 paise = Integer.parseInt(pasieRange);
									 } else{
										 paise=Integer.parseInt(pasieRange.substring(0,3));
									 }
									 
									 
									 String str2 = "";
									 if (paise != 0) {
										   str2 += " and";
										   str2 = w.convert(paise);
										   str2 += " Paise";
									 }
									 String str3 = str1 + str2 + " only";
									 
									 map.put("mrAmt", str3);
									 map.put("mr",moneyReceipt);
									 
									 List<Map<String,Object>> blDetList = (List<Map<String, Object>>) map.get("blDetList");
									 
									 Map<String,Object> blDetMap = new HashMap<String, Object>();
									 
									 if(actMrList.get(i).getBill() != null){
										 blDetMap.put("billNo",actMrList.get(i).getBill().getBlBillNo());
										 blDetMap.put("billDt",actMrList.get(i).getBill().getBlBillDt());
										 blDetMap.put("frtAmt",actMrList.get(i).getMrFreight());
										 blDetMap.put("tdsAmt",actMrList.get(i).getMrTdsAmt());
										 blDetMap.put("excAmt",actMrList.get(i).getMrAccessAmt());
										 blDetMap.put("dedAmt",actMrList.get(i).getMrDedAmt());
										 blDetMap.put("recAmt",actMrList.get(i).getMrNetAmt());
									 }
									
									 blDetList.add(blDetMap);
									 map.put("blDetList", blDetList);
									 
									 mrList.remove(j);
									 mrList.add(j,map);
									 mrUpdate = true;
									 break;
								 }
							 }
							 
							 if(!mrUpdate){
								 Map<String,Object> map = new HashMap<String, Object>();
								 Branch branch = (Branch) session.get(Branch.class,actMrList.get(i).getMrBrhId());
								 Customer customer = (Customer) session.get(Customer.class,actMrList.get(i).getMrCustId());
								 
								 String amt = String.valueOf(actMrList.get(i).getMrNetAmt());
								 int rupees = Integer.parseInt(amt.split("\\.")[0]);
								 String str1 = w.convert(rupees);
								 str1 += " Rupees ";
								 int paise = Integer.parseInt(amt.split("\\.")[1]);
								 String str2 = "";
								 if (paise != 0) {
									   str2 += " and";
									   str2 = w.convert(paise);
									   str2 += " Paise";
								 }
								 String str3 = str1 + str2 + "only";
								  
								 map.put("mr",actMrList.get(i));
								 map.put("brhAdd",branch.getBranchAdd());
								 map.put("brhCode",branch.getBranchFaCode());
								 map.put("brhName",branch.getBranchName());
								 map.put("custName",customer.getCustName());
								 map.put("mrAmt",str3);
								 
								 Map<String,Object> blDetMap = new HashMap<String, Object>();
								 
								 if(actMrList.get(i).getBill() != null){
									 blDetMap.put("billNo",actMrList.get(i).getBill().getBlBillNo());
									 blDetMap.put("billDt",actMrList.get(i).getBill().getBlBillDt());
									 blDetMap.put("frtAmt",actMrList.get(i).getMrFreight());
									 blDetMap.put("tdsAmt",actMrList.get(i).getMrTdsAmt());
									 blDetMap.put("excAmt",actMrList.get(i).getMrAccessAmt());
									 blDetMap.put("dedAmt",actMrList.get(i).getMrDedAmt());
									 blDetMap.put("recAmt",actMrList.get(i).getMrNetAmt());
								 }
								 
								 List<Map<String,Object>> blDetList = new ArrayList<Map<String,Object>>();
								 
								 blDetList.add(blDetMap);
								 
								 map.put("blDetList",blDetList);
								 
								 mrList.add(map);
							 }
						 }
					 }else{
						 if(Integer.parseInt(actMrList.get(i).getMrNo().substring(2)) >= frMr && Integer.parseInt(actMrList.get(i).getMrNo().substring(2)) <= toMr){
							 Map<String,Object> map = new HashMap<String, Object>();
							 Branch branch = (Branch) session.get(Branch.class,actMrList.get(i).getMrBrhId());
							 Customer customer = (Customer) session.get(Customer.class,actMrList.get(i).getMrCustId());
							 
							 String amt = String.valueOf(actMrList.get(i).getMrNetAmt());
							 int rupees = Integer.parseInt(amt.split("\\.")[0]);
							 String str1 = w.convert(rupees);
							 str1 += " Rupees ";
							 int paise = Integer.parseInt(amt.split("\\.")[1]);
							 String str2 = "";
							 if (paise != 0) {
								   str2 += " and";
								   str2 = w.convert(paise);
								   str2 += " Paise";
							 }
							 String str3 = str1 + str2 + "only";
							  
							 map.put("mr",actMrList.get(i));
							 map.put("brhAdd",branch.getBranchAdd());
							 map.put("brhCode",branch.getBranchFaCode());
							 map.put("brhName",branch.getBranchName());
							 map.put("custName",customer.getCustName());
							 map.put("mrAmt",str3);
							 
							 Map<String,Object> blDetMap = new HashMap<String, Object>();
							 
							 if(actMrList.get(i).getBill() != null){
								 blDetMap.put("billNo",actMrList.get(i).getBill().getBlBillNo());
								 blDetMap.put("billDt",actMrList.get(i).getBill().getBlBillDt());
								 blDetMap.put("frtAmt",actMrList.get(i).getMrFreight());
								 blDetMap.put("tdsAmt",actMrList.get(i).getMrTdsAmt());
								 blDetMap.put("excAmt",actMrList.get(i).getMrAccessAmt());
								 blDetMap.put("dedAmt",actMrList.get(i).getMrDedAmt());
								 blDetMap.put("recAmt",actMrList.get(i).getMrNetAmt());
							 }
							
							 List<Map<String,Object>> blDetList = new ArrayList<Map<String,Object>>();
							 
							 blDetList.add(blDetMap);
							 
							 map.put("blDetList",blDetList);
							 
							 mrList.add(map);
						 }
					 }
				 }
			 }
			
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception in getMrFrPrint"+e);
		}finally{
			session.clear();
			session.close();
		}
		return mrList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> getMrrFrPrint(int brhId, Date mrDt){
		System.out.println("enter into getMrrFrPrint function");
		List<Map<String,Object>> mrList = new ArrayList<>();
		List<MoneyReceipt> actMrList = new ArrayList<MoneyReceipt>();
		Session session=null;
		try{
			 session = this.sessionFactory.openSession();
			 Criteria cr = session.createCriteria(MoneyReceipt.class);
			 cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID,brhId));
			 cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_Date,mrDt));
			 actMrList = cr.list();
			 
			 if(!actMrList.isEmpty()){
				// NumToWords w = new NumToWords();
				 for(int i=0;i<actMrList.size();i++){
					 
					 if(!mrList.isEmpty()){
						 boolean duplicate = false;
						 for(int j=0;j<mrList.size();j++){
							 Map<String,Object> map = mrList.get(j);
							 MoneyReceipt prevMr = (MoneyReceipt) mrList.get(j).get("mr");
							 if(actMrList.get(i).getMrNo().equalsIgnoreCase(prevMr.getMrNo())){
								 prevMr.setMrDedAmt(prevMr.getMrDedAmt() + actMrList.get(i).getMrDedAmt());
								 prevMr.setMrFreight(prevMr.getMrFreight() + actMrList.get(i).getMrFreight());
								 prevMr.setMrNetAmt(prevMr.getMrNetAmt() + actMrList.get(i).getMrNetAmt());
								 prevMr.setMrSrvTaxAmt(prevMr.getMrSrvTaxAmt() + actMrList.get(i).getMrSrvTaxAmt());
								 prevMr.setMrTdsAmt(prevMr.getMrTdsAmt() + actMrList.get(i).getMrTdsAmt());
								 
								 map.remove("mr");
								 map.put("mr",prevMr);
								 mrList.remove(j);
								 mrList.add(j, map);
								 
								 duplicate = true;
								 break;
							 }
						 }
						 
						 if(!duplicate){
							 Map<String,Object> map = new HashMap<String, Object>();
							 Branch branch = (Branch) session.get(Branch.class,actMrList.get(i).getMrBrhId());
							 Customer customer = (Customer) session.get(Customer.class,actMrList.get(i).getMrCustId());
							 if(actMrList.get(i).getMrBnkId() > 0){
								 BankMstr bank = (BankMstr)session.get(BankMstr.class, actMrList.get(i).getMrBnkId());
								 map.put("bnkName",bank.getBnkName());
							 }
							  
							 map.put("mr",actMrList.get(i));
							 map.put("brhAdd",branch.getBranchAdd());
							 map.put("brhCode",branch.getBranchFaCode());
							 map.put("brhName",branch.getBranchName());
							 map.put("custName",customer.getCustName());
							 
							 mrList.add(map);
						 }
					 }else{
						 Map<String,Object> map = new HashMap<String, Object>();
						 Branch branch = (Branch) session.get(Branch.class,actMrList.get(i).getMrBrhId());
						 Customer customer = (Customer) session.get(Customer.class,actMrList.get(i).getMrCustId());
						 if(actMrList.get(i).getMrBnkId() > 0){
							 BankMstr bank = (BankMstr)session.get(BankMstr.class, actMrList.get(i).getMrBnkId());
							 map.put("bnkName",bank.getBnkName());
						 }
						 /*String amt = String.valueOf(actMrList.get(i).getMrNetAmt());
						 int rupees = Integer.parseInt(amt.split("\\.")[0]);
						 String str1 = w.convert(rupees);
						 str1 += " Rupees ";
						 int paise = Integer.parseInt(amt.split("\\.")[1]);
						 String str2 = "";
						 if (paise != 0) {
							   str2 += " and";
							   str2 = w.convert(paise);
							   str2 += " Paise";
						 }
						 String str3 = str1 + str2 + "only";*/
						  
						 map.put("mr",actMrList.get(i));
						 map.put("brhAdd",branch.getBranchAdd());
						 map.put("brhCode",branch.getBranchFaCode());
						 map.put("brhName",branch.getBranchName());
						 map.put("custName",customer.getCustName());
						 //map.put("mrAmt",str3);
						 
						 /*if(actMrList.get(i).getBill() != null){
							 map.put("billNo",actMrList.get(i).getBill().getBlBillNo());
							 map.put("billDt",actMrList.get(i).getBill().getBlBillDt());
						 }*/
						 
						 mrList.add(map);
					 }
				 }
			 }
			
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception in getMrrFrPrint "+e);
		}finally{
			session.clear();
			session.close();
		}
		return mrList;
	}
	
	
	@SuppressWarnings("unchecked")
	public int setOnAccRemAmt(){
		System.out.println("enter into setOnAccRemAmt function");
		int res = 0;
		Session session=null;
		Transaction transaction=null;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
					
			//flush
			List<MoneyReceipt> mrOnList = new ArrayList<MoneyReceipt>(); 
			Integer id[] = new Integer[]{2360,1749,724,428};
			Criteria cr = session.createCriteria(MoneyReceipt.class);
			cr.add(Restrictions.in(MoneyReceiptCNTS.MR_ID,id));
			mrOnList = cr.list();
			
			if(!mrOnList.isEmpty()){
				for(int i=0;i<mrOnList.size();i++){
					MoneyReceipt moneyReceipt = mrOnList.get(i);
					if(moneyReceipt.getMrFrPDList().size() > 1){
						moneyReceipt.getMrFrPDList().remove(0);
					}
					moneyReceipt.setMrRemAmt(0);
					session.merge(moneyReceipt);
				}
				MoneyReceipt moneyReceipt = (MoneyReceipt) session.get(MoneyReceipt.class,1642);
				moneyReceipt.setMrRemAmt(0);
				moneyReceipt.setMrCancel(true);
				
				MoneyReceipt moneyReceipt1 = (MoneyReceipt) session.get(MoneyReceipt.class,1652);
				moneyReceipt1.setMrRemAmt(0);
				moneyReceipt1.setMrFrPDList(moneyReceipt.getMrFrPDList());
				
				session.merge(moneyReceipt1);
				session.merge(moneyReceipt);
				//session.delete(moneyReceipt);
				//transaction.commit();
			}
			
			List<MoneyReceipt> mrList = new ArrayList<MoneyReceipt>(); 
			cr = session.createCriteria(MoneyReceipt.class);
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE,"On Accounting"));
			mrList = cr.list();
			
			if(!mrList.isEmpty()){
				for(int i=0;i<mrList.size();i++){
					MoneyReceipt moneyReceipt = mrList.get(i);
					List<Map<String,Object>> mapList = new ArrayList<Map<String,Object>>();
					mapList = moneyReceipt.getMrFrPDList();
					if(!mapList.isEmpty()){
						double detAmt = 0;
						for(int j=0;j<mapList.size();j++){
							detAmt = detAmt + Double.parseDouble(String.valueOf(mapList.get(j).get("detAmt"))); 
						}
						moneyReceipt.setMrRemAmt(moneyReceipt.getMrNetAmt() - detAmt);
					}else{
						moneyReceipt.setMrRemAmt(moneyReceipt.getMrNetAmt());
					}
					
					if(moneyReceipt.getMrOthMrAmt() > 0){
						moneyReceipt.setMrRemAmt(0);
					}
					
					session.merge(moneyReceipt);
					
					List<Map<String,Object>> mapList1 = new ArrayList<Map<String,Object>>();
					mapList1 = moneyReceipt.getMrFrPDList();
					
					if(!mapList1.isEmpty()){
						for(int j=0;j<mapList1.size();j++){
							Map<String,Object> map = mapList1.get(j);
							List<MoneyReceipt> pdmrList = new ArrayList<MoneyReceipt>();
							cr = session.createCriteria(MoneyReceipt.class);
							cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE,"Payment Detail"));
							cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_NO,String.valueOf(map.get("pdMr"))));
							cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID,Integer.parseInt(String.valueOf(map.get("brhId")))));
							pdmrList = cr.list();
							if(!pdmrList.isEmpty()){
								for(int k=0;k<pdmrList.size();k++){
									MoneyReceipt pdmr = pdmrList.get(k);
									/*List<Map<String,Object>> mapList2 = new ArrayList<Map<String,Object>>();
									mapList2 = pdmr.getMrFrPDList();*/
									if(!pdmr.getMrFrPDList().isEmpty()){
										for(int l=0;l<pdmr.getMrFrPDList().size();l++){
											if(String.valueOf(pdmr.getMrFrPDList().get(l).get("onAccMr")).equalsIgnoreCase(moneyReceipt.getMrNo())){
												pdmr.getMrFrPDList().get(l).put("brhId",Integer.parseInt(String.valueOf(map.get("brhId"))));
												break;
											}
										}
										session.merge(pdmr);
									}
								}
							}
						}
					}
					
				}
				
				
				transaction.commit();
				res = 1;
				
				
				/*List<MoneyReceipt> mrList1 = new ArrayList<MoneyReceipt>(); 
				cr = session.createCriteria(MoneyReceipt.class);
				cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE,"Payment Detail"));
				cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID,20));
				mrList1 = cr.list();*/
				
				/*List<MoneyReceipt> mrList2 = new ArrayList<MoneyReceipt>(); 
				cr = session.createCriteria(MoneyReceipt.class);
				cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE,"Payment Detail"));
				//cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID,1));
				mrList2 = cr.list();*/
				
				/*if(!mrList1.isEmpty()){
					for(int i=0;i<mrList1.size();i++){
						if(mrList1.get(i).getMrFrPDList().size() < 1 && mrList1.get(i).getMrNetAmt() != mrList1.get(i).getMrRemAmt()
								&& mrList1.get(i).getMrOthMrAmt() < 1){
							System.out.println("########## "+mrList1.get(i).getMrNo()+" ########## "+mrList1.get(i).getMrBrhId());
						}
					}
				}*/
				
				
				/*if(!mrList1.isEmpty()){
					for(int i=0;i<mrList1.size();i++){
						MoneyReceipt moneyReceipt = mrList1.get(i);
						System.out.println("pdMr = "+moneyReceipt.getMrNo());
						System.out.println(moneyReceipt.getMrFrPDList());
						System.out.println();
						System.out.println();System.out.println();
					}
				}*/
/*				int countCan = 0;
				List<MoneyReceipt> duplicaList = new ArrayList<MoneyReceipt>();
				List<MoneyReceipt> duplicaList1 = new ArrayList<MoneyReceipt>();
				if(!mrList2.isEmpty()){
					for(int i=0;i<mrList2.size();i++){
						//System.out.println("#####Payment Detail = "+mrList2.get(i).getMrNo()+"("+mrList2.get(i).getMrBrhId()+")");
						for(int j=0;j<mrList2.get(i).getMrFrPDList().size();j++){
							List<MoneyReceipt> onMrList = new ArrayList<MoneyReceipt>();
							cr = session.createCriteria(MoneyReceipt.class);
							cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE,"On Accounting"));
							cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_NO,mrList2.get(i).getMrFrPDList().get(j).get("onAccMr")));
							cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_IS_CANCEL,true));
							//System.out.println("###On Acc = "+mrList2.get(i).getMrFrPDList().get(j).get("onAccMr")+" ### "+mrList2.get(i).getMrFrPDList().get(j).get("detAmt")+" ###");
							onMrList = cr.list();
							if(!onMrList.isEmpty()){
								for(int k=0;k<onMrList.size();k++){
									countCan = countCan + 1;
									duplicaList.add(onMrList.get(k));
									duplicaList1.add(mrList2.get(i));
									System.out.println("#####Payment Detail = "+mrList2.get(i).getMrNo()+"("+mrList2.get(i).getMrBrhId()+")");
								}
							}
						}
						System.out.println("");
						System.out.println("");System.out.println("");System.out.println("");
						System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%");
					}
				}
					System.out.println("countCan = "+countCan);
					for(int k=0;k<duplicaList1.size();k++){
						System.out.println("#####Payment Detail = "+duplicaList1.get(k).getMrNo()+"("+duplicaList1.get(k).getMrBrhId()+")   Amt = "+duplicaList1.get(k).getMrNetAmt());
						System.out.println("@@@@ "+duplicaList1.get(k).getMrFrPDList());
					}
					
					System.out.println("^^^^^^^^^^^^^^^^^^^^^^");
					
					for(int k=0;k<duplicaList.size();k++){
						System.out.println("id= "+duplicaList.get(k).getMrId()+" On Acc = "+duplicaList.get(k).getMrNo()+"("+duplicaList.get(k).getMrBrhId()+")--->"+duplicaList.get(k).getMrNetAmt());
					}*/
			}else{
				res = 0;
			}
		}catch(Exception e){
			e.printStackTrace();
			transaction.rollback();
			res = 0;
			logger.info("Exception in setOnAccRemAmt "+e);
		}finally{
			session.clear();
			session.close();
		}
		return res;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	public int setPdmrToOnacc(){
		System.out.println("enter into setPdmrToOnacc function");
		int res = 0;
		List<Map<String,Object>> mrList = new ArrayList<>();
		int countMr = 0;
		Session session=null;
		Transaction transaction=null;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			List<Branch> brhList = new ArrayList<Branch>();
					
			Criteria cr = session.createCriteria(Branch.class);
			brhList = cr.list();
			
			if(!brhList.isEmpty()){
				for(int i=0;i<brhList.size();i++){
					
					List<MoneyReceipt> onAccList1 = new ArrayList<MoneyReceipt>();
					cr = session.createCriteria(MoneyReceipt.class);
					cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID,brhList.get(i).getBranchId()));
					//cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE,"Payment Detail"));
					cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE,"On Accounting"));
					onAccList1 = cr.list();
					
					/*if(!onAccList1.isEmpty()){
						for(int j=0;j<onAccList1.size();j++){
							MoneyReceipt onAcc = onAccList1.get(j);
							//System.out.println("payment detail MR = "+onAcc.getMrNo());
							System.out.println("On Acc MR = "+onAcc.getMrNo());
							List<Map<String,Object>> onAccMapList = onAcc.getMrFrPDList();
							if(!onAccMapList.isEmpty()){
								for(int k=0;k<onAccMapList.size();k++){
									//System.out.println("**********onAcc = "+onAccMapList.get(k).get("onAccMr"));
									System.out.println("**********payDet = "+onAccMapList.get(k).get("pdMr"));
									System.out.println("**********detAmt = "+onAccMapList.get(k).get("detAmt"));
									System.out.println("----");
								}
							}
							System.out.println();
							System.out.println();
							System.out.println();
							System.out.println("########################");
						}
					}*/
					
					
					if(!onAccList1.isEmpty()){
						for(int j=0;j<onAccList1.size();j++){
							MoneyReceipt onAcc = onAccList1.get(j);
							onAcc.getMrFrPDList().clear();
							
							session.merge(onAcc);
						}
					}
					
					
					
					System.out.println("BRANCH = "+brhList.get(i).getBranchName());
					List<MoneyReceipt> pdmrList = new ArrayList<MoneyReceipt>();
					cr = session.createCriteria(MoneyReceipt.class);
					cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID,brhList.get(i).getBranchId()));
					cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE,"Payment Detail"));
					pdmrList = cr.list();
					
					
					if(!pdmrList.isEmpty()){
						
						for(int m=0;m<pdmrList.size();m++){
							int count = 0;
							for(int n=0;n<pdmrList.size();n++){
								if(pdmrList.get(m).getMrNo().equalsIgnoreCase(pdmrList.get(n).getMrNo())){
									count = count + 1;
								}
							}
							
							if(count > 1){
								pdmrList.remove(m);
								m = m - 1;	
							}
						}
						
						for(int j=0;j<pdmrList.size();j++){
							List<Map<String,Object>> onAccMapList = new ArrayList<Map<String,Object>>();
							onAccMapList = pdmrList.get(j).getMrFrPDList();
							if(!onAccMapList.isEmpty()){
								for(int k=0;k<onAccMapList.size();k++){
									System.out.println("^^^^^^^^^^^^^^^^ "+onAccMapList.get(k).get("onAccMr"));
									List<MoneyReceipt> onAccList = new ArrayList<MoneyReceipt>();
									 cr = session.createCriteria(MoneyReceipt.class);
									 cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID,pdmrList.get(j).getMrBrhId()));
									 cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE,"On Accounting"));
									 cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID,pdmrList.get(j).getMrCustId()));
									 cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_NO,String.valueOf(onAccMapList.get(k).get("onAccMr"))));
									 onAccList = cr.list();
									 
									 System.out.println("onAccListSize============>>>>"+onAccList.size());
									 
									 if(!onAccList.isEmpty()){
										 countMr = countMr + 1;
										 
										 MoneyReceipt onAcc = onAccList.get(0);
										 Map<String,Object> map = new HashMap<String,Object>();
										 map.put("pdMr", pdmrList.get(j).getMrNo());
										 map.put("brhId", pdmrList.get(j).getMrBrhId());
										 map.put("detAmt",Double.parseDouble(String.valueOf(onAccMapList.get(k).get("detAmt"))));
										 
										 onAcc.getMrFrPDList().add(map);
										 
										 if(!onAcc.getMrFrPDList().isEmpty()){
											 double detAmt = 0.0;
											 for(int z=0;z<onAcc.getMrFrPDList().size();z++){
												 detAmt = detAmt + Double.parseDouble(String.valueOf(onAcc.getMrFrPDList().get(z).get("detAmt")));
											 }
											 onAcc.setMrRemAmt(onAcc.getMrNetAmt() - detAmt);
										 }
										 
										 System.out.println("pdmrList.get(j).getMrNo() = "+pdmrList.get(j).getMrNo());
										 System.out.println("***********************");
										 System.out.println("Double.parseDouble(String.valueOf(onAccMapList.get(k).get(detAmt))) = "+Double.parseDouble(String.valueOf(onAccMapList.get(k).get("detAmt"))));
										 System.out.println("onAccNo$$$$$$$: "+onAcc.getMrNo());
										 session.merge(onAcc);
										 System.out.println("onAccId$$$$$$$: "+onAcc.getMrId());
									 }else{
										 
										 
										 List<MoneyReceipt> onAccList11 = new ArrayList<MoneyReceipt>();
										 cr = session.createCriteria(MoneyReceipt.class);
										 cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_NET_AMT,Double.parseDouble(String.valueOf(onAccMapList.get(k).get("detAmt")))));
										 cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE,"On Accounting"));
										 cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID,pdmrList.get(j).getMrCustId()));
										 cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_NO,String.valueOf(onAccMapList.get(k).get("onAccMr"))));
										 cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_IS_CANCEL,false));
										 onAccList11 = cr.list();
										 
										 if(!onAccList11.isEmpty()){
											 MoneyReceipt onAcc = onAccList11.get(0);
											 Map<String,Object> map = new HashMap<String,Object>();
											 map.put("pdMr", pdmrList.get(j).getMrNo());
											 map.put("brhId", pdmrList.get(j).getMrBrhId());
											 map.put("detAmt",Double.parseDouble(String.valueOf(onAccMapList.get(k).get("detAmt"))));
											 
											 onAcc.getMrFrPDList().add(map);
											 
											 if(!onAcc.getMrFrPDList().isEmpty()){
												 double detAmt = 0.0;
												 for(int z=0;z<onAcc.getMrFrPDList().size();z++){
													 detAmt = detAmt + Double.parseDouble(String.valueOf(onAcc.getMrFrPDList().get(z).get("detAmt")));
												 }
												 onAcc.setMrRemAmt(onAcc.getMrNetAmt() - detAmt);
											 }
											 
											 System.out.println("pdmrList.get(j).getMrNo() = "+pdmrList.get(j).getMrNo());
											 System.out.println("***********************");
											 System.out.println("Double.parseDouble(String.valueOf(onAccMapList.get(k).get(detAmt))) = "+Double.parseDouble(String.valueOf(onAccMapList.get(k).get("detAmt"))));
											 System.out.println("onAccNo$$$$$$$: "+onAcc.getMrNo());
											 session.merge(onAcc);
											 System.out.println("onAccId$$$$$$$: "+onAcc.getMrId());
										 }else{
											 Map<String,Object> map = new HashMap<String, Object>();
											 map.put("onAcc",String.valueOf(onAccMapList.get(k).get("onAccMr")));
											 map.put("pdMr",pdmrList.get(j));
											 map.put("detAmt",onAccMapList.get(k).get("detAmt"));
											 mrList.add(map);
										 }
										 
									 }
								}
							}
						}
					}
				}
				transaction.commit();
				res = 1;
			}else{
				System.out.println("#################################");
			}
			
		}catch(Exception e){
			e.printStackTrace();
			transaction.rollback();
			res = 0;
			logger.info("Excption in setPdmrToOnacc "+e);
		}finally{
			session.clear();
			session.close();
		}
		System.out.println("countMr = "+countMr);
		System.out.println("size of mrList  = "+mrList.size());
		for(int i=0;i<mrList.size();i++){
			MoneyReceipt pdmr = (MoneyReceipt) mrList.get(i).get("pdMr");
			System.out.println("@@@@@@@@@@@ "+mrList.get(i).get("onAcc")+" ------>pdmr="+pdmr.getMrNo()+" ("+pdmr.getMrBrhId()+")"+" == "+mrList.get(i).get("detAmt"));
		}
		return res;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public int duplicateMR(MoneyReceipt moneyReceipt){
		System.out.println("enter into duplicateMR function");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		int res = 0;
		Session session=null;
		try{
			session = this.sessionFactory.openSession();
			System.out.println("moneyReceipt.getMrCustId() = "+moneyReceipt.getMrCustId());
			System.out.println("moneyReceipt.getMrDate() = "+moneyReceipt.getMrDate());
			System.out.println("moneyReceipt.getMrNetAmt() = "+moneyReceipt.getMrNetAmt());
			
			List<MoneyReceipt> mrList = new ArrayList<MoneyReceipt>();
			Criteria cr = session.createCriteria(MoneyReceipt.class);
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID,Integer.parseInt(currentUser.getUserBranchCode())));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID,moneyReceipt.getMrCustId()));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_Date,moneyReceipt.getMrDate()));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_NET_AMT,moneyReceipt.getMrNetAmt()));
			mrList = cr.list();
			System.out.println("size of mrList = "+mrList);
			
			if(!mrList.isEmpty()){
				res = 1;
			}else{
				res = 0;
			}
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		   logger.info("Exception in duplicateMR"+e);
		}finally{
			session.clear();
			session.close();
		}
		return res;
	}
	
	
	@SuppressWarnings("unchecked")
	public int duplicateDPMR(List<MoneyReceipt> mrList){
		System.out.println("enter into duplicateDPMR function");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		int res = 0;
		Session session=null;
		try{
			session = this.sessionFactory.openSession();
			List<MoneyReceipt> dpmrList = new ArrayList<MoneyReceipt>();
			Criteria cr = session.createCriteria(MoneyReceipt.class);
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID,Integer.parseInt(currentUser.getUserBranchCode())));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID,mrList.get(0).getMrCustId()));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_Date,mrList.get(0).getMrDate()));
			dpmrList = cr.list();
			
			double mrAmt = 0.0;
			for(int i=0;i<mrList.size();i++){
				mrAmt = mrAmt + mrList.get(i).getMrNetAmt(); 
			}
			
			boolean duplicate = false;
			
			System.out.println("size of dpmrList = "+dpmrList.size());
			System.out.println("mrAmt = "+mrAmt);
			
			if(!dpmrList.isEmpty()){
				for(int i=0;i<dpmrList.size();i++){
					double amt = 0.0;
					for(int j=0;j<dpmrList.size();j++){
						if(dpmrList.get(i).getMrNo().equalsIgnoreCase(dpmrList.get(j).getMrNo())){
							amt = amt + dpmrList.get(j).getMrNetAmt();
						}
					}
					
					if(amt == mrAmt){
						duplicate = true;
						break;
					}
				}
				
				if(duplicate){
					res = 1;
				}else{
					res = 0;
				}
			}else{
				res = 0;
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception in duplicateDPMR"+e);
		}finally{
			session.clear();
			session.close();
		}
		return res;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public Map<String,Object> cancelOnAcc(int brhId, String mrNo){
		System.out.println("enter into cancelOnAcc function");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		Map<String,Object> map = new HashMap<String, Object>();
		Session session = null;
		Transaction transaction = null;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			List<MoneyReceipt> oamrList = new ArrayList<MoneyReceipt>();
			Criteria cr = session.createCriteria(MoneyReceipt.class);
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID,brhId));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_NO,mrNo));
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE,"On Accounting"));
			oamrList = cr.list();
			
			if(!oamrList.isEmpty()){
				MoneyReceipt oaMr = oamrList.get(0);
				if(oaMr.isMrCancel()){
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					map.put("msg",mrNo+" On Account MR already canceled");
				}else if(oaMr.getMrFrPDList().size() > 0) {
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					map.put("msg","You can not cancel the "+mrNo+" because you entered the payment detail");
				}else{
					cr = session.createCriteria(CashStmtStatus.class);        
					cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,String.valueOf(brhId)));
					cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT,oaMr.getMrDate()));
					List<CashStmtStatus> cssList = cr.list();
					
					if(!cssList.isEmpty()){
						List<CashStmt> csList = cssList.get(0).getCashStmtList();
						
						System.out.println("size====>>"+csList.size());
						
						double revBnkAmt = 0.0;
						String bnkFaCode = "";
						int vouchNo = 1;
						List<CashStmt> newCsList = new ArrayList<CashStmt>();
						
						if(!csList.isEmpty()){
							for(int i=0;i<csList.size();i++){
								System.out.print("csList.get(i).getCsTvNo() ==>>"+csList.get(i).getCsTvNo()+"\t");
								System.out.print("oaMr.getMrNo()==>>"+oaMr.getMrNo()+"\t");
								System.out.print("i==>>"+i+"\n");
								
								if(vouchNo < csList.get(i).getCsVouchNo()){
									vouchNo = csList.get(i).getCsVouchNo();
								}
							String temp=csList.get(i).getCsTvNo();
							    int mrSize=oaMr.getMrNo().length();
							   if(temp.length()>=mrSize){
								   temp=temp.substring(0, mrSize);
							   }
								
								if(temp.equalsIgnoreCase(oaMr.getMrNo())){
									System.out.println("****************************");
									CashStmt cashStmt = new CashStmt();
									cashStmt.setbCode(currentUser.getUserBranchCode());
									cashStmt.setCsAmt(csList.get(i).getCsAmt());
									cashStmt.setCsChequeType(csList.get(i).getCsChequeType());
									cashStmt.setPayMode(csList.get(i).getPayMode());
									System.out.println("csList.get(i).getCsDescription() = "+csList.get(i).getCsDescription());
									cashStmt.setCsDescription("CANCEL "+csList.get(i).getCsDescription());
									
									if(csList.get(i).getCsDrCr() == 'C'){
										cashStmt.setCsDrCr('D');
									}else{
										cashStmt.setCsDrCr('C');
									}
									cashStmt.setCsDt(new Date(new java.util.Date().getTime()));
									cashStmt.setCsFaCode(csList.get(i).getCsFaCode());
									
									if(csList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
										System.out.println("^^^^^^^^^^^csList.get(i).getCsFaCode() = "+csList.get(i).getCsFaCode());
										revBnkAmt = revBnkAmt + csList.get(i).getCsAmt();
										bnkFaCode = csList.get(i).getCsFaCode();
									}
									
									cashStmt.setCsIsClose(csList.get(i).isCsIsClose());
									cashStmt.setCsIsVRev(csList.get(i).isCsIsVRev());
									cashStmt.setCsLhpvTempNo(csList.get(i).getCsLhpvTempNo());
									cashStmt.setCsMrNo(csList.get(i).getCsMrNo());
									cashStmt.setCsPayTo(csList.get(i).getCsPayTo());
									cashStmt.setCsSFId(csList.get(i).getCsSFId());
									cashStmt.setCsTravIdList(csList.get(i).getCsTravIdList());
									cashStmt.setCsTvNo(csList.get(i).getCsTvNo());
									cashStmt.setCsType(csList.get(i).getCsType());
									cashStmt.setCsUpdate(false);
									//cashStmt.setCsVouchNo(csList.get(i).getCsVouchNo());
									cashStmt.setCsVouchType(csList.get(i).getCsVouchType());
									cashStmt.setUserCode(currentUser.getUserCode());
									
									/*CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,cssList.get(0).getCssId());
									cashStmt.setCsNo(actCss);
									actCss.getCashStmtList().add(cashStmt);
									session.merge(actCss);*/
									
									newCsList.add(cashStmt);
								}
							}
							
							
							if(!newCsList.isEmpty()){
								System.out.println("size of newCsList = "+newCsList.size());
								for(int i=0;i<newCsList.size();i++){      
									CashStmt cashStmt = newCsList.get(i); 
									cashStmt.setCsVouchNo(vouchNo + 1);
									
									CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,cssList.get(0).getCssId());
									cashStmt.setCsNo(actCss);
									actCss.getCashStmtList().add(cashStmt);
									session.merge(actCss);
								}
							}
							
							System.out.println("revBnkAmt = "+revBnkAmt);
							System.out.println("bnkFaCode = "+bnkFaCode);
							
							cr = session.createCriteria(BankCS.class);
							cr.add(Restrictions.eq(BankCSCNTS.BCS_BNK_CODE,bnkFaCode));
							cr.add(Restrictions.ge(BankCSCNTS.BCS_DT,oaMr.getMrDate()));
							List<BankCS> bankCsList = cr.list();
							
							System.out.println("Size of bankCsList = "+bankCsList);
							if(!bankCsList.isEmpty()){
								int bnkId = bankCsList.get(0).getBcsBankId();
								if(bankCsList.size() == 1){
									BankCS bankCs = bankCsList.get(0);
									bankCs.setBcsOpenBal(bankCs.getBcsOpenBal() - revBnkAmt);
									session.merge(bankCs);
									System.out.println("%%%%%%%%%%%%% 1");
								}else if(bankCsList.size() == 2){
									BankCS bankCs = bankCsList.get(0);
									bankCs.setBcsCloseBal(bankCs.getBcsCloseBal() - revBnkAmt);
									session.merge(bankCs);
									System.out.println("%%%%%%%%%%%%% 1");
									
									BankCS bankCs2 = bankCsList.get(bankCsList.size()-1);
									bankCs2.setBcsOpenBal(bankCs2.getBcsOpenBal() - revBnkAmt);
									session.merge(bankCs2);
									System.out.println("%%%%%%%%%%%%% 2");
								}else{
									
									BankCS bankCs = bankCsList.get(0);
									bankCs.setBcsCloseBal(bankCs.getBcsCloseBal() - revBnkAmt);
									session.merge(bankCs);
									System.out.println("%%%%%%%%%%%%% 1");
									
									if(bankCsList.size()>2){
										for(int i=1;i<bankCsList.size()-1;i++){
											BankCS bankCs1 = bankCsList.get(i);
											bankCs1.setBcsOpenBal(bankCs1.getBcsOpenBal() - revBnkAmt);
											bankCs1.setBcsCloseBal(bankCs1.getBcsCloseBal() - revBnkAmt);
											session.merge(bankCs1);
											System.out.println("%%%%%%%%%%%%% 3");
										}
									}
									
									BankCS bankCs2 = bankCsList.get(bankCsList.size()-1);
									bankCs2.setBcsOpenBal(bankCs2.getBcsOpenBal() - revBnkAmt);
									session.merge(bankCs2);
									System.out.println("%%%%%%%%%%%%% 2");
								}
								
								BankMstr bankMstr = (BankMstr) session.get(BankMstr.class,bnkId);
								bankMstr.setBnkBalanceAmt(bankMstr.getBnkBalanceAmt() - revBnkAmt);
								session.merge(bankMstr);
								System.out.println("%%%%%%%%%%%%% 4");
							}
							
							oaMr.setMrCancel(true);
							session.merge(oaMr);
							
							transaction.commit();
							map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
							map.put("msg","Successfully cancel the MR "+mrNo);
						}else{
							map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
							map.put("msg","Branch doesn't have CS of "+oaMr.getMrDate());
						}
						
					}else{
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
						map.put("msg","Branch doesn't have CS of "+oaMr.getMrDate());
					}
					
				}
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				map.put("msg",mrNo+" On Account MR doesn't exist");
			}
			
		}catch(Exception e){
			transaction.rollback();
			logger.error(e.getMessage(), e);
		}finally{
			session.clear();
			session.close();
		}
		return map;
	}
	
	
	@SuppressWarnings("unchecked")
	public int chkAccuracy(){
		System.out.println("enter into chkAccuracy function");
		int res = 0;
		Session session=null;
		try{
			session = this.sessionFactory.openSession();
			
			List<MoneyReceipt> onAccList = new ArrayList<MoneyReceipt>();
			Criteria cr = session.createCriteria(MoneyReceipt.class);
			cr.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE,"On Accounting"));
			onAccList = cr.list();
			int count = 0;
			if(!onAccList.isEmpty()){
				for(int i=0;i<onAccList.size();i++){
					List<Map<String,Object>> mapList = new ArrayList<Map<String,Object>>();
					mapList = onAccList.get(i).getMrFrPDList();
					if(!mapList.isEmpty()){
						for(int j=0;j<mapList.size();j++){
							if(String.valueOf(mapList.get(j).get("brhId")) == null){
								System.out.println("onAccList.get("+i+") = "+onAccList.get(i).getMrNo()+"("+onAccList.get(i).getMrBrhId()+")");
								System.out.println("payDet = "+mapList.get(j).get("pdMr")+"    Amt = "+mapList.get(j).get("detAmt"));
							}
						}
					}
					System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
				}
				res = 1;
			}
					
		}catch(Exception e){
			e.printStackTrace();
			logger.info("exception in chkAccuracy"+e);
		}finally{
			session.clear();
			session.close();
		}
		return res;
	}
	
	@Transactional
	@Override
	public MoneyReceipt getLastMoenyReceipt(int userBranchCode) {
		MoneyReceipt moneyReceipt 		= 	null;
		Criteria moneyReceiptCriteria 	= 	this.sessionFactory.getCurrentSession().createCriteria(MoneyReceipt.class);
		
		moneyReceiptCriteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID, userBranchCode));
		moneyReceiptCriteria.addOrder(Order.desc(MoneyReceiptCNTS.MR_ID));
		moneyReceiptCriteria.setMaxResults(1);
		
		List<MoneyReceipt> moneyReceipts = moneyReceiptCriteria.list();
		
		if (!moneyReceipts.isEmpty())
			moneyReceipt = moneyReceipts.get(0);
		
		return moneyReceipt;
	}
	
	@Transactional
	@Override
	public void saveOAMRN(MoneyReceipt moneyReceipt) {
		this.sessionFactory.getCurrentSession().save(moneyReceipt);
	}
	
	
	@Override
	public Map removeBillFromSrtExs(int custId,String billNo) {
		Map<String,String> map=new HashMap<>();
		Session session=null;
		Transaction transaction=null;
		try {
			session=this.sessionFactory.openSession();
			transaction=session.beginTransaction();
			List<Bill> list=session.createCriteria(Bill.class)
					.add(Restrictions.eq(BillCNTS.BILL_CUST_ID, custId))
					.add(Restrictions.eq(BillCNTS.Bill_NO, billNo)).list();
			if(!list.isEmpty()) {
				SQLQuery query=session.createSQLQuery("select mrId from mr_bill where blId=:blId");
				query.setInteger("blId", list.get(0).getBlId());
				int mrId=(int) query.list().get(0);
				
				MoneyReceipt mr=(MoneyReceipt) session.get(MoneyReceipt.class, mrId);
				mr.setMrSrtExsDis(true);
				session.update(mr);
				
				map.put("result", "success");

			}else {
				map.put("result", "error");
				map.put("msg", "MR Not Exist");
			}
			transaction.commit();
		}catch(Exception e) {
			transaction.rollback();
			e.printStackTrace();
			map.put("result", "error");
			map.put("msg", "Some problem");
			logger.info("Exception in removeBillFromSrtExs()"+e);
		}finally {
			session.clear();
			session.close();
		}
		
		return map;
	}
	
}

package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.RevVoucherDAO;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.FaBPromCNTS;
import com.mylogistics.constants.FaCarDivisionCNTS;
import com.mylogistics.constants.FaTravDivisionCNTS;
import com.mylogistics.constants.SubElectricityMstrCNTS;
import com.mylogistics.constants.SubTelephoneMstrCNTS;
import com.mylogistics.constants.SubVehicleMstrCNTS;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.Customer;
import com.mylogistics.model.ElectricityMstr;
import com.mylogistics.model.Employee;
import com.mylogistics.model.FaBProm;
import com.mylogistics.model.FaCarDivision;
import com.mylogistics.model.FaTravDivision;
import com.mylogistics.model.SubElectricityMstr;
import com.mylogistics.model.SubTelephoneMstr;
import com.mylogistics.model.SubVehicleMstr;
import com.mylogistics.model.TelephoneMstr;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleMstr;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.services.ConstantsValues;

public class RevVoucherDAOImpl implements RevVoucherDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	private HttpSession httpSession;
	
	
	@Autowired
	public RevVoucherDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int revVoucher(List<CashStmt> reqCsList){
		int result = 0;
		User currentUser = (User) httpSession.getAttribute("currentUser");
		if(!reqCsList.isEmpty()){
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				List<CashStmt> csList1 = new ArrayList<CashStmt>();
				Criteria cr1 = session.createCriteria(CashStmt.class);
				cr1.add(Restrictions.eq(CashStmtCNTS.CS_ID,reqCsList.get(0).getCsId()));
				csList1 = cr1.list();
				
				CashStmt cs1 = csList1.get(0);
				CashStmtStatus cashStmtStatus1 = cs1.getCsNo();
				CashStmt cashStmt1 = new CashStmt();
				
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus1.getCssDt().getTime());
				Criteria cr2 = session.createCriteria(CashStmt.class);
				cr2.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
				cr2.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,cs1.getbCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("csVouchNo"));
				cr1.setProjection(proList);
				
				List<Integer>  voucherNoList = cr1.list();
				int vNo = voucherNoList.get(voucherNoList.size() - 1);
				int voucherNo = vNo + 1;
				
				
				for(int i=0;i<reqCsList.size();i++){
					List<CashStmt> csList = new ArrayList<CashStmt>();
					Criteria cr = session.createCriteria(CashStmt.class);
					cr.add(Restrictions.eq(CashStmtCNTS.CS_ID,reqCsList.get(i).getCsId()));
					csList = cr.list();
					
					CashStmt cs = csList.get(0);
					CashStmtStatus cashStmtStatus = cs.getCsNo();
					CashStmt cashStmt = new CashStmt();
					
					cs.setCsIsVRev(true);
					cashStmt.setCsIsVRev(true);
					cashStmt.setbCode(cs.getbCode());
					cashStmt.setCsAmt(cs.getCsAmt());
					cashStmt.setCsChequeType(cs.getCsChequeType());
					cashStmt.setCsDescription("CANCEL " + cs.getCsDescription());
					if(cs.getCsDrCr() == 'C'){
						cashStmt.setCsDrCr('D');
					}else{
						cashStmt.setCsDrCr('C');
					}
					cashStmt.setCsDt(cs.getCsDt());
					cashStmt.setCsFaCode(cs.getCsFaCode());
					cashStmt.setCsIsClose(cs.isCsIsClose());
					cashStmt.setCsMrNo(cs.getCsMrNo());
					cashStmt.setCsNo(cashStmtStatus);
					cashStmt.setCsPayTo(cs.getCsPayTo());
					cashStmt.setCsTvNo(cs.getCsTvNo());
					cashStmt.setCsType(ConstantsValues.REV_VOUCHER);
					cashStmt.setCsVouchNo(voucherNo);
					cashStmt.setCsVouchType(cs.getCsVouchType());
					cashStmt.setUserCode(currentUser.getUserCode());
					cashStmt.setPayMode(cs.getPayMode());
					
					session.update(cs);
					session.save(cashStmt);
					cashStmtStatus.getCashStmtList().add(cashStmt);
					session.update(cashStmtStatus);
					
					
					if(cs.getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
						List<BankMstr> bnkList = new ArrayList<>();
						cr = session.createCriteria(BankMstr.class);
						cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE,cs.getCsFaCode()));
						bnkList = cr.list();
						
						if(!bnkList.isEmpty()){
							BankMstr bankMstr = bnkList.get(0);
							if(cashStmt.getCsDrCr() == 'C'){
								double newBalAmt = bankMstr.getBnkBalanceAmt() - cashStmt.getCsAmt();
								bankMstr.setBnkBalanceAmt(newBalAmt);
								session.merge(bankMstr);
							}else{
								double newBalAmt = bankMstr.getBnkBalanceAmt() + cashStmt.getCsAmt();
								bankMstr.setBnkBalanceAmt(newBalAmt);
								session.merge(bankMstr);
							}			
						}
					}
					
				}
				
				transaction.commit();
				session.flush();
				result = 1;
				
			}catch(Exception e){
				e.printStackTrace();
				result = 0;
			}
			session.clear();
			session.close();
		}else{
			result = -1;
		}
		
		return result;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int revTelVoucher(List<CashStmt> reqCsList){
		System.out.println("enter into revTelVoucher function");
		int result = 0;
		User currentUser = (User) httpSession.getAttribute("currentUser");
		if(!reqCsList.isEmpty()){
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				List<CashStmt> csList1 = new ArrayList<CashStmt>();
				Criteria cr1 = session.createCriteria(CashStmt.class);
				cr1.add(Restrictions.eq(CashStmtCNTS.CS_ID,reqCsList.get(0).getCsId()));
				csList1 = cr1.list();
				
				CashStmt cs1 = csList1.get(0);
				CashStmtStatus cashStmtStatus1 = cs1.getCsNo();
				CashStmt cashStmt1 = new CashStmt();
				
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus1.getCssDt().getTime());
				Criteria cr2 = session.createCriteria(CashStmt.class);
				cr2.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
				cr2.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,cs1.getbCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("csVouchNo"));
				cr1.setProjection(proList);
				
				List<Integer>  voucherNoList = cr1.list();
				int vNo = voucherNoList.get(voucherNoList.size() - 1);
				int voucherNo = vNo + 1;
				
				for(int i=0;i<reqCsList.size();i++){
					List<CashStmt> csList = new ArrayList<CashStmt>();
					Criteria cr = session.createCriteria(CashStmt.class);
					cr.add(Restrictions.eq(CashStmtCNTS.CS_ID,reqCsList.get(i).getCsId()));
					csList = cr.list();
					
					CashStmt cs = csList.get(0);
					CashStmtStatus cashStmtStatus = cs.getCsNo();
					CashStmt cashStmt = new CashStmt();
					
					cs.setCsIsVRev(true);
					cashStmt.setCsIsVRev(true);
					cashStmt.setbCode(cs.getbCode());
					cashStmt.setCsAmt(cs.getCsAmt());
					cashStmt.setCsChequeType(cs.getCsChequeType());
					cashStmt.setCsDescription("CANCEL " + cs.getCsDescription());
					if(cs.getCsDrCr() == 'C'){
						cashStmt.setCsDrCr('D');
					}else{
						cashStmt.setCsDrCr('C');
					}
					cashStmt.setCsDt(cs.getCsDt());
					cashStmt.setCsFaCode(cs.getCsFaCode());
					cashStmt.setCsIsClose(cs.isCsIsClose());
					cashStmt.setCsMrNo(cs.getCsMrNo());
					cashStmt.setCsNo(cashStmtStatus);
					cashStmt.setCsPayTo(cs.getCsPayTo());
					cashStmt.setCsTvNo(cs.getCsTvNo());
					cashStmt.setCsType(ConstantsValues.REV_VOUCHER);
					cashStmt.setCsVouchNo(voucherNo);
					cashStmt.setCsVouchType(cs.getCsVouchType());
					cashStmt.setUserCode(currentUser.getUserCode());
					cashStmt.setPayMode(cs.getPayMode());
					
					session.update(cs);
					int csId = (Integer) session.save(cashStmt);
					
					int newStmtId = 0;
					
					if(cs.getCsSFId() > 0){
						List<SubTelephoneMstr> stmList = new ArrayList<SubTelephoneMstr>();
						Criteria cr3 = session.createCriteria(SubTelephoneMstr.class);
						cr3.add(Restrictions.eq(SubTelephoneMstrCNTS.STM_ID,cs.getCsSFId()));
						
						stmList = cr3.list();
						SubTelephoneMstr stm = stmList.get(0);
						TelephoneMstr telephoneMstr = stm.getTelephoneMstr();
						
						SubTelephoneMstr newStm = new SubTelephoneMstr();
						newStm.setbCode(currentUser.getUserBranchCode());
						newStm.setUserCode(currentUser.getUserCode());
						newStm.setStmCsId(csId);
						newStm.setStmBillFrDt(stm.getStmBillFrDt());
						newStm.setStmBillToDt(stm.getStmBillToDt());
						newStm.setStmCTDedAmt(-stm.getStmCTDedAmt());
						newStm.setStmDDedAmt(-stm.getStmDDedAmt());
						newStm.setStmGDedAmt(-stm.getStmGDedAmt());
						newStm.setStmIsdDedAmt(-stm.getStmIsdDedAmt());
						newStm.setStmPayAmt(-stm.getStmPayAmt());
						newStm.setStmPCDedAmt(-stm.getStmPCDedAmt());
						newStm.setStmRDedAmt(-stm.getStmRDedAmt());
						newStm.setStmSmsDedAmt(-stm.getStmSmsDedAmt());
						newStm.setStmTotDedAmt(-stm.getStmTotDedAmt());
						newStm.setStmTvNo(stm.getStmTvNo());
						newStm.setTelephoneMstr(telephoneMstr);
						
						telephoneMstr.getSubTelephoneMstrList().add(newStm);
						
						newStmtId = (Integer) session.save(newStm);
						session.update(telephoneMstr);
					}
					
					if(newStmtId > 0){
						cashStmt.setCsSFId(newStmtId);
						session.update(cashStmt);
					}
					
					cashStmtStatus.getCashStmtList().add(cashStmt);
					session.update(cashStmtStatus);
				}
				transaction.commit();
				session.flush();
				result = 1;
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
		}else{
			result = -1;
		}
		
		return result;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int revEleVoucher(List<CashStmt> reqCsList){
		System.out.println("enter into revEleVoucher function");
		int result = 0;
		User currentUser = (User) httpSession.getAttribute("currentUser");
		if(!reqCsList.isEmpty()){
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				List<CashStmt> csList1 = new ArrayList<CashStmt>();
				Criteria cr1 = session.createCriteria(CashStmt.class);
				cr1.add(Restrictions.eq(CashStmtCNTS.CS_ID,reqCsList.get(0).getCsId()));
				csList1 = cr1.list();
				
				CashStmt cs1 = csList1.get(0);
				CashStmtStatus cashStmtStatus1 = cs1.getCsNo();
				CashStmt cashStmt1 = new CashStmt();
				
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus1.getCssDt().getTime());
				Criteria cr2 = session.createCriteria(CashStmt.class);
				cr2.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
				cr2.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,cs1.getbCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("csVouchNo"));
				cr1.setProjection(proList);
				
				List<Integer>  voucherNoList = cr1.list();
				int vNo = voucherNoList.get(voucherNoList.size() - 1);
				int voucherNo = vNo + 1;
				
				for(int i=0;i<reqCsList.size();i++){
					List<CashStmt> csList = new ArrayList<CashStmt>();
					Criteria cr = session.createCriteria(CashStmt.class);
					cr.add(Restrictions.eq(CashStmtCNTS.CS_ID,reqCsList.get(i).getCsId()));
					csList = cr.list();
					
					CashStmt cs = csList.get(0);
					CashStmtStatus cashStmtStatus = cs.getCsNo();
					CashStmt cashStmt = new CashStmt();
				
					
					cs.setCsIsVRev(true);
					cashStmt.setCsIsVRev(true);
					cashStmt.setbCode(cs.getbCode());
					cashStmt.setCsAmt(cs.getCsAmt());
					cashStmt.setCsChequeType(cs.getCsChequeType());
					cashStmt.setCsDescription("CANCEL " + cs.getCsDescription());
					if(cs.getCsDrCr() == 'C'){
						cashStmt.setCsDrCr('D');
					}else{
						cashStmt.setCsDrCr('C');
					}
					cashStmt.setCsDt(cs.getCsDt());
					cashStmt.setCsFaCode(cs.getCsFaCode());
					cashStmt.setCsIsClose(cs.isCsIsClose());
					cashStmt.setCsMrNo(cs.getCsMrNo());
					cashStmt.setCsNo(cashStmtStatus);
					cashStmt.setCsPayTo(cs.getCsPayTo());
					cashStmt.setCsTvNo(cs.getCsTvNo());
					cashStmt.setCsType(ConstantsValues.REV_VOUCHER);
					cashStmt.setCsVouchNo(voucherNo);
					cashStmt.setCsVouchType(cs.getCsVouchType());
					cashStmt.setUserCode(currentUser.getUserCode());
					cashStmt.setPayMode(cs.getPayMode());
					
					session.update(cs);
					int csId = (Integer) session.save(cashStmt);
					
					int newSemtId = 0;
					
					if(cs.getCsSFId() > 0){
						List<SubElectricityMstr> semList = new ArrayList<SubElectricityMstr>();
						Criteria cr3 = session.createCriteria(SubElectricityMstr.class);
						cr3.add(Restrictions.eq(SubElectricityMstrCNTS.SEM_ID,cs.getCsSFId()));
						
						semList = cr3.list();
						SubElectricityMstr sem = semList.get(0);
						ElectricityMstr electricityMstr = sem.getElectricityMstr();
						
						SubElectricityMstr newSem = new SubElectricityMstr();
						newSem.setbCode(currentUser.getUserBranchCode());
						newSem.setUserCode(currentUser.getUserCode());
						newSem.setSemCsId(csId);
						newSem.setSemBillFrDt(sem.getSemBillFrDt());
						newSem.setSemBillToDt(sem.getSemBillToDt());
						newSem.setSemPayAmt(-sem.getSemPayAmt());
						newSem.setElectricityMstr(electricityMstr);
						
						electricityMstr.getSubElectricityMstr().add(newSem);
						
						newSemtId = (Integer) session.save(newSem);
						session.update(electricityMstr);
					}
					
					if(newSemtId > 0){
						cashStmt.setCsSFId(newSemtId);
						session.update(cashStmt);
					}
					
					cashStmtStatus.getCashStmtList().add(cashStmt);
					session.update(cashStmtStatus);
				}
				transaction.commit();
				session.flush();
				result = 1;
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
		}else{
			result = -1;
		}
		return result;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int revBPVoucher(List<CashStmt> reqCsList){
		System.out.println("enter into revBPVoucher function");
		int result = 0;
		User currentUser = (User) httpSession.getAttribute("currentUser");
		if(!reqCsList.isEmpty()){
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				List<CashStmt> csList1 = new ArrayList<CashStmt>();
				Criteria cr1 = session.createCriteria(CashStmt.class);
				cr1.add(Restrictions.eq(CashStmtCNTS.CS_ID,reqCsList.get(0).getCsId()));
				csList1 = cr1.list();
				
				CashStmt cs1 = csList1.get(0);
				CashStmtStatus cashStmtStatus1 = cs1.getCsNo();
				CashStmt cashStmt1 = new CashStmt();
				
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus1.getCssDt().getTime());
				Criteria cr2 = session.createCriteria(CashStmt.class);
				cr2.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
				cr2.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,cs1.getbCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("csVouchNo"));
				cr1.setProjection(proList);
				
				List<Integer>  voucherNoList = cr1.list();
				int vNo = voucherNoList.get(voucherNoList.size() - 1);
				int voucherNo = vNo + 1;
				
				for(int i=0;i<reqCsList.size();i++){
					List<CashStmt> csList = new ArrayList<CashStmt>();
					Criteria cr = session.createCriteria(CashStmt.class);
					cr.add(Restrictions.eq(CashStmtCNTS.CS_ID,reqCsList.get(i).getCsId()));
					csList = cr.list();
					
					CashStmt cs = csList.get(0);
					CashStmtStatus cashStmtStatus = cs.getCsNo();
					CashStmt cashStmt = new CashStmt();
					
					cs.setCsIsVRev(true);
					cashStmt.setCsIsVRev(true);
					cashStmt.setbCode(cs.getbCode());
					cashStmt.setCsAmt(cs.getCsAmt());
					cashStmt.setCsChequeType(cs.getCsChequeType());
					cashStmt.setCsDescription("CANCEL " + cs.getCsDescription());
					if(cs.getCsDrCr() == 'C'){
						cashStmt.setCsDrCr('D');
					}else{
						cashStmt.setCsDrCr('C');
					}
					cashStmt.setCsDt(cs.getCsDt());
					cashStmt.setCsFaCode(cs.getCsFaCode());
					cashStmt.setCsIsClose(cs.isCsIsClose());
					cashStmt.setCsMrNo(cs.getCsMrNo());
					cashStmt.setCsNo(cashStmtStatus);
					cashStmt.setCsPayTo(cs.getCsPayTo());
					cashStmt.setCsTvNo(cs.getCsTvNo());
					cashStmt.setCsType(ConstantsValues.REV_VOUCHER);
					cashStmt.setCsVouchNo(voucherNo);
					cashStmt.setCsVouchType(cs.getCsVouchType());
					cashStmt.setUserCode(currentUser.getUserCode());
					cashStmt.setPayMode(cs.getPayMode());
					
					session.update(cs);
					int csId = (Integer) session.save(cashStmt);
					
					int newSemtId = 0;
					
					if(cs.getCsSFId() > 0){
						List<FaBProm> fbpList = new ArrayList<FaBProm>();
						Criteria cr3 = session.createCriteria(FaBProm.class);
						cr3.add(Restrictions.eq(FaBPromCNTS.FBP_ID,cs.getCsSFId()));
						
						fbpList = cr3.list();
						FaBProm faBProm = fbpList.get(0);
						Customer customer = faBProm.getCustomer();
						
						FaBProm newfbp = new FaBProm();
						newfbp.setbCode(currentUser.getUserBranchCode());
						newfbp.setUserCode(currentUser.getUserCode());
						newfbp.setFbpCsId(csId);
						newfbp.setCustomer(customer);
						newfbp.setFbpAmt(-faBProm.getFbpAmt());
						newfbp.setFbpArticle(faBProm.getFbpArticle());
						newfbp.setFbpBrfaCode(faBProm.getFbpBrfaCode());
						newfbp.setFbpDesc("CANCEL "+faBProm.getFbpDesc());
						newfbp.setFbpExpTurn(faBProm.getFbpExpTurn());
						newfbp.setFbpPaidDesig(faBProm.getFbpPaidDesig());
						newfbp.setFbpPaidName(faBProm.getFbpPaidName());
						newfbp.setFbpPurpose(faBProm.getFbpPurpose());
						newfbp.setFbpTurnOver(faBProm.getFbpTurnOver());
						
						customer.getFbpList().add(newfbp);
						
						newSemtId = (Integer) session.save(newfbp);
						session.update(customer);
					}
					
					if(newSemtId > 0){
						cashStmt.setCsSFId(newSemtId);
						session.update(cashStmt);
					}
					
					cashStmtStatus.getCashStmtList().add(cashStmt);
					session.update(cashStmtStatus);
				}
				transaction.commit();
				session.flush();
				result = 1;
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
		}else{
			result = -1;
		}
		return result;
	}
	
	

	@SuppressWarnings("unchecked")
	@Transactional
	public int revVehVoucher(List<CashStmt> reqCsList){
		System.out.println("enter into revVehVoucher function");
		int result = 0;
		User currentUser = (User) httpSession.getAttribute("currentUser");
		if(!reqCsList.isEmpty()){
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				List<CashStmt> csList1 = new ArrayList<CashStmt>();
				Criteria cr1 = session.createCriteria(CashStmt.class);
				cr1.add(Restrictions.eq(CashStmtCNTS.CS_ID,reqCsList.get(0).getCsId()));
				csList1 = cr1.list();
				
				CashStmt cs1 = csList1.get(0);
				CashStmtStatus cashStmtStatus1 = cs1.getCsNo();
				CashStmt cashStmt1 = new CashStmt();
				
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus1.getCssDt().getTime());
				Criteria cr2 = session.createCriteria(CashStmt.class);
				cr2.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
				cr2.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,cs1.getbCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("csVouchNo"));
				cr1.setProjection(proList);
				
				List<Integer>  voucherNoList = cr1.list();
				int vNo = voucherNoList.get(voucherNoList.size() - 1);
				int voucherNo = vNo + 1;
				
				for(int i=0;i<reqCsList.size();i++){
					List<CashStmt> csList = new ArrayList<CashStmt>();
					Criteria cr = session.createCriteria(CashStmt.class);
					cr.add(Restrictions.eq(CashStmtCNTS.CS_ID,reqCsList.get(i).getCsId()));
					csList = cr.list();
					
					CashStmt cs = csList.get(0);
					CashStmtStatus cashStmtStatus = cs.getCsNo();
					CashStmt cashStmt = new CashStmt();
					
					cs.setCsIsVRev(true);
					cashStmt.setCsIsVRev(true);
					cashStmt.setbCode(cs.getbCode());
					cashStmt.setCsAmt(cs.getCsAmt());
					cashStmt.setCsChequeType(cs.getCsChequeType());
					cashStmt.setCsDescription("CANCEL " + cs.getCsDescription());
					if(cs.getCsDrCr() == 'C'){
						cashStmt.setCsDrCr('D');
					}else{
						cashStmt.setCsDrCr('C');
					}
					cashStmt.setCsDt(cs.getCsDt());
					cashStmt.setCsFaCode(cs.getCsFaCode());
					cashStmt.setCsIsClose(cs.isCsIsClose());
					cashStmt.setCsMrNo(cs.getCsMrNo());
					cashStmt.setCsNo(cashStmtStatus);
					cashStmt.setCsPayTo(cs.getCsPayTo());
					cashStmt.setCsTvNo(cs.getCsTvNo());
					cashStmt.setCsType(ConstantsValues.REV_VOUCHER);
					cashStmt.setCsVouchNo(voucherNo);
					cashStmt.setCsVouchType(cs.getCsVouchType());
					cashStmt.setUserCode(currentUser.getUserCode());
					cashStmt.setPayMode(cs.getPayMode());
					
					session.update(cs);
					int csId = (Integer) session.save(cashStmt);
					
					int newSvmId = 0;
					
					if(cs.getCsSFId() > 0){
						List<SubVehicleMstr> svmList = new ArrayList<SubVehicleMstr>();
						Criteria cr3 = session.createCriteria(SubVehicleMstr.class);
						cr3.add(Restrictions.eq(SubVehicleMstrCNTS.SVM_ID,cs.getCsSFId()));
						
						svmList = cr3.list();
						SubVehicleMstr svm = svmList.get(0);
						VehicleMstr vehicleMstr = svm.getVehicleMstr();
						
						SubVehicleMstr newSvm = new SubVehicleMstr();
						newSvm.setbCode(currentUser.getUserBranchCode());
						newSvm.setUserCode(currentUser.getUserCode());
						newSvm.setSvmCsId(csId);
						newSvm.setSvmFaCode(svm.getSvmFaCode());
						newSvm.setSvmTotAmt(-svm.getSvmTotAmt());
						newSvm.setSvmTvNo(svm.getSvmTvNo());
						newSvm.setVehicleMstr(vehicleMstr);
						
						vehicleMstr.getSvmList().add(newSvm);
						
						newSvmId = (Integer) session.save(newSvm);
						session.update(vehicleMstr);
						
						
						
						List<FaCarDivision> fcdList = new ArrayList<FaCarDivision>();
						Criteria cr4 = session.createCriteria(FaCarDivision.class);
						cr4.add(Restrictions.eq(FaCarDivisionCNTS.FCD_SVM_ID,svm.getSvmId()));
						fcdList = cr4.list();
						
						if(!fcdList.isEmpty()){
							for(int j=0;j<fcdList.size();j++){
								FaCarDivision newFcd = new FaCarDivision();
								newFcd.setbCode(currentUser.getUserBranchCode());
								newFcd.setUserCode(currentUser.getUserCode());
								newFcd.setFcdDesc("CANCEL "+fcdList.get(j).getFcdDesc());
								newFcd.setFcdExpAmt(-fcdList.get(j).getFcdExpAmt());
								newFcd.setFcdExpType(fcdList.get(j).getFcdExpType());
								newFcd.setFcdFuel(fcdList.get(j).getFcdFuel());
								newFcd.setFcdInsFrDt(fcdList.get(j).getFcdInsFrDt());
								newFcd.setFcdInsToDt(fcdList.get(j).getFcdInsToDt());
								newFcd.setFcdReading(fcdList.get(j).getFcdReading());
								newFcd.setFcdSvmId(newSvmId);
								
								session.save(newFcd);
							}
						}
					}
					
					if(newSvmId > 0){
						cashStmt.setCsSFId(newSvmId);
						session.update(cashStmt);
					}
					
					cashStmtStatus.getCashStmtList().add(cashStmt);
					session.update(cashStmtStatus);
				}
				transaction.commit();
				session.flush();
				result = 1;
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
		}else{
			result = -1;
		}
		return result;
	}
	


	@SuppressWarnings("unchecked")
	@Transactional
	public int revTrvVoucher(List<CashStmt> reqCsList){
		System.out.println("enter into revTrvVoucher function");
		int result = 0;
		User currentUser = (User) httpSession.getAttribute("currentUser");
		if(!reqCsList.isEmpty()){
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				List<CashStmt> csList1 = new ArrayList<CashStmt>();
				Criteria cr1 = session.createCriteria(CashStmt.class);
				cr1.add(Restrictions.eq(CashStmtCNTS.CS_ID,reqCsList.get(0).getCsId()));
				csList1 = cr1.list();
				
				CashStmt cs1 = csList1.get(0);
				CashStmtStatus cashStmtStatus1 = cs1.getCsNo();
				CashStmt cashStmt1 = new CashStmt();
				
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus1.getCssDt().getTime());
				Criteria cr2 = session.createCriteria(CashStmt.class);
				cr2.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
				cr2.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,cs1.getbCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("csVouchNo"));
				cr1.setProjection(proList);
				
				List<Integer>  voucherNoList = cr1.list();
				int vNo = voucherNoList.get(voucherNoList.size() - 1);
				int voucherNo = vNo + 1;
				
				for(int i=0;i<reqCsList.size();i++){
					List<CashStmt> csList = new ArrayList<CashStmt>();
					Criteria cr = session.createCriteria(CashStmt.class);
					cr.add(Restrictions.eq(CashStmtCNTS.CS_ID,reqCsList.get(i).getCsId()));
					csList = cr.list();
					
					CashStmt cs = csList.get(0);
					CashStmtStatus cashStmtStatus = cs.getCsNo();
					CashStmt cashStmt = new CashStmt();
					
					cs.setCsIsVRev(true);
					cashStmt.setCsIsVRev(true);
					cashStmt.setbCode(cs.getbCode());
					cashStmt.setCsAmt(cs.getCsAmt());
					cashStmt.setCsChequeType(cs.getCsChequeType());
					cashStmt.setCsDescription("CANCEL " + cs.getCsDescription());
					if(cs.getCsDrCr() == 'C'){
						cashStmt.setCsDrCr('D');
					}else{
						cashStmt.setCsDrCr('C');
					}
					cashStmt.setCsDt(cs.getCsDt());
					cashStmt.setCsFaCode(cs.getCsFaCode());
					cashStmt.setCsIsClose(cs.isCsIsClose());
					cashStmt.setCsMrNo(cs.getCsMrNo());
					cashStmt.setCsNo(cashStmtStatus);
					cashStmt.setCsPayTo(cs.getCsPayTo());
					cashStmt.setCsTvNo(cs.getCsTvNo());
					cashStmt.setCsType(ConstantsValues.REV_VOUCHER);
					cashStmt.setCsVouchNo(voucherNo);
					cashStmt.setCsVouchType(cs.getCsVouchType());
					cashStmt.setUserCode(currentUser.getUserCode());
					cashStmt.setPayMode(cs.getPayMode());
					
					session.update(cs);
					int csId = (Integer) session.save(cashStmt);
					
					
					
					if(!cs.getCsTravIdList().isEmpty()){
						
						for(int j=0;j<cs.getCsTravIdList().size();j++){
							
							int newFtdId = 0;
							List<FaTravDivision> ftdList = new ArrayList<FaTravDivision>();
							Criteria cr3 = session.createCriteria(FaTravDivision.class);
							cr3.add(Restrictions.eq(FaTravDivisionCNTS.FTD_ID,cs.getCsTravIdList().get(j)));
							
							ftdList = cr3.list();
							FaTravDivision ftd = ftdList.get(0);
							Employee employee = ftd.getEmployee();
							
							FaTravDivision newFtd = new FaTravDivision();
							newFtd.setbCode(currentUser.getUserBranchCode());
							newFtd.setUserCode(currentUser.getUserCode());
							newFtd.setFtdCsId(csId);
							newFtd.setFtdDesc(ftd.getFtdDesc());
							newFtd.setFtdFrDt(ftd.getFtdFrDt());
							newFtd.setFtdHName(ftd.getFtdHName());
							newFtd.setFtdHRate(ftd.getFtdHRate());
							newFtd.setFtdPlace(ftd.getFtdPlace());
							newFtd.setFtdToDt(ftd.getFtdToDt());
							newFtd.setFtdTravAmt(-ftd.getFtdTravAmt());
							newFtd.setFtdTravType(ftd.getFtdTravType());
							newFtd.setEmployee(employee);
							
							employee.getFtdList().add(newFtd);
							
							newFtdId = (Integer) session.save(newFtd);
							session.update(employee);
							
							cashStmt.getCsTravIdList().add(newFtdId);
							session.update(cashStmt);
							
						}
				
					}
					
					
					cashStmtStatus.getCashStmtList().add(cashStmt);
					session.update(cashStmtStatus);
				}
				transaction.commit();
				session.flush();
				result = 1;
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
		}else{
			result = -1;
		}
		return result;
	}
}

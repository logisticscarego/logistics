package com.mylogistics.DAOImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.mylogistics.DAO.MasterStnStkDao;
import com.mylogistics.model.MasterStationaryStk;
import com.mylogistics.model.User;

@Repository
public class MasterStnStkDaoImpl implements MasterStnStkDao {

	public static Logger logger = Logger.getLogger(MasterStnStkDaoImpl.class);
	
	@Override
	public List<MasterStationaryStk> getMasterStnStk(Session session, User user, String branchCode, String minNo, String maxNo, String type) {
		logger.info("UserID = "+user.getUserId()+" :Enter into getMasterStnStk()");
		List<MasterStationaryStk> list = null;
		try{
			list = session.createCriteria(MasterStationaryStk.class)
					.add(Restrictions.eq("mstrStnStkStatus", type))
					.add(Restrictions.eq("issuedBranchCode", Integer.parseInt(branchCode)))
					.add(Restrictions.and(Restrictions.ge("mstrStnStkStartNo", Long.parseLong(minNo)), Restrictions.le("mstrStnStkStartNo", Long.parseLong(maxNo))))
					.addOrder(Order.asc("mstrStnStkStartNo"))
					.list();			
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId()+" : Exit from getMasterStnStk() : MasterStkStock Size = "+list.size());
		return list;
	}

}

package com.mylogistics.DAOImpl;

import java.sql.Blob;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.object.SqlQuery;
import org.springframework.transaction.annotation.Transactional;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.mylogistics.DAO.BranchSStatsDAO;
import com.mylogistics.DAO.BranchStockLeafDetDAO;
import com.mylogistics.DAO.ChallanDAO;
import com.mylogistics.constants.BrokerCNTS;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.constants.ChallanDetailCNTS;
import com.mylogistics.constants.Cnmt_ChallanCNTS;
import com.mylogistics.constants.EmployeeCNTS;
import com.mylogistics.constants.LhpvAdvCNTS;
import com.mylogistics.constants.LhpvBalCNTS;
import com.mylogistics.constants.OwnerCNTS;
import com.mylogistics.constants.StationCNTS;
import com.mylogistics.constants.VehicleTypeCNTS;
import com.mylogistics.constants.VehicleVendorMstrCNTS;
import com.mylogistics.controller.AckValidatorCntlr;
import com.mylogistics.model.Acknowledge_Validation;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.BranchSStats;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.Broker;
import com.mylogistics.model.Challan;
import com.mylogistics.model.ChallanDetail;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.Cnmt_Challan;
import com.mylogistics.model.Employee;
import com.mylogistics.model.Owner;
import com.mylogistics.model.State;
import com.mylogistics.model.Station;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleType;
import com.mylogistics.model.VehicleVendorMstr;
import com.mylogistics.model.lhpv.LhpvAdv;
import com.mylogistics.model.lhpv.LhpvBal;
import com.mylogistics.services.Cnmt_ChallanService;
import com.mylogistics.services.ConstantsValues;

public class ChallanDAOImpl implements ChallanDAO{
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	public static Logger logger = Logger.getLogger(ChallanDAOImpl.class);
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchSStatsDAO branchSStatsDAO;
	
	@Autowired
	private BranchStockLeafDetDAO branchStockLeafDetDAO;

	
	@Autowired
	public ChallanDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}



	/*@Transactional
	@SuppressWarnings("unchecked")
	public Map<String,Object> getTransitDay(String contractCode) {
		System.out.println("========>>>."+contractCode);
		Map<String, Object> map = new HashMap<String, Object>();
		List<String> transitDayListDaily = new ArrayList<String>();
		List<String> transitDayListRegular = new ArrayList<String>();

		DailyContract dailyContract = new DailyContract();
		RegularContract regularContract = new RegularContract();


		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();

			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();

			Criteria cr1=session.createCriteria(RegularContract.class);
			cr1.add(Restrictions.eq(RegularContractCNTS.REG_CONT_CODE,contractCode));
			regularContract =(RegularContract)cr1.list();


			Criteria cr=session.createCriteria(DailyContract.class);
			cr.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_CODE,contractCode));
			dailyContract =(DailyContract)cr.list(); 

			transaction.commit();

			if(dailyContract != null ){
				int contTransitDay = dailyContract.getDlyContTransitDay();
				map.put("contTransitDay", contTransitDay);
			}else{
				int contTransitDay = regularContract.getRegContTransitDay();
				map.put("contTransitDay", contTransitDay);
			}  
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		} 
		session.close();
		return map;
	}*/

	@Transactional
	public int savePanIssueState(State state){
		int temp;
		try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			session.save(state);
			transaction.commit();
			session.flush();
			temp=1;
		}catch(Exception e){
			e.printStackTrace();
			temp=-1;
		}
		session.clear();
		session.close();
		return temp;

	}

	@Transactional
	@SuppressWarnings("unchecked")
	public int saveChallan(Challan challan){
		int id;
		try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			challan.setChlnArId(-1);
			challan.setChlnRemAdv(Double.parseDouble(challan.getChlnAdvance()));
			challan.setChlnRemBal(challan.getChlnBalance());
			id = (Integer) session.save(challan);
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			id=-1;
		}
		session.clear();
		session.close();
		return id;

	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public int saveChallan(Challan challan,Session session){
		int id;
			id = (Integer) session.save(challan);
		return id;

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void updateChallan(Challan challan,Session sassion){
		System.out.println("enter into updateChallan function-->"+challan.getChlnId());
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
			challan.setCreationTS(calendar);
			session.saveOrUpdate(challan);
	}
	
	
	
	/*@Transactional
	@SuppressWarnings("unchecked")
	public int saveChlnAndChd(Challan challan , ChallanDetail challanDetail){
		System.out.println("enter into saveChlnAndChd function");
		int res = 0;
		try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			
			challan.setChlnArId(-1);
			challan.setChlnRemAdv(Double.parseDouble(challan.getChlnAdvance()));
			challan.setChlnRemBal(challan.getChlnBalance());
			int chlnId = (Integer) session.save(challan);
			if(chlnId > 0){
				session.save(challanDetail);
				res = chlnId;
				transaction.commit();
			}
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			res = -1;
		}
		session.clear();
		session.close();
		return res;
	}*/
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public int saveChlnAndChd(Challan challan , ChallanDetail challanDetail){
		System.out.println("enter into saveChlnAndChd function");
		int res = 0;
		try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			
			List<Challan> chlnList=session.createCriteria(Challan.class)
					.add(Restrictions.eq("chlnCode", challan.getChlnCode())).list();
			
			int chlnId=0;
			if(!chlnList.isEmpty()) {
				chlnId=chlnList.get(0).getChlnId();
				Challan chln=chlnList.get(0);
				Double paidAdv=0.0;
				SQLQuery qr=session.createSQLQuery("select laId from lhpvadv_challan where chlnId=:chlnId");
				qr.setInteger("chlnId", chlnId);
				
				List<Integer> laIdList=qr.list();
				
				if(!laIdList.isEmpty()) {
					List<LhpvAdv> laList=session.createCriteria(LhpvAdv.class)
							.add(Restrictions.in("laId", laIdList)).list();
					
					if(!laList.isEmpty()) {
						for(LhpvAdv la :laList) {
							paidAdv=paidAdv+la.getLaLryAdvP();
						}
					}
				}
				
				/*challan.setChlnId(chlnId);
				challan.setChlnRemAdv((Double.parseDouble(challan.getChlnAdvance()))-paidAdv);
				challan.setChlnRemBal(challan.getChlnBalance());
				session.update(challan);*/
				chln.setChlnAdvance(challan.getChlnAdvance());
				chln.setChlnBalance(challan.getChlnBalance());
				chln.setChlnBrRate(challan.getChlnBrRate());
				chln.setChlnChgWt(challan.getChlnChgWt());
				chln.setChlnCraneChg(challan.getChlnCraneChg());
				chln.setChlnDetection(challan.getChlnDetection());
				chln.setChlnDt(challan.getChlnDt());
				chln.setChlnEmpCode(challan.getChlnEmpCode());
				chln.setChlnExtra(challan.getChlnExtra());
				chln.setChlnFreight(challan.getChlnFreight());
				chln.setChlnFromStn(challan.getChlnFromStn());
				chln.setChlnHeight(challan.getChlnHeight());
				chln.setChlnLoadingAmt(challan.getChlnLoadingAmt());
				chln.setChlnLryLoadTime(challan.getChlnLryLoadTime());
				chln.setChlnLryNo(challan.getChlnLryNo());
				chln.setChlnLryRate(challan.getChlnLryRate());
				chln.setChlnLryRepDT(challan.getChlnLryRepDT());
				chln.setChlnLryRptTime(challan.getChlnLryRptTime());
				chln.setChlnNoOfPkg(challan.getChlnNoOfPkg());
				chln.setChlnOthers(challan.getChlnOthers());
				chln.setChlnPayAt(challan.getChlnPayAt());
				chln.setChlnRemAdv((Double.parseDouble(challan.getChlnAdvance()))-paidAdv);
				chln.setChlnRemBal(challan.getChlnTotalFreight()-(Double.parseDouble(challan.getChlnAdvance())));
				chln.setChlnRrNo(challan.getChlnRrNo());
				chln.setChlnStatisticalChg(challan.getChlnStatisticalChg());
				chln.setChlnTdsAmt(challan.getChlnTdsAmt());
				chln.setChlnTimeAllow(challan.getChlnTimeAllow());
				chln.setChlnToolTax(challan.getChlnToolTax());
				chln.setChlnToStn(challan.getChlnToStn());
				chln.setChlnTotalFreight(challan.getChlnTotalFreight());
				chln.setChlnTotalWt(challan.getChlnTotalWt());
				chln.setChlnTrainNo(challan.getChlnTrainNo());
				chln.setChlnTwoPoint(challan.getChlnTwoPoint());
				chln.setChlnType(challan.getChlnType());
				chln.setChlnUnion(challan.getChlnUnion());
				chln.setChlnVehicleType(challan.getChlnVehicleType());
				chln.setChlnWeightmentChg(challan.getChlnWeightmentChg());
				chln.setChlnWtSlip(challan.getChlnWtSlip());
				chln.setUserCode(challan.getUserCode());
				
				session.update(chln);
				
			}else {
				challan.setChlnArId(-1);
				challan.setChlnRemAdv(Double.parseDouble(challan.getChlnAdvance()));
				challan.setChlnRemBal(challan.getChlnBalance());
				chlnId = (Integer) session.save(challan);
			}
			
			if(chlnId > 0){
				session.save(challanDetail);
				res = chlnId;
				transaction.commit();
			}
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			res = -1;
		}
		session.clear();
		session.close();
		return res;
	}
	
	
	@Override
	public int saveChlnAndChd(Challan challan , ChallanDetail challanDetail,Session session){
		System.out.println("enter into saveChlnAndChd function");
		int res = 0;
			
			challan.setChlnArId(-1);
			challan.setChlnRemAdv(Double.parseDouble(challan.getChlnAdvance()));
			challan.setChlnRemBal(challan.getChlnBalance());
			int chlnId = (Integer) session.save(challan);
			if(chlnId > 0){
				session.save(challanDetail);
				res = chlnId;
			}
		return res;
	}
	
	
	
	

	@Transactional
	@SuppressWarnings("unchecked")
	public List<Challan> getChallanById(int chlnId){
		List<Challan> challanList = new ArrayList<Challan>();
		try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			Criteria cr=session.createCriteria(Challan.class);
			cr.add(Restrictions.eq(ChallanCNTS.CHLN_ID,chlnId));
			challanList = cr.list();
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return challanList;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String,Object> getChlnInfoById(int chlnId){
		System.out.println("enter into getChlnInfoById function = "+chlnId);
		Map<String,Object> map = new HashMap<>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Challan challan = (Challan) session.get(Challan.class,chlnId);
			if(challan != null){
				map.put("chlnCode",challan.getChlnCode());
				map.put("chlnLryNo",challan.getChlnLryNo());
				map.put("chlnLryRate",challan.getChlnLryRate());
				map.put("chlnChgWt",challan.getChlnChgWt());
				map.put("chlnNoOfPkg", challan.getChlnNoOfPkg());
				map.put("chlnTotalWt", challan.getChlnTotalWt());
				map.put("chlnFreight", challan.getChlnFreight());
				map.put("chlnLoadingAmt", challan.getChlnLoadingAmt());
				map.put("chlnExtra", challan.getChlnExtra());
				map.put("chlnTotalFreight", challan.getChlnTotalFreight());
				map.put("chlnAdvance", challan.getChlnAdvance());
				map.put("chlnBalance", challan.getChlnBalance());
				map.put("chlnPayAt", challan.getChlnPayAt());
				map.put("chlnTimeAllow", challan.getChlnTimeAllow());
				map.put("chlnDt", challan.getChlnDt());
				map.put("chlnBrRate", challan.getChlnBrRate());
				map.put("chlnWtSlip", challan.getChlnWtSlip());
				map.put("chlnVehicleType", challan.getChlnVehicleType());
				map.put("chlnStatisticalChg", challan.getChlnStatisticalChg());
				map.put("chlnTdsAmt", challan.getChlnTdsAmt());
				map.put("chlnRrNo", challan.getChlnRrNo());
				map.put("chlnTrainNo", challan.getChlnTrainNo());
				map.put("chlnLryLoadTime", challan.getChlnLryLoadTime());
				map.put("chlnLryRepDT", challan.getChlnLryRepDT());
				map.put("chlnLryRptTime", challan.getChlnLryRptTime());
				map.put("chlnArId", challan.getChlnArId());
				
				if(challan.getChlnArId() > 0){
					ArrivalReport arrRep = (ArrivalReport) session.get(ArrivalReport.class,challan.getChlnArId());
					map.put("chlnRecWt", arrRep.getArRcvWt());
					map.put("recPkg", arrRep.getArPkg());
					map.put("unldng", arrRep.getArUnloading());
					map.put("det", arrRep.getArDetention());
					map.put("oth", arrRep.getArOther());
					map.put("ltDel", arrRep.getArLateDelivery());
					map.put("ltAck", arrRep.getArLateAck());
					map.put("ovrHgt", arrRep.getArOvrHgt());
					map.put("extKm", arrRep.getArExtKm());
					map.put("pen", arrRep.getArPenalty());
					map.put("wtSrt", arrRep.getArWtShrtg());
					map.put("drRcvr", arrRep.getArDrRcvrWt());
					map.put("arDesc", arrRep.getArDesc());
				}
				
				if(challan.getChlnFromStn() != null){
					Station frStn = (Station) session.get(Station.class,Integer.parseInt(challan.getChlnFromStn()));
					map.put("chlnFromStn",frStn.getStnName());
				}
				if(challan.getChlnToStn() != null){
					Station toStn = (Station) session.get(Station.class,Integer.parseInt(challan.getChlnToStn()));
					map.put("chlnToStn",toStn.getStnName());
				}
				
				List<ChallanDetail> chdList = new ArrayList<>();
				Criteria cr=session.createCriteria(ChallanDetail.class);
				cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE,challan.getChlnCode()));
				chdList = cr.list();
				if(!chdList.isEmpty()){
					ChallanDetail chd = chdList.get(0);
					if(challan.getChlnLryNo() != null){
						map.put("chlnLryNo",challan.getChlnLryNo());
					}else{
						map.put("chlnLryNo",chd.getChdRcNo());
					}
					
					map.put("driverName",chd.getChdDvrName());
					
					if(chd.getChdOwnCode() != null){
						List<Owner> ownList = new ArrayList<>();
						cr=session.createCriteria(Owner.class);
						cr.add(Restrictions.eq(OwnerCNTS.OWN_CODE,chd.getChdOwnCode()));
						ownList = cr.list();
						if(!ownList.isEmpty()){
							map.put("ownName",ownList.get(0).getOwnName());
						}
					}
					
					if(chd.getChdBrCode() != null){
						List<Broker> brkList = new ArrayList<>();
						cr=session.createCriteria(Broker.class);
						cr.add(Restrictions.eq(BrokerCNTS.BRK_CODE,chd.getChdCode()));
						brkList = cr.list();
						if(!brkList.isEmpty()){
							map.put("brkName",brkList.get(0).getBrkName());
						}
					}
				}
			}
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return map;
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Challan> getChallanisViewFalse(String branchCode){
		System.out.println("enter into getChallanisViewFalse function");
		System.out.println("The value of branch code is "+branchCode);
		List<Challan> result = new ArrayList<Challan>();

		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Challan.class);
			cr.add(Restrictions.eq(ChallanCNTS.IS_VIEW,false));
			cr.add(Restrictions.eq(ChallanCNTS.USER_BRANCH_CODE,branchCode));
			result =cr.list();
			transaction.commit();

			System.out.println("size of list is-->>"+result.size());
			for(int i = 0;i<result.size();i++){
				System.out.println("The values in the list is"+result.get(i).getChlnAdvance());
				System.out.println("The values in the list is"+result.get(i).getChlnCode());
			}
			System.out.println("After getting the list");

			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return  result; 
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public int updateChallanisViewTrue(String code[]){
		int temp;
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		for(int i=0;i<code.length;i++){
			System.out.println("The values inside array is ----------"+code[i]);
		}
		try{
			session = this.sessionFactory.openSession();
			for(int i=0;i<code.length;i++){
				transaction = session.beginTransaction();

				Challan challan = new Challan();

				List<Challan> list = new ArrayList<Challan>();
				Criteria cr=session.createCriteria(Challan.class);
				cr.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE,code[i]));
				list =cr.list();
				challan = list.get(0);
				challan.setView(true);
				challan.setCreationTS(calendar);
				session.update(challan);
				transaction.commit();
			}
			session.flush();

			temp = 1;
		}catch(Exception e){
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Challan> getChallanList(String chlnCode){
		System.out.println("enter into getChallanList function --->"+chlnCode);		
		List<Challan> result = new ArrayList<Challan>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Challan.class);
			cr.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE,chlnCode));
			result =cr.setMaxResults(1).list();
			/*if(!result.isEmpty()){
				Hibernate.initialize(result.get(0).getLhpvAdv());
			}
			if(!result.isEmpty()){
				List<Integer> chlnIdList = session.createQuery("select chlnId from lhpvadv_challan where chlnId='"+result.get(0).getChlnId()+"'").setMaxResults(1).list();
				System.out.println("size of chlnIdList = "+chlnIdList.size());
			}*/
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return  result; 
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int checkLA(int chlnId){
		System.out.println("enter into checkLA function");
		int res = 0;
		try{
			session = this.sessionFactory.openSession();
	            
            SQLQuery sqlQuery = session.createSQLQuery("SELECT * From lhpvadv_challan where chlnId = :chlnId");
            sqlQuery.setParameter("chlnId", chlnId);
            
            List<Object> query = sqlQuery.list();
            
            if(!query.isEmpty()){
            	for(Object rows : query){
                    Object[] row = (Object[]) rows;
                    int field1 = (int) row[0]; // contains field1
                    int field2 = (int) row[1];
                    System.out.println("query to third table: "+field1+" :"+field2);
            	}
            	res = 1;
            }else{
            	res = 0;
            }
             
            session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return res;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int checkLB(int chlnId){
		System.out.println("enter into checkLB function");
		int res = 0;
		try{
			session = this.sessionFactory.openSession();
	            
            SQLQuery sqlQuery = session.createSQLQuery("SELECT * From lhpvbal_challan where chlnId = :chlnId");
            sqlQuery.setParameter("chlnId", chlnId);
            
            List<Object> query = sqlQuery.list();
            
            if(!query.isEmpty()){
            	for(Object rows : query){
                    Object[] row = (Object[]) rows;
                    int field1 = (int) row[0]; // contains field1
                    int field2 = (int) row[1];
                    System.out.println("query to third table: "+field1+" :"+field2);
            	}
            	res = 1;
            }else{
            	res = 0;
            }
             
            session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return res;
	}

	
	@SuppressWarnings("unchecked")
	@Transactional
	public int checkLSP(int chlnId){
		System.out.println("enter into checkLSP function");
		int res = 0;
		try{
			session = this.sessionFactory.openSession();
	            
            SQLQuery sqlQuery = session.createSQLQuery("SELECT * From lhpvsup_challan where chlnId = :chlnId");
            sqlQuery.setParameter("chlnId", chlnId);
            
            List<Object> query = sqlQuery.list();
            
            if(!query.isEmpty()){
            	for(Object rows : query){
                    Object[] row = (Object[]) rows;
                    int field1 = (int) row[0]; // contains field1
                    int field2 = (int) row[1];
                    System.out.println("query to third table: "+field1+" :"+field2);
            	}
            	res = 1;
            }else{
            	res = 0;
            }
             
            session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return res;
	}
	
	

	@SuppressWarnings("unchecked")
	@Transactional
	public List<LhpvBal> getLBOfChln(int chlnId){
		System.out.println("enter into getLBOfChln function");
		List<LhpvBal> lbList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
            
            SQLQuery sqlQuery = session.createSQLQuery("SELECT * From lhpvbal_challan where chlnId = :chlnId");
            sqlQuery.setParameter("chlnId", chlnId);
            
            List<Object> query = sqlQuery.list();
            
            if(!query.isEmpty()){
            	for(Object rows : query){
                    Object[] row = (Object[]) rows;
                    int field1 = (int) row[0]; // contains field1
                    int field2 = (int) row[1];
                    System.out.println("query to third table: "+field1+" :"+field2);
                    
                    LhpvBal lhpvBal = (LhpvBal) session.get(LhpvBal.class, field2);
                    lbList.add(lhpvBal);
            	}
            }
             
            session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return lbList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int updateChallan(Challan challan){
		System.out.println("enter into updateChallan function-->"+challan.getChlnId());
		int temp;
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			challan.setCreationTS(calendar);
			session.saveOrUpdate(challan);
			transaction.commit();
			session.flush();
			temp= 1;
		}catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}		
		session.clear();
		session.close();
		return temp;
	}


	@Transactional
	public Blob getChallanImageByCode(String chlnCode){
		System.out.println("enter into getChallanImageByCode function");
		List<Challan> chlnList = new ArrayList<Challan>();
		Blob blob = null;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Challan.class);
			cr.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE, chlnCode));
			chlnList = cr.list();
			Challan challan = chlnList.get(0);
			//blob = challan.getChallanImage();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return blob;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String,Object> getChallanNChallanDet(String chlnCode){

		List<Challan> chlnList = new ArrayList<Challan>();
		List<ChallanDetail> chdList = new ArrayList<ChallanDetail>();
		Challan challan = new Challan();
		ChallanDetail challanDetail = new ChallanDetail();
		int temp = 0;

		try{
			System.out.println("wanna retrieve the list of challan");
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			System.out.println("Hello There");
			Criteria cr=session.createCriteria(Challan.class);//fixed
			cr.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE,chlnCode));
			chlnList =cr.setMaxResults(1).list();
			challan = chlnList.get(0);
			
			cr=session.createCriteria(ChallanDetail.class);//fixed
			cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE, chlnCode));
			chdList = cr.setMaxResults(1).list();
			challanDetail = chdList.get(0);
			
			temp = 1;
			session.flush();
		}catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("challan", challan);
		map.put("challanDetail", challanDetail);
		map.put("temp", temp);
		session.clear();
		session.close();
		return map;
	}
	
	
//	@SuppressWarnings("unchecked")
//	public Map<String,Object> getChallanNChallanDet(String chlnCode){
//
//		List<Challan> chlnList = new ArrayList<Challan>();
//		List<ChallanDetail> chdList = new ArrayList<ChallanDetail>();
//		Challan challan = null;
//		ChallanDetail challanDetail = null;
//		Owner own=null;
//		Broker brk=null;
//		Station frmStn=null;
//		Station toStn=null;
//		VehicleVendorMstr vvm=null;
//		VehicleType vt=null;
//		Employee emp=null;
//		
//		
//		int temp = 0;
//
//		try{
//			System.out.println("wanna retrieve the list of challan");
//			session = this.sessionFactory.openSession();
//			System.out.println("Hello There");
//			Criteria cr=session.createCriteria(Challan.class);//fixed
//			cr.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE,chlnCode));
//			chlnList =cr.setMaxResults(1).list();
//			challan = chlnList.get(0);
//			
//			cr=session.createCriteria(ChallanDetail.class);//fixed
//			cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE, chlnCode));
//			chdList = cr.setMaxResults(1).list();
//			challanDetail = chdList.get(0);
//			
//			cr=session.createCriteria(Owner.class)
//					.add(Restrictions.eq(OwnerCNTS.OWN_CODE, challanDetail.getChdOwnCode()));
//			own=(Owner) cr.setMaxResults(1).list().get(0);
//					
//			cr=session.createCriteria(Broker.class)
//					.add(Restrictions.eq(BrokerCNTS.BRK_CODE, challanDetail.getChdBrCode()));
//			brk=(Broker) cr.setMaxResults(1).list().get(0);
//			
//			cr= session.createCriteria(Station.class)
//					.add(Restrictions.eq(StationCNTS.STN_CODE, challan.getChlnFromStn()));
//			frmStn=(Station)cr.setMaxResults(1).list().get(0);
//			
//			cr= session.createCriteria(Station.class)
//					.add(Restrictions.eq(StationCNTS.STN_CODE, challan.getChlnToStn()));
//			toStn=(Station)cr.setMaxResults(1).list().get(0);
//			
//			cr= session.createCriteria(VehicleVendorMstr.class)
//					.add(Restrictions.eq(VehicleVendorMstrCNTS.VV_RCNO, challan.getChlnLryNo()));
//			vvm=(VehicleVendorMstr)cr.setMaxResults(1).list().get(0);
//			
//			cr= session.createCriteria(VehicleType.class)
//					.add(Restrictions.eq(VehicleTypeCNTS.VT_VEHICLE_TYPE, challan.getChlnVehicleType()));
//			vt = (VehicleType)cr.setMaxResults(1).list().get(0);
//			
//			cr=session.createCriteria(Employee.class)
//					.add(Restrictions.eq(EmployeeCNTS.EMP_CODE, challan.getChlnEmpCode()));
//			emp=(Employee)cr.setMaxResults(1).list().get(0);
//			
//			temp = 1;
//		}catch (Exception e) {
//			e.printStackTrace();
//			temp = -1;
//		}
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("challan", challan);
//		map.put("challanDetail", challanDetail);
//		map.put("owner", own);
//		map.put("broker", brk);
//		map.put("frmStn", frmStn);
//		map.put("toStn", toStn);
//		map.put("vehVndMstr", vvm);
//		map.put("vehType", vt);
//		map.put("emp", emp);
//		map.put("temp", temp);
//		session.clear();
//		session.close();
//		return map;
//	}
//	
//	
	

	@Transactional
	@SuppressWarnings("unchecked")
	public List<String> getChallanCode(){
		System.out.println("enter into getChallanCode function");
		List<String> challanList = new ArrayList<String>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Challan.class);
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property(ChallanCNTS.CHALLAN_CODE));
			cr.setProjection(projList);
			challanList = cr.list();
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return challanList;
	}


	@Transactional
	@SuppressWarnings("unchecked")
	public List<String> getChlnCodeFrLHB(){
		System.out.println("enter into getChlnCodeFrLHB function");
		List<String> chlnList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			List<Challan> challanList = new ArrayList<>();
			
			Criteria cr=session.createCriteria(Challan.class);
			cr.add(Restrictions.gt(ChallanCNTS.CHLN_REM_BAL,0.0));
			cr.add(Restrictions.gt(ChallanCNTS.CHLN_AR_ID,0));
			//cr.add(Restrictions.eq(ChallanCNTS.CHLN_REM_ADV,0.0));
			challanList = cr.list();
			System.out.println("size of challanList = "+challanList.size());
			if(!challanList.isEmpty()){
				for(int i=0;i<challanList.size();i++){
					/*//LhpvAdv lhpvAdv = challanList.get(i).getLhpvAdv();
					LhpvBal lhpvBal = challanList.get(i).getLhpvBal();
					//int arId = challanList.get(i).getChlnArId();
					if(lhpvAdv != null && lhpvBal == null){
						chlnList.add(challanList.get(i).getChlnCode());
					}
					//if(lhpvBal == null && arId > 0){
					if(lhpvBal == null){	
						chlnList.add(challanList.get(i).getChlnCode());
					}*/
					chlnList.add(challanList.get(i).getChlnCode());
				}
			}
			System.out.println("size of chlnList = "+chlnList.size());
			challanList.clear();
			session.flush();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return chlnList;	
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<String> getChlnCodeFrLHSP(){
		System.out.println("enter into getChlnCodeFrLHSP function");
		List<String> chlnList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			List<Challan> challanList = new ArrayList<>();
			
			Criteria cr=session.createCriteria(Challan.class);
			cr.add(Restrictions.eq(ChallanCNTS.CHLN_REM_BAL,0.0));
			cr.add(Restrictions.eq(ChallanCNTS.CHLN_REM_ADV,0.0));
			cr.add(Restrictions.gt(ChallanCNTS.CHLN_AR_ID,0));
			challanList = cr.list();
			System.out.println("size of challanList = "+challanList.size());
			Map<String,Boolean> map = new HashMap<>();
			if(!challanList.isEmpty()){
				for(int i=0;i<challanList.size();i++){
					/*//LhpvAdv lhpvAdv = challanList.get(i).getLhpvAdv();
					LhpvBal lhpvBal = challanList.get(i).getLhpvBal();
					LhpvSup lhpvSup = challanList.get(i).getLhpvSup();
					//if(lhpvAdv != null && lhpvBal != null && lhpvSup == null){
					if(lhpvBal != null && lhpvSup == null){
						if(lhpvBal.getLbWtShrtgCR() <= 0 || lhpvBal.getLbDrRcvrWtCR() <= 0 || lhpvBal.getLbLateAckCR() <= 0 || 
								lhpvBal.getLbLateDelCR() <= 0 || lhpvBal.getLbOthExtKmP() <= 0 || lhpvBal.getLbOthOvrHgtP() <= 0 
								|| lhpvBal.getLbOthPnltyP() <= 0 || lhpvBal.getLbOthMiscP() <= 0 || lhpvBal.getLbUnpDetP() <= 0 
								|| lhpvBal.getLbUnLoadingP() <= 0){
							chlnList.add(challanList.get(i).getChlnCode());
						}
					}*/
					
					chlnList.add(challanList.get(i).getChlnCode());
				}
			}
			System.out.println("size of chlnList = "+chlnList.size());
			challanList.clear();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return chlnList;
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<String> getChlnCodeFrLHA(){
		System.out.println("enter into getChlnCodeFrLHA function");
		List<String> chlnList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			List<Challan> challanList = new ArrayList<>();
			
			Criteria cr=session.createCriteria(Challan.class);
			cr.add(Restrictions.gt(ChallanCNTS.CHLN_REM_ADV,0.0));
			challanList = cr.list();
			System.out.println("size of challanList = "+challanList.size());
			if(!challanList.isEmpty()){
				for(int i=0;i<challanList.size();i++){
					/*LhpvAdv lhpvAdv = challanList.get(i).getLhpvAdv();
					if(lhpvAdv != null){
						//System.out.println("challan already have lhpv adv = "+challanList.get(i).getChlnCode());
					}else{
						chlnList.add(challanList.get(i).getChlnCode());
					}*/
					
					chlnList.add(challanList.get(i).getChlnCode());
				}
			}
			
			System.out.println("size of chlnList = "+chlnList.size());
			challanList.clear();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return chlnList;	
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Challan> getAllChallan(){
		System.out.println("enter into getAllChallan function");
		List<Challan> challanList = new ArrayList<Challan>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Challan.class);
			challanList = cr.list();
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return challanList;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public List<Challan> getAllChlnByBrh(String brhCode){
		System.out.println("enter into getAllChlnByBrh function");
		List<Challan> challanList = new ArrayList<Challan>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Challan.class);
			cr.add(Restrictions.eq(ChallanCNTS.BRANCH_CODE, brhCode));
			challanList = cr.list();
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return challanList;
	}

	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Challan> getChlnFrSedrByBrh(String brhCode){
		System.out.println("enter into getChlnFrSedrByBrh function");
		List<Challan> challanList = new ArrayList<Challan>();
		//List<Challan> actChlnList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			//transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Challan.class);
			cr.add(Restrictions.eq(ChallanCNTS.BRANCH_CODE, brhCode));
			cr.add(Restrictions.eq(ChallanCNTS.CHLN_AR_ID,-1));
			challanList = cr.list();
			
			System.out.println("challanListSize: "+challanList.size());
			
			/*if(!challanList.isEmpty()){
				for(int i=0;i<challanList.size();i++){
					Challan chln = challanList.get(i);
					LhpvAdv lhpvAdv = chln.getLhpvAdv();
					if(lhpvAdv != null){
						actChlnList.add(chln);
					}
				}
			}*/
			//challanList.clear();
			//transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		//return actChlnList;
		return challanList;
	}
	

	@Transactional
	@SuppressWarnings("unchecked")
	public List<Challan> getAllChallanCodes(String branchCode){

		List<Challan> challanCodeList = new ArrayList<Challan>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Challan.class);
			cr.add(Restrictions.eq(ChallanCNTS.BRANCH_CODE, branchCode));
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property(ChallanCNTS.CHALLAN_CODE));
			cr.setProjection(projList);
			challanCodeList = cr.list();
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return challanCodeList;
	}

	

	@Transactional
	@SuppressWarnings("unchecked")
	public List<String> getChCodeByBrh(String brhCode){
		System.out.println("enter into getChCodeByBrh function");
		List<String> chCodeList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			List<Challan> chlnList = new ArrayList<>();
			Criteria cr=session.createCriteria(Challan.class);
			cr.add(Restrictions.eq(ChallanCNTS.BRANCH_CODE, brhCode));
			chlnList = cr.list();
			if(!chlnList.isEmpty()){
				for(int i=0;i<chlnList.size();i++){
					chCodeList.add(chlnList.get(i).getChlnCode());
				}
			}
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return chCodeList;
	}



	@Override
	@Transactional
	public int saveChallanDetail(Map<String, String> chdService) {
		
		User currentUser = (User) httpSession.getAttribute("currentUser");
		
		int temp = 0;
		
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			Criteria cr = session.createCriteria(Owner.class);
			cr.add(Restrictions.eq(OwnerCNTS.OWN_CODE, chdService.get("ownCode")));
			Owner owner = (Owner) cr.list().get(0);
			
			cr = session.createCriteria(Broker.class);
			cr.add(Restrictions.eq(BrokerCNTS.BRK_CODE, chdService.get("brkCode")));
			Broker broker = (Broker) cr.list().get(0);
			
			cr = session.createCriteria(VehicleVendorMstr.class);
			cr.add(Restrictions.eq(VehicleVendorMstrCNTS.VV_RCNO, chdService.get("rcNo")));
			VehicleVendorMstr vvMstr = (VehicleVendorMstr) cr.list().get(0);
			
			if (owner.isOwnIsPanImg() || broker.isBrkIsPanImg()) {
				ChallanDetail challanDetail = new ChallanDetail();
				
				challanDetail.setbCode(currentUser.getUserBranchCode());
				challanDetail.setChdBankFinance(vvMstr.getVvFinanceBankName());
				challanDetail.setChdBrCode(broker.getBrkCode());
				if (broker.getBrkPhNoList() != null && !broker.getBrkPhNoList().isEmpty()) {
					challanDetail.setChdBrMobNo(broker.getBrkPhNoList().get(0));
				}
				challanDetail.setChdChlnCode(chdService.get("chlnCode"));
				challanDetail.setChdDlIssueDt(vvMstr.getVvDriverDLIssueDt());
				challanDetail.setChdDlNo(vvMstr.getVvDriverDLNo());
				challanDetail.setChdDlValidDt(vvMstr.getVvDriverDLValidDt());
				challanDetail.setChdDvrMobNo(vvMstr.getVvDriverMobNo());
				challanDetail.setChdDvrName(vvMstr.getVvDriverName());
				challanDetail.setChdFitDocIssueDt(vvMstr.getVvFitIssueDt());
				challanDetail.setChdFitDocValidDt(vvMstr.getVvFitValidDt());
				challanDetail.setChdFitDocNo(vvMstr.getVvFitNo());
				challanDetail.setChdFitDocPlace(vvMstr.getVvFitState());
				challanDetail.setChdOwnCode(owner.getOwnCode());
				if (owner.getOwnPhNoList() != null && !owner.getOwnPhNoList().isEmpty()) {
					challanDetail.setChdOwnMobNo(owner.getOwnPhNoList().get(0));
				}
				if (owner.isOwnIsPanImg()) {
					challanDetail.setChdPanHdrName(owner.getOwnName());
					challanDetail.setChdPanNo(owner.getOwnPanNo());
					challanDetail.setChdPanHdrType("owner");
				} else if (broker.isBrkIsPanImg()) {
					challanDetail.setChdPanHdrName(broker.getBrkName());
					challanDetail.setChdPanNo(broker.getBrkPanNo());
					challanDetail.setChdPanHdrType("broker");
				}
				challanDetail.setChdPerIssueDt(vvMstr.getVvPerIssueDt());
				challanDetail.setChdPerNo(vvMstr.getVvPerNo());
				challanDetail.setChdPerState(vvMstr.getVvPerState());
				challanDetail.setChdPerValidDt(vvMstr.getVvPerValidDt());
				challanDetail.setChdPolicyCom(vvMstr.getVvPolicyComp());
				challanDetail.setChdPolicyNo(vvMstr.getVvPolicyNo());
				challanDetail.setChdRcIssueDt(vvMstr.getVvRcIssueDt());
				challanDetail.setChdRcNo(vvMstr.getVvRcNo());
				challanDetail.setChdRcValidDt(vvMstr.getVvRcValidDt());
				challanDetail.setChdTransitPassNo(vvMstr.getVvTransitPassNo());
				challanDetail.setUserCode(currentUser.getUserCode());
				
				session.save(challanDetail);
				session.flush();
				transaction.commit();
				//start tomorrow
				temp = 1;
			} else {
				temp = -2;
			}
			
		} catch (Exception e) {
				transaction.rollback();
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}



	@Override
	public List<String> getChlnCodeNotInChd() {
		List<String> challanList = new ArrayList<String>();
		try{
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("SELECT chlnCode FROM Challan WHERE chlnCode NOT IN (SELECT chdChlnCode FROM ChallanDetail)");
			challanList = query.list();
			
			System.out.println(challanList.size());
			System.err.println(challanList.size());
			
			for (String string : challanList) {
				System.out.println(string);
			}
			
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return challanList;
	}



	@Override
	public int updateChlnNChd(Challan challan, ChallanDetail challanDetail) {
		System.out.println("updateChlnNChd()");
		int temp = 0;
		
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			challan.setChlnEdit(true);
			session.update(challan);
			
			challanDetail.setChdEdit(true);
			challanDetail.setChdChlnCode(challan.getChlnCode());
			session.update(challanDetail);
			
			transaction.commit();
			session.flush();
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		
		session.clear();
		session.close();
		return temp;
	}



	@Override
	@Transactional
	public Map<String, Object> getCnmtByChln(String chlnCode) {
		System.out.println("getCnmtByChln()");
		Map<String, Object> map = new HashMap<>();
		
		int temp = 0;
		
		try {
			session = this.sessionFactory.openSession();
			Criteria cr = session.createCriteria(Challan.class);
			cr.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE, chlnCode));
			
			Challan challan = (Challan) cr.list().get(0);
			
			Query query = session.createQuery("SELECT cnmtId FROM Cnmt_Challan where chlnId = :chlnId");
			query.setInteger("chlnId", challan.getChlnId());
			
			List<Integer> cnmtIdList = query.list();
			List<Map<String, Object>> cnmtCodeIdByChlnList = new ArrayList<>();
			for (Integer cnmtId : cnmtIdList) {
				Cnmt cnmt = (Cnmt) session.get(Cnmt.class, cnmtId);
				Map<String, Object> codeIdMap = new HashMap<>();
				codeIdMap.put("cnmtId", cnmt.getCnmtId());
				codeIdMap.put("cnmtCode", cnmt.getCnmtCode());
				cnmtCodeIdByChlnList.add(codeIdMap);
			}
			
			Query cnmtQuery = session.createQuery("SELECT new map(cnmtId as cnmtId, cnmtCode as cnmtCode) FROM Cnmt");
			List<Map<String, Object>> cnmtCodeIdList = cnmtQuery.list();
			
			map.put("chlnId", challan.getChlnId());
			map.put("cnmtCodeIdByChlnList", cnmtCodeIdByChlnList);
			map.put("cnmtCodeIdList", cnmtCodeIdList);
			session.flush();
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		
		session.clear();
		session.close();
		
		map.put("temp", temp);
 		return map;
	}



	@Override
	@Transactional
	public int submitChlnNCnmt(Cnmt_ChallanService chlnNCnmtService) {
		System.out.println("submitChlnNCnmt()");
		
		int temp = 0;
		
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			//remove cnmt from challan
			int chlnId = chlnNCnmtService.getChlnId();
			List<Integer> cnmtAddIdList = chlnNCnmtService.getCnmtAddIdList();
			List<Integer> cnmtRemoveIdList = chlnNCnmtService.getCnmtRemoveIdList();
			
			if (!cnmtRemoveIdList.isEmpty()) {
				for (Integer cnmtId : cnmtRemoveIdList) {
					Query delQuery = session.createQuery("DELETE FROM Cnmt_Challan where chlnId = :chlnId and cnmtId = :cnmtId");
					delQuery.setInteger("chlnId", chlnId);
					delQuery.setInteger("cnmtId", cnmtId);
					int delResult = delQuery.executeUpdate();
					System.out.println("del");
				}
			}
			
			if (!cnmtAddIdList.isEmpty()) {
				for (Integer cnmtId : cnmtAddIdList) {
					
					Query query = session.createQuery("FROM Cnmt_Challan where chlnId = :chlnId and cnmtId = :cnmtId");
					query.setInteger("chlnId", chlnId);
					query.setInteger("cnmtId", cnmtId);
					List<Cnmt_Challan> cnmt_ChallanList = query.list();
					
					if (cnmt_ChallanList.size()>0) {
						//no need to add
					} else {
						Cnmt_Challan cnmt_Challan = new Cnmt_Challan();
						cnmt_Challan.setChlnId(chlnId);
						cnmt_Challan.setCnmtId(cnmtId);
						session.save(cnmt_Challan);
					}
					
					
				}
			}
			
			transaction.commit();
			session.flush();
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}

	@Override
	@Transactional
	public List<String> getChlnCodeFrLHBTemp() {
		System.out.println("ChallanDAOImpl.getChlnCodeFrLHBTemp()");
		
		List<String> chlnList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			List<Challan> challanList = new ArrayList<>();
			
			Criteria cr=session.createCriteria(Challan.class);
			cr.add(Restrictions.gt(ChallanCNTS.CHLN_REM_BAL,0.0));
			//cr.add(Restrictions.gt(ChallanCNTS.CHLN_AR_ID,0));
			//cr.add(Restrictions.eq(ChallanCNTS.CHLN_REM_ADV,0.0));
			challanList = cr.list();
			challanList = cr.list();
			System.out.println("size of challanList = "+challanList.size());
			if(!challanList.isEmpty()){
				for(int i=0;i<challanList.size();i++){
					/*//LhpvAdv lhpvAdv = challanList.get(i).getLhpvAdv();
					LhpvBal lhpvBal = challanList.get(i).getLhpvBal();
					//int arId = challanList.get(i).getChlnArId();
					if(lhpvAdv != null && lhpvBal == null){
						chlnList.add(challanList.get(i).getChlnCode());
					}
					//if(lhpvBal == null && arId > 0){
					if(lhpvBal == null){	
						chlnList.add(challanList.get(i).getChlnCode());
					}*/
					chlnList.add(challanList.get(i).getChlnCode());
				}
			}
			System.out.println("size of chlnList = "+chlnList.size());
			challanList.clear();
			session.flush();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return chlnList;
		
	}

	@Override
	public Map<String, Object> verifyChlnForLhpvAdv(Map<String, String> chaln) {
		System.out.println("ChallanDAOImpl.verifyChlnForLhpvAdv()");
		logger.info("Enter into verifyChlnForLhpvAdv");
		Map<String, Object> map = new HashMap<>();
		List<String> chlnCodeList;
		Date lhpvDate=java.sql.Date.valueOf(chaln.get("date"));
		System.out.println("lhpvDate="+lhpvDate);
		logger.info("lhpv Date : "+lhpvDate);
		try {
			session = this.sessionFactory.openSession();
				Query query = session.createQuery("SELECT chln.chlnCode FROM Challan chln " +
						"WHERE chln.chlnCode = :chlnCode and chln.chlnRemAdv > :chlnRemAdv and chln.chlnDt <= :date");
				query.setString("chlnCode", chaln.get("chlnCode"));
				query.setParameter("date", lhpvDate);
				query.setDouble("chlnRemAdv", 0.0);
			    chlnCodeList = query.list();
			    System.out.println("OTHERSSSSSS");			
			if (chlnCodeList.isEmpty()) {
				map.put("msg", "Please Enter Valid Challan");
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			} else {
				Criteria chdCr = session.createCriteria(ChallanDetail.class);
				chdCr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE, chlnCodeList.get(0)));
				List<ChallanDetail> chdList = chdCr.list();
				if (chdList.isEmpty()) {
					map.put("msg", "challan Detail Not Found");
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				} else {
					if (chdList.get(0).getChdPanHdrType() == null) {
						map.put("msg", "PAN HOLDER Type not define in challan");
						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					} else {
						if (chdList.get(0).getChdPanHdrType().equalsIgnoreCase("Broker")) {
							Query brkQuery = session.createQuery("SELECT brk.brkFaCode FROM Broker brk WHERE brk.brkCode = :brkCode");
							brkQuery.setString("brkCode", chdList.get(0).getChdBrCode());
							List<String> brkFaCodeList = brkQuery.list();
							map.put("faCode", brkFaCodeList.get(0));
							map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
						} else { //consider owner
							Query ownQuery = session.createQuery("SELECT own.ownFaCode FROM Owner own WHERE own.ownCode = :ownCode");
							ownQuery.setString("ownCode", chdList.get(0).getChdOwnCode());
							List<String> ownFaCodeList = ownQuery.list();
							map.put("faCode", ownFaCodeList.get(0));
							map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
						}
					}
				}
			}
		} catch (Exception e) {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			e.printStackTrace();
		} finally {
			session.clear();
			session.close();
		}
		
		return map;
	}
	
	@Override
	public Map<String, Object> verifyChlnForLhpvBal(Map clientMap) {
		System.out.println("ChallanDAOImpl.verifyChlnForLhpvBal()");
		Map<String, Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		String chlnCode=clientMap.get("chlnNo").toString();
		String type=clientMap.get("type").toString();
		System.out.println("type= "+type);
		
		String acntHldr=null;
		if(type.equalsIgnoreCase("bank")) {
			acntHldr=clientMap.get("acntHldr").toString();
		}
		
		try {
			session = this.sessionFactory.openSession();
			
			
			Query query = null;
			
			if (currentUser.getUserBranchCode().equalsIgnoreCase("23")) {
				query = session.createQuery("SELECT chln.chlnCode FROM Challan chln " +
						"WHERE chln.chlnCode = :chlnCode and chln.chlnRemBal > :chlnRemBal");
				query.setString("chlnCode", chlnCode);
				query.setDouble("chlnRemBal", 0.0);	
			} else {
				query = session.createQuery("SELECT chln.chlnCode FROM Challan chln " +
						"WHERE chln.chlnCode = :chlnCode and chln.chlnRemBal > :chlnRemBal and (chln.chlnArId > :chlnArId or" +
						" chln.chlnDt<='2017-10-15')");
				query.setString("chlnCode", chlnCode);
				query.setDouble("chlnRemBal", 0.0);
				query.setDouble("chlnArId", 0.0);
			}
			
			List<String> chlnCodeList = query.list();
			if (chlnCodeList.isEmpty()) {
				map.put("msg", "Please Enter Valid Challan");
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			} else {
				
				Date d=new Date();
				Long dt=d.getTime();
				java.sql.Date date=new java.sql.Date(dt);
				
				List<String> chList=session.createSQLQuery("select chlnCode from challan where chlnCode='"+chlnCode+"'"
						+ " and (((To_days('"+date+"')- To_days(chlnDt)) <=60) OR chlnBalPayAlw=1)").list();
				
				if(chList.isEmpty()) {
					map.put("msg", "Challan date can't be 60 days older");
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					return map;
				}
				
				if(type.equalsIgnoreCase("card")) {
					
				}else {
					
					Query ar = session.createSQLQuery("select ch.chlnCode as chlnCode " +
							"from challan as ch left join arrivalreport ar on ch.chlnArId=ar.arId " + 
							" left join challandetail as chd on ch.chlnCode=chd.chdChlnCode " +
							"where (((To_days('"+date+"')- To_days(arIssueDt))>=7) or ch.bCode in(2,24,16) or chdOwnCode in('own2238') or chlnAdvance=0) and chlnCode='"+chlnCode+"'");
					if(ar.list().isEmpty()){
						map.put("msg", "AR issue date must be 7 days older");
						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
						return map;
					}
				}
				
				
				
				Criteria chdCr = session.createCriteria(ChallanDetail.class);
				chdCr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE, chlnCodeList.get(0)));
				List<ChallanDetail> chdList = chdCr.list();
				if (chdList.isEmpty()) {
					map.put("msg", "challan Detail Not Found");
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				} else {
					
					if (type.equalsIgnoreCase("bank") && acntHldr==null) {
						map.put("msg", "Please select Account holder");
						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					}else if (chdList.get(0).getChdPanHdrType() == null) {
						map.put("msg", "PAN HOLDER Type not define in challan");
						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					} else {
						//for ackValidation
//						Query cnQuery =session.createSQLQuery("select cn.cnmtCode from cnmt cn" +
//								" right join cnmt_challan cc on cn.cnmtId=cc.cnmtId " +
//								"right join challan ch on cc.chlnId=ch.chlnId " +
//								"where chlnCode=:chlnCode");
//						cnQuery.setString("chlnCode", chlnCodeList.get(0));
//						List cnList=cnQuery.list();
//						
//						List<Acknowledge_Validation> ackList=session.createCriteria(Acknowledge_Validation.class)
//								.add(Restrictions.eq("cnmtCode", cnList.get(0))).list();
//						
//						if(ackList.isEmpty()){
						
						if(type.equalsIgnoreCase("bank")) {
							
							if(acntHldr.equalsIgnoreCase("Broker")) {
								List<Broker> brkFaCodeList=session.createCriteria(Broker.class)
										.add(Restrictions.eq("brkCode", chdList.get(0).getChdBrCode())).list();
								
								map.put("acNo", brkFaCodeList.get(0).getBrkAccntNo());
								map.put("bankName", brkFaCodeList.get(0).getBrkBnkBranch());
								map.put("acName", brkFaCodeList.get(0).getBrkAcntHldrName());
								map.put("ifsc", brkFaCodeList.get(0).getBrkIfsc());
								map.put("name", brkFaCodeList.get(0).getBrkName());
								map.put("validAc", brkFaCodeList.get(0).isBrkBnkDetValid());
								
							}else {
								List<Owner> ownFaCodeList=session.createCriteria(Owner.class)
										.add(Restrictions.eq("ownCode", chdList.get(0).getChdOwnCode())).list();
								
							 map.put("acNo", ownFaCodeList.get(0).getOwnAccntNo());
								map.put("bankName", ownFaCodeList.get(0).getOwnBnkBranch());
								map.put("acName", ownFaCodeList.get(0).getOwnAcntHldrName());
								map.put("ifsc", ownFaCodeList.get(0).getOwnIfsc());
								map.put("name", ownFaCodeList.get(0).getOwnName());
								map.put("validAc", ownFaCodeList.get(0).isOwnBnkDetValid());
							}
						}
						
							if (chdList.get(0).getChdPanHdrType().equalsIgnoreCase("Broker")) {
//						if (panHldr.equalsIgnoreCase("Broker")) {
//								Query brkQuery = session.createQuery("SELECT brk.brkFaCode FROM Broker brk WHERE brk.brkCode = :brkCode and brkValidPan=true");
//								brkQuery.setString("brkCode", chdList.get(0).getChdBrCode());
//								List<String> brkFaCodeList = brkQuery.list();
							
							List<Broker> brkFaCodeList=session.createCriteria(Broker.class)
									.add(Restrictions.eq("brkCode", chdList.get(0).getChdBrCode()))
									.add(Restrictions.eq("brkValidPan", true)).list();
								
							
							/*Query brkQuery = session.createQuery("SELECT brk.brkFaCode FROM Broker brk WHERE brk.brkCode = :brkCode and brkValidPan=true");
							brkQuery.setString("brkCode", chdList.get(0).getChdBrCode());
							List<String> brkFaCodeList = brkQuery.list();
							*/
								if(brkFaCodeList.isEmpty()){
									map.put("msg", "Please validate Broker PAN no.");
									map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
									return map;
								}
								
								map.put("faCode", brkFaCodeList.get(0).getBrkFaCode());
								/*map.put("acNo", brkFaCodeList.get(0).getBrkAccntNo());
								map.put("bankName", brkFaCodeList.get(0).getBrkBnkBranch());
								map.put("acName", brkFaCodeList.get(0).getBrkAcntHldrName());
								map.put("ifsc", brkFaCodeList.get(0).getBrkIfsc());
								map.put("name", brkFaCodeList.get(0).getBrkName());
								map.put("validAc", brkFaCodeList.get(0).isBrkBnkDetValid());
								*/map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
							} else { //consider owner
//								Query ownQuery = session.createQuery("SELECT own.ownFaCode FROM Owner own WHERE own.ownCode = :ownCode and ownValidPan=true");
//								ownQuery.setString("ownCode", chdList.get(0).getChdOwnCode());
//								List<String> ownFaCodeList = ownQuery.list();
								
								List<Owner> ownFaCodeList=session.createCriteria(Owner.class)
										.add(Restrictions.eq("ownCode", chdList.get(0).getChdOwnCode()))
										.add(Restrictions.eq("ownValidPan", true)).list();
								
								if(ownFaCodeList.isEmpty()){
									map.put("msg", "Please validate owner PAN no.");
									map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
									return map;
								}
								
								map.put("faCode", ownFaCodeList.get(0).getOwnFaCode());
								/* map.put("acNo", ownFaCodeList.get(0).getOwnAccntNo());
									map.put("bankName", ownFaCodeList.get(0).getOwnBnkBranch());
									map.put("acName", ownFaCodeList.get(0).getOwnAcntHldrName());
									map.put("ifsc", ownFaCodeList.get(0).getOwnIfsc());
									map.put("name", ownFaCodeList.get(0).getOwnName());
									map.put("validAc", ownFaCodeList.get(0).isOwnBnkDetValid());
								*/
								map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
							}
//						}else if(ackList.get(0).getCheck()==null){
//							map.put("msg", "Please Validate ACK First");
//							map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
//						}else if(ackList.get(0).getCheck().equalsIgnoreCase("Y")){
//							if (chdList.get(0).getChdPanHdrType().equalsIgnoreCase("Broker")) {
//								Query brkQuery = session.createQuery("SELECT brk.brkFaCode FROM Broker brk WHERE brk.brkCode = :brkCode");
//								brkQuery.setString("brkCode", chdList.get(0).getChdBrCode());
//								List<String> brkFaCodeList = brkQuery.list();
//								map.put("faCode", brkFaCodeList.get(0));
//								map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
//							} else { //consider owner
//								Query ownQuery = session.createQuery("SELECT own.ownFaCode FROM Owner own WHERE own.ownCode = :ownCode");
//								ownQuery.setString("ownCode", chdList.get(0).getChdOwnCode());
//								List<String> ownFaCodeList = ownQuery.list();
//								map.put("faCode", ownFaCodeList.get(0));
//								map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
//							}
//						}else {
//							map.put("msg", "Please Validate ACK First");
//							map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
//						}
						
						
					}
				}
			}
		} catch (Exception e) {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			e.printStackTrace();
		} finally {
			session.clear();
			session.close();
		}
		
		return map;
	}
	
	
	
	@Override
	@Transactional
	public Map<String, Object> verifyChlnForLhpvBalHO(String chlnCode) {
		System.out.println("ChallanDAOImpl.verifyChlnForLhpvBalHO()");
		Map<String, Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		try {
			session = this.sessionFactory.openSession();
			
			Query query = null;
			
			if (currentUser.getUserBranchCode().equalsIgnoreCase("23")) {
				query = session.createQuery("SELECT chln.chlnCode FROM Challan chln " +
						"WHERE chln.chlnCode = :chlnCode and chln.chlnRemBal > :chlnRemBal");
				query.setString("chlnCode", chlnCode);
				query.setDouble("chlnRemBal", 0.0);	
			} else {
				query = session.createQuery("SELECT chln.chlnCode FROM Challan chln " +
						"WHERE chln.chlnCode = :chlnCode and chln.chlnRemBal > :chlnRemBal and (chln.chlnArId > :chlnArId or" +
						" chln.chlnDt<='2017-10-15')");
				query.setString("chlnCode", chlnCode);
				query.setDouble("chlnRemBal", 0.0);
				query.setDouble("chlnArId", 0.0);
			}
			
			List<String> chlnCodeList = query.list();
			if (chlnCodeList.isEmpty()) {
				map.put("msg", "Please Enter Valid Challan");
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			} else {
				Criteria chdCr = session.createCriteria(ChallanDetail.class);
				chdCr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE, chlnCodeList.get(0)));
				List<ChallanDetail> chdList = chdCr.list();
				if (chdList.isEmpty()) {
					map.put("msg", "challan Detail Not Found");
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				} else {
					if (chdList.get(0).getChdPanHdrType() == null) {
						map.put("msg", "PAN HOLDER Type not define in challan");
						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					} else {
						//for ackValidation
//						Query cnQuery =session.createSQLQuery("select cn.cnmtCode from cnmt cn" +
//								" right join cnmt_challan cc on cn.cnmtId=cc.cnmtId " +
//								"right join challan ch on cc.chlnId=ch.chlnId " +
//								"where chlnCode=:chlnCode");
//						cnQuery.setString("chlnCode", chlnCodeList.get(0));
//						List cnList=cnQuery.list();
//						
//						List<Acknowledge_Validation> ackList=session.createCriteria(Acknowledge_Validation.class)
//								.add(Restrictions.eq("cnmtCode", cnList.get(0))).list();
//						
//						if(ackList.isEmpty()){
							if (chdList.get(0).getChdPanHdrType().equalsIgnoreCase("Broker")) {
								Query brkQuery = session.createQuery("SELECT brk.brkFaCode FROM Broker brk WHERE brk.brkCode = :brkCode and brkValidPan=true");
								brkQuery.setString("brkCode", chdList.get(0).getChdBrCode());
								List<String> brkFaCodeList = brkQuery.list();
								if(brkFaCodeList.isEmpty()){
									map.put("msg", "Please validate Broker PAN no.");
									map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
									return map;
								}
								
								map.put("faCode", brkFaCodeList.get(0));
								map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
							} else { //consider owner
								Query ownQuery = session.createQuery("SELECT own.ownFaCode FROM Owner own WHERE own.ownCode = :ownCode and ownValidPan=true");
								ownQuery.setString("ownCode", chdList.get(0).getChdOwnCode());
								List<String> ownFaCodeList = ownQuery.list();
								
								if(ownFaCodeList.isEmpty()){
									map.put("msg", "Please validate owner PAN no.");
									map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
									return map;
								}
								
								map.put("faCode", ownFaCodeList.get(0));
								map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
							}
//						}else if(ackList.get(0).getCheck()==null){
//							map.put("msg", "Please Validate ACK First");
//							map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
//						}else if(ackList.get(0).getCheck().equalsIgnoreCase("Y")){
//							if (chdList.get(0).getChdPanHdrType().equalsIgnoreCase("Broker")) {
//								Query brkQuery = session.createQuery("SELECT brk.brkFaCode FROM Broker brk WHERE brk.brkCode = :brkCode");
//								brkQuery.setString("brkCode", chdList.get(0).getChdBrCode());
//								List<String> brkFaCodeList = brkQuery.list();
//								map.put("faCode", brkFaCodeList.get(0));
//								map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
//							} else { //consider owner
//								Query ownQuery = session.createQuery("SELECT own.ownFaCode FROM Owner own WHERE own.ownCode = :ownCode");
//								ownQuery.setString("ownCode", chdList.get(0).getChdOwnCode());
//								List<String> ownFaCodeList = ownQuery.list();
//								map.put("faCode", ownFaCodeList.get(0));
//								map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
//							}
//						}else {
//							map.put("msg", "Please Validate ACK First");
//							map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
//						}
						
						
					}
				}
			}
		} catch (Exception e) {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			e.printStackTrace();
		} finally {
			session.clear();
			session.close();
		}
		
		return map;
	}
	
	
	
	
	@Override
	@Transactional
	public Map<String, Object> verifyChlnForLhpvSup(String chlnCode) {
		System.out.println("ChallanDAOImpl.verifyChlnForLhpvSup()");
		Map<String, Object> map = new HashMap<>();
		
		try {
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("SELECT chln.chlnCode FROM Challan chln " +
					"WHERE chln.chlnCode = :chlnCode and chln.chlnRemAdv = :chlnRemAdv and " +
					"chln.chlnRemBal = :chlnRemBal and chln.chlnArId > :chlnArId");
			query.setString("chlnCode", chlnCode);
			query.setDouble("chlnRemAdv", 0.0);
			query.setDouble("chlnRemBal", 0.0);
			query.setDouble("chlnArId", 0.0);
			
			List<String> chlnCodeList = query.list();
			if (chlnCodeList.isEmpty()) {
				map.put("msg", "Please Enter Valid Challan");
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			} else {
				Criteria chdCr = session.createCriteria(ChallanDetail.class);
				chdCr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE, chlnCodeList.get(0)));
				List<ChallanDetail> chdList = chdCr.list();
				if (chdList.isEmpty()) {
					map.put("msg", "challan Detail Not Found");
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				} else {
					if (chdList.get(0).getChdPanHdrType() == null) {
						map.put("msg", "PAN HOLDER Type not define in challan");
						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					} else {
						if (chdList.get(0).getChdPanHdrType().equalsIgnoreCase("Broker")) {
							Query brkQuery = session.createQuery("SELECT brk.brkFaCode FROM Broker brk WHERE brk.brkCode = :brkCode");
							brkQuery.setString("brkCode", chdList.get(0).getChdBrCode());
							List<String> brkFaCodeList = brkQuery.list();
							map.put("faCode", brkFaCodeList.get(0));
							map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
						} else { //consider owner
							Query ownQuery = session.createQuery("SELECT own.ownFaCode FROM Owner own WHERE own.ownCode = :ownCode");
							ownQuery.setString("ownCode", chdList.get(0).getChdOwnCode());
							List<String> ownFaCodeList = ownQuery.list();
							map.put("faCode", ownFaCodeList.get(0));
							map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
						}
					}
				}
			}
		} catch (Exception e) {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			e.printStackTrace();
		} finally {
			session.clear();
			session.close();
		}
		
		return map;
	}
	
	
	@Override
	@Transactional
	public Map<String, Object> verifyChlnForLhpvAdvRvrs(Map<String,String> chlnCheq) {
		System.out.println("ChallanDAOImpl.verifyChlnForLhpvAdvRvrs()");
		Map<String, Object> map = new HashMap<>();
		List<String> chlnCodeList;
		List<LhpvAdv> chlnDtailList;
		List<Integer> chlnIdList;
		List<Integer> LaIdList;
		System.out.println(chlnCheq.get("chlnNo"));
		System.out.println(chlnCheq.get("chqNo"));
		if(chlnCheq.get("chqNo")=="" || chlnCheq.get("chqNo")== null){
			map.put("msg", "Please Enter Cheq Number");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			return map;
		}
		
		try {
			session = this.sessionFactory.openSession();
				Query query = session.createQuery("SELECT chln.chlnCode,chln.chlnId FROM Challan chln " +
						"WHERE chln.chlnCode = :chlnCode AND chln.chlnAdvance > chln.chlnRemAdv");
				query.setString("chlnCode",chlnCheq.get("chlnNo"));
				 chlnCodeList = query.list();
				 System.out.println("DPSSSSSSSSSSSS");
			
			if (chlnCodeList.isEmpty()) {
				map.put("msg", "Please Enter Valid Challan");
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				return map;
			} else {
				
				SQLQuery sqlChlnIdList = session.createSQLQuery("SELECT chlnId FROM challan WHERE chlnCode=:chlnCode");
				sqlChlnIdList.setParameter("chlnCode", chlnCheq.get("chlnNo"));
				chlnIdList=sqlChlnIdList.list();
				int chlnId=chlnIdList.get(0);
				
				SQLQuery sqlLaIdList= session.createSQLQuery("SELECT laId FROM lhpvadv_challan WHERE chlnId=:chlnId ORDER BY laId DESC");
				sqlLaIdList.setParameter("chlnId",chlnId);
				LaIdList=sqlLaIdList.list();
				
				int test=0;
				
				for(Integer i : LaIdList){
				Criteria cr=session.createCriteria(LhpvAdv.class);
				cr.add(Restrictions.eq(LhpvAdvCNTS.LHPV_ADV_LAID, i));
				cr.add(Restrictions.eq(LhpvAdvCNTS.LHPV_ADV_CHQ_NO, chlnCheq.get("chqNo")));
				cr.add(Restrictions.eq(LhpvAdvCNTS.LHPV_ADV_IS_CLOSE, true));
				 chlnDtailList=cr.list();
				
				System.out.println("i="+i);
				
					if(!chlnDtailList.isEmpty()){
						
						System.out.println(chlnDtailList.get(0).getbCode());
						
						SQLQuery qry=session.createSQLQuery("SELECT chequeleaves.chqLChqNo FROM chequeleaves INNER JOIN bankmstr ON chequeleaves.bnkId=bankmstr.bnkId WHERE bankmstr.bnkFaCode=:bnkFaCode AND chequeleaves.chqLChqNo=:chqueNo AND chqLCancel = 0");
						qry.setParameter("bnkFaCode", chlnDtailList.get(0).getLaBankCode());
						qry.setParameter("chqueNo",chlnCheq.get("chqNo"));
						List<String> chqnolist=qry.list();
						System.out.println("Size ====="+chqnolist.size());
						System.out.println(chlnCheq.get("chqNo"));
						System.out.println(chlnDtailList.get(0).getLaBankCode());
						if(!chqnolist.isEmpty()){
						map.put("faCode", (chlnDtailList.get(0).getLaBrkOwn()));
						map.put("loryAdvP", (chlnDtailList.get(0).getLaLryAdvP()));
						map.put("cashDisc", (chlnDtailList.get(0).getLaCashDiscR()));
						map.put("munsiana", (chlnDtailList.get(0).getLaMunsR()));
						map.put("tdsAmt", (chlnDtailList.get(0).getLaTdsR()));
						map.put("payAmt", (chlnDtailList.get(0).getLaTotPayAmt()));
						map.put("recvrAmt", (chlnDtailList.get(0).getLaTotRcvrAmt()));
						map.put("finlTotAmt", (chlnDtailList.get(0).getLaFinalTot()));
						map.put("payBy", (chlnDtailList.get(0).getLaPayMethod()));
						map.put("cheqNo", (chlnDtailList.get(0).getLaChqNo()));
						map.put("cheqType", (chlnDtailList.get(0).getLaChqType()));
						map.put("bankCode", (chlnDtailList.get(0).getLaBankCode()));
						
						test=1;
						}else{
							map.put("msg", "Please Enter Valid ChequeNo");
							map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
							return map;
						}
						
					}
				}
				
				if(test==1){
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put("msg", "challan Detail Not Found");
					map.put(ConstantsValues.ERROR, ConstantsValues.ERROR);
					
				}
			}
		} catch (Exception e) {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			e.printStackTrace();
		} finally {
			session.clear();
			session.close();
		}
		
		return map;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public int chlnDtlFrCncl(Map<String, String> chlnDtl){
		 System.out.println("chlnDtlFrCncl()");
		System.out.println("chlncode="+chlnDtl.get("chlnCode"));
		System.out.println("Lorry No.=="+chlnDtl.get("lorryNo"));
		System.out.println("Date=="+chlnDtl.get("date"));
		String date=chlnDtl.get("date");
		String chlnCode=chlnDtl.get("chlnCode");
		String status="chln";
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		int temp=0;
		
		try{
			
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			Query query=session.createQuery("SELECT brsld FROM BranchStockLeafDet brsld WHERE brsLeafDetStatus = :status  AND brsLeafDetSNo = :chlnCode");
			query.setParameter("status", status);
			query.setParameter("chlnCode", chlnCode);
			List<BranchStockLeafDet> list=query.list();

			if(!list.isEmpty()){
				BranchSStats branchSStats = new BranchSStats();
				branchSStats.setBrsStatsBrCode(list.get(0).getBrsLeafDetBrCode());
				branchSStats.setBrsStatsType("chln");
				branchSStats.setBrsStatsSerialNo(list.get(0).getBrsLeafDetSNo());
				branchSStats.setUserCode(currentUser.getUserCode());
				branchSStats.setbCode(currentUser.getUserBranchCode());
				branchSStats.setBrsStatsDt( java.sql.Date.valueOf(date));
				session.save(branchSStats);
				BranchStockLeafDet branchStockLeafDet=list.get(0);
				session.delete(branchStockLeafDet);
				
					Challan challan=new Challan();
					challan.setChlnCode(chlnCode);
					challan.setbCode(list.get(0).getBrsLeafDetBrCode());
					challan.setChlnDt(java.sql.Date.valueOf(date));
					challan.setChlnLryNo(chlnDtl.get("lorryNo"));
					challan.setCancel(true);
					session.save(challan);
					session.flush();
					session.clear();
					transaction.commit();
					temp=1;
					
			}else{
				temp=0;
			}
		}catch(Exception e){
				transaction.rollback();
			e.printStackTrace();
			temp=0;
		}
		session.close();
		return temp;
	}
	
	
	public Map<String, Object> verifyChlnForLhpvBalRvrs(Map<String,String> chlnData){
		System.out.println("ChallanDAOImpl.verifyChlnForLhpvBalRvrs()");
		String payBy=chlnData.get("payBy");
		String chqNo,bnkCode,chlnCode,brkOwn;
		Map<String, Object> map = new HashMap<>();
		List<String> chlnCodeList;
		List<LhpvBal> chlnDtailList;
		List<Integer> chlnIdList;
		List<Integer> LbIdList;
		List<Challan>chlnCdList= getChallanList(chlnCode=chlnData.get("chlnCode"));
		 
		if(payBy.equals("Q")){
			 chqNo=chlnData.get("chqNo");
			 bnkCode=chlnData.get("bankCode");
			 chlnCode=chlnData.get("chlnCode");
			 brkOwn=chlnData.get("brkOwnFaCode");
			 
				 try{
					 
						session = this.sessionFactory.openSession();
						Query query = session.createQuery("SELECT chln.chlnCode,chln.chlnId FROM Challan chln " +
								"WHERE chln.chlnCode = :chlnCode AND chln.chlnBalance > chln.chlnRemBal");
						query.setString("chlnCode",chlnCode);
						 chlnCodeList = query.list();
						 System.out.println("DPSSSSSSSSSSSS");
						 
							if (chlnCodeList.isEmpty()) {
								map.put("msg", "Please Enter Valid Challan");
								map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
								return map;
							} else {
								
								SQLQuery sqlChlnIdList = session.createSQLQuery("SELECT chlnId FROM challan WHERE chlnCode=:chlnCode");
								sqlChlnIdList.setParameter("chlnCode", chlnCode);
								chlnIdList=sqlChlnIdList.list();
								int chlnId=chlnIdList.get(0);
								
								SQLQuery sqlLbIdList= session.createSQLQuery("SELECT lbId FROM lhpvbal_challan WHERE chlnId=:chlnId ORDER BY lbId DESC");
								sqlLbIdList.setParameter("chlnId",chlnId);
								LbIdList=sqlLbIdList.list();
								
								int test=0;
								
								for(Integer i : LbIdList){
								Criteria cr=session.createCriteria(LhpvBal.class);
								cr.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_ID, i));
								cr.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_CHQ_NO, chqNo));
								cr.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_IS_CLOSE, true));
								 chlnDtailList=cr.list();
								
								System.out.println("i="+i);
								
									if(!chlnDtailList.isEmpty()){
										
										System.out.println(chlnDtailList.get(0).getbCode());
										SQLQuery qry=session.createSQLQuery("SELECT chequeleaves.chqLChqNo FROM chequeleaves INNER JOIN bankmstr ON chequeleaves.bnkId=bankmstr.bnkId WHERE bankmstr.bnkFaCode=:bnkFaCode AND chequeleaves.chqLChqNo=:chqueNo AND chqLCancel = 0");
										qry.setParameter("bnkFaCode", chlnDtailList.get(0).getLbBankCode());
										qry.setParameter("chqueNo",chqNo);
										List<String> chqnolist=qry.list();
										System.out.println("Size ====="+chqnolist.size());
										System.out.println(chqNo);
										System.out.println(chlnDtailList.get(0).getLbBankCode());
										if(!chqnolist.isEmpty()){
											map.put("challan", chlnCdList.get(0));
											map.put("faCode", (chlnDtailList.get(0).getLbBrkOwn()));
											map.put("loryBalP", (chlnDtailList.get(0).getLbLryBalP()));
											map.put("cashDisc", (chlnDtailList.get(0).getLbCashDiscR()));
											map.put("munsiana", (chlnDtailList.get(0).getLbMunsR()));
											map.put("tdsAmt", (chlnDtailList.get(0).getLbTdsR()));
											map.put("payAmt", (chlnDtailList.get(0).getLbTotPayAmt()));
											map.put("recvrAmt", (chlnDtailList.get(0).getLbTotRcvrAmt()));
											map.put("finlTotAmt", (chlnDtailList.get(0).getLbFinalTot()));
											map.put("payBy", (chlnDtailList.get(0).getLbPayMethod()));
											map.put("cheqNo", (chlnDtailList.get(0).getLbChqNo()));
											map.put("cheqType", (chlnDtailList.get(0).getLbChqType()));
											map.put("bankCode", (chlnDtailList.get(0).getLbBankCode()));
											map.put("drvrClaim", chlnDtailList.get(0).getLbDrRcvrWtCR());
											map.put("wtSrtg", chlnDtailList.get(0).getLbWtShrtgCR());
											map.put("lateDlvr", chlnDtailList.get(0).getLbLateDelCR());
											map.put("lateAck", chlnDtailList.get(0).getLbLateAckCR());
											map.put("extKM", chlnDtailList.get(0).getLbOthExtKmP());
											map.put("ovrHgt", chlnDtailList.get(0).getLbOthOvrHgtP());
											map.put("penalty", chlnDtailList.get(0).getLbOthPnltyP());
											map.put("othrMisc", chlnDtailList.get(0).getLbOthMiscP());
											map.put("dtnsn", chlnDtailList.get(0).getLbUnpDetP());
											map.put("unLding", chlnDtailList.get(0).getLbUnLoadingP());
											map.put("rcvryAmt", chlnDtailList.get(0).getLbTotRcvrAmt());
											map.put("desc", chlnDtailList.get(0).getLbDesc());
										
											test=1;
										}else{
											map.put("msg", "Please Enter Valid Detail");
											map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
											return map;
										}
										
									}
								}
								
								if(test==1){
									map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
								}else{
									map.put("msg", "challan Detail Not Found");
									map.put(ConstantsValues.ERROR, ConstantsValues.ERROR);
									
								}
							}
					 
					 
				 }catch(Exception e){
					 e.printStackTrace();
				 }
				 
			 
			 
			 
			 
			 
		}else if(payBy.equals("R")){
			//String chqNo=chlnData.get("chqNo");
			 bnkCode=chlnData.get("bankCode");
			 chlnCode=chlnData.get("chlnCode");
			 brkOwn=chlnData.get("brkOwnFaCode");
			 
			 map.put("msg", "only By Cheque Entry can be reversed");
			 
		}else if(payBy.equals("C")){
			//String chqNo=chlnData.get("chqNo");
			//String bnkCode=chlnData.get("bankCode");
			 chlnCode=chlnData.get("chlnCode");
			 brkOwn=chlnData.get("brkOwnFaCode");
			 map.put("msg", "only By Cheque Entry can be reversed");
			 
		}else{
			map.put(ConstantsValues.ERROR, ConstantsValues.ERROR);
		}
	
		
		
		return map;
	}
	
	
	//by kamal
	public List<Challan> getChallanByCode(String chlnCode){
		List<Challan>chlnList=new ArrayList<Challan>();
		System.out.println("challanCode=="+chlnCode);
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Challan.class)
					.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE, chlnCode));
			chlnList=cr.list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return chlnList;
	}
	
	@Override
	public List<Challan> getChallanByCodeFrAr(String chlnCode){
		List<Challan>chlnList=new ArrayList<Challan>();
		System.out.println("challanCode=="+chlnCode);
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Challan.class)
					.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE, chlnCode))
					.add(Restrictions.eq(ChallanCNTS.CHLN_AR_ID, -1))
					.add(Restrictions.eq(ChallanCNTS.CHLN_CANCEL, false));
			chlnList=cr.list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return chlnList;
	}
	
	@Override
	public Map<String,Object> getChallanForHoLbPay(String bankCode){
		Map<String,Object> map=new HashMap<>();
		Session session=this.sessionFactory.openSession();
		
		Date d=new Date();
		Long dt=d.getTime();
		java.sql.Date date=new java.sql.Date(dt);
		//System.out.println(date);
		
		Query query;
		//if(bankCode.equalsIgnoreCase("0700044") ){
		if(bankCode.equalsIgnoreCase("P") ){
			query=session.createSQLQuery("select ch.chlnCode as chlnCode,ch.bCode as bCode,ch.chlnPayAt as chlnPayAt,ch.cardFaCode as cardFaCode, " +
					"ch.bankName as bankName,ch.payeeName as payeeName,ch.accountNo as accountNo,ch.ifscCode as ifscCode,brh.branchName as branchName" +
					" from challan as ch left join branch as brh on ch.bCode=brh.branchCode" +
					" where ch.chlnRemBal>0.0 and ch.cardFaCode is not null and ch.cardFaCode !='' and ch.bankName in('HP','BP','IO')");
		/*}else if(bankCode.equalsIgnoreCase("0700046")){
				query=session.createSQLQuery("select ch.chlnCode as chlnCode,ch.bCode as bCode,ch.chlnPayAt as chlnPayAt," +
						"ch.bankName as bankName,ch.payeeName as payeeName,ch.accountNo as accountNo,ch.ifscCode as ifscCode,brh.branchName as branchName" +
						" from challan as ch left join branch as brh on ch.bCode=brh.branchCode" +
						" where ch.chlnRemBal>0.0 and ch.ifscCode is null and ch.bankName ='BP'");
		}else if(bankCode.equalsIgnoreCase("0700047")){
					query=session.createSQLQuery("select ch.chlnCode as chlnCode,ch.bCode as bCode,ch.chlnPayAt as chlnPayAt," +
							"ch.bankName as bankName,ch.payeeName as payeeName,ch.accountNo as accountNo,ch.ifscCode as ifscCode,brh.branchName as branchName" +
							" from challan as ch left join branch as brh on ch.bCode=brh.branchCode" +
							" where ch.chlnRemBal>0.0 and ch.ifscCode is null and ch.bankName ='IO'");
		*/}else{
			query=session.createSQLQuery("select ch.chlnCode as chlnCode,ch.bCode as bCode,ch.chlnPayAt as chlnPayAt,chd.chdPanHdrType as chdPanHdrType, " +
					" ch.bankName as bankName,ch.payeeName as payeeName,ch.accountNo as accountNo,ch.ifscCode as ifscCode,brh.branchName as branchName," +
					" chd.chdBrCode as brkCode, own.ownCode as ownCode, brk.brkName as brkName,own.ownName as ownName "
					+ " from challan as ch left join arrivalreport ar on ch.chlnArId=ar.arId" +
					" left join branch as brh on ch.bCode=brh.branchCode" +
					" left join challandetail as chd on ch.chlnCode=chd.chdChlnCode" +
					" left join broker as brk on chd.chdBrCode=brk.brkCode "
					+ " left join owner as own on chd.chdOwnCode=own.ownCode " +
					" where ch.chlnRemBal>0.0 and (((To_days('"+date+"')- To_days(arIssueDt))>=7) or ch.bCode in(2,24,16) or chd.chdOwnCode in('own2238'))" +
					" and ch.ifscCode is not null and ch.bankName is not null");
		}
		
		List<Map<String, Object>> list=query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
		if(!list.isEmpty()){
			map.put("chlnList", list);
			map.put("result", "success");
		}else{
			map.put("result", "error");
		}
		session.close();

		return map;
		
	}
	
	
	/*  
		@SuppressWarnings("unchecked")
	    public String getChlnCodeById(int chlnId){
			System.out.println("enter into getChlnCodeById function");
			String chlnCode = "";
			Challan challan = new Challan();
			try{
				session = this.sessionFactory.openSession();
				Criteria cr=session.createCriteria(Challan.class);
				cr.add(Restrictions.eq(ChallanCNTS.CHLN_ID,chlnId));
				challan = (Challan) cr.list().get(0);
				chlnCode = challan.getChlnCode();
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			} 

			session.close();
			return chlnCode;
		}
	 */	  
	
	
	
	
	@Override
	public Map<String, Object> verifyChlnForLhpvBalOld(String chlnCode) {
		System.out.println("ChallanDAOImpl.verifyChlnForLhpvBal()");
		Map<String, Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		try {
			session = this.sessionFactory.openSession();
			
			Query query = null;
			
			if (currentUser.getUserBranchCode().equalsIgnoreCase("23")) {
				query = session.createQuery("SELECT chln.chlnCode FROM Challan chln " +
						"WHERE chln.chlnCode = :chlnCode and chln.chlnRemBal > :chlnRemBal");
				query.setString("chlnCode", chlnCode);
				query.setDouble("chlnRemBal", 0.0);	
			} else {
				query = session.createQuery("SELECT chln.chlnCode FROM Challan chln " +
						"WHERE chln.chlnCode = :chlnCode and chln.chlnRemBal > :chlnRemBal and (chln.chlnArId > :chlnArId or" +
						" chln.chlnDt<='2017-10-15')");
				query.setString("chlnCode", chlnCode);
				query.setDouble("chlnRemBal", 0.0);
				query.setDouble("chlnArId", 0.0);
			}
			
			List<String> chlnCodeList = query.list();
			if (chlnCodeList.isEmpty()) {
				map.put("msg", "Please Enter Valid Challan");
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			} else {
				/*Date d=new Date();
				Long dt=d.getTime();
				java.sql.Date date=new java.sql.Date(dt);
				
				Query ar = session.createSQLQuery("select ch.chlnCode as chlnCode " +
						"from challan as ch left join arrivalreport ar on ch.chlnArId=ar.arId " + 
						" left join challandetail as chd on ch.chlnCode=chd.chdChlnCode " +
						"where (((To_days('"+date+"')- To_days(arIssueDt))>=15) or ch.bCode in(2,24,16) or chdOwnCode in('own2238')) and chlnCode='"+chlnCode+"'");
				if(ar.list().isEmpty()){
					map.put("msg", "AR issue date must be 15 days older");
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					return map;
				}*/
				
				Criteria chdCr = session.createCriteria(ChallanDetail.class);
				chdCr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE, chlnCodeList.get(0)));
				List<ChallanDetail> chdList = chdCr.list();
				if (chdList.isEmpty()) {
					map.put("msg", "challan Detail Not Found");
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				} else {
					if (chdList.get(0).getChdPanHdrType() == null) {
						map.put("msg", "PAN HOLDER Type not define in challan");
						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					} else {
						//for ackValidation
//						Query cnQuery =session.createSQLQuery("select cn.cnmtCode from cnmt cn" +
//								" right join cnmt_challan cc on cn.cnmtId=cc.cnmtId " +
//								"right join challan ch on cc.chlnId=ch.chlnId " +
//								"where chlnCode=:chlnCode");
//						cnQuery.setString("chlnCode", chlnCodeList.get(0));
//						List cnList=cnQuery.list();
//						
//						List<Acknowledge_Validation> ackList=session.createCriteria(Acknowledge_Validation.class)
//								.add(Restrictions.eq("cnmtCode", cnList.get(0))).list();
//						
//						if(ackList.isEmpty()){
							if (chdList.get(0).getChdPanHdrType().equalsIgnoreCase("Broker")) {
								Query brkQuery = session.createQuery("SELECT brk.brkFaCode FROM Broker brk WHERE brk.brkCode = :brkCode and brkValidPan=true");
								brkQuery.setString("brkCode", chdList.get(0).getChdBrCode());
								List<String> brkFaCodeList = brkQuery.list();
								
								if(brkFaCodeList.isEmpty()){
									map.put("msg", "Please validate Broker PAN no.");
									map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
									return map;
								}
								
								map.put("faCode", brkFaCodeList.get(0));
								map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
							} else { //consider owner
								Query ownQuery = session.createQuery("SELECT own.ownFaCode FROM Owner own WHERE own.ownCode = :ownCode and ownValidPan=true");
								ownQuery.setString("ownCode", chdList.get(0).getChdOwnCode());
								List<String> ownFaCodeList = ownQuery.list();
								if(ownFaCodeList.isEmpty()){
									map.put("msg", "Please validate owner PAN no.");
									map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
									return map;
								}
								
								map.put("faCode", ownFaCodeList.get(0));
								map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
							}
//						}else if(ackList.get(0).getCheck()==null){
//							map.put("msg", "Please Validate ACK First");
//							map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
//						}else if(ackList.get(0).getCheck().equalsIgnoreCase("Y")){
//							if (chdList.get(0).getChdPanHdrType().equalsIgnoreCase("Broker")) {
//								Query brkQuery = session.createQuery("SELECT brk.brkFaCode FROM Broker brk WHERE brk.brkCode = :brkCode");
//								brkQuery.setString("brkCode", chdList.get(0).getChdBrCode());
//								List<String> brkFaCodeList = brkQuery.list();
//								map.put("faCode", brkFaCodeList.get(0));
//								map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
//							} else { //consider owner
//								Query ownQuery = session.createQuery("SELECT own.ownFaCode FROM Owner own WHERE own.ownCode = :ownCode");
//								ownQuery.setString("ownCode", chdList.get(0).getChdOwnCode());
//								List<String> ownFaCodeList = ownQuery.list();
//								map.put("faCode", ownFaCodeList.get(0));
//								map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
//							}
//						}else {
//							map.put("msg", "Please Validate ACK First");
//							map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
//						}
						
						
					}
				}
			}
		} catch (Exception e) {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			e.printStackTrace();
		} finally {
			session.clear();
			session.close();
		}
		
		return map;
	}
	
	
	@Override
	public Map<String, Object> verifyChlnForLhpvBalCash(String chlnCode) {
		System.out.println("ChallanDAOImpl.verifyChlnForLhpvBalCash()");
		Map<String, Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		try {
			session = this.sessionFactory.openSession();
			
			Query query = null;
			
			if (currentUser.getUserBranchCode().equalsIgnoreCase("23")) {
				query = session.createQuery("SELECT chln.chlnCode FROM Challan chln " +
						"WHERE chln.chlnCode = :chlnCode and chln.chlnRemBal > :chlnRemBal and chln.chlnBalance <= :balance");
				query.setString("chlnCode", chlnCode);
				query.setDouble("chlnRemBal", 0.0);
				query.setDouble("balance", 1000.0);
			} else {
				query = session.createQuery("SELECT chln.chlnCode FROM Challan chln " +
						"WHERE chln.chlnCode = :chlnCode and chln.chlnRemBal > :chlnRemBal and chln.chlnArId > :chlnArId and" +
						"  chln.chlnBalance <= :balance");
				query.setString("chlnCode", chlnCode);
				query.setDouble("chlnRemBal", 0.0);
				query.setDouble("chlnArId", 0.0);
				query.setDouble("balance", 1000.0);
			}
			
			List<String> chlnCodeList = query.list();
			if (chlnCodeList.isEmpty()) {
				map.put("msg", "Please Enter Valid Challan");
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			} else {
				/*Date d=new Date();
				Long dt=d.getTime();
				java.sql.Date date=new java.sql.Date(dt);
				
				Query ar = session.createSQLQuery("select ch.chlnCode as chlnCode " +
						"from challan as ch left join arrivalreport ar on ch.chlnArId=ar.arId " + 
						" left join challandetail as chd on ch.chlnCode=chd.chdChlnCode " +
						"where (((To_days('"+date+"')- To_days(arIssueDt))>=15) or ch.bCode in(2,24,16) or chdOwnCode in('own2238')) and chlnCode='"+chlnCode+"'");
				if(ar.list().isEmpty()){
					map.put("msg", "AR issue date must be 15 days older");
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					return map;
				}*/
				
				Criteria chdCr = session.createCriteria(ChallanDetail.class);
				chdCr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE, chlnCodeList.get(0)));
				List<ChallanDetail> chdList = chdCr.list();
				if (chdList.isEmpty()) {
					map.put("msg", "challan Detail Not Found");
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				} else {
					if (chdList.get(0).getChdPanHdrType() == null) {
						map.put("msg", "PAN HOLDER Type not define in challan");
						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					} else {
						//for ackValidation
//						Query cnQuery =session.createSQLQuery("select cn.cnmtCode from cnmt cn" +
//								" right join cnmt_challan cc on cn.cnmtId=cc.cnmtId " +
//								"right join challan ch on cc.chlnId=ch.chlnId " +
//								"where chlnCode=:chlnCode");
//						cnQuery.setString("chlnCode", chlnCodeList.get(0));
//						List cnList=cnQuery.list();
//						
//						List<Acknowledge_Validation> ackList=session.createCriteria(Acknowledge_Validation.class)
//								.add(Restrictions.eq("cnmtCode", cnList.get(0))).list();
//						
//						if(ackList.isEmpty()){
							if (chdList.get(0).getChdPanHdrType().equalsIgnoreCase("Broker")) {
								Query brkQuery = session.createQuery("SELECT brk.brkFaCode FROM Broker brk WHERE brk.brkCode = :brkCode and brkValidPan=true");
								brkQuery.setString("brkCode", chdList.get(0).getChdBrCode());
								List<String> brkFaCodeList = brkQuery.list();
								
								if(brkFaCodeList.isEmpty()){
									map.put("msg", "Please validate Broker PAN no.");
									map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
									return map;
								}
								
								map.put("faCode", brkFaCodeList.get(0));
								map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
							} else { //consider owner
								Query ownQuery = session.createQuery("SELECT own.ownFaCode FROM Owner own WHERE own.ownCode = :ownCode and ownValidPan=true");
								ownQuery.setString("ownCode", chdList.get(0).getChdOwnCode());
								List<String> ownFaCodeList = ownQuery.list();
								if(ownFaCodeList.isEmpty()){
									map.put("msg", "Please validate owner PAN no.");
									map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
									return map;
								}
								
								map.put("faCode", ownFaCodeList.get(0));
								map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
							}
//						}else if(ackList.get(0).getCheck()==null){
//							map.put("msg", "Please Validate ACK First");
//							map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
//						}else if(ackList.get(0).getCheck().equalsIgnoreCase("Y")){
//							if (chdList.get(0).getChdPanHdrType().equalsIgnoreCase("Broker")) {
//								Query brkQuery = session.createQuery("SELECT brk.brkFaCode FROM Broker brk WHERE brk.brkCode = :brkCode");
//								brkQuery.setString("brkCode", chdList.get(0).getChdBrCode());
//								List<String> brkFaCodeList = brkQuery.list();
//								map.put("faCode", brkFaCodeList.get(0));
//								map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
//							} else { //consider owner
//								Query ownQuery = session.createQuery("SELECT own.ownFaCode FROM Owner own WHERE own.ownCode = :ownCode");
//								ownQuery.setString("ownCode", chdList.get(0).getChdOwnCode());
//								List<String> ownFaCodeList = ownQuery.list();
//								map.put("faCode", ownFaCodeList.get(0));
//								map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
//							}
//						}else {
//							map.put("msg", "Please Validate ACK First");
//							map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
//						}
						
						
					}
				}
			}
		} catch (Exception e) {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			e.printStackTrace();
		} finally {
			session.clear();
			session.close();
		}
		
		return map;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
    public int getChlnId(String chlnCode){
		System.out.println("enter into getChlnCodeById function");
		int chlnId =0;
		Challan challan = new Challan();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Challan.class);
			cr.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE,chlnCode));
			challan = (Challan) cr.list().get(0);
			chlnId = challan.getChlnId();
		}catch(Exception e){
			e.printStackTrace();
		} 

		session.close();
		return chlnId;
	}
	
	
	public Map<String, Object> getChlnDetFrAdvPay(String chlnCode){
		Map<String, Object> map=new HashMap<>();
		
		Session session=this.sessionFactory.openSession();
		try {
			List<Challan> chlnList=session.createCriteria(Challan.class)
					.add(Restrictions.eq("chlnCode", chlnCode))
					.add(Restrictions.gt("chlnRemAdv", 0.0))
					.add(Restrictions.eq("isCancel", false)).list();
			
			if(!chlnList.isEmpty()) {
				Challan chln=chlnList.get(0);
				Station frmStn=(Station)session.get(Station.class, Integer.parseInt(chln.getChlnFromStn()));
				Station toStn=(Station)session.get(Station.class, Integer.parseInt(chln.getChlnToStn()));
				map.put("fromStn", frmStn);
				map.put("toStn", toStn);
				
				List<ChallanDetail> chdlist=session.createCriteria(ChallanDetail.class)
						.add(Restrictions.eq("chdChlnCode", chlnCode)).list();
				
				if(!chdlist.isEmpty()) {
					ChallanDetail chd=chdlist.get(0);
					if(chd.getChdBrCode() != null) {
						Broker brk=(Broker) session.createCriteria(Broker.class)
								.add(Restrictions.eq("brkCode", chd.getChdBrCode())).list().get(0);
						map.put("brk", brk);
						
					}else {
						map.put("brk", null);
					}
					if(chd.getChdOwnCode() != null) {
						Owner own=(Owner) session.createCriteria(Owner.class)
								.add(Restrictions.eq("ownCode", chd.getChdOwnCode())).list().get(0);
						map.put("own", own);
						
					}else {
						map.put("own", null);
					}
					
					map.put("chln", chln);
					map.put("result", "success");
					
				}else {
					map.put("result", "error");
					map.put("chln", null);
					map.put("msg", "Challan detail not found");
				}
			}else {
				map.put("result", "error");
				map.put("chln", null);
				map.put("msg", "Challan not found");
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			map.put("result", "error");
			map.put("chln", null);
			map.put("msg", "Exception");
		}finally {
			session.clear();
			session.close();
		}
		
		return map;
	}
	
	
	@Override
	public Map<String, String> alwOldBalance(String chlnCode){
		Map<String, String> resultMap=new HashMap<>();
		User user=(User) httpSession.getAttribute("currentUser");
		if(user==null) {
			resultMap.put("result", "error");
			resultMap.put("msg", "Please Login");
			return resultMap;
		}
		
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			List<Challan> chlnList=session.createCriteria(Challan.class)
					.add(Restrictions.eq("chlnCode", chlnCode)).list();
			
			if(!chlnList.isEmpty()) {
				Challan chln=chlnList.get(0);
				chln.setChlnBalPayAlw(true);
				session.update(chln);
				session.flush();
				session.clear();
				transaction.commit();
				resultMap.put("result", "success");
			}else {
				resultMap.put("result", "error");
				resultMap.put("msg", "challan not found");
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
			transaction.rollback();
			resultMap.put("result", "error");
			resultMap.put("msg", "Exception");
		}finally {
			session.close();
		}
		
		return resultMap;
	}
	
}

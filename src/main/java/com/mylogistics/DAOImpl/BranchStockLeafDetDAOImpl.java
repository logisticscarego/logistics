package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.BranchStockLeafDetDAO;
import com.mylogistics.constants.BranchStockLeafDetCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.User;

public class BranchStockLeafDetDAOImpl implements BranchStockLeafDetDAO{
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	HttpSession httpSession;
	
	public static Logger logger = Logger.getLogger(BranchStockLeafDetDAOImpl.class);

	@Autowired
	public BranchStockLeafDetDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	public int saveBranchStockLeafDet(BranchStockLeafDet branchStockLeafDet){
		System.out.println("enter into saveBranchStockLeafDet function");
		int temp;
		String startNo = branchStockLeafDet.getBrsLeafDetSNo();
		   try{
			   session = this.sessionFactory.openSession();
			   transaction =  session.beginTransaction();
			   for(int i=0;i<50;i++){
				   System.out.println("start no of cnmt == > "+startNo);
				   BranchStockLeafDet brStkLeafDet = new BranchStockLeafDet();
				   brStkLeafDet.setbCode(branchStockLeafDet.getbCode());
				   brStkLeafDet.setBrsLeafDetBrCode(branchStockLeafDet.getBrsLeafDetBrCode());
				   brStkLeafDet.setBrsLeafDetSNo(startNo);
				   brStkLeafDet.setBrsLeafDetStatus(branchStockLeafDet.getBrsLeafDetStatus());
				   brStkLeafDet.setUserCode(branchStockLeafDet.getUserCode());
				  
				   session.save(brStkLeafDet);
				   
				   /*int SNO = Integer.parseInt(startNo);
				   SNO = SNO + 1;
				   long stno = 10000000 + SNO;
				   startNo = String.valueOf(stno).substring(1,8);*/
				   long SNO = Long.parseLong(startNo); 
				   SNO = SNO + 1;
				   startNo = String.valueOf(SNO);
			   }
			   transaction.commit();
			   session.flush();
			   temp=1;
		   }
		   catch(Exception e){
				e.printStackTrace();
				temp=-1;
		   }
		   session.clear();
		   session.close();
		   return temp;
	}
	
	@Transactional
    @SuppressWarnings("unchecked")
	public List<BranchStockLeafDet> getBranchStockLeafDetData(String branchCode){
		List<BranchStockLeafDet> bList = new ArrayList<BranchStockLeafDet>();
		System.out.println("The branchCode here is-----------------------"+branchCode);
	    try{
	    	 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(BranchStockLeafDet.class);
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS,"chln"));
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE,branchCode));
			 bList =cr.list();
			 transaction.commit();
			 session.flush();
	    	}
		catch (Exception e){
			e.printStackTrace();
		}
	    session.clear();
	    session.close();
		return bList;
   }
	
	@Transactional
    @SuppressWarnings("unchecked")
	public List<String> getAvlChln(String brhCode){
		System.out.println("enter into getAvlChln function");
		List<String> chlnList = new ArrayList<>();
		try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 List<BranchStockLeafDet> bList = new ArrayList<BranchStockLeafDet>();
			 Criteria cr=session.createCriteria(BranchStockLeafDet.class);
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS,"chln"));
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE,brhCode));
			 bList =cr.list();
			 
			 if(!bList.isEmpty()){
				 for(int i=0;i<bList.size();i++){
					 chlnList.add(bList.get(i).getBrsLeafDetSNo());
				 }
			 }
			 
			 session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return chlnList;
	}
	
	@Transactional
    @SuppressWarnings("unchecked")
	public List<String> getAvlChln(String brhCode, String chlnCode){		
		logger.info("Enter into getAvlChln() : ChlnCode = "+chlnCode);
		List<String> chlnList = new ArrayList<>();
		try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 List<BranchStockLeafDet> bList = new ArrayList<BranchStockLeafDet>();
			 
			 Criteria cr=session.createCriteria(BranchStockLeafDet.class);
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS,"chln"));
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE,brhCode));
			 cr.add(Restrictions.like(BranchStockLeafDetCNTS.BRSLD_SNO, "%"+chlnCode+"%"));
			 bList =cr.list();
			 if(!bList.isEmpty()){
				 logger.info("Challan found : Challan Size = "+bList.size());
				 for(int i=0;i<bList.size();i++){
					 chlnList.add(bList.get(i).getBrsLeafDetSNo());
				 }
			 }else
				 logger.info("Challan not found !");			 
			 session.flush();
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception : "+e);
		}		
		session.clear();
		session.close();
		logger.info("Exit from getAvlChln()...");
		return chlnList;
	}
	
	
	@Transactional
	public BranchStockLeafDet deleteBSLD(String serialNo,String brsLeafDetStatus){
		System.out.println("serial no in dao is-----------------------"+serialNo);
		System.out.println("brsLeafDetStatus = "+brsLeafDetStatus);
		BranchStockLeafDet branchStockLeafDet = new BranchStockLeafDet();
		/*BranchStockLeafDet branchStockLeafDet = null;*/
	    try{
	    	session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(BranchStockLeafDet.class);
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_SNO,serialNo));
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS,brsLeafDetStatus));
			 List list = cr.list();
			 if(! list.isEmpty()){
				 branchStockLeafDet = (BranchStockLeafDet) cr.list().get(0);
				 session.delete(branchStockLeafDet);
			 }			
			 transaction.commit();
			 session.flush();
			
			 System.out.println("branchStockLeafDet Data is deleted successfully");	 
	    }catch (Exception e){
			e.printStackTrace();
		}
	    session.clear();
	    session.close();
		 return branchStockLeafDet;
   }
	
	
	
	@Override
	public BranchStockLeafDet deleteBSLD(String serialNo,String brsLeafDetStatus,Session session){
		System.out.println("serial no in dao is-----------------------"+serialNo);
		System.out.println("brsLeafDetStatus = "+brsLeafDetStatus);
		BranchStockLeafDet branchStockLeafDet = new BranchStockLeafDet();
	    
			 Criteria cr=session.createCriteria(BranchStockLeafDet.class);
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_SNO,serialNo));
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS,brsLeafDetStatus));
			 List list = cr.list();
			 if(! list.isEmpty()){
				 branchStockLeafDet = (BranchStockLeafDet) cr.list().get(0);
				 session.delete(branchStockLeafDet);

				 System.out.println("branchStockLeafDet Data is deleted successfully");	 
			 }else{
				 branchStockLeafDet=null;
			 }
		 return branchStockLeafDet;
   }
	
	
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<BranchStockLeafDet> getCodeList(String branchCode,String status){
		System.out.println("enter into getCnmtCodeList function--branchCode = "+branchCode);
		System.out.println("enter into getCnmtCodeList function--status = "+status);
		List<BranchStockLeafDet> cnmtCodeList = new ArrayList<BranchStockLeafDet>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(BranchStockLeafDet.class);
			cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS,status));
			cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE,branchCode));
			cnmtCodeList = cr.list();
			transaction.commit();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return cnmtCodeList;
	}
	
	
	/*@Transactional
	@SuppressWarnings("unchecked")
	public List<BranchStockLeafDet> getCnmtCodeList(String branchCode){
		System.out.println("enter into getCnmtCodeList function");
		List<BranchStockLeafDet> cnmtCodeList = new ArrayList<BranchStockLeafDet>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(BranchStockLeafDet.class);
			cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS,"cnmt"));
			cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE,branchCode));
			cnmtCodeList = cr.list();
			transaction.commit();
		}
		catch(Exception e){
			e.printStackTrace();
		} 
		session.flush();
		session.close();
		return cnmtCodeList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<BranchStockLeafDet> getARCodeList(String branchCode){
		
		List<BranchStockLeafDet> arCodeList = new ArrayList<BranchStockLeafDet>();
		try{
			System.out.println("branchCode ------------"+branchCode);
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(BranchStockLeafDet.class);
			cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS,"sedr"));
			cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE,branchCode));
			arCodeList = cr.list();
			System.out.println("-----arCodeList-------------"+arCodeList.size());
			transaction.commit();
		}
		catch(Exception e){
			e.printStackTrace();
		} 
		session.flush();
		session.close();
		return arCodeList;
	}
	*/
	@Transactional
    @SuppressWarnings("unchecked")
	public int getLeftStationary(String branchCode,String type){
		List<BranchStockLeafDet> bList = new ArrayList<BranchStockLeafDet>();
		System.out.println("The branchCode here is-----------------------"+branchCode);
		int cnmtLeft =0;
	    try{
	    	session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(BranchStockLeafDet.class);
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS,type));
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE,branchCode));
			 bList =cr.list();
			 cnmtLeft=bList.size();
			 transaction.commit();
			 session.flush();
	    	}
		catch (Exception e){
			e.printStackTrace();
		}
	    session.clear();
	    session.close();
		return cnmtLeft;
   }
	
	
	
	@Override
	public int getLeftStationary(String branchCode,String type,Session session){
		List<BranchStockLeafDet> bList = new ArrayList<BranchStockLeafDet>();
		System.out.println("The branchCode here is-----------------------"+branchCode);
		int cnmtLeft =0;
			 Criteria cr=session.createCriteria(BranchStockLeafDet.class);
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS,type));
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE,branchCode));
			 bList =cr.list();
			 cnmtLeft=bList.size();
		return cnmtLeft;
   }
	
	
	@Transactional
    @SuppressWarnings("unchecked")
	public int updateCnmt(String brhCode , String serialNo , boolean allow){
		System.out.println("enter into updateCnmt function");
		int res = 0;
		try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 List<BranchStockLeafDet> bsldList = new ArrayList<>();
			 
			 Criteria cr=session.createCriteria(BranchStockLeafDet.class);
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS,"cnmt"));
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE,brhCode));
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_SNO,serialNo));
			 bsldList = cr.list();
			 
			 if(!bsldList.isEmpty()){
				 BranchStockLeafDet branchStockLeafDet = bsldList.get(0);
				 branchStockLeafDet.setBrsLeafDetDlyRt(allow);
				 session.update(branchStockLeafDet);
				 transaction.commit();
				 res = 1;
			 }
			 
			 session.flush();
		}catch(Exception e){
			e.printStackTrace();
			res = -1;
		}
		session.clear();
		session.close();
		return res;
	}
	
	
	
	/*@Transactional
    @SuppressWarnings("unchecked")
	public int getLeftChlns(String branchCode){
		List<BranchStockLeafDet> bList = new ArrayList<BranchStockLeafDet>();
		System.out.println("The branchCode here is-----------------------"+branchCode);
		int chlnLeft =0;
	    try{
	    	 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(BranchStockLeafDet.class);
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS,"chln"));
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE,branchCode));
			 bList =cr.list();
			 chlnLeft=bList.size();
			 transaction.commit();
			 session.flush();
	    	}
		catch (Exception e){
			e.printStackTrace();
		}
	   
	    session.close();
		return chlnLeft;
   }
	
	@Transactional
    @SuppressWarnings("unchecked")
	public int getLeftSedrs(String branchCode){
		List<BranchStockLeafDet> bList = new ArrayList<BranchStockLeafDet>();
		System.out.println("The branchCode here is-----------------------"+branchCode);
		int sedrLeft =0;
	    try{
	    	session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(BranchStockLeafDet.class);
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS,"sedr"));
			 cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE,branchCode));
			 bList =cr.list();
			 sedrLeft=bList.size();
			 transaction.commit();
			 session.flush();
	    	}
		catch (Exception e){
			e.printStackTrace();
		}
	   
	    session.close();
		return sedrLeft;
   }*/
	
	
	
	public List<BranchStockLeafDet> getCodeList(Session session, User user, String cnmtCode, String status){
		logger.info("UserID = "+user.getUserId()+" : Enter into getCodeList() : CnmtCode = "+cnmtCode);
		List<BranchStockLeafDet> codeList = null;
		try{
			Criteria cr = session.createCriteria(BranchStockLeafDet.class);
			cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS, status));
			cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE, user.getUserBranchCode()));
			cr.add(Restrictions.like(BranchStockLeafDetCNTS.BRSLD_SNO, "%"+cnmtCode));
			codeList = cr.list();
			logger.info("UserID = "+user.getUserId()+" : StockListSize = "+codeList.size());
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId() + " : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId()+" : Exit from getCodeList() : CnmtCode = "+cnmtCode);
		return codeList;
	}
	
	
	@Override
	public BranchStockLeafDet getCodeByCode( String code, String status){
		BranchStockLeafDet brs=null;
		User user =(User) httpSession.getAttribute("currentUser");
		Session session=this.sessionFactory.openSession();
		try{
			List<BranchStockLeafDet> codeList = session.createCriteria(BranchStockLeafDet.class)
			.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS, status))
			.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE, user.getUserBranchCode()))
			.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_SNO, code)).list();

			if(!codeList.isEmpty()) {
				brs=codeList.get(0);
			}
			
		}catch(Exception e){
			logger.error(" : Exception in getCodeByCode()= "+e);
		}finally {
			session.clear();
			session.close();
		}
		return brs;
	}
	
	
	@Override
	public BranchStockLeafDet getCodeByCodeFrLa( String code, String status){
		BranchStockLeafDet brs=null;
		User user =(User) httpSession.getAttribute("currentUser");
		Session session=this.sessionFactory.openSession();
		try{
			List<BranchStockLeafDet> codeList = session.createCriteria(BranchStockLeafDet.class)
			.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS, status))
			.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_SNO, code)).list();

			if(!codeList.isEmpty()) {
				brs=codeList.get(0);
			}
			
		}catch(Exception e){
			logger.error(" : Exception in getCodeByCodeFrLa()= "+e);
		}finally {
			session.clear();
			session.close();
		}
		return brs;
	}
	
	
}

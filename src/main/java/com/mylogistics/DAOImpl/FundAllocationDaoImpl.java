package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.xmlbeans.impl.xb.xsdschema.RestrictionDocument.Restriction;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import com.mylogistics.DAO.FundAllocationDao;
import com.mylogistics.model.Challan;
//import com.mylogistics.model.AllowAdvance;
//import com.mylogistics.model.Challan;
import com.mylogistics.model.ChallanDetail;
import com.mylogistics.model.FundAllocation;
import com.mylogistics.model.FundAllocationDetails;
import com.mylogistics.model.Owner;
import com.mylogistics.model.PetroCard;
import com.mylogistics.model.PetroCardAdv;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleVendorMstr;

public class FundAllocationDaoImpl implements FundAllocationDao {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	HttpSession httpSession;

	public FundAllocationDaoImpl(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		this.sessionFactory = sessionFactory;
	}

	@Override
	public int saveFundAllocation(FundAllocation fundAllocation) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		int i = 0;
		try {
			i = (Integer) session.save(fundAllocation);
			transaction.commit();
		} catch (Exception e) {
			// TODO: handle exception
			transaction.rollback();
		} finally {
			session.clear();
			session.close();
		}
		return i;
	}
	
	/*@Override
	public int saveVehclAdvAlwReq(Map<String, String> map) {
		Session session = this.sessionFactory.openSession();
		User user=(User) httpSession.getAttribute("currentUser");
		Transaction transaction = session.beginTransaction();
		int i = 0;
		try {
			AllowAdvance aa=new AllowAdvance();
			aa.setbCode(user.getUserBranchCode());
			aa.setChlnDt(java.sql.Date.valueOf(map.get("chlnDt")));
			aa.setChlnNo(map.get("chlnNo"));
			aa.setCnmtNo(map.get("cnmtNo"));
			aa.setFromStn(map.get("from"));
			aa.setLryNo(map.get("lryNo"));
			aa.setToStn(map.get("to"));
			aa.setRequestBy(user.getUserCode());
			i = (Integer) session.save(aa);
			session.flush();
			transaction.commit();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			i=-1;
			transaction.rollback();
		} finally {
			session.clear();
			session.close();
		}
		return i;
	}
	
	@Override
	public AllowAdvance getAlwdVehclFrAdv(Map<String, String> map) {
		AllowAdvance aa=null;
		Session session=sessionFactory.openSession();
		try {
			List<AllowAdvance> aaList=session.createCriteria(AllowAdvance.class)
					.add(Restrictions.eq("lryNo", map.get("lryNo")))
					.add(Restrictions.eq("chlnDt", java.sql.Date.valueOf(map.get("chlnDt")))).list();
				
			if(!aaList.isEmpty()) {
				aa=aaList.get(0);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return aa;
		
	}*/

	@SuppressWarnings("unchecked")
	@Override
	public boolean checkChallanNo(String chlnCode) {
		boolean b=false;
		Session session=sessionFactory.openSession();
		try {
			/*List<FundAllocationDetails> fdList=session.createCriteria(FundAllocationDetails.class)
					.add(Restrictions.eq("challanNo", chlnCode)).list();
			*/
			
			SQLQuery qr=session.createSQLQuery("select fa_Id from fundAllocationDetails where challanNo=:chlnCode");
			qr.setString("chlnCode", chlnCode);
			List<Integer> faIdList=new ArrayList<>();
			faIdList=qr.list();
			if(!faIdList.isEmpty()) {
				
				List<FundAllocation>faList=session.createCriteria(FundAllocation.class)
						.add(Restrictions.in("faId", faIdList))
						.add(Restrictions.eq("allowed", false))
						.add(Restrictions.eq("rejected", false)).list();
				if(!faList.isEmpty())
					b=true;
			}else
				b=false;
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return b;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean checkChallanNoInPetro(String chlnCode) {
		boolean b=false;
		Session session=sessionFactory.openSession();
		try {
			List<PetroCardAdv> fdList=session.createCriteria(PetroCardAdv.class)
					.add(Restrictions.eq("challanNo", chlnCode))
					.add(Restrictions.eq("allowed", false))
					.add(Restrictions.eq("rejected", false)).list();
				
			if(!fdList.isEmpty()) {
				b=true;
			}else
				b=false;
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return b;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<FundAllocation> getCurrentDateFundAllocationList() {
		// TODO Auto-generated method stub

		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		String val = date.toString();
		System.out.println("Val of Calender is   " + val);
		Session session = this.sessionFactory.openSession();
		Criteria criteria = session.createCriteria(FundAllocation.class);
		criteria.add(Restrictions.eq("isGenerated", "No"));
		criteria.add(Restrictions.eq("approved", false));
		criteria.add(Restrictions.eq("rejected", false));
		criteria.addOrder(Order.asc("bCode"));
		criteria.addOrder(Order.asc("beneficiaryAccountNo"));
		List<FundAllocation> fundList = criteria.list();
		for(int i=0;i<fundList.size();i++){
			Hibernate.initialize(fundList.get(i).getFundAllocationDetails());
		}
		//System.out.println("fdList="+fundList.get(0).getFundAllocationDetails().get(0).getChallanNo());
		session.close();
		return fundList;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<FundAllocation> getCurrentDateFundAllocationListFrExcel() {
		// TODO Auto-generated method stub

		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		String val = date.toString();
		System.out.println("Val of Calender is   " + val);
		Session session = this.sessionFactory.openSession();
		Criteria criteria = session.createCriteria(FundAllocation.class);
		criteria.add(Restrictions.eq("isGenerated", "No"));
		criteria.add(Restrictions.eq("approved", true));
		criteria.add(Restrictions.eq("rejected", false));
		criteria.addOrder(Order.asc("bCode"));
		criteria.addOrder(Order.asc("beneficiaryAccountNo"));
		List<FundAllocation> fundList = criteria.list();
		for(int i=0;i<fundList.size();i++){
			Hibernate.initialize(fundList.get(i).getFundAllocationDetails());
		}
		//System.out.println("fdList="+fundList.get(0).getFundAllocationDetails().get(0).getChallanNo());
		session.close();
		return fundList;
	}
	
	/*@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getCurrentFundAllocationForPay(){
		Map<String, Object> fundMap=new HashMap<>();
		
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		String val = date.toString();
		System.out.println("Val of Calender is   " + val);
		Session session = this.sessionFactory.openSession();
		Criteria criteria = session.createCriteria(FundAllocation.class);
		criteria.add(Restrictions.eq("isGenerated", "No"));
		criteria.addOrder(Order.asc("bCode"));
		criteria.addOrder(Order.asc("beneficiaryAccountNo"));
		List<FundAllocation> fundList = criteria.list();
		List<Map<String, Object>> listMap=new ArrayList<>();
		for(int i=0;i<fundList.size();i++){
			Hibernate.initialize(fundList.get(i).getFundAllocationDetails());
			for(int k=0;k<fundList.get(i).getFundAllocationDetails().size();k++) {
				System.out.println("faDId size="+fundList.get(i).getFundAllocationDetails().size());
				FundAllocationDetails fd=fundList.get(i).getFundAllocationDetails().get(k);
				Map<String, Object> localMap=new HashMap<>();
				if("".equalsIgnoreCase(fd.getChallanNo()) || fd.getChallanNo() == null) {
					Criteria cr=session.createCriteria(VehicleVendorMstr.class)
							.add(Restrictions.eq("vvRcNo", fd.getVehicleNo()));
					List<VehicleVendorMstr> vvList=cr.list();
					System.out.println("vvList Size"+vvList.size());
					if(!vvList.isEmpty()) {
						Owner own=vvList.get(0).getOwner();
						System.out.println("ownCode"+own.getOwnCode());
						localMap.put("ownBnkName", own.getOwnBnkBranch());
						localMap.put("ownIFSC", own.getOwnIfsc());
						localMap.put("own", own);
					}
					
					
				}else {
					Criteria cr=session.createCriteria(ChallanDetail.class)
							.add(Restrictions.eq("chdChlnCode", fd.getChallanNo()));
					
					List<ChallanDetail> chdList=cr.list();
					
					if(!chdList.isEmpty()) {
						ChallanDetail chd=chdList.get(0);
						if(chd.getChdPanHdrType().equalsIgnoreCase("Owner")) {
							Owner own=(Owner) session.createCriteria(Owner.class)
									.add(Restrictions.eq("ownCode", chd.getChdOwnCode())).list().get(0);
						}else {
							
						}
						
					}
					
					
				}
				
			}
			
		}
		
		fundMap.put("fundList",fundList);
		fundMap.put("", "");
		
		session.close();
		
		return fundMap;
	}*/
	

	@Override
	public int updateFundAllocation(FundAllocation fundAllocation) {
		// TODO Auto-generated method stub
		int i = 0;
		Session session = this.sessionFactory.openSession();
		try {
			FundAllocation allocation = (FundAllocation) session.load(FundAllocation.class, fundAllocation.getFaId());
			allocation.setAmount(fundAllocation.getAmount());
			allocation.setInstrumentAmount(fundAllocation.getInstrumentAmount());
			allocation.setChequeTmDate(Calendar.getInstance());
			String ifscCode = fundAllocation.getIfscCode();
			if (ifscCode.substring(0, 4).equalsIgnoreCase("HDFC")) {
				allocation.setTransactionType("I");
			} else {
				if (fundAllocation.getInstrumentAmount() > 200000) {

					allocation.setTransactionType("R");
				} else {
					allocation.setTransactionType("N");
				}
			}
			session.update(allocation);
			session.beginTransaction().commit();
			i = 1;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			i = 0;
		}
		session.close();
		return i;
	}
	
	
	@Override
	public void updateFundAllocation(FundAllocation fa,Session session) {
		// TODO Auto-generated method stub
			session.update(fa);
	}
	
	
	
	@Override
	public int rejectFundAllocation(FundAllocation fundAllocation) {
		// TODO Auto-generated method stub
		int i = 0;
		System.out.println("Fundallocation"+fundAllocation.getAmount());
		Session session = this.sessionFactory.openSession();
		try {
			/*FundAllocation allocation = (FundAllocation) session.load(FundAllocation.class, fundAllocation.getFaId());
			allocation.setAmount(fundAllocation.getAmount());
			allocation.setInstrumentAmount(fundAllocation.getInstrumentAmount());
			allocation.setChequeTmDate(Calendar.getInstance());
			String ifscCode = fundAllocation.getIfscCode();
			if (ifscCode.substring(0, 4).equalsIgnoreCase("HDFC")) {
				allocation.setTransactionType("I");
			} else {
				if (fundAllocation.getInstrumentAmount() > 200000) {

					allocation.setTransactionType("R");
				} else {
					allocation.setTransactionType("N");
				}
			}*/
			FundAllocation fa=(FundAllocation) session.get(FundAllocation.class, fundAllocation.getFaId());
			fa.setApproved(false);
			fa.setRejected(true);
			session.update(fa);
			session.beginTransaction().commit();
			i = 1;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			i = 0;
		}
		session.close();
		return i;
	}
	

	@Override
	public void setGenerated(List<FundAllocation> fundAllocationList) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.openSession();
		Transaction transaction = null;
		FundAllocation allocation = null;
		transaction = session.beginTransaction();
		
		try {
		
			for (FundAllocation fundAllocation : fundAllocationList) {
			allocation = (FundAllocation) session.load(FundAllocation.class,
					fundAllocation.getFaId());
			allocation.setIsGenerated("Yes");
			
				session.update(allocation);
			
			}
		
			transaction.commit();
		} catch (Exception e) {
			// TODO: handle exception
			transaction.rollback();
		}
		
		session.close();

	}
	
	
	@Override
	public int savePetroAdv(PetroCardAdv adv) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		int i = 0;
		try {
			i = (Integer) session.save(adv);
			transaction.commit();
		} catch (Exception e) {
			// TODO: handle exception
			i=0;
			transaction.rollback();
		} finally {
			session.clear();
			session.close();
		}
		return i;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<PetroCardAdv> getCurrentDatePetroAllocationList() {
		// TODO Auto-generated method stub

		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		String val = date.toString();
		System.out.println("Val of Calender is   " + val);
		Session session = this.sessionFactory.openSession();
		Criteria criteria = session.createCriteria(PetroCardAdv.class);
		criteria.add(Restrictions.eq("isGenerated", "No"));
		criteria.add(Restrictions.eq("approved", false));
		criteria.add(Restrictions.eq("rejected", false));
		criteria.addOrder(Order.asc("bCode"));
		criteria.addOrder(Order.asc("cardType"));
		List<PetroCardAdv> petroList = criteria.list();
		session.close();
		return petroList;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PetroCardAdv> getPetroAllocationListFrExl() {
		// TODO Auto-generated method stub

		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		String val = date.toString();
		System.out.println("Val of Calender is   " + val);
		Session session = this.sessionFactory.openSession();
		Criteria criteria = session.createCriteria(PetroCardAdv.class);
		criteria.add(Restrictions.eq("isGenerated", "No"));
		criteria.add(Restrictions.eq("approved", true));
		criteria.add(Restrictions.eq("rejected", false));
		criteria.addOrder(Order.asc("bCode"));
		criteria.addOrder(Order.asc("cardType"));
		List<PetroCardAdv> petroList = criteria.list();
		session.close();
		return petroList;
	}
	
	
	
	@Override
	public int updatePetroAllocation(PetroCardAdv adv) {
		// TODO Auto-generated method stub
		int i = 0;
		Session session = this.sessionFactory.openSession();
		try {
			//PetroCardAdv allocation = (PetroCardAdv) session.load(PetroCardAdv.class, adv.getPetId());
			/*allocation.setAmount(adv.getAmount());
			allocation.setCreationTS(Calendar.getInstance());*/
			
			session.update(adv);
			session.beginTransaction().commit();
			i = 1;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			i = 0;
		}
		session.close();
		return i;
	}
	
	
	@Override
	public void updatePetroAllocation(PetroCardAdv adv,Session session) {
		// TODO Auto-generated method stub
			session.update(adv);
	}
	
	
	@Override
	public void setPetroGenerated(List<PetroCardAdv> petroAllocationList) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.openSession();
		Transaction transaction = null;
		PetroCardAdv allocation = null;
		transaction = session.beginTransaction();
		
		try {
		
			for (PetroCardAdv fundAllocation : petroAllocationList) {
			allocation = (PetroCardAdv) session.load(PetroCardAdv.class,
					fundAllocation.getPetId());
			allocation.setIsGenerated("Yes");
			
				session.update(allocation);
			
			}
		
			transaction.commit();
		} catch (Exception e) {
			// TODO: handle exception
			transaction.rollback();
		}
		
		session.close();

	}
	
	
	@Override
	public PetroCard getCardByCardNo(String cardNo) {
		PetroCard card=null;
		Session session=this.sessionFactory.openSession();
		List<PetroCard> pcList=session.createCriteria(PetroCard.class)
				.add(Restrictions.eq("cardNo", cardNo)).list();
		
		if(!pcList.isEmpty()) {
			card=pcList.get(0);
		}
		session.clear();
		session.close();
		return card;
	}
	
	
	@Override
	public Map<String,String> alwChlnFrPetroAdvanceMT(String chlnCode){
		Map<String, String> resultMap=new HashMap<>();
		User user=(User) httpSession.getAttribute("currentUser");
		if(user==null) {
			resultMap.put("result", "error");
			resultMap.put("msg", "Please Login");
			return resultMap;
		}
		
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			List<PetroCardAdv> ptroList=session.createCriteria(PetroCardAdv.class)
					.add(Restrictions.eq("challanNo", chlnCode))
					.add(Restrictions.eq("rejected", false)).list();
			
			if(!ptroList.isEmpty()) {
				for(int i=0;i<ptroList.size();i++) {
					PetroCardAdv petroAdv=ptroList.get(i);
					petroAdv.setAllowed(true);
					session.update(petroAdv);
				}
				session.flush();
				session.clear();
				transaction.commit();
				resultMap.put("result", "success");
			}else {
				resultMap.put("result", "error");
				resultMap.put("msg", "challan not found");
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
			transaction.rollback();
			resultMap.put("result", "error");
			resultMap.put("msg", "Exception");
		}finally {
			session.close();
		}
		
		return resultMap;
	}
	
	
	
	@Override
	public Map<String,String> alwChlnFrFundAdvanceMT(String chlnCode){
		Map<String, String> resultMap=new HashMap<>();
		User user=(User) httpSession.getAttribute("currentUser");
		if(user==null) {
			resultMap.put("result", "error");
			resultMap.put("msg", "Please Login");
			return resultMap;
		}
		
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			
			List<Integer> fdIdList=session.createSQLQuery("select fa_Id from fundAllocationDetails where challanNo='"+chlnCode+"'").list();
			if(!fdIdList.isEmpty()) {
				List<FundAllocation> fundList=session.createCriteria(FundAllocation.class)
						.add(Restrictions.in("faId", fdIdList))
						.add(Restrictions.eq("rejected", false)).list();
				
				if(!fundList.isEmpty()) {
					for(int i=0;i<fundList.size();i++) {
						FundAllocation fundAl=fundList.get(i);
						fundAl.setAllowed(true);
						session.update(fundAl);
					}
					session.flush();
					session.clear();
					transaction.commit();
					resultMap.put("result", "success");
				}else {
					resultMap.put("result", "error");
					resultMap.put("msg", "challan not found");
				}
			}else {
				resultMap.put("result", "error");
				resultMap.put("msg", "challan not found");
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			transaction.rollback();
			resultMap.put("result", "error");
			resultMap.put("msg", "Exception");
		}finally {
			session.close();
		}
		
		return resultMap;
	}
	
	
}

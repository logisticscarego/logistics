package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.StationDAO;
import com.mylogistics.constants.ContToStnCNTS;
import com.mylogistics.constants.StateCNTS;
import com.mylogistics.constants.StationCNTS;
import com.mylogistics.model.AllStation;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.State;
import com.mylogistics.model.Station;
import com.mylogistics.model.User;

public class StationDAOImpl implements StationDAO {

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	public static Logger logger = Logger.getLogger(StationDAOImpl.class);

	
	@Autowired
	public  StationDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
    @SuppressWarnings("unchecked")
	public List<Station> getStationData(){
		List<Station> stationList = new ArrayList<Station>();
	    try{
		     session = this.sessionFactory.openSession();
	         stationList = session.createCriteria(Station.class).list();
	    }catch (Exception e){
			e.printStackTrace();
		}
	    session.clear();
	    session.close();
		return stationList;
   }
	
	@Transactional
    @SuppressWarnings("unchecked")
	public List<Station> getStationDataByNameCode(String stnNameCode){
		List<Station> stationList = new ArrayList<Station>();
	    try{
		     session = this.sessionFactory.openSession();
		     Criteria cr = session.createCriteria(Station.class);
		     if(NumberUtils.isNumber(stnNameCode))
		    	 cr.add(Restrictions.like(StationCNTS.STN_CODE, stnNameCode+"%"));
		     else
		    	 cr.add(Restrictions.like(StationCNTS.STN_NAME, stnNameCode+"%"));
		     stationList = cr.list();
	    }catch (Exception e){
			e.printStackTrace();
		}
	    session.clear();
	    session.close();
		return stationList;
   }
	
	@Transactional
    @SuppressWarnings("unchecked")
	public List<Station> getStationDataByStationName(String stnName){
		List<Station> stationList = new ArrayList<Station>();
	    try{
		     session = this.sessionFactory.openSession();
	         stationList = session.createCriteria(Station.class)
	        		 .add(Restrictions.like(StationCNTS.STN_NAME, "%"+stnName+"%"))
	        		 .list();
	    }catch (Exception e){
			e.printStackTrace();
		}
	    session.clear();
	    session.close();
		return stationList;
   }
	
	/*@Transactional
	@SuppressWarnings("unchecked")
    public List<String> getStationCode(){
		List<String> stationCodeList = new ArrayList<String>();
		try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(Station.class);
			 ProjectionList projList = Projections.projectionList();
			 projList.add(Projections.property(StationCNTS.STN_CODE));
			 cr.setProjection(projList);
			 stationCodeList = cr.list();
			  transaction.commit();
			 session.flush();
		}catch(Exception e){
			e.printStackTrace();
		} 
	    session.close();
		return stationCodeList;
	 }*/
	
	
	@Transactional
	public int saveStationToDB(Station station){
		int temp;
		List<State> stList = new ArrayList<State>();
		try{
			 session = this.sessionFactory.openSession();
			 transaction =  session.beginTransaction();
			 
			 /*List<Station> stnList = session.createCriteria(Station.class)
					 .add(Restrictions.eq(StationCNTS.STN_NAME, station.getStnName()))
					 .list();
			 if(stnList.isEmpty()){	*/		 
				 System.out.println("station.getStateCode: "+station.getStateCode());			 
				 Criteria cr=session.createCriteria(State.class);
				 cr.add(Restrictions.eq(StateCNTS.STATE_ID,Integer.parseInt(station.getStateCode())));
				 stList = cr.list();
				 
				 System.out.println("stList.size: "+stList.size());
				 
				 if(!stList.isEmpty()){
					 State state = stList.get(0);
					 station.setState(state);
					 int stnId = (Integer) session.save(station);
					 station.setStnCode(String.valueOf(stnId));
					 state.getStnList().add(station);
					 session.update(state);
					 System.out.println("hiiiiiiiiiiiiiiiiiiiiiiii");
					 
					 List<AllStation> list= session.createCriteria(AllStation.class)
							 .add(Restrictions.eq("stationName", station.getStnName()))
							 .add(Restrictions.eq("pinCode", Integer.parseInt(station.getStnPin())))
							 .add(Restrictions.eq("city", station.getStnCity()))
							 .add(Restrictions.eq("stationId", "")).list();
					 if(!list.isEmpty()) {
						 AllStation allStation=list.get(0);
						 allStation.setStationId(String.valueOf(stnId));
						 session.update(allStation);
					 }
				 }
				 transaction.commit();
				 //session.flush();
				 temp= 1;
			/* }else
				 temp = 0;*/
		}catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}
		session.clear();
		session.close();
		return temp;
			
	}
	
	/*@Transactional
    @SuppressWarnings("unchecked")
	public List<String> getAllStationCodes(){
		System.out.println("Entered into getAllStationCodes function in DAOImpl---");
		List<String> stationList = new ArrayList<String>();
		try{
		     session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(Station.class);
			 ProjectionList projList = Projections.projectionList();
			 projList.add(Projections.property(StationCNTS.STN_CODE));
			 cr.setProjection(projList);
			 stationList = cr.list();
			 transaction.commit();
			 session.flush();
		}catch(Exception e){
			e.printStackTrace();
		} 
		session.close();
		return stationList;
	 }*/
	
	@Transactional
	 @SuppressWarnings("unchecked")
	 public List<Station> retrieveStation(String stationCode){
		System.out.println("Entered into retrieveStation function---");
		 List<Station> list = new ArrayList<Station>();
		 try{
			   session = this.sessionFactory.openSession();
			   transaction =  session.beginTransaction();
			   Criteria cr=session.createCriteria(Station.class);
			   cr.add(Restrictions.eq(StationCNTS.STN_CODE,stationCode));
			   list =cr.setMaxResults(1).list();
			   session.flush();
		}
		catch(Exception e){
				e.printStackTrace();
		}
		 session.clear();
		session.close();
		return list; 
	 }
	
	 @Transactional
	 public int updateStation(Station station){
		 System.out.println("Entered into updateStation function of stationDAOImpl");
		 int temp;
		 Date date = new Date();
	 	 Calendar calendar = Calendar.getInstance();
	 	 calendar.setTime(date);	 	
		 try{
			  session = this.sessionFactory.openSession();
			  transaction =  session.beginTransaction();
			  station.setCreationTS(calendar);
			  session.update(station);
			  transaction.commit();
			  session.flush();
			  temp=1;
		}catch(Exception e){
			e.printStackTrace();
			temp=-1;
		}
		 session.clear();
		session.close();
		return temp;		 
	 }
	 
	 @Transactional
	 public long totalCount(){
		 long num = -1;
		 System.out.println("Entered into totalCount function");
		 try{
		     session = this.sessionFactory.openSession();
		     num = (Long) session.createCriteria(Station.class).setProjection(Projections.rowCount()).uniqueResult();
		     session.flush();
		 } 
		 catch(Exception e){
	 		 e.printStackTrace();
	 	}
		 session.clear();
	 	session.close();
		return num;
		
	}
	 
	  @Transactional
	  @SuppressWarnings("unchecked")
	  public List<Station> getLastStationRow(){
		  List<Station> last = new ArrayList<Station>();
		  try{
			   session = this.sessionFactory.openSession();
			   last = session.createQuery("from Station order by stnId DESC ").setMaxResults(1).list();
			   session.flush();
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		  session.clear();
		  session.close(); 
		  return last;
	  }

	@Override
	@Transactional
	public List<Map<String, Object>> getStnNameCodePin() {

		List<Map<String, Object>> stationNameCodePinList = new ArrayList<>();
		
		try {
			
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("SELECT new map(stnId as stnId, stnCode as stnCode, stnDistrict as stnDistrict, stnName as stnName, stnPin as stnPin) FROM Station");
			stationNameCodePinList = query.list();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		
		return stationNameCodePinList;
	}

	@Override
	@Transactional
	public List<Station> getFrmStnList(String custCode) {
		System.out.println("StationDAOImpl.getFrmStnList()");
		List<Station> frmStnList = new ArrayList<>();
		try {
			session = this.sessionFactory.openSession();
			//add from regular contract
			Query regContQuery = session.createQuery("SELECT regContFromStation FROM RegularContract WHERE regContBLPMCode = :custCode");
			regContQuery.setString("custCode", custCode);
			List<String> regFrmStnIdList = regContQuery.list();
			
			if (!regFrmStnIdList.isEmpty()) {
				for (String stnId : regFrmStnIdList) {
					Station stn = (Station) session.get(Station.class, Integer.parseInt(stnId));
					frmStnList.add(stn);
				}
			}
			
			//add from daily contract
			
			Query dlyContQuery = session.createQuery("SELECT dlyContFromStation FROM DailyContract WHERE dlyContBLPMCode = :custCode");
			dlyContQuery.setString("custCode", custCode);
			List<String> dlyFrmStnIdList = dlyContQuery.list();
			
			if (!dlyFrmStnIdList.isEmpty()) {
				for (String stnId : dlyFrmStnIdList) {
					Station stn = (Station) session.get(Station.class, Integer.parseInt(stnId));
					frmStnList.add(stn);
				}
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.clear();
			session.close();
		}
		
		return frmStnList;
	}

	@Override
	@Transactional
	public List<Station> getToStnList(String contCode) {
		System.out.println("StationDAOImpl.getToStnList()");
		List<Station> toStnList = new ArrayList<>();
		
		try {
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("SELECT ctsToStn FROM ContToStn WHERE ctsContCode = :contCode");
			query.setString("contCode", contCode);
			List<String> toStnIdList = query.list();
			
			if (!toStnIdList.isEmpty()) {
				for (String stnId : toStnIdList) {
					Station stn = (Station) session.get(Station.class, Integer.parseInt(stnId));
					toStnList.add(stn);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.clear();
			session.close();
		}
		
		return toStnList;
	}
	
	@Transactional
    @SuppressWarnings("unchecked")
	public List<Station> getStationList(){
		List<Station> stationList = new ArrayList<Station>();
	    try{
		     session = this.sessionFactory.openSession();
	         Criteria cr = session.createCriteria(Station.class);
	         ProjectionList projList = Projections.projectionList();
			 projList.add(Projections.property(StationCNTS.STN_CODE));
			 projList.add(Projections.property(StationCNTS.STN_NAME));
			 cr.setProjection(projList);
			 stationList=cr.list();
	    }catch (Exception e){
			e.printStackTrace();
		}
	    session.clear();
	    session.close();
		return stationList;
   }
	
	@Override
	public List<Station> getStationDataByNameCode(Session session, User user, String stnNameCode){
		logger.info("UserID = "+user.getUserId()+" : Enter into getStationDataByNameCode() : StnNameCode = "+stnNameCode);
		List<Station> stationList = null;
	    try{		     
		     Criteria cr = session.createCriteria(Station.class);
		     if(NumberUtils.isNumber(stnNameCode))
		    	 cr.add(Restrictions.like(StationCNTS.STN_CODE, stnNameCode+"%"));
		     else
		    	 cr.add(Restrictions.like(StationCNTS.STN_NAME, stnNameCode+"%"));
		     
		     stationList = cr.list();
	    }catch (Exception e){
	    	logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}
	    logger.info("UserID = "+user.getUserId()+" : Exit from getStationDataByNameCode() : StnNameCode = "+stnNameCode);
		return stationList;
   }
	
	@Override
	public Station getStationByStnCode(Session session, User user, String stnCode){
		logger.info("UserID = "+user.getUserId()+" : Enter into getStationByStnCode() : StnNameCode = "+stnCode);
		Station station = null;
	    try{		     
		     Criteria cr = session.createCriteria(Station.class);
		     cr.add(Restrictions.eq(StationCNTS.STN_CODE, stnCode));
		     
		     List list = cr.list();
		     if(! list.isEmpty())
		    	 station = (Station) list.get(0);
	    }catch (Exception e){
	    	logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}
	    logger.info("UserID = "+user.getUserId()+" : Exit from getStationDataByNameCode() : StnNameCode = "+stnCode);
		return station;
   }
	
	
	@Override
	public List getDistByStateCode(String stateCode) {
		List<String> distList=new ArrayList<>();
		Session session=null;
		try {
			session=this.sessionFactory.openSession();
			Query cr=session.createSQLQuery("select distinct stnDistrict from station where stateCode='"+stateCode+"'");
			distList=cr.list();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return distList;
	}
	
	
	
	@Override
	public List getADistByStateCode(String stateCode) {
		List<String> distList=new ArrayList<>();
		Session session=null;
		try {
			session=this.sessionFactory.openSession();
			Query cr=session.createSQLQuery("select distinct district from allstation where stateCode='"+stateCode+"'");
			distList=cr.list();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return distList;
	}
	
	
	@Override
	public List getCityByDistName(String distName,String stateCode) {
		List<String> cityList=new ArrayList<>();
		Session session=null;
		try {
			session=this.sessionFactory.openSession();
			Query cr=session.createSQLQuery("select distinct  stnCity from station where stnDistrict='"+distName+"' and stateCode='"+stateCode+"'");
			cityList=cr.list();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return cityList;
	}
	
	
	
	@Override
	public List getACityByDistName(String distName,String stateCode) {
		List<String> cityList=new ArrayList<>();
		Session session=null;
		try {
			session=this.sessionFactory.openSession();
			Query cr=session.createSQLQuery("select distinct  city from allstation where district='"+distName+"' and stateCode='"+stateCode+"'");
			cityList=cr.list();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return cityList;
	}
	
	
	@Override
	public List<Station> getStnByCityDistState(String cityName,String distName,String stateCode) {
		List<Station> stnList=new ArrayList<>();
		Session session=null;
		try {
			session=this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Station.class)
					.add(Restrictions.eq("stnCity", cityName))
					.add(Restrictions.eq("stnDistrict", distName))
					.add(Restrictions.eq("stateCode", stateCode));
			stnList=cr.list();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return stnList;
	}
	
	
	
	@Override
	public List<AllStation> getAStnByCityDistState(String cityName,String distName,String stateCode) {
		List<AllStation> stnList=new ArrayList<>();
		Session session=null;
		try {
			session=this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(AllStation.class)
					.add(Restrictions.eq("city", cityName))
					.add(Restrictions.eq("district", distName))
					.add(Restrictions.eq("stateCode", Integer.parseInt(stateCode)));
					//.add(Restrictions.eq("stationId", ""));
			stnList=cr.list();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return stnList;
	}
	
	
	@Override
	public List<Station> getStnByPin(String pin) {
		List<Station> stnList=new ArrayList<>();
		Session session=null;
		try {
			session=this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Station.class)
					.add(Restrictions.eq("stnPin", pin));
			stnList=cr.list();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return stnList;
	}
	
	
	@Override
	public List<AllStation> getAStnByPin(String pin) {
		List<AllStation> stnList=new ArrayList<>();
		Session session=null;
		try {
			session=this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(AllStation.class)
					.add(Restrictions.eq("pinCode", Integer.parseInt(pin)));
					//.add(Restrictions.eq("stationId", ""));
			stnList=cr.list();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return stnList;
	}

	@Override
	public boolean getStationExist(String stnName,String stnCity,String pin){
		boolean b=false;
		Session session=this.sessionFactory.openSession();
		try {
			List list=session.createCriteria(Station.class)
					.add(Restrictions.eq(StationCNTS.STN_NAME, stnName))
					.add(Restrictions.eq(StationCNTS.STN_CITY, stnCity))
					.add(Restrictions.eq(StationCNTS.STN_PIN, pin)).list();
			if(!list.isEmpty())
				b=true;
			System.out.println("list size="+list.size());
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		return b;
	}
	
}

package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.LhpvTempDAO;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.constants.FAMasterCNTS;
import com.mylogistics.constants.LhpvTempCNTS;
import com.mylogistics.controller.LhpvTempCntlr;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.LhpvTemp;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;

public class LhpvTempDAOImpl implements LhpvTempDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;

	@Autowired
	private HttpSession httpSession;
	
	
	@Autowired
	public LhpvTempDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	 @Transactional
	 @SuppressWarnings("unchecked")
	public int saveLhpvTemp(VoucherService voucherService){
		System.out.println("enter into saveLhpvTemp funciton");
		int result = 0;
		User currentUser = (User) httpSession.getAttribute("currentUser");
		LhpvTemp lhpvTemp = voucherService.getLhpvTemp();
		CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
		int voucherNo = 0;
		try{
			if(lhpvTemp !=null){
				lhpvTemp.setbCode(currentUser.getUserBranchCode());
				lhpvTemp.setUserCode(currentUser.getUserCode());
				
				List<CashStmtStatus> cssList = new ArrayList<>();
				
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
				
				 Criteria cr = session.createCriteria(CashStmt.class);
				 cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
				 cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
				 ProjectionList proList = Projections.projectionList();
				 proList.add(Projections.property("csVouchNo"));
				 cr.setProjection(proList);
					
				 List<Integer>  voucherNoList = cr.list();
				 if(!voucherNoList.isEmpty()){
						int vNo = voucherNoList.get(voucherNoList.size() - 1);
						voucherNo = vNo + 1;
				 }else{
						voucherNo = 1;
				 }
				 
				 result = voucherNo;
				 
				double payment = lhpvTemp.getLtAdv() + lhpvTemp.getLtBal() + 
						         lhpvTemp.getLtDet() + lhpvTemp.getLtUnload() + 
						         lhpvTemp.getLtOth();	 
					 
				double recovery = lhpvTemp.getLtTds() + lhpvTemp.getLtCashDis() + 
						          lhpvTemp.getLtMuns() + lhpvTemp.getLtClaim();
				
				double finalAmt = payment - recovery;
				lhpvTemp.setLtFinalAmt(finalAmt);
				
				if(lhpvTemp.getLtTransType().equalsIgnoreCase("BANK")){
					
					 CashStmt cashStmt = new CashStmt();
					 cashStmt.setbCode(currentUser.getUserBranchCode());
					 cashStmt.setUserCode(currentUser.getUserCode());
					 cashStmt.setCsDescription(voucherService.getDesc());
					 cashStmt.setCsDrCr('C');
					 cashStmt.setCsAmt(finalAmt);
					 cashStmt.setCsType(voucherService.getVoucherType());
					 cashStmt.setCsVouchType(lhpvTemp.getLtTransType());
					 cashStmt.setCsPayTo(voucherService.getPayTo());
					 //cashStmt.setCsChequeType(chequeType);
					 cashStmt.setCsTvNo("0000000000000");
					 cashStmt.setCsFaCode(lhpvTemp.getLtBankCode());
					 cashStmt.setCsVouchNo(voucherNo);
					 cashStmt.setCsDt(sqlDate);
					 cashStmt.setCsLhpvTempNo(lhpvTemp.getLtLhpvNo());
					
					 cr = session.createCriteria(CashStmtStatus.class);
					 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
					 cssList = cr.list();
					 
					 if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt.setCsNo(csStatus);
							session.save(cashStmt);
							csStatus.getCashStmtList().add(cashStmt);
							session.update(csStatus);
					}		
				}
				
				
						
					if(payment > 0){
						
						 List<FAMaster> famList = new ArrayList<>();	
						 cr = session.createCriteria(FAMaster.class);
						 cr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.LHPV_FANAME));
						 famList = cr.list();
						
						 CashStmt cashStmt = new CashStmt();
						 cashStmt.setbCode(currentUser.getUserBranchCode());
						 cashStmt.setUserCode(currentUser.getUserCode());
						 cashStmt.setCsDescription(voucherService.getDesc());
						 cashStmt.setCsDrCr('D');
						 cashStmt.setCsAmt(payment);
						 cashStmt.setCsType(voucherService.getVoucherType());
						 cashStmt.setCsVouchType(lhpvTemp.getLtTransType());
						 cashStmt.setCsPayTo(voucherService.getPayTo());
						 //cashStmt.setCsChequeType(chequeType);
						 cashStmt.setCsTvNo("0000000000000");
						 cashStmt.setCsFaCode(famList.get(0).getFaMfaCode());
						 cashStmt.setCsVouchNo(voucherNo);
						 cashStmt.setCsDt(sqlDate);
						 cashStmt.setCsLhpvTempNo(lhpvTemp.getLtLhpvNo());
						
						 cr = session.createCriteria(CashStmtStatus.class);
						 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						 cssList = cr.list();
						 
						 if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt.setCsNo(csStatus);
								session.save(cashStmt);
								csStatus.getCashStmtList().add(cashStmt);
								session.update(csStatus);
						}	
						
					}else{
						
						 List<FAMaster> famList = new ArrayList<>();	
						 cr = session.createCriteria(FAMaster.class);
						 cr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.LHPV_FANAME));
						 famList = cr.list();
						
						 CashStmt cashStmt = new CashStmt();
						 cashStmt.setbCode(currentUser.getUserBranchCode());
						 cashStmt.setUserCode(currentUser.getUserCode());
						 cashStmt.setCsDescription(voucherService.getDesc());
						 cashStmt.setCsDrCr('C');
						 cashStmt.setCsAmt(payment);
						 cashStmt.setCsType(voucherService.getVoucherType());
						 cashStmt.setCsVouchType(lhpvTemp.getLtTransType());
						 cashStmt.setCsPayTo(voucherService.getPayTo());
						 //cashStmt.setCsChequeType(chequeType);
						 cashStmt.setCsTvNo("0000000000000");
						 cashStmt.setCsFaCode(famList.get(0).getFaMfaCode());
						 cashStmt.setCsVouchNo(voucherNo);
						 cashStmt.setCsDt(sqlDate);
						 cashStmt.setCsLhpvTempNo(lhpvTemp.getLtLhpvNo());
						
						 cr = session.createCriteria(CashStmtStatus.class);
						 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						 cssList = cr.list();
						 
						 if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt.setCsNo(csStatus);
								session.save(cashStmt);
								csStatus.getCashStmtList().add(cashStmt);
								session.update(csStatus);
						}	
								
					}
					 
					if(lhpvTemp.getLtTds() > 0){

						 List<FAMaster> famList = new ArrayList<>();	
						 cr = session.createCriteria(FAMaster.class);
						 cr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.LHPV_TDS));
						 famList = cr.list();
						
						 CashStmt cashStmt = new CashStmt();
						 cashStmt.setbCode(currentUser.getUserBranchCode());
						 cashStmt.setUserCode(currentUser.getUserCode());
						 cashStmt.setCsDescription(voucherService.getDesc());
						 cashStmt.setCsDrCr('C');
						 cashStmt.setCsAmt(lhpvTemp.getLtTds());
						 cashStmt.setCsType(voucherService.getVoucherType());
						 cashStmt.setCsVouchType(lhpvTemp.getLtTransType());
						 cashStmt.setCsPayTo(voucherService.getPayTo());
						 //cashStmt.setCsChequeType(chequeType);
						 cashStmt.setCsTvNo("0000000000000");
						 cashStmt.setCsFaCode(famList.get(0).getFaMfaCode());
						 cashStmt.setCsVouchNo(voucherNo);
						 cashStmt.setCsDt(sqlDate);
						 cashStmt.setCsLhpvTempNo(lhpvTemp.getLtLhpvNo());
						
						 cr = session.createCriteria(CashStmtStatus.class);
						 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						 cssList = cr.list();
						 
						 if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt.setCsNo(csStatus);
								session.save(cashStmt);
								csStatus.getCashStmtList().add(cashStmt);
								session.update(csStatus);
						}	
					}
					
					if(lhpvTemp.getLtCashDis() > 0){
						
						 List<FAMaster> famList = new ArrayList<>();	
						 cr = session.createCriteria(FAMaster.class);
						 cr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.LHPV_CASH_DIS));
						 famList = cr.list();
						
						 CashStmt cashStmt = new CashStmt();
						 cashStmt.setbCode(currentUser.getUserBranchCode());
						 cashStmt.setUserCode(currentUser.getUserCode());
						 cashStmt.setCsDescription(voucherService.getDesc());
						 cashStmt.setCsDrCr('C');
						 cashStmt.setCsAmt(lhpvTemp.getLtCashDis());
						 cashStmt.setCsType(voucherService.getVoucherType());
						 cashStmt.setCsVouchType(lhpvTemp.getLtTransType());
						 cashStmt.setCsPayTo(voucherService.getPayTo());
						 //cashStmt.setCsChequeType(chequeType);
						 cashStmt.setCsTvNo("0000000000000");
						 cashStmt.setCsFaCode(famList.get(0).getFaMfaCode());
						 cashStmt.setCsVouchNo(voucherNo);
						 cashStmt.setCsDt(sqlDate);
						 cashStmt.setCsLhpvTempNo(lhpvTemp.getLtLhpvNo());
						
						 cr = session.createCriteria(CashStmtStatus.class);
						 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						 cssList = cr.list();
						 
						 if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt.setCsNo(csStatus);
								session.save(cashStmt);
								csStatus.getCashStmtList().add(cashStmt);
								session.update(csStatus);
						}	
					}
					
					
					if(lhpvTemp.getLtMuns() > 0){
						 List<FAMaster> famList = new ArrayList<>();	
						 cr = session.createCriteria(FAMaster.class);
						 cr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.LHPV_MUNS));
						 famList = cr.list();
						
						 CashStmt cashStmt = new CashStmt();
						 cashStmt.setbCode(currentUser.getUserBranchCode());
						 cashStmt.setUserCode(currentUser.getUserCode());
						 cashStmt.setCsDescription(voucherService.getDesc());
						 cashStmt.setCsDrCr('C');
						 cashStmt.setCsAmt(lhpvTemp.getLtMuns());
						 cashStmt.setCsType(voucherService.getVoucherType());
						 cashStmt.setCsVouchType(lhpvTemp.getLtTransType());
						 cashStmt.setCsPayTo(voucherService.getPayTo());
						 //cashStmt.setCsChequeType(chequeType);
						 cashStmt.setCsTvNo("0000000000000");
						 cashStmt.setCsFaCode(famList.get(0).getFaMfaCode());
						 cashStmt.setCsVouchNo(voucherNo);
						 cashStmt.setCsDt(sqlDate);
						 cashStmt.setCsLhpvTempNo(lhpvTemp.getLtLhpvNo());
						
						 cr = session.createCriteria(CashStmtStatus.class);
						 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						 cssList = cr.list();
						 
						 if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt.setCsNo(csStatus);
								session.save(cashStmt);
								csStatus.getCashStmtList().add(cashStmt);
								session.update(csStatus);
						}	
					}
					
					
					if(lhpvTemp.getLtClaim() > 0){
						 List<FAMaster> famList = new ArrayList<>();	
						 cr = session.createCriteria(FAMaster.class);
						 cr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.LHPV_CLAIM));
						 famList = cr.list();
						
						 CashStmt cashStmt = new CashStmt();
						 cashStmt.setbCode(currentUser.getUserBranchCode());
						 cashStmt.setUserCode(currentUser.getUserCode());
						 cashStmt.setCsDescription(voucherService.getDesc());
						 cashStmt.setCsDrCr('C');
						 cashStmt.setCsAmt(lhpvTemp.getLtClaim());
						 cashStmt.setCsType(voucherService.getVoucherType());
						 cashStmt.setCsVouchType(lhpvTemp.getLtTransType());
						 cashStmt.setCsPayTo(voucherService.getPayTo());
						 //cashStmt.setCsChequeType(chequeType);
						 cashStmt.setCsTvNo("0000000000000");
						 cashStmt.setCsFaCode(famList.get(0).getFaMfaCode());
						 cashStmt.setCsVouchNo(voucherNo);
						 cashStmt.setCsDt(sqlDate);
						 cashStmt.setCsLhpvTempNo(lhpvTemp.getLtLhpvNo());
						
						 cr = session.createCriteria(CashStmtStatus.class);
						 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						 cssList = cr.list();
						 
						 if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt.setCsNo(csStatus);
								session.save(cashStmt);
								csStatus.getCashStmtList().add(cashStmt);
								session.update(csStatus);
						}	
					}
					
					
					session.save(lhpvTemp);
					transaction.commit(); 
				
			}else{
				result = -1;
			}
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			result = -1;
		}
		session.clear();
		session.close();
		return result;
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<LhpvTemp> getLhpvList(String bCode){
		System.out.println("enter into getLhpvList function");
		List<LhpvTemp> lhpvList = new ArrayList<LhpvTemp>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
					
			Criteria cr = session.createCriteria(LhpvTemp.class);
			cr.add(Restrictions.eq(LhpvTempCNTS.LHPV_START,true));
			cr.add(Restrictions.eq(LhpvTempCNTS.LHPV_END,false));
			cr.add(Restrictions.eq(LhpvTempCNTS.B_CODE,bCode));
			lhpvList = cr.list();
			
			if(lhpvList.isEmpty()){
				cr = session.createCriteria(LhpvTemp.class);
				cr.add(Restrictions.eq(LhpvTempCNTS.LHPV_START,false));
				cr.add(Restrictions.eq(LhpvTempCNTS.LHPV_END,false));
				cr.add(Restrictions.eq(LhpvTempCNTS.B_CODE,bCode));
				lhpvList = cr.list();
			}
		
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return lhpvList;
	}
	
}

package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transaction;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.FYearDAO;
import com.mylogistics.model.FYear;

public class FYearDAOImpl implements FYearDAO {

	@Autowired
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	
	public FYearDAOImpl(SessionFactory sessionFactory) {
		super();
		this.sessionFactory = sessionFactory;
	}
	Logger logger = Logger.getLogger(FYearDAOImpl.class);
	
	
	
	public List<FYear> getAllFYear(){
		logger.info("Enter into getAllFYear()");
		List<FYear> fYearList = new ArrayList<FYear>();
		try{
			session = this.sessionFactory.openSession();
			fYearList = session.createCriteria(FYear.class)
					.addOrder(Order.asc("fyFrmDt"))
					.list();
		}catch(Exception e){
			logger.error("Exception : "+e);
		}finally{
			session.flush();
			session.clear();
			session.close();
		}	
		logger.info("Exit from getAllFYear()");
		return fYearList;
	}
	
	
	
	public FYear getCurrentFYear(){
		logger.info("Enter into getCurrentFYear()");
		FYear fYear = null;
		try{
			session = this.sessionFactory.openSession();
			List<FYear> fYears = session.createCriteria(FYear.class)
					.add(Restrictions.eq("isActive", 'Y'))
					.list();
			if(! fYears.isEmpty())
				fYear = (FYear) fYears.get(0);
		}catch(Exception e){
			logger.error("Exception : "+e);
		}finally{
			session.flush();
			session.clear();
			session.close();
		}	
		logger.info("Exit into getCurrentFYear()");
		return fYear;
	}
	
	
	public FYear getFYearByDate(Date date){
		logger.info("Enter into getFYearByDate()");
		FYear fYear = null;
		try{
			session = this.sessionFactory.openSession();
			List<FYear> fYears = session.createCriteria(FYear.class)
					.add(Restrictions.and(Restrictions.ge("fyFrmDt", date), Restrictions.le("fyToDt", date)))
					.list();
			if(! fYears.isEmpty())
				fYear = fYears.get(0);
		}catch(Exception e){
			logger.error("Exception : "+e);
		}finally{
			session.flush();
			session.clear();
			session.close();
		}	
		logger.info("Exit from getFYearByDate()");
		return fYear;
	}
	
}
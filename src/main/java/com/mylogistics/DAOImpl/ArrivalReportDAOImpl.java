package com.mylogistics.DAOImpl;

import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.FetchType;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.log4j.Logger;
import org.apache.xmlbeans.impl.validator.ValidatorUtil;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.mylogistics.DAO.ArrivalReportDAO;
import com.mylogistics.constants.ArrivalReportCNTS;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.constants.CnmtCNTS;
import com.mylogistics.constants.CnmtImageCNTS;
import com.mylogistics.constants.Cnmt_ChallanCNTS;
import com.mylogistics.constants.CustomerCNTS;
import com.mylogistics.constants.EmployeeCNTS;
import com.mylogistics.constants.MemoCNTS;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.Bill;
import com.mylogistics.model.BranchSStats;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.Challan;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.CnmtImage;
import com.mylogistics.model.Cnmt_Challan;
import com.mylogistics.model.Customer;
import com.mylogistics.model.Memo;
import com.mylogistics.model.StationarySupplier;
import com.mylogistics.model.User;
import com.mylogistics.model.Bill.BillInsert;
import com.mylogistics.model.lhpv.LhpvBal;
import com.mylogistics.model.lhpv.LhpvSup;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.MemoService;
import com.mylogistics.services.ValidatorFactoryUtil;
import com.mylogistics.services.VoucherService;

public class ArrivalReportDAOImpl implements ArrivalReportDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	private HttpSession httpSession;
	
	public static Logger logger = Logger.getLogger(ArrivalReportDAOImpl.class);

	@Autowired
	public ArrivalReportDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public int saveArrivalReportToDB(ArrivalReport ar){
		int temp;
		System.out.println("enter into saveArrivalReportToDB function of IMPL");
		try{
			session = this.sessionFactory.openSession();//fixed
			transaction = session.beginTransaction();//fixed
			ar.setArIsHOAlw(true);
			int arId = (Integer) session.save(ar);
			
			if(arId >0){
				List<Challan> chlnList = new ArrayList<>();
				Criteria cr=session.createCriteria(Challan.class);
				cr.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE,ar.getArchlnCode()));
				chlnList = cr.list();
				if(!chlnList.isEmpty()){
					Challan challan = chlnList.get(0);
					challan.setChlnArId(arId);
					session.update(challan);
				}
			}
			
			transaction.commit();//fixed
			System.out.println("user data inserted successfully");
			temp= 1;
			session.flush();

		}
		catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}
		session.clear();
		session.close();
		return temp;
	}

	
	
	
	@Override
	public int saveArrivalReportToDB(ArrivalReport ar,Session session){
		int temp;
		System.out.println("enter into saveArrivalReportToDB function of IMPL");
			ar.setArIsHOAlw(true);
			int arId = (Integer) session.save(ar);
			
			if(arId >0){
				List<Challan> chlnList = new ArrayList<>();
				Criteria cr=session.createCriteria(Challan.class);
				cr.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE,ar.getArchlnCode()));
				chlnList = cr.list();
				if(!chlnList.isEmpty()){
					Challan challan = chlnList.get(0);
					challan.setChlnArId(arId);
					session.update(challan);
				}
			}
			
			System.out.println("user data inserted successfully");
			temp= 1;

		return temp;
	}

	
	
	

	@Transactional
	@SuppressWarnings("unchecked")
	public List<ArrivalReport> getARisViewFalse(String branchCode){

		System.out.println("The value of branch code is "+branchCode);
		List<ArrivalReport> result = new ArrayList<ArrivalReport>();

		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(ArrivalReport.class);
			cr.add(Restrictions.eq(ArrivalReportCNTS.BRANCH_CODE,branchCode));
			cr.add(Restrictions.eq(ArrivalReportCNTS.IS_VIEW,false));
			result =cr.list();
			transaction.commit();

			System.out.println("size of list is-->>"+result.size());
			for(int i = 0;i<result.size();i++){
				System.out.println("The values in the list is"+result.get(i).getCreationTS());
			}
			System.out.println("After getting the list");
			session.flush();

		}
		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return  result; 
	}


	@Transactional
	@SuppressWarnings("unchecked")
	public int updateIsViewARTrue(String[] code) {
		int temp;
		for(int i=0;i<code.length;i++){
			System.out.println("The values inside array is ----------"+code[i]);
		}
		try{
			session = this.sessionFactory.openSession();
			for(int i=0;i<code.length;i++){
				transaction = session.beginTransaction();

				ArrivalReport arrivalReport = new ArrivalReport();

				List<ArrivalReport> list = new ArrayList<ArrivalReport>();
				Criteria cr=session.createCriteria(ArrivalReport.class);
				cr.add(Restrictions.eq(ArrivalReportCNTS.AR_CODE,code[i]));
				list =cr.list();
				arrivalReport = list.get(0);
				arrivalReport.setView(true);
				session.update(arrivalReport);
				transaction.commit();
				session.flush();

			}

			temp = 1;
		}

		catch(Exception e){
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<ArrivalReport> getARList(String arCode){

		List<ArrivalReport> result = new ArrayList<ArrivalReport>();
		System.out.println("-----------------Entrr to retriev list of arlist on entred broker code--");	 
		try{
			System.out.println("arCode is"+arCode);
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(ArrivalReport.class);
			cr.add(Restrictions.eq(ArrivalReportCNTS.AR_CODE,arCode));
			result =cr.setMaxResults(1).list();
			System.out.println("The values inside list is"+result.get(0).getArClaim());
			System.out.println("The values inside list is"+result.get(0).getArCode());
			System.out.println("The values inside list is"+result.get(0).getArDetention());
			session.flush();

		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return  result; 
	}


	@Transactional
	public int updateAR(ArrivalReport arrivalReport) {

		int temp;
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			arrivalReport.setCreationTS(calendar);
			session.saveOrUpdate(arrivalReport);
			transaction.commit();
			temp= 1;
			session.flush();
			
		}
		catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}		
		session.clear();
		session.close();
		return temp;
	}

	@Transactional
	public Blob getArImageByCode(String arCode){
		System.out.println("enter into getArImageByCode function");
		List<ArrivalReport> arList = new ArrayList<ArrivalReport>();
		Blob blob = null;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(ArrivalReport.class);
			cr.add(Restrictions.eq(ArrivalReportCNTS.AR_CODE, arCode));
			arList = cr.list();
			ArrivalReport arrivalReport= arList.get(0);
			//blob = arrivalReport.getArImage();
			session.flush();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return blob;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<ArrivalReport> getArrivalReport(String arCode){
		List<ArrivalReport> result = new ArrayList<ArrivalReport>();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(ArrivalReport.class);
			cr.add(Restrictions.eq("arCode",arCode));
			result =cr.list();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return  result; 
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public List<String> getArCode(){

		List<String> arList = new ArrayList<String>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(ArrivalReport.class);
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property(ArrivalReportCNTS.AR_CODE));
			cr.setProjection(projList);
			arList = cr.list();
			transaction.commit();
			session.flush();

		}
		catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return arList;
	} 

	@Transactional
	@SuppressWarnings("unchecked")
	public List<ArrivalReport> getAllArCodes(String branchCode){
		System.out.println("enter into getAllArCodes function = "+branchCode);
		List<ArrivalReport> arCodeList = new ArrayList<ArrivalReport>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(ArrivalReport.class);
			cr.add(Restrictions.eq(ArrivalReportCNTS.BRANCH_CODE,branchCode));
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property(ArrivalReportCNTS.AR_CODE));
			cr.setProjection(projList);
			arCodeList = cr.list();
			//System.out.println("arCodeList size = "+arCodeList.size());
			//transaction.commit();
			session.flush();

		}
		catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return arCodeList;
	}

	@Transactional
	public int updateAr(ArrivalReport ar){
		System.out.println("Entered into updateAr of DAOImpl----");

		int temp;
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			ar.setCreationTS(calendar);
			session.update(ar);
			transaction.commit();
			temp= 1;
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}
		session.clear();
		session.close();
		return temp;
	}


	@Transactional
	public List<ArrivalReport> getARByChlnCode(String chlnCode){
		System.out.println("Entered into getARByChlnCode of DAOImpl----");
		List<ArrivalReport> arrList = new ArrayList<ArrivalReport>();
		try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			Criteria cr=session.createCriteria(ArrivalReport.class);
			cr.add(Restrictions.eq(ArrivalReportCNTS.AR_CHLN_CODE,chlnCode));
			arrList = cr.list();
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();

		}
		session.clear();
		session.close();
		return arrList;
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public int saveCompARToDB(ArrivalReport arrivalReport){
		System.out.println("enter into saveCompARToDB function");
		int temp;
		try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			Criteria cr=session.createCriteria(ArrivalReport.class);
			cr.add(Restrictions.eq(ArrivalReportCNTS.AR_REP_TYPE,"Computerized"));
			ProjectionList proList = Projections.projectionList();
			proList.add(Projections.property("arCode"));
			cr.setProjection(proList);
			
			List<String> codeList = cr.list();
			
			if(!codeList.isEmpty()){
				/*int code = Integer.parseInt(codeList.get(codeList.size() - 1));
				code = code + 1;*/
				long code = codeList.size() + 1 + 100000000;
				String arCode = String.valueOf(code).substring(1,9);
				arrivalReport.setArCode("C"+arCode);
				arrivalReport.setArIsHOAlw(true);
				temp = (Integer) session.save(arrivalReport);
				
				if(temp >0){
					List<Challan> chlnList = new ArrayList<>();
					cr=session.createCriteria(Challan.class);
					cr.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE,arrivalReport.getArchlnCode()));
					chlnList = cr.list();
					if(!chlnList.isEmpty()){
						Challan challan = chlnList.get(0);
						challan.setChlnArId(temp);
						session.update(challan);
					}
				}
				
			}else{
				arrivalReport.setArCode("C00000001");
				arrivalReport.setArIsHOAlw(true);
				temp = (Integer) session.save(arrivalReport);
				
				if(temp >0){
					List<Challan> chlnList = new ArrayList<>();
					cr=session.createCriteria(Challan.class);
					cr.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE,arrivalReport.getArchlnCode()));
					chlnList = cr.list();
					if(!chlnList.isEmpty()){
						Challan challan = chlnList.get(0);
						challan.setChlnArId(temp);
						session.update(challan);
					}
				}
			}
			
			transaction.commit();
		}catch(Exception e){
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}
	
	
	
	
	@Override
	public int saveCompARToDB(ArrivalReport arrivalReport,Session session){
		System.out.println("enter into saveCompARToDB function");
		int temp;
			Criteria cr=session.createCriteria(ArrivalReport.class);
			cr.add(Restrictions.eq(ArrivalReportCNTS.AR_REP_TYPE,"Computerized"));
			ProjectionList proList = Projections.projectionList();
			proList.add(Projections.property("arCode"));
			cr.setProjection(proList);
			
			List<String> codeList = cr.list();
			
			if(!codeList.isEmpty()){
				/*int code = Integer.parseInt(codeList.get(codeList.size() - 1));
				code = code + 1;*/
				long code = codeList.size() + 1 + 100000000;
				String arCode = String.valueOf(code).substring(1,9);
				arrivalReport.setArCode("C"+arCode);
				arrivalReport.setArIsHOAlw(true);
				temp = (Integer) session.save(arrivalReport);
				
				if(temp >0){
					List<Challan> chlnList = new ArrayList<>();
					cr=session.createCriteria(Challan.class);
					cr.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE,arrivalReport.getArchlnCode()));
					chlnList = cr.list();
					if(!chlnList.isEmpty()){
						Challan challan = chlnList.get(0);
						challan.setChlnArId(temp);
						session.update(challan);
					}
				}
				
			}else{
				arrivalReport.setArCode("C00000001");
				arrivalReport.setArIsHOAlw(true);
				temp = (Integer) session.save(arrivalReport);
				
				if(temp >0){
					List<Challan> chlnList = new ArrayList<>();
					cr=session.createCriteria(Challan.class);
					cr.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE,arrivalReport.getArchlnCode()));
					chlnList = cr.list();
					if(!chlnList.isEmpty()){
						Challan challan = chlnList.get(0);
						challan.setChlnArId(temp);
						session.update(challan);
					}
				}
			}
			
		return temp;
	}
	
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public ArrivalReport getArById(int arId){
		System.out.println("enter into getArById function = "+arId);
		ArrivalReport arrivalReport = null;
		try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			arrivalReport = (ArrivalReport) session.get(ArrivalReport.class,arId);
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return arrivalReport;
	}

	@Override
	public int saveArRemBal(VoucherService voucherService) {
		System.out.println("enter into saveRemBal function ");
		int temp=0;
		List<LhpvBal> lhpvBalList =	voucherService.getLhpvBalList();
		
		User currentUser = (User) httpSession.getAttribute("currentUser");
		
		if(!lhpvBalList.isEmpty()){
			
			LhpvBal lhpvBal=lhpvBalList.get(0);
			
			ArrivalReport arrivalReport=null;
			
			try{
				session = this.sessionFactory.openSession();
				transaction =  session.beginTransaction();
				
				System.out.println("branchCode: "+currentUser.getUserBranchCode());
				
				if (Integer.parseInt(currentUser.getUserBranchCode()) != 23) {
					
					String date="2017-10-16";

					java.sql.Date chlnDate=java.sql.Date.valueOf(date);

					
					System.out.println("chlnDate= "+chlnDate+" difference = "+lhpvBal.getChallan().getChlnDt().compareTo(chlnDate));
					
					//This code is for avoiding arrival at the time of balance payment if chlnDt is less than 2017-10-16
					if(lhpvBal.getChallan().getChlnDt().before(chlnDate) && lhpvBal.getChallan().getChlnArId()==-1){
						temp=1;
						System.out.println("My code is running smoothly");
					}else{

						arrivalReport = (ArrivalReport) session.get(ArrivalReport.class,lhpvBal.getChallan().getChlnArId());
						double arRemWtShrtg = arrivalReport.getArRemWtShrtg() - lhpvBal.getLbWtShrtgCR();
						double arRemDrRcvrWt=arrivalReport.getArRemDrRcvrWt() - lhpvBal.getLbDrRcvrWtCR();
						double arRemLateDelivery = arrivalReport.getArRemLateDelivery() - lhpvBal.getLbLateDelCR();
						double arRemLateAck = arrivalReport.getArRemLateAck() - lhpvBal.getLbLateAckCR();
						double arRemExtKm = arrivalReport.getArRemExtKm() - lhpvBal.getLbOthExtKmP();
						double arRemOvrHgt = arrivalReport.getArRemOvrHgt() - lhpvBal.getLbOthOvrHgtP();
						double arPenalty = arrivalReport.getArPenalty() - lhpvBal.getLbOthPnltyP();
						double arOther = arrivalReport.getArOther() - lhpvBal.getLbOthMiscP();
						double arRemDetention = arrivalReport.getArRemDetention() - lhpvBal.getLbUnpDetP();
						double arRemUnloading = arrivalReport.getArRemUnloading() - lhpvBal.getLbUnLoadingP();
											
						arrivalReport.setArRemWtShrtg(arRemWtShrtg);
						arrivalReport.setArRemDrRcvrWt(arRemDrRcvrWt);
						arrivalReport.setArRemLateDelivery(arRemLateDelivery);
						arrivalReport.setArRemLateAck(arRemLateAck);
						arrivalReport.setArRemExtKm(arRemExtKm);
						arrivalReport.setArRemOvrHgt(arRemOvrHgt);
						arrivalReport.setArPenalty(arPenalty);
						arrivalReport.setArOther(arOther);
						arrivalReport.setArRemDetention(arRemDetention);
						arrivalReport.setArRemUnloading(arRemUnloading);
											
						session.update(arrivalReport);
						transaction.commit();
					
					}
				}
							
				temp=1;
								
			}catch(Exception e){
				e.printStackTrace();
				temp=-1;
			}
			
			session.clear();
			session.close();
			return temp;
			
		}
		return temp;
	}


	
	
	@Override
	public int saveArRemBal(VoucherService voucherService, Session session) {
		System.out.println("enter into saveRemBal function ");
		int temp=0;
		List<LhpvBal> lhpvBalList =	voucherService.getLhpvBalList();
		
		User currentUser = (User) httpSession.getAttribute("currentUser");
		
		if(!lhpvBalList.isEmpty()){
			
			for(LhpvBal lhpvBal : lhpvBalList) {
				
				ArrivalReport arrivalReport;
				
					
					System.out.println("branchCode: "+currentUser.getUserBranchCode());
					
					if (Integer.parseInt(currentUser.getUserBranchCode()) != 23) {
						
						String date="2017-10-16";

						java.sql.Date chlnDate=java.sql.Date.valueOf(date);

						
						System.out.println("chlnDate= "+chlnDate+" difference = "+lhpvBal.getChallan().getChlnDt().compareTo(chlnDate));
						
						//This code is for avoiding arrival at the time of balance payment if chlnDt is less than 2017-10-16
						if(lhpvBal.getChallan().getChlnDt().before(chlnDate) && lhpvBal.getChallan().getChlnArId()==-1){
							temp=1;
							System.out.println("My code is running smoothly");
						}else{

							arrivalReport = (ArrivalReport) session.get(ArrivalReport.class,lhpvBal.getChallan().getChlnArId());
							double arRemWtShrtg = arrivalReport.getArRemWtShrtg() - lhpvBal.getLbWtShrtgCR();
							double arRemDrRcvrWt=arrivalReport.getArRemDrRcvrWt() - lhpvBal.getLbDrRcvrWtCR();
							double arRemLateDelivery = arrivalReport.getArRemLateDelivery() - lhpvBal.getLbLateDelCR();
							double arRemLateAck = arrivalReport.getArRemLateAck() - lhpvBal.getLbLateAckCR();
							double arRemExtKm = arrivalReport.getArRemExtKm() - lhpvBal.getLbOthExtKmP();
							double arRemOvrHgt = arrivalReport.getArRemOvrHgt() - lhpvBal.getLbOthOvrHgtP();
							double arPenalty = arrivalReport.getArPenalty() - lhpvBal.getLbOthPnltyP();
							double arOther = arrivalReport.getArOther() - lhpvBal.getLbOthMiscP();
							double arRemDetention = arrivalReport.getArRemDetention() - lhpvBal.getLbUnpDetP();
							double arRemUnloading = arrivalReport.getArRemUnloading() - lhpvBal.getLbUnLoadingP();
												
							arrivalReport.setArRemWtShrtg(arRemWtShrtg);
							arrivalReport.setArRemDrRcvrWt(arRemDrRcvrWt);
							arrivalReport.setArRemLateDelivery(arRemLateDelivery);
							arrivalReport.setArRemLateAck(arRemLateAck);
							arrivalReport.setArRemExtKm(arRemExtKm);
							arrivalReport.setArRemOvrHgt(arRemOvrHgt);
							arrivalReport.setArPenalty(arPenalty);
							arrivalReport.setArOther(arOther);
							arrivalReport.setArRemDetention(arRemDetention);
							arrivalReport.setArRemUnloading(arRemUnloading);
												
							session.update(arrivalReport);
							temp=1;
						}
					}
			}

		}
		return temp;
	}

	
	
	
	@Override
	public int saveRemBalSup(VoucherService voucherService) {
		System.out.println("enter into saveRemBalSup function ");
		int temp=0;
		List<LhpvSup> lhpvSupList = voucherService.getLhpvSupList();
		
		if(!lhpvSupList.isEmpty()){
			
			LhpvSup lhpvSup=lhpvSupList.get(0);
			
			ArrivalReport arrivalReport=null;
			
			try{
				session = this.sessionFactory.openSession();
				transaction =  session.beginTransaction();
				
				arrivalReport = (ArrivalReport) session.get(ArrivalReport.class,lhpvSup.getChallan().getChlnArId());
				
				double arRemWtShrtg = arrivalReport.getArRemWtShrtg() - lhpvSup.getLspWtShrtgCR();
				double arRemDrRcvrWt=arrivalReport.getArRemDrRcvrWt() - lhpvSup.getLspDrRcvrWtCR();
				double arRemLateDelivery = arrivalReport.getArRemLateDelivery() - lhpvSup.getLspLateDelCR();
				double arRemLateAck = arrivalReport.getArRemLateAck() - lhpvSup.getLspLateAckCR();
				double arRemExtKm = arrivalReport.getArRemExtKm() - lhpvSup.getLspOthExtKmP();
				double arRemOvrHgt = arrivalReport.getArRemOvrHgt() - lhpvSup.getLspOthOvrHgtP();
				double arPenalty = arrivalReport.getArPenalty() - lhpvSup.getLspOthPnltyP();
				double arOther = arrivalReport.getArOther() - lhpvSup.getLspOthMiscP();
				double arRemDetention = arrivalReport.getArRemDetention() - lhpvSup.getLspUnpDetP();
				double arRemUnloading = arrivalReport.getArRemUnloading() - lhpvSup.getLspUnLoadingP();
			
				arrivalReport.setArRemWtShrtg(arRemWtShrtg);
				arrivalReport.setArRemDrRcvrWt(arRemDrRcvrWt);
				arrivalReport.setArRemLateDelivery(arRemLateDelivery);
				arrivalReport.setArRemLateAck(arRemLateAck);
				arrivalReport.setArRemExtKm(arRemExtKm);
				arrivalReport.setArRemOvrHgt(arRemOvrHgt);
				arrivalReport.setArPenalty(arPenalty);
				arrivalReport.setArOther(arOther);
				arrivalReport.setArRemDetention(arRemDetention);
				arrivalReport.setArRemUnloading(arRemUnloading);
				
				
				session.update(arrivalReport);
				temp=1;
				transaction.commit();
				
			}catch(Exception e){
				e.printStackTrace();
				temp=-1;
			}
			
			session.clear();
			session.close();
			return temp;
			
		}
		return temp;
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public int sedrDtlFrCncl(Map<String, String> sedrDtl){
		 System.out.println("sedrDtlFrCncl()");
		System.out.println("sedrcode="+sedrDtl.get("sedrCode"));
		System.out.println("Date=="+sedrDtl.get("date"));
		String date=sedrDtl.get("date");
		String sedrCode=sedrDtl.get("sedrCode");
		String status="sedr";
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		int temp=0;
		
		try{
			
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			Query query=session.createQuery("SELECT brsld FROM BranchStockLeafDet brsld where brsLeafDetStatus = :status  AND brsLeafDetSNo = :sedrCode");
			query.setParameter("status", status);
			query.setParameter("sedrCode", sedrCode);
			List<BranchStockLeafDet> list=query.list();

			if(!list.isEmpty()){
				BranchSStats branchSStats = new BranchSStats();
				branchSStats.setBrsStatsBrCode(list.get(0).getBrsLeafDetBrCode());
				branchSStats.setBrsStatsType("sedr");
				branchSStats.setBrsStatsSerialNo(list.get(0).getBrsLeafDetSNo());
				branchSStats.setUserCode(currentUser.getUserCode());
				branchSStats.setbCode(currentUser.getUserBranchCode());
				branchSStats.setBrsStatsDt( java.sql.Date.valueOf(date));
				session.save(branchSStats);
				BranchStockLeafDet branchStockLeafDet=list.get(0);
				session.delete(branchStockLeafDet);
				
					ArrivalReport arrivalReport=new ArrivalReport();
					arrivalReport.setArCode(sedrCode);
					arrivalReport.setbCode(list.get(0).getBrsLeafDetBrCode());
					arrivalReport.setArDt(java.sql.Date.valueOf(date));
					arrivalReport.setCancel(true);
					session.save(arrivalReport);
					session.flush();
					session.clear();
					transaction.commit();
					temp=1;
					
			}else{
				temp=0;
			}
		}catch(Exception e){
				transaction.rollback();
			e.printStackTrace();
			temp=0;
		}
		session.close();
		return temp;
	}
	
	
	public List<String> getArChlnCodeFrSrtgDmgAlw(){
		System.out.println("getArChlnCodeFrSrtgDmgAlw()");
		List<String> list=new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria criteria=session.createCriteria(ArrivalReport.class)
					.add(Restrictions.ne(ArrivalReportCNTS.AR_SRTG_DMG, "OK"))
					.add(Restrictions.isNotNull(ArrivalReportCNTS.AR_SRTG_DMG))
					.add(Restrictions.eq(ArrivalReportCNTS.AR_IS_SRTG_DMG_ALW, false));
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property(ArrivalReportCNTS.AR_CHLN_CODE));
			criteria.setProjection(projList);
			list=criteria.list();
		}catch(Exception e){
			e.printStackTrace();
		}
		if(!list.isEmpty()){
			int j=0;
		for(String i : list){
			System.out.println("chlncodeList=="+list.get(j));
				j++;
		}
		}else{
			System.out.println("list is empty");
		}
		session.clear();
		session.close();
		return list;
	}
	
	@Override
	public Memo getLastMemoByBranch(Session session, User user, String branchCode){
		logger.info("UserID = "+user.getUserId()+" : Enter into getMemoNoByBranchCode()");
		Memo memo = null;
		try{
			List<Memo> memoList = session.createCriteria(Memo.class)
					.add(Restrictions.eq(MemoCNTS.MEMO_BRANCH_CODE, branchCode))
					.addOrder(Order.desc(MemoCNTS.MEMO_MEMO_ID))
					.setMaxResults(1)
					.list();
			if(memoList.isEmpty()){
				logger.info("UserID = "+user.getUserId()+" : Last memo not found !");				
			}else{
				memo = memoList.get(0);
				memoList.clear();
				memoList = null;
			}
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId()+" : Exit from getMemoNoByBranchCode()");
		return memo;
	}
	
	@Override
	public List<ArrivalReport> getArDetailForMemo(Session session, User user, String arNo){
		logger.info("UserID = "+user.getUserId()+" : Enter into getArDetailForMemo() : ArNo = "+arNo);
		List<ArrivalReport> arList = null;
		try{
			Criteria cr = session.createCriteria(ArrivalReport.class)
					.add(Restrictions.eq(ArrivalReportCNTS.AR_CODE, arNo))					
					.setFetchMode("memo", FetchMode.SELECT);

			arList = cr.list();
			logger.info("UserID = "+user.getUserId()+" : Ar Size = "+arList.size());
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId()+" : Exit from getArrivalReportWithCondit_Project() ");
		return arList;
	}
	
	@Override
	public Memo getMemoWithChild(Session session, User user, String memoNo){
		logger.info("UserID = "+user.getUserId()+" : Enter into getMemoWithChild() : Memo = "+memoNo);
		Memo memo = null;
		try{
			List<Memo> memoList = session.createCriteria(Memo.class)
					.add(Restrictions.eq(MemoCNTS.MEMO_MEMO_NO, memoNo))					
					.setFetchMode(MemoCNTS.MEMO_ARRIVAL_REPORTS, FetchMode.SELECT)
					.list();
			
			logger.info("UserID = "+user.getUserId()+" : Memo Size = "+memoList.size());
			if(! memoList.isEmpty())
				memo = memoList.get(0);
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId()+" : Exit from getMemoWithChild() ");
		return memo;
	}
	
	@Override
	public Map<String, Object> saveArMemo(Session session, User user, MemoService memoService, Map<String, Object> resultMap){
		logger.info("Enter into saveArMemo()"); 
		try{
			Memo lastMemo = getLastMemoByBranch(session, user, user.getUserBranchCode());
			
			String memoNo = "";
			if(lastMemo == null)
				memoNo = user.getUserBranchCode() + "00000";
			else
				memoNo = lastMemo.getMemoNo();
						
			memoNo = String.valueOf(Long.parseLong(memoNo) + 1);
			if(user.getUserBranchCode().length() == 1)
				memoNo = "0" + memoNo;
			
			lastMemo = null;
			
			logger.info("Memo No = "+memoNo);
			
			Memo memo = memoService.getMemo();	
			memo.setBranchCode(user.getUserBranchCode());
			memo.setUserCode(user.getUserCode());
			memo.setMemoNo(memoNo);
			
			session.save(memo);
			session.refresh(memo);
			
			List<Map<String, Object>> arList = memoService.getArList();
			for(int i=0; i<arList.size(); i++){
				Map<String, Object> map = arList.get(i);
				Integer arId = Integer.parseInt(String.valueOf(map.get(ArrivalReportCNTS.AR_ID)));
				ArrivalReport ar = (ArrivalReport) session.get(ArrivalReport.class, arId);
				logger.info("Ar Found ! "+ar.getArId());
				ar.setMemo(memo);
				memo.getArrivalReports().add(ar);
				
				List<Map<String, Object>> cnmtDt = (List) map.get("cnmtDt");
				for(int j=0; j<cnmtDt.size(); j++){
					Map<String, Object> cnmtMap = cnmtDt.get(j);
					System.out.println("CNMTMap = "+cnmtMap);
					Cnmt cnmt = (Cnmt) session.get(Cnmt.class, Integer.parseInt(String.valueOf(cnmtMap.get("cnmtId"))));
					System.err.println("Cnmt = "+cnmt);
					cnmt.setArRemark(String.valueOf(cnmtMap.get("arRemark")));
					session.merge(cnmt);
				}							
			}
			
			session.merge(memo);
			
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			resultMap.put("msg", "Ar is saved, Memo No = "+memoNo);
			resultMap.put("printMemoNo", memoNo);
			
		}catch(Exception e){
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Please try again !");
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}
		return resultMap;
	}
	
	
	
	@Transactional
	@Override
	public List<Integer> getChlnId(String chlnCode) {
		System.out.println(chlnCode);
		Session session = this.sessionFactory.openSession();
		Criteria criteria = session.createCriteria(Challan.class);
		criteria.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE, chlnCode));
		criteria.setProjection(Projections.property(ChallanCNTS.CHLN_ID));
		List<Integer> chlnId = criteria.list();
		session.close();
		return chlnId;
	}

	@Transactional
	@Override
	public java.util.List<Integer> getCnmtId(java.util.List<Integer> chlnId) {
		System.out.println(chlnId);
		Session session = this.sessionFactory.openSession();
		Criteria criteria = session.createCriteria(Cnmt_Challan.class);
		criteria.add(Restrictions.in(Cnmt_ChallanCNTS.Cnmt_Chln_chlnId, chlnId));
		criteria.setProjection(Projections
				.property(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId));
		List<Integer> cnmtId = criteria.list();
		session.close();
		return cnmtId;
	}

	@Transactional
	@Override
	public java.util.List<com.mylogistics.model.CnmtImage> getCNmtImageData(
			java.util.List<Integer> cnmtId) {

		System.out.println(cnmtId);
		Session session = this.sessionFactory.openSession();
		Criteria criteria = session.createCriteria(CnmtImage.class);
		criteria.add(Restrictions.in(CnmtImageCNTS.CM_CNID, cnmtId));
		List<CnmtImage> cnmtImages = criteria.list();
		session.close();
		return cnmtImages;
	}

	@Transactional
	@Override
	public List<ArrivalReport> getArByDate(java.sql.Date startDate,
			java.sql.Date endDate) {

		System.out.println("From Date " + startDate + " ENd date " + endDate);
		Session session = this.sessionFactory.openSession();
		SQLQuery query = session
				.createSQLQuery("select ar.bCode,ar.archlnCode,ar.arCode,ch.chlnLryNo,ch.chlnDt,ar.arUnloading,ar.arDetention,ar.arDesc,ar.arOvrHgt,ar.arExtKm,"
						+ " ar.arLateAck,ar.arLateDelivery,ar.arWtShrtg,ar.arDrRcvrWt from arrivalreport ar left join challan ch on ar.arChlnCode=ch.chlnCode where ar.arDt>=:startDate and ar.arDt<=:endDate and (isValidate != :isValidate or isValidate is null) "
						+ " and (ar.arRemUnloading >0 or ar.arRemDetention>0 or ar.arRemOvrHgt>0 or ar.arRemExtKm>0 or ar.arRemLateAck>0 or ar.arLateDelivery>0 or ar.arRemWtShrtg>0 or ar.arRemDrRcvrWt>0)");
		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);
		query.setParameter("isValidate", "Y");
		List list = query.list();
		System.out.println(list.size());

		session.close();
		return list;
	}

	@Transactional
	@Override
	public int updateRemAr(java.util.Map<String, Object> map) {
		int i = 0;
		String arNo = map.get("arNo").toString();
		String validate = map.get("validate").toString();
		String remark = map.get("remark").toString();
		Session session = this.sessionFactory.openSession();
		String sql = "update ArrivalReport set isValidate=?,arDesc=?,arRemUnloading=0,arRemDetention=0"
				+ ",arRemOvrHgt=0,arRemExtKm=0,arRemLateAck=0,arRemLateDelivery=0,arRemWtShrtg=0,arRemDrRcvrWt=0  where arCode=?";
		System.out.println(sql);
		Query query = session.createQuery(sql);
		query.setParameter(0, validate);
		query.setParameter(1, remark);
		query.setParameter(2, arNo);
		i = query.executeUpdate();
		session.close();
		return i;
	}

	@Transactional(readOnly = true)
	public List<ArrivalReport> getArrivalReportByArCode(String arCode) {
		Session session = this.sessionFactory.openSession();
		Criteria criteria = session.createCriteria(ArrivalReport.class);
		criteria.add(Restrictions.eq(ArrivalReportCNTS.AR_CODE, arCode));
		List<ArrivalReport> arrivalReports = criteria.list();
		session.close();
		return arrivalReports;

	}

	@Transactional()
	@Override
	public int doValidate(java.util.Map<String, Object> map) {
		int i = 0;
		System.out.println(map);
		String arNo = map.get("arNo").toString();
		String validate = map.get("validate").toString();
		double unloading = 0;
		if (!map.get("unloading").toString().isEmpty()) {
			unloading = Double.parseDouble(map.get("unloading").toString());
		}
		String amt = map.get("detention").toString();
		System.out.println("OverHeight is " + map.get("overheight").toString());
		Double detention = (double) 0;
		if (!amt.isEmpty()) {
			detention = Double.parseDouble(amt);
		}

		double overheight = 0;
		if (!map.get("overheight").toString().isEmpty()) {
			overheight = Double.parseDouble(map.get("overheight").toString());
		}

		double extraKm = 0;

		if (!map.get("extraKm").toString().isEmpty()) {
			extraKm = Double.parseDouble(map.get("extraKm").toString());
		}

		double lateAck = 0;
		if (!map.get("lateAck").toString().isEmpty()) {
			lateAck = Double.parseDouble(map.get("lateAck").toString());
		}

		double lateDelivery = 0;

		if (!map.get("lateDelivery").toString().isEmpty()) {
			lateDelivery = Double.parseDouble(map.get("lateDelivery")
					.toString());
		}

		double wtShrtg = 0;

		if (!map.get("wtShrtg").toString().isEmpty()) {
			wtShrtg = Double.parseDouble(map.get("wtShrtg").toString());
		}

		double drClaim = 0;
		if (!map.get("drClaim").toString().isEmpty()) {
			drClaim = Double.parseDouble(map.get("drClaim").toString());
		}
		// Double.parseDouble(map.get("drClaim").toString());
		String remark = map.get("remark").toString();
		double arClaim = (lateAck + lateDelivery + wtShrtg + drClaim);
		@SuppressWarnings("unchecked")
		List<Object> list = (List<Object>) map.get("arActualData");

		List<ArrivalReport> arrivalReports = getArrivalReportByArCode(arNo);

		double arRemUnloading = arrivalReports.get(0).getArRemUnloading()
				- (Double.parseDouble(list.get(5).toString()) - unloading);
		double arRemDetention = arrivalReports.get(0).getArRemDetention()
				- (Double.parseDouble(list.get(6).toString()) - detention);
		double arRemOverHeight = arrivalReports.get(0).getArRemOvrHgt()
				- (Double.parseDouble(list.get(8).toString()) - overheight);
		double arRemExtreaKm = arrivalReports.get(0).getArRemExtKm()
				- (Double.parseDouble(list.get(9).toString()) - extraKm);
		double arRemLateAck = arrivalReports.get(0).getArRemLateAck()
				- (Double.parseDouble(list.get(10).toString()) - lateAck);
		double arRemLtDlvry = arrivalReports.get(0).getArRemLateDelivery()
				- (Double.parseDouble(list.get(11).toString()) - lateDelivery);
		double arRemwshrtg = arrivalReports.get(0).getArRemWtShrtg()
				- (Double.parseDouble(list.get(12).toString()) - wtShrtg);
		double arRemdrClaim = arrivalReports.get(0).getArRemDrRcvrWt()
				- (Double.parseDouble(list.get(13).toString()) - drClaim);

		Session session = this.sessionFactory.openSession();
		String sql = "update ArrivalReport set isValidate=?,arDesc=?,arUnloading=?,arDetention=?"
				+ ",arOvrHgt=?,arExtKm=?,arLateAck=?,arLateDelivery=?,arWtShrtg=?,arDrRcvrWt=?,arRemUnloading=?,arRemDetention=?"
				+ ",arRemOvrHgt=?,arRemExtKm=?,arRemLateAck=?,arRemLateDelivery=?,arRemWtShrtg=?,arRemDrRcvrWt=?,arClaim=?  where arCode=?";
		System.out.println(sql);
		Query query = session.createQuery(sql);
		query.setParameter(0, validate);
		query.setParameter(1, remark);
		query.setParameter(2, unloading);
		query.setParameter(3, detention);
		query.setParameter(4, overheight);
		query.setParameter(5, extraKm);
		query.setParameter(6, lateAck);
		query.setParameter(7, lateDelivery);
		query.setParameter(8, wtShrtg);
		query.setParameter(9, drClaim);
		query.setParameter(10, arRemUnloading);
		query.setParameter(11, arRemDetention);
		query.setParameter(12, arRemOverHeight);
		query.setParameter(13, arRemExtreaKm);
		query.setParameter(14, arRemLateAck);
		query.setParameter(15, arRemLtDlvry);
		query.setParameter(16, arRemwshrtg);
		query.setParameter(17, arRemdrClaim);
		query.setParameter(18, arClaim);
		query.setParameter(19, arNo);

		if (arRemUnloading < 0 || arRemDetention < 0 || arRemOverHeight < 0
				|| arRemExtreaKm < 0 || arRemdrClaim < 0 || arRemLateAck < 0
				|| arRemLtDlvry < 0 || arRemwshrtg < 0) {
			i = 0;
		} else {
			i = query.executeUpdate();
			System.out.println(i);
		}
		return i;

	}	
	
}
package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.ChqPayVoucherDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.BrokerCNTS;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.constants.EmployeeCNTS;
import com.mylogistics.constants.OwnerCNTS;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.constants.bank.ChequeLeavesCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Broker;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.model.Employee;
import com.mylogistics.model.Owner;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;
//import java.util.Date;

public class ChqPayVoucherDAOImpl implements ChqPayVoucherDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	private HttpSession httpSession;
	
	
	@Autowired
	public ChqPayVoucherDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String,Object> saveChqPayVoucher(VoucherService voucherService , List<Map<String,Object>> faList){
		System.out.println("enter into saveChqPayVoucher function");
		Map<String,Object> map = new HashMap<String, Object>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
		//java.sql.Date sqlDate = new java.sql.Date(new Date().getTime());
		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		char chequeType = voucherService.getChequeType();
		int voucherNo = 0;
		
		Date date = new Date(new java.util.Date().getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		System.out.println("unique id =====>" + calendar.getTimeInMillis());
		long milliSec = calendar.getTimeInMillis();
		String tvNo = "";
		
		if(!faList.isEmpty()){
			try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				
				 java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
				 
				 Criteria crr = session.createCriteria(Branch.class);
				 crr.add(Restrictions.eq(BranchCNTS.BRANCH_FA_CODE,brFaCode));
				 List<Branch> brList = crr.list();
				 String branchCode = brList.get(0).getBranchCode();
				 
		
				 Criteria cr = session.createCriteria(CashStmt.class);
				 cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
				 cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
				 ProjectionList proList = Projections.projectionList();
				 proList.add(Projections.property("csVouchNo"));
				 cr.setProjection(proList);
					
				 List<Integer>  voucherNoList = cr.list();
				 /*if(!voucherNoList.isEmpty()){
						int vNo = voucherNoList.get(voucherNoList.size() - 1);
						voucherNo = vNo + 1;
				 }else{
						voucherNo = 1;
				 }*/
				 
				 List<CashStmt> clientCsList = new ArrayList<CashStmt>();
				 CashStmtStatus clientCSS = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
				 clientCsList = clientCSS.getCashStmtList();
				 
				 if(!clientCsList.isEmpty()){
					 voucherNo = 0;
					 //int vNo  = clientCsList.get(clientCsList.size() - 1).getCsVouchNo();
					 for(int i=0;i<clientCsList.size();i++){
						 if(voucherNo < clientCsList.get(i).getCsVouchNo()){
							 voucherNo = clientCsList.get(i).getCsVouchNo();
						 }
					 }
					 voucherNo = voucherNo + 1;
				 }else{
					 voucherNo = 1;
				 }
				 
				 double tdsAmt = voucherService.getTdsAmt();
				 String tdsCode = voucherService.getTdsCode();
				 double totAmtFQ = 0.0;
				 boolean chqUsed = true;
				 
				 String payMode = "";
				 
					if(voucherService.getPayBy() == 'Q'){
						//Cheque updation start
						payMode = "Q";
						List<ChequeLeaves> chqList = new ArrayList<ChequeLeaves>();
						ChequeLeaves chequeLeaves = voucherService.getChequeLeaves();
						System.out.println("chequeLeaves id = "+chequeLeaves.getChqLId());
						Criteria cr2 = session.createCriteria(ChequeLeaves.class);
						cr2.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_ID,chequeLeaves.getChqLId()));
						chqList = cr2.list();
						System.out.println("size of chqList = "+chqList.size());
						ChequeLeaves chql = chqList.get(0);
						
						
						for(int i=0;i<faList.size();i++){
							Map<String,Object> clientMap = faList.get(i);
							double faAmt1 = Double.parseDouble(String.valueOf(clientMap.get("amt")));
							double faAmt = (double) faAmt1;
							totAmtFQ = totAmtFQ + faAmt;
						}
						
						chql.setChqLChqAmt(totAmtFQ - tdsAmt);
						chql.setChqLUsedDt(new Date(new java.util.Date().getTime()));
						chql.setChqLUsed(true);
						tvNo = chql.getChqLChqNo();
						
						session.update(chql);
						//Cheque updation end
						
						//BankMstr update start
						BankMstr bank = chql.getBankMstr();
						double balAmt = bank.getBnkBalanceAmt();
						double newBalAmt = balAmt - totAmtFQ;
						bank.setBnkBalanceAmt(newBalAmt);
						session.merge(bank);
						//BankMstr update end
						
					}else{
						payMode = "O";
						chqUsed = false;
						
						for(int i=0;i<faList.size();i++){
							Map<String,Object> clientMap = faList.get(i);
							double faAmt1 = Double.parseDouble(String.valueOf(clientMap.get("amt")));
							double faAmt = (double) faAmt1;
							totAmtFQ = totAmtFQ + faAmt;
						}
						
						List<BankMstr> bnkList = new ArrayList<>();
						cr = session.createCriteria(BankMstr.class);
						cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE,voucherService.getBankCode()));
						bnkList = cr.list();
						if(!bnkList.isEmpty()){
							//BankMstr update start
							BankMstr bank = bnkList.get(0);
							double balAmt = bank.getBnkBalanceAmt();
							double newBalAmt = balAmt - totAmtFQ;
							bank.setBnkBalanceAmt(newBalAmt);
							session.merge(bank);
							//BankMstr update end
						}
					}
					
					//main Bank entry which effect BankMstr
					CashStmt mainCs = new CashStmt(); 
					mainCs.setbCode(currentUser.getUserBranchCode());
					mainCs.setUserCode(currentUser.getUserCode());
					mainCs.setCsDescription(voucherService.getDesc());
					mainCs.setCsDrCr('C');
					mainCs.setCsAmt(totAmtFQ - tdsAmt);
					mainCs.setCsType(voucherService.getVoucherType());
					mainCs.setCsVouchType("bank");
					if(chqUsed){
						mainCs.setCsChequeType(chequeType);
					}	
					mainCs.setCsPayTo(voucherService.getPayTo());
					mainCs.setCsFaCode(voucherService.getBankCode());
					
					if(voucherService.getPayBy() == 'O'){
						tvNo = String.valueOf(milliSec);
					}
					mainCs.setCsTvNo(tvNo);
					mainCs.setCsDt(sqlDate);
					mainCs.setCsVouchNo(voucherNo);
					mainCs.setPayMode(payMode);
					
					Criteria cr4 = session.createCriteria(CashStmtStatus.class);
					cr4.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
					cssList = cr4.list();
					if(!cssList.isEmpty()){
						CashStmtStatus csStatus = cssList.get(0);
						mainCs.setCsNo(csStatus);
						csStatus.getCashStmtList().add(mainCs);
						session.update(csStatus);
					}
				    //end main Bank entry which effect BankMstr
					
					
					// InterBranch Case Main Cash Stmt Start
					/*double amount = 0.0;
					int count = 0;
					for(int i=0;i<faList.size();i++){
						boolean interBr = false;
						Map<String,Object> clientMap = faList.get(i);
						String faCode = (String) clientMap.get("faCode"); 
						String custBrCode = (String) clientMap.get("custBrCode"); 
						int faAmt1 = (Integer) clientMap.get("amt");
						double faAmt = (double) faAmt1;
						String decided = faCode.substring(0,2);
						
						
						if(custBrCode != null){
							if(custBrCode.equalsIgnoreCase(brFaCode)){
								interBr = false;
							}else{
								interBr = true;
							}
						}else{
							interBr = false;
						}
						
						
						
						if(decided.equalsIgnoreCase(ConstantsValues.FAPC_EMP)){
							List<Employee> empList = new ArrayList<Employee>(); 
						 	Criteria cr3 = session.createCriteria(Employee.class);
							cr3.add(Restrictions.eq(EmployeeCNTS.EMP_FA_CODE,faCode));
							empList = cr3.list();
							
							String empBrCode = empList.get(0).getBranchCode();
							
							if(empBrCode.equalsIgnoreCase(branchCode)){
								//employee of same branch
							}else{
								interBr = true;
							}
						 }else if(decided.equalsIgnoreCase(ConstantsValues.FAPC_CUST)){
							 if(custBrCode.equalsIgnoreCase(brFaCode)){
								//customer of same branch 
							 }else{
								 interBr = true;
							 } 
						 }else if(decided.equalsIgnoreCase(ConstantsValues.FAPC_OWN)){
							 List<Owner> ownList = new ArrayList<Owner>(); 
							 Criteria cr3 = session.createCriteria(Owner.class);
							 cr3.add(Restrictions.eq(OwnerCNTS.OWN_FA_CODE,faCode));
							 ownList = cr3.list();
							 
							 String ownBrCode = ownList.get(0).getBranchCode();
							 
							 if(ownBrCode.equalsIgnoreCase(branchCode)){
								 //owner of same branch
							 }else{
								 interBr = true;
							 }
						 }else if(decided.equalsIgnoreCase(ConstantsValues.FAPC_BRK)){
							 List<Broker> brkList = new ArrayList<Broker>();
							 Criteria cr3 = session.createCriteria(Broker.class);
							 cr3.add(Restrictions.eq(BrokerCNTS.BRK_FA_CODE,faCode));
							 brkList = cr3.list();
							 
							 String brkBrCode = brkList.get(0).getBranchCode();
							 
							 if(brkBrCode.equalsIgnoreCase(branchCode)){
								 //broker of same branch
							 }else{
								 interBr = true;
							 }
						 }else{
							 //simple entry
						 }
					
						 
						 if(interBr == true){
							 amount = amount + faAmt;
							 count = count + 1;
						 }
					}
					
					System.out.println(count+" faCode are from other branches--");*/
					/*CashStmt mainCashStmt = new CashStmt();
					
					if(count > 0){
						
						mainCashStmt.setbCode(currentUser.getUserBranchCode());
						mainCashStmt.setUserCode(currentUser.getUserCode());
						mainCashStmt.setCsDescription(voucherService.getDesc());
						mainCashStmt.setCsDrCr('C');
						mainCashStmt.setCsAmt(amount);
						mainCashStmt.setCsType(voucherService.getVoucherType());
						mainCashStmt.setCsVouchType("contra");
						mainCashStmt.setCsChequeType(chequeType);
						mainCashStmt.setCsPayTo(voucherService.getPayTo());
						mainCashStmt.setCsFaCode(voucherService.getBankCode());
						mainCashStmt.setCsTvNo(tvNo);
						mainCashStmt.setCsDt(sqlDate);
						mainCashStmt.setCsVouchNo(voucherNo);
						
						Criteria cr4 = session.createCriteria(CashStmtStatus.class);
						cr4.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr4.list();
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							mainCashStmt.setCsNo(csStatus);
							csStatus.getCashStmtList().add(mainCashStmt);
							session.update(csStatus);
							//transaction.commit();
						}
					}*/
					
					// InterBranch Case Main Cash Stmt End
					
					
					for(int i=0;i<faList.size();i++){
						boolean interBr = false;
						Map<String,Object> clientMap = faList.get(i);
						String faCode = (String) clientMap.get("faCode"); 
						String custBrCode = (String) clientMap.get("custBrCode"); 
						double faAmt1 = Double.parseDouble(String.valueOf(clientMap.get("amt")));
						double faAmt = (double) faAmt1;
						String decided = faCode.substring(0,2);
						System.out.println("custBrCode = "+custBrCode);
						//String destContBr = "";
						
						if(custBrCode != null){
							if(custBrCode.equalsIgnoreCase(brFaCode)){
								interBr = false;
							}else{
								interBr = true;
							}
						}else{
							interBr = false;
						}
						
						
						
						/*if(decided.equalsIgnoreCase(ConstantsValues.FAPC_EMP)){
							List<Employee> empList = new ArrayList<Employee>(); 
						 	Criteria cr3 = session.createCriteria(Employee.class);
							cr3.add(Restrictions.eq(EmployeeCNTS.EMP_FA_CODE,faCode));
							empList = cr3.list();
							
							String empBrCode = empList.get(0).getBranchCode();
							
							List<Branch> actBrList = new ArrayList<Branch>();
							cr3 = session.createCriteria(Branch.class);
							cr3.add(Restrictions.eq(BranchCNTS.BRANCH_CODE,empBrCode));
							actBrList = cr3.list();
							destContBr = actBrList.get(0).getBranchFaCode();
							
							if(empBrCode.equalsIgnoreCase(branchCode)){
								//employee of same branch
							}else{
								interBr = true;
							}
						 }else if(decided.equalsIgnoreCase(ConstantsValues.FAPC_CUST)){
							 destContBr = custBrCode;
							 if(custBrCode.equalsIgnoreCase(brFaCode)){
								//customer of same branch 
							 }else{
								 interBr = true;
							 } 
						 }else if(decided.equalsIgnoreCase(ConstantsValues.FAPC_OWN)){
							 List<Owner> ownList = new ArrayList<Owner>(); 
							 Criteria cr3 = session.createCriteria(Owner.class);
							 cr3.add(Restrictions.eq(OwnerCNTS.OWN_FA_CODE,faCode));
							 ownList = cr3.list();
							 
							 String ownBrCode = ownList.get(0).getBranchCode();
							 
							 List<Branch> actBrList = new ArrayList<Branch>();
							 cr3 = session.createCriteria(Branch.class);
							 cr3.add(Restrictions.eq(BranchCNTS.BRANCH_CODE,ownBrCode));
							 actBrList = cr3.list();
							 destContBr = actBrList.get(0).getBranchFaCode();
								
							 if(ownBrCode.equalsIgnoreCase(branchCode)){
								 //owner of same branch
							 }else{
								 interBr = true;
							 }
						 }else if(decided.equalsIgnoreCase(ConstantsValues.FAPC_BRK)){
							 List<Broker> brkList = new ArrayList<Broker>();
							 Criteria cr3 = session.createCriteria(Broker.class);
							 cr3.add(Restrictions.eq(BrokerCNTS.BRK_FA_CODE,faCode));
							 brkList = cr3.list();
							 
							 String brkBrCode = brkList.get(0).getBranchCode();
							 
							 List<Branch> actBrList = new ArrayList<Branch>();
							 cr3 = session.createCriteria(Branch.class);
							 cr3.add(Restrictions.eq(BranchCNTS.BRANCH_CODE,brkBrCode));
							 actBrList = cr3.list();
							 destContBr = actBrList.get(0).getBranchFaCode();
							 
							 if(brkBrCode.equalsIgnoreCase(branchCode)){
								 //broker of same branch
							 }else{
								 interBr = true;
							 }
						 }else{
							 //simple entry
						 }
*/						
						System.out.println("********interBr********* = "+interBr);
						if(interBr == false){
							
							 //TDS entry start
							/* String tdsCode = voucherService.getTdsCode();
								double tdsAmt = voucherService.getTdsAmt();*/
								System.out.println("tdsCode = "+tdsCode+" --- tdsAmt = "+tdsAmt);
								if(tdsCode != null && tdsAmt > 0){
									
									CashStmt csWTds = new CashStmt();
									csWTds.setbCode(currentUser.getUserBranchCode());
									csWTds.setUserCode(currentUser.getUserCode());
									csWTds.setCsDescription(voucherService.getDesc());
									csWTds.setCsDrCr('C');
									csWTds.setCsAmt(tdsAmt);
									csWTds.setCsType(voucherService.getVoucherType());
									csWTds.setCsVouchType("bank");
									if(chqUsed){
										csWTds.setCsChequeType(chequeType);
									}
									csWTds.setCsPayTo(voucherService.getPayTo());
									if(voucherService.getPayBy() == 'O'){
										tvNo = String.valueOf(milliSec);
									}
									csWTds.setCsTvNo(tvNo);
									csWTds.setCsFaCode(tdsCode);
									csWTds.setCsVouchNo(voucherNo);
									csWTds.setCsDt(sqlDate);
									csWTds.setPayMode(payMode);
									
									Criteria cr1 = session.createCriteria(CashStmtStatus.class);
									cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
									cssList = cr1.list();
									
									if(!cssList.isEmpty()){
										CashStmtStatus csStatus = cssList.get(0);
										csWTds.setCsNo(csStatus);
										csStatus.getCashStmtList().add(csWTds);
										System.out.println("tds case successfully saved");
										session.update(csStatus);
										//transaction.commit();
									}	
									
									session.save(csWTds);
									
								}
								//TDS entry end
								
							CashStmt cashStmt = new CashStmt();
							cashStmt.setbCode(currentUser.getUserBranchCode());
							cashStmt.setUserCode(currentUser.getUserCode());
							cashStmt.setCsDescription(voucherService.getDesc());
							cashStmt.setCsDrCr('D');
							cashStmt.setCsAmt(faAmt);
							cashStmt.setCsType(voucherService.getVoucherType());
							cashStmt.setCsVouchType("bank");
							cashStmt.setCsPayTo(voucherService.getPayTo());
							if(chqUsed){
								cashStmt.setCsChequeType(chequeType);
							}
							if(voucherService.getPayBy() == 'O'){
								tvNo = String.valueOf(milliSec);
							}
							cashStmt.setCsTvNo(tvNo);
							cashStmt.setCsFaCode(faCode);
							cashStmt.setCsVouchNo(voucherNo);
							cashStmt.setCsDt(sqlDate);
							cashStmt.setPayMode(payMode);
							
							Criteria cr5 = session.createCriteria(CashStmtStatus.class);
							cr5.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = cr5.list();
							
							if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt.setCsNo(csStatus);
								session.save(cashStmt);
								csStatus.getCashStmtList().add(cashStmt);
								session.update(csStatus);
							}	
							
					/*		
							CashStmt cStmt = new CashStmt();
							cStmt.setbCode(currentUser.getUserBranchCode());
							cStmt.setUserCode(currentUser.getUserCode());
							cStmt.setCsDescription(voucherService.getDesc());
							cStmt.setCsDrCr('C');
							cStmt.setCsAmt(faAmt);
							cStmt.setCsType(voucherService.getVoucherType());
							cStmt.setCsVouchType("bank");
							cStmt.setCsChequeType(chequeType);
							cStmt.setCsPayTo(voucherService.getPayTo());
							cStmt.setCsVouchNo(voucherNo);
							cStmt.setCsTvNo(tvNo);
							cStmt.setCsFaCode(voucherService.getBankCode());
							cStmt.setCsDt(sqlDate);
							
							 
							Criteria cr6 = session.createCriteria(CashStmtStatus.class);
							cr6.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = cr6.list();
							
							if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								cStmt.setCsNo(csStatus);
								session.save(cStmt);
								csStatus.getCashStmtList().add(cStmt);
								session.update(csStatus);
							}	*/
							
						
						}else{
							
							CashStmt cashStmt = new CashStmt();
							cashStmt.setbCode(currentUser.getUserBranchCode());
							cashStmt.setUserCode(currentUser.getUserCode());
							cashStmt.setCsDescription(voucherService.getDesc());
							cashStmt.setCsDrCr('D');
							cashStmt.setCsAmt(totAmtFQ - tdsAmt);
							cashStmt.setCsType(voucherService.getVoucherType());
							cashStmt.setCsVouchType("bank");
							cashStmt.setCsPayTo(voucherService.getPayTo());
							if(chqUsed){
								cashStmt.setCsChequeType(chequeType);
							}
							if(voucherService.getPayBy() == 'O'){
								tvNo = String.valueOf(milliSec);
							}
							cashStmt.setPayMode(payMode);
							cashStmt.setCsTvNo(tvNo);
							cashStmt.setCsFaCode(custBrCode);
							cashStmt.setCsVouchNo(voucherNo);
							cashStmt.setCsDt(sqlDate);
							 
							Criteria cr5 = session.createCriteria(CashStmtStatus.class);
							cr5.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = cr5.list();
							
							if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt.setCsNo(csStatus);
								session.save(cashStmt);
								csStatus.getCashStmtList().add(cashStmt);
								session.update(csStatus);
							}	
							
							
							CashStmtTemp cashStmt1 = new CashStmtTemp();
							cashStmt1.setbCode(currentUser.getUserBranchCode());
							cashStmt1.setUserCode(currentUser.getUserCode());
							cashStmt1.setCsDescription(voucherService.getDesc());
							cashStmt1.setCsDrCr('C');
							cashStmt1.setCsAmt(faAmt - tdsAmt);
							cashStmt1.setCsType(voucherService.getVoucherType());
							cashStmt1.setCsVouchType("contra");
							if(chqUsed){
								cashStmt1.setCsChequeType(chequeType);
							}
							if(voucherService.getPayBy() == 'O'){
								tvNo = String.valueOf(milliSec);
							}
							cashStmt1.setCsTvNo(tvNo);
							cashStmt1.setCsPayTo(voucherService.getPayTo());
							cashStmt1.setCsFaCode(brFaCode);
							cashStmt1.setCsVouchNo(voucherNo);
							cashStmt1.setCsDt(sqlDate);
							cashStmt1.setCsBrhFaCode(custBrCode);
							cashStmt1.setPayMode(payMode);
							
							session.save(cashStmt1);
						/*	Criteria cr7 = session.createCriteria(CashStmtStatus.class);
							cr7.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = cr7.list();
							
							if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt1.setCsNo(csStatus);
							    //session.save(cashStmt1);
								csStatus.getCashStmtList().add(cashStmt1);
								session.update(csStatus);
							}*/
							
							
							CashStmtTemp cashStmt2 = new CashStmtTemp();
							cashStmt2.setbCode(currentUser.getUserBranchCode());
							cashStmt2.setUserCode(currentUser.getUserCode());
							cashStmt2.setCsDescription(voucherService.getDesc());
							cashStmt2.setCsDrCr('D');
							cashStmt2.setCsAmt(faAmt);
							cashStmt2.setCsType(voucherService.getVoucherType());
							cashStmt2.setCsVouchType("contra");
							if(chqUsed){
								cashStmt2.setCsChequeType(chequeType);
							}
							cashStmt2.setCsPayTo(voucherService.getPayTo());
							if(voucherService.getPayBy() == 'O'){
								tvNo = String.valueOf(milliSec);
							}
							cashStmt2.setCsTvNo(tvNo);
							cashStmt2.setCsFaCode(faCode);
							cashStmt2.setCsVouchNo(voucherNo);
							cashStmt2.setCsDt(sqlDate);
							cashStmt2.setCsBrhFaCode(custBrCode);
							cashStmt2.setPayMode(payMode);
							
							session.save(cashStmt2);
							/*Criteria cr8 = session.createCriteria(CashStmtStatus.class);
							cr8.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = cr8.list();
							if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt2.setCsNo(csStatus);
								csStatus.getCashStmtList().add(cashStmt2);
								session.update(csStatus);
							}*/
							
							if(tdsCode != null && tdsAmt > 0){
								CashStmtTemp cashStmt3 = new CashStmtTemp();
								cashStmt3.setbCode(currentUser.getUserBranchCode());
								cashStmt3.setUserCode(currentUser.getUserCode());
								cashStmt3.setCsDescription(voucherService.getDesc());
								cashStmt3.setCsDrCr('C');
								cashStmt3.setCsAmt(tdsAmt);
								cashStmt3.setCsType(voucherService.getVoucherType());
								cashStmt3.setCsVouchType("contra");
								if(chqUsed){
									cashStmt3.setCsChequeType(chequeType);
								}
								cashStmt3.setCsPayTo(voucherService.getPayTo());
								if(voucherService.getPayBy() == 'O'){
									tvNo = String.valueOf(milliSec);
								}
								cashStmt3.setCsTvNo(tvNo);
								cashStmt3.setCsFaCode(tdsCode);
								cashStmt3.setCsVouchNo(voucherNo);
								cashStmt3.setCsDt(sqlDate);
								cashStmt3.setCsBrhFaCode(custBrCode);
								cashStmt3.setPayMode(payMode);
								
								session.save(cashStmt3);
							}
							
							/*Criteria cr9 = session.createCriteria(CashStmtStatus.class);
							cr9.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = cr9.list();
							if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt3.setCsNo(csStatus);
								csStatus.getCashStmtList().add(cashStmt3);
								session.update(csStatus);
							}*/
							
						}
						
					}
					
					transaction.commit();
						
					map.put("vhNo",voucherNo);
			}catch(Exception e){
				e.printStackTrace();
				map.put("vhNo",0);
			}
			session.clear();
			session.close();
		}else{
			System.out.println("faList is empty");
			map.put("vhNo",0);
		}
		
		return map;
	}
	
}

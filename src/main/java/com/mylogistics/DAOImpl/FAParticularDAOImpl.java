package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.FAParticularDAO;
import com.mylogistics.constants.FAParticularCNTS;
import com.mylogistics.model.FAParticular;

public class FAParticularDAOImpl implements FAParticularDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	public static Logger logger = Logger.getLogger(FAParticularDAOImpl.class);
	
	@Autowired
	public FAParticularDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @SuppressWarnings("unchecked")
	 @Transactional
	 public List<FAParticular> getFAParticular(String type){
		logger.info("Enter into getFAParticular() : Type = "+type);
		List<FAParticular> result = new ArrayList<FAParticular>();
		try{
			session = this.sessionFactory.openSession();
			
			Criteria cr=session.createCriteria(FAParticular.class);
			cr.add(Restrictions.eq(FAParticularCNTS.FA_PAR_TYPE, type));
			
			result =cr.list();			
		 }catch(Exception e){
			 logger.error("Exception : "+e);
			 e.printStackTrace();
		 }
		session.flush();
		session.clear();
		session.close();
		logger.info("Exit from getFAParticular() : FaSize = "+result.size());
		return  result; 
	 }
	 
	 
	 @Override
	 public List<FAParticular> getFAParticular(String type,Session session){
			logger.info("Enter into getFAParticular() : Type = "+type);
			List<FAParticular> result = new ArrayList<FAParticular>();
				
				Criteria cr=session.createCriteria(FAParticular.class);
				cr.add(Restrictions.eq(FAParticularCNTS.FA_PAR_TYPE, type));
				
				result =cr.list();			
			logger.info("Exit from getFAParticular() : FaSize = "+result.size());
			return  result; 
		 }
	 
}

package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.FAMasterCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

public class FaMasterDAOImpl implements FaMasterDAO{

	private SessionFactory sessionFactory;
	
	private static Logger logger = Logger.getLogger(FaMasterDAOImpl.class);
	
	@Autowired
	public FaMasterDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	public int saveFaMaster(FAMaster fAMaster){
		logger.info("Enter into saveFaMaster() ");
		int tmp;
		Session session = this.sessionFactory.openSession();
	     Transaction transaction =  session.beginTransaction();
		try{
			 session.save(fAMaster);
			 transaction.commit();
			 tmp= 1;	
			 logger.info("FaMaster is saved !");
		}catch(Exception e){
			 logger.error("Exception : "+e);
			 tmp= -1;
		}
		session.clear();
		session.close();
		logger.info("Exit from saveFaMaster()");
		return tmp;
	}
	
	 @Transactional
	 public int updateFaMaster(FAMaster fAMaster){
		 int temp;
		 Date date = new Date();
	 	 Calendar calendar = Calendar.getInstance();
	 	 calendar.setTime(date);
	 	Session session = this.sessionFactory.openSession();
	     Transaction transaction =  session.beginTransaction();
			try{
				 fAMaster.setCreationTS(calendar);
				 session.saveOrUpdate(fAMaster);
				 transaction.commit();
				 temp= 1;
				 session.flush();
			}catch(Exception e){
				e.printStackTrace();
				temp= -1;
			}
			session.clear();
			session.close();
			return temp;
	 }
	 
	 @Override
	 public void updateFaMaster(FAMaster fAMaster,Session session){
		 Date date = new Date();
	 	 Calendar calendar = Calendar.getInstance();
	 	 calendar.setTime(date);
				 fAMaster.setCreationTS(calendar);
				 session.saveOrUpdate(fAMaster);
	 }
	 
	    @SuppressWarnings("unchecked")
		@Transactional
		public List<FAMaster> retrieveFAMaster(String faMfaCode){
			List<FAMaster> list = new ArrayList<FAMaster>();
			Session session = this.sessionFactory.openSession();
			try{
				 Criteria cr=session.createCriteria(FAMaster.class);
				 cr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_CODE,faMfaCode));
				 list =cr.setMaxResults(1).list();
				 session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
		    session.close();
			return list; 
		 }
	    
	    public List<FAMaster> retrieveFAMaster(String faMfaCode,Session session){
			List<FAMaster> list = new ArrayList<FAMaster>();
				 Criteria cr=session.createCriteria(FAMaster.class);
				 cr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_CODE,faMfaCode));
				 list =cr.setMaxResults(1).list();
			return list; 
		 }
	
	
	    
	    @SuppressWarnings("unchecked")
		@Transactional
	    public List<FAMaster> getMultiFaM(String faMfaType){
	    	System.out.println("enter into getMultiFaM function");
	    	List<FAMaster> faMList = new ArrayList<FAMaster>();
	    	Session session = this.sessionFactory.openSession();
	    	try{
				 Criteria cr=session.createCriteria(FAMaster.class);
				 cr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_TYPE,faMfaType));
				 faMList =cr.list();
				 session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
	    	session.clear();
		    session.close();
			return faMList; 
	    }
	    
	    
	    @SuppressWarnings("unchecked")
		@Transactional
	    public List<Map<String, Object>> getMultiFaMByNameOrCode(String faNameCode){
	    	logger.info("Enter into getMultiFaMByName() : faNmae = "+faNameCode);
	    	List<Map<String, Object>> faMList = new ArrayList<Map<String, Object>>();
	    	List<String> list = new ArrayList<String>();
	    	Session session = this.sessionFactory.openSession();
	    	try{
	    		
	    		list.add("employee");
	    		list.add("owner");
	    		list.add("broker");
	    		list.add("miscfa");
	    		list.add("profitnloss");
	    		list.add("assestsnliab");    		  		
	    		
				Criteria cr=session.createCriteria(FAMaster.class);
				if(NumberUtils.isNumber(faNameCode))
					cr.add(Restrictions.like(FAMasterCNTS.FA_MFA_CODE, "%"+faNameCode));
				else
					cr.add(Restrictions.like(FAMasterCNTS.FA_MFA_NAME, faNameCode+"%"));
				cr.add(Restrictions.in(FAMasterCNTS.FA_MFA_TYPE, list));				
				List mstrList = cr.list();
				if(mstrList.isEmpty())
					logger.info("FaMaster not found !");
				else{
					logger.info("FaMaster found : Size = "+mstrList.size());
					Iterator<FAMaster> it = mstrList.iterator();
					while(it.hasNext()){
						FAMaster mstr = (FAMaster) it.next();
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("faMfaCode", mstr.getFaMfaCode());						
						map.put("faMfaName", mstr.getFaMfaName());
						map.put("faMfaType", mstr.getFaMfaType());
						
						String bCode = mstr.getbCode();			
						
						Criteria brhCriteria = session.createCriteria(Branch.class);
						brhCriteria.add(Restrictions.eq(BranchCNTS.BRANCH_CODE, bCode));
						ProjectionList projectionList = Projections.projectionList();
						projectionList.add(Projections.property(BranchCNTS.BRANCH_NAME));
						brhCriteria.setProjection(projectionList);
						List<String> brhList = brhCriteria.list();
						
						if(! brhList.isEmpty()){
							String branchName = brhList.get(0);
							map.put("branchName", branchName);
						}
						faMList.add(map);
					}
				}
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
				logger.error("Exception : "+e);
			}
	    	session.clear();
		    session.close();
		    logger.info("Exit from getMultiFaMByName()");
			return faMList; 
	    }
	    
	    
	    @SuppressWarnings("unchecked")
		@Transactional
	    public List<FAMaster> getFAMByFaName(String faMfaName){
	    	System.out.println("enter into getFAMByFaName function");
	    	List<FAMaster> faMList = new ArrayList<FAMaster>();
	    	Session session = this.sessionFactory.openSession();
	    	try{
				 Criteria cr=session.createCriteria(FAMaster.class);
				 cr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,faMfaName));
				 faMList =cr.list();
				 session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
	    	session.clear();
		    session.close();
			return faMList; 
	    }
	    
	    
	    @SuppressWarnings("unchecked")
		@Transactional
	    public List<FAMaster> getAllTdsFAM(){
	    	logger.info("Enter into getAllTdsFAM()");
	    	System.out.println("enter into getAllTdsFAM function");
	    	List<FAMaster> faMList = new ArrayList<FAMaster>();
	    	String TDS = "TDS";
	    	Session session = this.sessionFactory.openSession();
	    	try{
				 faMList = session.createQuery("from FAMaster where faMfaName like :tds")
			       .setParameter("tds", TDS + "%")
			       .list();
				 if(faMList.isEmpty())
					 logger.info("No TDs found !");
				 else
					 logger.info("TDs found : Size = "+faMList.size());				 
				 session.flush();
			}catch(Exception e){
				logger.error("Exception : "+e);
				e.printStackTrace();
			}
	    	session.clear();
		    session.close();
		    logger.info("Exit from getAllTdsFAM()");
			return faMList; 
	    }
	    
	    
	    
	    @SuppressWarnings("unchecked")
		@Transactional
	    public List<FAMaster> getAllFaMstr(){
	    	System.out.println("enter into getAllFaMstr");
	    	List<FAMaster> faMList = new ArrayList<FAMaster>(); 
	    	Session session = this.sessionFactory.openSession();
	    	try{
				 Criteria cr=session.createCriteria(FAMaster.class);
				 cr.add(Restrictions.neOrIsNotNull(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.TEL_FANAME));
				 cr.add(Restrictions.neOrIsNotNull(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.ELECT_FANAME));
				 cr.add(Restrictions.neOrIsNotNull(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.CAR_EXP_FANAME));
				 cr.add(Restrictions.neOrIsNotNull(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.SANDM_EXP_FANAME));
				 cr.add(Restrictions.neOrIsNotNull(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.TRAV_EXP_FANAME));
				 cr.add(Restrictions.neOrIsNotNull(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.B_PROM_FANAME));
				 cr.add(Restrictions.neOrIsNotNull(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.TDS_FANAME));
				 
				 faMList = cr.list(); 
			
				session.flush();
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
	    	session.clear();
	    	session.close();
	    	return faMList;
	    }
	    
	    @SuppressWarnings("unchecked")
		@Transactional
	    public List<FAMaster> getAllFaMstrByFaNameOrCode(String faNameCode){
	    	System.out.println("Enter into getAllFaMstr() : FaNameCode = "+faNameCode);
	    	List<FAMaster> faMList = new ArrayList<FAMaster>(); 
	    	Session session = this.sessionFactory.openSession();
	    	try{
				Criteria cr=session.createCriteria(FAMaster.class);
				if(NumberUtils.isNumber(faNameCode))
					 cr.add(Restrictions.like(FAMasterCNTS.FA_MFA_CODE, "%"+faNameCode));
				 else
					 cr.add(Restrictions.like(FAMasterCNTS.FA_MFA_NAME, faNameCode+"%")); 
				
				faMList = cr.list();			
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
	    	session.clear();
	    	session.close();
	    	return faMList;
	    }
	    
	    
	    @SuppressWarnings("unchecked")
		@Transactional
	    public List<FAMaster> getAllFAM(){
	    	System.out.println("enter into getAllFAM function");
	    	List<FAMaster> faMList = new ArrayList<FAMaster>();
	    	Session session = this.sessionFactory.openSession();
	    	try{
				Criteria cr=session.createCriteria(FAMaster.class);
				faMList = cr.list();
				
				session.flush();
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
	    	session.clear();
	    	session.close();
	    	return faMList;
	    }
	    
	    
	    @Override
	    @SuppressWarnings("unchecked")
	    public List<FAMaster> getAllFAM(Session session){
	    	System.out.println("enter into getAllFAM function");
	    	List<FAMaster> faMList = new ArrayList<FAMaster>();
				Criteria cr=session.createCriteria(FAMaster.class);
				faMList = cr.list();
	    	return faMList;
	    }
	    
	    

		@Override
		@Transactional
		public List<Map<String, Object>> getFaCodeName() {
			Session session = this.sessionFactory.openSession();
			List<Map<String, Object>> faCodeNameList = new ArrayList<>();
			try {
				Query query = session.createQuery("select new map(faMfaCode as faMfaCode, faMfaName as faMfaName) from FAMaster");
				faCodeNameList = query.list();
				session.flush();
			} catch (Exception e) {
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return faCodeNameList;
		}
		
		@Override
		public Object getFaCodeFrLdr(String faMfaCode){
			System.out.println("getFaCodeFrLdr()");
			List<String> list = new ArrayList<String>();
			Session session = this.sessionFactory.openSession();
			try{
				 Query query = session.createQuery("select faMfaCode from FAMaster where faMfaCode like :faMfaCode");
				 query.setString("faMfaCode", "%"+faMfaCode+"%");
				 list = query.list();
				 System.out.println(list);
				 session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return list; 
		 }
		
		@Override
		public Map<String, String> getAllFAM(Session session, User user, Set<String> faCodes){
			logger.info("UserID = "+user.getUserId() + " : Enter into getAllFAM()");
			Map<String, String> faMList = new HashMap<String, String>();
			try{
				List<FAMaster> list = session.createCriteria(FAMaster.class)
						.add(Restrictions.in(FAMasterCNTS.FA_MFA_CODE, faCodes))
						.list();
				
				if(! list.isEmpty()){
					for(int i=0; i<list.size(); i++){
						FAMaster master = (FAMaster) list.get(i);
						faMList.put(master.getFaMfaCode(), master.getFaMfaName());
					}
				}
				
			}catch(Exception e){
				logger.error("UserID = "+user.getUserId() + " : Exception = "+e);
			}
			logger.info("UserID = "+user.getUserId() + " : Exit from getAllFAM() : FaMaster Size = "+faMList.size());
			return faMList;
		}
		
		/**
		 * @author Mohd Furkan
		 */
		@Override
	    public List<FAMaster> getFAMByFaNameN(Session session, String faMfaName){
	    	logger.info("enter into getFAMByFaName function");
	    	List<FAMaster> faMList = new ArrayList<FAMaster>();
	    	try{
				 Criteria cr=session.createCriteria(FAMaster.class);
				 cr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,faMfaName));
				 faMList =cr.list();
			}catch(Exception e){
				e.printStackTrace();
			}
			return faMList; 
	    }
		
		
		@SuppressWarnings("unchecked")
//		@Transactional
		@Override
	    public List<FAMaster> getFAMByFaName(Session session, String faMfaName){
	    	System.out.println("enter into getFAMByFaName function");
	    	List<FAMaster> faMList = new ArrayList<FAMaster>();
	    	//try{
				 Criteria cr=session.createCriteria(FAMaster.class);
				 cr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,faMfaName));
				 faMList =cr.list();
//			}catch(Exception e){
//				e.printStackTrace();
//			}
			return faMList; 
	    }
		
		@Override
		public int saveFaMaster(FAMaster fAMaster, Session session){
			logger.info("Enter into saveFaMaster() ");
			int tmp;
				 session.save(fAMaster);
				// session.flush();
				 //session.clear();
				 tmp= 1;	
			logger.info("Exit from saveFaMaster()");
			return tmp;
		}

		
}
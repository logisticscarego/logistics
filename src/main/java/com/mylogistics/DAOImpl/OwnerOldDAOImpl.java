package com.mylogistics.DAOImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.OwnerOldDAO;
import com.mylogistics.model.OwnerOld;

public class OwnerOldDAOImpl implements OwnerOldDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public OwnerOldDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	   public int saveOwnerOld(OwnerOld ownerOld){
		 System.out.println("enter into saveOwnerOld function");
		 int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 session.save(ownerOld);
			 transaction.commit();
			 temp = 1;
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
	 
	 
	 public int saveOwnerOld(OwnerOld ownerOld,Session session){
		 System.out.println("enter into saveOwnerOld function");
		 int temp;
		 temp= (int) session.save(ownerOld);
		 return temp;
	 }
	 
}

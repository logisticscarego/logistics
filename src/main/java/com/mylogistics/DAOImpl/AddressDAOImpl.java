package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.omg.CosNaming.IstringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.AddressDAO;
import com.mylogistics.constants.AddressCNTS;
import com.mylogistics.model.Address;
import com.mylogistics.services.ConstantsValues;

public class AddressDAOImpl implements AddressDAO {
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	public static Logger logger = Logger.getLogger(AddressDAOImpl.class);
	
	@Autowired
	public AddressDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	   public int saveAddress(Address address){
		 logger.info("Enter into saveAddress() : Address = "+address);
		 int temp = 0;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 int id = (Integer) session.save(address);
			 transaction.commit();
			 temp =  id;
			 logger.info("Address is saved !");
		 }catch(Exception e){
			 logger.error("Exception : "+e);
			 temp = -1;
		 }
		 session.flush();
		 session.clear();
		 session.close();
		 logger.info("Exit from saveAddress()");
		 return temp;
	 }
	
	 @SuppressWarnings("unchecked")
	 public List<Address> getAddress(String brokerCode,String type){
		 System.out.println("Enter into get Address function--"+type);
		 List<Address> addresses = new ArrayList<Address>();
		 try{
			 session = this.sessionFactory.openSession();
	         Criteria cr = session.createCriteria(Address.class);   
	         cr.add(Restrictions.eq(AddressCNTS.TYPE,type));
	         cr.add(Restrictions.eq(AddressCNTS.REF_CODE,brokerCode));
	         addresses =cr.list();
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return addresses;
	}
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<Address> getAddressDataById(int id){
		 System.out.println("Enter into get getAddressDataById function--"+id);
		 List<Address> addresses = new ArrayList<Address>();
		 try{
			 session = this.sessionFactory.openSession();
	         transaction = session.beginTransaction();
	         Criteria cr = session.createCriteria(Address.class);   
	         cr.add(Restrictions.eq(AddressCNTS.ID,id));
	         addresses =cr.list();
	         transaction.commit();
	         session.flush();
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return addresses;
	}
	 
	 
	 @Transactional
	 public int updateAddress(Address address){
 		int temp;
 		Date date = new Date();
 		Calendar calendar = Calendar.getInstance();
 		calendar.setTime(date);
 		System.out.println("======address.getAddRefCode()"+address.getAddRefCode());
		 try{
			 session = this.sessionFactory.openSession();
			 Criteria cr=session.createCriteria(Address.class);
			 cr.add(Restrictions.eq(AddressCNTS.REF_CODE,address.getAddRefCode()));
			 transaction = session.beginTransaction();
			 address.setCreationTS(calendar);
			 session.update(address);
			 transaction.commit();
			 session.flush();
			 temp= 1;
		 }catch(Exception e){
			 e.printStackTrace();
			 temp= -1;
		 }	
		 session.clear();
		 session.close();
		 return temp;
	 }
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<Address> getAddressData(String custCode){
		 System.out.println("Enter into getAddressData function-----"+custCode);
		 List<Address> addresses = new ArrayList<Address>();
		 try{
			 session = this.sessionFactory.openSession();
	         transaction = session.beginTransaction();
	         Criteria cr = session.createCriteria(Address.class);   
	         cr.add(Restrictions.eq(AddressCNTS.REF_CODE,custCode));
	         addresses =cr.list();
	         transaction.commit();
	         session.flush();
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return addresses;
	}
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<Address> getRTOAddress(){
		 System.out.println("enter into getRTOAddress function");
		 List<Address> addList = new ArrayList<Address>();
		 try{
			 session = this.sessionFactory.openSession();
	         transaction = session.beginTransaction();
	         Criteria cr = session.createCriteria(Address.class);   
	         cr.add(Restrictions.eq(AddressCNTS.TYPE,ConstantsValues.RTO_ADD));
	         addList = cr.list();
	         session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return addList;
	 }
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<Address> getAddByType(String addType){
		 System.out.println("enter into getAddByType function");
		 List<Address> addList = new ArrayList<Address>();
		 try{
			 session = this.sessionFactory.openSession();
	         transaction = session.beginTransaction();
	         Criteria cr = session.createCriteria(Address.class);   
	         cr.add(Restrictions.eq(AddressCNTS.TYPE,addType));
	         addList = cr.list();
	         session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return addList;
	 }
	 
	 @Override
	 public int saveAddress(Address address,Session session){
		 logger.info("Enter into saveAddress() : Address = "+address);
		 int temp = 0;
			 int id = (Integer) session.save(address);
			 temp =  id;
		 logger.info("Exit from saveAddress()");
		 return temp;
	 }
	 
	 @Override
	 public void updateAddress(Address address,Session session){
		 logger.info("Enter into saveAddress() : Address = "+address);
			  session.saveOrUpdate(address);
		 logger.info("Exit from updateAddress()");
	 }
	 
	 
	 @Override
	 public List<Address> getAddByTypeRef(Session session,String addType, String refCode){
		
		 System.out.println("enter into getAddByTypeRef function");
		 List<Address> addList = new ArrayList<Address>();
		 
		  Criteria cr = session.createCriteria(Address.class);   
	         cr.add(Restrictions.eq(AddressCNTS.TYPE,addType));
	         cr.add(Restrictions.eq(AddressCNTS.REF_CODE,refCode));
	         addList = cr.list();
		 
		 return addList;
	 }
	 
	 
}

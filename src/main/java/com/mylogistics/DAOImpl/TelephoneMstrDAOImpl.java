package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.TelephoneMstrDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.EmployeeCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.SubTelephoneMstr;
import com.mylogistics.model.TelephoneMstr;

public class TelephoneMstrDAOImpl implements TelephoneMstrDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public TelephoneMstrDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}	
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public int saveTelephoneMstr(Branch branch , Employee employee , TelephoneMstr telephoneMstr){
		System.out.println("enter into saveTelephoneMstr function");
		 int temp;
		 int brId = branch.getBranchId();
		 int empId = employee.getEmpId();
		 List<Branch> brList = new ArrayList<Branch>();
		 List<Employee> empList = new ArrayList<Employee>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			
			 Criteria cr=session.createCriteria(Branch.class);
			 cr.add(Restrictions.eq(BranchCNTS.BRANCH_ID,brId));
			 brList = cr.list();
			 Branch actBranch = brList.get(0);
			 if(!brList.isEmpty()){
				 telephoneMstr.setBranch(actBranch);
			 }
			 
			 Criteria cr1=session.createCriteria(Employee.class);
			 cr1.add(Restrictions.eq(EmployeeCNTS.EMP_ID,empId));
			 empList = cr1.list();
			 Employee actEmployee = empList.get(0);
			 if(!empList.isEmpty()){
				 telephoneMstr.setEmployee(actEmployee);
			 }
			 
			 actBranch.getTelephoneMstrList().add(telephoneMstr);
			 actEmployee.getTelephoneMstr().add(telephoneMstr);
			 
			 session.update(actBranch);
			 session.update(actEmployee);
			 
			 transaction.commit();
			 session.flush();
			 temp = 1;
		 }catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<TelephoneMstr> getAllTelMstr(){
		System.out.println("enter into getAllTelMstr function");
		List<TelephoneMstr> telMList = new ArrayList<TelephoneMstr>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(TelephoneMstr.class);
			telMList = cr.list();
			/*if(!telMList.isEmpty()){
				for(int i=0;i<telMList.size();i++){
					Hibernate.initialize(telMList.get(i).getEmployee());
				}
			}*/
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return telMList;
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public int updateTMBySTM(int tmId , SubTelephoneMstr subTelephoneMstr){
		System.out.println("enter into updateTMBySTM function");
		List<TelephoneMstr> tmList = new ArrayList<TelephoneMstr>();
		int temp;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(TelephoneMstr.class);
			cr.add(Restrictions.eq("tmId",tmId));
			tmList = cr.list();
			
			TelephoneMstr telephoneMstr = tmList.get(0);
			subTelephoneMstr.setTelephoneMstr(telephoneMstr);
			telephoneMstr.getSubTelephoneMstrList().add(subTelephoneMstr);
			
			session.update(telephoneMstr);
			
			temp = 1;
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public int updateTMBySTM(Session session, int tmId , SubTelephoneMstr subTelephoneMstr){
		System.out.println("enter into updateTMBySTM function");
		List<TelephoneMstr> tmList = new ArrayList<TelephoneMstr>();
		int temp;
//		try{
			Criteria cr=session.createCriteria(TelephoneMstr.class);
			cr.add(Restrictions.eq("tmId",tmId));
			tmList = cr.list();
			
			TelephoneMstr telephoneMstr = tmList.get(0);
			subTelephoneMstr.setTelephoneMstr(telephoneMstr);
			telephoneMstr.getSubTelephoneMstrList().add(subTelephoneMstr);
			
			session.update(telephoneMstr);
			
			temp = 1;
//		}catch(Exception e){
//			e.printStackTrace();
//			temp = -1;
//		}
		return temp;
	}

	
	
}

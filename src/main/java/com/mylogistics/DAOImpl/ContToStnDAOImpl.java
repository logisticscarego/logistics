package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.xmlbeans.impl.xb.xsdschema.Attribute.Use;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.ContToStnDAO;
import com.mylogistics.constants.CnmtCNTS;
import com.mylogistics.constants.ContToStnCNTS;
import com.mylogistics.constants.DailyContractCNTS;
import com.mylogistics.constants.RegularContractCNTS;
import com.mylogistics.model.BillDetail;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.Station;
import com.mylogistics.model.User;
import com.mylogistics.model.dataintegration.ContToStnDemo;
import com.mylogistics.services.CodePatternService;

public class ContToStnDAOImpl implements ContToStnDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;

	@Autowired
	private HttpSession httSession;
	
	@Autowired
	public ContToStnDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public int saveContToStn(ContToStn contToStn){
		int temp;
		System.out.println("Enter into saveContToStn function");
		User currentUser = (User)httSession.getAttribute("currentUser");
		contToStn.setUserCode(currentUser.getUserCode());
		contToStn.setbCode(currentUser.getUserBranchCode());
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.save(contToStn);
			transaction.commit();
			System.out.println("contToStn Data is inserted successfully");
			session.flush();
			temp = 1;
		}catch(Exception e){
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}


	@Transactional
	public List<ContToStn> getContToStnRateW(String contCode , String stnCode , String vehicleType){
		System.out.println("Enter into getContToStnRate function");
		List<ContToStn> ContToStnList = new ArrayList<ContToStn>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(ContToStn.class);
			cr.add(Restrictions.eq("ctsContCode",contCode));
			cr.add(Restrictions.eq("ctsToStn",stnCode));
			cr.add(Restrictions.eq("ctsVehicleType",vehicleType));
			
			ContToStnList = cr.list();
			//rate = contToStn.getCtsRate();
			transaction.commit();
			session.flush();

		}catch(Exception e){
			e.printStackTrace();

		}
		session.clear();
		session.close();
		return ContToStnList;
	}
	
	@Transactional
	public List<ContToStn> getContToStnRateCnmtW(String contCode , String stnCode , String vehicleType , Date cnmtDt){
		System.out.println("Enter into getContToStnRate function");
		System.out.println("cnmtDt in getContToStnRate Function="+cnmtDt);
		List<ContToStn> ContToStnList = new ArrayList<ContToStn>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(ContToStn.class);
			cr.add(Restrictions.eq(ContToStnCNTS.CTS_CONT_CODE,contCode));//"ctsContCode"
			cr.add(Restrictions.eq(ContToStnCNTS.CONT_TO_STN,stnCode));//"ctsToStn"
			cr.add(Restrictions.eq(ContToStnCNTS.CTS_VEHICLE_TYPE,vehicleType));//"ctsVehicleType"
			cr.add(Restrictions.le(ContToStnCNTS.CTS_FR_DT, cnmtDt));//"ctsFrDt"
			cr.add(Restrictions.ge(ContToStnCNTS.CTS_TO_DT, cnmtDt));//"ctsToDt"
			
			ContToStnList = cr.list();
			//rate = contToStn.getCtsRate();
			transaction.commit();
			session.flush();

		}catch(Exception e){
			e.printStackTrace();

		}
		session.clear();
		session.close();
		return ContToStnList;
	}


	@Transactional
	public List<ContToStn> getContToStnRateQ(String contCode , String stnCode , String productType ,String vehicleType , Date cnmtDt){
		System.out.println("Enter into getContToStnRate function");
		List<ContToStn> ContToStnList = new ArrayList<ContToStn>();
		double rate = 0.0;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(ContToStn.class);
			cr.add(Restrictions.eq("ctsContCode",contCode));
			cr.add(Restrictions.eq("ctsToStn",stnCode));
			cr.add(Restrictions.eq("ctsProductType",productType));
			cr.add(Restrictions.eq(ContToStnCNTS.CTS_VEHICLE_TYPE,vehicleType));//"ctsVehicleType"
			cr.add(Restrictions.le(ContToStnCNTS.CTS_FR_DT, cnmtDt));//"ctsFrDt"
			cr.add(Restrictions.ge(ContToStnCNTS.CTS_TO_DT, cnmtDt));//"ctsToDt"
			ContToStnList = cr.list();
			if(!ContToStnList.isEmpty()){
				rate = ContToStnList.get(0).getCtsRate();
			}
			/*ContToStn contToStn = (ContToStn) cr.list();
			 rate = contToStn.getCtsRate();*/
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();

		}
		session.clear();
		session.close();
		return ContToStnList;
	}

	@Transactional
	public List<ContToStn> getContToStnRateCnmtQ(String contCode , String stnCode , String productType , Date cnmtDt){
		System.out.println("Enter into getContToStnRate function");
		List<ContToStn> ContToStnList = new ArrayList<ContToStn>();
		double rate = 0.0;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(ContToStn.class);
			cr.add(Restrictions.eq(ContToStnCNTS.CTS_CONT_CODE,contCode));//"ctsContCode"
			cr.add(Restrictions.eq(ContToStnCNTS.CONT_TO_STN,stnCode));//"ctsToStn"
			cr.add(Restrictions.eq(ContToStnCNTS.CTS_PRODUCT_TYPE,productType));//"ctsProductType"
			cr.add(Restrictions.le(ContToStnCNTS.CTS_FR_DT, cnmtDt));//"ctsFrDt"
			cr.add(Restrictions.ge(ContToStnCNTS.CTS_TO_DT, cnmtDt));//"ctsToDt"
			ContToStnList = cr.list();
			if(!ContToStnList.isEmpty()){
				rate = ContToStnList.get(0).getCtsRate();
			}
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();

		}
		session.clear();
		session.close();
		return ContToStnList;
	}

	@SuppressWarnings("unchecked")
	public List<ContToStn> getContToStn(String code){
		List<ContToStn> contToStns = new ArrayList<ContToStn>();
		System.out.println("Enter into ContToStn function--- "+code);
		try{
			session = this.sessionFactory.openSession();
			Criteria cr = session.createCriteria(ContToStn.class);
			cr.add(Restrictions.eq(ContToStnCNTS.CTS_CONT_CODE,code));
			contToStns =cr.list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return contToStns;
	}

	
	@Override
	public List<ContToStn> getContToStn(String code,Date sqlDate){
		List<ContToStn> contToStns = new ArrayList<ContToStn>();
		System.out.println("Enter into ContToStn function--- "+code);
		try{
			
			session = this.sessionFactory.openSession();
			Criteria cr = session.createCriteria(ContToStn.class);
			cr.add(Restrictions.eq(ContToStnCNTS.CTS_CONT_CODE,code));
			cr.add(Restrictions.ge(ContToStnCNTS.CTS_TO_DT, sqlDate));
			contToStns =cr.list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return contToStns;
	}
	
	
	@Transactional
	public ContToStn deleteCTS(String code){
		System.out.println("code in dao is-----------------------"+code);

		ContToStn contToStn = new ContToStn();

		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(ContToStn.class);
			cr.add(Restrictions.eq(ContToStnCNTS.CTS_CONT_CODE,code));

			contToStn = (ContToStn) cr.list().get(0);
			session.delete(contToStn);
			transaction.commit();
			session.flush();

			System.out.println("contToStn Data is deleted successfully");	 
		}catch (Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return contToStn;
	}

	@Transactional
	public List<ContToStn> getContForView(String contCode , String stnCode){
		System.out.println("Enter into getDlyContForView function");
		List<ContToStn> ContToStnList = new ArrayList<ContToStn>();
		String station = null;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(ContToStn.class);
			cr.add(Restrictions.eq(ContToStnCNTS.CTS_CONT_CODE,contCode));
			cr.add(Restrictions.eq(ContToStnCNTS.CONT_TO_STN,stnCode));
			ContToStnList = cr.list();
			/*if(!ContToStnList.isEmpty()){
				 station = ContToStnList.get(0).getCtsToStn();
			 }*/
			/*ContToStn contToStn = (ContToStn) cr.list();
			 rate = contToStn.getCtsRate();*/
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();

		}
		session.clear();
		session.close();
		return ContToStnList;
	}

	@Override
	@Transactional
	public List<String> getUnregStnList() {
		System.out.println("Enter into getUnregStnList function");
		List<String> unregStnList = new ArrayList<>();
		List<ContToStnDemo> contToStnDemoList = new ArrayList<>();
		List<Station> stationList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr = session.createCriteria(ContToStnDemo.class);
			contToStnDemoList = cr.list();
			
			cr = session.createCriteria(Station.class);
			stationList = cr.list();
			
			boolean stationListFlag = true;
			
			for (ContToStnDemo contToStnDemo : contToStnDemoList) {
				for (Station station : stationList) {
					if (station.getStnName().equalsIgnoreCase(contToStnDemo.getCtsStnName())
							&& station.getStnPin().equalsIgnoreCase(contToStnDemo.getCtsStnPinNo())) {
						stationListFlag = true;
						break;
					} else{
						stationListFlag = false;
					}
				}
				if (!stationListFlag) {
					unregStnList.add(contToStnDemo.getCtsStnName());
				}
			}
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();

		}
		session.clear();
		session.close();
		return unregStnList;
	}
	
	
	@SuppressWarnings("unchecked")
	public int chgStnRate(Date renDt , List<Map<String, Object>> chgRateList){
		System.out.println("enter into chgStnRate function");
		int res = 0; 
	
		User currentUser = (User) httSession.getAttribute("currentUser");
		Session session=this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			
			if(!chgRateList.isEmpty()){
				Calendar cal = Calendar.getInstance();
				cal.setTime(renDt);
				cal.add(Calendar.DATE, -1);
				Date ctsToDt = new Date(cal.getTime().getTime());
					System.out.println("ctsToDt="+ctsToDt);
				for(int i=0;i<chgRateList.size();i++){
					int id = Integer.parseInt(String.valueOf(chgRateList.get(i).get("ctsId")));
					System.out.println("id="+id);
					double newRate = Double.parseDouble(String.valueOf(chgRateList.get(i).get("newRate")));
					System.out.println("newRate"+newRate);
					if(id > 0){
						
						ContToStn preCts = (ContToStn) session.get(ContToStn.class,id);
						Date toDt = preCts.getCtsToDt();
						System.out.println("toDt="+toDt);
						preCts.setCtsToDt(ctsToDt);
						
						
						ContToStn newCts = new ContToStn();
						newCts.setbCode(currentUser.getUserBranchCode());
						newCts.setUserCode(currentUser.getUserCode());
						newCts.setCtsAdditionalRate(preCts.getCtsAdditionalRate());
						newCts.setCtsContCode(preCts.getCtsContCode());
						newCts.setCtsFrDt(renDt);
						newCts.setCtsToDt(toDt);
						newCts.setCtsFromWt(preCts.getCtsFromWt());
						newCts.setCtsProductType(preCts.getCtsProductType());
						newCts.setCtsRate(newRate);
						newCts.setCtsToLoad(preCts.getCtsToLoad());
						newCts.setCtsToStatChg(preCts.getCtsToStatChg());
						newCts.setCtsToStn(preCts.getCtsToStn());
						newCts.setCtsToTransitDay(preCts.getCtsToTransitDay());
						newCts.setCtsToUnLoad(preCts.getCtsToUnLoad());
						newCts.setCtsToWt(preCts.getCtsToWt());
						newCts.setCtsVehicleType(preCts.getCtsVehicleType());
						
						session.update(preCts);
						session.save(newCts);
						System.out.println("successfull..save..");
						
						Criteria regularConCriteria=session.createCriteria(RegularContract.class);
						List<RegularContract> regularConList=regularConCriteria.add(Restrictions.eq(RegularContractCNTS.REG_CONT_CODE,newCts.getCtsContCode())).list();    
						
						String frStation = null;
						if (!regularConList.isEmpty()) {
							frStation=regularConList.get(0).getRegContFromStation();
						}
						System.out.println("frDate=" +newCts.getCtsFrDt());
						System.out.println("toDate=" +newCts.getCtsToDt());
						System.out.println("contractCode="+ newCts.getCtsContCode());
						System.out.println("frStation="+frStation);
						System.out.println("toStation="+newCts.getCtsToStn());
						System.out.println("vehicleType="+newCts.getCtsVehicleType());
						
						Criteria cnmtCriteria = session.createCriteria(Cnmt.class);
						cnmtCriteria.add(Restrictions.between(CnmtCNTS.CNMT_DT, newCts.getCtsFrDt(), newCts.getCtsToDt()));
						cnmtCriteria.add(Restrictions.eq(CnmtCNTS.CNMT_FrSt,frStation));
						cnmtCriteria.add(Restrictions.eq(CnmtCNTS.CNMT_TSt,newCts.getCtsToStn()));
						cnmtCriteria.add(Restrictions.eq(CnmtCNTS.CONTRACT_CODE, newCts.getCtsContCode()));
						cnmtCriteria.add(Restrictions.eq(CnmtCNTS.CNMT_VEHICLE_TYPE, newCts.getCtsVehicleType()));//by Kamal(2017-01-24)
						cnmtCriteria.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
						List<Cnmt> cnmtList = cnmtCriteria.list();
						
						System.out.println("cnmtList="+cnmtList.size());
						
						if (!cnmtList.isEmpty()) {
							for (int j = 0; j < cnmtList.size(); j++) {
								Cnmt cnmt = cnmtList.get(j);
								if(regularConList.get(0).getRegContBillBasis().equalsIgnoreCase("fixed")) {
									cnmt.setCnmtRate(newCts.getCtsRate()*1000);
									cnmt.setCnmtFreight(newCts.getCtsRate()	* 1000);
									cnmt.setCnmtTOT(newCts.getCtsRate()	* 1000);
								}else {
									cnmt.setCnmtRate(newCts.getCtsRate());
									cnmt.setCnmtFreight(newCts.getCtsRate()	* cnmt.getCnmtGuaranteeWt());
									cnmt.setCnmtTOT(newCts.getCtsRate()	* cnmt.getCnmtGuaranteeWt());
								}
								
								session.update(cnmt);
							}
						}
					}
				}
				session.flush();
				session.clear();
				transaction.commit();
				res = 1;
			}else{
				System.out.println("chgRateList is empty");
				res = 0;
			}
		}catch(Exception e){
			e.printStackTrace();
			res = -1;
			transaction.rollback();
		}
		session.close();
		return res;
	}
	
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public int chgDlyStnRate(Date renDt , List<Map<String, Object>> chgRateList, String contCode){
		System.out.println("enter into chgDlyStnRate function");
		int res = 0;
		User currentUser = (User) httSession.getAttribute("currentUser");
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			if(!chgRateList.isEmpty()){
				Calendar cal = Calendar.getInstance();
				cal.setTime(renDt);
				cal.add(Calendar.DATE, -1);
				Date ctsToDt = new Date(cal.getTime().getTime());

				List<DailyContract> dlyList = new ArrayList<>();
				Criteria cr=session.createCriteria(DailyContract.class);
				cr.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_CODE,contCode));
				dlyList = cr.list();
				
				if(!dlyList.isEmpty()){
					DailyContract dlyContract = dlyList.get(0);
					
					for(int i=0;i<chgRateList.size();i++){
						int id = Integer.parseInt(String.valueOf(chgRateList.get(i).get("ctsId")));
						double newRate = Double.parseDouble(String.valueOf(chgRateList.get(i).get("newRate")));
						if(id > 0){
							
							ContToStn preCts = (ContToStn) session.get(ContToStn.class,id);
							Date toDt = preCts.getCtsToDt();
							
							preCts.setCtsToDt(ctsToDt);
							
							ContToStn newCts = new ContToStn();
							newCts.setbCode(currentUser.getUserBranchCode());
							newCts.setUserCode(currentUser.getUserCode());
							newCts.setCtsAdditionalRate(preCts.getCtsAdditionalRate());
							newCts.setCtsContCode(preCts.getCtsContCode());
							newCts.setCtsFrDt(renDt);
							//newCts.setCtsToDt(dlyContract.getDlyContEndDt());
							newCts.setCtsToDt(toDt);
							newCts.setCtsFromWt(preCts.getCtsFromWt());
							newCts.setCtsProductType(preCts.getCtsProductType());
							newCts.setCtsRate(newRate);
							newCts.setCtsToLoad(preCts.getCtsToLoad());
							newCts.setCtsToStatChg(preCts.getCtsToStatChg());
							newCts.setCtsToStn(preCts.getCtsToStn());
							newCts.setCtsToTransitDay(preCts.getCtsToTransitDay());
							newCts.setCtsToUnLoad(preCts.getCtsToUnLoad());
							newCts.setCtsToWt(preCts.getCtsToWt());
							newCts.setCtsVehicleType(preCts.getCtsVehicleType());
							
							session.update(preCts);
							session.save(newCts);
							
						}
					}
					
					transaction.commit();
					session.flush();
					res = 1;
				}
			}else{
				System.out.println("chgRateList is empty");
				res = 0;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return res;
	}

	@Override
	@Transactional
	public double getRateFrBbl(Map<String, Object> rateService) {
		System.out.println("ContToStnDAOImpl.getRateFrBbl()");
		
		String contCode = (String) rateService.get("contCode");
		Date dt = CodePatternService.getSqlDate(String.valueOf(rateService.get("dt")));
		int toStnId = (int) rateService.get("toStnId"); 
		
		double rate = 0.0;
		
		try {
			session = this.sessionFactory.openSession();
			Criteria cr = session.createCriteria(ContToStn.class);
			cr.add(Restrictions.eq(ContToStnCNTS.CTS_CONT_CODE, contCode));
			cr.add(Restrictions.eq(ContToStnCNTS.CONT_TO_STN, String.valueOf(toStnId)));
			cr.add(Restrictions.ge(ContToStnCNTS.CTS_TO_DT, dt));
			cr.add(Restrictions.le(ContToStnCNTS.CTS_FR_DT, dt));
			
			System.out.println(contCode);
			System.out.println(toStnId);
			System.out.println(dt);
			
			System.out.println("list size===>>: "+cr.list().isEmpty());
			System.out.println("list size===>>: "+cr.list().size());
			
			if (cr.list() != null && !cr.list().isEmpty()) {
				ContToStn contToStn = (ContToStn) cr.list().get(0);
				rate = contToStn.getCtsRate();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			rate = -1;
		} finally {
			session.clear();
			session.close();
		}
		return rate;
	}

	@Override
	@Transactional
	public int updateToWt(ContToStn contToStn) {
		System.out.println("ContToStnDAOImpl.updateToWt()");
		int temp = 0;
		
		User currentUser = (User) httSession.getAttribute("currentUser");
		
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			ContToStn cts = (ContToStn) session.get(ContToStn.class, contToStn.getCtsId());
			cts.setCtsToWt(contToStn.getCtsToWt());
			
			/*System.out.println("Date : "+contToStn.getExtendDate());
			cts.setCtsToDt(contToStn.getCtsToDt());*/
			cts.setCtsUpdate(true);
			cts.setCtsUpdateUserId(currentUser.getUserId());
			session.merge(cts);
			transaction.commit();
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			temp = -1;
		} finally {
			session.close();
		}
		
		return temp;
	}

}

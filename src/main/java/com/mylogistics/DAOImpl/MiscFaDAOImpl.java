package com.mylogistics.DAOImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.MiscFaDAO;
import com.mylogistics.model.AssestsNLiab;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.MiscFa;
import com.mylogistics.model.ProfitNLoss;
import com.mylogistics.model.dataintegration.AssestsNLiabDemo;
import com.mylogistics.model.dataintegration.MiscFaDemo;
import com.mylogistics.model.dataintegration.ProfitNLossDemo;

public class MiscFaDAOImpl implements MiscFaDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public MiscFaDAOImpl(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	@Override
	public int saveMiscFa() {
		
		List<MiscFaDemo> miscList;
		List<FAParticular> faParticularsList;
		int temp = 0;
		try{
			session = this.sessionFactory.openSession();//fixed
			transaction = session.beginTransaction();//fixed
			
			Criteria cr=session.createCriteria(FAParticular.class);
			
			cr.add(Restrictions.eq("faPerType", "miscfa"));
			faParticularsList = cr.list();
			if (!faParticularsList.isEmpty()) {
				temp = 1;
				String id = String.valueOf(faParticularsList.get(0).getFaPerId());
				
				if (id.length()<2) {
					id="0"+id;
				}
				
				System.out.println("\n\nFAParticular Id: "+id);
				
				cr=session.createCriteria(MiscFaDemo.class);
				miscList  = cr.list();
				for (MiscFaDemo miscFaDemo : miscList) {
					MiscFa miscFa = new MiscFa();
					miscFa.setMiscFaName(miscFaDemo.getMiscFaName());
					miscFa.setMiscFaCode(id+miscFaDemo.getMiscFaCode());
					session.save(miscFa);
				}
			}else {
				System.out.println("no entry for miscFa in faPerticular Table");
				temp = 0;
			}
			transaction.commit();//fixed
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
			temp = 0;
		}
		session.clear();
		session.close();
		return temp;
	}

	@Transactional
	@Override
	public int saveMiscFaMstr(String userCode, String bCode) {

		List<MiscFa> miscList;
		int temp = 0;
		try{
			session = this.sessionFactory.openSession();//fixed
			transaction = session.beginTransaction();//fixed
			
			Criteria cr=session.createCriteria(MiscFa.class);
			
			miscList = cr.list();
			if (!miscList.isEmpty()) {
				temp = 1;
				for (MiscFa miscFa : miscList) {
					System.out.println("miscfa FaCode: "+miscFa.getMiscFaCode()+
							"\t"+"miscfa Name: "+miscFa.getMiscFaName());
					
					FAMaster faMaster = new FAMaster();
					faMaster.setFaMfaCode(miscFa.getMiscFaCode());
					faMaster.setFaMfaName(miscFa.getMiscFaName());
					faMaster.setbCode(bCode);
					faMaster.setUserCode(userCode);
					faMaster.setFaMfaType("miscfa");
					session.save(faMaster);
				}
			} else {
				System.out.println("no entry for miscfa in faMaster Table");
				temp = 0;
			}
			transaction.commit();//fixed
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
			temp = 0;
		}
		session.clear();
		session.close();
		return temp;
	}

}

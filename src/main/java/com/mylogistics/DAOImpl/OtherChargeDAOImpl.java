package com.mylogistics.DAOImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.OtherChargeDAO;
import com.mylogistics.model.Address;
import com.mylogistics.model.OtherCharge;

public class OtherChargeDAOImpl implements OtherChargeDAO{
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public OtherChargeDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	

	 @Transactional
	   public int saveOtChg(OtherCharge otherCharge){
		 System.out.println("enter into saveOtChg function");
		 int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 int id = (Integer) session.save(otherCharge);
			 transaction.commit();
			 temp = (int) id;
			 session.flush();
			 
		 }catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
}

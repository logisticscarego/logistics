package com.mylogistics.DAOImpl.bank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.FAParticularCNTS;
import com.mylogistics.constants.bank.AtmCardMstrCNTS;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.constants.bank.ChequeLeavesCNTS;
import com.mylogistics.model.Address;
import com.mylogistics.model.Branch;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.AtmCardMstr;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.BankName;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.services.CodePatternService;

public class BankMstrDAOImpl implements BankMstrDAO{

	private String TAG = BankMstrDAOImpl.class.getName()+": ";
	
	private SessionFactory sessionFactory;
	
	private static Logger logger = Logger.getLogger(BankMstrDAOImpl.class);
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	public BankMstrDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<String> getBankCodeList(String branchCode){

		List<String> bankCodeList = new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_BRH_ID, Integer.parseInt(branchCode)));
			ProjectionList projectionList=Projections.projectionList();
			projectionList.add(Projections.property(BankMstrCNTS.BNK_FA_CODE));
			criteria.setProjection(projectionList);
			bankCodeList = criteria.list();

			transaction.commit();
			session.flush();

		}catch(Exception e){
			e.printStackTrace();

		}
		session.clear();
		session.close();

		return bankCodeList;

	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Map<String,Object>> getBnkNameAndCode(String branchCode){
		System.out.println("enter into getBnkNameAndCode function");
		List<Map<String,Object>> bnkList = new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		try{
			List<BankMstr> bmList = new ArrayList<>();
			List<Branch> brhList = new ArrayList<>();
			Criteria criteria=session.createCriteria(Branch.class);
			criteria.add(Restrictions.eq(BranchCNTS.BRANCH_ID,Integer.parseInt(branchCode)));
			brhList = criteria.list();
			
			if(!brhList.isEmpty()){
				bmList = brhList.get(0).getBankMstrList();
				if(!bmList.isEmpty()){
					Map<String,Object> map=null;
					for(int i=0;i<bmList.size();i++){
						 map = new HashMap<String, Object>();
						map.put("bnkFaCode",bmList.get(i).getBnkFaCode());
						map.put("bnkName",bmList.get(i).getBnkName());
						
						bnkList.add(map);
					}
					if(map!=null && !brhList.get(0).getBranchCode().equalsIgnoreCase("1")){
						map=new HashMap<String, Object>();
						map.put("bnkFaCode","0700003");
						map.put("bnkName", "HDFC BANK LTD.");
						bnkList.add(map);
						map=new HashMap<String, Object>();
						map.put("bnkFaCode","0700010");
						map.put("bnkName", "KOTAK MAHINDRA BANK LTD.");
						bnkList.add(map);
						map=new HashMap<String, Object>();
						map.put("bnkFaCode","0700023");
						map.put("bnkName", "STATE BANK OF INDIA");
						bnkList.add(map);
					}
					//bnkList.add(map);
				}
			}
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return bnkList;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ChequeLeaves> getChqLByBnk(String bankCode,char CType){
		System.out.println("enter into getChqLByBnk function");
		List<ChequeLeaves> chqList = new ArrayList<>();
		List<ChequeLeaves> actChqList = new ArrayList<>();
		List<BankMstr> bankList = new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		try{
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
			bankList = criteria.list();
					
			if(!bankList.isEmpty()){
				Hibernate.initialize(bankList.get(0).getChequeLeavesList());
				chqList = bankList.get(0).getChequeLeavesList();
			}		
			
			if(!chqList.isEmpty()){
				
				 Collections.sort(chqList,
	                        new Comparator<ChequeLeaves>() {
	                            @Override
	                            public int compare(final ChequeLeaves cl1,
	                                    final ChequeLeaves cl2) {	                            	
	                                return cl1.getChqLChqNo().compareTo(cl2.getChqLChqNo());
	                            }
	              });
				
				
				for(int i=0;i<chqList.size();i++ ){
					if(chqList.get(i).isChqLUsed() == false && chqList.get(i).isChqLCancel() == false &&
							chqList.get(i).getChqLCType() == CType){
						actChqList.add(chqList.get(i));
					}
				}

			}
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		System.out.println("Ac : "+actChqList.size());
		return actChqList;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public ChequeLeaves getChqListByBnkC(String bankCode , char CType){
		System.out.println("enter into getChqListByBnkC funvtion");
		List<ChequeLeaves> chqLeavesList = new ArrayList<>();
		List<BankMstr> bankList = new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		try{
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
			bankList = criteria.list();
			if(!bankList.isEmpty()) {
				Hibernate.initialize(bankList.get(0).getChequeLeavesList());
				chqLeavesList = bankList.get(0).getChequeLeavesList();
			}
			session.flush();

			System.out.println("size of chqLeavesList = "+chqLeavesList.size());
			if(!chqLeavesList.isEmpty()){
				
				 Collections.sort(chqLeavesList,
	                        new Comparator<ChequeLeaves>() {
	                            @Override
	                            public int compare(final ChequeLeaves cl1,
	                                    final ChequeLeaves cl2) {
	                                return cl1.getChqLChqNo().compareTo(
	                                        cl2.getChqLChqNo());
	                            }
	              });
				
				for(int i=0;i<chqLeavesList.size();i++ ){
					if(chqLeavesList.get(i).isChqLUsed() == false && chqLeavesList.get(i).isChqLCancel() == false &&
							chqLeavesList.get(i).getChqLCType() == CType){
						session.clear();
						session.close();
						return chqLeavesList.get(i);
					}
				}

			}
		}catch(Exception e){
			e.printStackTrace();

		}
		session.clear();
		session.close();
		return null;

	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ChequeLeaves> getAPChqLByBnkC(String bankCode,char CType){
		System.out.println("enter into getAPChqLByBnkC function");
		List<ChequeLeaves> chqLeavesList = new ArrayList<>();
		List<ChequeLeaves> actChqL = new ArrayList<>();
		List<BankMstr> bankList = new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		try{
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
			bankList = criteria.list();
			if(!bankList.isEmpty()){
				Hibernate.initialize(bankList.get(0).getChequeLeavesList());
				chqLeavesList = bankList.get(0).getChequeLeavesList();
			}
			if(!chqLeavesList.isEmpty()){
				
				 Collections.sort(chqLeavesList,
	                        new Comparator<ChequeLeaves>() {
	                            @Override
	                            public int compare(final ChequeLeaves cl1,
	                                    final ChequeLeaves cl2) {
	                                return cl1.getChqLChqNo().compareTo(
	                                        cl2.getChqLChqNo());
	                            }
	              });
				
				
				for(int i=0;i<chqLeavesList.size();i++ ){				
					if(chqLeavesList.get(i).isChqLUsed() == false && chqLeavesList.get(i).isChqLCancel() == false &&
							chqLeavesList.get(i).getChqLChqType() == 'A' && chqLeavesList.get(i).getChqLCType() == CType){
							
						actChqL.add(chqLeavesList.get(i));
					}
				}

			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			session.clear();
			session.close();
		}
		session.clear();
		session.close();
		return actChqL;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ChequeLeaves> getAPChqLByBnkC(String bankCode,char CType, String chqNo){
		logger.info("Enter into getAPChqLByBnkC() : bankCode = "+bankCode+" : CType = "+CType+" : chqNo = "+chqNo);
		System.out.println("enter into getAPChqLByBnkC function");
		List<ChequeLeaves> chqLeavesList = new ArrayList<>();
		List<ChequeLeaves> actChqL = new ArrayList<>();
		List<BankMstr> bankList = new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		try{
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
			bankList = criteria.list();
			if(!bankList.isEmpty()){
				logger.info("Bank found : Bank ID = "+bankList.get(0).getBnkId());
				
				chqLeavesList = session.createCriteria(ChequeLeaves.class)
						.createAlias("bankMstr", "bankMstr")
						.add(Restrictions.eq("bankMstr.bnkId", bankList.get(0).getBnkId()))
						.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_USED, false))
						.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_CHQ_CANCEL, false))
						.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_CHQ_TYPE, 'A'))
						.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_C_TYPE, CType))
						.add(Restrictions.like(ChequeLeavesCNTS.CHQ_L_CHQ_NO, "%"+chqNo))
						.list();	
				
				if(chqLeavesList.isEmpty())
					logger.info("Leave cheques not found !");
				else
					logger.info("Leave cheques found :  Size = "+chqLeavesList.size());
			}
			
			session.flush();

			if(!chqLeavesList.isEmpty()){
				
				 Collections.sort(chqLeavesList,
	                        new Comparator<ChequeLeaves>() {
	                            @Override
	                            public int compare(final ChequeLeaves cl1,
	                                    final ChequeLeaves cl2) {
	                                return cl1.getChqLChqNo().compareTo(
	                                        cl2.getChqLChqNo());
	                            }
	              });			

			}			
			actChqL = chqLeavesList;
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return actChqL;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public ChequeLeaves getAccPayChqByBnkC(String bankCode , char CType){
		System.out.println("enter into getAccPayChqByBnkC function");
		List<ChequeLeaves> chqLeavesList = new ArrayList<>();
		List<BankMstr> bankList = new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		try{
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
			bankList = criteria.list();
			if(!bankList.isEmpty()){
				Hibernate.initialize(bankList.get(0).getChequeLeavesList());
				chqLeavesList = bankList.get(0).getChequeLeavesList();
			}
			session.flush();

			if(!chqLeavesList.isEmpty()){
				
				 Collections.sort(chqLeavesList,
	                        new Comparator<ChequeLeaves>() {
	                            @Override
	                            public int compare(final ChequeLeaves cl1,
	                                    final ChequeLeaves cl2) {
	                                return cl1.getChqLChqNo().compareTo(
	                                        cl2.getChqLChqNo());
	                            }
	              });
				
				
				for(int i=0;i<chqLeavesList.size();i++ ){				
					if(chqLeavesList.get(i).isChqLUsed() == false && chqLeavesList.get(i).isChqLCancel() == false &&
							chqLeavesList.get(i).getChqLChqType() == 'A' && chqLeavesList.get(i).getChqLCType() == CType){
							session.clear();
							session.close();
							return chqLeavesList.get(i);
					}
				}

			}
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return null;

	}

	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<BankMstr> getBankByBankCode(String bankCode){
		System.out.println("enter into getBankByBankCode function = "+bankCode);
		List<BankMstr> bankMstrList = new ArrayList<BankMstr>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			Criteria cr=session.createCriteria(BankMstr.class);
			cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
			bankMstrList = cr.list();
			transaction.commit();
			session.flush();

		}catch(Exception e){
			e.printStackTrace();

		}
		session.clear();
		session.close();
		return bankMstrList;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<BankName> getBankNameList() {
		System.out.println("enter into getBankNameList function = ");
		List<BankName> bankNames = new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			Criteria cr = session.createCriteria(BankName.class);
			bankNames = cr.list();
			transaction.commit();
			session.flush();

		}catch(Exception e){
			e.printStackTrace();

		}
		session.clear();
		session.close();
		return bankNames;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public ChequeLeaves getChqListByBnkC(String bankCode){

		List<ChequeLeaves> chqLeavesList = new ArrayList<>();
		List<BankMstr> bankList = new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		try{
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
			bankList = criteria.list();
			if(!bankList.isEmpty()){
				Hibernate.initialize(bankList.get(0).getChequeLeavesList());
				chqLeavesList = bankList.get(0).getChequeLeavesList();
			}
			session.flush();

			System.out.println("size of chqLeavesList = "+chqLeavesList.size());
			if(!chqLeavesList.isEmpty()){
				
				 Collections.sort(chqLeavesList,
	                        new Comparator<ChequeLeaves>() {
	                            @Override
	                            public int compare(final ChequeLeaves cl1,
	                                    final ChequeLeaves cl2) {
	                                return cl1.getChqLChqNo().compareTo(
	                                        cl2.getChqLChqNo());
	                            }
	              });
				
				for(int i=0;i<chqLeavesList.size();i++ ){
					if(chqLeavesList.get(i).isChqLUsed() == false && chqLeavesList.get(i).isChqLCancel() == false &&
							chqLeavesList.get(i).getChqLChqType() == 'B'){
						session.clear();
						session.close();
						return chqLeavesList.get(i);
					}
				}

			}
		}catch(Exception e){
			e.printStackTrace();

		}
		session.clear();
		session.close();
		return null;

	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public String saveBankMstr(BankMstr bankMstr) {
		System.out.println("enter into saveBankMstr function->>>>>");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		String bankFaCode = null;
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			int id = (Integer) session.save(bankMstr);
			
			Criteria criteria=session.createCriteria(FAParticular.class);
			criteria.add(Restrictions.eq(FAParticularCNTS.FA_PAR_TYPE, "bank"));
			List<FAParticular> faParticularlList = criteria.list(); 
			int faPerId = faParticularlList.get(0).getFaPerId();
			
			criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_ID, id));
			List<BankMstr> bankMstrList = criteria.list();
			BankMstr bMstr = bankMstrList.get(0);
			Address address = bMstr.getAddress();
			bankFaCode = CodePatternService.generateFaCode(id, faPerId);
			bMstr.setBnkFaCode(bankFaCode);
			address.setAddRefCode(bankFaCode);
			session.saveOrUpdate(bMstr);
			
			FAMaster faMaster = new FAMaster();
			faMaster.setbCode(currentUser.getUserBranchCode());
			faMaster.setFaMfaCode(bankFaCode);
			faMaster.setFaMfaName(bMstr.getBnkName());
			faMaster.setFaMfaType("bank");
			faMaster.setUserCode(currentUser.getUserCode());
			
			session.save(faMaster);
			
			
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			bankFaCode = null;
		}
		session.clear();
		session.close();
		return bankFaCode;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<BankMstr> getUnAssignBank(){
		System.out.println(TAG+"enter into getUnAssignBank()");
		List<BankMstr> bankMstrList = new ArrayList<BankMstr>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			Criteria cr=session.createCriteria(BankMstr.class);
			cr.add(Restrictions.eq(BankMstrCNTS.BNK_ALLOTED, false));
			bankMstrList = cr.list();
			transaction.commit();
			session.flush();

		}catch(Exception e){
			e.printStackTrace();

		}
		session.clear();
		session.close();
		return bankMstrList;
	}

	@Transactional
	@Override
	public int saveBankMstr(BankMstr bMstr, Branch branch) {
		
		System.out.println("enter into saveBankMstr function->>>>>");
		int temp = 0;
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			//get branch
			Branch updatedBranch = (Branch) session.get(Branch.class, branch.getBranchId());
			
			//get bank
			BankMstr bankMstr = (BankMstr) session.get(BankMstr.class, bMstr.getBnkId());
			
			bankMstr.setBranch(updatedBranch);
			bankMstr.setBnkAlloted(true);
			updatedBranch.getBankMstrList().add(bankMstr);
			session.update(updatedBranch);
			transaction.commit();
			session.flush();
			temp= 1;
		}catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}
		session.clear();
		session.close();
		return temp;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<BankMstr> getBankMstr() {
		System.out.println(TAG+"enter into getBankMstr()");
		List<BankMstr> bankMstrList = new ArrayList<BankMstr>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			Criteria cr=session.createCriteria(BankMstr.class);
			bankMstrList = cr.list();
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return bankMstrList;
	}

	@Transactional
	@Override
	public int dissBMFromBranch(BankMstr bankMstr, Branch branch) {
		System.out.println("enter into dissBMFromBranch function->>>>>");
		int temp = -1;
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			//remove bankMstr from list
			Criteria criteria=session.createCriteria(Branch.class);
			criteria.add(Restrictions.eq(BranchCNTS.BRANCH_ID, branch.getBranchId()));
			Branch br = (Branch)criteria.list().get(0);
			
			for (int i = 0; i < br.getBankMstrList().size(); i++) {
				if (br.getBankMstrList().get(i).getBnkId() == bankMstr.getBnkId()) {
					System.out.println("remove bmstr: "+br.getBankMstrList().get(i).getBnkId());
					br.getBankMstrList().remove(i);
					
				}
			}
			session.save(br);
			
			//remove branch from bankMstr
			criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_ID, bankMstr.getBnkId()));
			BankMstr bMstr  = (BankMstr) criteria.list().get(0);
			bMstr.setBnkAlloted(false);
			bMstr.setBranch(null);
			session.save(bMstr);
			
			transaction.commit();
			temp= 1;
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}
		session.clear();
		session.close();
		return temp;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<BankMstr> getAssignedBank() {
		System.out.println(TAG+"enter into getAssignedBank()");
		List<BankMstr> bankMstrList = new ArrayList<BankMstr>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			Criteria cr=session.createCriteria(BankMstr.class);
			cr.add(Restrictions.eq(BankMstrCNTS.BNK_ALLOTED, true));
			bankMstrList = cr.list();
			transaction.commit();
			session.flush();

		}catch(Exception e){
			e.printStackTrace();

		}
		session.clear();
		session.close();
		return bankMstrList;
	}

	@Transactional
	@Override
	public int dissBankMstr(BankMstr bankMstr) {
		
		System.out.println("enter into dissBankMstr function->>>>>");
		int temp = -1;
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			//get bank
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_ID, bankMstr.getBnkId()));
			BankMstr bMstr  = (BankMstr) criteria.list().get(0);
			
			//get branch
			criteria=session.createCriteria(Branch.class);
			criteria.add(Restrictions.eq(BranchCNTS.BRANCH_ID, bMstr.getBranch().getBranchId()));
			Branch br = (Branch)criteria.list().get(0);
			
			//remove bank master from branch list
			for (int i = 0; i < br.getBankMstrList().size(); i++) {
				if (br.getBankMstrList().get(i).getBnkId() == bankMstr.getBnkId()) {
					System.out.println("remove bmstr: "+br.getBankMstrList().get(i).getBnkId());
					br.getBankMstrList().remove(i);
				}
			}
			session.save(br);

			bMstr.setBnkAlloted(false);
			bMstr.setBranch(null);
			session.save(bMstr);
			
			transaction.commit();
			temp= 1;
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}
		session.clear();
		session.close();
		return temp;
	}

	
	@SuppressWarnings("unchecked")
	@Transactional
	public int decBankBal(int bnkId, double amt){
		System.out.println("enter into decBankBal function  --- "+amt);
		int res = 0;
		List<BankMstr> bnkList = new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_ID, bnkId));
			bnkList = criteria.list();
			if(!bnkList.isEmpty()){
				BankMstr bank = bnkList.get(0);
				double balAmt = bank.getBnkBalanceAmt();
				double newBal = balAmt - amt;
				bank.setBnkBalanceAmt(newBal);
				session.merge(bank);
				transaction.commit();
				res = 1;
			}
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return res;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int incBnkBal(int bnkId, double amt){
		System.out.println("enter into incBnkBal function  --- "+amt);
		int res = 0;
		List<BankMstr> bnkList = new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_ID, bnkId));
			bnkList = criteria.list();
			if(!bnkList.isEmpty()){
				BankMstr bank = bnkList.get(0);
				double balAmt = bank.getBnkBalanceAmt();
				double newBal = balAmt + amt;
				bank.setBnkBalanceAmt(newBal);
				session.merge(bank);
				transaction.commit();
				res = 1;
			}
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return res;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public BankMstr getBankByAtm(String atmCard){
		System.out.println("enter into getBankByAtm function");
		BankMstr bankMstr = null;
		List<AtmCardMstr> atmList =  new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		try{
			Criteria criteria=session.createCriteria(AtmCardMstr.class);
			criteria.add(Restrictions.eq(AtmCardMstrCNTS.ATM_CARD_NO,atmCard));
			atmList = criteria.list();
			
			if(!atmList.isEmpty()){
				bankMstr = atmList.get(0).getBankMstr();
			}
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return bankMstr;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public String getBankName(String bankFaCode){
		System.out.println("enter into getBankName function");
		List<BankMstr> bnkList = new ArrayList<>();
		String bnkName = null;
		Session session = this.sessionFactory.openSession();
		try{
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE,bankFaCode));
			bnkList = criteria.list();
			if(!bnkList.isEmpty()){
				bnkName = bnkList.get(0).getBnkName() ;
			}
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return bnkName;
	}

	
	@Override
	public String getBankName(String bankFaCode,Session session){
		System.out.println("enter into getBankName function");
		List<BankMstr> bnkList = new ArrayList<>();
		String bnkName = null;
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE,bankFaCode));
			bnkList = criteria.list();
			if(!bnkList.isEmpty()){
				bnkName = bnkList.get(0).getBnkName() ;
			}
		return bnkName;
	}

	
	
	
	@Override
	public List<ChequeLeaves> getChqLByBnkId(int bnkId) {
		
		List<ChequeLeaves> chequeLeaveList = new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		try {
			BankMstr bankMstr = (BankMstr) session.get(BankMstr.class, bnkId);
			chequeLeaveList = bankMstr.getChequeLeavesList();
			Hibernate.initialize(bankMstr.getChequeLeavesList());
			chequeLeaveList = bankMstr.getChequeLeavesList();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		session.clear();
		session.close();
		return chequeLeaveList;
	}

	@Override
	@Transactional
	public List<Map<String, Object>> getBankNCAIList(String branchId) {

		List<Map<String, Object>> bankList = new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		try {
			Branch branch = (Branch) session.get(Branch.class, Integer.parseInt(branchId));
			
			List<BankMstr> bankMstrList = branch.getBankMstrList();
			
			System.out.println("bankMstr List size: "+bankMstrList.size());
			
			for (BankMstr bankMstr : bankMstrList) {
				Map<String, Object> map = new HashMap<>();
				map.put(BankMstrCNTS.BNK_ID, bankMstr.getBnkId());
				map.put(BankMstrCNTS.BNK_AC_NO, bankMstr.getBnkAcNo());
				map.put(BankMstrCNTS.BNK_FA_CODE, bankMstr.getBnkFaCode());
				map.put(BankMstrCNTS.BNK_NAME, bankMstr.getBnkName());
				bankList.add(map);
			}
			
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		session.clear();
		session.close();
		
		return bankList;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public ChequeLeaves getChqListByBnkCRvrs(String bankCode , char CType){
		System.out.println("enter into getChqListByBnkC funvtion");
		List<ChequeLeaves> chqLeavesList = new ArrayList<>();
		List<BankMstr> bankList = new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		try{
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
			bankList = criteria.list();
			if(!bankList.isEmpty()){
				Hibernate.initialize(bankList.get(0).getChequeLeavesList());
				chqLeavesList = bankList.get(0).getChequeLeavesList();
			}
			session.flush();

			System.out.println("size of chqLeavesList = "+chqLeavesList.size());
			if(!chqLeavesList.isEmpty()){
				
				 Collections.sort(chqLeavesList,
	                        new Comparator<ChequeLeaves>() {
	                            @Override
	                            public int compare(final ChequeLeaves cl1,
	                                    final ChequeLeaves cl2) {
	                                return cl1.getChqLChqNo().compareTo(
	                                        cl2.getChqLChqNo());
	                            }
	              });
				
				
				for(int i=0;i<chqLeavesList.size();i++ ){
					if(chqLeavesList.get(i).isChqLUsed() == true && chqLeavesList.get(i).isChqLCancel() == false &&
							chqLeavesList.get(i).getChqLCType() == CType){
						session.clear();
						session.close();
						return chqLeavesList.get(i);
					}
				}

			}
		}catch(Exception e){
			e.printStackTrace();

		}
		session.clear();
		session.close();
		return null;

	}
	
	//@Transactional
	@Override
	public List<BankMstr> getBankByBankCodeN(Session session, String bankCode){
		System.out.println("enter into getBankByBankCode function = "+bankCode);
		List<BankMstr> bankMstrList = new ArrayList<BankMstr>();
		//try{
			Criteria cr=session.createCriteria(BankMstr.class);
			cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
			bankMstrList = cr.list();
//		}catch(Exception e){
//			e.printStackTrace();
//		}
		return bankMstrList;
	}
	
	public BankMstr getBankMstr(int bankMstrId) {
		return (BankMstr) this.sessionFactory.getCurrentSession().get(BankMstr.class, bankMstrId);
	}
	
	public void update(BankMstr bankMstr) {
		this.sessionFactory.getCurrentSession().update(bankMstr);
	}
	
	
	@Override
	public List<BankMstr> getBankByBankCode(Session session, String bankCode){
		System.out.println("enter into getBankByBankCode function = "+bankCode);
		List<BankMstr> bankMstrList = new ArrayList<BankMstr>();
	//	try{
			Criteria cr=session.createCriteria(BankMstr.class);
			cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
			bankMstrList = cr.list();
//		}catch(Exception e){
//			e.printStackTrace();
//		}
		return bankMstrList;
	}
	
	@SuppressWarnings("unchecked")
//	@Transactional
	public int decBankBal(Session session, int bnkId, double amt){
		System.out.println("enter into decBankBal function  --- "+amt);
		int res = 0;
		List<BankMstr> bnkList = new ArrayList<>();
	//	try{
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_ID, bnkId));
			bnkList = criteria.list();
			if(!bnkList.isEmpty()){
				BankMstr bank = bnkList.get(0);
				double balAmt = bank.getBnkBalanceAmt();
				double newBal = balAmt - amt;
				bank.setBnkBalanceAmt(newBal);
				session.merge(bank);
				res = 1;
			}
//		}catch(Exception e){
//			e.printStackTrace();
//		}
		return res;
	}
	
}
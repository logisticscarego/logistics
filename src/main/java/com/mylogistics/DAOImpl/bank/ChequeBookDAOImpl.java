package com.mylogistics.DAOImpl.bank;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.bank.ChequeBookDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.constants.bank.ChequeBookCNTS;
import com.mylogistics.constants.bank.ChequeRequestCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeBook;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.model.bank.ChequeRequest;
import com.mylogistics.services.CodePatternService;

public class ChequeBookDAOImpl implements ChequeBookDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public ChequeBookDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	@Transactional
	public String saveChequeRequest(ChequeRequest chequeRequest) {
		
		String str = null;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			int id = (Integer) session.save(chequeRequest);
			str = CodePatternService.get7DigitCode(id);
			chequeRequest.setcReqRefNo(str);
			session.update(chequeRequest);
			
			//get bank from cheque request
			//get bank
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_ID, chequeRequest.getBankMstr().getBnkId()));
			BankMstr bankMstr  = (BankMstr) criteria.list().get(0);
			
			bankMstr.getChequeRequestList().add(chequeRequest);
			session.update(bankMstr);
			
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			str = null;
		}
		session.clear();
		session.close();
		return str;
	}

	@Override
	@Transactional
	public List<ChequeRequest> getChqReqList(BankMstr bankMstr) {
		
		List<ChequeRequest> chequeRequestList = new ArrayList<>();
		
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_ID, bankMstr.getBnkId()));
			BankMstr bMstr  = (BankMstr) criteria.list().get(0);
			
			Hibernate.initialize(bMstr.getChequeRequestList());
			chequeRequestList = bMstr.getChequeRequestList();
			
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		
		return chequeRequestList;
	}

	//cheque book received at head office but not issued to branch till now
	@Override
	@Transactional
	public int receiveChequeBook(List<ChequeBook> chequeBookList,
			ChequeRequest chequeRequest, BankMstr bankMstr, User user) {
		
		int temp = -1;
		
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_ID, bankMstr.getBnkId()));
			BankMstr bMstr  = (BankMstr) criteria.list().get(0);
			
			/*criteria=session.createCriteria(Branch.class);
			criteria.add(Restrictions.eq(BranchCNTS.BRANCH_ID, bMstr.getBranch().getBranchId()));
			Branch branch  = (Branch) criteria.list().get(0);*/
			
			criteria=session.createCriteria(ChequeRequest.class);
			criteria.add(Restrictions.eq(ChequeRequestCNTS.C_REQ_ID, chequeRequest.getcReqId()));
			ChequeRequest chqReq  = (ChequeRequest) criteria.list().get(0);
			
			for (ChequeBook chequeBook : chequeBookList) {
				chequeBook.setbCode(user.getUserBranchCode());
				chequeBook.setUserCode(user.getUserCode());
				chequeBook.setChequeRequest(chequeRequest);
				chequeBook.setBankMstr(bMstr);
				//chequeBook.setBranch(branch);
				chequeBook.setChqBkFromChqNo(CodePatternService.get10DigitCode(Long.parseLong(chequeBook.getChqBkFromChqNo())));
				chequeBook.setChqBkToChqNo(CodePatternService.get10DigitCode(Long.parseLong(chequeBook.getChqBkToChqNo())));
				bMstr.getChequeBookList().add(chequeBook);
				//branch.getChequeBookList().add(chequeBook);
				session.update(bMstr);
				//session.update(branch);
			}
			
			chqReq.setcReqIsReceived(true);
			session.update(chqReq);
			
			transaction.commit();
			session.flush();
			temp = 1;
		}catch(Exception e){
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		
		return temp;
	}

	@Override
	@Transactional
	public List<ChequeBook> getChqBook(BankMstr bankMstr) {
		
		List<ChequeBook> chequeBookList = new ArrayList<>();
		
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_ID, bankMstr.getBnkId()));
			BankMstr bMstr  = (BankMstr) criteria.list().get(0);
			
			Hibernate.initialize(bMstr.getChequeBookList());
			chequeBookList = bMstr.getChequeBookList();
			
			transaction.commit();
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		
		return chequeBookList;
	}

	@Override
	@Transactional
	public int issueChequeBook(List<ChequeBook> chequeBookList,	Employee authEmp1, Employee authEmp2) {
		
		int temp = -1;
		java.sql.Date sqlDate = new java.sql.Date(new Date().getTime());
		
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			for (ChequeBook chequeBook : chequeBookList) {
				Criteria criteria=session.createCriteria(ChequeBook.class);
				criteria.add(Restrictions.eq(ChequeBookCNTS.CHQ_BK_ID, chequeBook.getChqBkId()));
				ChequeBook chqBook  = (ChequeBook) criteria.list().get(0);
				if (Long.parseLong(chequeBook.getChqBkToChqNo())<Long.parseLong(chqBook.getChqBkToChqNo())) {
					
					ChequeBook chqBookClone = new ChequeBook();
					
					criteria=session.createCriteria(BankMstr.class);
					criteria.add(Restrictions.eq(BankMstrCNTS.BNK_ID, chqBook.getBankMstr().getBnkId()));
					BankMstr bMstr  = (BankMstr) criteria.list().get(0);
					
					/*criteria=session.createCriteria(Branch.class);
					criteria.add(Restrictions.eq(BranchCNTS.BRANCH_ID, bMstr.getBranch().getBranchId()));
					Branch branch  = (Branch) criteria.list().get(0);*/
					
					criteria=session.createCriteria(ChequeRequest.class);
					criteria.add(Restrictions.eq(ChequeRequestCNTS.C_REQ_ID, chqBook.getChequeRequest().getcReqId()));
					ChequeRequest chqReq  = (ChequeRequest) criteria.list().get(0);
					
					chqBookClone.setChqBkFromChqNo(CodePatternService.get10DigitCode(Long.parseLong(chequeBook.getChqBkToChqNo())+1));
					chqBookClone.setChqBkToChqNo(CodePatternService.get10DigitCode(Long.parseLong(chqBook.getChqBkToChqNo())));
					chqBookClone.setChqBkNOChq((int)(Long.parseLong(chqBook.getChqBkToChqNo())-Long.parseLong(chequeBook.getChqBkToChqNo())));
					chqBookClone.setUserCode(chqBook.getUserCode());
					chqBookClone.setbCode(chqBook.getbCode());
					chqBookClone.setChequeRequest(chqReq);
					chqBookClone.setBankMstr(bMstr);
					//chqBookClone.setBranch(branch);
					bMstr.getChequeBookList().add(chqBookClone);
					//branch.getChequeBookList().add(chqBookClone);
					session.update(bMstr);
					//session.update(branch);
					
					//chqBook.setChqBkFromChqNo(CodePatternService.get10DigitCode(Long.parseLong(chequeBook.getChqBkFromChqNo())));
					chqBook.setChqBkToChqNo(CodePatternService.get10DigitCode(Long.parseLong(chequeBook.getChqBkToChqNo())));
					chqBook.setChqBkIsIssued(true);
					chqBook.setChqBkIssueDt(sqlDate);
					chqBook.setChqBkNOChq((int)(Long.parseLong(chequeBook.getChqBkToChqNo())-Long.parseLong(chqBook.getChqBkFromChqNo())+1));
					chqBook.setChqBkPayType(chequeBook.getChqBkPayType());
					chqBook.setChqBkMaxAmtLimit(chequeBook.getChqBkMaxAmtLimit());
					chqBook.setAuthEmp1(authEmp1);
					chqBook.setAuthEmp2(authEmp2);
					
					session.merge(chqBook);
					session.update(chqBook);
					//session.save(chqBookClone);
					
				} else {
					chqBook.setChqBkIsIssued(true);
					chqBook.setChqBkIssueDt(sqlDate);
					chqBook.setChqBkPayType(chequeBook.getChqBkPayType());
					chqBook.setChqBkMaxAmtLimit(chequeBook.getChqBkMaxAmtLimit());
					chqBook.setAuthEmp1(authEmp1);
					chqBook.setAuthEmp2(authEmp2);
					session.clear();
					session.update(chqBook);
				}
			}
			
			transaction.commit();
			session.flush();
			temp = 1;
		}catch(Exception e){
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		
		return temp;
	}

	@Override
	@Transactional
	public List<ChequeBook> getIssuedChqBook(BankMstr bankMstr) {
		
		List<ChequeBook> chequeBookList = new ArrayList<>();
		
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			Criteria criteria=session.createCriteria(BankMstr.class);
			criteria.add(Restrictions.eq(BankMstrCNTS.BNK_ID, bankMstr.getBnkId()));
			BankMstr bMstr  = (BankMstr) criteria.list().get(0);
			
			Hibernate.initialize(bMstr.getChequeBookList());
			List<ChequeBook> chqBkList = bMstr.getChequeBookList();
			
			for (ChequeBook chequeBook : chqBkList) {
				if (chequeBook.isChqBkIsIssued() && !chequeBook.isChqBkIsRecByBr()) {
					chequeBookList.add(chequeBook);
				}
			}
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		
		return chequeBookList;
	}

	@Override
	@Transactional
	public int updateChqBookRecvdBr(List<ChequeBook> chequeBookList, User currentUser) {
		
		int temp = 0;
		
		java.sql.Date sqlDate = new java.sql.Date(new Date().getTime());
		
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			Criteria criteria=session.createCriteria(Branch.class);
			criteria.add(Restrictions.eq(BranchCNTS.BRANCH_ID, Integer.parseInt(currentUser.getUserBranchCode())));
			
			Branch branch = (Branch) criteria.list().get(0);
			
			for (ChequeBook chequeBook : chequeBookList) {
				if (chequeBook.isChqBkIsRecByBr()) {
					System.out.println("chequeType: "+chequeBook.getChequeRequest().getcReqType());
					chequeBook.setBranch(branch);
					chequeBook.setChqBkReceByBrt(sqlDate);
					branch.getChequeBookList().add(chequeBook);
					
					//cheque book to cheque leaves
					
					for (long i = Long.parseLong(chequeBook.getChqBkFromChqNo()); i <= Long.parseLong(chequeBook.getChqBkToChqNo()); i++) {
						ChequeLeaves chequeLeaves = new ChequeLeaves();
						chequeLeaves.setbCode(currentUser.getUserBranchCode());
						chequeLeaves.setChqLCType(chequeBook.getChequeRequest().getcReqType());
						chequeLeaves.setChqLChqMaxLt(chequeBook.getChqBkMaxAmtLimit());
						chequeLeaves.setChqLChqNo(CodePatternService.get10DigitCode(i));
						chequeLeaves.setChqLChqType(chequeBook.getChqBkPayType());
						chequeLeaves.setUserCode(currentUser.getUserCode());
						chequeLeaves.setChequeBook(chequeBook);
						chequeLeaves.setBranch(chequeBook.getBranch());
						chequeLeaves.setBankMstr(chequeBook.getBankMstr());
						
						//get bank
						criteria=session.createCriteria(BankMstr.class);
						criteria.add(Restrictions.eq(BankMstrCNTS.BNK_ID, chequeBook.getBankMstr().getBnkId()));
						BankMstr bMstr  = (BankMstr) criteria.list().get(0);
						bMstr.getChequeLeavesList().add(chequeLeaves);
						
						//get branch
						criteria=session.createCriteria(Branch.class);
						criteria.add(Restrictions.eq(BranchCNTS.BRANCH_ID, chequeBook.getBranch().getBranchId()));
						Branch br = (Branch) criteria.list().get(0);
						br.getChequeLeaveList().add(chequeLeaves);
						
						session.save(bMstr);
						session.save(br);
					}
				}
			}
			session.update(branch);
			transaction.commit();
			session.flush();
			temp = 1;
		}catch(Exception e){
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		
		return temp;
	}
}

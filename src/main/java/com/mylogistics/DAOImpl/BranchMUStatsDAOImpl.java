package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.BranchMUStatsDAO;
import com.mylogistics.constants.BranchMUStatsCNTS;
import com.mylogistics.model.BranchMUStats;
import com.mylogistics.model.BranchMUStatsBk;

public class BranchMUStatsDAOImpl implements BranchMUStatsDAO {
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public BranchMUStatsDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public int saveOrUpdateBMUS(BranchMUStats branchMUStats){
		System.out.println("enter into branchMonthlyUsageStats function");
		int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 //Criteria cr=session.createCriteria(BranchMUStats.class);
			 session.save(branchMUStats);
			 transaction.commit();
			 temp = 1;
			 session.flush();
			
		 }
		   
		 catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	}
	
	
	@Override
	public int saveOrUpdateBMUS(BranchMUStats branchMUStats,Session session){
		System.out.println("enter into branchMonthlyUsageStats function");
		int temp;
			 session.save(branchMUStats);
			 temp = 1;
		 return temp;
	}
	
	
	
	/*@SuppressWarnings("unchecked")
	@Transactional
	public List<BranchMUStats> getLastCnmtData(String branchCode,String type){
		List<BranchMUStats> List = new ArrayList<BranchMUStats>();
		Date lastDate=null;
		int id=0;
		System.out.println("Enter in getLastCnmtDate function---------------");
		 
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			session = this.sessionFactory.openSession();
			List = session.createQuery("from BranchMUStats where bmusStType='cnmt' order by bmusId DESC").setMaxResults(1).list();
			transaction.commit();
			lastDate = List.get(0).getBmusDt();
			id = List.get(0).getBmusId();
			System.out.println(" in DAO the last date is-------- "+lastDate);
			System.out.println(" in DAO the last id is-------- "+id);
			 session.flush();
			 
		}
		catch(Exception e){
			 e.printStackTrace();
		}
		session.close();
			 return List;
		 }*/
		
	@SuppressWarnings("unchecked")
	@Transactional
	 public int totalBMUSRows(String type){
		 int rows = -1;
		 List<BranchMUStats> branchMUStats = new ArrayList<BranchMUStats>();
		 try {
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(BranchMUStats.class);
			 cr.add(Restrictions.eq(BranchMUStatsCNTS.BMUS_ST_TYPE,type));
			 branchMUStats =cr.list();
			 rows = branchMUStats.size();
			 transaction.commit();
			 session.flush();
			 
		 	} 
		 catch (Exception e) {
		 	e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		return rows;
	 }
	
	
	@Override
	 public int totalBMUSRows(String type,Session session){
		 int rows = -1;
		 List<BranchMUStats> branchMUStats = new ArrayList<BranchMUStats>();
			 Criteria cr=session.createCriteria(BranchMUStats.class);
			 cr.add(Restrictions.eq(BranchMUStatsCNTS.BMUS_ST_TYPE,type));
			 branchMUStats =cr.list();
			 rows = branchMUStats.size();
		return rows;
	 }
	
	
	
	/*@SuppressWarnings("unchecked")
	@Transactional
	 public int totalChlnRows(){
		 int rows = -1;
		 List<BranchMUStats> branchMUStats = new ArrayList<BranchMUStats>();
		 try {
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(BranchMUStats.class);
			 cr.add(Restrictions.eq(BranchMUStatsCNTS.BMUS_ST_TYPE,"chln"));
			 branchMUStats =cr.list();
			 rows = branchMUStats.size();
			 transaction.commit();
				session.flush();
				session.close();
		 	} 
		 catch (Exception e) {
		 	e.printStackTrace();
		 }
	
		return rows;
	 }
	
	@SuppressWarnings("unchecked")
	@Transactional
	 public int totalSedrRows(){
		 int rows = -1;
		 List<BranchMUStats> branchMUStats = new ArrayList<BranchMUStats>();
		 try {
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(BranchMUStats.class);
			 cr.add(Restrictions.eq(BranchMUStatsCNTS.BMUS_ST_TYPE,"sedr"));
			 branchMUStats =cr.list();
			 rows = branchMUStats.size();
			 transaction.commit();
			 session.flush();
			
		 	} 
		 catch (Exception e) {
		 	e.printStackTrace();
		 }
			session.close();
		return rows;
	 }*/
	
	@Transactional
	public int updateBMUS(BranchMUStats branchMUStats){
		System.out.println("enter into branchMonthlyUsageStats function-----"+branchMUStats.getBmusAvgMonthUsage());
		System.out.println("enter into branchMonthlyUsageStats function-----"+branchMUStats.getBmusId());
		System.out.println("enter into branchMonthlyUsageStats function-----"+branchMUStats.getBmusDaysLast());
		int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 session.update(branchMUStats);
			 transaction.commit();
			 temp = 1;
			 session.flush();
		
		 }
		   
		 catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	}
	
	
	@Override
	public int updateBMUS(BranchMUStats branchMUStats,Session session){
		System.out.println("enter into branchMonthlyUsageStats function-----"+branchMUStats.getBmusAvgMonthUsage());
		System.out.println("enter into branchMonthlyUsageStats function-----"+branchMUStats.getBmusId());
		System.out.println("enter into branchMonthlyUsageStats function-----"+branchMUStats.getBmusDaysLast());
		int temp;
			 session.update(branchMUStats);
			 temp = 1;
		
		 return temp;
	}
	
	
	
	
	/*@SuppressWarnings("unchecked")
	@Transactional
	public List<BranchMUStats> getLastChlnData(String branchCode){
		List<BranchMUStats> List = new ArrayList<BranchMUStats>();
		Date lastDate=null;
		int id=0;
		System.out.println("Enter in getLastChlnData function---------------");
		 
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			session = this.sessionFactory.openSession();
			List = session.createQuery("from BranchMUStats where bmusStType='chln' order by bmusId DESC").setMaxResults(1).list();
			transaction.commit();
			lastDate = List.get(0).getBmusDt();
			id = List.get(0).getBmusId();
			System.out.println(" in DAO the last date is-------- "+lastDate);
			System.out.println(" in DAO the last id is-------- "+id);
			 session.flush();
			 
		}
		catch(Exception e){
			 e.printStackTrace();
		}
		session.close();
			 return List;
		 }*/
	
	/*@SuppressWarnings("unchecked")
	@Transactional
	public List<BranchMUStats> getLastSedrData(String branchCode){
		List<BranchMUStats> List = new ArrayList<BranchMUStats>();
		Date lastDate=null;
		int id=0;
		System.out.println("Enter in getLastSedrData function---------------");
		 
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			session = this.sessionFactory.openSession();
			List = session.createQuery("from BranchMUStats where bmusStType='sedr' order by bmusId DESC").setMaxResults(1).list();
			transaction.commit();
			lastDate = List.get(0).getBmusDt();
			id = List.get(0).getBmusId();
			System.out.println(" in DAO the last date is-------- "+lastDate);
			System.out.println(" in DAO the last id is-------- "+id);
			 session.flush();
			
		}
		catch(Exception e){
			 e.printStackTrace();
		}
		 session.close();
			 return List;
		 }*/
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<String> getAllBranch(){
		System.out.println("enter into getAllBranch function");
		List<BranchMUStats> list = new ArrayList<BranchMUStats>();
		List<String> branchCodeList = new ArrayList<String>();
		
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria criteria = session.createCriteria(BranchMUStats.class);
			
			 list = criteria.list();
			 
			 HashSet<String> hashSet = new HashSet<String>();
			 for(int i=0;i<list.size();i++){
				 hashSet.add(list.get(i).getBmusBranchCode());
			 }
			 
			 Iterator<String> itr = hashSet.iterator();
		        while(itr.hasNext())
		        {	
		        	branchCodeList.add((String)itr.next());
		        }
			 
			 transaction.commit();
			 session.flush();
			
		 }
		 catch(Exception e){
			 e.printStackTrace(); 
		 }
		 session.clear();
		 session.close();
		 return branchCodeList;
	 }
	
	
	@SuppressWarnings("unchecked")
	@Transactional 
	public double getAvgMonUsg(String type){
		System.out.println("enter into getAvgMonUsgCNMT function --->");
		List<BranchMUStats> list = new ArrayList<BranchMUStats>();
		double avgMonUsgCNMT = 0.0;
		 try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria criteria = session.createCriteria(BranchMUStats.class);
				 criteria.add(Restrictions.ge(BranchMUStatsCNTS.BMUS_ST_TYPE,type)); 
				 list = criteria.list();
				 /*System.out.println("*************list.get(i).getBmusAvgMonthUsage = "+list.get(i).getBmusAvgMonthUsage());*/
				// list = session.createQuery("from BranchMUStats where bmusBranchCode='"+branchList.get(i)+"' order by bmusId ").setMaxResults(1).list();
				 for(int i=0;i<list.size();i++){
					 avgMonUsgCNMT = avgMonUsgCNMT + list.get(i).getBmusAvgMonthUsage();
				 }
				
			 
			 System.out.println("avgMonUsgCNMT = "+avgMonUsgCNMT);
			 transaction.commit();
			 session.flush();
			
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		return avgMonUsgCNMT;
	 }
	
	
/*	@SuppressWarnings("unchecked")
	@Transactional
	public Date getFirstEntryDate(){
		System.out.println("enter into getAvgMonUsgCNMT function --->");
		List<BranchMUStats> branchMUStats = new ArrayList<BranchMUStats>();
		 Date date = null;
		 try{
			 session = this.sessionFactory.openSession();
			 branchMUStats = session.createQuery("from BranchMUStats where bmusStType='cnmt' order by brsStatsId ").setMaxResults(1).list();
			 date = branchMUStats.get(0).getBmusDt();
			 System.out.println("The first date is---------------"+date);
			 session.flush();
			 
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 session.close();
		 return date;
	 }*/
	
	
	/*@SuppressWarnings("unchecked")
	@Transactional
	public double getNoOfCnmt(Date startDate,Date endDate){
		
		List<BranchMUStats> BranchMUStats = new ArrayList<BranchMUStats>();
		double noOfCnmt = 0;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria criteria = session.createCriteria(BranchMUStats.class);
			 criteria.add(Restrictions.ge(BranchMUStatsCNTS.BMUS_DT,endDate)); 
			 criteria.add(Restrictions.lt(BranchMUStatsCNTS.BMUS_DT,startDate));
			 criteria.add(Restrictions.ge(BranchMUStatsCNTS.BMUS_ST_TYPE,"dispatch")); 
			 BranchMUStats = criteria.list();
			 for(int i=0;i<BranchMUStats.size();i++){
				 noOfCnmt = BranchMUStats.get(i).get
			 }
			 listSize=branchSStats.size();
			 transaction.commit();
		 }
		 catch(Exception e){
			 e.printStackTrace(); 
		 }
		 session.flush();
		 session.close();
		 return listSize;
	 }*/
	
	/*@SuppressWarnings("unchecked")
	@Transactional
	public double getAvgMonUsgCHLN(){
		System.out.println("enter into getAvgMonUsgCHLN function --->");
		List<BranchMUStats> list = new ArrayList<BranchMUStats>();
		double avgMonUsgCHLN = 0.0;
		 try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria criteria = session.createCriteria(BranchMUStats.class);
				 criteria.add(Restrictions.ge(BranchMUStatsCNTS.BMUS_ST_TYPE,"chln")); 
				 list = criteria.list();
				 for(int i=0;i<list.size();i++){
					 avgMonUsgCHLN = avgMonUsgCHLN + list.get(i).getBmusAvgMonthUsage();
				 }
				
			 
			 System.out.println("avgMonUsgCHLN = "+avgMonUsgCHLN);
			 transaction.commit();
			 session.flush();
		
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 session.close();
		 return avgMonUsgCHLN;
	 }
	
	@SuppressWarnings("unchecked")
	@Transactional
	public double getAvgMonUsgSEDR(){
		System.out.println("enter into getAvgMonUsgSEDR function --->");
		List<BranchMUStats> list = new ArrayList<BranchMUStats>();
		double avgMonUsgSEDR = 0.0;
		 try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria criteria = session.createCriteria(BranchMUStats.class);
				 criteria.add(Restrictions.ge(BranchMUStatsCNTS.BMUS_ST_TYPE,"sedr")); 
				 list = criteria.list();
				 for(int i=0;i<list.size();i++){
					 avgMonUsgSEDR = avgMonUsgSEDR + list.get(i).getBmusAvgMonthUsage();
				 }
				
			 
			 System.out.println("avgMonUsgSEDR = "+avgMonUsgSEDR);
			 transaction.commit();
			 session.flush();
			
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 session.close();
		 return avgMonUsgSEDR;
	}*/
	
	@Transactional
	public int deleteBMUS(int id){
		BranchMUStats bMuStats = new BranchMUStats();
		int temp=-1;
	    try{
	    	session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(BranchMUStats.class);
			 cr.add(Restrictions.eq(BranchMUStatsCNTS.BMUS_ID,id));
			 bMuStats = (BranchMUStats) cr.list().get(0);
			 session.delete(bMuStats);
			 transaction.commit();
			 System.out.println("bMuStats Data is deleted successfully");	 
			 temp=1;
			 session.flush();
			
	    }catch (Exception e){
			e.printStackTrace();
		}
	    session.clear();
	    session.close();
		 return temp;
   }
	
	
	@Override
	public int deleteBMUS(int id,Session session){
		BranchMUStats bMuStats = new BranchMUStats();
		int temp=-1;
			 Criteria cr=session.createCriteria(BranchMUStats.class);
			 cr.add(Restrictions.eq(BranchMUStatsCNTS.BMUS_ID,id));
			 bMuStats = (BranchMUStats) cr.list().get(0);
			 session.delete(bMuStats);
			 System.out.println("bMuStats Data is deleted successfully");	 
			 temp=1;
		 return temp;
   }
	
	
	@Transactional
	public int saveBMUSBK(BranchMUStatsBk branchMUStatsBk){
		System.out.println("enter into branchMonthlyUsageStats function");
		int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 session.save(branchMUStatsBk);
			 transaction.commit();
			 temp = 1;
			 session.flush();
			
		 }
		   
		 catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	}
	
	
	
	@Override
	public int saveBMUSBK(BranchMUStatsBk branchMUStatsBk,Session session){
		System.out.println("enter into branchMonthlyUsageStats function");
		int temp;
			 session.save(branchMUStatsBk);
			 temp = 1;
			
		 return temp;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<BranchMUStats> getLastData(String branchCode,String type){
		List<BranchMUStats> List = new ArrayList<BranchMUStats>();
		Date lastDate=null;
		int id=0;
		System.out.println("Enter in getLastData function---------------"+type);
		 
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			org.hibernate.Query query =  session.createQuery("from BranchMUStats where bmusStType= :type order by bmusId DESC").setMaxResults(1);
			query.setParameter("type",type);
			List=query.list();
			transaction.commit();
			lastDate = List.get(0).getBmusDt();
			id = List.get(0).getBmusId();
			System.out.println(" in DAO the last date is-------- "+lastDate);
			System.out.println(" in DAO the last id is-------- "+id);
			 session.flush();
			
		}
		catch(Exception e){
			 e.printStackTrace();
		}
		session.clear();
		 session.close();
			 return List;
		 }
	
	
	@Override
	public List<BranchMUStats> getLastData(String branchCode,String type,Session session){
		List<BranchMUStats> List = new ArrayList<BranchMUStats>();
		Date lastDate=null;
		int id=0;
		System.out.println("Enter in getLastData function---------------"+type);
		 
			org.hibernate.Query query =  session.createQuery("from BranchMUStats where bmusStType= :type order by bmusId DESC").setMaxResults(1);
			query.setParameter("type",type);
			List=query.list();
			lastDate = List.get(0).getBmusDt();
			id = List.get(0).getBmusId();
			System.out.println(" in DAO the last date is-------- "+lastDate);
			System.out.println(" in DAO the last id is-------- "+id);
			
			 return List;
		 }

	
	
	
	
}


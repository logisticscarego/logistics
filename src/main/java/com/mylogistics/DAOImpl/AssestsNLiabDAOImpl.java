package com.mylogistics.DAOImpl;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.AssestsNLiabDAO;
import com.mylogistics.constants.UserCNTS;
import com.mylogistics.model.AssestsNLiab;
import com.mylogistics.model.Employee;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.MoneyReceipt;
import com.mylogistics.model.User;
import com.mylogistics.model.dataintegration.AssestsNLiabDemo;

public class AssestsNLiabDAOImpl implements AssestsNLiabDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public AssestsNLiabDAOImpl(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	@Override
	public int saveAssestsNLiab() {

		List<AssestsNLiabDemo> alList;
		List<FAParticular> faParticularsList;
		int temp = 0;
		try{
			session = this.sessionFactory.openSession();//fixed
			transaction = session.beginTransaction();//fixed
			
			Criteria cr=session.createCriteria(FAParticular.class);
			
			cr.add(Restrictions.eq("faPerType", "assestsnliab"));
			faParticularsList = cr.list();
			System.out.println("saveAssestsNLiab DB function=========>>");
			if (!faParticularsList.isEmpty()) {
				temp = 1;
				String id = String.valueOf(faParticularsList.get(0).getFaPerId());
				
				if (id.length()<2) {
					id="0"+id;
				}
				
				System.out.println("\n\nFAParticular Id: "+id);
				
				cr=session.createCriteria(AssestsNLiabDemo.class);
				alList  = cr.list();
				
				for (AssestsNLiabDemo assestsNLiabDemo : alList) {
					
					AssestsNLiab assestsNLiab = new AssestsNLiab();
					assestsNLiab.setAlName(assestsNLiabDemo.getAlName());
					assestsNLiab.setAlFaCode(id+assestsNLiabDemo.getAlFaCode());
					session.save(assestsNLiab);
					
				}
			}else {
				System.out.println("no entry for assestsNLiab in faPerticular Table");
				temp = 0;
			}
			transaction.commit();//fixed
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
			temp = 0;
		}
		session.clear();
		session.close();
		return temp;
	}
	
	@Transactional
	@Override
	public int saveAssestsNLiabFaMstr(String userCode, String bCode) {

		List<AssestsNLiab> alList;
		//List<FAParticular> faParticularsList;
		int temp = 0;
		try{
			session = this.sessionFactory.openSession();//fixed
			transaction = session.beginTransaction();//fixed
			
			Criteria cr=session.createCriteria(AssestsNLiab.class);
			
			alList = cr.list();
			if (!alList.isEmpty()) {
				temp = 1;
				for (AssestsNLiab assestsNLiab : alList) {
					System.out.println("assestsNLiab FaCode: "+assestsNLiab.getAlFaCode()+
							"\t"+"assestsNLiab Name: "+assestsNLiab.getAlName());
					
					FAMaster faMaster = new FAMaster();
					faMaster.setFaMfaCode(assestsNLiab.getAlFaCode());
					faMaster.setFaMfaName(assestsNLiab.getAlName());
					faMaster.setbCode(bCode);
					faMaster.setUserCode(userCode);
					faMaster.setFaMfaType("assestsnliab");
					
					session.save(faMaster);
				}
			} else {
				System.out.println("no entry for assestsNLiab in faMaster Table");
				temp = 0;
			}
			transaction.commit();//fixed
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
			temp = 0;
		}
		session.clear();
		session.close();
		return temp;
	}

	@Override
	@Transactional
	public int getTest() {
		System.out.println("AssestsNLiabDAOImpl.getTest()");
		try {
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("from MoneyReceipt where mrNo = 'MR000589' and mrBrhId = 20");
			List<MoneyReceipt> mrList = query.list();
			System.out.println("mrListSize: "+mrList.size());
			if (!mrList.isEmpty()) {
				for (MoneyReceipt moneyReceipt : mrList) {
					System.out.println("mrFrPdListSize: "+moneyReceipt.getMrFrPDList().size());
					for (Map<String, Object> mrFrPd : moneyReceipt.getMrFrPDList()) {
						System.out.println("mrFrPd List: "+mrFrPd.entrySet());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return 0;
	}

}

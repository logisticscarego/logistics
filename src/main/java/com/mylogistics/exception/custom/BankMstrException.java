package com.mylogistics.exception.custom;

public class BankMstrException extends Exception {

	private static final long serialVersionUID = -7835604683557087305L;

	public BankMstrException(String exceptionMsg) {
		super(exceptionMsg);
	}
	
}
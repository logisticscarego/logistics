package com.mylogistics.validator;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.mylogistics.constants.MoneyReceiptCNTS;
import com.mylogistics.model.MoneyReceipt;

@Component
@Qualifier("onAccountingMRValidator")
public class OnAccountingMRValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return MoneyReceipt.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		MoneyReceipt moneyReceipt = (MoneyReceipt) target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, MoneyReceiptCNTS.MR_Date, "", "MR date is required");
		if (moneyReceipt.getMrCustId() <= 0)
			errors.rejectValue("", "Customer is required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, MoneyReceiptCNTS.MR_PAY_BY, "", "Payby is required");
		if (moneyReceipt.getMrNetAmt() <= 0)
			errors.rejectValue("", "Net amount should be greater than 0");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, MoneyReceiptCNTS.MR_DESC, "Description is required", "");
		if (moneyReceipt.getMrDesc() != null && moneyReceipt.getMrDesc().length() < 2)
			errors.rejectValue("", "Description is too short than 2");
		if (moneyReceipt.getMrDesc() != null && moneyReceipt.getMrDesc().length() > 200)
			errors.rejectValue("", "Description is too long than 200");
		
		if (moneyReceipt.getMrPayBy() != 'C') {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, MoneyReceiptCNTS.MR_CUST_BANK_NAME, "Customer bank is required", "");
			if (moneyReceipt.getMrBnkId() <= 0)
				errors.rejectValue("", "Bank is required");
			if (moneyReceipt.getMrPayBy() == 'R')
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, MoneyReceiptCNTS.MR_RTGS_REF_NO, "RTGS is required", "");
			else if (moneyReceipt.getMrPayBy() == 'Q') {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, MoneyReceiptCNTS.MR_CHQ_NO, "Cheque is required", "");
				if (moneyReceipt.getMrChqNo().length() < 6)
					errors.rejectValue("", "Cheque no. should be greater than 5");
			}
		}
			
	}	

}
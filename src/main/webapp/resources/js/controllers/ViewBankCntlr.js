'use strict';

var app = angular.module('application');

app.controller('ViewBankCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){

	console.log("ViewBankCntlr Started");
	
	$scope.bankMstrList = [];
	
	$scope.bankMstr = {};
	
	$scope.bankDBFlag = true;
	$scope.viewBankFlag = true;
	
	$scope.viewBankMstr = function(){
		console.log("viewBankMstr Entered");
		var response = $http.post($scope.projectName+'/viewBankMstr');
		response.success(function(data, status, headers, config){
			if (data.result === "success") {
				console.log("viewBankMstr Success");
				$scope.bankMstrList = data.bankMstrList;
				for ( var i = 0; i < $scope.bankMstrList.length; i++) {
					console.log("Bank Name: "+$scope.bankMstrList[i].bnkName);
				}
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
	}
	
	$scope.openBankDB = function(){
		console.log("Entered into openBankDB");
		$scope.bankDBFlag = false;
		$('div#bankDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bank",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.bankNameDBFlag = true;
			}
		});

		$('div#bankDB').dialog('open');
	}
	
	$scope.saveBank =  function(bankMstr){
		console.log("bankMstr.bnkName----->"+bankMstr.bnkName);
		console.log("bankMstr.bnkFaCode----->"+bankMstr.bnkFaCode);
		 $scope.bankMstr = bankMstr;
		$('div#bankDB').dialog('close');
	}
	
	
	$scope.viewBankSubmit =  function(viewBankForm){
		console.log("entered into viewBnakSubmit");
		
		if (viewBankForm.$invalid) {
			if (viewBankForm.selectBank.$invalid) {
				$scope.alertToast("please select bank");
			} 
		}else {
			$scope.alertToast("write code over here");
			$scope.viewBankFlag = false;
		}
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.viewBankMstr();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("ViewBankCntlr Ended");
}]);
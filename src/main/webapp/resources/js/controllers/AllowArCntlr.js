'use strict';

var app = angular.module('application');

app.controller('AllowArCntlr',['$scope','$location','$http','$window','FileUploadService','$filter',
                            function($scope,$location,$http,$window,FileUploadService,$filter){

	$scope.message="";
	$scope.message2="";
	$scope.arTableDBFlag=true;
	$scope.UDamt=0;
	$scope.amt=[];
	$scope.undetFlag=[];
	$scope.message3=[];
	$scope.message4=[];
	$scope.message5=[];
	$scope.message6=[];
	$scope.message7=[];
	$scope.message8=[];
	$scope.unloadingAmt=0;
	$scope.detentionAmt=0;
	$scope.overheightAmt=0;
	$scope.extraKmAmt=0;
	$scope.lateAckAmt=0;
	$scope.lateDlvryAmt=0;
	$scope.wtshrtgAmt=0;
	$scope.drClaimAmt=0;


	
	$scope.allowAr=function(startDate,endDate)
	{
		var dateData={fromDate:startDate, toDate:endDate};
		console.log(dateData);
		console.log(startDate);
		console.log(endDate);
		var sdate=new Date(startDate);
		var edate=new Date(endDate)
		console.log(sdate);
		if(sdate>new Date())
			{
			$scope.message="Should be less than or equal Today's Date";
			}
		else{
			$scope.message="";
		}
		if(edate>new Date() || endDate<startDate)
			{
			$scope.message2="should be greater than Start Date and less than or equal Today's Date";
			}
		else
			{
			$scope.message2="";
			}
		if(sdate<=new Date() && edate<=new Date() && endDate>=startDate)
			{
			console.log(" Hi i am here and data is good");
			var response=	$http.post($scope.projectName + '/fetchArData', dateData);
			response.success(function(data, status, headers, config){
				console.log(data);
				if(data.result === "success"){
				console.log(data.result);
				 $scope.alertToast(data.result);
				if(data.arData.length>0)
					{
					$scope.arTableDBFlag=false;
					$scope.arData=data.arData;
					}
				else
					{
					$scope.arTableDBFlag=true;
					}
				
			}});
			response.error(function(data, status, headers, config){
				console.log("Error in hitting /getCnmtCodeByCode");
				 $scope.alertToast("Error");
			});
			}
	}
	

	$scope.updateAr=function(newRmrk,validate,allowed,arNo,index,unloadingAmt,detentionAmt,overheightAmt,extraKmAmt,lateAckAmt,lateDlvryAmt,wtshrtgAmt,drClaimAmt,object)
	{
		

		var arUpdateData={remark:newRmrk,validate:validate,arNo:arNo,unloading:unloadingAmt,detention:detentionAmt,overheight:overheightAmt,extraKm:extraKmAmt,lateAck:lateAckAmt,lateDelivery:lateDlvryAmt,wtShrtg:wtshrtgAmt,drClaim:drClaimAmt,arActualData:object};
		console.log(arUpdateData)
 if(angular.isDefined(validate) && angular.isDefined(allowed)  && angular.isDefined(newRmrk))
 {
			console.log("Okkkkkkkkkkk");
			$scope.message3[index]="";
			$scope.message4[index]="";
			$scope.message5[index]="";
			$scope.message6[index]="";
			$scope.message7[index]="";
			$scope.message8[index]="";

			if(allowed==="Y")
				{
				if(unloadingAmt>=0 && detentionAmt>=0 && extraKmAmt>=0 && overheightAmt>=0 && lateAckAmt>=0 && lateDlvryAmt>=0&& wtshrtgAmt>=0 && drClaimAmt>=0)
					{
			var response=$http.post($scope.projectName + '/updateAr', arUpdateData);
				$scope.message5[index]="";
				response.success(function(data, status, headers, config){
					console.log(data);
					if(data.result === "success"){
					console.log(data.result);
			      $scope.alertToast(data.result);
			      $scope.arData.splice(index,1);
	

					
				}
					else
						{
						$scope.alertToast("Remaining can't be less than 0");
						}
					
					});
				response.error(function(data, status, headers, config){
					console.log("Error in hitting /getCnmtCodeByCode");
					  $scope.alertToast(data.result);
					  
				});
				}
				else
					{
					$scope.alertToast("Input Value can't be less than 0");
					}
				}
			else
				{
				//$scope.message5[index]="Please Select Yes";
				alert("Not Allowed");
				var response=$http.post($scope.projectName + '/updateRemAr', arUpdateData);	
				console.log("I am here")
				response.success(function(data, status, headers, config){
					console.log(data);
					if(data.result === "success"){
					console.log(data.result);
			      $scope.alertToast(data.result);
			      $scope.arData.splice(index,1);	
				}});
				response.error(function(data, status, headers, config){
					console.log("Error in hitting /getCnmtCodeByCode");
					  $scope.alertToast(data.result);
					  
				});
				}
			}
		else if(angular.isDefined(validate)===false)
			{
			$scope.message3[index]="Required";
			$scope.message4[index]="";
			}

		else if(angular.isDefined(newRmrk)===false)
		{
		$scope.message7[index]="Required";
		$scope.message6[index]="";
		}
		else if(angular.isDefined(allowed)===false)
			{
			$scope.message3[index]="";
			$scope.message4[index]="Required";
			}
	}
	
	$scope.openCnmt=function(chlnCode)
	{
		console.log(chlnCode);
		var response=$http.post($scope.projectName + '/viewCnmtFile', chlnCode);
		response.success(function(data, status, headers, config){
		     console.log(data.result);
			if(data.result==="success")
				{

         $scope.alertToast(data.result);
         var form = document.createElement("form");
         form.method = "POST";
         form.action = $scope.projectName + '/downloadImgCnmtAr';
         form.target = "_blank";
         document.body.appendChild(form);
         form.submit();
         
//     	$window.open($scope.projectName + '/downloadImgCnmtAr');
		}
			else if(data.result==="error")
				{
				  $scope.alertToast("File Not Found");
				}
		
		});
		response.error(function(data, status, headers, config){
			console.log("Error in hitting /getCnmtCodeByCode");
			console.log(data);
			 // $scope.alertToast("File Not Found");
		});
		
	}
	
}]);

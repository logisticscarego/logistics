'use strict';

var app = angular.module('application');

app.controller('StnTransferCntlr',['$scope','$location','$http','AuthorisationService',
                                 function($scope,$location,$http,AuthorisationService){

	console.log("StnTransferCntlr Started");
	
	$scope.branchDBFlag = true;
	$scope.openTotalDBFlag = true;
	$scope.submitButton = true;
	
	$scope.branchList = [];
	$scope.noList = [];
	
	$scope.transfer = {};	
	
	$('#startNo').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});
	
	$('#endNo').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});
	
		
	$scope.getBranchData = function(){
		console.log("Enter into getBranchData() ");
		var response = $http.post($scope.projectName+'/getBranchDataForCustomer');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log(data.result);
				$scope.branchList = data.list;				
			}else{
				console.log(data.result);
			}
		});
		response.error(function(data, status, headers,config) {
			console.log("Error.....");
		});
		console.log("Exit from getBranchData() ");
	}
	
	$scope.saveBranch = function(branch){
		$scope.transfer.branchCode = branch.branchCode;
		$('div#branchDB').dialog('close');
	}
	
	$scope.make = function(){		  
		 var len = parseInt($scope.totalNo);
		 $scope.noList = [];
		 $scope.transfer.startList = [];
		 var len = parseInt($scope.totalNo);		 
		 for(var i=0; i<len; i++)
			 $scope.noList.push(i);		 
	}
	
	$scope.openTotalDB = function(){
		$scope.openTotalDBFlag = false;
		$('div#openTotalDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Start No",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});
		$('div#openTotalDB').dialog('open');
	}
	
	$scope.submitTransferForm = function(stnTransferForm, transfer){
		console.log("Enter into submitTransferForm() : Form Valid = "+stnTransferForm.$valid);
		if(stnTransferForm.$invalid){
			if(stnTransferForm.branchCode.$invalid){
				$scope.alertToast("Please select branch !");
			}
			if(stnTransferForm.transferType.$invalid){
				$scope.alertToast("Please select type !");
			}	
			if(angular.isUndefined(transfer.startList)){
				$scope.alertToast("Enter start no !");
			}
		}else{
			if(angular.isUndefined(transfer.startList)){
				$scope.alertToast("Enter start no !");
				return ;
			}
			if(transfer.startList.length == 0){
				$scope.alertToast("Enter start no !");
				return ;
			}
			for(var i=0; i<transfer.startList.length; i++){
				if($.trim(transfer.startList[i]) === ""){
					$scope.alertToast("Enter start no !");
					return ;
				}
			}
			console.log("Data : "+$scope.transfer);
			$("#transferSubmit").attr("disabled", "disabled");
			var response = $http.post($scope.projectName+'/stnTransferToBrh', $scope.transfer);
			response.success(function(data, status, headers, config){
				console.log("Result = "+data.result);
				if(data.result === "success"){
					$scope.alertToast(data.result);
					$("#transferSubmit").removeAttr("disabled");
				}else{
					for(var i=0; i<data.msg.length; i++)
						$scope.alertToast(data.msg[i]);
				}
			});
			response.error(function(data, status, headers, config){
				console.log("Error in httting /stnTransferToBrh");
			});
		}
		
		console.log("Exit from submitTransferForm()");
	}

	$scope.OpenBranchDB = function(){
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Branch",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});
		$('div#branchDB').dialog('open');
	}
	
	$scope.isStnAvailable = function(){
		console.log("Enter into isStnAvailable()");
		if(angular.isUndefined($scope.transfer.startList) || parseInt($scope.totalNo) < 1){
			$scope.alertToast("Enter tatoal no. !");
			return ;
		}

		if($scope.transfer.startList.length < 1)
			$scope.alertToast("Enter start no. !");
		else{		
			var response = $http.post($scope.projectName + '/isStnAvailable', $scope.transfer);
			response.success(function(data, status, headers, config){
				console.log(data.result);
				if(data.result === "success"){
					$scope.alertToast(data.result);
					$scope.openTotalDBFlag = true;
					$scope.submitButton = false;
					$('div#openTotalDB').dialog('close');
				}else{					
					for(var i=0; i<data.msg.length; i++)
						$scope.alertToast(data.msg[i]);
				}
			});
			response.error(function(data, status, headers, config){
				console.log("Error in hitting /isStnAvailable ");
			});
		}	
		console.log("Exit from isStnAvailable()");
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getBranchData();
	}else if($scope.logoutStatus === true){	
		$location.path("/");
	}else{
		console.log("****************");
	}	
	console.log("StnTransferCntlr Ended");
}]);
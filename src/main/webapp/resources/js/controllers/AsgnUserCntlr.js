'use strict';

var app = angular.module('application');

app.controller('AsgnUserCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.userList = [];
	//$scope.branchList = [];
	$scope.empList = [];
	
	$scope.user = {};
	$scope.branch = {};
	$scope.emp = {};
	
	$scope.userModDBFlag = true;
	$scope.brModDBFlag = true;
	$scope.empModDBFlag = true;
	
	$scope.getUserFrAsgn = function(){
		console.log("enter into getUserAndBr funciton");
		var response = $http.post($scope.projectName+'/getUserFrAsgn');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.userList = data.userList;
			}else if(data.result === "error"){
				$scope.alertToast(data.msg);
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.openUserModel = function(){
		console.log("enter into openUserModel function");
		$scope.userModDBFlag = false;
		$('div#userModDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select User",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.userModDBFlag = true; 
		    }
			});
		
		$('div#userModDB').dialog('open');	
	}
	
	
	$scope.selectUser = function(user){
		console.log("enter into selectUser funciton");
		$scope.user = user;
		$('div#userModDB').dialog('close');	
		
		if(!angular.isUndefined($scope.user)){
			var response = $http.post($scope.projectName+'/getBrFrAsgn',$scope.user.userBranchCode);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.branch = data.branch;
					if(data.msg === "emp avaliable"){
						$scope.empList = data.empList;
					}else{
						$scope.alertToast(data.msg);
					}
				}else{
					console.log("error in braing User branch");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
		}
		
	}
	
	/*$scope.openBranchModel = function(){
		console.log("enter into openBranchModel funciton");
		$scope.brModDBFlag = false;
		$('div#brModDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select User",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.brModDBFlag = true;
		    }
			});
		
		$('div#brModDB').dialog('open');	
	}
	
	
	$scope.selectBranch = function(brh){
		console.log("enter into selectBranch function");
		$scope.branch = brh;
		$('div#brModDB').dialog('close');
	}*/
	
	
	$scope.openEmpModel = function(){
		console.log("enter into openEmpModel funciton");
		$scope.empModDBFlag = false;
		$('div#empModDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Employee",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.empModDBFlag = true;
		    }
			});
		
		$('div#empModDB').dialog('open');	
	}
	
	$scope.selectEmp = function(emp){
		console.log("enter into selectEmp function");
		$scope.emp = emp;
		$('div#empModDB').dialog('close');	
	}
	
	$scope.assgnSubmit = function(assgnForm){
		
		console.log("enter into assgnSubmit function");
		if(assgnForm.$invalid){
			$scope.alertToast("please fill correct form");
		}else{
			var actData = {
				"userId" : 	$scope.user.userId,
				"empId"  :  $scope.emp.empId,
			}
			 $('#saveId').attr("disabled","disabled");
			var response = $http.post($scope.projectName+'/submitAsgnUsr',actData);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.user = {};
					$scope.branch = {};
					$('#saveId').removeAttr("disabled");
					$scope.emp = {};
					$scope.getUserFrAsgn()
					$scope.alertToast(data.result);
				}else{
					$scope.alertToast(data.result);
					console.log("error in assigning user");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
			
		}
	}
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getUserFrAsgn();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
}]);
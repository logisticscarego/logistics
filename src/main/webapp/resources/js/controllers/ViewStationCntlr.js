'use strict';

var app = angular.module('application');

app.controller('ViewStationCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.stateCodeDBFlag = true;
	$scope.stationCodeDetailsFlag = true;
	$scope.stationDetailsFlag = false;
	$scope.stationCodeDBFlag = true;
	$scope.station ={};
	$scope.stn = {};
	$scope.state = {};
	
	
	$scope.getStationDetails = function(){
		console.log("Entered into getStationDetails function------>");
		var response = $http.post($scope.projectName+'/getStationDetails');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.stationList = data.list;
				for(var i=0;i<$scope.stationList.length;i++){
					$scope.stationList[i].creationTS =  $filter('date')($scope.stationList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
				}
				$scope.successToast(data.result);
			}else{
				console.log(data);
			}	
			$scope.getStateForStn();
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	 $scope.openStateCodeDB = function(){
		  $scope.stateCodeDBFlag = false;
			$('div#stateCodeDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				draggable: true,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "State Code",
				close: function(event, ui) { 
		            $(this).dialog('destroy');
		            $(this).hide();
		        }
				});
			
			$('div#stateCodeDB').dialog('open');
	  }
	  
	  $scope.saveStateCode =  function(state){
			console.log("enter into saveStateCode----->"+state.stateCode);
			$scope.stateName = state.stateName;
			$scope.station.stateCode = state. stateCode;
			$('div#stateCodeDB').dialog('close');
			$scope.stateCodeDBFlag = true;
		//	$scope.filterStateCode = "";
			
		}
			
		$scope.getStateForStn = function(){
			console.log("Entered into getStateForStn function------>");
			var response = $http.post($scope.projectName+'/getStateForStn');
			response.success(function(data, status, headers, config){
				if(data.result==="success"){
					$scope.stateList = data.list;
					$scope.successToast(data.result);
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}
		
		$scope.submitCode = function(stationName,stationCode){
			console.log("enter into submit function--->"+$scope.stationCode);
			$scope.code=$('#stationName').val();
			if($scope.code === ""){
				$scope.alertToast("Please enter station code....");
			}else{
				var response = $http.post($scope.projectName+'/stationDetails', stationCode);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.station = data.station;
						$scope.stateName = $scope.station.stateCode;
						$scope.stationCodeDetailsFlag = false;
						$scope.stationDetailsFlag = true;
						$scope.time =  $filter('date')($scope.station.creationTS, 'MM/dd/yyyy hh:mm:ss');
						$scope.successToast(data.result);
						
					}else{
						console.log(data);
					}				
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
			}
		}
		
		$scope.updateStation = function(StationForm){
			console.log("Entered into updateStation function");
			
			 if(StationForm.$invalid){
				 if(StationForm.stnName.$invalid){
						$scope.alertToast("Please enter station name between 3-40 characters...");
					}else if(StationForm.stnDistrict.$invalid){
						$scope.alertToast("Please enter station district between 3-40 characters...");
					}else if(StationForm.stnPin.$invalid){
						$scope.alertToast("Please enter station pin of 6 digits...");
					}
			 }else{	
				 var response = $http.post($scope.projectName+'/updateStation',$scope.station);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
							console.log("success");
							$scope.successToast(data.result);
							$scope.station="";
							$scope.stationCodeDetailsFlag = true;
							$scope.stationDetailsFlag = false;
							$scope.getStationDetails();
						}else{
							console.log(data);
						}		
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data);
				    });
					
			 }
			
		}
		
		 $scope.openStationCodeDB = function(){
			  $scope.stationCodeDBFlag = false;
				$('div#stationCodeDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					draggable: true,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Station Code",
					close: function(event, ui) { 
			            $(this).dialog('destroy');
			            $(this).hide();
			        }
					});
				
				$('div#stationCodeDB').dialog('open');
		  }
		
		/*$scope.getStationCodeList = function(){
			console.log("Entered into getStationCodeList function---");
			var response = $http.post($scope.projectName+'/getStationCodeList');
			response.success(function(data, status, headers, config){
				if(data.result==="success"){
					$scope.stationCodeList = data.list;
					$scope.successToast(data.result);
				}else{
					console.log(data);
				}
				$scope.getStateForStn();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}*/
		
		 $scope.saveStationCode =  function(stn){
				console.log("enter into saveStationCode----->"+stn.stnCode);
				$scope.stationName = stn.stnName;
				$scope.stationCode = stn.stnCode;
				$('div#stationCodeDB').dialog('close');
				$scope.stationCodeDBFlag = true;
				$scope.filterStnCode = "";
		}
		 
		 $('#stnPin').keypress(function(key) {
		       if(key.charCode < 48 || key.charCode > 57)
		           return false;
		   });

		 $('#stnPin').keypress(function(e) {
		       if (this.value.length == 6) {
		           e.preventDefault();
		       }
		   });
		 
		 $('#stnName').keypress(function(e) {
		       if (this.value.length == 40) {
		           e.preventDefault();
		       }
		});
		 
		 $('#district').keypress(function(e) {
		       if (this.value.length == 40) {
		           e.preventDefault();
		       }
		});
	
		 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
			 $scope.getStationDetails();
		 }else if($scope.logoutStatus === true){
			 $location.path("/");
		 }else{
			 console.log("****************");
		 } 
		
	
	
}]);

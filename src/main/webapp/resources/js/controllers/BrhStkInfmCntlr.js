'use strict';

var app = angular.module('application');

app.controller('BrhStkInfmCntlr',['$scope','$location','$http','AuthorisationService',
                                 function($scope,$location,$http,AuthorisationService){

	console.log("BrhStkInfmCntlr Started");
	
	$scope.branchDBFlag = true;
	$scope.stkEnqTable = true;
	$scope.stkEnqDBFlag = true;
	$scope.stkEnqDBUsedFlag = true;
	$scope.stkEnqUsedUnusedDBFlag = true;
	$scope.showUsed = true;
	$scope.mstrStkTable = true;
	$scope.findByStockTable = true;
	$scope.masterStkDBFlag = true;

	// CNMT Detail
	$scope.totalCnmt = [];
	$scope.recCnmt = [];
	$scope.unRecCnmt = [];
	$scope.usedCnmt = [];
	$scope.usedCnmtWithCount = [];
	$scope.unUsedCnmt = [];
	
	//	Challan Detail
	$scope.totalChln = [];
	$scope.recChln = [];
	$scope.unRecChln = [];
	$scope.usedChln = [];
	$scope.usedChlnWithCount = [];
	$scope.unUsedChln = [];
	
	// SEDR Detail
	$scope.totalSedr = [];
	$scope.recSedr = [];	
	$scope.unRecSedr = [];	
	$scope.usedSedr = [];
	$scope.usedSedrWithCount = [];
	$scope.unUsedSedr = [];	
	
	//	This is used to take which used one information is being accessed (CNMT , Challn, SEDR)
	$scope.usedType = "";
	$scope.usedUnusedList = [];
	
	// Enquriy List that is used to show Start No
	$scope.enqList = [];
	
	// Find By Stock 
	$scope.stock = {};
	$scope.byStkList = [];
	
	// Master Stock	
	$scope.masterStkDBFlag = true;
	$scope.masterStk = [];
	$scope.masterDt = [];
	
	// Missing CNMT/Challan/SEDR
	$scope.missStn = {};
	$scope.cnmtMissFlag = false;
	$scope.chlnMissFlag = false;
	$scope.sedrMissFlag = false;
	
	$scope.findMissingStn = function(StkEnqForm){
		console.log("Enter into findMisingStn()");
		$("#findMissingStn").attr("disabled", "disabled");
		var response = $http.post($scope.projectName + '/getMissingReport', $scope.stock);
		response.success(function(data, status, headers, config){
			$("#findMissingStn").removeAttr("disabled");
			console.log("Response from /getMissingReport = "+data.result);
			console.log("Response from /getMissingReport = "+data.cnmt);
			console.log("Response from /getMissingReport = "+data.chln);
			console.log("Response from /getMissingReport = "+data.sedr);
			if(data.result === "success"){
				$scope.alertToast(data.result);
				if(angular.isUndefined($scope.stock.stnType) || $scope.stock.stnType === null || $scope.stock.stnType === ''){
					$scope.missStn.cnmt = data.cnmt;
					$scope.missStn.chln = data.chln;
					$scope.missStn.sedr = data.sedr;
					$scope.cnmtMissFlag = true;
					$scope.chlnMissFlag = true;
					$scope.sedrMissFlag = true;
				}else{
					if($scope.stock.stnType === 'cnmt'){
						$scope.missStn.cnmt = data.cnmt;
						$scope.missStn.chln = [];
						$scope.missStn.sedr = [];
						$scope.cnmtMissFlag = true;
						$scope.chlnMissFlag = false;
						$scope.sedrMissFlag = false;
					}else if($scope.stock.stnType === 'chln'){
						$scope.missStn.chln = data.chln;
						$scope.missStn.cnmt = [];
						$scope.missStn.sedr = [];
						$scope.cnmtMissFlag = false;
						$scope.chlnMissFlag = true;
						$scope.sedrMissFlag = false;
					}else if($scope.stock.stnType === 'sedr'){
						$scope.missStn.sedr = data.sedr;
						$scope.missStn.cnmt = [];
						$scope.missStn.chln = [];
						$scope.cnmtMissFlag = false;
						$scope.chlnMissFlag = false;
						$scope.sedrMissFlag = true;
					}
				}
				
			}else{
				$scope.alertToast(data.msg);
				$scope.missStn.cnmt = [];
				$scope.missStn.chln = [];
				$scope.missStn.sedr = [];
				$scope.cnmtMissFlag = false;
				$scope.chlnMissFlag = false;
				$scope.sedrMissFlag = false;
			}
		});
		response.error(function(data, status, headers, config){
			$("#findMissingStn").removeAttr("disabled");
			$scope.alertToast("Server connection error !");
			console.log("Error in hitting /getMissingReport");
		});	
		console.log("Exit from findMisingStn()");
	}
	
	$scope.downLoadMissingStn = function(){
		var blob = new Blob([document.getElementById('missingTable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });		
        saveAs(blob, "MissingReport.xls");
	}
		
	$scope.getBranchData = function(){
		console.log("Enter into getBranchData() ");
		var response = $http.post($scope.projectName+'/getBranchDataForCustomer');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log(data.result);
				$scope.branchList = data.list;				
			}else{
				console.log(data.result);
			}
		});
		response.error(function(data, status, headers,config) {
			console.log("Error.....");
		});
		console.log("Exit from getBranchData() ");
	}
	
	$scope.saveBranch = function(branch){
		$scope.branchFaCode = branch.branchFaCode;
		$scope.branchCode = branch.branchCode;
		$scope.stock.branchCode = branch.branchCode;
		$('div#branchDB').dialog('close');
	}
	
	$scope.findByBranch = function(){
		$scope.stkEnqTable = true;
		$scope.mstrStkTable = true;
		$scope.findByStockTable = true;
		console.log("Enter into findByBranch()");
		$("#stkEnqFormSubmit").attr("disabled", "disabled");
		var response = $http.post($scope.projectName+'/getBrhStkDetail', $scope.branchCode);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$("#stkEnqFormSubmit").removeAttr("disabled");
				for(var i=0; i<data.stkEnqList.length; i++){
					$scope.alertToast(data.result);
					console.log(data.result);					
					// Set CNMT Detail						
					$scope.totalCnmt = data.stkEnqList[i].totalCnmt;
					$scope.recCnmt = data.stkEnqList[i].recCnmt;
					$scope.unRecCnmt = data.stkEnqList[i].unRecCnmt;
					$scope.usedCnmt = data.stkEnqList[i].usedCnmt;
					$scope.unUsedCnmt = data.stkEnqList[i].unUsedCnmt;
					
					//	Set Challan Detail						
					$scope.totalChln = data.stkEnqList[i].totalChln;
					$scope.recChln = data.stkEnqList[i].recChln;
					$scope.unRecChln = data.stkEnqList[i].unRecChln;
					$scope.usedChln = data.stkEnqList[i].usedChln;
					$scope.unUsedChln = data.stkEnqList[i].unUsedChln;
					
					// Set SEDR
					$scope.totalSedr = data.stkEnqList[i].totalSedr;								
					$scope.recSedr = data.stkEnqList[i].recSedr;
					$scope.unRecSedr = data.stkEnqList[i].unRecSedr;
					$scope.usedSedr = data.stkEnqList[i].usedSedr;
					$scope.unUsedSedr = data.stkEnqList[i].unUsedSedr;
					
					$scope.stkEnqTable = false;
				}					
			}else{
				console.log(data.msg);
				$scope.alertToast(data.msg);
				$("#stkEnqFormSubmit").removeAttr("disabled");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("Error for hitting /getBrhStkDetial URL");
			$("#stkEnqFormSubmit").removeAttr("disabled");
		});
		console.log("Exit from findByBranch()");
	}
	
	$scope.findByStk = function(stkForm){
		$scope.stkEnqTable = true;
		$scope.mstrStkTable = true;
		$scope.findByStockTable = true;
		console.log("Enter into findByStk()");
		var startNo = $.trim($("#startNo").val());
		var endNo = $.trim($("#endNo").val());
		var typeName = $.trim($("#typeName").val());
		if(startNo == "" || endNo == "" || typeName == ""){
			if(startNo == "")
				$scope.alertToast("Enter start no. !");
			if(endNo == "")
				$scope.alertToast("Enter end no. !");
			if(typeName == "")
				$scope.alertToast("Select type !");
		}else{
			$("#findByStk").attr("disabled", "disabled");
			var response = $http.post($scope.projectName + '/findByStk', $scope.stock);
			response.success(function(data, status, headers, config){
				$("#findByStk").removeAttr("disabled");
				if(data.result === "success"){
					$scope.alertToast(data.result);
					$scope.byStkList = data.byStkList;
					$scope.findByStockTable = false;
				}else{
					$scope.alertToast(data.msg);
					console.log(data.msg);
					$scope.findByStockTable = true;
				}
			});
			response.error(function(data, status, headers, config){
				$("#findByStk").removeAttr("disabled");
				console.log("Error in hitting /findByStk");
			});
		}	
		console.log("Exit from findByStk()");
	}
	
	$scope.findByMstrStock = function(){
		console.log("Enter into findByMstrStock()");
		$scope.stkEnqTableTable = true;
		$scope.findByStockTable = true;
		$scope.mstrStkTable = true;		
		$("#findByMstrStock").attr("disabled", "disabled");
		var response = $http.post($scope.projectName+'/getMstrStock');
		response.success(function(data, status, headers, config){
			$("#findByMstrStock").removeAttr("disabled");
			if(data.result === "success"){
				$scope.alertToast(data.result);
				$scope.mstrStkTable = false;
				$scope.masterStk = data.masterStk;	
			}else{
				$scope.alertToast(data.msg);
				console.log(data.result);
			}
		});
		response.error(function(data, status, headers, config){
			$("#findByMstrStock").removeAttr("disabled");
			console.log("Error in hitting /getMstrStock");
		});
		console.log("Exit from findByMstrStock()");
	}
	
	
	$scope.submitStkEnq = function(StkEnqForm, findType){
		console.log("Enter into submitStkEnq() ");		
		if(findType === "findByBranch"){
			if(StkEnqForm.branchFaCode.$invalid){
				$scope.alertToast("Select Branch !");
				return;
			}
			$scope.findByBranch();
		}else if(findType === "findByStock"){
			
		}else if(findType === "findByMstrStock"){
			$scope.findByMstrStock();
		}
		console.log("Exit from submitStkEnq() ");
	}
	
	

	$scope.showDetail = function(list, title){
		$scope.enqList = [];		
		if(title === "Used CNMT" || title === "Used Challan" || title === "Used SEDR"){
			
			if(title === "Used CNMT")
				$scope.usedType = "cnmt";
			else if(title === "Used Challan")
				$scope.usedType = "chln";
			else if(title === "Used SEDR")
				$scope.usedType = "sedr";
			
			$scope.enqList = list;
			$scope.stkEnqDBUsedFlag = false;
			$scope.stkEnqDBFlag = true;
			$('div#stkEnqUsedDB').dialog({
				autoOpen: false,
				modal:true,
				title: title,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});
			$('div#stkEnqUsedDB').dialog('open');
		}else{
			$scope.enqList = list;
			$scope.stkEnqDBFlag = false;
			$scope.stkEnqDBUsedFlag = true;
			$('div#stkEnqDB').dialog({
				autoOpen: false,
				modal:true,
				title: title,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});
			$('div#stkEnqDB').dialog('open');
		}
	}
	
	$scope.fetchDetail = function(startNo, usedUnused){
		console.log("Enter into fetchDetail() : StartNo = "+startNo+" : UsedUnused = "+usedUnused);
		var initParam = {
				"startNo"		:	startNo,
				"usedUnused"	:	usedUnused,
				"type"			:	$scope.usedType,
				"branchCode"	:	$scope.branchCode
		}		
		console.log("Going to hit /fetchStnaryDetail URL  : Data = "+initParam);
		var response = $http.post($scope.projectName+'/fetchStnaryDetail', initParam);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log(data.result);
				$scope.alertToast(data.result);
				$scope.usedUnusedList = [];
				$scope.usedUnusedList = data.usedUnusedList;
				$scope.stkEnqUsedUnusedDBFlag = false;
				$('div#stkEnqUsedUnusedDB').dialog({
					autoOpen: false,
					modal:true,
					title: $scope.usedType,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					resizable: false,
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy') ;
						$(this).hide();
					}
				});
				$('div#stkEnqUsedUnusedDB').dialog('open');				
			}else{
				console.log(data.result);
				$scope.alertToast(data.msg);
			}
		});
		response.error(function(data, status, headers, config){
			$scope.alertToast("Error !");
			console.log("Error.......");
		});
		console.log("Exit from fetchDetail() ");
	}
	
	$scope.showMaster = function(mstr){
		$scope.masterDt = mstr;
		$scope.masterStkDBFlag = false;
		$('div#masterStkDB').dialog({
			autoOpen: false,
			modal:true,
			title: $scope.usedType,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});
		$('div#masterStkDB').dialog('open');	
	}
	
	$scope.showDt = function(list){
		$scope.usedUnusedList = [];
		$scope.usedUnusedList = list
		$scope.stkEnqUsedUnusedDBFlag = false;
		$('div#stkEnqUsedUnusedDB').dialog({
			autoOpen: false,
			modal:true,
			title: $scope.usedType,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});
		$('div#stkEnqUsedUnusedDB').dialog('open');	
	}
	
	$scope.OpenBranchDB = function(){
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Branch",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});
		$('div#branchDB').dialog('open');
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getBranchData();
	}else if($scope.logoutStatus === true){	
		$location.path("/");
	}else{
	
		console.log("****************");
	}	
	console.log("BrhStkInfmCntlr Ended");
}]);
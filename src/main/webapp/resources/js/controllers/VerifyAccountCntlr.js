
'use strict';

var app = angular.module('application');

app.controller('VerifyAccountCntlr',['$scope','$location','$http','$filter','$window','FileUploadService',
                                 function($scope,$location,$http,$filter,$window,FileUploadService){


	
	$scope.brkList=[];
	$scope.ownList=[];
	$scope.brokerDB=false;
	$scope.ownerDB=false;
	
	$scope.getBrokerList=function(){
		console.log("getBrokerList()");
		$scope.ownerDB=false;
		var response = $http.post($scope.projectName+'/getBrokerFrBnkVerify');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.brkList = data.brkList;
				$scope.brokerDB=true;
				console.log("broker List="+$scope.brkList);
			}else{
				$scope.alertToast("Detail not found");
				$scope.alertToast(data.msg);
				console.log(data.exc);
			}
		});
		response.error(function(data, status, headers,config) {
			$scope.errorToast(data);
		});
	};
	
	
	
	$scope.showImage=function(brk){
		
		var img={
				"imageType":"BRK",
				"id" : brk.brkId,
				"ownBrkImgType": "CHQ"
		};
		
		
		var response = $http.post($scope.projectName+'/isDownloadImg', img);
		response.success(function(data, status, headers, config){
			console.log("Result = "+data.result);
			if(data.result === "success"){
				$scope.alertToast(data.result);
				var form = document.createElement("form");
		        form.method = "POST";
		        form.action = $scope.projectName + '/downloadImg';
		        form.target = "_blank";
		        document.body.appendChild(form);
		        form.submit();
			}else
				$scope.alertToast(data.msg);
		});
		response.error(function(data){
			console.log("Error in hitting /downloadImg");
		});
		
		
	}
	
	
	$scope.validBnkDet=function(brk){
		
		var response = $http.post($scope.projectName+'/validBnkDet', brk.brkId);
		response.success(function(data, status, headers, config){
			console.log("Result = "+data.result);
			if(data.result === "success"){
				$scope.alertToast(data.result);
				$scope.brkList=[];
				$scope.getBrokerList();
			}else
				$scope.alertToast("Retry");
		});
		response.error(function(data){
			console.log("Error in hitting /validBnkDet");
		});
		
	}
	
	
	$scope.invalidBnkDet=function(brk){
		
		var response = $http.post($scope.projectName+'/invalidBnkDet', brk.brkId);
		response.success(function(data, status, headers, config){
			console.log("Result = "+data.result);
			if(data.result === "success"){
				$scope.alertToast(data.result);
				$scope.brkList=[];
				$scope.getBrokerList();
			}else
				$scope.alertToast("Retry");
		});
		response.error(function(data){
			console.log("Error in hitting /invalidBnkDet");
		});
		
	}
	
	
	/* ----------------------------------------------------------------*/
	
	$scope.getOwnerList=function(){
		console.log("getOwnerList()");
		$scope.brokerDB=false;
		var response = $http.post($scope.projectName+'/getOwnerFrBnkVerify');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.ownList = data.ownList;
				$scope.ownerDB=true;
				console.log("owner List="+$scope.ownList);
			}else{
				$scope.alertToast("Detail not found");
				$scope.alertToast(data.msg);
				console.log(data.exc);
			}
		});
		response.error(function(data, status, headers,config) {
			$scope.errorToast(data);
		});
	};
	
	
	
	$scope.showOwnImage=function(own){
		
		var img={
				"imageType":"own",
				"id" : own.ownId,
				"ownBrkImgType": "CHQ"
		};
		
		
		var response = $http.post($scope.projectName+'/isDownloadImg', img);
		response.success(function(data, status, headers, config){
			console.log("Result = "+data.result);
			if(data.result === "success"){
				$scope.alertToast(data.result);
				var form = document.createElement("form");
		        form.method = "POST";
		        form.action = $scope.projectName + '/downloadImg';
		        form.target = "_blank";
		        document.body.appendChild(form);
		        form.submit();
			}else
				$scope.alertToast(data.msg);
		});
		response.error(function(data){
			console.log("Error in hitting /downloadImg");
		});
		
		
	}
	
	
	$scope.validOwnBnkDet=function(own){
		
		var response = $http.post($scope.projectName+'/validOwnBnkDet', own.ownId);
		response.success(function(data, status, headers, config){
			console.log("Result = "+data.result);
			if(data.result === "success"){
				$scope.alertToast(data.result);
				$scope.ownList=[];
				$scope.getOwnerList();
			}else
				$scope.alertToast("Retry");
		});
		response.error(function(data){
			console.log("Error in hitting /validOwnBnkDet");
		});
		
	}
	
	
	$scope.invalidOwnBnkDet=function(own){
		
		var response = $http.post($scope.projectName+'/invalidOwnBnkDet', own.ownId);
		response.success(function(data, status, headers, config){
			console.log("Result = "+data.result);
			if(data.result === "success"){
				$scope.alertToast(data.result);
				$scope.ownList=[];
				$scope.getOwnerList();
			}else
				$scope.alertToast("Retry");
		});
		response.error(function(data){
			console.log("Error in hitting /invalidOwnBnkDet");
		});
		
	}
	
	
	
}]);
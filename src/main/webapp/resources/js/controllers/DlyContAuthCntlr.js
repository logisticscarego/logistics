'use strict';

var app = angular.module('application');

app.controller('DlyContAuthCntlr',['$scope','$location','$http',
                                 function($scope,$location,$http){
	
	$scope.custList = [];
	$scope.stList = [];
	$scope.finalStList = [];
	$scope.dca = {};
	$scope.custCodeDBFlag = true;
	$scope.stCodeDBFlag = true;
	$scope.saveDCAFlag = true;
	
	$scope.getCustCode = function(){
		console.log("enter into getCustCode function");
		var response = $http.post($scope.projectName+'/getDCACustCode');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.custList = data.list;
				$scope.getStateList();
			}else{
				console.log(data.result);
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.getStateList = function(){
		console.log("enter into getStateList function");
		var response = $http.post($scope.projectName+'/getDCAStateList');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.stList = data.list;
				console.log("from server====>"+$scope.stList[0].stnName);
			}else{
				console.log(data.result);
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	$scope.openCustCodeDB = function(){
		console.log("enter into openCustCodeDB function");
		$scope.custCodeDBFlag = false;
		$('div#custCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Customer Code",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.bankCodeDBFlag = true;
			}
		});
		$('div#custCodeDB').dialog('open');
	}
	
	$scope.saveCustCode = function(cust){
		console.log("enter into saveCustCode function");
		$('div#custCodeDB').dialog('close');
		var response = $http.post($scope.projectName+'/chkExstFrCust',cust.custCode);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.dcaCustCodeTemp = cust.custFaCode;
				$scope.dca.dcaCustCode = cust.custCode
			}else{
				console.log(data.result);
				$scope.alertToast("You already authorized "+cust.custFaCode+" , but does not create DailyContract");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.setIsActive = function(){
		console.log("enter into setIsActiv function");
		console.log("dca.dcaIsActive = "+$scope.dca.dcaIsActive);
		/*if($scope.dca.dcaIsActive === "YES"){
			$scope.dca.dcaIsActive = true
		}else{
			$scope.dca.dcaIsActive = false;
		}*/
	}
	
	$scope.selectSt = function(){
		console.log("enter into selectSt function");
		$('#addStId').removeAttr("disabled");
		$('#allStId').attr("disabled","disabled");
		$scope.finalStList = [];
	}
	
	$scope.allSt = function(){
		console.log("enter into allSt function");
		$scope.finalStList = [];
		$('#allStId').removeAttr("disabled");
		$('#addStId').attr("disabled","disabled");
	}
	
	$scope.OpenStList = function(){
		console.log("enter into OpenStList function");
		$scope.stCodeDBFlag = false;
		$('div#stCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Station",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.bankCodeDBFlag = true;
			}
		});
		$('div#stCodeDB').dialog('open');
	}
	
	
	$scope.saveStCode = function(st){
		console.log("enter into saveStCode function");
		$scope.finalStList.push(st);
		$('div#stCodeDB').dialog('close');
	}
	
	
	$scope.removeSt = function(index){
		console.log("enter into removeSt function");
		$scope.finalStList.splice(index,1);
	}
	
	
	$scope.allowAllSt = function(){
		console.log("enter into allowAllSt function");
		$scope.finalStList = $scope.stList;
	}
	
	
	$scope.submit = function(dlyContAuthForm){
		console.log("enter into submit function---->"+dlyContAuthForm.$invalid);
		if(dlyContAuthForm.$invalid){
			$scope.alertToast("please fill correct form");
		}else{
			$scope.dca.dcaStatList = [];
			console.log("length of $scope.finalStList = "+$scope.finalStList.length);
			for(var i=0;i<$scope.finalStList.length;i++){
				$scope.dca.dcaStatList.push($scope.finalStList[i].stnCode);
			}
			
			$scope.saveDCAFlag = false;
			$('div#saveDcaDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Station",
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
					$scope.bankCodeDBFlag = true;
				}
			});
			$('div#saveDcaDB').dialog('open');
		}
	}
	
	
	$scope.saveDCA = function(){
		console.log("enter into saveDCA function");
		$('div#saveDcaDB').dialog('close');
		if($scope.dca.dcaIsActive === "YES"){
			$scope.dca.dcaIsActive = true
		}else{
			$scope.dca.dcaIsActive = false;
		}
		
		var response = $http.post($scope.projectName+'/saveDCA',$scope.dca);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.alertToast(data.result);
				$scope.dcaCustCodeTemp = "";
				$scope.dca = {};
				$scope.finalStList = [];
			}else{
				$scope.alertToast(data.result);
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	$scope.closeDCA = function(){
		console.log("enter into closeDCA function");
		$('div#saveDcaDB').dialog('close');
	}
	
	
	$scope.getCustCode();
	
}]);
'use strict';

var app = angular.module('application');

app.controller('ReIssueCntlr',['$scope','$location','$http','AuthorisationService',
                                 function($scope,$location,$http,AuthorisationService){

	console.log("ReIssueCntlr Started");
	
	$scope.cnmtFlag = false;
	$scope.chlnFlag = true;
	$scope.sedrFlag = true;
	$scope.chqFlag = true;
	
	$scope.stnry = {};
	//$scope.ownBrkList = [];
	
	$scope.changeFlag = function(){
		if($("#stnryTypeId").val() === "cnmt"){
			$scope.cnmtFlag = false;
			$scope.chlnFlag = true;
			$scope.sedrFlag = true;
			$scope.chqFlag = true;
		}else if($("#stnryTypeId").val() === "chln"){
			$scope.cnmtFlag = true;
			$scope.chlnFlag = false;
			$scope.sedrFlag = true;
			$scope.chqFlag = true;
		}else if($("#stnryTypeId").val() === "sedr"){
			$scope.cnmtFlag = true;
			$scope.chlnFlag = true;
			$scope.sedrFlag = false;
			$scope.chqFlag = true;
		}else if($("#stnryTypeId").val() === "chq"){
			$scope.cnmtFlag = true;
			$scope.chlnFlag = true;
			$scope.sedrFlag = true;
			$scope.chqFlag = false;
		}else{
			$scope.cnmtFlag = true;
			$scope.chlnFlag = true;
			$scope.sedrFlag = true;
			$scope.chqFlag = true;
		}
	}
	
//	$scope.getList = function(keyCode){				
//		if(keyCode === 8)
//			return;
//		
//		if($scope.image.imageType === "chln" || $scope.image.imageType === "lry")
//			return;	
//		
//		if(parseInt($scope.image.ownBrkNC.length) < 2){
//			return;
//		}else{
//			$scope.getOwnBrkList();
//		}		
//	}
	
//	$scope.getOwnBrkList = function(){
//		var response = $http.post($scope.projectName + '/getOwnBrkForImgRead', $scope.image);
//		response.success(function(data, status, headers, config){
//			console.log(data.result);
//			$scope.alertToast(data.result);
//			if(data.result === "success"){
//				$scope.ownBrkList = data.list;
//				$scope.image.ownBrkNC = "";
//				$scope.ownBrkDBFlag = false;
//				$('div#ownBrkDB').dialog({
//					autoOpen: false,
//					modal:true,
//					title: $scope.image.imageType,
//					show: UDShow,
//					hide: UDHide,
//					position: UDPos,
//					resizable: false,
//					draggable: true,
//					close: function(event, ui) { 
//						$(this).dialog('destroy') ;
//						$(this).hide();
//					}
//				});
//				$('div#ownBrkDB').dialog('open');
//			}else{
//				$scope.ownBrkList = [];
//				$scope.alertToast(data.msg);
//			}
//		});
//		response.error(function(data, status, headers, config){
//			console.log("Error in hitting URL");
//		});
//
//	}
	
//	$scope.saveOwnBrk = function(obj){
//		$scope.image.id = obj.id;
//		$scope.image.ownBrkNC = obj.name;
//		$('div#ownBrkDB').dialog('close');
//	}
	
	$scope.stnryReIssue = function(stnryForm){
		console.log("Enter into stnryReIssue() : FormValid = "+stnryForm.$valid);
		if(stnryForm.$invalid){
			if(stnryForm.stnryType.$invalid)
				$scope.alertToast("Select stnry type");
		}else{
			var stnryData={
				"type" :$scope.stnry.stationryType,
				"cnmtNo":$scope.stnry.cnmtNo,
				"chlnNo":$scope.stnry.chlnCode,
				"chqNo":$scope.stnry.chqNo,
				"bnkNo":$scope.stnry.bnkNo,
				"sedrNo":$scope.stnry.sedrNo
			};
			
			var response = $http.post($scope.projectName+'/reIssueStnry', stnryData);
			response.success(function(data, status, headers, config){
				console.log("Result = "+data.result);
				if(data.result === "success"){
					$scope.alertToast(data.result);
				}else
					$scope.alertToast(data.msg);
			});
			response.error(function(data){
				console.log("Error in hitting /Server");
			});
		}		
		console.log("Exit from stnryReIssue()");
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){		
	}else if($scope.logoutStatus === true){	
		$location.path("/");
	}else{	
		console.log("****************");
	}	
	console.log("ReIsssueCntlr Ended");
	
}]);
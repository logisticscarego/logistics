'use strict';

var app = angular.module("application");

app.controller('AlwLryPymntCntlr',['$scope','$rootScope','$http','$location','DataFactoryService','$window','$filter','AuthorisationService',
                              function($scope,$rootScope,$http,$location,DataFactoryService,$window,$filter,AuthorisationService){
	console.log("AlwLryPymntCntlr");
	
	
	$scope.allowFrLhpv=function(){
		
		var chlnMap={
				"chlnNo" : $scope.chlnNo,
				"alwType" : $scope.alwType
		};
		var response = $http.post($scope.projectName+'/alwChlnFrLryPmnt',chlnMap);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.alertToast("success");
			}else{
				console.log("error ");
				$scope.alertToast(data.msg);
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
}]);
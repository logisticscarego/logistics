'use strict';

var app = angular.module('application');

app.controller('DisallowAdvanceCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.selectCust=[];
	$scope.selectBranch=[];
	$scope.selectStation=[];
	$scope.custRightList=[];
	$scope.branchRightList=[];
	$scope.stnRightList=[];
	
	$scope.viewCustRightsFlag = true;
	$scope.viewBranchRightsFlag = true;
	$scope.viewStationRightsFlag = true;
	
	var idxCust;
	var idxBranch;
	var idxStation;
 
	$scope.checkAdvanceType = function(advncType){
		console.log("Advance type is----"+advncType);
		if(advncType === "customer" || advncType === "consignor" || advncType === "consignee"){
			if($scope.custRightList){
				$scope.viewCustRightsFlag = false;
			}else{
				$scope.viewCustRightsFlag = true;
			}
			$scope.viewBranchRightsFlag = true;
			$scope.viewStationRightsFlag = true;
		}else if(advncType === "branch"){
			
			if($scope.branchRightList){
				$scope.viewBranchRightsFlag = false;
			}else{
				$scope.viewBranchRightsFlag = true;
			}
			$scope.viewCustRightsFlag = true;
			$scope.viewStationRightsFlag = true;
		}else if(advncType === "fromStn" || advncType === "toStn"){
			
			if($scope.stnRightList){
				$scope.viewStationRightsFlag = false;
			}else{
				$scope.viewStationRightsFlag = true;
			}
			$scope.viewCustRightsFlag = true;
			$scope.viewBranchRightsFlag = true;
		}
	}
	
	$scope.getCustRightsData = function(){
		console.log("Entered into getCustRightsData function------");
		var response = $http.post($scope.projectName+'/getCustRightsData');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.custRightList = data.list;	
				for(var i=0;i<$scope.custRightList.length;i++){
					$scope.custRightList[i].crhAdvValidDt =  $filter('date')($scope.custRightList[i].crhAdvValidDt, 'MM/dd/yyyy hh:mm:ss');
					$scope.custRightList[i].creationTS =  $filter('date')($scope.custRightList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
				}
				$scope.successToast(data.result);
				$scope.getBranchRightsData();
			}else if(data.result === "error"){
				$scope.custRightList = data.list;
				$scope.viewCustRightsFlag = true;
				$scope.alertToast("No customer rights....");
				$scope.getBranchRightsData();
			}	
		});
		response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		});
	}
	
	$scope.getBranchRightsData = function(){
		console.log("Entered into getBranchRightsData function------");
		var response = $http.post($scope.projectName+'/getBranchRightsData');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.branchRightList = data.list;	
				for(var i=0;i<$scope.branchRightList.length;i++){
					$scope.branchRightList[i].brhAdvValidDt =  $filter('date')($scope.branchRightList[i].brhAdvValidDt, 'MM/dd/yyyy hh:mm:ss');
					$scope.branchRightList[i].creationTS =  $filter('date')($scope.branchRightList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
				}
				$scope.successToast(data.result);
				$scope.getStationRightsData();
			}else if(data.result === "error"){
				$scope.branchRightList = data.list;	
				$scope.viewBranchRightsFlag = true;
				$scope.alertToast("No branch rights ....");
				$scope.getStationRightsData();
			}	
		});
		response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		});
	}
	
	$scope.getStationRightsData = function(){
		console.log("Entered into getStationRightsData function------");
		var response = $http.post($scope.projectName+'/getStationRightsData');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.stnRightList = data.list;
				for(var i=0;i<$scope.stnRightList.length;i++){
					$scope.stnRightList[i].strhAdvValidDt =  $filter('date')($scope.stnRightList[i].strhAdvValidDt, 'MM/dd/yyyy hh:mm:ss');
					$scope.stnRightList[i].creationTS =  $filter('date')($scope.stnRightList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
				}
				$scope.successToast(data.result);
			}else if(data.result === "error"){
				$scope.stnRightList = data.list;
				$scope.viewStationRightsFlag = true;
				$scope.alertToast("No station rights...")
			}	
		});
		response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		});
	}
	
	$scope.toggleSelectCust = function toggleSelectCust(crhId) {
		idxCust = $scope.selectCust.indexOf(crhId);
		   
		   // is currently selected
		   if (idxCust > -1) {
			   $scope.selectCust.splice(idxCust, 1);
		   }
		   // is newly selected
		   else {
			   $scope.selectCust.push(crhId);
		   }
	   }	  

	$scope.submitCustIsAllowNo = function(){
		console.log("Entered into submitCustIsAllowNo function--- "+$scope.selectCust.length);
		if(!$scope.selectCust.length){
			 $scope.alertToast("You have not selected anything");
		 }else{
			 	var response = $http.post($scope.projectName+'/submitCustIsAllowNo',$scope.selectCust.toString());
			 	response.success(function(data, status, headers, config){
			 		$scope.successToast(data.result);
			 		$scope.getCustRightsData();
                });     
			 	response.error(function(data, status, headers, config) {
			 		$scope.errorToast(data.result);
			 	});
		 }	 	
	}
	
	$scope.toggleSelectBranch = function toggleSelectBranch(brhId) {
		idxBranch = $scope.selectBranch.indexOf(brhId);
		   
		   // is currently selected
		   if (idxBranch > -1) {
			   $scope.selectBranch.splice(idxBranch, 1);
		   }
		   // is newly selected
		   else {
			   $scope.selectBranch.push(brhId);
		   }
	   }	  

	$scope.submitBranchIsAllowNo = function(){
		console.log("Entered into submitBranchIsAllowNo function--- "+$scope.selectBranch.length);
		if(!$scope.selectBranch.length){
			 $scope.alertToast("You have not selected anything");
		 }else{
			 	var response = $http.post($scope.projectName+'/submitBranchIsAllowNo',$scope.selectBranch.toString());
			 	response.success(function(data, status, headers, config){
			 		$scope.successToast(data.result);
			 		$scope.getBranchRightsData();
                });     
			 	response.error(function(data, status, headers, config) {
			 		$scope.errorToast(data.result);
			 	});
		 }	 	
	}
	
	$scope.toggleSelectStation = function toggleSelectStation(strhId) {
		idxStation = $scope.selectStation.indexOf(strhId);
		   
		   // is currently selected
		   if (idxStation > -1) {
			   $scope.selectStation.splice(idxStation, 1);
		   }
		   // is newly selected
		   else {
			   $scope.selectStation.push(strhId);
		   }
	   }	  

	$scope.submitStationIsAllowNo = function(){
		console.log("Entered into submitStationIsAllowNo function--- "+$scope.selectStation.length);
		if(!$scope.selectStation.length){
			 $scope.alertToast("You have not selected anything");
		 }else{
			 	var response = $http.post($scope.projectName+'/submitStationIsAllowNo',$scope.selectStation.toString());
			 	response.success(function(data, status, headers, config){
			 		$scope.successToast(data.result);
			 		$scope.getStationRightsData();
                });     
			 	response.error(function(data, status, headers, config) {
			 		$scope.errorToast(data.result);
			 	});
		 }	 	
	}
	
	if($scope.adminLogin === true || $scope.superAdminLogin === true){
		$scope.getCustRightsData();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
}]);
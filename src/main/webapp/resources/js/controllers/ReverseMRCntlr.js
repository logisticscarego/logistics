'use strict';

var app = angular.module('application');

app.controller('ReverseMRNCntlr',['$scope','$location','$http','$filter','$sce',
                              function($scope,$location,$http,$filter,$sce){
	
	$scope.selActCustFlag = true;
	$scope.selBrhFlag = true;
	$scope.selOAFlag = true;
	$scope.selCustFlag = true;
	$scope.isFormSubmitted = false;
	$scope.mr = {};
	$scope.branch = {};
	$scope.actCust = {};
	$scope.customer = {};
	$scope.custList = [];
	$scope.onAccList = [];
	$scope.brhList = [];
	
	$scope.getRvMrDetail = function(){
		console.log("enter into $scope.getRvMrDetail function");
		var response = $http.post($scope.projectName+'/getRvMrDetail');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.csDt = data.cssDt;
				  $scope.cssDt = data.cssDt;
				  
				  $scope.custList = data.list;
				  $scope.brhList = data.brhList;
				  if($scope.currentBranch === "CRPO"){
					  $("#csDtId").attr("readonly", false);
				  }else{
					  $("#csDtId").attr("readonly", true);
				  }
			  }else{
				  $scope.errorToast("SERVER ERROR");
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });	
	}
	
	
	$scope.chngDt = function(){
		console.log("enter into chngDt function");
		if($scope.csDt > $scope.cssDt){
			$scope.alertToast("You can't enter greater than "+$scope.pdmr.mrDate+" date");
			$scope.csDt = $scope.cssDt; 
		}
	}
	
	
	
	$scope.selectActCust = function(){
		console.log("enter into selectActCust function");
		$scope.selActCustFlag = false;
		$('div#selActCustId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Customer",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selActCustFlag = true;
		    }
			});
		
		$('div#selActCustId').dialog('open');	
	}
	
	
	$scope.saveActCust = function(cust){
		console.log("enter into saveActCust funciton");
		$('div#selActCustId').dialog('close');	
		$scope.actCust = cust;
		$scope.onAccList = [];
	}
	
	
	
	$scope.selectBrh = function(){
		console.log("enter into selectBrh function");
		$scope.selBrhFlag = false;
		$('div#selBrhId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select MR Branch",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selBrhFlag = true;
		    }
			});
		
		$('div#selBrhId').dialog('open');	
	}
	
	
	$scope.saveBranch = function(brh){
		console.log("enter into saveBranch function");
		$('div#selBrhId').dialog('close');	
		$scope.branch = brh;
		$scope.onAccList = [];
		
		if(angular.isUndefined($scope.customer) || angular.isUndefined($scope.actCust.custId)){
			$scope.branch = {};
			$scope.onAccList = [];
			$scope.alertToast("please select a customer");
		}else{
			var map = {
					"custId" : $scope.actCust.custId,
					"brhId"  : $scope.branch.branchId
			};
			
			var response = $http.post($scope.projectName+'/getOnAccFrRev',map);
			  response.success(function(data, status, headers, config){
				  if(data.result === "success"){
					  $scope.onAccList = data.list;
				  }else{
					  $scope.alertToast("There is no On A/C MR for "+$scope.actCust.custname+" in "+$scope.branch.branchName+" branch");
				  }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
			   });	
		}
	}
	
	
	$scope.selectOnAcc = function(){
		console.log("enter into selectOnAcc function");
		$scope.selOAFlag = false;
		$('div#selOAId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select On A/C MR",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selOAFlag = true;
		    }
			});
		
		$('div#selOAId').dialog('open');	
	}
	
	
	
	$scope.saveMR = function(OA){
		console.log("enter into saveMR function");
		$scope.onAcc = OA;
		$('div#selOAId').dialog('close');
	}
	
	
	$scope.selectCust = function(){
		console.log("enter into selectCust function");
		$scope.selCustFlag = false;
		$('div#selCustId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Reverse Customer",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selCustFlag = true;
		    }
			});
		
		$('div#selCustId').dialog('open');	
	}
	
	
	$scope.saveCust = function(cust){
		console.log("enter into saveCust function");
		$('div#selCustId').dialog('close');	 
		$scope.customer = cust;
	}
	
	
	$scope.revMrSubmit = function(revMrForm){
		console.log("enter into revMrSubmit function = "+revMrForm.$invalid);
		if(revMrForm.$invalid){
			$scope.isFormSubmitted = true;
		}else{
			$('#saveId').attr("disabled","disabled");
			var map = {
				"prevCustId" : $scope.actCust.custId,
				"prevBrhId"  : $scope.branch.branchId,
				"prevMrNo"   : $scope.onAcc.mrNo,
				"newCustId"  : $scope.customer.custId,
				"newDesc"    : $scope.mrDesc,
				"csDt"       : $scope.csDt
			};
			var response = $http.post($scope.projectName+'/revMrSubmit',map);
			  response.success(function(data, status, headers, config){
				  if(data.result === "success"){
					  $scope.alertToast("Successfully generate MR No. = "+data.mrNo);
					  $scope.csDt = $scope.cssDt;
					  $scope.onAccList = [];
					  $scope.mr = {};
					  $scope.branch = {};
					  $scope.actCust = {};
					  $scope.customer = {};
					  $scope.onAcc = {};
					  $scope.desc = "";
					  $scope.mrDesc = "";
					  $scope.isFormSubmitted = false;
					  $('#saveId').removeAttr("disabled");
				  }else{
					  $scope.alertToast(data.msg);
					  $('#saveId').removeAttr("disabled");
				  }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
			   });	
		}
	}
	
	
	
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
			$scope.getRvMrDetail();
		 }else if($scope.logoutStatus === true){
			 $location.path("/");
		 }else{
			 console.log("****************");
		 } 
	}]);
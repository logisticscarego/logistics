'use strict';

var app = angular.module('application');

app.controller('RelAnnextureRptCntlr', ['$scope','$location','$http','$filter','$sce','$window','$log',
                              function($scope,$location,$http,$filter,$sce,$window,$log){
	console.log("start RelAnnextureCntlr.......");
	
	$scope.relCustDBFlag = true;	
	$scope.customerGroupId='';
	$scope.lodingFlag = false;
	$scope.relCustDB = function(){
		console.log("enter into relCustomer Dialog Box");
		$scope.relCustDBFlag = false;
		$('div#custDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Group",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.relCustDBFlag = true;
		    }
			});
		
		$('div#custDB').dialog('open');	
	};
	
	
	$scope.getCustGroup=function(){
		console.log("enter in to getCustGroup....");
		
		var response=$http.post($scope.projectName+'/getcustGroup');
		response.success(function(data,status,headers,config){
			if(data.result === "success"){
				$scope.custGroupList=data.custGroupList;
			}else{
				$scope.alertToast("problem in CustomerGroup");
			}
		});
		
	};	
	
	$scope.saveCustGroup=function(custGroup){
		console.log("save CustGroup..");
		console.log(custGroup.groupId);
		$scope.customerGroupId=custGroup.groupId;
		$('div#custDB').dialog('close');
	};
	
	
	$scope.changeCommaNumber=function(numberRate){
		//console.log(numberRate.replace(',', ''));
		return numberRate.replace(',', '');
	};
	
		
	$scope.fromValidDate=function(fromDate){
		console.log("fromDt="+fromDate);
		var date = $filter('date')(new Date, "yyyy-MM-dd");
		console.log("nowDt="+date);
		if(fromDate>date){
			$scope.fromDate=date;
		}
	};
	
	$scope.toValidDate=function(toDate){
		
		console.log("toDt="+toDate);
		var date = $filter('date')(new Date, "yyyy-MM-dd");
		console.log("nowDt="+date);
		if(toDate>date){
			$scope.toDate=date;
		}
	};
	
	
	$scope.relBlPrintSubmit=function(relBlPrintForm){
		console.log("submit form " +relBlPrintForm.$invalid);
		console.log(relBlPrintForm.fromDateName);
		console.log(relBlPrintForm.toDateName);
		console.log(relBlPrintForm.gruoupName);
		if(relBlPrintForm.$invalid){
			$scope.alertToast("please fill the correct form");
		}else if(relBlPrintForm.fromDateName.$invalid){
			$scope.alertToast("please select valid fromDate");
		}else if(relBlPrintForm.toDateName.$invalid){
			$scope.alertToast("please select valid toDate");
		}else if($scope.customerGroupId === ""){
			$scope.alertToast("please select customer Group");
		}else{
			console.log("formDt"+$scope.fromDate);
			console.log("toDt"+$scope.toDate);
			console.log("$scope.customerGroupId"+$scope.customerGroupId);
			
			var req = { 
						"fromDate"  : $scope.fromDate,
						  "toDate" 	: $scope.toDate,
					 "custGroupId" 	: $scope.customerGroupId
			}; 
			$scope.lodingFlag = true;
			var response = $http.post($scope.projectName+'/relAnnexPrintSubmit',req);
			 response.success(function(data, status, headers, config){
					console.log(data.result);
				  if(data.result === "success"){
					 $scope.blList = data.list;
					 $scope.lodingFlag = false;
					 $scope.alertToast(data.result);
					 console.log("size of $scope.blList = "+$scope.blList.length);
					 if($scope.blList.length > 0){
						 	$scope.relBlShow = true;
					 }
					 $log.info($scope.blList);
				  }else{
					  $scope.lodingFlag = false;
					  $scope.alertToast("some problem");
				  }
			 }); 
		};
			 };	
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getCustGroup();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
	
	$scope.downloadAnx = function(){
		console.log("enter into downloadAnx function");
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Annexure.xls");
	};
	
}]);
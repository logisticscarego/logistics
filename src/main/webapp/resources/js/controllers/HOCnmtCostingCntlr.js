'use strict';

var app = angular.module('application');

app.controller('HOCnmtCostingCntlr', ['$scope','$location','$http','$filter','$q',
		function($scope, $location, $http, $filter, $q) {

			console.log("HOCnmtCostingCntlr started");

			$scope.brhOpenFlag = true;
			$scope.isConsolidate = false;
			$scope.isBranch = false;
			$scope.custOpenFlag = true;
			
			
			$scope.cnmtCoting = {};
			$scope.cnmtCostingList = [];
			$scope.smry = {};
			$scope.brhName = "";
			$scope.custName = "";
			
			

			$scope.getBankCsInfo = function() {
				console.log("enter into getBankCsInfo function");
				var response = $http.post($scope.projectName + '/getAllBranchesList');
				response.success(function(data, status, headers, config) {
					if (data.result === "success") {
						$scope.brhList = data.branchCodeList;
					} else {
						$scope.brhList = [];
						$scope.alertToast("Server error");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
			}

			$scope.consolidateRB = function() {
				console.log("consolidateRB()");
				$scope.branch = {};
				$scope.isConsolidate = true;
				$scope.isBranch = false;
				$scope.isConBrh = "con"
				$('#branchNameId').attr("disabled", "disabled");
			}

			$scope.branchRB = function() {
				console.log("branchRB()");
				$scope.isConsolidate = false;
				$scope.isBranch = true;
				$scope.isConBrh = "brh"
				$('#branchNameId').removeAttr("disabled");
			}

			$scope.openBranchDB = function() {
				$scope.getBankCsInfo();
				console.log("enter into openBranchDB function");
				$scope.brhOpenFlag = false;
				$('div#brhOpenDB').dialog({
					autoOpen : false,
					modal : true,
					resizable : false,
					title : "Bank Code",
					show : UDShow,
					hide : UDHide,
					position : UDPos,
					draggable : true,
					close : function(event, ui) {
						$(this).dialog('destroy');
						$(this).hide();
						$scope.brhOpenFlag = true;
					}
				});

				$('div#brhOpenDB').dialog('open');
			}

			$scope.saveBrhCode = function(brh) {
				console.log("enter into saveBrhCode function");
				$scope.brhName = brh.branchName;
				$scope.cnmtCosting.brhId = brh.branchId;
				$('div#brhOpenDB').dialog('close');
			}

			$scope.openCustDB = function() {
				$scope.getCustomer();
				console.log("enter into openCustDB  function");
				$scope.custOpenFlag = false;
				$('div#custOpenDB').dialog({
					autoOpen : false,
					modal : true,
					resizable : false,
					title : "Customer Code",
					show : UDShow,
					hide : UDHide,
					position : UDPos,
					draggable : true,
					close : function(event, ui) {
						$(this).dialog('destroy');
						$(this).hide();
						$scope.brhOpenFlag = true;
					}
				});

				$('div#custOpenDB').dialog('open');
			}

			$scope.saveCustCode = function(cust) {
				console.log("enter into saveBrhCode function");
				$scope.custName = cust.custName;
				$scope.cnmtCosting.custId = cust.custId;
				$('div#custOpenDB').dialog('close');
			}

			$scope.getCustomer = function() {
				console.log("getCustomer------>Kamal");
				var response = $http.get($scope.projectName	+ '/getCustomerList');
				response.success(function(data, status, headers, config) {
					if (data.result === "success") {
						$scope.customerList = data.list;
					} else {
						$scope.alertToast("you don't have any customer");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
			}

			$scope.getCostingReport = function() {
				console.log("Enter into getCostingReport() ");
				
				$("#printXlsId").attr("disabled", "disabled");
				
				var response = $http.post($scope.projectName + '/getHoCostingReportXLSX', $scope.cnmtCosting);
				response.success(function(data, status, headers, config){
					$("#printXlsId").removeAttr("disabled");
					console.log("Result = "+data.result);
					if(data.result === "success"){
						$scope.cnmtCostingList = data.cnmtCostingList;
						$scope.smry = data.smry;
					}else{
						$scope.alertToast(data.msg);
					}
				});
				response.error(function(data, status, headers, config){
					console.log("Error in hitting /getHoCostingReportXLSX");
					$("#printXlsId").removeAttr("disabled");
				});
				
				console.log("Exit from getCostingReport() ");
			}
			
			$scope.downloadReport = function(){
				console.log("downloadReport()");				
				var blob = new Blob([document.getElementById('cnmtCostingTable').innerHTML], {
		            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
		        });		
		        saveAs(blob, "CnmtCosting.xls");
		        
			}

			$scope.costing = function() {

				var detail = {
					"branch" : $scope.branch.branchId,
					"frmDt" : $scope.frmDt,
					"toDt" : $scope.toDt,
					"isBranch" : $scope.isBranch,
					"isConsolidate" : $scope.isConsolidate,
					"custCode" : $scope.custCode
				}
				$('#submitId').attr("disabled", "disabled");

				var response = $http.post($scope.projectName + '/cnmtCosting',
						detail);
				response.success(function(data, status, headers, config) {
					$('#submitId').removeAttr("disabled");
					if (data.result === "success") {
						$scope.alertToast(" success");
						console.log(data);
					} else {
						$scope.alertToast(data.result);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
					$('#submitId').removeAttr("disabled");
				});
			}

		} ]);
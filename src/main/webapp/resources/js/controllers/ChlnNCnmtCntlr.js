'use strict';

var app = angular.module('application');

app.controller('ChlnNCnmtCntlr',['$scope','$location','$http','$filter','$window', 'FileUploadService',
                                 function($scope,$location,$http,$filter,$window,FileUploadService){

	console.log("ChlnNCnmtCntlr Started");
	
	$scope.chlnNoList = [];
	
	$scope.chlnDBFlag = true;
	
	$scope.cnmtCodeIdByChlnList = [];
	$scope.cnmtCodeIdList = [];
	$scope.cnmt = {};
	$scope.cnmtAddIdList = [];
	$scope.cnmtRemoveIdList = [];
	
	$scope.showTableFlag = false;
	$scope.cnmtDBFlag = true;
	
	$scope.getChlnNoList = function(){
		console.log(" entered into getChlnNoList------>");
		var response = $http.post($scope.projectName+'/getChlnNoList');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.chlnNoList = data.chlnNoList;
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.openChlnDB = function(){
		console.log("Entered into openChlnDB");
		$scope.chlnDBFlag = false;
		$('div#chlnDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Challan No",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.chlnDBFlag = true;
			}
		});

		$('div#chlnDB').dialog('open');
	}
	
	$scope.selectChlnCode = function(chlnCode){
		$('div#chlnDB').dialog('close');
		console.log("selectChlnCode()");
		$scope.chlnCode = chlnCode;
		
		var res = $http.post($scope.projectName+'/getCnmtByChln', chlnCode);
		res.success(function(data, status, headers, config) {
			if (data.result==="success") {
				console.log("getCnmtByChln result: "+data.result);
				$scope.cnmtCodeIdByChlnList = data.cnmtCodeIdByChlnList;
				$scope.cnmtCodeIdList = data.cnmtCodeIdList;
				$scope.chlnId = data.chlnId;
				$scope.showTableFlag = true;
			} else {
				console.log("getCnmtByChln result: "+data.result);
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in response getCnmtByChln: "+data);
		});
	}
	
	$scope.openCnmtDB = function(){
		console.log("Entered into openCnmtDB");
		$scope.cnmtDBFlag = false;
		$('div#cnmtDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Cnmt No",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.cnmtDBFlag = true;
			}
		});

		$('div#cnmtDB').dialog('open');
	}
	
	$scope.addCnmt = function(cnmt) {
		var tempFlag = false;
		if ($scope.cnmtCodeIdByChlnList.length>0) {
			for ( var i = 0; i < $scope.cnmtCodeIdByChlnList.length; i++) {
				if ($scope.cnmtCodeIdByChlnList[i].cnmtCode === cnmt.cnmtCode) {
					tempFlag = true;
				}
			}
		}
		
		if (tempFlag) {
			$scope.alertToast("Already in the list");
		} else {
			if ($scope.cnmtRemoveIdList.length > 0) {
				for ( var i = 0; i < $scope.cnmtRemoveIdList.length; i++) {
					if ($scope.cnmtRemoveIdList[i]===cnmt.cnmtId) {
						$scope.cnmtRemoveIdList.splice(i, 1);
					}
				}
				$scope.cnmtCodeIdByChlnList.push(cnmt);
				$scope.cnmtAddIdList.push(cnmt.cnmtId);
				$('div#cnmtDB').dialog('close');
			} else {
				$scope.cnmtCodeIdByChlnList.push(cnmt);
				$scope.cnmtAddIdList.push(cnmt.cnmtId);
				$('div#cnmtDB').dialog('close');
			}
			
		}
	}
	
	$scope.removeCnmt = function(index, cnmt) {
		console.log("removeCnmt()");
		if ($scope.cnmtAddIdList.length > 0) {
			for ( var i = 0; i < $scope.cnmtAddIdList.length; i++) {
				if ($scope.cnmtAddIdList[i]===cnmt.cnmtId) {
					$scope.cnmtAddIdList.splice(i, 1);
				}
			}
			$scope.cnmtRemoveIdList.push(cnmt.cnmtId);
			$scope.cnmtCodeIdByChlnList.splice(index, 1);
		} else {
			$scope.cnmtRemoveIdList.push(cnmt.cnmtId);
			$scope.cnmtCodeIdByChlnList.splice(index, 1);
		}
	}
	
	$scope.submitChlnNCnmt = function(chlnForm) {
		console.log("submitChlnNCnmt");
		var chlnNCnmtService = {
				"chlnId" : $scope.chlnId,
				"cnmtAddIdList" : $scope.cnmtAddIdList,
				"cnmtRemoveIdList" : $scope.cnmtRemoveIdList
		}
		
		var res = $http.post($scope.projectName+'/submitChlnNCnmt', chlnNCnmtService);
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("submitChlnNCnmt result: "+data.result);
				//$scope.chlnNoList = [];
				$scope.alertToast(data.result);
				$scope.cnmtCodeIdByChlnList = [];
				$scope.cnmtCodeIdList = [];
				$scope.cnmt = {};
				$scope.cnmtAddIdList = [];
				$scope.cnmtRemoveIdList = [];
				$scope.chlnCode = "";
				$scope.chlnId = 0;
				
				$scope.showTableFlag = false;
				$scope.chlnDBFlag = true;
				$scope.cnmtDBFlag = true;
			} else {
				console.log("submitChlnNCnmt result: "+data.result);
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in response submitChlnNCnmt : "+data);
		});
	}
	
	if($scope.adminLogin === true || $scope.superAdminLogin === true){
		$scope.getChlnNoList();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("ChlnNCnmtCntlr Ended");
}]);
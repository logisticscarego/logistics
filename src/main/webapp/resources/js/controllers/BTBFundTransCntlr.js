var app = angular.module('application');

app.controller('BTBFundTransCntlr',['$scope','$location','$http','$filter',
                                      function($scope,$location,$http,$filter){
	
	$scope.btbFServ = {};
	$scope.frBankList = [];
	$scope.brList = [];
	$scope.toBnkList = [];
	$scope.toEmpList=[];
	$scope.toPetroList=[];
	var toBnkListTemp=[];
	var toPetroListTemp=[];
	var toEmpListTemp=[];
	var hideRadioEmpButton=0;
	var hideRadioBankButton=0;
	var hideRadioPetroButton=0;
	$scope.toBrh = {};
	$scope.toBrhList = [];
	$scope.changeRadioBank=false;
	$scope.changeRadioEmp=false;
	$scope.changeRadioPetro=false;
	$scope.bankDBFlag = true;
	$scope.toEmpDBFlag=true;
	$scope.toPetroDBFlag=true;
	$scope.fTransDBFlag = true;
	$scope.branchDBFlag = true;
	$scope.toBnkDBFlag = true;
	$scope.chequeDBFlag = true;
	$scope.loadingFlag = false;
	$scope.isBankLabel=false;
	$scope.isBankText=false;
	$scope.radioShow=false;
	$scope.empRadioBut=true;
	$scope.petroRadioBut=true;
	$scope.bnkRadioBut=true;
	$scope.isFormSubmitted = false;
	$scope.isFundFormSubmitted = false;
	
    var	max = 15;
    $scope.maxLimit = 0;
    
	 $('#regContFromWt').keypress(function(e) {
     	if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length === max)){
             e.preventDefault();
         } 
     });
	 
	 
	 $('#toAmtId').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	 });
	 
	 
	 $scope.getFYear = function(){
			var response = $http.get($scope.projectName+'/getCurntFYear');
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					console.log("Entered Date in current financial year");
					console.log(data.fYear.fyFrmDt);
					console.log(data.fYear.fyToDt);
					$scope.fyFrmDt=data.fYear.fyFrmDt;
					$scope.fyToDt=data.fYear.fyToDt;
				}else{
					$scope.alertToast("There is no any current Financial year");
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.alertToast("There is some problem in fetching date");
			});
		}();
	 
	 
	 $scope.checkCsIsClose=function(date){
		 $scope.checkDt=date;
		 console.log($scope.checkDt);
		 if($scope.btbFServ.fundDt<$scope.fyFrmDt || $scope.btbFServ.fundDt>$scope.fyToDt){
			 $scope.btbFServ.fundDt="";
			 $scope.checkDt="";
				$scope.alertToast("Date should be in current Financial year");
				return;
			}
		  var req={
				  "checkDt" : $scope.checkDt
		  };
		var response = $http.post($scope.projectName+'/checkCsCloseDate',req);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.alertToast("You can Transfer.");
				$('#fTransId').removeAttr("disabled");
			}else{
				$scope.alertToast("CS has not closed.");
				$('#fTransId').attr("disabled","disabled");
			}
		});
		
		response.error(function(data, status, headers, config){
			$('#fTransId').attr("disabled","disabled");
			console.log(data);
		});	
		
	 };
	 
	 
	/**************  select bank button************************/ 
	 
	    $scope.bank=function(){
	    	console.log("bank button..");
	       	$scope.isBankLabel=true;
	    	$scope.isEmployeLabel=false;
	    	$scope.isPetroLabel=false;
	    	$scope.isBankText=true;
	    	$scope.toBnkList=toBnkListTemp;
	    	$scope.toBrh.toBkName=null;
	    	console.log("toBnkList="+$scope.toBnkList);
	    }
	
	  /**************  select employee button ************************/   
	    $scope.employee=function(){
	    	console.log("employee button..");
	       	$scope.isEmployeLabel=true;
	       	$scope.isPetroLabel=false;
	    	$scope.isBankLabel=false;
	    	$scope.isBankText=true;
	    	$scope.toBnkList=[];
	    	$scope.toEmpList=toEmpListTemp;
	    	$scope.toBrh.toBkName=null;
	    };
	    
	    $scope.petroCard=function(){
	    	console.log("petro button..");
	    	$scope.isPetroLabel=true;
	       	$scope.isEmployeLabel=false;
	    	$scope.isBankLabel=false;
	    	$scope.isBankText=true;
	    	$scope.toBnkList=[];
	    	$scope.toEmpList=[];
	    	$scope.toPetroList=toPetroListTemp;
	    	$scope.toBrh.toBkName=null;
	    };
	
	$scope.getFrBank = function(){
		console.log("enter into getFrBank function");
		$('#fTransId').attr("disabled","disabled");
		var response = $http.post($scope.projectName+'/getFrBank');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.branch = data.branch;
				$scope.btbFServ.frBrCode = $scope.branch.branchFaCode;
				$scope.frBankList = data.list;
				$scope.brList = data.brList;
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});	
	}
	
	
	$scope.openBankDB = function(){
		console.log("enter into openBankDB function");
		$scope.bankDBFlag = false;
		$('div#bankDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title : "Select Bank",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.bankDBFlag = true;
		    }
			});
		$('div#bankDB').dialog('open');
	}
	
	
	$scope.selectBank = function(bank){
		console.log("enter into selectBank funciton");
		$('div#bankDB').dialog('close');
		$scope.btbFServ.frBkName = bank.bnkName;
		$scope.btbFServ.frBkAccNo = bank.bnkAcNo;
		$scope.btbFServ.frBkCode = bank.bnkFaCode;
		$scope.btbFServ.frBkAmt = bank.bnkBalanceAmt;
		
		var chqDet = {
				"bankCode" : $scope.btbFServ.frBkCode,
				"CType"    : $scope.btbFServ.btbChqType
		};
		
		var response = $http.post($scope.projectName+'/getChqFrBTB',chqDet);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.chqList = data.list;
				console.log("size of $scope.chqList = "+$scope.chqList.length);
			}else{
				console.log("Error in fetching data from getChqFrBTB");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});	
	}
	
	$scope.openFTrans = function(){
		console.log("enter into openFTrans function");
		if(angular.isUndefined($scope.btbFServ.frBkName) || $scope.btbFServ.frBkName === "" || $scope.btbFServ.frBkName === null){
			$scope.alertToast("Please select a bank");
		}else{
			$scope.fTransDBFlag = false;
			$('div#fTransDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title : "Fund Transfer From "+$scope.bkName+" Bank",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.bankDBFlag = true;
			    }
				});
			$('div#fTransDB').dialog('open');
		} 
	}
	
	
	$scope.openBranchDB = function(){
		console.log("enter into openBranchDB funciton");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title : "Select Branch",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.branchDBFlag = true;
		    }
			});
		$('div#branchDB').dialog('open');
	}
	
	
	$scope.selectBrh = function(brh){
		console.log("enter into selectBrh function");
		$scope.toBnkList = [];
		$scope.toEmpList=[];
		$scope.toPetroList=[];
		$scope.toBrh.toBrName = brh.branchName;
		$scope.toBrh.toBrCode = brh.branchFaCode;
		$('div#branchDB').dialog('close');
		var response = $http.post($scope.projectName+'/getToBnkDet',brh.branchId);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
			//	$scope.toBnkList = data.list;
				toBnkListTemp=data.list;
				toEmpListTemp=data.empList;
				toPetroListTemp=data.petroList;
				$scope.radioShow=true;
				$scope.changeRadioBank=false;
				$scope.changeRadioEmp=false;
				$scope.changeRadioPetro=false;
				$scope.toBrh.toBkName=null;
				console.log("bnkList="+toBnkListTemp);
				console.log("PetroList="+toPetroListTemp);
				console.log("empList="+toEmpListTemp);
			}else{
				console.log("Error in bringing data");
				$scope.alertToast("There is no Bank for "+$scope.toBrh.toBrName+" Branch");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});	
	};
	
	
	$scope.openToBankDB = function(){
		console.log("enter into openToBankDB funciton");
		
		var titleStmt,divId;
		if($scope.isBankLabel===true){
			$scope.toBnkDBFlag = false;
			titleStmt="Select Bank";
			divId='div#toBnkDB';
			
		}else if($scope.isEmployeLabel===true){
			titleStmt="Select Employee"
			divId="div#toEmpDB";
			$scope.toEmpDBFlag=false;
		}else if($scope.isPetroLabel===true){
			titleStmt="Select Petro"
				divId="div#toPetroDB";
				$scope.toPetroDBFlag=false;
			}
		$(divId).dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title :titleStmt,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        if($scope.isEmployeLabel===true){
		        $scope.toBnkDBFlag = true;
		        }else if($scope.isBankLabel===true){
		        	$scope.toEmpDBFlag=true;
		        }else if($scope.isPetroLabel===true){
		        	$scope.toPetroDBFlag=true;
		        }
		    }
			});
		$(divId).dialog('open');
	}
	
	
	$scope.selectToBank = function(bank){
		console.log("enter into selectToBank funciton");
		$scope.toBrh.toBkCode = bank.bnkFaCode;
		$scope.toBrh.toBkAccNo = bank.bnkAcNo
		$scope.toBrh.toBkName = bank.bnkName;
		$scope.toBrh.toBkVouchType="bank";
		 hideRadioEmpButton=0;
		 hideRadioPetroButton=0;
		 hideRadioBankButton=1;
		console.log("bank="+$scope.toBrhList.length);
		$('div#toBnkDB').dialog('close');
	}
	
	$scope.selectToEmp= function(emp){
		console.log("select employee...");
		$scope.toBrh.toBkCode=emp.empFaCode;
		$scope.toBrh.toBkName=emp.empName;
		$scope.toBrh.toBkVouchType="contra";
		$scope.toBrh.toBcode=emp.branchCode;
		 hideRadioEmpButton=1;
		 hideRadioPetroButton=0;
		hideRadioBankButton=0;
		console.log(emp);
         console.log("emp="+$scope.toBrhList.length);
		$('div#toEmpDB').dialog('close');
	}
	
	$scope.selectToPetro= function(petro){
		console.log("select Petro...");
		$scope.toBrh.toBkCode=petro.cardFaCode;
		$scope.toBrh.toBkName=petro.cardNo;
		$scope.toBrh.toBkVouchType="contra";
		$scope.toBrh.toBcode=petro.branch;
		 hideRadioEmpButton=0;
		 hideRadioPetroButton=1;
		hideRadioBankButton=0;
		console.log(petro);
         console.log("petro="+$scope.toBrhList.length);
		$('div#toPetroDB').dialog('close');
	}
	
	$scope.saveToBrDet = function(FundTranForm){
		console.log("enter into saveToBrDet funciton");
		if (FundTranForm.$invalid) {
			$scope.isFundFormSubmitted = true;
			return false;
		} else
			$scope.isFundFormSubmitted = false;  
		/*var amt = 0;
		if($scope.toBrhList.length > 0){
			console.log("$scope.toBrhList.length == "+$scope.toBrhList.length);
			for(var i=0;i<$scope.toBrhList.length.length;i++){
				amt = amt + $scope.toBrhList[i].toAmt;
			}
			amt = amt + $scope.toBrh.toAmt;
		}else{
			amt = $scope.toBrh.toAmt;
		}
		console.log("amt === "+amt);*/
		//if($scope.btbFServ.frBkAmt > amt){
		$scope.toBrh.toAmt = parseFloat($scope.toBrh.toAmt); 
		if($scope.toBrh.toAmt <= 4000000){
			console.log("$scope.maxLimit = "+$scope.maxLimit);
			console.log("$scope.toBrh.toAmt = "+$scope.toBrh.toAmt);
			console.log($scope.toBrh.toBkVouchType);
			if(angular.isUndefined($scope.toBrh.toBkCode)){
				$scope.alertToast("Please select a bank code");
			}else{
				if(!angular.isUndefined($scope.toBrh.frChqNo)){
					if($scope.maxLimit >= $scope.toBrh.toAmt){
						//var response = $http.post($scope.projectName+'/saveToBrDet',$scope.toBrh);
						//response.success(function(data, status, headers, config){
						//	if(data.result === "success"){
							//	$scope.fetchToBrh();
								$scope.toBrhList.push($scope.toBrh);
								$scope.toBrh = {};
								$scope.bnkRadioBut=true;
								$scope.empRadioBut=true;
								$scope.petroRadioBut=true;
								$('div#fTransDB').dialog('close');
//							}else{
//								console.log("Error in bringing data from saveToBrDet");
//							}
					//	});
//						response.error(function(data, status, headers, config){
//							console.log(data);
//						});	
					}else{
						$scope.toBrh.toAmt = 0;
						$scope.alertToast("Your Cheque amount maximum limit is "+$scope.maxLimit);
					}
				}else{
//					var response = $http.post($scope.projectName+'/saveToBrDet',$scope.toBrh);
//					response.success(function(data, status, headers, config){
//						if(data.result === "success"){
							//$scope.fetchToBrh();
					$scope.toBrhList.push($scope.toBrh);
							$scope.toBrh = {};
							$('div#fTransDB').dialog('close');
//						}else{
//							console.log("Error in bringing data from saveToBrDet");
//						}
//					});
//					response.error(function(data, status, headers, config){
//						console.log(data);
//					});	
				}
				
				$scope.clear();
			}
		
		}else{
			
			//$scope.alertToast($scope.btbFServ.frBkName+" of "+$scope.currentBranch+" branch doesn't have sufficient balance");
			$scope.alertToast("You can't transfer more than 4000000");
		}
	}
	
	
	$scope.clear = function(){
		console.log("enter into clear fucntion");
		$scope.filterBranch = "";
		$scope.toBankFilter = "";
		$scope.fileterEmployee = "";
		if(hideRadioEmpButton===1){
			$scope.bnkRadioBut=false;
			$scope.petroRadioBut=false;
			console.log("hideRadioEmpButton="+hideRadioEmpButton);
		}
		if(hideRadioBankButton===1){
			console.log("hideRadioEmpButton="+hideRadioBankButton);
			$scope.empRadioBut=false;
			$scope.petroRadioBut=false;
		}
		
		if(hideRadioPetroButton===1){
			console.log("hideRadioPetroButton="+hideRadioPetroButton);
			$scope.empRadioBut=false;
			$scope.bnkRadioBut=false;
		}
	}
	
	
	$scope.removeFT = function(index,toBrh){
		console.log("enter into removeFT function = "+index);
//
//		var remData = {
//				"index" : index
//		};
//		var response = $http.post($scope.projectName+'/removeFT',remData);
//		response.success(function(data, status, headers, config){
		//	if(data.result === "success"){
		$scope.toBrhList.splice(index,1);
				//$scope.fetchToBrh();
//			}else{
//				console.log("Error in bringing data from removeFT");
//			}
//		});
//		response.error(function(data, status, headers, config){
//			console.log(data);
//		});	
	}
	
	
//	$scope.fetchToBrh = function(){
//		console.log("enter into fetchToBrh function");
//		var response = $http.post($scope.projectName+'/fetchToBrh');
//		response.success(function(data, status, headers, config){
//			if(data.result === "success"){
//				$scope.toBrhList = data.list;
//			}else{
//				console.log("Error in bringing data from removeFT");
//			}
//		});
//		response.error(function(data, status, headers, config){
//			console.log(data);
//		});	
//	} 
	
	
	$scope.openChqDB = function(){
		console.log("enter into openChqDB function");
		
		$scope.chequeDBFlag = false;
		$('div#chequeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title : "Select Cheque",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.chequeDBFlag = true;
		    }
			});
		$('div#chequeDB').dialog('open');
	}
	
	
	$scope.saveChqNo = function(chq){
		console.log("enter into saveChqNo function = "+chq.chqLChqNo);
		var duplicate = false;
		$scope.maxLimit = 0;
		if($scope.toBrhList.length > 0){
			for(var i=0;i<$scope.toBrhList.length;i++){
				if($scope.toBrhList[i].frChqNo === chq.chqLChqNo){
					duplicate = true;
					break;
				}
			}
		}
		
		if(duplicate === true){
			$scope.alertToast("You already selected "+chq.chqLChqNo+" cheque no.");
			$scope.maxLimit = 0;
		}else{
			$scope.toBrh.frChqNo = chq.chqLChqNo;
			$scope.maxLimit = chq.chqLChqMaxLt;
		}
		console.log("$scope.toBrh.frChqNo = "+$scope.toBrh.frChqNo);
		$('div#chequeDB').dialog('close');
	}
	
	
	
	
	$scope.FundTraSubmit = function(btbFTForm){
		console.log("enter into FundTraSubmit function ="+btbFTForm.$invalid);
		
		if(btbFTForm.$invalid){
			$scope.isFormSubmitted = true;
		}else{
			$scope.isFormSubmitted = false;
			$('#saveId').attr("disabled","disabled");
			$scope.loadingFlag = true;
			$scope.btbFServ.toFundList=$scope.toBrhList;
			var response = $http.post($scope.projectName+'/FundTraSubmit',$scope.btbFServ);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.loadingFlag = false;
					$('#saveId').removeAttr("disabled");
					$scope.btbFServ = {};
					$scope.frBankList = [];
					$scope.brList = [];
					$scope.toBnkList = [];
					$scope.toBrh = {};
					$scope.toBrhList = [];
					
					$scope.getFrBank();
					$scope.btbFServ.btbChqType = 'C';
					$scope.alertToast(data.result);
					$scope.filterBank = "";
					$scope.radioShow = false;
					$scope.bnkRadioBut = true;
					$scope.empRadioBut = true;
					$scope.petroRadioBut = true;
					$scope.isBankLabel = false;
					$scope.isEmployeLabel = false;
					$scope.isBankText=false;
				}else{
					$scope.loadingFlag = false;
					$scope.alertToast("transfer fail");
					console.log("Error in bringing data from FundTraSubmit");
				}
			});
			response.error(function(data, status, headers, config){
				$scope.loadingFlag = false;
				console.log(data);
			});	
		}
	}
	
	
	
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getFrBank();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);
'use strict';

var app = angular.module('application');

app.controller('NewRentDetailCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	console.log("NewRentDetailCntlr Started");
	$scope.branchList=[];
	$scope.branchDBFlag = true;
	$scope.branchDBFlag2 = true;
	$scope.rentDBFlag=true;
	$scope.updateForm=true;
	$scope.rentAddress=true;
	$scope.bankDBFlag=true;
//	$scope.rentDetail={};
	
	$scope.openBranchDB = function(){
		console.log("Entered into openBranchDB");
		var response = $http.post($scope.projectName+'/getAllBranchesList');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getAllBranchesList Success");
				$scope.branchList = data.branchCodeList;
				$scope.branchDBFlag = false;
				$('div#branchDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Branch",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
						$scope.branchDBFlag = true;
					}
				});

				$('div#branchDB').dialog('open');
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
	}
	$scope.openBranchDB2 = function(){
		console.log("Entered into openBranchDB");
		var response = $http.post($scope.projectName+'/getAllBranchesList');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getAllBranchesList Success");
				$scope.branchList = data.branchCodeList;
				$scope.branchDBFlag2 = false;
				$('div#branchDB2').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Branch",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
						$scope.branchDBFlag = true;
					}
				});

				$('div#branchDB2').dialog('open');
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
	}
	$scope.saveBranch =  function(branch){
		console.log("saveBranch");
		console.log("enter into branchName----->"+branch.branchName);
		console.log("enter into branchFaCode----->"+branch.branchFaCode);
		$scope.branch = branch;
		$scope.selBranchId=branch.branchId;
		$('div#branchDB').dialog('close');
		var branchId=branch.branchId;
		console.log("Branch Id   "+branchId);
		var response = $http.post($scope.projectName+'/getRentByBranchId',branchId);
		response.success(function(data){
			if(data.result==="success"){
				$scope.rentMstrList=data.rentList;
				if($scope.rentMstrList.length>0){
					$scope.rentDBFlag=false;
					$('div#rentDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						position: UDPos,
						show: UDShow,
						hide: UDHide,
						title: "RentMstr",
						draggable: true,
						close: function(event, ui) { 
							$(this).dialog('destroy');
							$(this).hide();
							$scope.rentDBFlag=true;
						}
					});

					$('div#rentDB').dialog('open');
				}
			}else{
				$scope.alertToast(data.result);
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getRentMstr Error: "+data);
			$scope.alertToast("Error")
		});	
	}
	
	
	$scope.saveBranch2 =  function(branch){
		console.log("saveBranch");
		console.log("enter into branchName----->"+branch.branchName);
		console.log("enter into branchFaCode----->"+branch.branchFaCode);
		$scope.branch2 = branch;
		$scope.selBranchId=branch.branchId;
		$('div#branchDB2').dialog('close');
		var branchId=branch.branchId;
		console.log("Branch Id   "+branchId);
		var response = $http.post($scope.projectName+'/getBankNCAI',branchId);
		response.success(function(data){
			if(data.result==="success"){
				$scope.bankMstrList=data.bankNCAIList;
				if($scope.bankMstrList.length>0){
					$scope.bankDBFlag=false;
					$('div#bankDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						position: UDPos,
						show: UDShow,
						hide: UDHide,
						title: "Bank Master",
						draggable: true,
						close: function(event, ui) { 
							$(this).dialog('destroy');
							$(this).hide();
							$scope.rentDBFlag=true;
						}
					});

					$('div#bankDB').dialog('open');
				}
			}else{
				$scope.alertToast(data.result);
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getRentMstr Error: "+data);
			$scope.alertToast("Error")
		});	
	}
	
	$scope.saveBankMstr=function(bankMstr){
		$('div#bankDB').dialog('close');
		$scope.bankDBFlag=true;
		$scope.updateForm=false;
		$scope.bankMstr=bankMstr;
		console.log(bankMstr);
		
		var response = $http.post($scope.projectName+'/getRentMstrList');
		response.success(function(data){
			if(data.result==="success"){
			$scope.myRent=data.rentList;
			console.log("Herehhhhhhhhhhhhhhh");
			console.log($scope.myRent); 
				
			}else{
				$scope.alertToast(data.result);
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getRentMstr Error: "+data);
			$scope.alertToast("Error")
		});	
		
	}
	
	$scope.saveRentMstr=function(rentMstr){
		$('div#rentDB').dialog('close');
		$scope.rentDBFlag=true;
		$scope.updateForm=false;
		$scope.rent=rentMstr;
		console.log(rentMstr)
		
	}
	
	$scope.openBankMstr=function(){
		console.log($scope.branch2);
		if(angular.isUndefined($scope.branch2)){
			$scope.alertToast("Select Branch ");
		}else{
			var branchId=$scope.branch2.branchId;
			var response = $http.post($scope.projectName+'/getBankNCAI',branchId);
			response.success(function(data){
				if(data.result==="success"){
					$scope.bankMstrList=data.bankNCAIList;
					if($scope.bankMstrList.length>0){
						$scope.bankDBFlag=false;
						$('div#bankDB').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							position: UDPos,
							show: UDShow,
							hide: UDHide,
							title: "Bank Master",
							draggable: true,
							close: function(event, ui) { 
								$(this).dialog('destroy');
								$(this).hide();
								$scope.rentDBFlag=true;
							}
						});

						$('div#bankDB').dialog('open');
					}
				}else{
					$scope.alertToast(data.result);
				}
			});
			response.error(function(data, status, headers, config){
				console.log("getRentMstr Error: "+data);
				$scope.alertToast("Error")
			});	
		}
	}
	
	$scope.openRentMstr=function(){
		console.log($scope.branch);
		if(angular.isUndefined($scope.branch)){
			$scope.alertToast("Select Branch ");
		}else{
			
			var branchId=$scope.branch.branchId;
			console.log("Branch Id   "+branchId);
			var response = $http.post($scope.projectName+'/getRentByBranchId',branchId);
			response.success(function(data){
				if(data.result==="success"){
					$scope.rentMstrList=data.rentList;
					if($scope.rentMstrList.length>0){
						$scope.rentDBFlag=false;
						$('div#rentDB').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							position: UDPos,
							show: UDShow,
							hide: UDHide,
							title: "RentMstr",
							draggable: true,
							close: function(event, ui) { 
								$(this).dialog('destroy');
								$(this).hide();
								$scope.rentDBFlag=true;
							}
						});

						$('div#rentDB').dialog('open');
					}
				}else{
					$scope.alertToast(data.result);
				}
			});
			response.error(function(data, status, headers, config){
				console.log("getRentMstr Error: "+data);
				$scope.alertToast("Error")
			});	

		}
		
	}
	
	$scope.openAddDB=function(){
		$scope.rentAddress=false;
		$('div#addDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Address",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.rentAddress=true;
			}
		});

		$('div#addDB').dialog('open');
	}

	$scope.saveAdd=function(){
		$scope.rentAddress=true;
		$('div#addDB').dialog('close');
	}
	
	$scope.updateRentMstr=function(rent){
		console.log(rent);
		var response=$http.post($scope.projectName+'/updateRentMstr',rent);
		response.success(function(data,status){
			if(data.result==="success"){
				$scope.alertToast(data.result);
				$scope.rent={};
				$scope.branch={};
				$scope.updateForm=true;
				
			}else{
				$scope.alertToast(data.result);
			}
			
		});
	}
	
	
	$scope.submitRentDetails=function(mrt,tdsAmt,index,desc){
		if(mrt.rentPM===null){
			console.log("Hello");
		}else{
		console.log(mrt);
		var rentDetail={};
		console.log("Index is "+index);
		rentDetail.rentMstrId=mrt.rmId;
		rentDetail.rentAmt=mrt.rentPM;
		rentDetail.bankFaCode=$scope.bankMstr.bnkFaCode;
		rentDetail.fromBranch=$scope.branch2.branchCode;
		rentDetail.tdsAmt=tdsAmt;
		rentDetail.rentDesc=desc;
		console.log(rentDetail);
		var response=$http.post($scope.projectName+"/submitRentDetails",rentDetail);
		response.success(function(data,status){
			if(data.result==="success"){
				$scope.alertToast(data.result);
				$scope.myRent.splice(index,1);
				
			}else{
				$scope.alertToast(data.result);
			}
			
		});
		}
	}

	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
	
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("NewRentDetailCntlr Ended");
	
}]);
'use strict';

var app = angular.module('application');

app.controller('StationaryCntlr',['$scope','$location','$http','$window',
                                 function($scope,$location,$http,$window){
	
	$scope.st={};
	$scope.chCnSe={};
	$scope.show=true;
	$scope.StationarySupplierFlag=true;
	$scope.supplierCodeList={};
	
	$scope.openStationarySupplierDB = function(){
		console.log("enter into openStationarySupplierDB function");
		$scope.StationarySupplierFlag = false;
    	$('div#StationarySupplierDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
		
		$('div#StationarySupplierDB').dialog('open');
	}
	
		$scope.submitStationaryOrder = function(StationaryOrderForm,st,chCnSe){
		console.log("enter into submitStationaryOrder function--->");
		if(StationaryOrderForm.$invalid){
			if(StationaryOrderForm.stOdrCnmt.$invalid){
				$scope.alertToast("Cnmt cannot be greater than 7 digits");	
			}else if(StationaryOrderForm.stOdrChln.$invalid){
				$scope.alertToast("Chln cannot be greater than 7 digits");	
			}else if(StationaryOrderForm.stOdrSedr.$invalid){
				$scope.alertToast("Sedr cannot be greater than 7 digits");	
			}else if(StationaryOrderForm.stSupCode.$invalid){
				$scope.alertToast("Please Enter Supplier Code");	
			}
		}else{
			if((st.stOdrCnmt===null || angular.isUndefined(st.stOdrCnmt) || st.stOdrCnmt==="")&&
					(st.stOdrChln===null || angular.isUndefined(st.stOdrChln) || st.stOdrChln==="")&&	
					(st.stOdrSedr===null || angular.isUndefined(st.stOdrSedr) || st.stOdrSedr==="")){
				$scope.alertToast("please Enter any one field ");
			}
			else{
				var response = $http.post($scope.projectName+'/saveStationaryOrder',st);
				
				response.success(function(data, status, headers, config) {
					console.log("from server ---->"+data.result);
					$scope.successToast(data.result);
					$scope.successToast(data.resultCnmt);
					$scope.successToast(data.resultChln);
					$scope.successToast(data.resultSedr);
					
					$scope.st="";
			//		$scope.getStationarySupplierCode();
				});
				response.error(function(data, status, headers, config) {
					console.log("from server ---->"+data.result);
					$scope.errorToast(data);
				});
			}
		}	
	}
	
	$scope.saveSupplierCode = function(supplier){
		console.log("Enter into saveSupplierCode");
		$('div#StationarySupplierDB').dialog('close');
		console.log("supplier.stSupCode"+supplier.stSupCode);
		$scope.st.stSupCode = supplier.stSupCode;
	}
	
	$scope.getStationarySupplierCode = function(){
		console.log("Enter into getStationarySupplierCode function");
		
		var response = $http.post($scope.projectName+'/getStationarySupplierCode');
		response.success(function(data, status, headers, config) {
			console.log("from server ---->"+data.list);
			$scope.supplierCodeList = data.list;
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.getStationarySupplierCode();
	
}]);
'use strict';

var app = angular.module('application');

app.controller('DisplayArrivalReportCntlr',['$scope','$location','$http','$filter',
                                            function($scope,$location,$http,$filter){


	$scope.arDBFlag = true;
	$scope.availableTags = [];
	$scope.dlyCont=true;
	$scope.selection=[];
	$scope.showAR = true;
	//$scope.divForAutoCode = true;
	$scope.isViewNo=false;
	$scope.arByModal=false;
	$scope.arByAuto=false;


	$scope.OpenARDB = function(){
		$scope.arDBFlag = false;
		$('div#arDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#arDB').dialog('open');
	}

	$scope.enableModalTextBox = function(){
		$('#arByMId').removeAttr("disabled");
		$('#arByAId').attr("disabled","disabled");
		$('#ok').attr("disabled","disabled");
		$scope.arByA="";
	}

	$scope.enableAutoTextBox = function(){
		$('#arByAId').removeAttr("disabled");
		$('#ok').removeAttr("disabled");
		$('#arByMId').attr("disabled","disabled");
		$scope.arByM="";
	}


	$scope.saveARCode = function(arCodes){
		$scope.arByM=arCodes;
		$('div#arDB').dialog('close');
		$scope.showAR = false;
		//$scope.divForAutoCode = true;
		$scope.isViewNo=true;
		var response = $http.post($scope.projectName+'/arDetails',$scope.arByM);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.arrivalReport = data.arrivalReport;	
				$scope.alertToast(data.result);
				if(data.image === "yes"){
					$('#submitImage').removeAttr("disabled");
				}else{
					$('#submitImage').attr("disabled","disabled");
				}
			}

		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}


	$scope.getARList=function(){
		console.log("Enter into getARList function");
		$( "#arByAId" ).autocomplete({
			source: $scope.availableTags
		});
	}



	$scope.getArriRepList = function(){
		$scope.code = $( "#arByAId" ).val();

		if($scope.code===""){
			$scope.alertToast("Please enter code");	
		}else{
			$scope.saveARCode = false;
			$scope.isViewNo=true;
			var response = $http.post($scope.projectName+'/arDetails',$scope.code);
			response.success(function(data, status, headers, config) {
				$scope.arrivalReport = data.arrivalReport;
				$scope.showAR = false;

			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	}


	$scope.getARIsViewNo = function(){
		console.log("getARIsViewNo------>");
		var response = $http.post($scope.projectName+'/getARIsViewNo');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.successToast(data.result);
				console.log("List is not empty");
				$scope.arList = data.list;
				$scope.isViewNo=false;
				for(var i=0;i<$scope.arList.length;i++){
					$scope.arList[i].creationTS =  $filter('date')($scope.arList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
				}
			}else{
				$scope.isViewNo=true;
				$('#bkToList').attr("disabled","disabled");
				console.log("List is empty");

				$scope.alertToast(data.result);
			}

			$scope.getAllArCode();
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}

	$scope.toggleSelection = function toggleSelection(arCode) {
		console.log("inside toggleSelection check = ")
		var idx = $scope.selection.indexOf(arCode);

		// is currently selected
		if (idx > -1) {
			$scope.selection.splice(idx, 1);
		}
		// is newly selected
		else {
			$scope.selection.push(arCode);
		}
	}	  

	$scope.verifyAR = function(){
		if(!$scope.selection.length){
			$scope.alertToast("You have not selected anything");
		}else{
			var response = $http.post($scope.projectName+'/updateIsViewar',$scope.selection.toString());
			response.success(function(data, status, headers, config){
				$scope.getARIsViewNo();
				console.log("---------->message==>"+data.result);
				$scope.successToast(data.result);
			});	 
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	}

	$scope.backToList = function(){
		$scope.saveARCode = true;
		$scope.isViewNo=false;
		$scope.arByM="";
		$scope.arByA="";
		$('#arByAId').attr("disabled","disabled");
		$('#arByMId').attr("disabled","disabled");
		$scope.showAR=true;
	}

	$scope.editARSubmit = function(arrivalReport){
		var response = $http.post($scope.projectName+'/saveeditar', arrivalReport);
		response.success(function(data, status, headers, config) {
			console.log(data.result);
			$scope.arrivalReport="";
			$scope.showAR = true;
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.getAllArCode = function(){
		console.log("entered into getAllArCode function------>");
		var response = $http.post($scope.projectName+'/getAllArCode');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.arCodeList = data.arCodeList;
				$scope.availableTags = data.arCodeList;
				console.log("******************");
				$scope.successToast(data.result);
			}else{
				console.log(data);
			}	
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	if($scope.adminLogin === true || $scope.superAdminLogin === true){
		$scope.getARIsViewNo();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }

}]);
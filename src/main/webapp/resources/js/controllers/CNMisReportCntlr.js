'use strict';

var app = angular.module('application');

app.controller('CNMisReportCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){
	
	$scope.branchList = [];
	$scope.branch = {};
	$scope.branchDBFlag = true;
	$scope.fromCnmtNo = '';
	$scope.toCnmtNo = '';
	$scope.misCnmtList = [];
	
	$scope.downloadExl = function(){
		console.log("enter into downloadExl function");
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
	            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
	        });
	        saveAs(blob, "MissingCNMTReport.xls");
	}
	
	$scope.getAllBranchesList = function(){
		console.log("getAllBranchesList Entered");
		var response = $http.post($scope.projectName+'/getAllBranchesList');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getAllBranchesList Success");
				$scope.branchList = data.branchCodeList;
				/*if ($scope.assignBankRBFlag) {
					$('#selectBranchId').removeAttr("disabled");
				}*/
				
				for ( var i = 0; i < $scope.branchList.length; i++) {
					console.log("Branch Name: "+$scope.branchList[i].branchName);
					console.log("Branch Code: "+$scope.branchList[i].branchCode);
					console.log("Branch Address: "+$scope.branchList[i].branchAdd);
				}
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
	}
	
	$scope.openBranchDB = function(){
		console.log("openBranchDB()");
		
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});

		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch =  function(branch){
		console.log("saveBranch()");
		$scope.branch = branch;
		$('div#branchDB').dialog('close');
	}
	
	$scope.submitBranch = function(branchForm) {
		console.log("submitBranch()");
		if (branchForm.$invalid) {
			if (branchForm.branchName.$invalid) {
				$scope.alertToast("please enter branch");
			} else if (branchForm.cnmtNoName.$invalid) {
				$scope.alertToast("please enter correct CNMT No");
			}
		}else {
			
			var misCnmtMap = {
					"fromCnmtNo"	: $scope.fromCnmtNo,
					"toCnmtNo"		: $scope.toCnmtNo,
					"branchId"		: $scope.branch.branchId
			};
			
			var response = $http.post($scope.projectName+'/getMisCnmt', misCnmtMap);
			response.success(function(data, status, headers, config){
				
				if (data.result === "success") {
					$scope.misCnmtList = data.misCnmtList;
					$('#getXlBtnId').removeAttr("disabled");
					$scope.alertToast("please click on download excel");
				}else {
					$scope.alertToast("either No Missing cnmt at this branch or incorrect cnmt No");
				}
			});
			response.error(function(data, status, headers, config){
				console.log("submitBranch Error: "+data);
			});
			
			
		}
	}

	if($scope.adminLogin === true || $scope.superAdminLogin === true){
		$scope.getAllBranchesList();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
}]);
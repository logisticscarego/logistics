'use strict';

var app = angular.module('application');

app
		.controller(
				'ClrPendingIBTVCntlr',
				[
						'$scope',
						'$location',
						'$http',
						'$filter',
						'$window',
						function($scope, $location, $http, $filter, $window) {

							$scope.vs = {};
							$scope.branch = {};
							$scope.tibtList = []

							$scope.actIndex = 0;
							$scope.bankCodeDBFlag = true;
							$scope.loadingFlag = false;

							$scope.getVoucherDetails = function() {
								console.log("Enter into getDetails");
								$scope.saveVsFlag = true;
								var response = $http.post($scope.projectName
										+ '/getVDetFrIBTV');
								response
										.success(function(data, status,
												headers, config) {
											if (data.result === "success") {
												$scope.vs.branch = data.branch;
												$scope.bankCodeList = data.bankCodeList;
												$scope.vs.cashStmtStatus = data.cashStmtStatus;
												$scope.vs.voucherType = $scope.IBTV_VOUCHER;
												$scope.dateTemp = $filter(
														'date')
														(
																data.cashStmtStatus.cssDt,
																"yyyy-MM-dd'");

												// $scope.getLoginBrDet();
												$scope.getPendingIBTV();
											} else {
												console
														.log("Error in bringing data");
											}
										});
								response.error(function(data, status, headers,
										config) {
									console.log(data);
								});
							}

							$scope.getLoginBrDet = function() {
								console
										.log("enter into getLoginBrDet function");
								var response = $http.post($scope.projectName
										+ '/getLoginBrDet');
								response
										.success(function(data, status,
												headers, config) {
											if (data.result === "success") {
												$scope.branch = data.branch;
												$scope.getPendingIBTV();
											} else {
												console
														.log("error in bringing LoginBrDet");
											}
										});
								response.error(function(data, status, headers,
										config) {
									console.log(data);
								});
							}

							$scope.getPendingIBTV = function() {
								console
										.log("enter into $scope.getPendingIBTV function");
								$scope.loadingFlag = true;
								var response = $http.post($scope.projectName
										+ '/getPendingIBTV',
										$scope.vs.branch.branchFaCode);
								response.success(function(data, status,
										headers, config) {
									$scope.loadingFlag = false;
									$scope.tibtList = data.list;
									$scope.bnkList = data.bnkList;
									console.log("size of bnkList = "
											+ $scope.bnkList.length);
								});
								response.error(function(data, status, headers,
										config) {
									$scope.loadingFlag = false;
									console.log(data);
								});
							}

							$scope.openBankDB = function(index) {
								console.log("enter into openBankDB function");
								$scope.actIndex = index;
								$scope.bankCodeDBFlag = false;
								$('div#bankCodeDB').dialog({
									autoOpen : false,
									modal : true,
									resizable : false,
									title : "Select Bank",
									show : UDShow,
									hide : UDHide,
									position : UDPos,
									draggable : true,
									close : function(event, ui) {
										$(this).dialog('destroy');
										$(this).hide();
										$scope.bankCodeDBFlag = true;
									}
								});

								$('div#bankCodeDB').dialog('open');
							}

							$scope.saveBankCode = function(code) {
								console.log("enter into saveBankCode function");
								$scope.tibtList[$scope.actIndex].tibBnkFaCode = code;
								$('div#bankCodeDB').dialog('close');
							}

							$scope.clrAllIBTV = function() {
								console.log("enter into clrAllIBTV funciton");
								var ibtService = {
									"voucherService" : $scope.vs,
									"tibList" : $scope.tibtList
								};
								$('#clrIbtvId').attr("disabled", "disabled");
								var response = $http.post($scope.projectName
										+ '/clrAllIBTV', ibtService);
								response
										.success(function(data, status,
												headers, config) {
											if (data.result === "success") {
												$('#clrIbtvId').removeAttr(
														"disabled");
												$scope
														.alertToast("successfully clear all pending IBTV C/D");
												$scope.getPendingIBTV();
											} else {
												console
														.log("error in clearAllIBTV;")
											}
										});
								response.error(function(data, status, headers,
										config) {
									console.log(data);
								});
							}

							$scope.clrIBTV = function(ibTv, index) {
								console.log("enter into clrIBTV function = "
										+ index + "  --  " + ibTv.tibBnkFaCode);

								if (angular.isUndefined(ibTv.tibBnkFaCode)
										|| ibTv.tibBnkFaCode === null
										|| ibTv.tibBnkFaCode === "") {
									$scope.alertToast("Please select bank")
								} else {
									$("#clrtvId" + index).attr("disabled",
											"disabled");

									var response = $http.post(
											$scope.projectName + '/clrIBTV',
											ibTv);
									response
											.success(function(data, status,
													headers, config) {
												if (data.result === "success") {
													$scope.tibtList.splice(
															index, 1);
													$scope
															.alertToast("SUCCESS");
												} else {
													$("#clrtvId" + index)
															.removeAttr(
																	"disabled");
													$scope
															.alertToast("ERROR in clear IBTV");
												}
											});
									response.error(function(data, status,
											headers, config) {
										console.log(data);
									});
								}
							}

							if ($scope.operatorLogin === true
									|| $scope.superAdminLogin === true) {
								$scope.getVoucherDetails();
							} else if ($scope.logoutStatus === true) {
								$location.path("/");
							} else {
								console.log("****************");
							}

						} ]);
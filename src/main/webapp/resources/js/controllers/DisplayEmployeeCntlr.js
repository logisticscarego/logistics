'use strict';

var app = angular.module('application');

app.controller('DisplayEmployeeCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	
	$scope.selection=[];
	$scope.empModalRadio = false ;
	$scope.empAutoRadio = false;
	$scope.empCodeDBFlag = true;
	$scope.employeeTableDBFlag = true;
	$scope.editEmployeeDetailsDBFlag = true;
	$scope.employee={};
	$scope.availableTags = [];
	$scope.autoEmpCodeList = [];
	$scope.employeeList =[];
	var idx;
	$scope.emp ={};
	 
	var basic;
	var hra;
	var pf;
	var gross;
	var netsalary;
	var esi;
	var otherallowance = 0;
	
	$scope.branchCodeDBFlag = true;
	$scope.presentAddrDBFlag = true;
	$scope.viewEmpDetailsFlag=true;
	
	$scope.creationTSDBFlag = true;
	$scope.viewEmpOldDetailsFlag = true;
	$scope.empOldList =[];
	$scope.empOld = {};
	
	$scope.displayEmpDataForAdmin = function(){
		console.log("Entered into displayEmpDataForAdmin function------>");
		var response = $http.post($scope.projectName+'/displayEmpDataForAdmin');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.employeeList = data.list;
				$scope.employeeTableDBFlag = false;
				for(var i=0;i<$scope.employeeList.length;i++){
					$scope.employeeList[i].creationTS =  $filter('date')($scope.employeeList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
				}
				$scope.successToast(data.result);
				
			}else{
				$scope.employeeTableDBFlag = true;
				$scope.alertToast(data.result);
			}
			$scope.getAllEmpListForAdmin();
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
		
	}
	
	$scope.toggleSelection = function toggleSelection(empCode) {
		   idx = $scope.selection.indexOf(empCode);
		   
		   // is currently selected
		   if (idx > -1) {
			   $scope.selection.splice(idx, 1);
		   }
		   // is newly selected
		   else {
			   $scope.selection.push(empCode);
		   }
	   }	  

	$scope.verifyEmployee = function(){
		console.log("Entered into verifyEmployee function--- ");
		if(!$scope.selection.length){
			 $scope.alertToast("You have not selected anything");
		 }else{
			 	var response = $http.post($scope.projectName+'/updateIsViewEmployee',$scope.selection.toString());
			 	response.success(function(data, status, headers, config){
			 		$scope.successToast(data.result);
			 		$scope.displayEmpDataForAdmin();
             
			 	});     
			 	response.error(function(data, status, headers, config) {
			 		$scope.errorToast(data.result);
			 	});
		 }	 	
    }
	
	
	$scope.enableEmpCodeTextbox = function(){
		$('#empTextList').removeAttr("disabled");
		$('#empAutoTextList').attr("disabled","disabled");
		$scope.empCodeAutoList="";
	}
	
	$scope.OpenEmpCodeDB = function(){
		$scope.empCodeDBFlag = false;
		$scope.empOld = "";
		$scope.empOldList ="";
		$scope.creationTSModel ="";
    	$('div#empCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Code",
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
	});
		
			$('div#empCodeDB').dialog('open');
	}
		
	$scope.saveEmployeeCode =  function(emp){
		console.log("enter into saveEmployeeCode----->"+emp.empCode);
		$scope.empCodeModalList = emp.empCode;
		$('div#empCodeDB').dialog('close');
		$scope.empCodeDBFlag = true;
		$scope.filterTextbox = "";
		$("#creationTSId").removeAttr('disabled');
		$scope.showEmployeeDetails(emp.empCode);
	}
	
	$scope.showEmployeeDetails = function(empCode){
		console.log("Entered into showEmployeeDetails function--- ");
		$scope.employeeTableDBFlag = true;
		$scope.editEmployeeDetailsDBFlag = false;
		var response = $http.post($scope.projectName+'/employeeDetails', empCode);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.employee = data.employee;	
				$scope.editEmployeeDetailsDBFlag = false;
				$scope.viewEmpOldDetailsFlag = true;
				$scope.getBranchIsNCR($scope.employee.branchCode);
				$scope.getModifiedEmpDetail(empCode);
			}else{
				console.log(data);
			}				
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.getBranchIsNCR = function(branchCode){
		console.log("Entered into getBranchIsNCR function----"+branchCode);
		
		var response = $http.post($scope.projectName+'/getBranchIsNCR', branchCode);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.employee.isInNCR=data.isNCR;
				console.log("value of isNCR after returning from server side----"+$scope.employee.isInNCR);
				/*$scope.successToast(data.result);*/
			}else{
				console.log(data);
			}				
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.enableAutoEmpCodeTextBox = function(){
		$('#empAutoTextList').removeAttr("disabled");
		$('#empTextList').attr("disabled","disabled");
		$scope.empCodeModalList="";
		
		$("#creationTSId").attr('disabled','disabled');
		$scope.empOldList ="";
		$scope.empOld ="";
		$scope.creationTSModel ="";
		$scope.viewEmpOldDetailsFlag = true;
		$scope.employeeTableDBFlag = false;
	}
	

	$scope.getEmpCodeList=function(){
		console.log("Enter into getEmpCodeList function");
	      $( "#empAutoTextList" ).autocomplete({
	    	  source: $scope.autoEmpCodeList
	      });
	}
		
		$scope.empDataByEmpCodeTemp = function(empCodeAutoList){
	
			if(angular.isUndefined(empCodeAutoList) || empCodeAutoList === null || empCodeAutoList === ""){
				$scope.alertToast("Please enter employee code...")
			}else{
				$scope.employeeTableDBFlag=true;			
				$scope.code = $( "#empAutoTextList" ).val();
				console.log("$scope.code---------"+$scope.code);
			
				var response = $http.post($scope.projectName+'/empDataByEmpCodeTemp',$scope.code);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.employee = data.employee;	
						$scope.editEmployeeDetailsDBFlag = false;
						$scope.viewEmpOldDetailsFlag = true;
						$scope.getBranchIsNCR($scope.employee.branchCode);
					}else if(data.result==="error"){
						$scope.editEmployeeDetailsDBFlag = true;
						$scope.alertToast("There is no such employee...")
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
		}

	$scope.backToList = function(){
		if(!$scope.employeeList.length){
			$scope.employeeTableDBFlag = true;
		}else{
			$scope.employeeTableDBFlag = false;
		}
		$scope.editEmployeeDetailsDBFlag = true;
		$scope.empCodeModalList="";
		$scope.empCodeAutoList="";
		$scope.empModalRadio = false ;
		$scope.empAutoRadio = false;
		$('#empTextList').attr("disabled","disabled");
		$('#empAutoTextList').attr("disabled","disabled");
		
		$("#creationTSId").attr('disabled','disabled');
		$scope.empOldList ="";
		$scope.empOld ="";
		$scope.creationTSModel ="";
		$scope.viewEmpOldDetailsFlag = true;
	}
	
	$scope.editEmployeeSubmit = function(EmployeeForm,employee){
		console.log("enter into editEmployeeSubmit function--->");
		if(EmployeeForm.$invalid){
			console.log("enter into if part of submit function--->");
			
			if(EmployeeForm.empName.$invalid){
				$scope.alertToast("Please enter employee name between 7-122 characters.....");
			}else if(EmployeeForm.empDesignation.$invalid){
				$scope.alertToast("Please enter employee designation between 2-40 characters.....");
			}else if(EmployeeForm.empAdd.$invalid){
				$scope.alertToast("Please enter employee address between 3-255 characters.....");
			}else if(EmployeeForm.empCity.$invalid){
				$scope.alertToast("Please enter employee city between 3-40 characters.....");
			}else if(EmployeeForm.empState.$invalid){
				$scope.alertToast("Please enter employee state between 3-40 characters.....");
			}else if(EmployeeForm.empPin.$invalid){
				$scope.alertToast("Please enter PIN number of 6 digits.....");
			}else if(EmployeeForm.empPFNo.$invalid){
				$scope.alertToast("Please enter employee PF number between 8-18 characters...");
			}else if(EmployeeForm.empESINo.$invalid){
				$scope.alertToast("Please enter employee ESI number between 3-40 characters...");
			}else if(EmployeeForm.empESIDispensary.$invalid){
				$scope.alertToast("Please enter employee ESI dispensary between 3-40 characters...");
			}else if(EmployeeForm.empNominee.$invalid){
				$scope.alertToast("Please enter employee nominee name between 7-81 characters...");
			}else if(EmployeeForm.empEduQuali.$invalid){
				$scope.alertToast("Please enter employee educational qualification between 2-255 characters...");
			}else if(EmployeeForm.empPresentAdd.$invalid){
				$scope.alertToast("Please enter present address of employee between 3-255 characters.....");
			}else if(EmployeeForm.empPresentCity.$invalid){
				$scope.alertToast("Please enter present city of employee between 3-40 characters.....");
			}else if(EmployeeForm.empPresentState.$invalid){
				$scope.alertToast("Please enter present state of employee between 3-40 characters.....");
			}else if(EmployeeForm.empPresentPin.$invalid){
				$scope.alertToast("Please enter present PIN number of employee of 6 digits...");
			}else if(EmployeeForm.empSex.$invalid){
				$scope.alertToast("Please enter employeee sex...");
			}else if(EmployeeForm.empFatherName.$invalid){
				$scope.alertToast("Please enter father's name of employee between 7-81 characters...");
			}else if(EmployeeForm.empLicenseNo.$invalid){
				$scope.alertToast("Please enter employee licence number between 3-40 characters...");
			}else if(EmployeeForm.empBankAcNo.$invalid){
				$scope.alertToast("Please enter correct format of Bank Account number between 10-40 characters...");
			}else if(EmployeeForm.empPanNo.$invalid){
				$scope.alertToast("Please enter correct format of PAN number of 10 characters...");
			}else if(EmployeeForm.empMailId.$invalid){
				$scope.alertToast("Please enter correct format of email id...");
			}else if(EmployeeForm.empPhNo.$invalid){
				$scope.alertToast("Please enter phone number of 4-15 digits....");
			}else if(EmployeeForm.empBankIFS.$invalid){
				$scope.alertToast("Please enter employee Bank IFSC between 3-40 characters....");
			}else if(EmployeeForm.empBankName.$invalid){
				$scope.alertToast("Please enter employee bank name between 3-40 characters....");
			}else if(EmployeeForm.empBankBranch.$invalid){
				$scope.alertToast("Please enter employee bank branch between 3-40 characters....");
			}else if(EmployeeForm.isTerminate.$invalid){
				$scope.alertToast("Please enter employee termination status....");
			}
		}else{
			if($scope.employee.empDOB>$scope.employee.empDOAppointment){
				$scope.alertToast("Please enter valid date of appointment...");
			}else{
				
				$scope.viewEmpDetailsFlag = false;
				$('div#viewEmpDetailsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "View Employee Details",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
				});
				$('div#viewEmpDetailsDB').dialog('open');
			}
		}
	}	
		
		$scope.saveEdittedEmployee = function(employee){
			console.log("Entered into saveEdittedEmployee function");
			var response = $http.post($scope.projectName+'/editEmployeeSubmit', employee);
			response.success(function(data, status, headers, config) {
				if(data.result==="success"){
					$('div#viewEmpDetailsDB').dialog('close');
					$scope.viewEmpDetailsFlag = true;
					$scope.editEmployeeDetailsDBFlag = true;
					$scope.successToast(data.result);
					$scope.getModifiedEmpDetail($scope.employee.empCode);
				}else{
					console.log(data);
				}		
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		    });
		}
		
		$scope.closeViewEmpDetailsDB = function(){
			$('div#viewEmpDetailsDB').dialog('close');
			$scope.viewEmpDetailsFlag = true;
		}
		
	$scope.getAllEmpListForAdmin = function(){
		console.log("entered into getAllEmpListForAdmin function------>");
		var response = $http.post($scope.projectName+'/getAllEmpListForAdmin');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.empCodeList = data.empCodeList;
				//$scope.autoEmpCodeList = data.empCodeList;
				for(var i=0;i<$scope.empCodeList.length;i++){
					/*console.log("value ---->"+$scope.empCodeList[i].empCodeTemp);*/
					$scope.autoEmpCodeList[i] = $scope.empCodeList[i].empCodeTemp;
					/*console.log("$scope.autoEmpCodeList----"+$scope.autoEmpCodeList);*/
				}
				/*for(var i=0;i<$scope.autoEmpCodeList.length;i++){
					console.log("valUE IN autoEmpCodeList ---->"+$scope.autoEmpCodeList[i]);
				}*/
				$scope.successToast(data.result);
			}else{
				console.log(data);
			}	
			$scope.getBranchData();
		});
		response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		});
	}
	
	$scope.openBranchCodeDB = function(){
		   $scope.branchCodeDBFlag = false;
			$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch Code",
			draggable: true,
		    close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchCodeDB').dialog('open');
	   }
	
	$scope.getBranchData = function(){
		   console.log("getBranchData------>");
		   var response = $http.post($scope.projectName+'/getBranchDataForEmp');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
					$scope.branchList = data.list;
					$scope.getDesignation();
				}else{
					console.log(data);
				}	
			 
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		   });
	}
	
	$scope.saveBranchCode =  function(branchCode,isNCR){
		console.log("enter into saveBranchCode----->"+branchCode);
		console.log("enter into saveBranchCode----->"+isNCR);
		$scope.employee.branchCode = branchCode;
		$('div#branchCodeDB').dialog('close');
		$scope.branchCodeDBFlag = true;
	}
	
	$scope.getEmpDesigList=function(employee){
	      $( "#empDesignation" ).autocomplete({
	    	  source: $scope.availableTags
	      });
	} 
	
	$scope.fillEmpDesigVal = function(){
		console.log("enter into myfun----------->>"+$( "#empDesignation" ).val());
		$scope.employee.empDesignation=$( "#empDesignation" ).val();
	}
		
	$scope.getDesignation = function(){
		console.log("Entered into getDesignation function----");
		var response = $http.get($scope.projectName+'/getDesignation');
		response.success(function(data, status, headers, config) {
			$scope.availableTags = data;
	    });
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	 $scope.calculateOnBasic = function(){	
		 
		   console.log("Entered into calculateOnBasic function----");
		   console.log("value of  isNCR----"+$scope.employee.isInNCR);
		   basic=$scope.employee.empBasic;
		   console.log(basic);
		   	   	   
		   if($scope.employee.isInNCR==="yes"){
			   hra=0.5*basic; 
		   }else if($scope.employee.isInNCR==="no"){
			   hra=0.4*basic;
		   }
		   	     
		   pf=0.24*basic;
		   gross=parseInt(basic)+parseInt(hra)+parseInt(pf)+parseInt(otherallowance);
		  
		   $scope.employee.empHRA=hra;
		   $scope.employee.empPF=pf;
		   $scope.employee.empGross=gross;
		   
		   if(gross<=14999)
		   {
		    esi=0.065*gross;
		    netsalary=((parseInt(basic)+parseInt(hra)+parseInt(otherallowance))-((parseInt(basic)+parseInt(hra)+parseInt(otherallowance))*0.0175+0.12*parseInt(basic)));
		    $scope.employee.empESI=esi
		    $("#empESI").removeAttr('disabled');
		    $scope.employee.netSalary=netsalary;
		   } 
		   else if(gross>=15000){
			   $scope.employee.empESI="";
			   $("#empESI").attr('disabled','disabled');
			   netsalary=(parseInt(basic)+parseInt(hra)+parseInt(otherallowance))-(0.12*parseInt(basic));
			   $scope.employee.netSalary=netsalary;
		   }
	   }
	   
	   $scope.calculateOnOthers = function(value){
		   console.log("Entered into calculateOnOthers function----");
		   if(angular.isUndefined(value) || value === null || value ===""){
			   otherallowance =0;
		   }else{
			   otherallowance = parseInt(value);
	       }
		   $scope.calculateOnBasic();
	   }
	   
	   $scope.chkPresentAddrDB = function(){
		   if(angular.isUndefined($scope.employee.empAdd) || $scope.employee.empAdd === null || $scope.employee.empAdd === ""){
			   $scope.alertToast("Please enter employee address");
		   }else if(angular.isUndefined($scope.employee.empCity) || $scope.employee.empCity === null || $scope.employee.empCity === ""){
			   $scope.alertToast("Please enter employee city");
		   }else if(angular.isUndefined($scope.employee.empState) || $scope.employee.empState === null || $scope.employee.empState === ""){
			   $scope.alertToast("Please enter employee state");
		   }else if(angular.isUndefined($scope.employee.empPin) || $scope.employee.empPin === null || $scope.employee.empPin === ""){
			   $scope.alertToast("Please enter employee pin");
		   }else{
			   $scope.presentAddrDBFlag = false;
				$('div#presentAddrDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Employee Present Address",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
				});
				$('div#presentAddrDB').dialog('open');
		   }
	  }
	  
	  $scope.yesPresentAddr = function(){
		  console.log("Entered into yesPresentAddr function");
		  $scope.employee.empPresentAdd = $scope.employee.empAdd;
		  $scope.employee.empPresentCity = $scope.employee.empCity;
		  $scope.employee.empPresentState = $scope.employee.empState;
		  $scope.employee.empPresentPin = $scope.employee.empPin;
		  $('div#presentAddrDB').dialog('close');
		  $scope.presentAddrDBFlag = true;
	  }
	  
	  $scope.YesNoAddr = function(){
		  console.log("Entered into YesNoAddr function");
		  $('div#presentAddrDB').dialog('close');
		  $scope.presentAddrDBFlag = true;
	  }
	  
	  $scope.openBranchPage = function(){
		   console.log("**************Entered into openBranchPage*************");
		   $('div#branchCodeDB').dialog('close');
		   $scope.branchCodeDBFlag = true;
		   $location.path('/operator/branch');
	   }
	  
	  $('#empPin').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });


	   $('#empPin').keypress(function(e) {
		   if (this.value.length == 6) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPresentPin').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });


	   $('#empPresentPin').keypress(function(e) {
	       if (this.value.length == 6) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empBasic').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });


	   $('#empBasic').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	   
	   $('#empLoanBal').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });


	   $('#empLoanBal').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	   
	   $('#empLoanPayment').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });


	   $('#empLoanPayment').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	     
	   $('#empTelephoneAmt').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });


	   $('#empTelephoneAmt').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	      
	   $('#empMobAmt').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });

	   $('#empMobAmt').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPhNo').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });


	   $('#empPhNo').keypress(function(e) {
	       if (this.value.length == 15) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empBankAcNo').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });


	   $('#empBankAcNo').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empOtherAllowance').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });


	   $('#empOtherAllowance').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPanNo').keypress(function(e) {
	       if (this.value.length == 10) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empName').keypress(function(e) {
	       if (this.value.length == 122) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empDesignation').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empEduQualification').keypress(function(e) {
	       if (this.value.length == 255) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empFatherName').keypress(function(e) {
	       if (this.value.length == 81) {
	           e.preventDefault();
	       }
	   });
	  	   
	   $('#empNominee').keypress(function(e) {
	       if (this.value.length == 81) {
	           e.preventDefault();
	       }
	   });
	     
	   $('#empAdd').keypress(function(e) {
	       if (this.value.length == 255) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empCity').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empState').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPresentAdd').keypress(function(e) {
	       if (this.value.length == 255) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPresentCity').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPresentState').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empESINo').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empESIDispensary').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPFNo').keypress(function(e) {
	       if (this.value.length == 18) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empLicenseNo').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empMailId').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empBankName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empBankBranch').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empBankIFS').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $scope.getModifiedEmpDetail = function(empCode){
		   console.log("Entered into getModifiedEmpDetail function----");
		   var response = $http.post($scope.projectName+'/getModifiedEmpDetail',empCode);
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
					$scope.empOldList = data.list;
					for(var i=0;i<$scope.empOldList.length;i++){
						$scope.empOldList[i].creationTS =  $filter('date')($scope.empOldList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
					}
			   }else if(data.result==="error"){
					$scope.alertToast("Selected employee has never been modified....")
					$("#creationTSId").attr('disabled','disabled');
			   }	
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		   });
	   }
	   
	   $scope.OpenCreationTSDB = function(){
		   $scope.creationTSDBFlag = false;
			$('div#creationTSDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Modification Time List",
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
				}
			});
			$('div#creationTSDB').dialog('open');
	   }
	   
	   $scope.saveCreationTS = function(creationTime){
		   $scope.creationTSModel = creationTime;
		   $('div#creationTSDB').dialog('close');
		   $scope.creationTSDBFlag = true;
		   $scope.editEmployeeDetailsDBFlag = true;
		   for(var i=0;i<$scope.empOldList.length;i++){
			   if($scope.empOldList[i].creationTS === creationTime){
				   console.log("Entered into if--")
				   $scope.empOld = $scope.empOldList[i];
				   $scope.viewEmpOldDetailsFlag = false;
			   }
		   }
	   }
	
	   if($scope.adminLogin === true || $scope.superAdminLogin === true){
		   $scope.displayEmpDataForAdmin();
		 }else if($scope.logoutStatus === true){
			 $location.path("/");
		 }else{
			 console.log("****************");
		 } 
		
}]);

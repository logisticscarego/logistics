'use strict';

var app = angular.module('application');

app.controller('DisplayOwnerCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	
	$scope.OwnerCodeFlag = true;
	$scope.isViewNo=false;
	$scope.selection=[];
	$scope.availableTags = [];
	$scope.showOwnerDetails = true;
	$scope.owner={};
	$scope.ownCodeByModal=false;
	$scope.ownCodeByAuto=false;

	$scope.OpenOwnCodeCodeDB = function(){
		$scope.OwnerCodeFlag = false;
    	$('div#OwnerCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
			$('div#OwnerCodeDB').dialog('open');
		}
	
	$scope.enableModalTextBox = function(){
		$('#ownCodeByMId').removeAttr("disabled");
		 $('#ownCodeByAId').attr("disabled","disabled");
		 $('#ok').attr("disabled","disabled");
		 $scope.ownCodeByA="";
	}
	
	$scope.enableAutoTextBox = function(){
		$('#ownCodeByAId').removeAttr("disabled");
		$('#ok').removeAttr("disabled");
		 $('#ownCodeByMId').attr("disabled","disabled");
		 $scope.ownCodeByM="";
	}
	
	$scope.getOwnIsViewNo = function(){
		   var response = $http.post($scope.projectName+'/getOwnIsViewNo');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.successToast(data.result);
			   $scope.OwnerList = data.list;
			   for(var i=0;i<$scope.OwnerList.length;i++){
			   $scope.OwnerList[i].creationTS = $filter('date')($scope.OwnerList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');   
			   }
			  // $scope.getOwnerCode();
			   }else{
				   $scope.isViewNo=true;
				   $scope.alertToast(data.result);
			   }
	    });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	 }
	
	$scope.saveOwnCode = function(ownerCodes){
		$scope.ownCodeByM=ownerCodes;
		$('div#OwnerCodeDB').dialog('close');
		$scope.showOwnerDetails  = false;
		$scope.isViewNo=true;
		var response = $http.post($scope.projectName+'/ownerDetails',ownerCodes);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.owner = data.owner;	
			}else{
				console.log(data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	
	
	$scope.toggleSelection = function toggleSelection(ownCode) {
		   var idx = $scope.selection.indexOf(ownCode);
		   
		   if (idx > -1) {
			   $scope.selection.splice(idx, 1);
		   }else {
			   $scope.selection.push(ownCode);
		   }
	   }	
	
	$scope.verifyOwner = function(){
		
		if(!$scope.selection.length){
			 $scope.alertToast("You have not selected anything");
		 }else{
		 var response = $http.post($scope.projectName+'/updateIsViewOwner',$scope.selection.toString());
		 response.success(function(data, status, headers, config){
			 if(data.result==="success"){
				console.log("---------->message==>"+data.result);
				$scope.successToast(data.result);
				$scope.getOwnIsViewNo(); 
			 }else{
				 console.log(data.result);
			 }
			   
	    });	 
		 response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	 }
	}
		
	$scope.getOwnCodeList=function(){
	      $( "#ownCodeByAId" ).autocomplete({
	    	  source: $scope.availableTags
	      });
	}
	
	
	$scope.getOwnerCode = function(){
		var response = $http.get($scope.projectName+'/getOwnerCode');
		response.success(function(data, status, headers, config) {
			$scope.availableTags = data;
			$scope.getOwnIsViewNo();
	    });
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.getOwnerList = function(){
		$scope.code = $("#ownCodeByAId").val();
		if($scope.code===""){
			$scope.alertToast("Please enter code");
		}else if($scope.code.substring(0,3)==="own"){
			$scope.showOwnerDetails = false;
			$scope.isViewNo=true;
			var response = $http.post($scope.projectName+'/ownerDetails',$scope.code);
			response.success(function(data, status, headers, config) {
				if(data.result==="success"){
					$scope.owner = data.owner;	
				}else{
					console.log(data.result);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}else{
			$scope.alertToast("Enter correct code");
		}
	}
	
	$scope.backToList = function(){
		 $scope.showOwnerDetails = true;
		 $scope.isViewNo=false;
		 $scope.ownCodeByM="";
		 $scope.ownCodeByA="";
		 $scope.ownCodeByModal=false;
		 $scope.ownCodeByAuto=false;
		 $('#ok').attr("disabled","disabled");
		 $('#ownCodeByAId').attr("disabled","disabled");
		 $('#ownCodeByMId').attr("disabled","disabled");
		 $scope.getOwnIsViewNo();
	 }
	
	$scope.EditOwnerSubmit = function(owner){
		var response = $http.post($scope.projectName+'/EditOwnerSubmit', owner);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.alertToast(data.result);
				$scope.owner="";
				$scope.ownCodeByM="";
				$scope.ownCodeByA="";
			}else{
				console.log(data.result);
			}
			
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	$scope.getOwnerCodesList = function(){
		console.log("Enetr into $scope.getOwnerCodesList function");
		var response = $http.post($scope.projectName+'/getOwnerCodesList');
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.alertToast(data.result);
				$scope.ownerCodeList = data.list;
				$scope.getOwnerCode();
				console.log("---"+$scope.ownerCodeList);	
			}else{
				console.log(data.result);
			}
			
	    });
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	if($scope.adminLogin === true || $scope.superAdminLogin === true){
		$scope.getOwnerCodesList();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
	
	//$scope.getOwnIsViewNo();
}]);

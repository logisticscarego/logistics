'use strict';

var app = angular.module('application');

app.controller('AckValidatorCntlr',['$scope','$location','$http','$window','FileUploadService','$filter',
                            function($scope,$location,$http,$window,FileUploadService,$filter){


	$scope.AckValidatorDBFlag=true;
	$scope.ackFormData=true;
	$scope.getAckValidationForm=false;
	$scope.dspwDiv=true;
	$scope.VOGDiv=true;
	$scope.weightOrPkg="";
	$scope.damOrShrtg="";
	$scope.claimwght=0;
	
	$scope.getDataFromAckValidator=function(fromDate,toDate,remarkType)
	{
		console.log(fromDate);
		console.log(toDate);
		console.log(remarkType);
		var dateData={fromDate:fromDate, toDate:toDate,remarkType:remarkType};
		console.log(dateData)
	   var response = $http.post($scope.projectName + '/getAckValidatorData', dateData);
			response.success(function(data, status, headers, config){
				console.log(data.result);
				$scope.alertToast(data.result);
				if(data.result==="success")
					{
					$scope.ackValidationData=data.ackValidationData;
					console.log($scope.ackValidationData);
					if(data.ackValidationData.length>0)
						{
					$scope.AckValidatorDBFlag=false;
					$scope.getAckValidationForm=true;
						}
					else
					{
					$scope.AckValidatorDBFlag=true;
					}
					}
				else
					{
					$scope.alertToast(data.error);
					}
			

			});
			response.error(function(data, status, headers, config){

			});
			}
	
	
	$scope.openCnmtPdf=function(cnmtCode)
	{
		console.log(cnmtCode);
		var response=$http.post($scope.projectName + '/getCnmtPdfFile', cnmtCode);
		response.success(function(data, status, headers, config){
		     console.log(data.result);
			if(data.result==="success")
				{
				$scope.AckValidatorDBFlag=true;
				$scope.ackFormData=false;
				
				$scope.cnmt=data.cnmt;
				$scope.consigneeCode=$scope.cnmt.cnmtConsignee;
				$scope.consignorCode=$scope.cnmt.cnmtConsignor;
				$scope.consignee=data.consignee;
				$scope.consignor=data.consignor;
		        $scope.arrivalReport=data.arrivalReport;
		        $scope.challan=data.challan;
		        $scope.branchName=data.branchName;
		        $scope.recWeight=$scope.arrivalReport.arRcvWt;
		        $scope.recPkg=$scope.arrivalReport.arPkg;
		        console.log($scope.branchName);
		        console.log($scope.arrivalReport);
		        console.log($scope.challan);
		        $scope.pkg=$scope.challan.chlnNoOfPkg;
		        $scope.wght=$scope.challan.chlnTotalWt;
		        
		        var cnDt=new Date($scope.challan.chlnDt);
				var arRepDt=new Date($scope.arrivalReport.arRepDt);
				$scope.timeTaken=Math.round((arRepDt.getTime()-cnDt.getTime())/(1000*60*60*24));
				console.log($scope.timeTaken);
				
				
         $scope.alertToast(data.result);

     var form = document.createElement("form");
         form.method = "POST";
         form.action = $scope.projectName + '/openCnmtPdf';
         form.target = "_blank";
         document.body.appendChild(form);
         form.submit();
      		}
			else if(data.result==="error")
				{
				  $scope.alertToast("File Not Found");
				}
		
		});
		response.error(function(data, status, headers, config){
			console.log("Error in hitting /getCnmtCodeByCode");
			console.log(data);
			 // $scope.alertToast("File Not Found");
		});
		
	}
	
	$scope.lrArrivalDt=function(lrarrivalDate)
	{
		console.log(lrarrivalDate);
		$scope.arrivalDate=lrarrivalDate;	
	}
	$scope.lrUnloadingDt=function(lrUnloadingdate)
	{
	var unloadingdate=new Date(lrUnloadingdate);
	var arrivalkDate=new Date($scope.arrivalDate);
	$scope.days=Math.round((unloadingdate.getTime()-arrivalkDate.getTime())/(1000*60*60*24));
	console.log($scope.days+" days");
	
	}
	$scope.packORWeight=function(weightOrPkg)
	{
		console.log(weightOrPkg);
	if(weightOrPkg==="Package")
		{
		$scope.pw=true;
		var vogperPacage=$scope.cnmt.cnmtVOG/$scope.cnmt.cnmtNoOfPkg;
		$scope.gdClaimAmt=Math.round(vogperPacage*$scope.claimPkg);
		}
	else if(weightOrPkg==="Weight")
		{
		$scope.pw=false;
		var vogforWeight=$scope.cnmt.cnmtVOG/$scope.cnmt.cnmtActualWt;
		$scope.gdClaimAmt=Math.round(vogforWeight*$scope.claimwght);
		}
	}
	
	$scope.calculatePkgDiff=function(recPkg)
	{
		$scope.claimPkg=$scope.pkg-recPkg;	
		var vogperPacage=$scope.cnmt.cnmtVOG/$scope.cnmt.cnmtNoOfPkg;
		$scope.gdClaimAmt=Math.round(vogperPacage*$scope.claimPkg);

	}
	
	$scope.calculateWeightDiff=function(recWeight)
	{
	$scope.claimwght=$scope.wght-recWeight;
	var vogforWeight=$scope.cnmt.cnmtVOG/$scope.cnmt.cnmtActualWt;
	$scope.gdClaimAmt=Math.round(vogforWeight*$scope.claimwght);

	}
	
	$scope.remarksSel=function(remarks)
	{
		if(remarks==="ok")
			{
			$scope.dspwDiv=true;
			$scope.VOGDiv=true;
			}
		else if(remarks==="QUALIFIED")
			{
			$scope.dspwDiv=false;
			$scope.VOGDiv=false;
			}
		
	}
	
	$scope.doAckValidation=function(cnmtCode,cnmtDate,arrivalReport,challan,consignor,consignee,lryArTime,lrarrivalDate,uploadingTime,unloadingAmt,lrUnloadingdate,detention,othAmt,days,recWeight,recPkg,remarks,damOrShrtg,weightOrPkg,claimPkg,claimwght,gdClaimAmt,reason)
	{
		console.log(cnmtCode);
		console.log(arrivalReport);
		console.log(challan.chlnCode);
		console.log(consignee);
		console.log(consignor);
		console.log(damOrShrtg);
		console.log(reason);
		console.log(remarks);
		$scope.damOrShrtg=damOrShrtg;
		$scope.weightOrPkg=weightOrPkg;
		$scope.lryArTime=lryArTime;
		$scope.lrarrivalDate=lrarrivalDate;
		$scope.uploadingTime=uploadingTime;
		$scope.unloadingAmt=unloadingAmt;
		$scope.lrUnloadingdate=lrUnloadingdate;
		$scope.detention=detention;
		$scope.othAmt=othAmt;
		$scope.recWeight=recWeight;
		$scope.recPkg=recPkg;
		$scope.remarks=remarks;
		$scope.claimPkg=claimPkg;
		$scope.claimwght=claimwght;
		$scope.gdClaimAmt=gdClaimAmt;
		$scope.reason=reason;
		
		if(recWeight<=challan.chlnTotalWt && recPkg<=challan.chlnNoOfPkg)
			{
		if(recWeight==challan.chlnTotalWt && recPkg==challan.chlnNoOfPkg)
		{
			console.log("Equal");
			if(remarks=="ok")
				{
				damOrShrtg="";
				weightOrPkg="";
				claimwght=0;
				claimPkg=0;
				var ackPackage={
						cnmtCode:cnmtCode,
				        arrivalReport:arrivalReport,
				        challan:challan.chlnCode,
				        consignor:consignor,
				        consignee:consignee,
				        cnmtDate:cnmtDate,
				        consignorCode:$scope.consignorCode,
				        consigneeCode:$scope.consigneeCode,
				        lryArTime:lryArTime,lrarrivalDate:lrarrivalDate,uploadingTime:uploadingTime,unloadingAmt:unloadingAmt,lrUnloadingdate:lrUnloadingdate,detention:detention,othAmt:othAmt,days:days,recWeight:recWeight,recPkg:recPkg,remarks:remarks,damOrShrtg:damOrShrtg,weightOrPkg:weightOrPkg,claimPkg:claimPkg,claimwght:claimwght,gdClaimAmt:gdClaimAmt,reason:reason
			
				};
		var response=$http.post($scope.projectName + '/updateAckValidation', ackPackage);

				$scope.AckValidatorDBFlag=false;
				$scope.ackFormData=true;
				$scope.ackValidationData.splice(0,1);
				$scope.lryArTime="";
				$scope.lrarrivalDate="";
				$scope.uploadingTime="";
				$scope.unloadingAmt="";
				$scope.lrUnloadingdate="";
				$scope.detention="";
				$scope.othAmt="";
				$scope.recWeight="";
				$scope.recPkg="";
				$scope.remarks="";
				$scope.claimPkg="";
				$scope.claimwght="";
				$scope.gdClaimAmt="";
				$scope.reason="";
				$scope.damOrShrtg="";
				$scope.weightOrPkg="";
				if($scope.ackValidationData.length===0)
					{
					$scope.AckValidatorDBFlag=true;
					$scope.getAckValidationForm=false;
					}
	
		}
			else
				{
				$scope.alertToast("Remark Should be Ok");
			
				}
			}
		else if(recWeight<challan.chlnTotalWt || recPkg<challan.chlnNoOfPkg)
			{
			console.log("1111111111111");
			if(remarks=="QUALIFIED")
				{
				console.log("ClaimPkg "+claimPkg);
				if(angular.isDefined(claimPkg)===false || claimPkg==='' )
					{
					claimPkg=0;
					}
				if(angular.isDefined(claimwght)==false || claimwght==='')
				{
					claimwght=0;
				}
				console.log("claimwght "+claimwght);
				var ackPackage={
						cnmtCode:cnmtCode,
				        arrivalReport:arrivalReport,
				        challan:challan.chlnCode,
				        consignor:consignor,
				        consignee:consignee,
				        cnmtDate:cnmtDate,
				        consignorCode:$scope.consignorCode,
				        consigneeCode:$scope.consigneeCode,
				        lryArTime:lryArTime,lrarrivalDate:lrarrivalDate,uploadingTime:uploadingTime,unloadingAmt:unloadingAmt,lrUnloadingdate:lrUnloadingdate,detention:detention,othAmt:othAmt,days:days,recWeight:recWeight,recPkg:recPkg,remarks:remarks,damOrShrtg:damOrShrtg,weightOrPkg:weightOrPkg,claimPkg:claimPkg,claimwght:claimwght,gdClaimAmt:gdClaimAmt,reason:reason
			
				};
		var response=$http.post($scope.projectName + '/updateAckValidation', ackPackage);
				
					
						$scope.AckValidatorDBFlag=false;
						$scope.ackFormData=true;
						$scope.ackValidationData.splice(0,1);
						$scope.lryArTime="";
						$scope.lrarrivalDate="";
						$scope.uploadingTime="";
						$scope.unloadingAmt="";
						$scope.lrUnloadingdate="";
						$scope.detention="";
						$scope.othAmt="";
						$scope.recWeight="";
						$scope.recPkg="";
						$scope.remarks="";
						$scope.claimPkg="";
						$scope.claimwght="";
						$scope.gdClaimAmt="";
						$scope.reason="";
						$scope.damOrShrtg="";
						$scope.weightOrPkg="";
						if($scope.ackValidationData.length===0)
							{
							$scope.AckValidatorDBFlag=true;
							$scope.getAckValidationForm=false;
							}
				}
			else
				{
				console.log("3333333333333333333333333")
				$scope.alertToast("Remark Should be Qualified");
				}
			}
	
			}
		else if(recWeight>challan.chlnTotalWt)
			{
			$scope.alertToast("Receive Weight can't be greater than Total Weight");
			}
		else if(recPkg>challan.chlnNoOfPkg)
			{
			$scope.alertToast("Receive Package can't be greater than Total Packages");
			}
	}
	
	
}]);
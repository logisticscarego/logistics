'use strict';

var app = angular.module('application');

app.controller('CashStmtCntlr',['$scope','$location','$http','$filter','$window','$log',
                                 function($scope,$location,$http,$filter,$window,$log){
    
	$scope.reportList = [];
	$scope.brhList = [];	
	$scope.brhFlag = true;
	$scope.branch = {};
	
	$scope.printVsFlag = true;
	
	$scope.acHdList = [];
	$scope.actCsList = [];
	$scope.bkCsMapList = [];
	
	$scope.cashCsList = [];
	$scope.bankCsList = [];
	$scope.bankCsTempList = [];
	$scope.contraCsList = [];
	$scope.toDate = "";
	
	$scope.openBal = 0;
	$scope.closeBal = 0;
	$scope.bkOpenBal = 0;
	$scope.bkCloseBal = 0;
	
	$scope.date = new Date();
	console.log("branch="+$scope.currentBranch);
	
	$scope.getBranch = function(){
		console.log("enter into getBranch function");
		
		var response = $http.post($scope.projectName+'/getBrhFrCS');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.brhList = data.list;
				console.log("size="+$scope.brhList.length);
				for (var i=0;i<$scope.brhList.length;i++){
					if($scope.brhList[i].branchName===$scope.currentBranch){
						$scope.branch=$scope.brhList[i];
						break;
					}
				}
			}else{
				console.log("error in fetching Branch Data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}                            
             
	
	$scope.openBrhDB = function(){
		console.log("enter into openBrhDB function");
		$scope.brhFlag = false;
    	$('div#brhId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.brhFlag = true;
		    }
			});
		$('div#brhId').dialog('open');	
	}
	
	
	$scope.saveBranch = function(branch){
		console.log("enter into saveBranch function");
		$('div#brhId').dialog('close');	
		$scope.branch = branch;
	}
	
	
	$scope.submitCS = function(cashStmtForm){
		console.log("enter into submitCS function = "+cashStmtForm.$invalid);
		if(cashStmtForm.$invalid){
			$scope.alertToast("please fill Branch and Date");
		}else{
			var reqData = {
					"brhId" 	:	$scope.branch.branchId,
					"date"  	:	$scope.csDate,
					"toDate"	:	$scope.toDate
			};
			var response = $http.post($scope.projectName+'/submitCStmt',reqData);
			response.success(function(data, status, headers, config){
				$log.info(data);
				console.log(data);
				if(data.result === "success"){
					$scope.reportList = data.reportList;
					$scope.actCsList = data.list;					
					$scope.acHdList = data.acHdList;					
					$scope.bkCsMapList = data.bkCsMapList;					
					$scope.openBal = data.openBal;					
					$scope.closeBal = data.closeBal;
					$scope.bkOpenBal = data.bkOpenBal;
					$scope.bkCloseBal = data.bkCloseBal;
					$scope.sheetNo = data.shtNo;
					$scope.brhName = data.brhName;
					
					console.log("###############");
					console.log(data.bkCsMapList);
					console.log("###############");
					
					if($scope.acHdList.length > 0){
						for(var i=0;i<$scope.acHdList.length;i++){
							for(var j=i+1;j<$scope.acHdList.length;j++){
								if($scope.acHdList[i].faCode === $scope.acHdList[j].faCode){
									$scope.acHdList.splice(j,1);
									j = j-1;
								}
							}
						}
					}
					console.log("new size of $scope.acHdList = "+$scope.acHdList.length);
					
			
					if($scope.actCsList.length > 0){
						for(var i=0;i<$scope.actCsList.length;i++){
							if($scope.actCsList[i].csVouchType === "CASH" || $scope.actCsList[i].csVouchType === "cash" ){
								$scope.cashCsList.push($scope.actCsList[i]);
							}else if($scope.actCsList[i].csVouchType === "BANK" || $scope.actCsList[i].csVouchType === "bank"){
								var subStr = $scope.actCsList[i].csFaCode.substr(0,2);
								if(subStr !== "07"){
									$scope.bankCsList.push($scope.actCsList[i]);
								}else{
									/*if($scope.actCsList[i].csType === "Fund Transfer"){
										$scope.bankCsList.push($scope.actCsList[i]);
									}else{
										$scope.bankCsTempList.push($scope.actCsList[i]);
									}*/
									
									$scope.bankCsList.push($scope.actCsList[i]);
									
									/*console.log(i+" ====> "+$scope.actCsList[i].csType);
									if($scope.actCsList[i].csType === "CashWithdraw(ATM)" || $scope.actCsList[i].csType === "CashWithdraw" || $scope.actCsList[i].csType === "CashDeposite"){
										$scope.bankCsTempList.push($scope.actCsList[i]);
										console.log(i+"************");
									}else{
										$scope.bankCsList.push($scope.actCsList[i]);
										//$scope.bankCsTempList.push($scope.actCsList[i]);
										console.log(i+"%%%%%%%%%%%%%%%");
									}*/
								}
							}else if($scope.actCsList[i].csVouchType === "CONTRA" || $scope.actCsList[i].csVouchType === "contra" || $scope.actCsList[i].csVouchType === "RTGS" || $scope.actCsList[i].csVouchType === "rtgs"|| $scope.actCsList[i].csVouchType === "PETRO"){
								$scope.contraCsList.push($scope.actCsList[i]);
								console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
							}else{
								console.log("*********invalid*********** "+i);
							}
							
						}
					}
					
					
					if($scope.cashCsList.length > 0){
						$scope.recAmt = $scope.openBal;
						$scope.payAmt = $scope.closeBal;
						for(var i=0;i<$scope.cashCsList.length;i++){
							if($scope.cashCsList[i].csDrCr === 'C'){
								$scope.recAmt = $scope.recAmt + $scope.cashCsList[i].csAmt;
							}else if($scope.cashCsList[i].csDrCr === 'D'){
								$scope.payAmt = $scope.payAmt + $scope.cashCsList[i].csAmt;
							}else{
								
							}
						}
					}else{
						$scope.recAmt = $scope.openBal;
						$scope.payAmt = $scope.closeBal;
					}
					
					
					
					if($scope.bankCsList.length > 0){
						$scope.recBnkAmt = $scope.bkOpenBal;
						$scope.payBnkAmt = $scope.bkCloseBal;
						for(var i=0;i<$scope.bankCsList.length;i++){
							if($scope.bankCsList[i].csDrCr === 'C'){
								$scope.recBnkAmt = $scope.recBnkAmt + $scope.bankCsList[i].csAmt;
							}else if($scope.bankCsList[i].csDrCr === 'D'){
								$scope.payBnkAmt = $scope.payBnkAmt + $scope.bankCsList[i].csAmt;
							}else{
								
							}
						}
					}else{
						$scope.recBnkAmt = $scope.bkOpenBal;
						$scope.payBnkAmt = $scope.bkCloseBal;
					}					
					
					$scope.printVsFlag = false;
			    	$('div#printVsDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					        $scope.printVsFlag = true;
					    }
						});
					$('div#printVsDB').dialog('open');
					
					
					
					
				}else{
					console.log("error in fetching Cash Stmt Data");
					$scope.alertToast("Please enter correct Branch and Date");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
		}
	}
	
	
	
	$scope.cancelPrint = function(){
		console.log("enter into cancelPrint function");
		$('div#printVsDB').dialog('close');
		$scope.acHdList = [];
		$scope.actCsList = [];
		
		$scope.cashCsList = [];
		$scope.bankCsList = [];
		$scope.bankCsTempList = [];
		$scope.contraCsList = [];
		
		$scope.openBal = 0;
		$scope.closeBal = 0;
		$scope.bkOpenBal = 0;
		$scope.bkCloseBal = 0;
	}	
	
	$scope.newCsPrint = function(){
		var content = document.getElementById("newCsPrint").innerHTML;
		var mywindow = window.open('', '_blank');
	    mywindow.document.write('<html><body>');
	    mywindow.document.write(content);
	    mywindow.document.write('</body></html>');
	    mywindow.document.close();
	    mywindow.print();
	    mywindow.close();
	    
	    $('div#printVsDB').dialog('close');
	}
	
	
	$scope.printVs = function(){
		console.log("enter into printVs function");
		
		//$window.print();
		
		$scope.newCsPrint();
		
		$scope.acHdList = [];
		$scope.actCsList = [];
		
		$scope.cashCsList = [];
		$scope.bankCsList = [];
		$scope.bankCsTempList = [];
		$scope.contraCsList = [];
		
		$scope.openBal = 0;
		$scope.closeBal = 0;
		$scope.bkOpenBal = 0;
		$scope.bkCloseBal = 0;
	}
                                 
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getBranch();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
}]);                 





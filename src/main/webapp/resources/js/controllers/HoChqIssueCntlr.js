'use strict';

var app = angular.module('application');

app.controller('HoChqIssueCntlr',['$scope','$location','$http','$filter','$window',
                                 function($scope,$location,$http,$filter,$window){

	console.log("HoChqIssueCntlr Started");
	
	$scope.branchList	= [];
	$scope.empList = [];
	$scope.bankMstrList = [];
	$scope.chqBookList = [];
	$scope.issueChqBookList = [];
	$scope.filterChqBookList = [];
	
	$scope.branch = {};
	$scope.bankMstr = {};
	$scope.chqBook = {};
	
	//$scope.IssueChqBook = {};
	
	$scope.totalIssueChq = [];
	$scope.toChqNo = "";
	$scope.payType = "";
	$scope.maxAmtLimit = "";
	
	$scope.authEmp1 = {};
	$scope.authEmp2 = {};
	
	$scope.branchDBFlag = true;
	$scope.bankMstrDBFlag = true;
	$scope.issueChqDBFlag = true;
	$scope.finalSubmitChqBookFlag = true;
	$scope.printChqBookDBFlag = true;
	$scope.authEmp1DBFlag = true;
	$scope.authEmp2DBFlag = true;
	$scope.manualChqBrBackDBFlag = true;
	
	$scope.manualChqBrBackFlag = false;
	//$scope.chqBookDBFlag = true;
	
	$scope.getAllBranchesList = function(){
		console.log("getAllBranchesList Entered");
		var response = $http.post($scope.projectName+'/getAllBranchesList');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getAllBranchesList Success");
				$scope.branchList = data.branchCodeList;
				if ($scope.assignBankRBFlag) {
					$('#selectBranchId').removeAttr("disabled");
				}
				
				for ( var i = 0; i < $scope.branchList.length; i++) {
					console.log("Branch Name: "+$scope.branchList[i].branchName);
					console.log("Branch Code: "+$scope.branchList[i].branchCode);
					console.log("Branch Address: "+$scope.branchList[i].branchAdd);
				}
				
				$scope.getListOfEmployee();//get the list of employee
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
	}
	
	//get list of employee
	$scope.getListOfEmployee = function() {
		var response = $http.post($scope.projectName+'/getListOfEmployee');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getListOfEmployee");
				$scope.empList = data.list;
				
				$('#authEmp1Id').removeAttr("disabled");
				$('#authEmp2Id').removeAttr("disabled");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
	}
	
	$scope.openBranchDB = function(){
		console.log("openBranchDB()");
		
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});

		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch =  function(branch){
		console.log("saveBranch()");
		console.log("enter into branchName----->"+branch.branchName);
		console.log("enter into branchFaCode----->"+branch.branchFaCode);
		$('#bankMstrId').attr("disabled","disabled");
		$scope.bankMstr = {};
		$scope.branch = branch;
		$('div#branchDB').dialog('close');
		
		var response = $http.post($scope.projectName+'/getChildBankMstrList', $scope.branch.branchId);
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				$('#bankMstrId').removeAttr("disabled");
				$scope.bankMstrList = data.bankMstrList;
				for ( var i = 0; i < $scope.bankMstrList.length; i++) {
					console.log("bankName==>"+$scope.bankMstrList[i].bnkName);
					console.log("bankFaCode==>"+$scope.bankMstrList[i].bnkFaCode);
				}
			}else {
				$('#bankMstrId').attr("disabled","disabled");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
		
	}
	
	$scope.openBankDB = function(){
		console.log("Entered into openBankDB");
		$scope.bankMstrDBFlag = false;
		
		$('div#bankMstrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bank",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.bankMstrDBFlag = true;
			}
		});

		$('div#bankMstrDB').dialog('open');
	}
	
	$scope.saveBank =  function(bankMstr){
		console.log("saveBank()");
		console.log("enter into branchName----->"+bankMstr.bnkName);
		console.log("enter into branchFaCode----->"+bankMstr.bnkFaCode);
		$scope.bankMstr = bankMstr;
		$('div#bankMstrDB').dialog('close');
	}
	
	//start from here
	$scope.openAuthEmp1DB = function(){
		console.log("openAuthEmp1DB()");
		
		$scope.authEmp1DBFlag = false;
		$('div#authEmp1DB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.authEmp1DBFlag = true;
			}
		});

		$('div#authEmp1DB').dialog('open');
	}
	
	$scope.saveAuthEmp1 =  function(employee){
		console.log("saveAuthEmp1()");
		$scope.authEmp1 = employee;
		$('div#authEmp1DB').dialog('close');
	}
	
	$scope.openAuthEmp2DB = function(){
		console.log("openAuthEmp2DB()");
		
		$scope.authEmp2DBFlag = false;
		$('div#authEmp2DB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.authEmp2DBFlag = true;
			}
		});

		$('div#authEmp2DB').dialog('open');
	}
	
	$scope.saveAuthEmp2 =  function(employee){
		console.log("saveAuthEmp2()");
		$scope.authEmp2 = employee;
		$('div#authEmp2DB').dialog('close');
	}
	
	$scope.issueChqBookTB = function(chqBook, $index) {
		console.log("issueChqBookTB()");
		$scope.chqBook = chqBook;
		$scope.chqBookIndex = $index;
		console.log("From: "+$scope.chqBook.chqBkFromChqNo);
		console.log("To: "+$scope.chqBook.chqBkToChqNo);
		
		
		$scope.issueChqDBFlag = false;
		$('div#issueChqDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Issuing Cheques",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.issueChqDBFlag = true;
			}
		});

		$('div#issueChqDB').dialog('open');
	}
	
	$scope.submitChequeBTN = function() {
		console.log("submitChequeBTN()");
		if (angular.isUndefined($scope.toChqNo) || $scope.toChqNo === "" || $scope.toChqNo === null) {
			$scope.alertToast("Enter valid to cheque no");
		}else {
			if (parseInt($scope.toChqNo)>parseInt($scope.chqBook.chqBkToChqNo)) {
				$scope.alertToast("Enter valid to cheque no");
			}else {
				
				if (parseInt($scope.toChqNo)<parseInt($scope.chqBook.chqBkFromChqNo)) {
					$scope.alertToast("Enter valid to cheque no");
				} else {
					$scope.totalIssueChq[$scope.chqBookIndex] = parseInt($scope.toChqNo) - parseInt($scope.chqBook.chqBkFromChqNo) + 1;
					$('#totalIssueChqId'+$scope.chqBookIndex).attr("disabled","disabled"); 
					//$scope.chqBook.chqBkToChqNo = $scope.toChqNo;
					$scope.issueChqBook = {};
					$scope.issueChqBook.chqBkToChqNo = $scope.toChqNo;
					$scope.issueChqBook.chqBkFromChqNo = $scope.chqBook.chqBkFromChqNo;
					$scope.issueChqBook.chqBkId = $scope.chqBook.chqBkId;
					$scope.issueChqBook.chqBkNOChq = $scope.totalIssueChq[$scope.chqBookIndex];
					$scope.issueChqBook.chqBkPayType = $scope.payType;
					$scope.issueChqBook.chqBkMaxAmtLimit = $scope.maxAmtLimit; 
					
					$scope.issueChqBookList.push($scope.issueChqBook);
					$scope.chqBook = {};
					$scope.toChqNo = "";
					$('div#issueChqDB').dialog('close');
				}
			}
		}
	}
	
	$scope.selectChqType = function() {
		console.log("selectChqType()")
		$scope.filterChqBookList = [];
		$scope.totalIssueChq = [];
		if ($scope.chqReqType == 'M') {
			console.log("Manual");
			//$scope.chqReqPrintTypeFlag = false;
			$('#chqReqPrintTypeId').attr("disabled","disabled");
			$scope.chqReqPrintType = "";
		} else if($scope.chqReqType == 'C') {
			console.log("Computerised");
			//$scope.chqReqPrintTypeFlag = true;
			$scope.chqReqPrintType = 'L';
			$('#chqReqPrintTypeId').removeAttr("disabled");
		}
	}
	
	$scope.selectChqPrintType = function() {
		console.log("selectChqPrintType()")
		$scope.filterChqBookList = [];
		$scope.totalIssueChq = [];
	}
	
	$scope.submitChqIssue = function(hoChqIssueForm) {
		console.log("submitChqIssue()");
		if (hoChqIssueForm.$invalid) {
			if (hoChqIssueForm.branchName.$invalid) {
				$scope.alertToast("please enter branch");
			} else if (hoChqIssueForm.bankMstrName.$invalid) {
				$scope.alertToast("please enter bank");
			} else if (hoChqIssueForm.chqReqTypeName.$invalid) {
				$scope.alertToast("please enter Cheque Type");
			} else if (hoChqIssueForm.authEmp1Name.$invalid) {
				$scope.alertToast("please enter authEmp1");
			} else if (hoChqIssueForm.authEmp2Name.$invalid) {
				$scope.alertToast("please enter authEmp2");
			} 
		}else {
			
			var response = $http.post($scope.projectName+'/getUnIssueChqBook', $scope.bankMstr);
			response.success(function(data, status, headers, config){
				
				if (data.result === "success") {
					console.log("Success");
					$scope.chqBookList = data.unIssueChqBookList;
					console.log("chqBook List length==>"+$scope.chqBookList.length);
					
					if ($scope.chqReqType == 'M') {
						console.log("manual cheques");
						$scope.filterChqBookList = [];
						for ( var i = 0; i < $scope.chqBookList.length; i++) {
							if ($scope.chqBookList[i].chequeRequest.cReqType == 'M') {
								$scope.filterChqBookList.push($scope.chqBookList[i]);
							}
						}
						if ($scope.filterChqBookList.length === 0) {
							$scope.alertToast("No Manual Cheques at ho")
						}
					}else {
						if ($scope.chqReqPrintType == 'L') {
							console.log("computorised+lazerjet");
							$scope.filterChqBookList = [];
							for ( var i = 0; i < $scope.chqBookList.length; i++) {
								if ($scope.chqBookList[i].chequeRequest.cReqPrintType == 'L') {
									$scope.filterChqBookList.push($scope.chqBookList[i]);
								}
							}
							if ($scope.filterChqBookList.length === 0) {
								$scope.alertToast("No Lazer Jet cheques at ho")
							}
						} else {
							console.log("computorised+dotmatrix");
							$scope.filterChqBookList = [];
							for ( var i = 0; i < $scope.chqBookList.length; i++) {
								if ($scope.chqBookList[i].chequeRequest.cReqPrintType == 'D') {
									$scope.filterChqBookList.push($scope.chqBookList[i]);
								}
							}
							if ($scope.filterChqBookList.length === 0) {
								$scope.alertToast("No Dot Matrix cheques at ho")
							}
						}
					}
					
				}else {
					$scope.alertToast("No Cheques at ho");
					$scope.chqBookList = [];
				}
			});
			response.error(function(data, status, headers, config){
				console.log("getUnIssueChqBook Error: "+data);
			});
			
			
		}
	}
	
	$scope.submitChequeBook = function() {
		console.log("submitChequeBook()");
		
		if (parseInt($scope.totalIssueChq.length) > 0) {
			
			$scope.finalSubmitChqBookFlag = false;
			$('div#finalSubmitChqBookDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Issuing cheques detail",
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
					$scope.finalSubmitChqBookFlag = true;
				}
			});

			$('div#finalSubmitChqBookDB').dialog('open');
		
		} else {
			$scope.alertToast("Please fill Issuing cheque");
		}
		
		
		
	}
	
	$scope.cancel = function() {
		$('div#finalSubmitChqBookDB').dialog('close');
	}
	
	//final submission
	$scope.finalSubmitChqBook = function() {
		console.log("finalSubmitChqBook()");
		
		var chqService = {
				"chequeBookList"	: $scope.issueChqBookList,
				"authEmp1"			: $scope.authEmp1,
				"authEmp2"			: $scope.authEmp2
		 };
		
		var response = $http.post($scope.projectName+'/issueChqBook', chqService);
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("Success");
				$scope.bankMstrList = [];
				$scope.chqBookList = [];
				$scope.issueChqBookList = [];
				$scope.filterChqBookList = [];
				
				$scope.branch = {};
				$scope.bankMstr = {};
				$scope.chqBook = {};
				
				$scope.totalIssueChq = [];
				$scope.toChqNo = "";
				
				$scope.branchDBFlag = true;
				$scope.bankMstrDBFlag = true;
				$scope.issueChqDBFlag = true;
				
				$scope.maxAmtLimit = "";
				
				$scope.authEmp1 = {};
				$scope.authEmp2 = {};
				
				$('#bankMstrId').attr("disabled","disabled");
				
				$('div#finalSubmitChqBookDB').dialog('close');
				
				//open dialog for print
				$scope.printChqBookDBFlag = false;
		    	$('div#printChqBookDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.printChqBookDBFlag = true;
				    }
					});
				$('div#printChqBookDB').dialog('open');
				
			}else {
				console.log("Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("issueChqBook Error: "+data);
		});
	}
	
	$scope.cancelPrint = function() {
		console.log("cancelPrint()");
		$('div#printChqBookDB').dialog('close');
	}
	
	$scope.yesPrint = function() {
		console.log("yesPrint()");
		$('div#printChqBookDB').dialog('close');
		if ($scope.chqReqType === 'M') {
			
			if ($scope.payType === 'A') {
				$('div#manualPrintAC').addClass('printable');
				$('div#manualPrintAC').removeClass('hidden noprint takemeaway');
				
				$('div#manualPrintBR').addClass('hidden noprint takemeaway');
				$('div#manualPrintBR').removeClass('printable');
				
				$('div#manualPrintBackBR').addClass('hidden noprint takemeaway');
				$('div#manualPrintBackBR').removeClass('printable');
				
				$('div#lazerJetPrintAC').addClass( 'hidden noprint takemeaway' );
				$('div#lazerJetPrintAC').removeClass('printable');
				
				$('div#lazerJetPrintBR').addClass( 'hidden noprint takemeaway' );
				$('div#lazerJetPrintBR').removeClass('printable');
				
				$('div#dotMatrixPrintAC').addClass( 'hidden noprint takemeaway' );
				$('div#dotMatrixPrintAC').removeClass('printable');
				
				$('div#dotMatrixPrintBR').addClass( 'hidden noprint takemeaway' );
				$('div#dotMatrixPrintBR').removeClass('printable');
				
				$scope.manualChqBrBackFlag = false;
			} else {
				$('div#manualPrintAC').addClass('hidden noprint takemeaway');
				$('div#manualPrintAC').removeClass('printable');
				
				$('div#manualPrintBR').addClass('printable');
				$('div#manualPrintBR').removeClass('hidden noprint takemeaway');
				
				$('div#manualPrintBackBR').addClass('hidden noprint takemeaway');
				$('div#manualPrintBackBR').removeClass('printable');
				
				$('div#lazerJetPrintAC').addClass( 'hidden noprint takemeaway' );
				$('div#lazerJetPrintAC').removeClass('printable');
				
				$('div#lazerJetPrintBR').addClass( 'hidden noprint takemeaway' );
				$('div#lazerJetPrintBR').removeClass('printable');
				
				$('div#dotMatrixPrintAC').addClass( 'hidden noprint takemeaway' );
				$('div#dotMatrixPrintAC').removeClass('printable');
				
				$('div#dotMatrixPrintBR').addClass( 'hidden noprint takemeaway' );
				$('div#dotMatrixPrintBR').removeClass('printable');
				
				$scope.manualChqBrBackFlag = true;
			}
			
		}else {
			if ($scope.chqReqPrintType === 'L') {
				
				if ($scope.payType === 'A') {
					$('div#manualPrintAC').addClass('hidden noprint takemeaway');
					$('div#manualPrintAC').removeClass('printable');
					
					$('div#manualPrintBR').addClass('hidden noprint takemeaway');
					$('div#manualPrintBR').removeClass('printable');
					
					$('div#manualPrintBackBR').addClass('hidden noprint takemeaway');
					$('div#manualPrintBackBR').removeClass('printable');
					
					$('div#lazerJetPrintAC').addClass('printable');
					$('div#lazerJetPrintAC').removeClass( 'hidden noprint takemeaway' );
					
					$('div#lazerJetPrintBR').addClass( 'hidden noprint takemeaway' );
					$('div#lazerJetPrintBR').removeClass('printable');
					
					$('div#dotMatrixPrintAC').addClass( 'hidden noprint takemeaway' );
					$('div#dotMatrixPrintAC').removeClass('printable');
					
					$('div#dotMatrixPrintBR').addClass( 'hidden noprint takemeaway' );
					$('div#dotMatrixPrintBR').removeClass('printable');
				} else {
					$('div#manualPrintAC').addClass('hidden noprint takemeaway');
					$('div#manualPrintAC').removeClass('printable');
					
					$('div#manualPrintBR').addClass('hidden noprint takemeaway');
					$('div#manualPrintBR').removeClass('printable');
					
					$('div#manualPrintBackBR').addClass('hidden noprint takemeaway');
					$('div#manualPrintBackBR').removeClass('printable');
					
					$('div#lazerJetPrintAC').addClass( 'hidden noprint takemeaway' );
					$('div#lazerJetPrintAC').removeClass('printable');
					
					$('div#lazerJetPrintBR').addClass('printable');
					$('div#lazerJetPrintBR').removeClass( 'hidden noprint takemeaway' );
					
					$('div#dotMatrixPrintAC').addClass( 'hidden noprint takemeaway' );
					$('div#dotMatrixPrintAC').removeClass('printable');
					
					$('div#dotMatrixPrintBR').addClass( 'hidden noprint takemeaway' );
					$('div#dotMatrixPrintBR').removeClass('printable');
				}
				
				$scope.manualChqBrBackFlag = false;
				
			} else {
				
				if ($scope.payType === 'A') {
					$('div#manualPrintAC').addClass('hidden noprint takemeaway');
					$('div#manualPrintAC').removeClass('printable');
					
					$('div#manualPrintBR').addClass('hidden noprint takemeaway');
					$('div#manualPrintBR').removeClass('printable');
					
					$('div#manualPrintBackBR').addClass('hidden noprint takemeaway');
					$('div#manualPrintBackBR').removeClass('printable');
					
					$('div#lazerJetPrintAC').addClass( 'hidden noprint takemeaway' );
					$('div#lazerJetPrintAC').removeClass('printable');
					
					$('div#lazerJetPrintBR').addClass( 'hidden noprint takemeaway' );
					$('div#lazerJetPrintBR').removeClass('printable');
					
					$('div#dotMatrixPrintAC').addClass('printable');
					$('div#dotMatrixPrintAC').removeClass( 'hidden noprint takemeaway' );
					
					$('div#dotMatrixPrintBR').addClass( 'hidden noprint takemeaway' );
					$('div#dotMatrixPrintBR').removeClass('printable');
				} else {
					$('div#manualPrintAC').addClass('hidden noprint takemeaway');
					$('div#manualPrintAC').removeClass('printable');
					
					$('div#manualPrintBR').addClass('hidden noprint takemeaway');
					$('div#manualPrintBR').removeClass('printable');
					
					$('div#manualPrintBackBR').addClass('hidden noprint takemeaway');
					$('div#manualPrintBackBR').removeClass('printable');
					
					$('div#lazerJetPrintAC').addClass( 'hidden noprint takemeaway' );
					$('div#lazerJetPrintAC').removeClass('printable');
					
					$('div#lazerJetPrintBR').addClass( 'hidden noprint takemeaway' );
					$('div#lazerJetPrintBR').removeClass('printable');
					
					$('div#dotMatrixPrintAC').addClass( 'hidden noprint takemeaway' );
					$('div#dotMatrixPrintAC').removeClass('printable');
					
					$('div#dotMatrixPrintBR').addClass('printable');
					$('div#dotMatrixPrintBR').removeClass( 'hidden noprint takemeaway' );
				}
				
				$scope.manualChqBrBackFlag = false;
			}
		}
		$window.print();
		$scope.manualChqBrBack();
	}
	
	$scope.manualChqBrBack = function() {
		if ($scope.manualChqBrBackFlag) {
			
			$scope.manualChqBrBackDBFlag = false;
	    	$('div#manualChqBrBackDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.manualChqBrBackDB = true;
			    }
				});
			$('div#manualChqBrBackDB').dialog('open');
		}
	}
	
	$scope.cancelBackPrint = function() {
		console.log("cancelBackPrint()");
		$('div#manualChqBrBackDB').dialog('close');
	}
	
	$scope.yesBackPrint = function() {
		console.log("yesBackPrint()");
		
		$('div#manualPrintAC').addClass('hidden noprint takemeaway');
		$('div#manualPrintAC').removeClass('printable');
		
		$('div#manualPrintBR').addClass('hidden noprint takemeaway');
		$('div#manualPrintBR').removeClass('printable');
		
		$('div#manualPrintBackBR').addClass('printable');
		$('div#manualPrintBackBR').removeClass('hidden noprint takemeaway');
		
		$('div#lazerJetPrintAC').addClass( 'hidden noprint takemeaway' );
		$('div#lazerJetPrintAC').removeClass('printable');
		
		$('div#lazerJetPrintBR').addClass( 'hidden noprint takemeaway' );
		$('div#lazerJetPrintBR').removeClass('printable');
		
		$('div#dotMatrixPrintAC').addClass( 'hidden noprint takemeaway' );
		$('div#dotMatrixPrintAC').removeClass('printable');
		
		$('div#dotMatrixPrintBR').addClass( 'hidden noprint takemeaway' );
		$('div#dotMatrixPrintBR').removeClass('printable');
		
		$('div#manualChqBrBackDB').dialog('close');
		
		$window.print();
	}
	
	$scope.filterChqBook = function(value) {
		console.log("filer value: "+value);
		$scope.filterChqBookList = [];
		for ( var i = 0; i < $scope.chqBookList.length; i++) {
			if ($scope.chqBookList[i].chequeRequest.cReqType == value) {
				$scope.filterChqBookList.push($scope.chqBookList[i]);
			} else {
				
			}
		}
	}
	
	/*$scope.addClassDynamic = function() {
		console.log("Add dynamic");
		$('div#manualPrint').addClass('printable');
		$('div#manualPrint').removeClass('hidden noprint');
		
		$('div#lazerJetPrint').addClass( "hidden noprint" );
		$('div#lazerJetPrint').removeClass('printable');
		
		$('div#dotMatrixPrint').addClass( "hidden noprint" );
		$('div#dotMatrixPrint').removeClass('printable');
	}*/
	
	$('#maxAmtLimitId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#maxAmtLimitId').keypress(function(e) {
		if (this.value.length == 9) {
			e.preventDefault();
		}
	});
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		//$scope.addClassDynamic();
		$scope.getAllBranchesList();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("HoChqIssueCntlr Ended");
}]);
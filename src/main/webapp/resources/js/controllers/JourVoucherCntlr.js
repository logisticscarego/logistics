'use strict';

var app = angular.module('application');

app.controller('JourVoucherCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){
	
	$scope.vs = {};
	$scope.jv = {};
	
	$scope.jvList = [];
	$scope.dbAmtSum=0;
	$scope.crAmtSum=0;
	$scope.openCreditDBFlag = true;
	$scope.openDebitDBFlag = true;
	$scope.faCodeDBFlag = true;
	$scope.saveVsFlag = true;
	$scope.openChqNoDBFlag = true;
	$scope.isFormSubmitted = false;
	$scope.isCRFormSubmitted = false;
	$scope.isDRFormSubmitted = false;
	
	$scope.chqNoList = [];
	
	 $scope.getFYear = function() {
			var response = $http.get($scope.projectName+'/getCurntFYear');
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					console.log("Entered Date in current financial year");
					console.log(data.fYear.fyFrmDt);
					console.log(data.fYear.fyToDt);
					$scope.fyFrmDt=data.fYear.fyFrmDt;
					$scope.fyToDt=data.fYear.fyToDt;
				}else{
					$scope.alertToast("There is no any current Financial year");
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.alertToast("There is some problem in fetching date");
			});
	};
		
		$scope.isInFY = function(){
			console.log("isInFY------>Kamal");
			 var date = $filter('date')(new Date, "yyyy-MM-dd");
			  console.log("date= "+date);
			if($scope.vs.cashStmtStatus.cssDt<$scope.fyFrmDt || $scope.vs.cashStmtStatus.cssDt>$scope.fyToDt){
				$scope.vs.cashStmtStatus.cssDt="";
				$scope.alertToast("Date should be in current Financial year");
			}else if($scope.vs.cashStmtStatus.cssDt> date){
				  $scope.vs.cashStmtStatus.cssDt="";
					$scope.alertToast("Date cannot be greater than today");
				}
		}
	 
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		var response = $http.post($scope.projectName+'/getVDetFrJV');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.bankCodeList = data.bankCodeList;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.vs.voucherType = $scope.JR_VOUCHER;
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");			
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	$scope.getAllFaCode = function(keyCode){
		console.log("enter into getAllFaCode function");
		if(keyCode === 8)
			return;	
		if(parseInt($scope.jv.faCode.length) < 2)
			return;
		
		var response = $http.post($scope.projectName+'/getAllFaCodeFrCPVByFaName', $scope.jv.faCode);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.faMList = data.faMList;								
				$scope.openFaCodeBD();
			}else{
				console.log("Error in bringing FaCode");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	$scope.openFaCodeBD = function(){
		console.log("enter into openFaCodeBD function");
		$scope.faCodeDBFlag = false;
		$('div#faCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select FACode",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
		});
		$('div#faCodeDB').dialog('open');	
	}
	
	$scope.saveFACode = function(faMstr){
		console.log("enter into saveFACode fucntion");
		
		$scope.jv.faCode = "";
		$scope.faCodeDBFlag = true;
		$('div#faCodeDB').dialog('close');	
		
		$scope.jv.faCode = faMstr.faMfaCode;
		//get cheque of perticular bank
		$scope.chqNoList = [];
		$scope.jv.chqNo = "";
		$scope.getChqNoList(faMstr);
		
	}
	
	$scope.getChqNoList = function(faMstr) {
		if (faMstr.faMfaCode.substring(0,2) === "07") {
			var bankId = parseInt(faMstr.faMfaCode.substring(2)); 
			var res = $http.post($scope.projectName+'/getUnusedChqByBnk', bankId);
			res.success(function(data, status, headers, config) {
				if (data.result === "success") {
					console.log("getUnusedChqByBnk "+data.result);
					$scope.chqNoList = data.chqNoList;
				} else {
					console.log("getUnusedChqByBnk "+data.result);
				}
			});
			res.error(function(data, status, headers, config) {
				console.log("error in getUnusedChqByBnk responseL: "+data);
			});
		} else {
			$scope.chqNoList = [];
			$scope.jv.chqNo = "";
		}
	}
	
	$scope.openCreditAmt = function(){
		console.log("enter into openCreditAmt function");
		$scope.jv = {};
		$scope.openCreditDBFlag = false;
		$('div#openCreditDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Enter Credit Amount Details",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#openCreditDB').dialog('open');	
	}
	
	$scope.submitCRAmt = function(crAmtForm){
		console.log("enter into submitCRAmt function = "+crAmtForm.$invalid);
		if(crAmtForm.$invalid){
			$scope.isCRFormSubmitted = true;
		}else{
			$scope.isCRFormSubmitted = false;
			$scope.crAmtSum=$scope.crAmtSum+$scope.jv.crAmt;
			$scope.openCreditDBFlag = true;
			$('div#openCreditDB').dialog('close');	
			$scope.jv.dbAmt = 0.0;
			$scope.jvList.push($scope.jv);
		}
	}
	
	$scope.openDebitAmt = function(){
		console.log("enter into openDebitAmt function");
		$scope.jv = {};
		$scope.openDebitDBFlag = false;
		$('div#openDebitDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Enter Debit Amount Details",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#openDebitDB').dialog('open');	
	}
	
	$scope.submitDBAmt = function(dbAmtForm){
		console.log("enter into submitCRAmt function = "+dbAmtForm.$invalid);
		if(dbAmtForm.$invalid){
			$scope.isDRFormSubmitted = true;
		}else{
			$scope.isDRFormSubmitted = false;
			$scope.dbAmtSum=$scope.dbAmtSum+$scope.jv.dbAmt;
			$scope.openDebitDBFlag = true;
			$('div#openDebitDB').dialog('close');	
			$scope.jv.crAmt = 0.0;
			$scope.jvList.push($scope.jv);
		}
	}
	
	$scope.removeJV = function(index,jvM){
		console.log("Enter into removeJV function");
		$scope.crAmtSum=$scope.crAmtSum-jvM.crAmt;
		$scope.dbAmtSum=$scope.dbAmtSum-jvM.dbAmt;
		$scope.jvList.splice(index,1);
	}
	
	$scope.voucherSubmit = function(newVoucherForm){
		console.log("entre into voucherSubmit function = "+newVoucherForm.$invalid);
		if(newVoucherForm.$invalid){
			$scope.isFormSubmitted = true;
			$scope.alertToast("Please enter correct vouher details.");
		} else if ($scope.jvList.length == 0) {
			$scope.alertToast("Please enter voucher details.");
		} else {
			$scope.isFormSubmitted = false;
			var creditAmt = 0;
			var debitAmt = 0;
			
			for(var i=0;i<$scope.jvList.length;i++){
				if(parseFloat($scope.jvList[i].crAmt) > 0){
					creditAmt = creditAmt + parseFloat($scope.jvList[i].crAmt);
				}else if(parseFloat($scope.jvList[i].dbAmt) > 0){
					debitAmt = debitAmt + parseFloat($scope.jvList[i].dbAmt);
				}
			}
			
			console.log("creditAmt = "+creditAmt);
			console.log("debitAmt = "+debitAmt);
			
			creditAmt=Math.round(creditAmt*100)/100.
			debitAmt=Math.round(debitAmt*100)/100;
			
			console.log("creditAmt = "+creditAmt);
			console.log("debitAmt = "+debitAmt);
			
			if(creditAmt == debitAmt){
				console.log("final submittion");
			    $scope.saveVsFlag = false;
		    	$('div#saveVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					title: "Voucher Details",
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				    }
					});
				$('div#saveVsDB').dialog('open');
			}else{
				$scope.alertToast("Credit & debit amount are not equal.");
			}
		}
	}

	$scope.back = function(){
		console.log("enter into back function");
		$scope.saveVsFlag = true;
		$('div#saveVsDB').dialog('close');
	}
	
	$scope.saveVS = function(){
		 console.log("enter into saveVS function");
		 $('#saveId').attr("disabled","disabled");
		 var jvVouch = {
				 "voucherService" : $scope.vs,
		 		 "jvList"         : $scope.jvList
		 };
		 
		 var response = $http.post($scope.projectName+'/submitJPVoucherN',jvVouch);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.alertToast("success");
					 $scope.saveVsFlag = true;
					$('div#saveVsDB').dialog('close');
					$('#saveId').removeAttr("disabled");
					$scope.vs = {};
					$scope.jvList = [];
					$scope.crAmtSum = 0;
					$scope.dbAmtSum = 0;
					$scope.getVoucherDetails();
					
				}else{
					 $scope.saveVsFlag = true;
					$('div#saveVsDB').dialog('close');
					console.log("Error in submitVoucher");
					$scope.alertToast(data.msg);
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	 }
	
	$scope.openChqNoDB = function() {
		console.log("openChqNoDB()");
		$scope.openChqNoDBFlag = false;
		console.log("chq no list size: "+$scope.chqNoList.length);
    	$('div#openChqNoDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			title: "Cheque No.",
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.openChqNoDBFlag = true;
		    }
			});
		$('div#openChqNoDB').dialog('open');
	} 
	
	$scope.saveChqNo = function(chqNo) {
		console.log("saveChqNo()");
		$scope.jv.chqNo = chqNo;
		$('div#openChqNoDB').dialog('close');
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();
		 $scope.getFYear();
	}else if($scope.logoutStatus === true){
		 $location.path("/");
	}else{
		 console.log("****************");
	}
	
}]);
'use strict';

var app = angular.module('application');

app.controller('CNDetReportCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){

	$scope.custList = [];
	$scope.cnDetList = [];
	$scope.brhList = [];
	
	$scope.custDBFlag = true;
	$scope.brhDBFlag = true;
	
	$scope.comp = "COMPLETE REPORT";
	$scope.customer = {};
	$scope.branch = {};
	$scope.loadingFlag = false;
	
	$scope.downloadExl = function(){
		console.log("enter into downloadExl function");
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
	            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
	        });
	        saveAs(blob, "CNDetailReport.xls");
	} 
	
	
	$scope.getCmpRep = function(){
		console.log("enter into getCnReport funciton===>");
		 $scope.custList = [];
		 $scope.cnDetList = [];
		 $scope.brhList = [];
		 $('#allCnDetId').attr("disabled","disabled"); 
		 $('#custId').attr("disabled","disabled"); 
		 $scope.customerName = "";
		 $('#allCnDetCustId').attr("disabled","disabled"); 
		 $('#brhId').attr("disabled","disabled"); 
		 $scope.branchName = "";
		 $('#allCnDetBrhId').attr("disabled","disabled"); 
	/*	 $('#stDtId').attr("disabled","disabled");
		 $scope.strtDt = "";
		 $('#eDtId').attr("disabled","disabled");
		 $scope.endDt = "";*/
		 $('#subCustId').attr("disabled","disabled");
		 $('#subBrhId').attr("disabled","disabled");
		 $('#allCnDetDtId').attr("disabled","disabled");
		 
		/* var a=10;
		 for(var i=0;i<=a;i++){
			 if(a>1){
				 console.log("a is greater than i");
				 if(a<11){
					 console.log("a is less than 11");
					 break;
				 }
			 }
		 }*/
		 
		$('#compId').removeAttr('disabled'); 
		$('#subAllId').removeAttr('disabled'); 
	}
	
	
	$scope.submitAll = function(){
		console.log("enter into submitAll function");
		console.log("$scope.strtDt = "+$scope.strtDt);
		console.log("$scope.endDt = "+$scope.endDt);
		
		if(!angular.isUndefined($scope.strtDt) && !angular.isUndefined($scope.endDt)){
			var req = {
					"frDt" : $scope.strtDt,
					"toDt" : $scope.endDt
			};
			
			$scope.loadingFlag = true;
			var response = $http.post($scope.projectName+'/getCnReport',req);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.loadingFlag = false;
					$('#allCnDetId').removeAttr('disabled');
					
					$scope.cnDetList = data.list;
					console.log("$scope.cnDetList.length ===> "+$scope.cnDetList.length);
				}else{
					$scope.loadingFlag = false;
					$scope.alertToast("Server Error");
				}
			});
			response.error(function(data, status, headers, config){
				$scope.loadingFlag = false;
				console.log(data);
			});
		}else{
			$scope.alertToast("Please select Date");
		}
	}
	
	
	$scope.getCnRepCust = function(){
		console.log("enter into getCnRepCust funciton");
		 $scope.custList = [];
		 $scope.cnDetList = [];
		 $scope.brhList = [];
		 $('#allCnDetId').attr("disabled","disabled"); 
		 $('#custId').attr("disabled","disabled"); 
		 $scope.customerName = "";
		 $('#allCnDetCustId').attr("disabled","disabled"); 
		 $('#brhId').attr("disabled","disabled"); 
		 $scope.branchName = "";
		 $('#allCnDetBrhId').attr("disabled","disabled"); 
		/* $('#stDtId').attr("disabled","disabled");
		 $scope.strtDt = "";
		 $('#eDtId').attr("disabled","disabled");
		 $scope.endDt = "";*/
		 $('#subAllId').attr("disabled","disabled");
		 $('#subBrhId').attr("disabled","disabled");
		 $('#allCnDetDtId').attr("disabled","disabled");
		 
		 
		 $('#custId').removeAttr('disabled'); 
		var response = $http.post($scope.projectName+'/getCnRepCust');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.custList = data.list;
				$('#custId').removeAttr('disabled');
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	$scope.openCustDB = function(){
		console.log("enter into openCustDB funciton");
		$scope.custDBFlag=false;
		  $('div#custDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Select Customer",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
				  $scope.custDBFlag = true;
			  }
		  });
		  $('div#custDB').dialog('open');
	}
	
	
	$scope.getCnRepBrh = function(){
		 console.log("enter into getCnRepBrh funciton");
		 $scope.custList = [];
		 $scope.cnDetList = [];
		 $scope.brhList = [];
		 $('#allCnDetId').attr("disabled","disabled"); 
		 $('#custId').attr("disabled","disabled"); 
		 $scope.customerName = "";
		 $('#allCnDetCustId').attr("disabled","disabled"); 
		 $('#brhId').attr("disabled","disabled"); 
		 $scope.branchName = "";
		 $('#allCnDetBrhId').attr("disabled","disabled"); 
		 /*$('#stDtId').attr("disabled","disabled");
		 $scope.strtDt = "";
		 $('#eDtId').attr("disabled","disabled");
		 $scope.endDt = "";*/
		 $('#subAllId').attr("disabled","disabled");
		 $('#subCustId').attr("disabled","disabled");
		 $('#allCnDetDtId').attr("disabled","disabled");
	
		 $('#brhId').removeAttr('disabled'); 
		 
		var response = $http.post($scope.projectName+'/getCnRepBrh');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.brhList = data.list;
				$('#brhId').removeAttr('disabled');
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});	
	}
	
	
	$scope.openBrhDB = function(){
		console.log("enter into openBrhDB funciton");
		$scope.brhDBFlag=false;
		  $('div#brhDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Select Branch",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
				  $scope.brhDBFlag = true;
			  }
		  });
		  $('div#brhDB').dialog('open');
	}
	

	$scope.saveBranch = function(brh){
		console.log("enter into saveBranch function");
		 $('div#brhDB').dialog('close');
		 $scope.branchName = brh.branchName;
		 $scope.branch = brh;
		 $('#subBrhId').removeAttr('disabled'); 
	}
	
	
	$scope.submitBrh = function(){
		console.log("enter into submitBrh funciton");
		
		if(!angular.isUndefined($scope.strtDt) && !angular.isUndefined($scope.endDt)){
			var req = {
					"frDt"     : $scope.strtDt,
					"toDt"     : $scope.endDt,
					"brhCode" : $scope.branch.branchCode
			};
			$('#subBrhId').attr("disabled","disabled");
			var response = $http.post($scope.projectName+'/getCnRepFrBrh',req);
			 response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.cnDetList = data.list;
					console.log("$scope.cnDetList.length ===> "+$scope.cnDetList.length);
					$('#allCnDetBrhId').removeAttr('disabled');
					$('#subBrhId').removeAttr('disabled');
				}else{
					$scope.alertToast("Server Error");
					$('#subBrhId').removeAttr('disabled');
				}
			 });
			 response.error(function(data, status, headers, config){
				console.log(data);
				$('#subBrhId').removeAttr('disabled');
			 });
		}else{
			$scope.alertToast("Please Select Date");
		}
	}
	
	
	$scope.saveCustomer = function(cust){
		console.log("enter into saveCustomer function");
		$('div#custDB').dialog('close');
		$scope.customerName = cust.custName;
		$('#subCustId').removeAttr('disabled'); 
		$scope.customer = cust;
	}
	
	
	$scope.submitCust = function(){
		console.log("enter into submitCust funciton");
		
		if(!angular.isUndefined($scope.strtDt) && !angular.isUndefined($scope.endDt)){
			var req = {
					"frDt"     : $scope.strtDt,
					"toDt"     : $scope.endDt,
					"custCode" : $scope.customer.custCode
			};
			$('#subCustId').attr("disabled","disabled");
			var response = $http.post($scope.projectName+'/getCnRepFrCust',req);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.cnDetList = data.list;
					console.log("$scope.cnDetList.length ===> "+$scope.cnDetList.length);
					$('#allCnDetCustId').removeAttr('disabled');
					$('#subCustId').removeAttr('disabled');
				}else{
					$('#subCustId').removeAttr('disabled');
					$scope.alertToast("Server Error");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
				$('#subCustId').removeAttr('disabled');
			});	
		}else{
			$scope.alertToast("Please Select Date");
			$('#subCustId').removeAttr('disabled');
		}
	} 
	
	
	$scope.getCnRepDt = function(){
		 console.log("enter into getCnRepDt function");
		 $scope.custList = [];
		 $scope.cnDetList = [];
		 $scope.brhList = [];
		 $('#allCnDetId').attr("disabled","disabled"); 
		 $('#custId').attr("disabled","disabled"); 
		 $scope.customerName = "";
		 $('#allCnDetCustId').attr("disabled","disabled"); 
		 $('#brhId').attr("disabled","disabled"); 
		 $scope.branchName = "";
		 $('#allCnDetBrhId').attr("disabled","disabled"); 
		 $('#stDtId').attr("disabled","disabled");
		 $scope.strtDt = "";
		 $('#eDtId').attr("disabled","disabled");
		 $scope.endDt = "";
		 $('#allCnDetDtId').attr("disabled","disabled");
		
		
		$('#stDtId').removeAttr('disabled');
		$('#eDtId').removeAttr('disabled');
	}
	
	
	$scope.checkDt = function(){
		console.log("entre into checkDt funciton");
		if(!angular.isUndefined($scope.strtDt) && !angular.isUndefined($scope.endDt)){
			
			var date = {
					"stDt"  : $scope.strtDt,
					"endDt" : $scope.endDt
			};
			
			var response = $http.post($scope.projectName+'/getCnRepFrDate',date);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.cnDetList = data.list;
					console.log("$scope.cnDetList.length ===> "+$scope.cnDetList.length);
					$('#allCnDetDtId').removeAttr('disabled');
				}else{
					$scope.alertToast("Server Error");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});	
		}
	}
	
	
	
	if($scope.adminLogin === true || $scope.superAdminLogin === true){
		//$scope.getCnReport();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
}]);
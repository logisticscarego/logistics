'use strict';

var app = angular.module('application');

app.controller('CustomerCntlr',['$scope','$location','$http','$window','$filter',
                                function($scope,$location,$http,$window,$filter){

	$scope.temp={};
	$scope.customer = {};
	$scope.show = "true";
	$scope.selection=[];
	$scope.BranchCodeDBFlag=true;
	$scope.CrdCbFlag=true;
	$scope.custrep = {};
	$scope.custDirectorDBFlag=true;
	$scope.custMktHeadDBFlag=true;
	$scope.custCorpExcDBFlag=true;
	$scope.custBrPersonDBFlag=true;
	$scope.custRepFlag=false;
	$scope.billFlag=true;
	$scope.viewCustDetailsFlag=true;
	$scope.customer.custGstNo="";

	
	$scope.afterSaveDBFlag = true;
	$scope.afterSaveCustCode = "";
	$scope.afterSaveCRepCode = "";
	$scope.branchFaCode = "";
	
	$scope.state = {};
	$scope.stateCodeDBFlag = true;

	
	

	$('#pin').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#pin').keypress(function(e) {
		if (this.value.length == 6) {
			e.preventDefault();
		}
	});

	$('#custBillValueId').keypress(function(e) {
		if (this.value.length == 2) {
			e.preventDefault();
		}
	});

	$('#custPanNo').keypress(function(e) {
		if (this.value.length == 10) {
			e.preventDefault();
		}
	});

	$('#crMobileNo').keypress(function(e) {
		if (this.value.length == 10) {
			e.preventDefault();
		}
	});


	$('#custTinNo').keypress(function(e) {
		if (this.value.length == 12) {
			e.preventDefault();
		}
	});

	$('#fname').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});

	$('#add').keypress(function(e) {
		if (this.value.length == 255) {
			e.preventDefault();
		}
	});

	$('#city').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});

	$('#state').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});

	$('#circle').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});

	$('#tdscity').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});

	$('#crMobileNo').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#crMobileNo').keypress(function(e) {
		if (this.value.length == 15) {
			e.preventDefault();
		}
	});


	$('#crName').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});


	$('#custRefNo').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});

	$('#custCRPeriod').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#custCRPeriod').keypress(function(e) {
		if (this.value.length == 3) {
			e.preventDefault();
		}
	});

	$('#crDesignation').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});

	$scope.getCustDesigList = function(custrep){
		$( "#crDesignation" ).autocomplete({
			source: $scope.availableTags
		});
	} 


	
	
	

	
	$scope.openStateGSTCodeDB = function(){

		if($scope.stateList == null || $scope.stateList == []){

			var response = $http.post($scope.projectName+'/getStateGSTList');
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.stateList = data.stateList;
					$scope.successToast(data.result);
					console.log("data from server-->"+$scope.stateList);
					
					$scope.stateCodeDBFlag = false;
					$('div#stateCodeDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						position: UDPos,
						show: UDShow,
						hide: UDHide,
						title: "State GST Code",
						draggable: true,
						close: function(event, ui) { 
				            $(this).dialog('destroy');
				            $(this).hide();
				        }
					});
				
					$('div#stateCodeDB').dialog('open');
					
					
				}else{
					console.log(data);
				}	
			});
			response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
			});
		
		}else{
			$scope.stateCodeDBFlag = false;
			$('div#stateCodeDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "State GST Code",
				draggable: true,
				close: function(event, ui) { 
		            $(this).dialog('destroy');
		            $(this).hide();
		        }
			});
		
			$('div#stateCodeDB').dialog('open');
			
		}	
  }

	$scope.saveStateCode =  function(state){
		console.log("enter into saveBranchStationCode----->"+state.stateGST);
		$scope.stateName = state.stateName;
		$scope.stateGST = state.stateGST;
		$scope.customer.stateGST = state.stateGST;
		$('div#stateCodeDB').dialog('close');
		$scope.stateCodeDBFlag = true;
	}

	
	
	
	
	$scope.OpenBranchCodeDB = function(){
		$scope.BranchCodeDBFlag=false;
		$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Branch Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#branchCodeDB').dialog('open');
	}

	$scope.OpenCRDB = function(){
		$scope.CrdCbFlag=false;
		$('div#crdCb').dialog({
			autoOpen: false,
			modal:true,
			title: "Customer Representative Details",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#crdCb').dialog('open');
	}

	$scope.OpenDirectorDB = function(){
		$scope.custDirectorDBFlag=false;
		$('div#custDirectorDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Director",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#custDirectorDB').dialog('open');
	}

	$scope.OpenMarketingDB = function(){
		$scope.custMktHeadDBFlag=false;
		$('div#custMktHeadDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Marketing Head",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#custMktHeadDB').dialog('open');
	}

	$scope.OpenExecutiveDB = function(){
		$scope.custCorpExcDBFlag=false;
		$('div#custCorpExcDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Corporate Executive",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#custCorpExcDB').dialog('open');
	}

	$scope.OpenBranchPersonDB = function(){
		$scope.custBrPersonDBFlag=false;
		$('div#custBrPersonDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Branch Head",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#custBrPersonDB').dialog('open');
	}

	$scope.fillCustDesigVal = function(){
		console.log("enter into myfun----------->>"+$( "#crDesignation" ).val());
		$scope.custrep.crDesignation=$( "#crDesignation" ).val();
	}

	$scope.getDesignation = function(){
		var response = $http.get($scope.projectName+'/getDesignationForCr');
		response.success(function(data, status, headers, config) {
			$scope.availableTags = data;
			console.log("******************");
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.getBranchData = function(){
		console.log("getBranchData------>");
		var response = $http.post($scope.projectName+'/getBranchDataForCustomer');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.branchList = data.list;
				$scope.getListOfEmployee();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers,config) {
			$scope.errorToast(data);
		});
	}

	$scope.getListOfEmployee = function(){
		console.log(" entered into getListOfEmployee------>");
		var response = $http.post($scope.projectName+'/getListOfEmpforCustomer');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.employeeList = data.list;
				$scope.successToast(data.result);
				$scope.getDesignation();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.saveCustDirector =  function(empCustDirector){
		console.log("enter into saveCustDirector----->");
		$scope.temp.custDirector = empCustDirector.empName;
		$scope.customer.custDirector = empCustDirector.empCode;
		$('div#custDirectorDB').dialog('close');
		$scope.custDirectorDBFlag = true;	
	}

	$scope.saveCustMktHead =  function(empCustMktHead){
		console.log("enter into saveCustMktHead----->");
		$scope.temp.custMarketingHead = empCustMktHead.empName;
		$scope.customer.custMktHead = empCustMktHead.empCode;
		$('div#custMktHeadDB').dialog('close');
		$scope.custMktHeadDBFlag = true;	
	}

	$scope.saveCustCorpExc =  function(empCustCorpExc){
		console.log("enter into saveCustCorpExc----->");
		$scope.temp.custCorpExecutive = empCustCorpExc.empName;
		$scope.customer.custCorpExc = empCustCorpExc.empCode;
		$('div#custCorpExcDB').dialog('close');
		$scope.custCorpExcDBFlag = true;	
	}

	$scope.saveCustBrPerson =  function(empCustBrPerson){
		console.log("enter into saveCustBrPerson----->");
		$scope.temp.custBranchPerson = empCustBrPerson.empName;
		$scope.customer.custBrPerson = empCustBrPerson.empCode;
		$('div#custBrPersonDB').dialog('close');
		$scope.custBrPersonDBFlag = true;	
	}


	$scope.saveBranchCode = function(branch){
		$scope.branchFaCode = branch.branchFaCode;
		$scope.customer.branchCode = branch.branchCode;
		$('div#branchCodeDB').dialog('close');
		$scope.BranchCodeDBFlag=true;
	}

	$scope.savecustIsDailyContAllow = function(){
		if($scope.checkcustIsDailyContAllow){
			$scope.customer.custIsDailyContAllow="yes";
		}else{
			$scope.customer.custIsDailyContAllow="no";
		}	  
	}

	$scope.submitCustomer = function(CustomerForm,customer,custrep){
		console.log("enter into Submit function CustomerForm.$invalid--->"+CustomerForm.$invalid);

		if(CustomerForm.$invalid){
			if(CustomerForm.custName.$invalid){
				$scope.alertToast("please enter customer name..");
			}else if(CustomerForm.branchCode.$invalid){
				$scope.alertToast("please enter branch Code..");
			}else if(CustomerForm.GSTName.$invalid){
				$scope.alertToast("please enter Valid GST No..");
			}else if(CustomerForm.custAdd.$invalid){
				$scope.alertToast("please enter customer address..");
			}else if(CustomerForm.custCity.$invalid){
				$scope.alertToast("please enter customer city..");
			}else if(CustomerForm.custState.$invalid){
				$scope.alertToast("please enter customer state..");
			}else if(CustomerForm.custPin.$invalid){
				$scope.alertToast("Please enter PIN number of 6 digits..");
			}else if(CustomerForm.custTdsCircle.$invalid){
				$scope.alertToast("please enter customer tds circle..");
			}else if(CustomerForm.custTdsCity.$invalid){
				$scope.alertToast("please enter customer tds city..");
			}else if(CustomerForm.custPanNo.$invalid){
				$scope.alertToast("Please enter PAN number of 10 digits..");
			}else if(CustomerForm.custTinNo.$invalid){
				$scope.alertToast("Please enter TIN number of 8-12 digits..");
			}else if(CustomerForm.custStatus.$invalid){
				$scope.alertToast("Please enter customer status..");
			}else if(CustomerForm.custCRPeriod.$invalid){
				$scope.alertToast("Please enter correct CRPeriod..");
			}else if(CustomerForm.custDirector.$invalid){
				$scope.alertToast("Please enter director.");
			}else if(CustomerForm.custMarketingHead.$invalid){
				$scope.alertToast("Please enter marketing head.");
			}else if(CustomerForm.custCorpExecutive.$invalid){
				$scope.alertToast("Please enter executive head.");
			}else if(CustomerForm.custBranchPerson.$invalid){
				$scope.alertToast("Please enter branch head.");
			}else if(CustomerForm.crSrvTaxBy.$invalid){
				$scope.alertToast("Please enter service tax.");
			}else if(CustomerForm.crBase.$invalid){
				$scope.alertToast("Please enter bill cycle.");
			}
		}else{

			/*if(customer.custSrvTaxBy === "Customer"){
	    		customer.custSrvTaxBy = 'C';
	    	}else if(customer.custSrvTaxBy === "SECL"){
	    		customer.custSrvTaxBy = 'S';

	    	}else*/
			console.log($scope.custRepFlag);
			console.log($scope.customer.custBillCycle);

			/*if($scope.custRepFlag===false){
				if(custrep.custRefNo===null || custrep.custRefNo==="" || angular.isUndefined(custrep.custRefNo) || custrep.crName===null || custrep.crName==="" || angular.isUndefined(custrep.crName)
						|| custrep.crDesignation===null || custrep.crDesignation ==="" || angular.isUndefined(custrep.crDesignation) || custrep.crMobileNo===null || custrep.crMobileNo==="" || angular.isUndefined(custrep.crMobileNo)	
						|| custrep.crEmailId ===null || custrep.crEmailId==="" || angular.isUndefined(custrep.crEmailId)){
					$scope.alertToast("please Enter Customer Representative");
				}
			}else */if($scope.billFlag===false){
				$scope.alertToast("Enter correct bill value");
			}
			else{
				$scope.custrep.crName = custrep.crFirstName+" "+custrep.crLastName; 
				/*console.log($scope.custrep.crFirstName+$scope.custrep.crLastName);
	    			$scope.custrep.crName = $scope.custrep.crFirstName+" " +$scope.custrep.crLastName;
	    	 var custCustRep = {
	    			 "customer"    				: $scope.customer,
	    			 "customerRepresentative"   : $scope.custrep	 
	    	 };*/

				$scope.viewCustDetailsFlag=false;
				$('div#viewCustDetailsDB').dialog({
					autoOpen: false,
					modal:true,
					title: "Customer Info",
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					resizable: false,
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy') ;
						$(this).hide();
					}
				});

				$('div#viewCustDetailsDB').dialog('open');
			}
		}
	}

	$scope.saveCustomer = function(customer){
		console.log("Entered into saveCustomer function----> ");

		console.log($scope.custrep.crFirstName+$scope.custrep.crLastName);
		$scope.custrep.crName = $scope.custrep.crFirstName+" " +$scope.custrep.crLastName;
		
		var custCustRep = {};
		
		if($scope.custRepFlag===false){
			if($scope.custrep.custRefNo===null || $scope.custrep.custRefNo==="" || angular.isUndefined($scope.custrep.custRefNo) || $scope.custrep.crName===null || $scope.custrep.crName==="" || angular.isUndefined($scope.custrep.crName)
					|| $scope.custrep.crDesignation===null || $scope.custrep.crDesignation ==="" || angular.isUndefined($scope.custrep.crDesignation) || $scope.custrep.crMobileNo===null || $scope.custrep.crMobileNo==="" || angular.isUndefined($scope.custrep.crMobileNo)	

					|| $scope.custrep.crEmailId ===null || $scope.custrep.crEmailId==="" || angular.isUndefined($scope.custrep.crEmailId)){
				custCustRep	 = {
						"customer"  : $scope.customer
				};
			}
		}else{
			custCustRep = {
					"customer"    				: $scope.customer,
					"customerRepresentative"   : $scope.custrep	 
			};
		}
		
		

		$('#saveBtnId').attr("disabled","disabled");
		
		var response = $http.post($scope.projectName+'/submitCustAndCustRep',custCustRep);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$('#saveBtnId').removeAttr("disabled");
				
				$('div#viewCustDetailsDB').dialog('close');
				console.log(data.result);
				/*$scope.successToast(data.result);
				$scope.successToast("customer Code ------> "+data.custFaCode);
				$scope.successToast("customer Reperesentative Code ------> "+data.custRepFaCode);*/
				
				$scope.afterSaveCustCode = data.custFaCode;
				$scope.afterSaveCRepCode = data.custRepFaCode;
				
				
				$scope.afterSaveDBFlag=false;
				$('div#afterSaveDB').dialog({
					autoOpen: false,
					modal:true,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					resizable: false,
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy') ;
						$(this).hide();
					}
				});

				$('div#afterSaveDB').dialog('open');
				
				
				$scope.branchFaCode = "";
				$scope.temp.custDirector = "";
				$scope.temp.custMarketingHead = "";
				$scope.temp.custCorpExecutive = "";
				$scope.temp.custBranchPerson = "";
				$scope.customer.branchCode="";
				$scope.customer.custName="";
				$scope.customer.custAdd="";
				$scope.customer.custCity="";
				$scope.customer.custState="";
				$scope.customer.custPin="";
				$scope.customer.custTdsCircle="";
				$scope.customer.custTdsCity="";
				$scope.customer.custPanNo="";
				$scope.customer.custTinNo="";
				$scope.customer.custStatus="";
				$scope.customer.custDirector="";
				$scope.customer.custMktHead="";
				$scope.customer.custCorpExc="";
				$scope.customer.custBrPerson="";
				$scope.customer.custCrPeriod="";
				$scope.customer.custCrBase="";
				$scope.customer.custSrvTaxBy="";
				$scope.customer.custBillCycle="";
				$scope.customer.custBillValue="";
				$scope.custrep.custRefNo="";
				$scope.custrep.crFirstName="";
				$scope.custrep.crLastName="";
				$scope.custrep.crDesignation="";
				$scope.custrep.crMobileNo="";
				$scope.custrep.crEmailId="";

			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.closeViewCustDetailsDB = function(){
		$('div#viewCustDetailsDB').dialog('close');
	}
	
	
	
	$scope.afterSaveClose = function(){
		$('div#afterSaveDB').dialog('close');
		$scope.afterSaveCustCode = "";
		$scope.afterSaveCRepCode = "";
	}

	/*$scope.next = function(CustomerForm,customer){
		console.log("enter into next function CustomerForm.$invalid--->"+CustomerForm.$invalid);
		if(CustomerForm.$invalid){
			if(CustomerForm.custName.$invalid){
				$scope.alertToast("please enter customer name..");
			}else if(CustomerForm.custAdd.$invalid){
				$scope.alertToast("please enter customer address..");
			}else if(CustomerForm.custCity.$invalid){
				$scope.alertToast("please enter customer city..");
			}else if(CustomerForm.custState.$invalid){
				$scope.alertToast("please enter customer state..");
			}else if(CustomerForm.custPin.$invalid){
				$scope.alertToast("Please enter PIN number of 6 digits..");
			}else if(CustomerForm.custTdsCircle.$invalid){
				$scope.alertToast("please enter customer tds circle..");
			}else if(CustomerForm.custTdsCity.$invalid){
				$scope.alertToast("please enter customer tds city..");
			}else if(CustomerForm.custPanNo.$invalid){
				$scope.alertToast("Please enter PAN number of 10 digits..");
			}else if(CustomerForm.custTinNo.$invalid){
				$scope.alertToast("Please enter TIN number of 8-12 digits..");
			}else if(CustomerForm.custStatus.$invalid){
				$scope.alertToast("Please enter customer status..");
			}
	     }else{
				var response = $http.post($scope.projectName+'/customerRepresentative', customer);
				response.success(function(data, status, headers, config) {
				if(data.result === "success"){
					console.log(data);
						$location.path("/operator/"+data.targetPage);
						$scope.successToast(data.result);
				    }else{
				    	console.log(data);
				    }
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
			}
		}*/

	$scope.SubmitEditCustomerCode = function(custCode){
		console.log("enter into next function--->");
		var response = $http.post($scope.projectName+'/editsubmitcustomercode', custCode);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				console.log(data);
				$scope.customer = data.customer;
				$scope.show = "false";
				$scope.successToast(data.result);
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.SubmitEditCustomer = function(customer){
		console.log("enter into SubmitEditCustomer function--->"+customer.custName);
		var response = $http.post($scope.projectName+'/saveeditcustomer', customer);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				console.log(data);
				$scope.show = "true";
				$scope.custCode="";
				$scope.successToast(data.result);
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});

	}

	$scope.nextAddCR = function(AddCustomerRepForm,custrep){
		console.log("enter into next function AddCustomerRepForm.$invalid--->"+AddCustomerRepForm.$invalid);
		$scope.custRepFlag=true;
		$('div#crdCb').dialog('close');
		if(AddCustomerRepForm.$invalid){
			if(AddCustomerRepForm.crFirstName.$invalid){
				$scope.alertToast("please enter customer representative first name..");
			}else if(AddCustomerRepForm.crLastName.$invalid){
				$scope.alertToast("please enter customer representative last name..");
			}else if(AddCustomerRepForm.crDesignation.$invalid){
				$scope.alertToast("please enter customer representative designation address..");
			}else if(AddCustomerRepForm.crMobileNo.$invalid){
				$scope.alertToast("Please enter phone number of 4-15 digits..");
			}else if(AddCustomerRepForm.crEmailId.$invalid){
				$scope.alertToast("Please enter correct format of email id..");
			}
		}else{
			$scope.alertToast("success");
			console.log("custrep----->"+custrep)

			/*var response = $http.post($scope.projectName+'/CustomerRepresentative', custrep);
				response.success(function(data, status, headers, config) {
					if(data.result === "success"){
					console.log("data.result = "+data.result);
					$location.path("/operator/"+data.targetPage);
					$scope.successToast(data.result);
					$window.location.href = "/MyLogistics/"+data.targetPage;
					}else{
				    	console.log(data);
				    }
				});
				response.error(function(data, status, headers, config) {
					console.log("error in CustomerRepCntlr");
					$scope.errorToast(data);
				});*/
		}
	}	


	$scope.setCustBillValue = function(){
		console.log("enter into setCustBillValue function--->"+$scope.customer.custBillCycle);
		if($scope.customer.custBillCycle === 'D'){
			$('#custBillValueId').attr("disabled","disabled");
		}else if($scope.customer.custBillCycle === 'W' ){
			$('#custBillValueId').removeAttr("disabled");
		}else if($scope.customer.custBillCycle === 'M'){
			$('#custBillValueId').removeAttr("disabled");			
		}
		else if($scope.customer.custBillCycle === 'F'){
			$('#custBillValueId').removeAttr("disabled");
		}else{
			$('#custBillValueId').attr("disabled","disabled");
		}
	}

	$scope.checkBillValue = function(customer){
		console.log("Enter into check bill value function--"+$scope.customer.custBillCycle);
		if($scope.customer.custBillCycle === 'M'){

			if($scope.customer.custBillValue>31){
				$scope.alertToast("Enter number between 1-31");
				$scope.billFlag=false;
			}else{
				$scope.billFlag=true;
			}
		}
		else if($scope.customer.custBillCycle === 'F'){
			$('#custBillValueId').removeAttr("disabled");

			if($scope.customer.custBillValue>15){
				$scope.alertToast("Enter number between 1-15");
				$scope.billFlag=false;
			}else{
				$scope.billFlag=true;
			}
		}

		else if($scope.customer.custBillCycle === 'W'){
			$('#custBillValueId').removeAttr("disabled");

			if($scope.customer.custBillValue>7){
				$scope.alertToast("Enter number between 1-7");
				$scope.billFlag=false;
			}else{
				$scope.billFlag=true;
			}
		}	
	}

	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getBranchData();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
	
	
	

}]);


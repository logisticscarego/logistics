'use strict';

var app = angular.module('application');

app.controller('ChallanCntlr',['$scope','$location','$http','FileUploadService','$filter',
                               function($scope,$location,$http,FileUploadService,$filter){
	$scope.show = true;
	$scope.flag = false;
	$scope.openExtraLoadingDBFlag = true;
	$scope.chln = {};
	$scope.stList = [];
	$scope.noOf = 0;
	$scope.acWt = 0;
		
	
	$scope.myCnmtListFlag = false;
	$scope.cnmtNo = "";
	$scope.myCnmtList = [];
	$scope.noList = [];
	
	$scope.chd = {};
	var singleCnmtData = {};
	$scope.multiCnmtCodeList = [];
	$scope.mobListBk = [];
	$scope.mobListOw = [];
	$scope.vehList = [];
	$scope.chln.chlnFreight = 0;
	$scope.chln.chlnExtra = 0;
	$scope.chln.chlnLoadingAmt = 0;
	$scope.chln.chlnStatisticalChg = 0;
	$scope.chln.chlnTdsAmt = 0;
	$scope.mobile={};
	$scope.brkrcode="";
	$scope.ownrcode="";
	$scope.mobileNo = "";
	$scope.chlnLryRatePer = "";
	$scope.chlnChgWtPer = "";
	
	$scope.cnmtType = "";

	$scope.multiCnmtCode = "";
	$scope.cnmtCode = "";
	$scope.chlnTotalWtPer = "";

	var chlnAdvc;
	var autoCode;
	var cnmtCodeList = [];
	
	$scope.pkg = [];
	$scope.wt = [];

	$scope.BranchCodeDBFlag=true;
	$scope.ChallanFromStationDBFlag=true;
	$scope.ChallanToStationDBFlag=true;
	$scope.EmployeeCodeDBFlag=true;
	$scope.PayAtDBFlag=true;
	$scope.SedrDBFlag=true;
	$scope.CnmtDBFlag=true;
	$scope.MultiCnmtDBFlag=true;
	$scope.VehicleTypeDBFlag=true;
	$scope.ChdCodeCbFlag=true;
	$scope.ChdBrCodeDBFlag=true;
	$scope.ChdOwnCodeDBFlag=true;
	$scope.ChdPanIssueStDBFlag=true;
	$scope.PanIssueStFlag1=true;
	$scope.ChdPerStateCodeDBFlag=true;
	$scope.PerStateCodeFlag1=true;
	$scope.ChdBrMobNoDBFlag=true;
	$scope.ChdOwnMobNoDBFlag=true;
	$scope.OwnMobNoFlag1=true;
	$scope.BrMobNoFlag1=true;
	$scope.saveMobileFlag=true;
	$scope.saveMobileOwnFlag = true;
	$scope.sameNoFlag = false;
	$scope.Sflag=true;
	$scope.Mflag=true;
	$scope.ChallanCodeDBFlag=true;
	$scope.CDFlag=false;
	$scope.viewChallanDetailsFlag=true;
	$scope.vehDBFlag=true;


	$('#chlnCode').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});
	
	$('#chlnLryRate').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnChgWt').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnNoOfPkg').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#chlnNoOfPkg').keypress(function(e) {
		if (this.value.length == 7) {
			e.preventDefault();
		}
	});

	$('#chlnTotalWt').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnFreight').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnTotalFreight').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnLoadingAmt').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnExtra').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnAdvance').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnBrRate').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnStatisticalChg').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#chlnTdsAmt').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#chlnCode').keypress(function(e) {
		if (this.value.length == 7) {
			e.preventDefault();
		}
	});

	$('#chdChlnCode').keypress(function(e) {
		if (this.value.length == 7) {
			e.preventDefault();
		}
	});

	$('#mobileNo').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#mobileNo').keypress(function(e) {
		if (this.value.length === 10) {
			e.preventDefault();
		}
	});

	$('#mobileOwnNo').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});

	$('#mobileOwnNo').keypress(function(e) {
		if (this.value.length === 10) {
			e.preventDefault();
		}
	});

	$('#chdPanNo').keypress(function(e) {
		if (this.value.length == 10) {
			e.preventDefault();
		}
	});

	/*$('#chdPanNo').keypress(function(key) {
	      if(key.charCode < 46 || key.charCode > 57)
	          return false;
	  })*/;

	  // by kamal to get current financial year at loading time
	  $scope.getFYear = function(){
			var response = $http.get($scope.projectName+'/getCurntFYear');
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					console.log("Entered Date in current financial year");
					console.log(data.fYear.fyFrmDt);
					console.log(data.fYear.fyToDt);
					$scope.fyFrmDt=data.fYear.fyFrmDt;
					$scope.fyToDt=data.fYear.fyToDt;
				}else{
					$scope.alertToast("There is no any current Financial year");
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.alertToast("There is some problem in fetching date");
			});
		}();
	  
	  $scope.openExtraLoadingDB = function(){
		  $scope.openExtraLoadingDBFlag = false;
		  $('div#openExtraLoadingDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Extra Amount",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#openExtraLoadingDB').dialog('open');
	  }

	  $scope.OpenBranchCodeDB = function(){
		  $scope.BranchCodeDBFlag=false;
		  $('div#branchCodeDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Branch Code",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#branchCodeDB').dialog('open');
	  }
	  
	  $scope.setExtraAmt = function(){
		  
		  var totExtra = 
			  		parseFloat($scope.chln.chlnDetection)
	  				+ parseFloat($scope.chln.chlnToolTax)
	  				+ parseFloat($scope.chln.chlnHeight)
	  				+ parseFloat($scope.chln.chlnUnion)
	  				+ parseFloat($scope.chln.chlnTwoPoint)
	  				+ parseFloat($scope.chln.chlnWeightmentChg)
	  				+ parseFloat($scope.chln.chlnCraneChg)
	  				+ parseFloat($scope.chln.chlnOthers);
		  
		  $scope.chln.chlnExtra = totExtra;
		  $scope.calTotalFreight($scope.chln);
	  	
		  $scope.openExtraLoadingDBFlag = true;
		  $('div#openExtraLoadingDB').dialog('close'); 
		  
	  }		

	  $scope.OpenChallanFromStationDB = function(){		  
		  var station = $scope.frmStationCode;
		  var len = parseInt(station.length);
		  if(len < 3)
			  return;
		  
		  $scope.frmStationCode = "";
		  var response = $http.post($scope.projectName+'/getStationDataForChallanByStation', station);
		  response.success(function(data, status, headers, config){
			 if(data.result === "success"){
				 $scope.filterChallanFromStation = station;
				 $scope.stationList = data.list;
				 $scope.ChallanFromStationDBFlag=false;
				  $('div#challanFromStationDB').dialog({
					  autoOpen: false,
					  modal:true,
					  title: "Challan From Station",
					  show: UDShow,
					  hide: UDHide,
					  position: UDPos,
					  resizable: false,
					  draggable: true,
					  close: function(event, ui) { 
						  $(this).dialog('destroy') ;
						  $(this).hide();
					  }
				  });
				  $('div#challanFromStationDB').dialog('open');
			 } else{
				 $scope.alertToast("Station not found !");
			 }
		  });
		  
	  }
	  
	  $scope.saveExtraCharges = function(){
		  
		  
		  
	  }
	  
	  $scope.OpenChallanToStationDB = function(){
		  
		  var station = $scope.toStationCode;
		  var len = parseInt(station.length);
		  if(len < 3)
			  return;
		  
		  $scope.toStationCode = "";
		  var response = $http.post($scope.projectName+'/getStationDataForChallanByStation', station);
		  response.success(function(data, status, headers, config){
			 if(data.result === "success"){
				 $scope.filterChallanToStation = station;
				 $scope.stationList = data.list;
				  $scope.ChallanToStationDBFlag=false;
				  $('div#challanToStationDB').dialog({
					  autoOpen: false,
					  modal:true,
					  title: "Challan To Station",
					  show: UDShow,
					  hide: UDHide,
					  position: UDPos,
					  resizable: false,
					  draggable: true,
					  close: function(event, ui) { 
						  $(this).dialog('destroy') ;
						  $(this).hide();
					  }
				  });
				  $('div#challanToStationDB').dialog('open');
			 } else{
				 $scope.alertToast("Station not found !");
			 }		
		  });
	  }

	  $scope.openChallanCodeDB = function(){
		   
		  if($("#chlnCode").val().length < 4)
			  return;
		  
		  var chln = $scope.chln.chlnCode;		
		  $scope.chln.chlnCode = "";		  
		  var response = $http.post($scope.projectName+'/getBrStLeafDetDataByChln', chln);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.filterChallanCode = chln;	  
				  $scope.challanCodesList = data.list;
				  $scope.chln.branchCode = data.brhCode;
				  $scope.branchCodeTemp = data.brhName;	
				  $scope.ChallanCodeDBFlag=false;
				  $('div#ChallanCodeDB').dialog({
					  autoOpen: false,
					  modal:true,
					  title: "Challan Code",
					  show: UDShow,
					  hide: UDHide,
					  position: UDPos,
					  resizable: false,
					  draggable: true,
					  close: function(event, ui) { 
						  $(this).dialog('destroy') ;
						  $(this).hide();
					  }
				  });
				  $('div#ChallanCodeDB').dialog('open');
			  }else{
				  $scope.alertToast("Challan not found !");							  
			  } 
		  });
		
	  }


	  $scope.OpenEmployeeCodeDB = function(){
		  console.log("Enter into openEmployeeCodeDB()....");
		  var empCode = $scope.empTempCode;
		  var len = parseInt(empCode.length);
		  if(len < 3)
			  return;
		  		  		
		  $scope.empTempCode = "";		  
		  console.log("Hitting /getListOfEmployeeForChallanByEmpName");
		  var response = $http.post($scope.projectName+'/getListOfEmployeeForChallanByEmpName', empCode);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.filterEmployeeCode = empCode;
				  $scope.employeeList = data.list;
				  $scope.EmployeeCodeDBFlag=false;
				  $('div#employeeCodeDB').dialog({
					  autoOpen: false,
					  modal:true,
					  title: "Employee Code",
					  show: UDShow,
					  hide: UDHide,
					  position: UDPos,
					  resizable: false,
					  draggable: true,
					  close: function(event, ui) { 
						  $(this).dialog('destroy') ;
						  $(this).hide();
					  }
				  });
				  $('div#employeeCodeDB').dialog('open');
			  }else{
				  $scope.alertToast("Employee not found !");
			  }
		  });
		  
	  }

	  $scope.OpenPayAtDB = function(){
		  
		  console.log("Enter into openPayAtDB()....");
		  var payOnCode = $scope.payOnCode;
		  var len = parseInt(payOnCode.length);
		  if(len < 3)
			  return;		  		  		
		  $scope.payOnCode = "";  
		 
		  var response = $http.post($scope.projectName+'/getBranchDataForChallanByBrhName', payOnCode);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.filterPay = payOnCode;
				  $scope.branchList = data.list;				  
				  $scope.PayAtDBFlag=false;
				  $('div#payAtDB').dialog({
					  autoOpen: false,
					  modal:true,
					  resizable: false,
					  draggable: true,
					  close: function(event, ui) { 
						  $(this).dialog('destroy') ;
						  $(this).hide();
					  }
				  });
				  $('div#payAtDB').dialog('open');
				  
			  }else{
				  $scope.alertToast("Branch not found !");
			  }
		  });	  
		 
	  }

	  $scope.OpenCnmtDB = function(){	
		  
		  if($scope.cnmtType == "single"){
			  console.log("Enter into openCNMTDB()....");		  
			  $scope.CnmtDBFlag=false;
			  $('div#cnmtDB').dialog({
				  autoOpen: false,
				  modal:true,
				  title: "CNMT Code",
				  show: UDShow,
				  hide: UDHide,
				  position: UDPos,
				  resizable: false,
				  draggable: true,
				  close: function(event, ui) { 
					  $(this).dialog('destroy') ;
					  $(this).hide();
				  }
			  });
			  $('div#cnmtDB').dialog('open');
		  }
		  if($scope.cnmtType === "double"){			 
			  $scope.OpenMultiCnmtDB2();		  
		  }
		  
		  
	  }

	  $scope.OpenMultiCnmtDB2 = function(){
		  $scope.MultiCnmtDBFlag=false;		  
		  $('div#multiCnmtDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Multi CNMT Code",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });
		  $('div#multiCnmtDB').dialog('open');
		  $scope.fetchMultiCnmtList();
	  }

	  $scope.OpenVehicleTypeDB = function(){	 
				  
				  $scope.VehicleTypeDBFlag=false;
				  $('div#vehicleTypeDB').dialog({
					  autoOpen: false,
					  modal:true,
					  title: "Vihicle Type",
					  show: UDShow,
					  hide: UDHide,
					  position: UDPos,
					  resizable: false,
					  draggable: true,
					  close: function(event, ui) { 
						  $(this).dialog('destroy') ;
						  $(this).hide();
					  }
				  });
				  $('div#vehicleTypeDB').dialog('open');		
	  }


	  $scope.OpenChdCodeCbDB = function(){
		  $scope.ChdCodeCbFlag=false;
		  $('div#chdCodeCb').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Challan Detail of "+$scope.chd.chdChlnCode,
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdCodeCb').dialog('open');
	  }

	  $scope.openBrkMobileNo = function()
	  {

		  console.log("Enter into openMobileNo");
		  $scope.saveMobileFlag=false;
		  $('div#saveMobileID').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Save Mobile No",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });
		  console.log("Line no 342");
		  $('div#saveMobileID').dialog('open');	
	  }


	  $scope.saveBrkMobNo = function(saveMobileForm,mobileNo)
	  {

		  if(saveMobileForm.$invalid)
		  {
			  $scope.alertToast("Mobile number should be of 10 digits");
		  }else{
			  console.log("Enter into save Mob No=" +mobileNo);
			  console.log($scope.mobListBk);

			  if($.inArray(mobileNo, $scope.mobListBk) !== -1){
				  $scope.alertToast("Mobile Number already present");
				  $scope.mobileNo = "";

			  }else{				  
				  
				  $scope.saveMobileFlag = true;
				  $('div#saveMobileID').dialog('close');

				  var code = {
						  "mobileno" : mobileNo,
						  "code"     : $scope.brkrcode
				  }

				  var response = $http.post($scope.projectName+'/updateMoblieNo', code);
				  response.success(function(data, status, headers, config) {
					  if(data.result==="success"){
						  $scope.successToast(data.result);
						  //$scope.chd.chdBrMobNo = mobileNo;
						  $scope.mobileNo = "";
						  $scope.getMobileListBk($scope.brkrcode);
					  }else{
						  console.log(data);
					  }	
				  });
				  response.error(function(data, status, headers, config) {
					  $scope.errorToast(data);
				  });
			  }						
		  }	
	  }




	  $scope.openOwnMobileNo = function()
	  {

		  console.log("Enter into openMobileNo");
		  $scope.saveMobileOwnFlag = false;
		  $('div#saveMobileOwnID').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Save Mobile No",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });
		  $('div#saveMobileOwnID').dialog('open');
		  console.log("End of openOwnMobileNo");
	  }


	  $scope.saveOwnMobNolist = function(saveMobileOwnForm,mobileOwnNo)
	  {
		  console.log("Enter into saveOwnMobNolist funciton");

		  if(saveMobileOwnForm.$invalid){

			  $scope.alertToast("Mobile number should be of 10 digits");

		  }else{
			  console.log("Enter into saveMobNolist =" + mobileOwnNo);
			  console.log($scope.mobListOw);

			  if($.inArray(mobileOwnNo, $scope.mobListOw) !== -1){
				  $scope.alertToast("Mobile Number already present");
				  $scope.mobileOwnNo = "";
			  }else {


				  $scope.saveMobileOwnFlag = true;
				  $('div#saveMobileOwnID').dialog('close');

				  var code = {
						  "mobileno" : mobileOwnNo,
						  "code"     : $scope.ownrcode
				  }

				  var response = $http.post($scope.projectName+'/updateMoblieNo', code);
				  response.success(function(data, status, headers, config) {
					  if(data.result==="success"){
						  $scope.successToast(data.result);
						  //$scope.chd.chdOwnMobNo = mobileOwnNo;
						  $scope.mobileOwnNo = "";
						  $scope.getMobileListOw($scope.ownrcode);
					  }else{
						  console.log(data);
					  }	
				  });
				  response.error(function(data, status, headers, config) {
					  $scope.errorToast(data);
				  });		
			  }
		  }		
	  }



	  $scope.OpenBrokerCodeDB = function(){
		  console.log("Enter into OpenBrokerCodeDB()");
		  var brkName = $scope.chdBrCodeTemp;
		  var len = parseInt(brkName.length);
		  if(len < 3)
			  return;
		  
		  $scope.chdBrCodeTemp = "";
		  var response = $http.post($scope.projectName+'/getBrokerCodeForCDByBrkName', brkName);
		  response.success(function(data, status, headers, config){
			 if(data.result === "success"){
				 $scope.filterChdBrCode = brkName;
				  $scope.brkList = data.list;
				  $scope.ChdBrCodeDBFlag=false;
				  $('div#chdBrCodeDB').dialog({
					  autoOpen: false,
					  modal:true,
					  title: "Broker Code",
					  show: UDShow,
					  hide: UDHide,
					  position: UDPos,
					  resizable: false,
					  draggable: true,
					  close: function(event, ui) { 
						  $(this).dialog('destroy') ;
						  $(this).hide();
					  }
				  });
				  $('div#chdBrCodeDB').dialog('open');
			 } else{
				 $scope.alertToast("Broker not found !");
			 }		
		  });
		  
		 
	  }

	  $scope.OpenOwnerCodeDB = function(){		  
		  console.log("Enter into OpenOwnerCodeDB()");
		  var ownName = $scope.chdOwnCodeTemp;		
		  var len = parseInt(ownName.length);
		  if(len < 3)
			  return;
		  
		  $scope.chdOwnCodeTemp = "";
		  var response = $http.post($scope.projectName+'/getOwnerCodeForCDByName', ownName);
		  response.success(function(data, status, headers, config){
			 if(data.result === "success"){
				 $scope.filterChdOwnCode = ownName;
				 $scope.ownList = data.list;
				  $scope.ChdOwnCodeDBFlag=false;
				  $('div#chdOwnCodeDB').dialog({
					  autoOpen: false,
					  modal:true,
					  title: "Owner Code",
					  show: UDShow,
					  hide: UDHide,
					  position: UDPos,
					  resizable: false,
					  draggable: true,
					  close: function(event, ui) { 
						  $(this).dialog('destroy') ;
						  $(this).hide();
					  }
				  });
				  $('div#chdOwnCodeDB').dialog('open');
			 } else{
				 $scope.alertToast("Broker not found !");
			 }		
		  });	  
		  
		 
	  }

	  $scope.OpenchdPanIssueStDB = function(){
		  
		  var len = $scope.stList.length;
		  if(! len > 0){
			  $scope.getStateCodeList();  
		  }		  
		  
		  $scope.ChdPanIssueStDBFlag=false;
		  $('div#chdPanIssueStDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Pan Issue State",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdPanIssueStDB').dialog('open');
	  }

	  $scope.OpenchdPanIssueStDB1 = function(){
		  $scope.PanIssueStFlag1=false;
		  $('div#chdPanIssueStDB1').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Pan Issue State",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdPanIssueStDB1').dialog('open');
	  }

	  
	  $scope.OpenchdPerStateCodeDB = function(){
		  
		  var len = $scope.stList.length;
		  if(! len > 0){
			  $scope.getStateCodeList();  
		  }		  
		  
		  $scope.ChdPerStateCodeDBFlag=false;
		  $('div#chdPerStateCodeDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Per State Code",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdPerStateCodeDB').dialog('open');
	  }

	  $scope.OpenperStateCodeDB1 = function(){
		  $scope.PerStateCodeFlag1=false;
		  $('div#chdPerStateCodeDB1').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Per State Code",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdPerStateCodeDB1').dialog('open');
	  }

	  $scope.OpenBrMobNoDB = function(){
		  $scope.ChdBrMobNoDBFlag=false;
		  $('div#chdBrMobNoDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Broker Mobile Number",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdBrMobNoDB').dialog('open');
	  }

	  $scope.OpenBrMobNoDB1 = function(){
		  $scope.BrMobNoFlag1=false;
		  $('div#chdBrMobNoDB1').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Broker Mobile Numer",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdBrMobNoDB1').dialog('open');
	  }

	  $scope.OpenOwnMobNoDB = function(){
		  $scope.ChdOwnMobNoDBFlag=false;
		  $('div#chdOwnMobNoDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Owner Mobile Number",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdOwnMobNoDB').dialog('open');
	  }

	  $scope.OpenOwnMobNoDB1 = function(){
		  $scope.OwnMobNoFlag1=false;
		  $('div#chdOwnMobNoDB1').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Owner Mobile Number",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });

		  $('div#chdOwnMobNoDB1').dialog('open');
	  }

	  $scope.getBranchData = function(){
		  console.log("getBranchData------>");
		  var response = $http.post($scope.projectName+'/getBranchDataForChallan');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.branchList = data.list;
				  //$scope.getStationData();
				  $scope.removeAllMultiCnmt();
			  }else{
				  $scope.alertToast("you don't have any active branch");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers,config) {
			  $scope.errorToast(data);
		  });
	  }

	  $scope.getStationData = function(){
		  console.log("getStationData------>");
		  var response = $http.post($scope.projectName+'/getStationDataForChallan');
		  response.success(function(data, status, headers, config){
			  if(data.result==="success"){
				  $scope.stationList = data.list;
				  $scope.getEmployeeList();
			  }else{
				  $scope.alertToast("you don't have any station");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.getEmployeeList = function(){
		  console.log(" entered into getListOfEmployee------>");
		  var response = $http.post($scope.projectName+'/getListOfEmployeeForChallan');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.employeeList = data.list;
				  //$scope.getCnmtList();
				  $scope.getVehicleTypeCode();
			  }else{
				  $scope.alertToast("You don't have any employee list");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data);
		  });
	  }

	  $scope.getCnmtList = function(){
		  console.log(" entered into getCnmtList------>");		  
		  $scope.cnmtList = [];
		  var response = $http.post($scope.projectName+'/getCnmtListForChallanByCnmt', $scope.myCnmtList);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.cnmtList = data.list;
				  $scope.myCnmtListFlag = true;
				  $('div#myCnmtListDB').dialog('close');
				  $("#cnmtCodeId").removeAttr("disabled");
			  }else{
				  $scope.alertToast("you don't have any cnmt");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data);
		  });
	  }

	  $scope.getStateList = function(){
		  console.log("getStateList------>");
		  var response = $http.post($scope.projectName+'/getStateLryPrefixForChallan');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.stateList = data.list;
				  //$scope.getVehicleTypeCode();
				  $scope.getStationData();
				  console.log("data from server-->"+$scope.stateList);
			  }else{
				  $scope.alertToast("you don't have any state list");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data);
		  });
	  }

	  $scope.getVehicleTypeCode = function(){
		  console.log("getVehicleTypeCode------>");
		  var response = $http.post($scope.projectName+'/getVehicleTypeCodeForChallan');
		  response.success(function(data, status, headers, config){
			  if(data.result==="success"){
				  $scope.vtList = data.list;				  
			  }else{
				  $scope.alertToast("you don't have any vehicle type");
				  console.log(data);   
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.getBrokerCode = function(){
		  console.log("getBrokerCode------>");
		  var response = $http.post($scope.projectName+'/getBrokerCodeForCD');
		  response.success(function(data, status, headers, config){
			  if(data.result==="success"){
				  $scope.brkList = data.list;
				  $scope.getOwnerCode();
			  }else{
				  $scope.alertToast("you don't have any broker");
				  console.log(data);   
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.getOwnerCode = function(){
		  console.log("getOwnerCode------>");
		  var response = $http.post($scope.projectName+'/getOwnerCodeForCD');
		  response.success(function(data, status, headers, config){
			  if(data.result==="success"){
				  $scope.ownList = data.list;
				  $scope.getStateCodeList();
			  }else{
				  $scope.alertToast("you don't have any owner");
				  console.log(data);   
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.getStateCodeList = function(){
		  console.log("getStateCodeData------>");
		  var response = $http.post($scope.projectName+'/getStateCodeDataForCD');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.stList = data.list;
				  //$scope.getChallanCodes();
				  $scope.getBranchData();
				  console.log("data from server-->"+$scope.stList);
			  }else{
				  $scope.alertToast("you don't have any state");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data);
		  });
	  }

	  $scope.getMobileListBk = function(code){
		  console.log("getMobileList------>"+code);
		  $scope.brkrcode = code;
		  for(var i=0;i<$scope.mobListBk.length;i++){
			  $scope.mobListBk.splice(i,1);
		  }
		  var response = $http.post($scope.projectName+'/getMobileListForCD',code);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.mobListBk = data.list;
				  /*$scope.getChallanCodes();*/
				  console.log("data from server-->"+$scope.mobListBk);
			  }else{
				  $scope.alertToast("you don't have any mobile list");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data);
		  });
	  }

	  $scope.getMobileListOw = function(code){
		  console.log("getMobileList------>"+code);
		  $scope.ownrcode = code;
		  var response = $http.post($scope.projectName+'/getMobileListForCD',code);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.mobListOw = data.list;
				  /*$scope.getChallanCodes();*/
				  console.log("data from server-->"+$scope.mobListOw);
			  }else{
				  $scope.alertToast("you don't have any mobile list");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data);
		  });
	  }

	  $scope.saveEmployeeCode =  function(employee){
		  console.log("enter into saveEmployeeCode----->");
		  $scope.empTempCode = employee.empFaCode;
		  $scope.chln.chlnEmpCode = employee.empCode;
		  $('div#employeeCodeDB').dialog('close');
		  $scope.EmployeeCodeDBFlag=true;
	  }


	  $scope.saveBranchCode = function(branch){
		  $scope.branchCodeTemp = branch.branchName;
		  $scope.chln.branchCode = branch.branchCode;
		  $('div#branchCodeDB').dialog('close');
		  $scope.BranchCodeDBFlag=true;
	  }

	  $scope.savePayAt = function(branch){
		  $scope.payOnCode = branch.branchFaCode;
		  $scope.chln.chlnPayAt = branch.branchCode;
		  $('div#payAtDB').dialog('close');;
		  $scope.PayAtDBFlag=true;
	  }

	  $scope.saveFrmStnCode = function(station){
		  $scope.frmStationCode = station.stnName;
		  $scope.chln.chlnFromStn = station.stnCode;
		  $('div#challanFromStationDB').dialog('close');
		  $scope.ChallanFromStationDBFlag=true;
	  }

	  $scope.saveToStnCode = function(station){
		  $scope.toStationCode = station.stnName;
		  $scope.chln.chlnToStn = station.stnCode;
		  $('div#challanToStationDB').dialog('close');
		  $scope.ChdPerStateCodeDBFlag=true;
	  }



	  $scope.saveVehicleType = function(vt){
		  $scope.chln.chlnVehicleType = vt.vtVehicleType;
		  $('div#vehicleTypeDB').dialog('close');
		  $scope.VehicleTypeDBFlag=true;
		  $scope.chlnRRNo($scope.vtList);
	  }

	  $scope.saveBrokerCode = function(brk){
		 /* $scope.chdBrCodeTemp = brk.brkName;
		  $scope.chd.chdBrCode= brk.brkCode;*/
		  $scope.chdBrCodeTemp = brk.name;
		  $scope.chd.chdBrCode= brk.code;
		  $('div#chdBrCodeDB').dialog('close');
		  $scope.ChdBrCodeDBFlag=true;
		  $scope.getMobileListBk($scope.chd.chdBrCode);
		  $('#chdBrMobNo').removeAttr("disabled");
		  $('#openMobbrk').removeAttr("disabled");
	  }

	  $scope.saveOwnerCode = function(own){
		  /*$scope.chdOwnCodeTemp = own.ownName;
		  $scope.chd.chdOwnCode= own.ownCode;*/
		  $scope.chdOwnCodeTemp = own.name;
		  $scope.chd.chdOwnCode= own.code;
		  $('div#chdOwnCodeDB').dialog('close');
		  $scope.ChdOwnCodeDBFlag=true;
		  $scope.getMobileListOw($scope.chd.chdOwnCode);
		  $('#chdOwnMobNo').removeAttr("disabled");
		  $('#openMobown').removeAttr("disabled");
	  }

	  $scope.savePanIssueStateCode = function(state){
		  $scope.chd.chdPanIssueSt= state.stateCode;
		  $('div#chdPanIssueStDB').dialog('close');
		  $scope.ChdPanIssueStDBFlag=true;
	  }

	  $scope.savePerStateCode = function(state){
		  $scope.chdPerStateTemp = state.stateName;
		  $scope.chd.chdPerState= state.stateCode;
		  $('div#chdPerStateCodeDB').dialog('close');
		  $scope.PerStateCodeFlag1=true;
	  }

	  $scope.saveBrMobNo = function(mobile){
		  $scope.chd.chdBrMobNo = mobile;
		  $('div#chdBrMobNoDB').dialog('close');
		  $scope.ChdBrMobNoDBFlag=true;
	  }

	  $scope.saveOwnMobNo = function(mobile){
		  $scope.chd.chdOwnMobNo= mobile;
		  $('div#chdOwnMobNoDB').dialog('close');
		  $scope.ChdOwnMobNoDBFlag=true;
	  }

	  $scope.saveChallanCode = function(challan){
		  console.log("enter into saveChallanCode function----"+challan);
		  $scope.chln.chlnCode=challan;
		  $('div#ChallanCodeDB').dialog('close');
		  $scope.ChallanCodeDBFlag=true;
		  autoCode = $scope.chln.chlnCode;
		  $scope.chd.chdChlnCode = autoCode;
		  $('#acd').removeAttr("disabled");
	  }

	  $scope.savepistate = function(state){
		  $('div#chdPanIssueStDB1').dialog('close');
		  console.log("enter into savepistate function--->");
		  var response = $http.post($scope.projectName+'/savePISForCD',state);
		  response.success(function(data, status, headers, config) {
			  if(data.result==="success"){
				  $scope.successToast(data.result);
				  $scope.state="";
			  }else{
				  console.log(data.result);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.savepscode = function(state){
		  $('div#chdPerStateCodeDB1').dialog('close');
		  console.log("enter into savepscode function--->");
		  var response = $http.post($scope.projectName+'/savePISForCD',state);
		  response.success(function(data, status, headers, config) {
			  if(data.result==="success"){
				  $scope.successToast(data.result);
				  $scope.state="";
			  }else{
				  console.log(data.result);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.saveownmob = function(mobile){
		  $('div#chdOwnMobNoDB1').dialog('close');
		  console.log("enter into saveownmob function--->");
		  var response = $http.post($scope.projectName+'/saveOWNMBForCD',mobile);
		  response.success(function(data, status, headers, config) {
			  if(data.result==="success"){
				  $scope.successToast(data.result);
				  $scope.mobile="";
			  }else{
				  console.log(data.result);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.savebrmob = function(mobile){
		  $('div#chdBrMobNoDB1').dialog('close');
		  console.log("enter into savebrmob function--->");
		  /*$scope.chd.chdBrMobNo = mobile;*/
		  var response = $http.post($scope.projectName+'/saveOWNMBForCD',mobile);
		  response.success(function(data, status, headers, config) {
			  if(data.result==="success"){
				  $scope.successToast(data.result);
				  $scope.mobile="";
			  }else{
				  console.log(data.result);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.submitCD = function(ChallanDetailForm,chd){
		  $scope.CDFlag=true;
		  console.log("enter into next function ChallanDetailForm.$invalid--->"+ChallanDetailForm.$invalid);
		  if(ChallanDetailForm.$invalid){
			  if(ChallanDetailForm.chdBrCode.$invalid){
				  $scope.alertToast("please enter broker code..");
			  }else if(ChallanDetailForm.chdOwnCode.$invalid){
				  $scope.alertToast("please enter owner code..");
			  }else if(ChallanDetailForm.chdBrMobNo.$invalid){
				  $scope.alertToast("please enter Broker's 10 digit mobile no..");
			  }else if(ChallanDetailForm.chdOwnMobNo.$invalid){
				  $scope.alertToast("please enter Owner's 10 digit mobile no..");
			  }else if(ChallanDetailForm.chdDvrName.$invalid){
				  $scope.alertToast("Please enter driver name..");
			  }else if(ChallanDetailForm.chdDvrMobNo.$invalid){
				  $scope.alertToast("please enter driver's 10 digit mobile no..");
			  }else if(ChallanDetailForm.chdDlNo.$invalid){
				  $scope.alertToast("Please enter DL no..");
			  }else if(ChallanDetailForm.chdPanHdrType.$invalid){
				  $scope.alertToast("Please enter PAN HDR TYpe..");
			  }
			  else{
				  $scope.alertToast("Challan detail entry not valid !");
				  console.log("***********************");
			  }
		  }else{
			  console.log("enter into submitCD function--->");
			  $('div#chdCodeCb').dialog('close');
			  $('#chlnAdvance').removeAttr("disabled");			
		  }
	  } 

	  $scope.calTotalFreight = function(chln){
		  chln.chlnTotalFreight=chln.chlnFreight+chln.chlnLoadingAmt+chln.chlnExtra-chln.chlnStatisticalChg-chln.chlnTdsAmt;
	  }

	  $scope.calculateFreight = function(){
		  var LryRate = 0;
		  var ChgWt = 0;
		  if($scope.chlnLryRatePer === "Ton"){
			  if($scope.chlnChgWtPer === "Ton"){
				  $scope.chln.chlnFreight = $scope.chln.chlnLryRate*$scope.chln.chlnChgWt;	
			  }else if($scope.chlnChgWtPer === "Kg"){
				  LryRate = $scope.chln.chlnLryRate/1000;
				  $scope.chln.chlnFreight = LryRate * $scope.chln.chlnChgWt;
			  }
		  }else if($scope.chlnLryRatePer === "Kg"){
			  if($scope.chlnChgWtPer === "Ton"){
				  LryRate = $scope.chln.chlnLryRate * 1000;
				  $scope.chln.chlnFreight = LryRate*$scope.chln.chlnChgWt;
			  }else if($scope.chlnChgWtPer === "Kg"){
				  $scope.chln.chlnFreight = $scope.chln.chlnLryRate * $scope.chln.chlnChgWt;
			  }
		  }
	  }

	  $scope.checkAdvance =  function(){
		  console.log("enter into checkAdvance funciton");
		  //chlnAdvc=0.95*parseInt($scope.chln.chlnTotalFreight);
		  chlnAdvc=parseInt($scope.chln.chlnTotalFreight);
		  if($scope.chln.chlnAdvance>chlnAdvc){
			  $scope.chln.chlnAdvance="";
			  $scope.alertToast("You can't enter more than 95% Total Freight");
		  }
	  }

	  $scope.balance = function(chln){
		  chln.chlnBalance=parseInt(chln.chlnTotalFreight)-parseInt(chln.chlnAdvance);
	  }	

	  $scope.chlnRRNo = function(chln){
		  console.log("enter into chlnRRNo fucntion");
		  if($scope.chln.chlnVehicleType === "train"){
			  $('#chlnRrNo').removeAttr("disabled");
			  $('#chlnTrainNo').removeAttr("disabled");
		  }
	  }	

	  $scope.submitChallan = function(chln,cnmtCode,ChallanForm,chd,chlnTotalWtPer){
		  console.log("enter into submitChallan function ChallanForm ="+ChallanForm.$invalid);
		  console.log("selected cnmtCode = "+cnmtCode);
		  console.log("chln.chlnTotalWt ---->"+chln.chlnTotalWt);
		  //var cnmtCodeList = [];
		  for(var i=0;i<cnmtCodeList.length;i++){
			  cnmtCodeList.splice(i,1);
		  }
		  
		 if(angular.isUndefined($scope.chln.chlnLryNo) || $scope.chln.chlnLryNo === "" || $scope.chln.chlnLryNo === null){
			  $scope.alertToast("please select a vehicle");
		 }else{
			  
			  if(ChallanForm.$invalid){
				  if(ChallanForm.chlnCode.$invalid){
					  $scope.alertToast("please enter Challan Code..");
				  }else if(ChallanForm.branchCode.$invalid){
					  $scope.alertToast("please enter Branch Code..");				
				  }else if(ChallanForm.chlnFromStn.$invalid){
					  $scope.alertToast("Please enter Challan From Station..");
				  }else if(ChallanForm.chlnToStn.$invalid){
					  $scope.alertToast("please enter Challan To Station..");
				  }else if(ChallanForm.chlnEmpCode.$invalid){
					  $scope.alertToast("please enter Employee Code");
				  }else if(ChallanForm.chlnLryRate.$invalid){
					  $scope.alertToast("Please enter Lorry Rate..");
				  }else if(ChallanForm.chlnChgWt.$invalid){
					  $scope.alertToast("Please enter Charge Weight..");
				  }else if(ChallanForm.chlnChgWtPer1.$invalid){
					  $scope.alertToast("Please enter per Ton/Kg for Charge Weight..");
				  }else if(ChallanForm.chlnNoOfPkg.$invalid){
					  $scope.alertToast("Please enter No.Of Packages ..");
				  }else if(ChallanForm.chlnTotalWt.$invalid){
					  $scope.alertToast("Please enter Total Weight..");
				  }else if(ChallanForm.chlnTotalWtPer.$invalid){
					  $scope.alertToast("Please enter per Ton/Kg for Total Weight..");
				  }else if(ChallanForm.chlnFreight.$invalid){
					  $scope.alertToast("Please enter Freight..");
				  }else if(ChallanForm.chlnLoadingAmt.$invalid){
					  $scope.alertToast("Please enter Loading Amount..");
				  }else if(ChallanForm.chlnExtra.$invalid){
					  $scope.alertToast("Please enter Extra..");
				  }else if(ChallanForm.chlnAdvance.$invalid){
					  $scope.alertToast("Please enter Advance..");
				  }else if(ChallanForm.chlnPayAt.$invalid){
					  $scope.alertToast("Please enter Pay At..");
				  }else if(ChallanForm.chlnDt.$invalid){
					  $scope.alertToast("Please enter Challan Date..");
				  }else if(ChallanForm.chlnTimeAllow.$invalid){
					  $scope.alertToast("Please enter Time Allowed..");
				  }else if(ChallanForm.chlnBrRate.$invalid){
					  $scope.alertToast("Please enter BR Rate..");
				  }else if(ChallanForm.chlnWtSlip.$invalid){
					  $scope.alertToast("Please enter Weight Slip..");
				  }else if(ChallanForm.chlnVehicleType.$invalid){
					  $scope.alertToast("Please enter Vehicle Type..");
				  }else if(ChallanForm.chlnStatisticalChg.$invalid){
					  $scope.alertToast("Please enter Statistical Charge..");
				  }else if(ChallanForm.chlnTdsAmt.$invalid){
					  $scope.alertToast("Please enter TDS Amount..");
				  }else if(ChallanForm.chlnLryLoadTime.$invalid){
					  $scope.alertToast("Please enter Lorry Load Time..");
				  }else if(ChallanForm.chlnLryRepDT.$invalid){
					  $scope.alertToast("Please enter Lorry Reporting Date..");
				  }else if(ChallanForm.chlnLryRptTime.$invalid){
					  $scope.alertToast("Please enter Lorry Reporting Time..");
				  }else{
					  $scope.alertToast("Challan entry not valid !");
				  }

			  }else{ 
				  if($scope.CDFlag===false){
					  console.log("here v are -------------------------------->");
					  if(chd.chdChlnCode===null || chd.chdChlnCode ==="" || angular.isUndefined(chd.chdChlnCode) || chd.chdBrCode=== null || chd.chdBrCode==="" || angular.isUndefined(chd.chdBrCode) || chd.chdOwnCode===null || chd.chdOwnCode==="" || angular.isUndefined(chd.chdOwnCode)
							  || chd.chdBrMobNo===null || chd.chdBrMobNo==="" || angular.isUndefined(chd.chdBrMobNo) || chd.chdOwnMobNo===null || chd.chdOwnMobNo==="" || angular.isUndefined(chd.chdOwnMobNo) || chd.chdDvrName===null || chd.chdDvrName==="" || angular.isUndefined(chd.chdDvrName)
							  || chd.chdDvrMobNo===null || chd.chdDvrMobNo==="" || angular.isUndefined(chd.chdDvrMobNo) ||  chd.chdDlNo===null || chd.chdDlNo==="" || angular.isUndefined(chd.chdDlNo) || chd.chdDlIssueDt===null || chd.chdDlIssueDt==="" || angular.isUndefined(chd.chdDlIssueDt)
							  || chd.chdDlValidDt===null || chd.chdDlValidDt==="" || angular.isUndefined(chd.chdDlValidDt)
							  || chd.chdPanHdrType===null || chd.chdPanHdrType==="" || angular.isUndefined(chd.chdPanHdrType) || chd.chdPanNo===null || chd.chdPanNo==="" || angular.isUndefined(chd.chdPanNo)
							  || chd.chdPanHdrName===null || chd.chdPanHdrName==="" || angular.isUndefined(chd.chdPanHdrName))
					  {
						  $scope.alertToast("please Enter challan details");
					  }
				  }else{	
					  console.log("enter into submitChallan function ChallanForm ="+ChallanForm.$invalid);
					  //var cnmtCodeList = {};
					  console.log("length of $scope.multiCnmtCodeList.length = "+$scope.multiCnmtCodeList.length);
					  if($scope.multiCnmtCodeList.length > 1){
						  //cnmtCodeList = $scope.multiCnmtCodeList;
						  console.log("multiple cnmt selected");
						  var cnmtActualWt = 0;
						  for(var i=0;i<$scope.multiCnmtCodeList.length;i++){
							  for(var j=0;j<$scope.cnmtList.length;j++){
								  if($scope.cnmtList[j].cnmtCode === $scope.multiCnmtCodeList[i]){
									  cnmtActualWt =  parseInt(cnmtActualWt) + parseInt($scope.cnmtList[j].cnmtActualWt);
								  }	
							  }		
						  }
						  console.log("cnmtActualWt ======> "+cnmtActualWt);
						  console.log("chlnTotalWtPer ----> "+chlnTotalWtPer);
						  
						  var tempChlnTotWt = chln.chlnTotalWt;
						  if(chlnTotalWtPer === "Ton"){
							  //$scope.chlnTotalWtPer = "Kg";
							  tempChlnTotWt =(chln.chlnTotalWt*$scope.kgInTon);
							  //$scope.chln.chlnTotalWt = chln.chlnTotalWt;
							  console.log("%%%%%%%%%%%%%%%%%"+chln.chlnTotalWt);
						  }
						  if(parseFloat(cnmtActualWt) > parseFloat(tempChlnTotWt)){
							  $scope.alertToast("Cnmts Actual weight("+ cnmtActualWt +"kg) must be less than Challan total weight");
						  }else{
							  cnmtCodeList = $scope.multiCnmtCodeList;
							  $scope.viewChallanDetailsFlag=false;
							  $('div#viewChallanDetailsDB').dialog({
								  autoOpen: false,
								  modal:true,
								  title: "Customer Info",
								  show: UDShow,
								  hide: UDHide,
								  position: UDPos,
								  resizable: false,
								  draggable: true,
								  close: function(event, ui) { 
									  $(this).dialog('destroy') ;
									  $(this).hide();
								  }
							  });

							  $('div#viewChallanDetailsDB').dialog('open');
						  }
					  }else{
						  console.log("----------->cnmtCode = "+cnmtCode);
						  if(angular.isUndefined(cnmtCode) || $scope.cnmtCode === "" || cnmtCode === null){
							  console.log("single cnmt selected");
							  $scope.alertToast("please select a cnmt code");
						  }else{
							  cnmtCodeList.push(cnmtCode);
							  var cnmtActualWt = 0;
							  for(var i=0;i<cnmtCodeList.length;i++){
								  for(var j=0;j<$scope.cnmtList.length;j++){
									  if($scope.cnmtList[j].cnmtCode === cnmtCodeList[i]){
										  cnmtActualWt =  parseInt(cnmtActualWt) + parseInt($scope.cnmtList[j].cnmtActualWt);
									  }	
								  }		
							  }

							  console.log("cnmtActualWt ======> "+cnmtActualWt);
							  console.log("chlnTotalWtPer ----> "+chlnTotalWtPer);
							  
							  var tempChlnTotWt = chln.chlnTotalWt;
							  if(chlnTotalWtPer === "Ton"){
								  //$scope.chlnTotalWtPer = "Kg";
								  console.log("#############"+chln.chlnTotalWt);
								  tempChlnTotWt = chln.chlnTotalWt * $scope.kgInTon;
								  //$scope.chln.chlnTotalWt = chln.chlnTotalWt;
								  console.log("%%%%%%%%%%%%%%%%%"+chln.chlnTotalWt);
							  }
							  
							  console.log("cnmtActual weight = "+cnmtActualWt);
							  console.log("tempChlnTotWt weight = "+tempChlnTotWt);
							  
							  if(parseFloat(cnmtActualWt) > parseFloat(tempChlnTotWt)){
								  $scope.alertToast("Cnmts Actual weight must be less than Challan total weight");
							  }else{
								  $scope.viewChallanDetailsFlag=false;
								  $('div#viewChallanDetailsDB').dialog({
									  autoOpen: false,
									  modal:true,
									  title: "Customer Info",
									  show: UDShow,
									  hide: UDHide,
									  position: UDPos,
									  resizable: false,
									  draggable: true,
									  close: function(event, ui) { 
										  $(this).dialog('destroy') ;
										  $(this).hide();
									  }
								  });

								  $('div#viewChallanDetailsDB').dialog('open');
							  }

						  }	
					  }
				  }
			  } 
		 }
	
	  }
	  $scope.saveChallan = function(challan,chlnLryRatePer,chlnChgWtPer,chlnTotalWtPer,chln){
		  console.log("enter into saveChallan function");
		  console.log("$scope.chln.chlnTotalWt ---> "+$scope.chln.chlnTotalWt);
		  if(chlnLryRatePer === "Ton"){ 
			  $scope.chln.chlnLryRate =(chln.chlnLryRate/$scope.kgInTon);
			  console.log("***********"+chln.chlnLryRate);
		  }
		  if(chlnChgWtPer === "Ton"){
			  chlnChgWtPer = "Kg";
			  $scope.chln.chlnChgWt =(chln.chlnChgWt*$scope.kgInTon);
			  console.log("############"+chln.chlnChgWt);
		  }
		  if(chlnTotalWtPer === "Ton"){
			  $scope.chln.chlnTotalWt =(chln.chlnTotalWt*$scope.kgInTon);
			  console.log("%%%%%%%%%%%%%%%%%"+chln.chlnTotalWt);
		  }
		  //$scope.chln.chlnLryNo = $scope.chlnLryNoPer+" "+$scope.chln.chlnLryNo;
		  
		  if($scope.multiCnmtCodeList.length > 1){
			  var Cnmt_Challan = {
					  "challan"      		: $scope.chln,
					  //"cnmtCodeList" 		: cnmtCodeList,
					  "cnmtCodeList" 		: $scope.multiCnmtCodeList,
					  "challanDetail"   	: $scope.chd
			  };
		  }else{
			  var cnmtCodeList = [];
			  console.log("single cnmt = "+singleCnmtData.cnmt);
			  console.log("No of pkg = "+singleCnmtData.pkg);
			  console.log("weight = "+singleCnmtData.wt);
			  cnmtCodeList.push(singleCnmtData);
			  var Cnmt_Challan = {
					  "challan"      		: $scope.chln,
					  "cnmtCodeList" 		: cnmtCodeList,
					  "challanDetail"   	: $scope.chd
			  };
		  } 
		  
		  $('#saveBtnId').attr("disabled","disabled");
		  
		  var response = $http.post($scope.projectName+'/submitChln&ChlnDet',Cnmt_Challan);
		  response.success(function(data, status, headers, config) {
			  if(data.result==="success"){
				  $('#saveBtnId').removeAttr("disabled");
				  
				  $scope.viewChallanDetailsFlag = true;
				  $('div#viewChallanDetailsDB').dialog('close');
				  $scope.successToast(data.result);
				  //$scope.multiCnmtCodeList = {};
				  
				  for(var i=0;i<$scope.multiCnmtCodeList.length;i++){
					  $scope.multiCnmtCodeList.splice(i,1);
				  }
				  $scope.state="";
				  
				  
				  $("#cnmtCodeId").attr("disabled","disabled");
				  $scope.stList = [];
				  $scope.noOf = 0;
				  $scope.acWt = 0;
				  $scope.myCnmtListFlag = false;
				  $scope.cnmtNo = "";
				  $scope.myCnmtList = [];
				  $scope.noList = [];
				  
				  
				  
			/*	  $scope.chln.chlnCode= "";
				  $scope.chln.branchCode= "";
				  $scope.chln.chlnLryNo= "";
				  $scope.chln.chlnFromStn= "";
				  $scope.chln.chlnToStn= "";
				  $scope.chln.chlnEmpCode= "";
				  $scope.chln.chlnLryRate= "";
				  $scope.chln.chlnNoOfPkg= "";
				  $scope.chln.chlnChgWt= "";
				  $scope.chln.chlnFreight= "";
				  $scope.chln.chlnLoadingAmt= "";
				  $scope.chln.chlnTotalWt= "";
				  $scope.chln.chlnExtra= "";
				  $scope.chln.chlnTotalFreight= "";
				  $scope.chln.chlnWtSlip= "";
				  $scope.chln.chlnVehicleType= "";
				  $scope.chln.chlnAdvance= "";
				  $scope.chln.chlnBalance= "";
				  $scope.chln.chlnPayAt= "";
				  $scope.chln.chlnTimeAllow= "";
				  $scope.chln.chlnDt= "";
				  $scope.chln.chlnBrRate= "";
				  $scope.chln.chlnStatisticalChg= "";
				  $scope.chln.chlnTdsAmt= "";
				  $scope.chln.chlnRrNo= "";
				  $scope.chln.chlnTrainNo= "";
				  $scope.chln.chlnLryLoadTime= "";
				  $scope.chln.chlnLryRptTime= "";
				  $scope.chln.chlnLryRepDT= "";*/
				  $scope.chln = {};
				  
				  	$scope.chln.chlnDetection = "0";
	  				$scope.chln.chlnToolTax = "0";
	  				$scope.chln.chlnHeight = "0";
	  				$scope.chln.chlnUnion = "0";
	  				$scope.chln.chlnTwoPoint = "0";
	  				$scope.chln.chlnWeightmentChg = "0";
	  				$scope.chln.chlnCraneChg = "0";
	  				$scope.chln.chlnOthers = "0";
	  				
	  				$scope.chln.chlnFreight = 0;
	  				$scope.chln.chlnLoadingAmt = 0;
	  				$scope.chln.chlnExtra = 0;
	  				$scope.chln.chlnStatisticalChg = 0;
	  				$scope.chln.chlnTdsAmt = 0;
	  				
	  				
				  
				  $scope.chd = {};
				  $scope.cnmtCode = "";
				  $scope.chlnLryNoPer = "";
				  $scope.branchCodeTemp = "";
				  $scope.frmStationCode = "";
				  $scope.toStationCode = "";
				  $scope.empTempCode = "";
				  var singleCnmtData = {};
				  var cnmtCodeList = [];
				  $scope.multiCnmtCodeList = [];
				  $scope.successToast(data.result);
				  $scope.getChallanCodes();
				  
				  /*for(var i=0 ; i < cnmtCodeList.length ; i++){
					cnmtCodeList.splice(i,1);
				}*/
			  }else{
				  console.log(data.result);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.closeViewChallanDetailsDB = function(){
		  $('div#viewChallanDetailsDB').dialog('close');
	  }


	  $scope.saveCnmt = function(cnmt,pkg,wt){
		  console.log("enter into saveCnmt fucntion ");
		 
		  console.log("CNMT = "+cnmt.cnmtCode+" : PKG = "+pkg+" : Wt = "+wt);
		  
		  
		  $scope.cnmtCode = cnmt.cnmtCode;
		  if(angular.isUndefined(pkg) || angular.isUndefined(wt)){
			  $scope.alertToast("Please enter No. of pkg and weight");
		  }else{
			  singleCnmtData = {
					  "cnmt" : cnmt.cnmtCode,
					  "pkg"  : pkg,
					  "wt"   : wt
			  };
			  
			  $scope.chln.chlnNoOfPkg = pkg;
			  $scope.chln.chlnTotalWt = wt;
			  $scope.frmStationCode = cnmt.fromStnName;
			  $scope.chln.chlnFromStn = cnmt.cnmtFromSt;
			  $scope.toStationCode = cnmt.toStnName;
			  $scope.chln.chlnToStn = cnmt.cnmtToSt;
			  $scope.empTempCode = cnmt.empFaCode;
			  $scope.chln.chlnEmpCode = cnmt.cnmtEmpCode;			  
			  
			  var response = $http.post($scope.projectName+'/getTransitDay',$scope.cnmtCode);
			  response.success(function(data, status, headers, config){
				  console.log("transit day from server ===>"+data.transitDay);
				  console.log("mc = "+data.mc);
				  if(data.result==="success"){
					  console.log(JSON.stringify(data));
					  if(data.mc === "true"){
						  $scope.chln.chlnTimeAllow = "";
					  }else{
						  $scope.chln.chlnTimeAllow = data.transitDay;
					  }
					  $scope.chln.chlnFromStn = data.fromStation;
					  $('div#cnmtDB').dialog('close');
					  $scope.CnmtDBFlag=true;
					  $scope.successToast(data.result);
				  }else{
					  console.log(data);   
				  }	
			  });
			  response.error(function(data, status, headers, config) {
				  $scope.errorToast(data.result);
			  });
		  }
	  }

	  $scope.saveMultiCnmt = function(multiCnmt,pkg,wt){
		  console.log("enter into saveMultiCnmt fucntion");
		  //$scope.multiCnmtCodeList.push(multiCnmt.cnmtCode);
		  
		  if(angular.isUndefined(pkg) || angular.isUndefined(wt)){
			  $scope.alertToast("Please enter No. of pkg and weight");
		  }else{
			  var req = {
					  "cnmt" : multiCnmt.cnmtCode,
					  "pkg"  : pkg,
					  "wt"   : wt
			  };			  
			 /* 
			  $scope.noOf = $scope.noOf + pkg;
			  $scope.acWt = $scope.acWt +wt;
			  
			  $scope.chln.chlnNoOfPkg = $scope.noOf; 
			  $scope.chln.chlnTotalWt = $scope.acWt;
						  			  
			  $scope.frmStationCode = multiCnmt.fromStnName;
			  $scope.chln.chlnFromStn = multiCnmt.cnmtFromSt;
			  $scope.toStationCode = multiCnmt.toStnName;
			  $scope.chln.chlnToStn = multiCnmt.cnmtToSt;
			  $scope.empTempCode = multiCnmt.empFaCode;
			  $scope.chln.chlnEmpCode = multiCnmt.cnmtEmpCode;		
			  */
			  $scope.multiCnmtCodeList.push(req);
			  $scope.multiCnmtCode = multiCnmt.cnmtCode;
			  console.log("$scope.multiCnmtCodeList = "+$scope.multiCnmtCodeList);
			  $('div#multiCnmtDB').dialog('close');
			  $scope.MultiCnmtDBFlag=true;
			  //$scope.addMultiCnmt(multiCnmt.cnmtCode);
			  $scope.addMultiCnmt(req, multiCnmt);
		  }
		  
	  }

	  $scope.singleCnmt = function(){
		  console.log("enter into singleCnmt fucntion");	
		  $scope.cnmtType = "single";		  
		  $scope.OpenMultiCnmtDB();			  
		  for(var i=0;i < $scope.multiCnmtCodeList.length;i++){
			  $scope.multiCnmtCodeList.splice(i,1);
		  }
		  $scope.multiCnmtCode = "";
		  $scope.pkg = [];
		  $scope.wt = [];
		  $scope.removeAllMultiCnmt();
	  }


	  $scope.multipleCnmt = function(){
		  $scope.cnmtType = "double";
		  $scope.cnmtNo = "";
		  $scope.noList = [];
		  $scope.OpenMultiCnmtDB();	  
		  console.log("enter into multipleCnmt fucntion");		 
		  $scope.cnmtCode = "";
		  var singleCnmtData = {};
		  $scope.pkg = [];
		  $scope.wt = [];
		  $scope.fetchMultiCnmtList();
	  }

	  $scope.fetchMultiCnmtList = function(){
		  console.log("enter into fetchMultiCnmtList fucntion");
		  var response = $http.get($scope.projectName+'/fetchMultiCnmtList');
		  response.success(function(data, status, headers, config){
			  $scope.multiCnmtCodeList = data.list; 	
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.addMultiCnmt = function(req, multiCnmt){
		  console.log("enter into addMultiCnmt fucntion");
		  var count = 0;
		  for(var i=0;i<$scope.multiCnmtCodeList.length;i++){
			  if($scope.multiCnmtCodeList[i].cnmt === req.cnmt){
				  //$scope.multiCnmtCodeList.splice(i,1);
				  //console.log("already selected");
				  count = count + 1;
			  }
			  if(count > 1){
				  $scope.multiCnmtCodeList.splice(i,1);
			  }
		  }
		  if(count > 1){
			  $scope.alertToast("already exist");
		  }else{
			  
			  $scope.noOf = $scope.noOf + req.pkg;
			  $scope.acWt = $scope.acWt + req.wt;
			  
			  $scope.chln.chlnNoOfPkg = $scope.noOf; 
			  $scope.chln.chlnTotalWt = $scope.acWt;
						  			  
			  $scope.frmStationCode = multiCnmt.fromStnName;
			  $scope.chln.chlnFromStn = multiCnmt.cnmtFromSt;
			  $scope.toStationCode = multiCnmt.toStnName;
			  $scope.chln.chlnToStn = multiCnmt.cnmtToSt;
			  $scope.empTempCode = multiCnmt.empFaCode;
			  $scope.chln.chlnEmpCode = multiCnmt.cnmtEmpCode;	
			  
			  console.log("$scope.multiCnmtCodeList = "+$scope.multiCnmtCodeList);
			  var response = $http.post($scope.projectName+'/addMultiCnmt',req);
			  response.success(function(data, status, headers, config){
				  if(data.result === "success")
					  $scope.fetchMultiCnmtList(); 	
			  });
			  response.error(function(data, status, headers, config) {
				  $scope.errorToast(data.result);
			  });
		  }
	  }

	  $scope.removeMultiCnmt = function(code) {
		  console.log("enter into removeMultiCnmt function ="+code);
		  var response = $http.post($scope.projectName+'/removeMultiCnmt',code);
		  response.success(function(data, status, headers, config) {
			  if(data.result === "success")
				  $scope.fetchMultiCnmtList();
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.removeAllMultiCnmt = function() {
		  console.log("enter into removeAllMultiCnmt fucntion");
		  var response = $http.post($scope.projectName+'/removeAllMultiCnmt');
		  response.success(function(data, status, headers, config) {
			  if(data.result === "success")
				  $scope.multiCnmtCode = "";
			  	  $scope.fetchMultiCnmtList();
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }

	  $scope.uploadChallanImage = function(){
		  console.log("enter into uploadChallanImage function");
		  var file = $scope.challanImage;
		  console.log("size of file ----->"+file.size);
		  if(angular.isUndefined(file) || file === null || file === ""){
			  $scope.alertToast("First choose the file----->");
		  }else if(file.size > $scope.maxFileSize){
			  $scope.alertToast("image size must be less than or equal to 1mb");
		  }else{
			  console.log('file is ' + JSON.stringify(file));
			  var uploadUrl = $scope.projectName+"/uploadChallanImage";
			  FileUploadService.uploadFileToUrl(file, uploadUrl);
			  console.log("file save on server");
		  }
	  }
	  
	  $scope.make = function(){
		  
		 var len = parseInt($scope.cnmtNo);
		 if($scope.cnmtType === "single"){
			 len = 1;
			 $scope.cnmtNo = len;
		 }
		 $scope.noList = [];
		 $scope.myCnmtList = [];
		 var len = parseInt($scope.cnmtNo);		 
		 for(var i=0; i<len; i++)
			 $scope.noList.push(i);		 
	  }

	  $scope.getChallanCodes = function(){
		  console.log("getChallanCodes------>");
		  var response = $http.post($scope.projectName+'/getBrStLeafDetData');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.challanCodesList = data.list;
				  $scope.chln.branchCode = data.brhCode;
				  $scope.branchCodeTemp = data.brhName;
				  $scope.getVehicleMstr();
				  //$scope.getStateList();
				  //$scope.removeAllMultiCnmt();
			  }else{
				  $scope.alertToast("you don't have any challan");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers,config) {
			  $scope.errorToast(data);
		  });
	  }
	  
	  
	  $scope.getVehicleMstr = function(){
		console.log("enter into getVehicleMstr function");
		var response = $http.post($scope.projectName+'/getVehicleMstrFC');
		  response.success(function(data, status, headers, config){
			 if(data.result === "success"){
				 $scope.vehList = data.list;
				 console.log("size of $scope.vehList = "+$scope.vehList.length);
				 $scope.getStateList();
			 }else{
				 $scope.alertToast("Vehicles are not avaliable");
				 console.log("error in fetching data from getVehicleMstr");
			 }
		  });
		  response.error(function(data, status, headers,config) {
			  $scope.errorToast(data);
		  });
	  }
	  
	  
	  $scope.openVehicleDB = function(){		  
		  console.log("Enter into openVehicleDB()....");
		  var chlnLryNo = $scope.chln.chlnLryNo;		 
		  var len = parseInt(chlnLryNo.length);
		  if(len < 4)
			  return;
		  $scope.chln.chlnLryNo = "";		  
		  console.log("Hitting /getVehicleMstrFCByLryNo ");
		  var response = $http.post($scope.projectName+'/getVehicleMstrFCByLryNo', chlnLryNo);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.filterVehCode = chlnLryNo;
				  $scope.vehList = data.list;
				  $scope.vehDBFlag=false;
				  $('div#vehDB').dialog({
					  autoOpen: false,
					  modal:true,
					  title: "Select Vehicle",
					  show: UDShow,
					  hide: UDHide,
					  position: UDPos,
					  resizable: false,
					  draggable: true,
					  close: function(event, ui) { 
						  $(this).dialog('destroy') ;
						  $(this).hide();
						  $scope.vehDBFlag=true;
					  }
				  });
				  $('div#vehDB').dialog('open');
			  }else{
				  $scope.alertToast("Vehicle not found !");
			  }
		  });	  
		
		 
	  }
	  
	  
	  $scope.selectVeh = function(selVeh){
		  console.log("enter into selectVeh function");
		  var response = $http.post($scope.projectName+'/selectVehFC',selVeh);
		  response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$('div#vehDB').dialog('close');
				$scope.vehM = data.vehM;
				console.log("++"+$scope.vehM.vvModel);
				$scope.chln.chlnLryNo = $scope.vehM.vvRcNo;
				$scope.chdOwnCodeTemp = data.ownName;
				$scope.chd.chdOwnCode = data.ownCode;
				$scope.chdBrCodeTemp = data.brkName;
				$scope.chd.chdBrCode= data.brkCode;
				$scope.brkrcode = $scope.chd.chdBrCode; 
				$scope.ownrcode = $scope.chd.chdOwnCode;
				$scope.mobListBk = [];
				$scope.mobListBk = data.brkMobList;
				if($scope.mobListBk.length > 0){
					$scope.chd.chdBrMobNo = $scope.mobListBk[0];
				}else{
					$('#openMobbrk').removeAttr("disabled");
				}
				$scope.mobListOw = []; 
				$scope.mobListOw = data.ownMobList;
				if($scope.mobListOw.length > 0){
					$scope.chd.chdOwnMobNo = $scope.mobListOw[0];
				}else{
					$('#openMobown').removeAttr("disabled");
				}
				
				$scope.chd.chdDvrName       = $scope.vehM.vvDriverName;
				console.log("$scope.vehM.vvDriverMobNo = "+$scope.vehM.vvDriverMobNo);
				$scope.chd.chdDvrMobNo      = $scope.vehM.vvDriverMobNo;
				$scope.chd.chdRcNo          = $scope.vehM.vvRcNo;
				$scope.chd.chdRcIssueDt     = $scope.vehM.vvRcIssueDt;
				$scope.chd.chdRcValidDt     = $scope.vehM.vvRcValidDt;
				$scope.chd.chdDlNo          = $scope.vehM.vvDriverDLNo;
				$scope.chd.chdDlIssueDt     = $scope.vehM.vvDriverDLIssueDt;
				$scope.chd.chdDlValidDt     = $scope.vehM.vvDriverDLValidDt;
				$scope.chd.chdPerNo         = $scope.vehM.vvPerNo;
				$scope.chd.chdPerIssueDt    = $scope.vehM.vvPerIssueDt;
				$scope.chd.chdPerValidDt    = $scope.vehM.vvPerValidDt;
				$scope.chd.chdFitDocNo      = $scope.vehM.vvFitNo;
				$scope.chd.chdFitDocIssueDt = $scope.vehM.vvFitIssueDt;
				$scope.chd.chdFitDocValidDt = $scope.vehM.vvFitValidDt;
				$scope.chd.chdFitDocPlace   = $scope.vehM.vvFitState;
				$scope.chd.chdBankFinance   = $scope.vehM.vvFinanceBankName;
				$scope.chd.chdPolicyNo      = $scope.vehM.vvPolicyNo;
				$scope.chd.chdPolicyCom     = $scope.vehM.vvPolicyComp;
				$scope.chd.chdTransitPassNo = $scope.vehM.vvTransitPassNo;
				$scope.chd.chdEngineNo      = $scope.vehM.vvEngineNo;
				$scope.chd.chdChassisNo     = $scope.vehM.vvChassisNo;
				$scope.chd.chdModel         = $scope.vehM.vvModel;
				
			}else{
				$('div#vehDB').dialog('close');
				$scope.alertToast(data.msg);				
			}
		  });
		  response.error(function(data, status, headers,config) {
			  $('div#vehDB').dialog('close');
			  $scope.errorToast(data);
		  });
	  }
	  
	  
     // by Kamal date validation for challan
	  $scope.checkdate = function(){
		  console.log("enter into checkdate--------->");
		  var date = $filter('date')(new Date, "yyyy-MM-dd");
		  console.log("date= "+date);
		  if($scope.chln.chlnDt<$scope.fyFrmDt || $scope.chln.chlnDt>$scope.fyToDt){
			  $scope.chln.chlnDt="";
				$scope.alertToast("Date should be in current Financial year");
			}else if($scope.chln.chlnDt> date){
				  $scope.chln.chlnDt="";
				$scope.alertToast("challan date cannot be greater than today");
			}else{
				for(var i=0; i<$scope.cnmtList.length; i++){
					  var cnmtCode = $scope.cnmtList[i].cnmtCode;
					  var date = $scope.cnmtList[i].cnmtDate;
					  if($scope.chln.chlnDt < date){
						$scope.alertToast("Challan date can not be less than Cnmt Date !");
						$scope.chln.chlnDt = "";
						break;
					  }				  
				  }
			}

	  }
	  
	  $scope.isInFY = function(){
			console.log("isInFY------>Kamal");
			if($scope.chln.chlnLryRepDT<$scope.fyFrmDt || $scope.chln.chlnLryRepDT>$scope.fyToDt){
				$scope.chln.chlnLryRepDT="";
				$scope.alertToast("Date should be in current Financial year");
			}
		}
	  
	  
	  
	  $scope.saveActDt = function(){
		  console.log("enter into saveActDt funciton  = "+$scope.actDate);
		  var newDate = new Date($scope.actDate);
		 // $scope.actDate = newDate;
		  
		  var response = $http.post($scope.projectName+'/saveDemoDt',newDate);
		  response.success(function(data, status, headers, config){
			 
		  });
		  response.error(function(data, status, headers,config) {
			  $scope.errorToast(data);
		  });
		  
	  }
	  
	  if($scope.operatorLogin === true || $scope.superAdminLogin === true){		 		
		  $scope.getVehicleTypeCode();
		  $scope.removeAllMultiCnmt();
		 }else if($scope.logoutStatus === true){
			 $location.path("/");
		 }else{
			 console.log("****************");
		 }
	  
	  $scope.OpenMultiCnmtDB = function(){
		  if($scope.cnmtType == "single"){
			  $scope.cnmtNo = "1";
			  $scope.noList = [1];
		  }else{
			  $scope.cnmtNo = "";
			  $scope.noList = [];
		  }
		  $scope.myCnmtListFlag = true;
		  $('div#MyCnmtListDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Total CNMT",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
			  }
		  });
		  $('div#MyCnmtListDB').dialog('open');
		  //$scope.fetchMultiCnmtList();
	  }
	 
}]);
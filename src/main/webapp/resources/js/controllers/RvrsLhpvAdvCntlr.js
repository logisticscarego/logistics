'use strict';

var app = angular.module('application');

	app.controller('RvrsLhpvAdvCntlr',['$scope','$location','$http','$filter','$window','AuthorisationService',
     function($scope,$location,$http,$filter,$window,AuthorisationService){
	
	$scope.vs = {};
	$scope.lhpvAdv = {};
	$scope.bnkList = [];
	$scope.chlnList = [];
	$scope.actBOList = [];
	$scope.lhpvAdvList = [];
	
	$scope.bankCodeDBFlag = true;
	$scope.chqNoDBFlag = true;
	$scope.brkOwnDBFlag = true;
	$scope.chlnDBFlag = true;
	$scope.lhpvAdvDBFlag = true;
	$scope.saveVsFlag = true;
	$scope.laFinalTotSum=0;
	//$scope.tdsPerFlag = false;
	$scope.lodingFlag = false;
	//$scope.vs.chequeType='';
	var faCodeTemp = '';
	$scope.vs.chequeLeaves={};
	
	$scope.curUsrCode = AuthorisationService.getUserCode();
	console.log("$scope.curUsrCode = "+$scope.curUsrCode);
	
	$scope.getLhpvDet = function(){
		console.log("enter into getLhpvDet function");
		var response = $http.post($scope.projectName+'/getLhpvDet');
		  response.success(function(data, status, headers, config){
			   if(data.result === "success"){
				   $scope.vs.branch = data.branch;
				   $scope.vs.lhpvStatus = data.lhpvStatus;
				   $scope.vs.voucherType = $scope.LHPV_ADV_VOUCHER;
				   $scope.dateTemp = $filter('date')(data.lhpvStatus.lsDt, "yyyy-MM-dd'");
				   $scope.bnkList = data.bnkList;
				   
				   if($scope.vs.lhpvStatus.lsClose === true){
					   $scope.alertToast("You already close the LHPV of "+$scope.dateTemp);
					   console.log("i m innnnnnnnnnnnnnn");
					   $('#verfChln').attr("disabled","disabled");
					   console.log("i m innnnnnnnnnnnnnn222222222");
				   }else{
					   //$scope.getChlnAndBrOwn();
				   }
			   }else{
				   console.log("error in fetching getLhpvDet data");
				   $scope.alertToast("Server Error");
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	
	
	
	$scope.openBankCodeDB = function(){
		console.log("enter into openBankCodeDB function");
		$scope.bankCodeDBFlag = false;
		$('div#bankCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Bank Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.bankCodeDBFlag = true;
		    }
			});
		
		$('div#bankCodeDB').dialog('open');	
	}
	
	 $scope.openChlnDB = function(){
		 console.log("enter into openChlnDB funciton");
		 if(angular.isUndefined($scope.vs.brkOwnFaCode) || $scope.vs.brkOwnFaCode === "" || $scope.vs.brkOwnFaCode === null){
			 $scope.alertToast("Please select Owner/Broker");
		 }else{
			 $scope.chlnDBFlag = false;
		    	$('div#chlnDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.chlnDBFlag = true;
				    }
					});
				$('div#chlnDB').dialog('open'); 
		 }
	 }
	
	 
	 $scope.saveChln = function(chlnCode){
		 console.log("enter into saveChln fucniton");
		 $scope.challan = chlnCode;
		 
		 var duplicate = false;
		 if($scope.lhpvAdvList.length > 0){
			 for(var i=0;i<$scope.lhpvAdvList.length;i++){
				if($scope.lhpvAdvList[i].challan.chlnCode === chlnCode){
					duplicate = true;
					break;
				}
			 }
		 }
		 
		 if(duplicate === false){
			 var data = {
					 "chlnCode"     : $scope.challan,
					 "brkOwnFaCode" : $scope.vs.brkOwnFaCode
			 };
			 
			 var response = $http.post($scope.projectName+'/getChlnFrLARvrs',data);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						//$('div#chlnDB').dialog('close'); 
						$scope.actChln = data.chln;
						
			
					
						$scope.lhpvAdv.challan = $scope.actChln;
						
						$scope.lhpvAdvDBFlag = false;
					    	$('div#lhpvAdvDB').dialog({
								autoOpen: false,
								modal:true,
								resizable: false,
								show: UDShow,
								title:"LHPV ADVANCE FOR CHALLAN ("+$scope.challan+")",
								hide: UDHide,
								position: UDPos,
								draggable: true,
								close: function(event, ui) { 
							        $(this).dialog('destroy');
							        $(this).hide();
							        $scope.lhpvAdvDBFlag = true;
							    }
								});
						$('div#lhpvAdvDB').dialog('open'); 
					}else{
						$scope.alertToast("server error");
						console.log("error in fetching getChlnFrLA data");
					}
				});
				response.error(function(data, status, headers, config){
					console.log(data);
				});
		 }else{
			 $scope.alertToast("Already create the lhpv advance for challan "+$scope.challan);
		 }
	 }
	 
	
	 $scope.submitLhpvAdv = function(lhpvAdvForm,lhAdv){
		 console.log("enter into submitLhpvAdv function = "+lhpvAdvForm.$invalid);
		 if(lhpvAdvForm.$invalid){
			$scope.alertToast("Please fill correct form");
		 }else{
			 if($scope.lhpvAdv.laLryAdvP >= 0){
				 $scope.alertToast("please fill the amount <= "+$scope.actAdv);
			 }else{
				 $scope.laFinalTotSum=$scope.laFinalTotSum+$scope.lhpvAdv.laFinalTot;
				 $scope.lhpvAdvList.push(lhAdv);
				 $scope.lhpvAdv = {};
				 $('div#lhpvAdvDB').dialog('close'); 
			 }
		 }
	 }
	 
	 
	 $scope.removeLhpvAdv = function(index,lhpvadv){
		 console.log("enter into removeLhpvAdv funciton = "+index);
		 $scope.laFinalTotSum=$scope.laFinalTotSum-lhpvadv.laFinalTot;
		 $scope.lhpvAdvList.splice(index,1);
	 }
	 
	 
	 $scope.voucherSubmit = function(voucherForm){
		 console.log("enter into voucherForm function = "+voucherForm.$invalid);
		 if(voucherForm.$invalid){
			$scope.alertToast("Please enter correct form");
		 }else{
			 if($scope.vs.payBy=='C'){
					if($scope.lhpvAdv.laFinalTot < (-35000))
						{
						$scope.alertToast("Please enter Amount greater than (-35000)");
						}else{

							 console.log("final submittion");
							    $scope.saveVsFlag = false;
						    	$('div#saveVsDB').dialog({
									autoOpen: false,
									modal:true,
									resizable: false,
									show: UDShow,
									title:"LHPV ADVANCE FINAL SUBMISSION",
									hide: UDHide,
									position: UDPos,
									draggable: true,
									close: function(event, ui) { 
								        $(this).dialog('destroy');
								        $(this).hide();
								        $scope.saveVsFlag = true;
								    }
									});
								$('div#saveVsDB').dialog('open');
						}
				}else{
					console.log("final submittion");
				    $scope.saveVsFlag = false;
			    	$('div#saveVsDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						show: UDShow,
						title:"LHPV ADVANCE FINAL SUBMISSION",
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					        $scope.saveVsFlag = true;
					    }
						});
					$('div#saveVsDB').dialog('open');
				}
		 }
	 }
	 
	 $scope.back = function(){
		 console.log("enter into back function");
		 $('div#saveVsDB').dialog('close');
	 } 
	 
	 
	 $scope.saveVS = function(){
		 console.log("enter into saveVS function ");
			$scope.vs.lhpvAdvList = $scope.lhpvAdvList;
			$scope.laFinalTotSum=0;
			faCodeTemp='';
			//console.log($scope.vs.chequeLeaves);
			console.log($scope.vs);
			
			$('#saveId').attr("disabled","disabled");
			var response = $http.post($scope.projectName+'/submitLhpvAdv',$scope.vs);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$('div#saveVsDB').dialog('close');
					$('#saveId').removeAttr("disabled");
					
					$scope.alertToast("Successfully save LHPV ADVANCE -->> "+data.laNo);
					
					$scope.vs = {};
					$scope.vs.payBy = '';
					$scope.challan = "";
					$scope.lhpvAdv = {};
					$scope.bnkList = [];
					$scope.chlnList = [];
					$scope.actBOList = [];
					$scope.lhpvAdvList = [];
					//$scope.vs.chequeLeaves.chqLChqNo="";
					
					$scope.vs.chequeType = '';
					$('#chequeTypeId').attr("disabled","disabled");
					$scope.vs.bankCode = "";
					$('#bankCodeId').attr("disabled","disabled");
					//$scope.vs.chequeLeaves.chqLChqNo ="" ;
					$('#chequeNoId').attr("disabled","disabled");
					
					$scope.getLhpvDet();
				}else{
					console.log("Error in submitVoucher");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	 }
	 
	 $scope.verifyChallan = function() {
		console.log("verifyChallan()");
		if (angular.isUndefined($scope.challan) || $scope.challan === "" || $scope.challan === null) {
			$scope.alertToast("Please Enter Challan No");
		} else {
			
			$scope.lodingFlag = true;
			
			var temp = {
					"chlnNo":$scope.challan,
					"chqNo" :$scope.vs.chequeLeaves.chqLChqNo
			};
			
			
			var res = $http.post($scope.projectName+'/verifyChlnForLhpvAdvRvrs',temp);
			res.success(function(data, status, headers, config) {
				if (data.result === "success") {
					
					
					$scope.vs.brkOwnFaCode = data.faCode;
					//$scope.lhpvAdvList = [];
					if (angular.isUndefined(faCodeTemp) || faCodeTemp === "" || faCodeTemp === null) {
						faCodeTemp = data.faCode;
						$scope.vs.brkOwnFaCode = data.faCode;
						$scope.laFinalTotSum=0;
						$scope.lhpvAdvList = [];
					} else {
						if (faCodeTemp === data.faCode) {
						} else {
							faCodeTemp = data.faCode;
							$scope.vs.brkOwnFaCode = data.faCode;
							$scope.laFinalTotSum=0;
							$scope.lhpvAdvList = [];
						}
					}
					
					if(data.finlTotAmt > 0){

						$scope.saveChln($scope.challan);

						$scope.lhpvAdv.laLryAdvP=(-1)*(data.loryAdvP);
						$scope.lhpvAdv.laCashDiscR=(-1)*(data.cashDisc);
						$scope.lhpvAdv.laMunsR=(-1)*(data.munsiana);
						$scope.lhpvAdv.laTdsR=(-1)*(data.tdsAmt);
						$scope.lhpvAdv.laTotPayAmt=(-1)*(data.payAmt);
						$scope.lhpvAdv.laTotRcvrAmt=(-1)*(data.recvrAmt);
						$scope.lhpvAdv.laFinalTot=(-1)*(data.finlTotAmt);
						$scope.vs.payBy=data.payBy;
						$scope.vs.chequeType=data.cheqType;
						$scope.vs.bankCode=data.bankCode;
						if($scope.vs.payBy=='Q'){
							$scope.vs.chequeLeaves.chqLChqNo=data.cheqNo;
							
							console.log($scope.vs.chequeLeaves.chqLChqNo);
						}else{
							$scope.vs.chequeLeaves.chqLChqNo="";
						}
					
						
						
					console.log($scope.lhpvAdv.laFinalTot);

						
					}else{
						$scope.alertToast("Please Entre Valid Challan No.");
					}
					
					
				}else {
					$scope.alertToast(data.msg);
				}
				$scope.lodingFlag = false;
			});
			res.error(function(data, status, headers, config) {
				console.log("error in response of verifyChlnForLhpvAdv: "+data);
				$scope.lodingFlag = false;
			});
		}
	}
	
	 $scope.notifyMsg = function() {
		console.log("notifyMsg");
		if (angular.isUndefined($scope.vs.brkOwnFaCode) || $scope.vs.brkOwnFaCode === "" || $scope.vs.brkOwnFaCode === null) {
			$scope.alertToast("Please Entre Challan No and Verify Challan");
		}		
	}
	 
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getLhpvDet();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
	
	
}]);
'use strict';

var app = angular.module('application');

app.controller('EditChlnCntlr',['$scope','$location','$http','$filter','$window', 'FileUploadService',
                                 function($scope,$location,$http,$filter,$window,FileUploadService){

	console.log("EditChlnCntlr Started");
	
	$scope.chlnNoList = [];
	$scope.chln = {};
	$scope.chd = {};

	$scope.chlnDBFlag = true;
	
	$scope.showChlnFlag = false;
	
	$scope.branchList = [];
	$scope.branch = {};
	
	$scope.stnList = [];
	$scope.frmStn = {};
	$scope.toStn = {};
	
	$scope.empList = [];
	$scope.emp = {};
	
	$scope.vehTypeList = [];
	
	$scope.brkList = [];
	$scope.brk = {};
	
	$scope.ownList = [];
	$scope.own = {};
	
	$scope.stateList = [];
	
	$scope.branchDBFlag = true;
	$scope.frmStnDBFlag = true;
	$scope.toStnDBFlag = true;
	$scope.empDBFlag = true;
	$scope.payAtDBFlag = true;
	$scope.vehicleTypeDBFlag = true;
	$scope.vehDBFlag = true;
	$scope.brkDBFlag = true;
	$scope.ownDBFlag = true;
	$scope.perStateDBFlag = true;
	$scope.panIssueStateDBFlag = true;
	
	$scope.vehList = [];
	 
	var chlnRemAdvDB;                
	var chlnAdvDB;
	var chlnBalanceDB;
	var chlnRemBalDB;
	
	$scope.getChlnNoList = function(){
		console.log(" entered into getChlnNoList------>");
		var response = $http.post($scope.projectName+'/getChlnNoList');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.chlnNoList = data.chlnNoList;
				$scope.getBranchList();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.openChlnDB = function(){
		console.log("Entered into openChlnDB");
		$scope.chlnDBFlag = false;
		$('div#chlnDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Challan No",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.chlnDBFlag = true;
			}
		});

		$('div#chlnDB').dialog('open');
	}
	
	$scope.saveChlnCode = function(chlnCode){
		$scope.chln.chlnCode = chlnCode;
		//$('div#chlnDB').dialog('close');
	}
	
	//this method get the result from database on subbmission
	$scope.getChln = function() {
		console.log("getChln()");
		var response = $http.post($scope.projectName+'/getChlnForEdit', $scope.chln.chlnCode);
		response.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log(data.result);
				$scope.chln = data.challan;
				$scope.chd = data.challanDetail;
				$scope.showChlnFlag = true;
				console.log("chln "+$scope.chln.chlnId);
				console.log("chd "+$scope.chd.chdId);
				
				$scope.getBranch();
				$scope.getFrmStn();
				$scope.getToStn();
				$scope.getEmp();
				$scope.getPayAt();
				$scope.getOwn();
				$scope.getBrk();
				
				chlnRemAdvDB=$scope.chln.chlnRemAdv;                
	            chlnAdvDB=$scope.chln.chlnAdvance;
	                
	            chlnBalanceDB=$scope.chln.chlnBalance;
	            chlnRemBalDB=$scope.chln.chlnRemBal;
	                
	            $scope.calRemAdvAndRemBal();
				
			}else {
				console.log(data.result);
				$scope.showChlnFlag = false;
				$scope.alertToast("challan not found");
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("error in response: "+data);
			$scope.showChlnFlag = false;
			$scope.alertToast("Database Exception");
		})
	}
	
	$scope.getBranch = function() {
		console.log("getBranch()");
		for ( var i = 0; i < $scope.branchList.length; i++) {
			if ($scope.branchList[i].branchId === parseInt($scope.chln.branchCode)) {
				console.log("entered If: "+$scope.branchList[i].branchName);
				$scope.branch = $scope.branchList[i];
				
			}
		}
	}
	
	$scope.getFrmStn = function() {
		console.log("getFrmStn()");
		for ( var i = 0; i < $scope.stnList.length; i++) {
			if ($scope.stnList[i].stnId === parseInt($scope.chln.chlnFromStn)) {
				$scope.frmStn = $scope.stnList[i];
			}
		}
	}
	
	$scope.getToStn = function() {
		console.log("toStn()");
		for ( var i = 0; i < $scope.stnList.length; i++) {
			if ($scope.stnList[i].stnId === parseInt($scope.chln.chlnToStn)) {
				$scope.toStn = $scope.stnList[i];
			}
		}
	}
	
	$scope.getEmp = function() {
		console.log("getEmp()");
		for ( var i = 0; i < $scope.empList.length; i++) {
			if ($scope.empList[i].empId === parseInt($scope.chln.chlnEmpCode)) {
				$scope.emp = $scope.empList[i];
			}
		}
	}
	
	$scope.getOwn = function() {
		console.log("getOwn()");
		for ( var i = 0; i < $scope.ownList.length; i++) {
			if ($scope.ownList[i].ownCode === $scope.chd.chdOwnCode) {
				$scope.own = $scope.ownList[i];
			}
		}
	}
	
	$scope.getBrk = function() {
		console.log("getBrk()");
		for ( var i = 0; i < $scope.brkList.length; i++) {
			if ($scope.brkList[i].brkCode === $scope.chd.chdBrCode) {
				$scope.brk = $scope.brkList[i];
			}
		}
	}
	
	$scope.getPayAt = function() {
		console.log("getPayAt()");
		for ( var i = 0; i < $scope.branchList.length; i++) {
			if ($scope.branchList[i].branchId === parseInt($scope.chln.chlnPayAt)) {
				$scope.payAt = $scope.branchList[i];
			}
		}
	}
	
	$scope.getStnList = function() {
		console.log("getStnList()");
		var response = $http.post($scope.projectName+'/getStnList');
		response.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("stnList result: "+data.result);
				$scope.stnList = data.stnList;
				$scope.getEmpList();
			} else {
				console.log("stnList result: "+data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("Error in stnList response: "+data);
		});
	}
	
	$scope.getVehTypeList = function() {
		console.log("getVehTypeList()");
		var response = $http.post($scope.projectName+'/getVehTypeList');
		response.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("vehicleTypeList result: "+data.result);
				$scope.vehTypeList = data.vehTypeList;
				$scope.getVehicleMstr();
			} else {
				console.log("vehicleTypeList result: "+data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("Error in vehTypeList response: "+data);
		})
	}
	
	$scope.getBranchList = function() {
		console.log("getBranch()");
		var response = $http.post($scope.projectName+'/getActiveBrNCI');
		response.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("branch List result: "+data.result);
				$scope.branchList = data.branchNCIList;
				$scope.getStnList();
			} else {
				console.log("branch List result: "+data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("Erro in branchlist response: "+data);
		});
	}
	
	//only client code
	$scope.getEmpList = function() {
		console.log("getEmpList()");
		var res = $http.post($scope.projectName+'/getEmpList');
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("empList result:"+data.result);
				$scope.empList = data.empList;
				$scope.getVehTypeList();
			} else {
				console.log("empList result:"+data.result);
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in empList response"+data);
		});
	}
	
	$scope.openBranchDB = function() {
		console.log("openBranchDB()");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});

		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch = function(branch){
		console.log("saveBranch()");
		$scope.branch = branch;
		$('div#branchDB').dialog('close');
	}
	
	$scope.openFrmStnDB = function() {
		console.log("openFrmStnDB()");
		$scope.frmStnDBFlag = false;
		$('div#frmStnDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "From Station",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.frmStnDBFlag = true;
			}
		});

		$('div#frmStnDB').dialog('open');
	}
	
	$scope.saveFrmStn = function(frmStn){
		console.log("saveFrnStn()");
		$scope.frmStn = frmStn;
		$('div#frmStnDB').dialog('close');
	}
	
	$scope.openToStnDB = function() {
		console.log("openToStnDB()");
		$scope.toStnDBFlag = false;
		$('div#toStnDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "To Station",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.toStnDBFlag = true;
			}
		});

		$('div#toStnDB').dialog('open');
	}
	
	$scope.saveToStn = function(toStn){
		console.log("saveToStn()");
		$scope.toStn = toStn;
		$('div#toStnDB').dialog('close');
	}
	
	$scope.openVehicleTypeDB = function() {
		console.log("openVehicleTypeDB");
		$scope.vehicleTypeDBFlag = false;
		$('div#vehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Vehicle Type",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.vehicleTypeDBFlag = true;
			}
		});

		$('div#vehicleTypeDB').dialog('open');
	}
	
	$scope.saveVehicleType = function(vehicleType){
		console.log("saveVehicleType()");
		$scope.chln.chlnVehicleType	 = vehicleType.vtVehicleType;
		$('div#vehicleTypeDB').dialog('close');
	}
	
	$scope.openBrkDB = function() {
		console.log("openBrkDB()");
		$scope.brkDBFlag = false;
		$('div#brkDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Broker",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.brkDBFlag = true;
			}
		});

		$('div#brkDB').dialog('open');
	}
	
	$scope.saveBrk = function(brk) {
		console.log("saveBrk()");
		//empty broker phone text box
		$scope.chd.chdBrMobNo = "";
		$scope.brk = brk;
		$('div#brkDB').dialog('close');
	}
	
	//openOwnDB
	$scope.openOwnDB = function() {
		console.log("openOwnDB()");
		$scope.ownDBFlag = false;
		$('div#ownDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Owner",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.ownDBFlag = true;
			}
		});

		$('div#ownDB').dialog('open');
	}
	
	$scope.saveOwn = function(own) {
		console.log("saveOwn()");
		$scope.chd.chdOwnMobNo = "";
		$scope.own = own;
		$('div#ownDB').dialog('close');
	}
	
	$scope.calFreight = function() {
		console.log("calFreight()");
		$scope.chln.chlnFreight = (parseFloat(parseFloat($scope.chln.chlnLryRate*$scope.chln.chlnChgWt).toFixed(2))).toString();
		console.log("freight: "+$scope.chln.chlnFreight);
		$scope.calTotalFreight();
	}
	
	$scope.calTotalFreight = function() {
		console.log("calFreight()");
		$scope.chln.chlnTotalFreight = (parseFloat(parseFloat(parseFloat($scope.chln.chlnFreight)+parseFloat($scope.chln.chlnLoadingAmt)+parseFloat($scope.chln.chlnExtra)-parseFloat($scope.chln.chlnStatisticalChg)-parseFloat($scope.chln.chlnTdsAmt)).toFixed(2)));
		console.log("freight: "+$scope.chln.chlnTotalFreight);
		$scope.calBalance();
	}
	
	$scope.calBalance = function() {
		console.log("calBalance()")
		$scope.chln.chlnBalance = parseFloat(parseFloat($scope.chln.chlnTotalFreight-$scope.chln.chlnAdvance).toFixed(2));
		
		$scope.calRemAdvAndRemBal();
	}
	
	$scope.calRemAdvAndRemBal = function(){
        console.log("calRemAdvAndRemBal()")
        $scope.chln.chlnRemAdv =parseFloat(parseFloat($scope.chln.chlnAdvance-parseFloat(chlnAdvDB-chlnRemAdvDB)).toFixed(2));
        $scope.chln.chlnRemBal=parseFloat(parseFloat($scope.chln.chlnBalance-parseFloat(chlnBalanceDB-chlnRemBalDB)).toFixed(2));
    }
	
	$scope.openPayAtDB = function() {
		console.log("openPayAtDB()");
		$scope.payAtDBFlag = false;
		$('div#payAtDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Pay At",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.payAtDBFlag = true;
			}
		});

		$('div#payAtDB').dialog('open');
	}
	
	$scope.savePayAt = function(payAt) {
		console.log("savePayAt()");
		$scope.payAt  = payAt;
		$('div#payAtDB').dialog('close');
	}
	
	$scope.openEmpDB = function() {
		console.log("openBillAtDB()");
		$scope.empDBFlag = false;
		$('div#empDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.empDBFlag = true;
			}
		});

		$('div#empDB').dialog('open');
	}
	
	$scope.saveEmp = function(emp) {
		console.log("saveEmpAt()");
		$scope.emp  = emp;
		$('div#empDB').dialog('close');
	}
	
	$scope.openPerStateDB = function() {
		console.log("openPerStateDB()");
		$scope.perStateDBFlag = false;
		$('div#perStateDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "State",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.perStateDBFlag = true;
			}
		});

		$('div#perStateDB').dialog('open');
	}
	
	$scope.savePerState = function(perState) {
		console.log("savePerState()");
		$scope.chd.chdPerState  = perState;
		$('div#perStateDB').dialog('close');
	}
	
	$scope.openPanIssueStateDB = function() {
		console.log("openPanIssueStateDB()");
		$scope.panIssueStateDBFlag = false;
		$('div#panIssueStateDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "State",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.panIssueStateDBFlag = true;
			}
		});

		$('div#panIssueStateDB').dialog('open');
	}
	
	$scope.savePanIssueState = function(panIssueState) {
		console.log("panIssueState()");
		$scope.chd.chdPanIssueSt  = panIssueState;
		$('div#panIssueStateDB').dialog('close');
	}
	
	//challanDetail
	$scope.getVehicleMstr = function(){
		console.log("enter into getVehicleMstr function");
		var response = $http.post($scope.projectName+'/getVehicleMstrFC');
		  response.success(function(data, status, headers, config){
			 if(data.result === "success"){
				 $scope.vehList = data.list;
				 console.log("size of $scope.vehList = "+$scope.vehList.length);
				 $scope.getBrokerList();
			 }else{
				 $scope.alertToast("Vehicles are not avaliable");
				 console.log("error in fetching data from getVehicleMstr");
			 }
		  });
		  response.error(function(data, status, headers,config) {
			  $scope.errorToast(data);
		  });
	  }
	
	$scope.openVehicleDB = function(){
		  console.log("enter into openVehicleDB funciton");
		  $scope.vehDBFlag=false;
		  $('div#vehDB').dialog({
			  autoOpen: false,
			  modal:true,
			  title: "Select Vehicle",
			  show: UDShow,
			  hide: UDHide,
			  position: UDPos,
			  resizable: false,
			  draggable: true,
			  close: function(event, ui) { 
				  $(this).dialog('destroy') ;
				  $(this).hide();
				  $scope.vehDBFlag=true;
			  }
		  });

		  $('div#vehDB').dialog('open');
	  }
	
	$scope.selectVeh = function(selVeh){
		  console.log("enter into selectVeh function");
		  
		  //clear mobile
		  $scope.chd.chdBrMobNo = "";
		  $scope.chd.chdOwnMobNo = "";
		  
		 
		  
		  var response = $http.post($scope.projectName+'/selectVehFC',selVeh);
		  response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$('div#vehDB').dialog('close');
				$scope.vehM = data.vehM;
				$scope.chln.chlnLryNo = $scope.vehM.vvRcNo;
				/*$scope.chdOwnCodeTemp = data.ownName;*/
				$scope.chd.chdOwnCode = data.ownCode;
				/*$scope.chdBrCodeTemp = data.brkName;*/
				$scope.chd.chdBrCode= data.brkCode;
				
				//change owner and broker
				$scope.getOwn();
				$scope.getBrk();
				
				//$scope.brkrcode = $scope.chd.chdBrCode; 
				//$scope.ownrcode = $scope.chd.chdOwnCode;
				$scope.mobListBk = [];
				$scope.mobListBk = data.brkMobList;
				if($scope.mobListBk.length > 0){
					$scope.chd.chdBrMobNo = $scope.mobListBk[0];
				}else{
					//$('#openMobbrk').removeAttr("disabled");
				}
				$scope.mobListOw = []; 
				$scope.mobListOw = data.ownMobList;
				if($scope.mobListOw.length > 0){
					$scope.chd.chdOwnMobNo = $scope.mobListOw[0];
				}else{
					//$('#openMobown').removeAttr("disabled");
				}
				
				$scope.chd.chdDvrName       = $scope.vehM.vvDriverName;
				$scope.chd.chdDvrMobNo      = $scope.vehM.vvDriverMobNo;
				$scope.chd.chdRcNo          = $scope.vehM.vvRcNo;
				$scope.chd.chdRcIssueDt     = $scope.vehM.vvRcIssueDt;
				$scope.chd.chdRcValidDt     = $scope.vehM.vvRcValidDt;
				$scope.chd.chdDlNo          = $scope.vehM.vvDriverDLNo;
				$scope.chd.chdDlIssueDt     = $scope.vehM.vvDriverDLIssueDt;
				$scope.chd.chdDlValidDt     = $scope.vehM.vvDriverDLValidDt;
				$scope.chd.chdPerNo         = $scope.vehM.vvPerNo;
				$scope.chd.chdPerIssueDt    = $scope.vehM.vvPerIssueDt;
				$scope.chd.chdPerValidDt    = $scope.vehM.vvPerValidDt;
				$scope.chd.chdFitDocNo      = $scope.vehM.vvFitNo;
				$scope.chd.chdFitDocIssueDt = $scope.vehM.vvFitIssueDt;
				$scope.chd.chdFitDocValidDt = $scope.vehM.vvFitValidDt;
				$scope.chd.chdFitDocPlace   = $scope.vehM.vvFitState;
				$scope.chd.chdBankFinance   = $scope.vehM.vvFinanceBankName;
				$scope.chd.chdPolicyNo      = $scope.vehM.vvPolicyNo;
				$scope.chd.chdPolicyCom     = $scope.vehM.vvPolicyComp;
				$scope.chd.chdTransitPassNo = $scope.vehM.vvTransitPassNo;
				
				$scope.chln.chlnLryNo		= $scope.vehM.vvRcNo;
				
			}else{
				$scope.alertToast("error in fetching data form selectVeh");
			}
		  });
		  response.error(function(data, status, headers,config) {
			  $scope.errorToast(data);
		  });
	  }

	$scope.getBrokerList = function(){
		  console.log("getBrokerCode------>");
		  var response = $http.post($scope.projectName+'/getBrkCodeList');
		  response.success(function(data, status, headers, config){
			  if(data.result==="success"){
				  $scope.brkList = data.brkCodeList;
				  $scope.getOwnerList();
			  }else{
				  $scope.alertToast("you don't have any broker");
				  console.log(data);   
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }
	
	$scope.getOwnerList = function(){
		  console.log("getOwnerCode------>");
		  var response = $http.post($scope.projectName+'/getOwnCodeList');
		  response.success(function(data, status, headers, config){
			  if(data.result==="success"){
				  $scope.ownList = data.ownCodeList;
				  $scope.getStateList();
			  }else{
				  $scope.alertToast("you don't have any owner");
				  console.log(data);   
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data.result);
		  });
	  }
	
	$scope.getStateList = function(){
		  console.log("getStateList------>");
		  var response = $http.post($scope.projectName+'/getStateList');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.stateList = data.stateList;
				  console.log("data from server-->"+$scope.stateList);
			  }else{
				  $scope.alertToast("you don't have any state list");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers, config) {
			  $scope.errorToast(data);
		  });
	  }
	
	$scope.submitChallan = function(chlnForm) {
		console.log("Enter into submitChallan()...");
		var ownBrkId = {
				"ownId" : $scope.own.ownId,
				"brkId" : $scope.brk.brkId
		}
		
		console.log("Owner Id : "+$scope.own.ownId);
		console.log("Broker Id : "+$scope.brk.brkId);
		
		var res = $http.post($scope.projectName+'/validateOwnBrkPan', ownBrkId);
		res.success(function(data, status, headers, config) {
			if (data.result==="success") {
				console.log("validateOwnBrkPan result: "+data.result);
				//$scope.alertToast(data.msg);				
				//submit challan and challan detail
				$scope.updateChlnNChd(chlnForm);				
			} else {
				console.log("validateOwnBrkPan result: "+data.result);
				$scope.alertToast(data.msg);
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in response validateOwnBrkPan "+data);
		});
	}
	
	$scope.updateChlnNChd = function(chlnForm) {
		console.log("updateChlnNChd()");
		if (chlnForm.$invalid) {
			
			$('input.ng-invalid').first().focus();			
			
			console.log("invalidate: "+chlnForm.$invalid);
			if (chlnForm.branchName.$invalid) {
				$scope.alertToast("Please enter valid Branch Name");
			} else if (chlnForm.frmStnName.$invalid) {
				$scope.alertToast("Please enter valid From Station");
			} else if (chlnForm.toStnName.$invalid) {
				$scope.alertToast("Please enter valid To Station");
			} else if (chlnForm.empName.$invalid) {
				$scope.alertToast("Please enter valid Employee");
			} else if (chlnForm.chlnNoOfPkgName.$invalid) {
				$scope.alertToast("Please enter valid No of Package");
			} else if (chlnForm.chlnLryRateName.$invalid) {
				$scope.alertToast("Please enter valid Lry Rate");
			} else if (chlnForm.chlnChgWtName.$invalid) {
				$scope.alertToast("Please enter valid Charge Weight");
			} else if (chlnForm.chlnFreightName.$invalid) {
				$scope.alertToast("Please enter valid Freight");
			} else if (chlnForm.chlnLoadingAmtName.$invalid) {
				$scope.alertToast("Please enter valid Loading Amt");
			} else if (chlnForm.chlnExtraName.$invalid) {
				$scope.alertToast("Please enter Extra Amt");
			} else if (chlnForm.chlnStatisticalChgName.$invalid) {
				$scope.alertToast("Please enter valid St Charge");
			} else if (chlnForm.chlnTdsAmtName.$invalid) {
				$scope.alertToast("Please enter valid Tds Amt");
			} else if (chlnForm.chlnAdvanceName.$invalid) {
				$scope.alertToast("Please enter valid Advance");
			} else if (chlnForm.chlnBalanceName.$invalid) {
				$scope.alertToast("Please enter valid Balance");
			} else if (chlnForm.chlnBrRateName.$invalid) {
				$scope.alertToast("Please enter valid BR Rate");
			} else if (chlnForm.chlnTotalWtName.$invalid) {
				$scope.alertToast("Please enter valid Total Weight");
			} else if (chlnForm.chlnRemAdvName.$invalid) {
				$scope.alertToast("Please enter valid Remaining Advance");
			} else if (chlnForm.chlnRemBalName.$invalid) {
				$scope.alertToast("Please enter valid Remaining Balance");
			} else if (chlnForm.chlnDtName.$invalid) {
				$scope.alertToast("Please enter valid Challan Date");
			} else if (chlnForm.chlnWtSlipName.$invalid) {
				$scope.alertToast("Please enter valid Weight Slip");
			} else if (chlnForm.chlnVehicleTypeName.$invalid) {
				$scope.alertToast("Please enter valid Vehivle type");
			} else if (chlnForm.chlnLryLoadTimeName.$invalid) {
				$scope.alertToast("Please enter valid Lry Load Time");
			} else if (chlnForm.chlnLryNoName.$invalid) {
				$scope.alertToast("Please enter valid Challan Lry No");
			} else if (chlnForm.chdRcNoName.$invalid) {
				$scope.alertToast("Please enter valid Challan Detail Rc No");
			} else if (chlnForm.chdBrCodeName.$invalid) {
				$scope.alertToast("Please enter valid Challan Detail Broker Code");
			} else if (chlnForm.chdOwnCodeName.$invalid) {
				$scope.alertToast("Please enter valid Challan Detail Owner Code");
			} else if(chlnForm.chlnTimeAllowName.$invalid){
				$scope.alertToast("Please enter valid Time Allow");
			} else if(chlnForm.chlnTotalFreightName.$invalid){
				$scope.alertToast("Please enter valid Total Freight");
			}else if(chlnForm.chdDvrMobNoName.$invalid){
				$scope.alertToast("Please enter valid Driver Mobile No.");
			}else if(chlnForm.chdDlNoName.$invalid){
				$scope.alertToast("Please enter valid Driver DL No.");
			}else if(chlnForm.chdPerNoName.$invalid){
				$scope.alertToast("Please enter valid Permit No.");
			}else if(chlnForm.chdPolicyNoName.$invalid){
				$scope.alertToast("Please enter valid Policy No.");
			}
		} else {
			console.log("invalidate: "+chlnForm.$invalid);
			$scope.chln.branchCode	= $scope.branch.branchId;
			$scope.chln.chlnFromStn	= $scope.frmStn.stnId;
			$scope.chln.chlnToStn	= $scope.toStn.stnId;
			$scope.chln.chlnEmpCode	= $scope.emp.empId;
			$scope.chln.chlnPayAt	= $scope.payAt.branchId;
			$scope.chd.chdOwnCode	= $scope.own.ownCode;
			$scope.chd.chdBrCode	= $scope.brk.brkCode;
			
			var chlnNChdService = {
				"challan" : $scope.chln,
				"challanDetail" : $scope.chd
			};
			
			var res = $http.post($scope.projectName+'/updateChlnNChd', chlnNChdService);
			res.success(function(data, status, headers, config) {
				if (data.result==="success") {
					$scope.alertToast("update: "+data.result);
					
					//clear the resources
					$scope.chln = {};
					$scope.chd = {};

					$scope.chlnDBFlag = true;
					
					$scope.showChlnFlag = false;
					
					$scope.branch = {};
					$scope.payAt = {};
					
					$scope.frmStn = {};
					$scope.toStn = {};
					
					$scope.emp = {};
					
					$scope.brk = {};
					
					$scope.own = {};
					
					$scope.branchDBFlag = true;
					$scope.frmStnDBFlag = true;
					$scope.toStnDBFlag = true;
					$scope.empDBFlag = true;
					$scope.payAtDBFlag = true;
					$scope.vehicleTypeDBFlag = true;
					$scope.vehDBFlag = true;
					$scope.brkDBFlag = true;
					$scope.ownDBFlag = true;
					$scope.perStateDBFlag = true;
					$scope.panIssueStateDBFlag = true;
				} else {
					$scope.alertToast("update: "+data.result);
				}
			});
			res.error(function(data, status, headers, config) {
				console.log("error in response in updateChlnNChd: "+data);
			});
		}
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		//$scope.getChlnNoList();
		$scope.getBranchList();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("EditChlnCntlr Ended");
}]);
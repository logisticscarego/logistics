'use strict';

var app = angular.module('application');

app.controller('CustomerRepNewCntlr',['$scope','$location','$http','$window',
                                      function($scope,$location,$http,$window){

	$scope.CustomerCodeDBFlag = true;
	$scope.custrep = {};
	$scope.custCodeTemp = "";

	$('#crMobileNo').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#crMobileNo').keypress(function(e) {
		if (this.value.length == 15) {
			e.preventDefault();
		}
	});

	$('#crName').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});


	$('#custRefNo').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});

	$('#crDesignation').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});


	$scope.OpenCustomerCodeDB = function(){
		$scope.CustomerCodeDBFlag=false;
		$('div#customerCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Customer Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#customerCodeDB').dialog('open');
	}


	$scope.saveCustomerCode = function(cust){
		$scope.custrep.custCode = cust.custCode;
		$scope.custCodeTemp = cust.custFaCode;
		$('div#CustomerCodeDB').dialog('close');
		$scope.CustomerCodeDBFlag=true;
	}

	$scope.getCustDesigList = function(custrep){
		$( "#crDesignation" ).autocomplete({
			source: $scope.availableTags
		});
	} 

	$scope.fillCustDesigVal = function(){
		console.log("enter into myfun----------->>"+$( "#crDesignation" ).val());
		$scope.custrep.crDesignation=$( "#crDesignation" ).val();
	}

	$scope.getDesignation = function(){
		var response = $http.get($scope.projectName+'/getDesignationForCr');
		response.success(function(data, status, headers, config) {
			$scope.availableTags = data;
			console.log("******************");
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.nextAddNewCR = function(NewCustomerRepForm,custrep){
		console.log("enter into next function NewCustomerRepForm.$invalid--->"+NewCustomerRepForm.$invalid);
		if(NewCustomerRepForm.$invalid){
			if(NewCustomerRepForm.custCode.$invalid){
				$scope.alertToast("please enter customer code..");
			}else if(NewCustomerRepForm.crDesignation.$invalid){
				$scope.alertToast("please enter customer representative name..");
			}else if(NewCustomerRepForm.crDesignation.$invalid){
				$scope.alertToast("please enter customer representative designation address..");
			}else if(NewCustomerRepForm.crMobileNo.$invalid){
				$scope.alertToast("Please enter phone number of 4-15 digits..");
			}else if(NewCustomerRepForm.crEmailId.$invalid){
				$scope.alertToast("Please enter correct format of email id..");
			}else if(NewCustomerRepForm.custRefNo.$invalid){
				$scope.alertToast("Please enter customer representative referance no..");
			}
		}else{
			
			
			$('#saveId').attr("disabled","disabled");
			var response = $http.post($scope.projectName+'/CustomerRepresentativeNew', custrep);
			response.success(function(data, status, headers, config) {
				if(data.result === "success"){
					console.log(data);
					$('#saveId').removeAttr("disabled");
					$scope.custrep.custCode="";
					$scope.custrep.custRefNo="";
					$scope.custrep.crName="";
					$scope.custrep.crDesignation="";
					$scope.custrep.crMobileNo="";
					$scope.custrep.crEmailId="";
					$scope.custCodeTemp = "";
					$location.path("/operator/"+data.targetPage);
					$scope.successToast(data.result);
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				console.log("error in CustomerRepCntlr");
				$scope.errorToast(data);
			});
		}
	}


	$scope.getCustomerCodeList = function(){
		console.log("getCustomerCodeData------>");
		var response = $http.post($scope.projectName+'/getCustListFV');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.custCodeList = data.list;
				$scope.getDesignation();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("error in CustomerRepCntlr");
			$scope.errorToast(data);
		});
	}

	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getCustomerCodeList();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
	

}]);
'use strict';

var app = angular.module('application');

app.controller('BrhWiseAssgnCntlr',['$scope','$location','$http',
                                 function($scope,$location,$http){
	
	$scope.branchList = [];
	$scope.existEmpList = [];
	$scope.newEmpList = [];
	
	
	$scope.temp = {};
	$scope.branch = {};
	
	
	$scope.openBrDBFlag = true;
	
	
	$scope.clrBtnBranchDirectorNE=false;
	$scope.clrBtnbranchMarketingHDNE=false;
	$scope.clrBtnBranchOutStandingHDNE=false;
	$scope.clrBtnBranchMarketingNE=false;
	$scope.clrBtnBranchMngrNE=false;
	$scope.clrBtnBranchCashierNE=false;
	$scope.clrBtnBranchTrafficNE=false;
	$scope.clrBtnBranchAreaMngrNE=false;
	$scope.clrBtnBranchRegionalMngrNE=false;
	$scope.clrBtnBranchDirectorEE=false;
	$scope.clrBtnbranchMarketingHDEE=false;
	$scope.clrBtnBranchOutStandingHDEE=false;
	$scope.clrBtnBranchMarketingEE=false;
	$scope.clrBtnBranchMngrEE=false;
	$scope.clrBtnBranchCashierEE=false;
	$scope.clrBtnBranchTrafficEE=false;
	$scope.clrBtnBranchAreaMngrEE=false;
	$scope.clrBtnBranchRegionalMngrEE=false;
	
	
	$scope.newEmpFrDrFlag = true; 
	$scope.existEmpFrDrFlag = true;
	$scope.newEmpFrBmhFlag = true;
	$scope.existEmpFrBmhFlag = true;
	$scope.newEmpFrOshFlag = true;
	$scope.existEmpFrOshFlag = true;
	$scope.newEmpFrBmkFlag = true;
	$scope.existEmpFrBmkFlag = true;
	$scope.newEmpFrBmgFlag = true;
	$scope.existEmpFrBmgFlag = true;
	$scope.newEmpFrCshFlag = true;
	$scope.existEmpFrCshFlag = true;
	$scope.newEmpFrTrfFlag = true;
	$scope.existEmpFrTrfFlag = true;
	$scope.newEmpFrAmgFlag = true;
	$scope.existEmpFrAmgFlag = true;
	$scope.newEmpFrRmgFlag = true;
	$scope.existEmpFrRmgFlag = true;
	
	$scope.viewBranchDetailsFlag = true;
	
	$scope.getBranch = function(){
		console.log("enter into getBranch function");
		var response = $http.post($scope.projectName+'/getBrFrBWAssgn');
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.branchList = data.list;
				$scope.getExistEmp();
			}else{
				console.log("error in bringing branch list");
			}
	    });
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	
	$scope.openBranchDB = function(){
		console.log("enter into openBranchDB function");
		$scope.openBrDBFlag = false;
		$('div#openBrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.openBrDBFlag = true;
			}
		});
		$('div#openBrDB').dialog('open');
	}
	
	
	$scope.saveBranch = function(br){
		console.log("enter into saveBranch function");
		$scope.branch = br;
		$('div#openBrDB').dialog('close');
	}
	
	
	$scope.getExistEmp = function(){
		console.log("enter into getExistEmp function");
		var response = $http.post($scope.projectName+'/getExistEmp');
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.existEmpList = data.list;
				$scope.getNewEmp();
			}else{
				console.log("error in brining existing employee");				
			}	
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	
	$scope.getNewEmp = function(){
		console.log("enter into getNewEmp funciton");
		var response = $http.post($scope.projectName+'/getNewEmp');
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.newEmpList = data.list;
			}else{
				$scope.alertToast("New Employees are not existing");				
			}
	    });
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});	
	}
	
	
	$scope.branchDirectorRadio = function(){
		console.log("enter into branchDirectorRadio function");
		/*$scope.clrBtnBranchDirectorNE = true;
		$scope.clrBtnBranchDirectorEE = false;*/
		
		$scope.newEmpFrDrFlag = false;
		$('div#newEmpFrDr').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select New Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.newEmpFrDrFlag = true;
			}
		});
		$('div#newEmpFrDr').dialog('open');
	}
	
	
	$scope.saveNewDr = function(emp){
		console.log("enter into saveNewDr function");
		$('div#newEmpFrDr').dialog('close');
		$scope.temp.branchDirector = emp.empName;
		$scope.branch.branchDirector = emp.empCode;
		
		/*$scope.clrBtnBranchDirectorNE = true;
		$scope.clrBtnBranchDirectorEE = false;*/
	}
	
	
	$scope.openBranchDirectorDB = function(){
		console.log("enter into openBranchDirectorDB function");
		/*$scope.clrBtnBranchDirectorNE = false;
		$scope.clrBtnBranchDirectorEE = true;*/
		
		$scope.existEmpFrDrFlag = false;
		$('div#existEmpFrDr').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select Existing Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.existEmpFrDrFlag = true;
			}
		});
		$('div#existEmpFrDr').dialog('open');
	}
	
	
	$scope.saveExsDr = function(emp){
		console.log("enter into saveExsDr function == "+emp.empName);
		$('div#existEmpFrDr').dialog('close');
		$scope.temp.branchDirector = emp.empName;
		$scope.branch.branchDirector = emp.empCode;
		
	/*	$scope.clrBtnBranchDirectorNE = false;
		$scope.clrBtnBranchDirectorEE = true;*/
	}
	
	
	$scope.branchMarketingHDRadio = function(){
		console.log("enter into branchMarketingHDRadio function");
		
		$scope.newEmpFrBmhFlag = false;
		$('div#newEmpFrBmh').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select New Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.newEmpFrBmhFlag = true;
			}
		});
		$('div#newEmpFrBmh').dialog('open');
	}
	
	
	$scope.saveNewBmh = function(emp){
		console.log("enter into saveNewBmh function");
		$('div#newEmpFrBmh').dialog('close');
		$scope.temp.branchMarketingHD = emp.empName;
		$scope.branch.branchMarketingHD = emp.empCode;
	}
	
	
	$scope.openBranchMarketingHDDB = function(){
		console.log("enter into openBranchMarketingHDDB function");
		$scope.existEmpFrBmhFlag = false;
		$('div#existEmpFrBmh').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select Existing Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.existEmpFrBmhFlag = true;
			}
		});
		$('div#existEmpFrBmh').dialog('open');
	}
	
	
	$scope.saveExsBmh = function(emp){
		console.log("enter into saveExsBmh function");
		$('div#existEmpFrBmh').dialog('close');
		$scope.temp.branchMarketingHD = emp.empName;
		$scope.branch.branchMarketingHD = emp.empCode;
	}
	
	
	$scope.branchOutStandingHDRadio = function(){
		console.log("enter into branchOutStandingHDRadio function");
		$scope.newEmpFrOshFlag = false;
		$('div#newEmpFrOsh').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select New Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.newEmpFrOshFlag = true;
			}
		});
		$('div#newEmpFrOsh').dialog('open');
	}
	
	
	$scope.saveNewOsh = function(emp){
		console.log("enter into saveNewOsh function");
		$('div#newEmpFrOsh').dialog('close');
		$scope.temp.branchOutStandingHD = emp.empName;
		$scope.branch.branchOutStandingHD = emp.empCode;
	}
	
	
	$scope.openBranchOutStandingHDDB = function(){
		console.log("enter into openBranchOutStandingHDDB function");
		$scope.existEmpFrOshFlag = false;
		$('div#existEmpFrOsh').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select Existing Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.existEmpFrOshFlag = true;
			}
		});
		$('div#existEmpFrOsh').dialog('open');
	}
	
	
	$scope.saveExsOsh = function(emp){
		console.log("enter into saveExsOsh function");
		$('div#existEmpFrOsh').dialog('close');
		$scope.temp.branchOutStandingHD = emp.empName;
		$scope.branch.branchOutStandingHD = emp.empCode;
	}
	
	
	$scope.branchMarketingRadio = function(){
		console.log("enter into branchMarketingRadio function");
		$scope.newEmpFrBmkFlag = false;
		$('div#newEmpFrBmk').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select New Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.newEmpFrBmkFlag = true;
			}
		});
		$('div#newEmpFrBmk').dialog('open');
	}
	
	
	$scope.saveNewBmk = function(emp){
		console.log("enter into saveNewBmk function");
		$('div#newEmpFrBmk').dialog('close');
		$scope.temp.branchMarketing = emp.empName;
		$scope.branch.branchMarketing = emp.empCode;
	}
	
	
	$scope.openBranchMarketingDB = function(){
		console.log("enter into openBranchMarketingDB function");
		$scope.existEmpFrBmkFlag = false;
		$('div#existEmpFrBmk').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select Existing Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.existEmpFrBmkFlag = true;
			}
		});
		$('div#existEmpFrBmk').dialog('open');
	}
	
	
	$scope.saveExsBmk = function(emp){
		console.log("enter into saveExsBmk function");
		$('div#existEmpFrBmk').dialog('close');
		$scope.temp.branchMarketing = emp.empName;
		$scope.branch.branchMarketing = emp.empCode;
	}
	
	
	$scope.branchMngrRadio = function(){
		console.log("enter into branchMngrRadio function");
		$scope.newEmpFrBmgFlag = false;
		$('div#newEmpFrBmg').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select New Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.newEmpFrBmgFlag = true;
			}
		});
		$('div#newEmpFrBmg').dialog('open');
	}
	
	
	$scope.saveNewBmg = function(emp){
		console.log("enter into saveNewBmg function");
		$('div#newEmpFrBmg').dialog('close');
		$scope.temp.branchMngr = emp.empName;
		$scope.branch.branchMngr = emp.empCode;
	}
	
	
	$scope.openBranchMngrDB = function(){
		console.log("enter into openBranchMngrDB function");
		$scope.existEmpFrBmgFlag = false;
		$('div#existEmpFrBmg').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select Existing Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.existEmpFrBmgFlag = true;
			}
		});
		$('div#existEmpFrBmg').dialog('open');
	}
	
	
	$scope.saveExsBmg = function(emp){
		console.log("enter into saveExsBmg function");
		$('div#existEmpFrBmg').dialog('close');
		$scope.temp.branchMngr = emp.empName;
		$scope.branch.branchMngr = emp.empCode;
	}
	
	
	$scope.branchCashierRadio = function(){
		console.log("enter into branchCashierRadio function");
		$scope.newEmpFrCshFlag = false;
		$('div#newEmpFrCsh').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select New Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.newEmpFrCshFlag = true;
			}
		});
		$('div#newEmpFrCsh').dialog('open');
	}
	
	
	$scope.saveNewCsh = function(emp){
		console.log("enter into saveNewCsh funciton");
		$('div#newEmpFrCsh').dialog('close');
		$scope.temp.branchCashier = emp.empName;
		$scope.branch.branchCashier = emp.empCode;
	}
	
	
	$scope.openBranchCashierDB = function(){
		console.log("enter into openBranchCashierDB function");
		$scope.existEmpFrCshFlag = false;
		$('div#existEmpFrCsh').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select Existing Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.existEmpFrCshFlag = true;
			}
		});
		$('div#existEmpFrCsh').dialog('open');
	}
	
	
	$scope.saveExsCsh = function(emp){
		console.log("enter into saveExsCsh function");
		$('div#existEmpFrCsh').dialog('close');
		$scope.temp.branchCashier = emp.empName;
		$scope.branch.branchCashier = emp.empCode;
	}
	
	
	$scope.branchTrafficRadio = function(){
		console.log("enter into branchTrafficRadio function");
		$scope.newEmpFrTrfFlag = false;
		$('div#newEmpFrTrf').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select New Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.newEmpFrTrfFlag = true;
			}
		});
		$('div#newEmpFrTrf').dialog('open');
	}
	
	
	$scope.saveNewTrf = function(emp){
		console.log("enter into saveNewTrf funciton");
		$('div#newEmpFrTrf').dialog('close');
		$scope.temp.branchTraffic = emp.empName;
		$scope.branch.branchTraffic = emp.empCode;
	}
	
	$scope.openBranchTrafficDB = function(){
		console.log("enter into openBranchTrafficDB function");
		$scope.existEmpFrTrfFlag = false;
		$('div#existEmpFrTrf').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select Existing Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.existEmpFrTrfFlag = true;
			}
		});
		$('div#existEmpFrTrf').dialog('open');
	}
	
	
	$scope.saveExsTrf = function(emp){
		console.log("enter into saveExsTrf function");
		$('div#existEmpFrTrf').dialog('close');
		$scope.temp.branchTraffic = emp.empName;
		$scope.branch.branchTraffic = emp.empCode;
	}
	
	$scope.branchAreaMngrRadio = function(){
		console.log("enter into branchAreaMngrRadio function");
		$scope.newEmpFrAmgFlag = false;
		$('div#newEmpFrAmg').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select New Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.newEmpFrAmgFlag = true;
			}
		});
		$('div#newEmpFrAmg').dialog('open');
	}
	
	$scope.saveNewAmg = function(emp){
		console.log("enter into saveNewAmg funciton");
		$('div#newEmpFrAmg').dialog('close');
		$scope.temp.branchAreaMngr = emp.empName;
		$scope.branch.branchAreaMngr = emp.empCode;
	}
	
	$scope.openBranchAreaMngrDB = function(){
		console.log("enter into openBranchAreaMngrDB function");
		$scope.existEmpFrAmgFlag = false;
		$('div#existEmpFrAmg').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select Existing Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.existEmpFrAmgFlag = true;
			}
		});
		$('div#existEmpFrAmg').dialog('open');
	}
	
	$scope.saveExsAmg = function(emp){
		console.log("enter into saveExsAmg function");
		$('div#existEmpFrAmg').dialog('close');
		$scope.temp.branchAreaMngr = emp.empName;
		$scope.branch.branchAreaMngr = emp.empCode;
	}
	
	$scope.branchRegionalMngrRadio = function(){
		console.log("enter into branchRegionalMngrRadio function");
		$scope.newEmpFrRmgFlag = false;
		$('div#newEmpFrRmg').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select New Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.newEmpFrRmgFlag = true;
			}
		});
		$('div#newEmpFrRmg').dialog('open');
	}
	
	
	$scope.saveNewRmg = function(emp){
		console.log("enter into saveNewRmg funciton");
		$('div#newEmpFrRmg').dialog('close');
		$scope.temp.branchRegionalMngr = emp.empName;
		$scope.branch.branchRegionalMngr = emp.empCode;
	}
	
	
	$scope.openBranchRegionalMngrDB = function(){
		console.log("enter into openBranchRegionalMngrDB function");
		$scope.existEmpFrRmgFlag = false;
		$('div#existEmpFrRmg').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select Existing Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.existEmpFrRmgFlag = true;
			}
		});
		$('div#existEmpFrRmg').dialog('open');
	}
	
	$scope.saveExsRmg = function(emp){
		console.log("enter into saveExsRmg funciton");
		$('div#existEmpFrRmg').dialog('close');
		$scope.temp.branchRegionalMngr = emp.empName;
		$scope.branch.branchRegionalMngr = emp.empCode;
	}
	
	
	$scope.SubmitBranch = function(brhWiseForm){
		console.log("enter into SubmitBranch function = "+brhWiseForm.$invalid);
		if(brhWiseForm.$invalid){
			$scope.alertToast("please fill correct form")
		}else{
			 $scope.viewBranchDetailsFlag = false;
				$('div#viewBranchDetailsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "View Branch Details",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
						$scope.viewBranchDetailsFlag = true;
					}
				});
				$('div#viewBranchDetailsDB').dialog('open');
		}
	}
	
	
	$scope.closeViewBranchDetailsDB = function(){
		console.log("enter into closeViewBranchDetailsDB function");
		$('div#viewBranchDetailsDB').dialog('close');
	}
	
	
	$scope.saveBrWise = function(branch){
		console.log("enter into saveBranch funciton");
		 $('#saveId').attr("disabled","disabled");
		var response = $http.post($scope.projectName+'/submitBrWise',branch);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$('div#viewBranchDetailsDB').dialog('close');
				$('#saveId').removeAttr("disabled");
				$scope.successToast(data.result);
				$scope.branch = {};
				$scope.temp = {};
				$scope.getBranch();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		});	
	}
	
	
	
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getBranch();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
}]);
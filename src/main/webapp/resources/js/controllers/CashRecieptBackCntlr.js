'use strict';

var app = angular.module('application');

app.controller('CashRecieptBackCntlr',['$scope','$location','$http','$filter','$window',
                                 function($scope,$location,$http,$filter,$window){
	
	$scope.vs = {};
	$scope.faMList = [];
	$scope.faCodeFlag = true;
	$scope.saveVsFlag = true;
	$scope.printVsFlag = true;
	
	$scope.loadingFlag = false;
	
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		$scope.saveVsFlag = true;
		var response = $http.post($scope.projectName+'/getCashRecDetailBack');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.vs.voucherType = $scope.CASH_RECIEPT;
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.saveFaCode = function(faM){
		$scope.faCodeFlag = true;
		$('div#faCodeDB').dialog('close');
		console.log("enter into saveFaCode function");
		$scope.vs.faCode = faM.faMfaCode;
		$scope.vs.faName = faM.faMfaName;
	}
	
	
	$scope.openFaCodeDB = function(keyCode){
		
		if(keyCode === 8)
			return;	
		var len = $scope.vs.faCode.length;
		if(len < 3)
			return ;
		
		var faCode = $scope.vs.faCode;
		$scope.vs.faCode = "";
		
		$scope.getAllFaCode(faCode);
		
		console.log("enter into openFaCodeDB function");
		$scope.faCodeFlag = false;
		$('div#faCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#faCodeDB').dialog('open');
	}
	
	$scope.getAllFaCode = function(faName){
		console.log("enter into getAllFaCode function : "+faName);
		$scope.loadingFlag = true;
		var response = $http.post($scope.projectName+'/getAllFaCodeFrCPVByFaName', faName);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.loadingFlag = false;
				$scope.faMList = data.faMList;				
			}else{
				$scope.loadingFlag = false;
				console.log("Error in bringing FaCode");
			}
		});
		response.error(function(data, status, headers, config){
			$scope.loadingFlag = false;
			console.log(data);
		});
	}
	
	
	$scope.voucherSubmit = function(newVoucherForm){
		console.log("enter into voucherSubmit function--->"+newVoucherForm.$invalid);
		if(newVoucherForm.$invalid){
			$scope.alertToast("Error in form");
		}else{
			if($scope.vs.amount <= 0){
				$scope.alertToast("please enter a valid amount");
			}else{
				console.log("final submittion");
				$scope.saveVsFlag = false;
		    	$('div#saveVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				    }
					});
				$('div#saveVsDB').dialog('open');
			}
		}
	}
	
	
	$scope.back = function(){
		console.log("enter into back function");
		$scope.saveVsFlag = true;
		$('div#saveVsDB').dialog('close');
	}
	
	
	$scope.saveVS = function(){
		console.log("enter into saveVs function");
		$('div#saveVsDB').dialog('close');
		$('#saveId').attr("disabled","disabled");
		var response = $http.post($scope.projectName+'/submitCashR',$scope.vs);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("success");
				$('#saveId').removeAttr("disabled");
				$scope.alertToast("voucher generated successfully");
				$scope.vs.vhNo = data.vhNo;
				$scope.vs.tvNo = data.tvNo;
				
				
				$scope.printVsFlag = false;
		    	$('div#printVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.printVsFlag = true;
				    }
					});
				$('div#printVsDB').dialog('open');
				
				
				/*$scope.vs = {};
				$scope.cNo = "";
				$scope.getVoucherDetails();*/
			}else{
				console.log("Error in submitVoucher");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	
	$scope.cancelPrint = function(){
		console.log("enter into cancelPrint function");
		$('div#printVsDB').dialog('close');
		$scope.vs = {};
		$scope.cNo = "";
		$scope.getVoucherDetails();
	}
	
	
	$scope.printVs = function(){
		console.log("enter into printVs function");
		$window.print();
		$('div#printVsDB').dialog('close');
		$scope.vs = {};
		$scope.cNo = "";
		$scope.getVoucherDetails();
	}
	
	
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
}]);
'use strict';

var app = angular.module('application');

app.controller('StationCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	$scope.station = {};
	$scope.stateList = {};
	$scope.stateCodeDBFlag = true;
	$scope.viewStationDetailsFlag = true;
	$scope.stnList =[];
	var code;
	$scope.state = {};
	$scope.type=[];
			
	$scope.Submit=function(StationForm,station){
		console.log("Entered into Submit function");
		$scope.station.stnName = station.stnName.toUpperCase();
		$scope.code = station.stnName+station.stnPin;
		
		if(StationForm.$invalid){
			if(StationForm.stnName.$invalid){
				$scope.alertToast("Please enter station name ");
				$scope.station.stnName="";
			}else if(StationForm.districtName.$invalid){
				$scope.alertToast("Please enter station district");
				$scope.station.stnDistrict="";
			}else if(StationForm.stateName.$invalid){
				$scope.alertToast("Please enter state code...");
			}else if(StationForm.pinCode.$invalid){
				$scope.alertToast("Please enter station pin of 6 digits...");
				$scope.station.stnPin="";
			}
		}else{
			if($.inArray($scope.code,$scope.type)!==-1){
				$scope.alertToast("Station already exists...");
			}else{
				$scope.viewStationDetailsFlag = false;
				$('div#viewStationDetailsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "View Station Details",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
				});
				$('div#viewStationDetailsDB').dialog('open');
			}
		}	
	}
	
	$scope.saveStation = function(station){
		console.log("Entered into saveStation function----");
		
		var response = $http.post($scope.projectName+'/submitStation', station);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$('div#viewStationDetailsDB').dialog('close');
				$scope.successToast(data.result);
				$scope.station={};
				$scope.stnName="";
				$scope.cityName="";
				$scope.distName="";
				$scope.stateCode="";
				$scope.pinCode="";
				$scope.stateName = "";
				$('input[name=stateName]').attr('checked',false);
				//$scope.getAllStations();
			}else{
				if(!angular.isUndefined(data.msg)){
					$scope.alertToast(data.msg);
				}
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.closeViewStationDetailsDB = function(){
		$('div#viewStationDetailsDB').dialog('close');
	}
	

	
	 
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		// $scope.getStateForStn();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	

		
		console.log("DataIntegrationCntlr started");

		$scope.stateDBFlag=true;
		$scope.distDBFlag=true;
		$scope.cityDBFlag=true;
		$scope.stnDBFlag=true;
		
		
		$('#pinCodeId').keypress(function(key) {
			if(key.charCode < 48 || key.charCode > 57)
				return false;
		});
		
		$scope.getState=function(){
			console.log("getState()");
			 var response= $http.post($scope.projectName+'/getStateDetails');
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						console.log(data.list);
						$scope.stateList=data.list;
						$scope.OpenStateDB();
					}else{
						$scope.alertToast("state not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching state");
				});
			
		}
		
		
		
		$scope.OpenStateDB = function(){
			$scope.stateDBFlag=false;
			$('div#stateDB').dialog({
				autoOpen: false,
				modal:true,
				title: "State",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#stateDB').dialog('open');
		}

		$scope.saveStateCode = function(state){
			$scope.stateCode = state.stateCode;
			$scope.stateName = state.stateName;
			$scope.stateGST=state.stateGST;
			$('div#stateDB').dialog('close');
			$scope.stateDBFlag=true;
			
			
			 var response= $http.post($scope.projectName+'/getADistByStateCode',state.stateCode);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						console.log(data.list);
						$scope.distList=data.list;
						$scope.OpenDistDB();
					}else{
						$scope.alertToast("District not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching District");
				});
			
			
		}
		
		
		$scope.OpenDistDB = function(){
			$scope.distDBFlag=false;
			$('div#distDB').dialog({
				autoOpen: false,
				modal:true,
				title: "District",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#distDB').dialog('open');
		}
		
		
		
		
		$scope.saveDist = function(dist){
			$scope.distName = dist;
			$('div#distDB').dialog('close');
			$scope.distDBFlag=true;
			var map={
					"dist":$scope.distName,
					"state":$scope.stateCode
			};
			
			 var response= $http.post($scope.projectName+'/getACityByDistName',map);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						console.log(data.list);
						$scope.cityList=data.list;
						$scope.OpenCityDB();
					}else{
						$scope.alertToast("city not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching city");
				});
			
			
		}
		
		
		
		$scope.OpenCityDB = function(){
			$scope.cityDBFlag=false;
			$('div#cityDB').dialog({
				autoOpen: false,
				modal:true,
				title: "City",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#cityDB').dialog('open');
		}
		
		
		$scope.saveCity = function(city){
			$scope.cityName = city;
			$('div#cityDB').dialog('close');
			$scope.cityDBFlag=true;
			var map={
					"city":$scope.cityName,
					"dist":$scope.distName,
					"state":$scope.stateCode
			};
			
			 var response= $http.post($scope.projectName+'/getAStnByCityDistName',map);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						console.log(data.list);
						$scope.stnList=data.list;
						$scope.OpenStnDB();
					}else{
						$scope.alertToast("station not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching station");
				});
			
			
		}
		
		
		$scope.OpenStnDB = function(){
			$scope.stnDBFlag=false;
			$('div#stnDB').dialog({
				autoOpen: false,
				modal:true,
				title: "Station",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#stnDB').dialog('open');
		}
		
		
		
		
		$scope.saveStn = function(stn){
			$scope.stateCode=stn.stateCode;
			$scope.stnName = stn.stationName;
			$scope.stnCode = stn.fstId;
			$scope.distName=stn.district;
			$scope.cityName=stn.city;
			$scope.pinCode=stn.pinCode;
			
			$scope.station.stnName=stn.stationName;
			$scope.station.stnDistrict=stn.district;
			$scope.station.stateCode=stn.stateCode;
			$scope.station.stnPin=stn.pinCode;
			$scope.station.stnCity=stn.city;
			
			$('div#stnDB').dialog('close');
			console.log("Station"+stn.stationName);
			$scope.stnDBFlag=true;
		}
		
		
		
		
		$scope.getStnByPin=function(){
			
			if($scope.station.stnPin.length<6)
				return;
			
			var response= $http.post($scope.projectName+'/getAStnByPin',$scope.station.stnPin);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					console.log(data.list +""+data.stateName);
					$scope.stnList=data.list;
					$scope.stateName=data.stateName;
					$scope.stateGST=data.stateGST;
					console.log("$scope.stateGST"+$scope.stateGST);
					$scope.OpenStnDB();
				}else{
					$scope.alertToast("station not found in database");
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.alertToast("There is some problem in fetching station");
			});
			
			
		}
		

		console.log("DataIntegrationCntlr ended");
		

	
}]);
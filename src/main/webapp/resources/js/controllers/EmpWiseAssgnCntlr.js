'use strict';

var app = angular.module('application');

app.controller('EmpWiseAssgnCntlr',['$scope','$location','$http',
                                 function($scope,$location,$http){
	
	$scope.branchList = [];
	$scope.newEmpList = [];
	$scope.existEmpList = [];
	
	$scope.newEmp = {};
	$scope.existEmp = {};
	$scope.branch = {};
	
	$scope.newEmpDBFlag = true;
	$scope.existEmpDBFlag = true;
	$scope.openBrDBFlag = true;
	$scope.saveFlag = true;
	
	
	$scope.getBranch = function(){
		console.log("enter into getBranch function");
		
		var response = $http.post($scope.projectName+'/getBrFrEWAssgn');
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.branchList = data.list;
				$scope.getExistEmp();
			}else{
				console.log("error in bringing branch list");
			}
	    });
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	
	$scope.newEmp = function(){
		console.log("enter into newEmp function");
		$('#newEmpId').removeAttr("disabled");
		$('#existEmpId').attr("disabled","disabled"); 
	}
	
	$scope.existEmp = function(){
		console.log("enter into existEmp function");
		$('#existEmpId').removeAttr("disabled");
		$('#newEmpId').attr("disabled","disabled"); 
	}
	
	
	$scope.getNewEmp = function(){
		console.log("enter into getNewEmp funciton");
		var response = $http.post($scope.projectName+'/getNewEmp');
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.newEmpList = data.list;
			}else{
				$scope.alertToast("New Employees are not existing");				
			}
	    });
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});	
	}
	
	
	$scope.openNewEmpDB = function(){
		console.log("enter into openNewEmpDB function");
		
				$scope.newEmpDBFlag = false;
				$('div#newEmpDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Select New Employee",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
						$scope.newEmpDBFlag = true;
					}
				});
				$('div#newEmpDB').dialog('open');
			
	}
	
	
	$scope.saveNewEmp = function(emp){
		console.log("enter into saveNewEmp function");
		$scope.newEmp = emp;
		$scope.existEmp = {};
		$('div#newEmpDB').dialog('close');
		$('#brId').removeAttr("disabled");
	}
	
	
	$scope.getExistEmp = function(){
		console.log("enter into getExistEmp function");
		var response = $http.post($scope.projectName+'/getExistEmp');
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.existEmpList = data.list;
				$scope.getNewEmp();
			}else{
				console.log("error in brining existing employee");				
			}	
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	
	$scope.openExistEmpDB = function(){
		console.log("enter into openExistEmpDB function");
		
				$scope.existEmpDBFlag = false;
				$('div#existEmpDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Select Existing Employee",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
						$scope.existEmpDBFlag = true;
					}
				});
				$('div#existEmpDB').dialog('open');
			
	    
	}
	
	
	$scope.saveExistEmp = function(emp){
		console.log("enter into saveExistEmp function");
		$scope.existEmp = emp;
		$scope.newEmp = {};
		$('div#existEmpDB').dialog('close');
		$('#brId').removeAttr("disabled");
	}
	
	
	$scope.openBranchDB = function(){
		console.log("enter into openBranchDB function");
		$scope.openBrDBFlag = false;
		$('div#openBrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Select Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.openBrDBFlag = true;
			}
		});
		$('div#openBrDB').dialog('open');
	}
	
	
	$scope.saveBranch = function(br){
		console.log("enter into saveBranch function");
		$scope.branch = br;
		$('div#openBrDB').dialog('close');
	}
	
	
	$scope.submitForm = function(empWiseForm){
		console.log("enter into submitForm function ===> "+empWiseForm.$invalid);
		if(empWiseForm.$invalid){
			$alertToast("please fill correct form");
		}else{
			$scope.saveFlag = false;
	    	$('div#saveDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				title: "Assigned Details",
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.saveFlag = true;
			    }
				});
	    	$('div#saveDB').dialog('open');
		}
	}
	
	
	$scope.back = function(){
		console.log("enter into back function");
		$('div#saveDB').dialog('close');
	}
	
	$scope.save = function(){
		console.log("enter into save function");
		
		var empId = 0;
		if(!angular.isUndefined($scope.newEmp.empName) && angular.isUndefined($scope.existEmp.empName)){
			empId = $scope.newEmp.empId;
		}else if(!angular.isUndefined($scope.existEmp.empName) && angular.isUndefined($scope.newEmp.empName)){
			console.log("$scope.existEmp.empId = "+$scope.existEmp.empId);
			empId = $scope.existEmp.empId;
		}else{ 
			empId = 0;
		}
		
		console.log("empId = "+empId);
		var detail = {
				"empId" : empId,
				"brId"  : $scope.branch.branchId
		};
		 $('#saveId').attr("disabled","disabled");
		var response = $http.post($scope.projectName+'/submitEmpWise',detail);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$('div#saveDB').dialog('close');
				$scope.newEmp = {};
				$scope.existEmp = {};
				$scope.branch = {};
				$('#saveId').removeAttr("disabled");
				$scope.branchList = [];
				$scope.newEmpList = [];
				$scope.existEmpList = [];
				
				$scope.getBranch();
			}else{
				console.log("error in submitting details");
			}
	    });
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getBranch();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
}]);
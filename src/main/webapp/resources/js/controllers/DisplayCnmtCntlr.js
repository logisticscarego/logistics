'use strict';

var app = angular.module('application');

app.controller('DisplayCnmtCntlr',['$scope','$location','$http','$filter','$window',
                                   function($scope,$location,$http,$filter,$window){

	$scope.biltyCodeFlag = true;
	$scope.availableTags = [];
	$scope.isViewNo=false;
	$scope.showCnmt = true;
	/*$scope.divForAutoCode = true;*/
	$scope.selection=[];
	$scope.biltyByModal=false;
	$scope.biltyByAuto=false;

	$scope.OpenBiltyCodeDB = function(){
		$scope.biltyCodeFlag = false;
		$('div#biltyCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#biltyCodeDB').dialog('open');
	}

	$scope.enableModalTextBox = function(){
		$('#biltyCodeByMId').removeAttr("disabled");
		$('#biltyCodeByAId').attr("disabled","disabled");
		$('#ok').attr("disabled","disabled");
		$scope.biltyCodeByA="";
	}

	$scope.enableAutoTextBox = function(){
		$('#biltyCodeByAId').removeAttr("disabled");
		$('#ok').removeAttr("disabled");
		$('#biltyCodeByMId').attr("disabled","disabled");
		$scope.biltyCodeByM="";
	}

	$scope.getBiltyIsViewNo = function(){ 
		var response = $http.post($scope.projectName+'/getBiltyIsViewNo');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.successToast(data.result);
				$scope.BiltyList = data.list;
				$scope.isViewNo=false;
				for(var i=0;i<$scope.BiltyList.length;i++){
					$scope.BiltyList[i].creationTS =  $filter('date')($scope.BiltyList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
				}
			}else{
				$scope.isViewNo=true;
				$('#bkToList').attr("disabled","disabled");
				$scope.alertToast(data.result);
			}

			$scope.getAllBiltyCodes();

		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}

	$scope.getBiltyCodeList=function(){
		$( "#biltyCodeByAId" ).autocomplete({
			source: $scope.availableTags
		});
	}

	$scope.getAllBiltyCodes = function(){
		var response = $http.get($scope.projectName+'/getAllBiltyCodes');
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.cnmtCodeList = data.cnmtCodeList;
				$scope.availableTags = data.cnmtCodeList;
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.saveBiltyCode = function(biltyCodes){
		$scope.biltyCodeByM=biltyCodes;
		console.log("biltyCodeByM--------"+$scope.biltyCodeByM);
		$('div#biltyCodeDB').dialog('close');
		$scope.showCnmt = false;
//		$scope.divForAutoCode = true;
		$scope.isViewNo=true;
		var response = $http.post($scope.projectName+'/biltyDetails',$scope.biltyCodeByM);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.cnmt = data.cnmt;
				$scope.alertToast(data.result);
				if(data.image === "yes"){
					$('#submitImage').removeAttr("disabled");
				}else{
					$('#submitImage').attr("disabled","disabled");
				}
			}


		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}

	$scope.getBiltyList = function(){
		$scope.code = $( "#biltyCodeByAId" ).val();

		if($scope.code===""){
			$scope.alertToast("Please enter code");	
		}else{
			$scope.saveBiltyCode = false;
			$scope.isViewNo=true;
			var response = $http.post($scope.projectName+'/biltyDetails',$scope.code);
			response.success(function(data, status, headers, config) {
				$scope.cnmt = data.cnmt;
				$scope.showCnmt = false;
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}	
	}

	$scope.backToList = function(){
		/*$scope.divForModalCode = true;*/
		$scope.saveBiltyCode = true;
		$scope.isViewNo=false;
		$scope.biltyCodeByM="";
		$scope.biltyCodeByA="";
		$('#biltyCodeByAId').attr("disabled","disabled");
		$('#biltyCodeByMId').attr("disabled","disabled");
		$scope.showCnmt=true;
	}

	$scope.toggleSelection = function toggleSelection(cnmtCode) {
		var idx = $scope.selection.indexOf(cnmtCode);

		if (idx > -1) {
			$scope.selection.splice(idx, 1);
		}
		else {
			$scope.selection.push(cnmtCode);
		}
	}	
	$scope.verifyBilty = function(){
		if(!$scope.selection.length){
			$scope.alertToast("You have not selected anything");
		}else{
			var response = $http.post($scope.projectName+'/updateIsViewBilty',$scope.selection.toString());
			response.success(function(data, status, headers, config){
				$scope.getBiltyIsViewNo();
				console.log("---------->message==>"+data.result);
				$scope.successToast(data.result);
			});	 
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			}); 
		}
	}

	$scope.EditBiltySubmit = function(cnmt){
		var response = $http.post($scope.projectName+'/EditBiltySubmit',cnmt);
		response.success(function(data, status, headers, config) {
			console.log(data.result);
			$scope.cnmt="";
			$scope.showCnmt = true;
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}

	/*	$scope.downloadCnmt = function(){
		console.log("enter into downloadCnmt funtion");
		var code = "1000003";
		var response = $http.post($scope.projectName+'/downloadCnmt',{responseType: 'arraybuffer'},code);
		response.success(function(data, status, headers, config) {
			//console.log(data);
			console.log(data.result);
			$scope.imgSrc = data;
			console.log("image code--->"+response);
			 var file = new Blob([data], {type: 'application/pdf'});
		     var fileURL = URL.createObjectURL(file);
		     $scope.content = $sce.trustAsResourceUrl(fileURL);
			var myBlob = new Blob([data],{type:'image/jpg'})
			var blobURL = (window.URL || window.webkitURL).createObjectURL(myBlob);
			var anchor = document.createElement("a");
			anchor.download = "myfile.txt";
			anchor.href = blobURL;
			anchor.click();
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
		$http({
		    url: $scope.projectName+'/downloadCnmt',
		    method: "POST",
		    data: json, //this is your json data string
		    headers: {
		       'Content-type': 'application/json'
		    },
		    responseType: 'arraybuffer'
		}).success(function (data, status, headers, config) {
			$scope.successToast(data.result);
		    var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
		    var objectUrl = URL.createObjectURL(blob);
		    window.open(objectUrl);
		}).error(function (data, status, headers, config) {
		    //upload failed
			$scope.errorToast(data.result);
		});

	}*/
	
	 if($scope.adminLogin === true || $scope.superAdminLogin === true){
		 $scope.getBiltyIsViewNo();
	}else if($scope.logoutStatus === true){
			 $location.path("/");
	}else{
			 console.log("****************");
	}

	

}]);

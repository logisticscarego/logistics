'use strict';

var app = angular.module('application');

app.controller('ImageReaderCntlr',['$scope','$location','$http','AuthorisationService',
                                 function($scope,$location,$http,AuthorisationService){

	console.log("ImageReaderCntlr Started");
	
	$scope.ownBrkFlag = true;
	$scope.cnmtFlag = false;
	$scope.ownBrkDBFlag = true;
	$scope.bilFlag = true;
	$scope.bfNoFlag=true;
	
	$scope.image = {};
	$scope.ownBrkList = [];
	
	$scope.changeFlag = function(){
		if($("#imageType").val() === "cnmt"){
			$scope.cnmtFlag = false;
			$scope.ownBrkFlag = true;
			$scope.bilFlag = true;
			$scope.bfNoFlag=true;
		}else if($("#imageType").val() === "bil"){
			$scope.ownBrkFlag = true;
			$scope.cnmtFlag = true;
			$scope.ownBrkDBFlag = true;
			$scope.bfNoFlag=true;
			$scope.bilFlag = false;
		}else if($("#imageType").val() === "bf"){
			$scope.ownBrkFlag = true;
			$scope.cnmtFlag = true;
			$scope.ownBrkDBFlag = true;
			$scope.bfNoFlag=false;
			$scope.bilFlag = true;
		}else{
			$scope.bfNoFlag=true;
			$scope.cnmtFlag = true;
			$scope.ownBrkFlag = false;
			$scope.bilFlag = true;
		}
	}
	
	$scope.getList = function(keyCode){				
		if(keyCode === 8)
			return;
		
		if($scope.image.imageType === "chln" || $scope.image.imageType === "lry")
			return;	
		
		if(parseInt($scope.image.ownBrkNC.length) < 2){
			return;
		}else{
			$scope.getOwnBrkList();
		}		
	}
	
	$scope.getOwnBrkList = function(){
		var response = $http.post($scope.projectName + '/getOwnBrkForImgRead', $scope.image);
		response.success(function(data, status, headers, config){
			console.log(data.result);
			$scope.alertToast(data.result);
			if(data.result === "success"){
				$scope.ownBrkList = data.list;
				$scope.image.ownBrkNC = "";
				$scope.ownBrkDBFlag = false;
				$('div#ownBrkDB').dialog({
					autoOpen: false,
					modal:true,
					title: $scope.image.imageType,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					resizable: false,
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy') ;
						$(this).hide();
					}
				});
				$('div#ownBrkDB').dialog('open');
			}else{
				$scope.ownBrkList = [];
				$scope.alertToast(data.msg);
			}
		});
		response.error(function(data, status, headers, config){
			console.log("Error in hitting URL");
		});

	}
	
	$scope.saveOwnBrk = function(obj){
		$scope.image.id = obj.id;
		$scope.image.ownBrkNC = obj.name;
		$('div#ownBrkDB').dialog('close');
	}
	
	$scope.readImage = function(ImageForm){
		console.log("Enter into readImage() : FormValid = "+ImageForm.$valid);
		if(ImageForm.$invalid){
			if(ImageForm.imageType.$invalid)
				$scope.alertToast("Select image type");
		}else{
			var response = $http.post($scope.projectName+'/isDownloadImg', $scope.image);
			response.success(function(data, status, headers, config){
				console.log("Result = "+data.result);
				if(data.result === "success"){
					$scope.alertToast(data.result);
					$("#imageForm").attr("action", "downloadImg");
					$("#imageForm").trigger("submit");
				}else
					$scope.alertToast(data.msg);
			});
			response.error(function(data){
				console.log("Error in hitting /downloadImg");
			});
		}		
		console.log("Exit from readImage()");
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){		
	}else if($scope.logoutStatus === true){	
		$location.path("/");
	}else{	
		console.log("****************");
	}	
	console.log("ImageReaderCntlr Ended");
	
}]);
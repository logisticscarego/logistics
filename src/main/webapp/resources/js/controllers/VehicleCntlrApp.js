'use strict';

var app = angular.module('application');

app.controller('VehicleCntlrApp',['$scope','$location','$http','$filter','$window',
                                 function($scope,$location,$http,$filter,$window){

	console.log("NewVehicleVendorCntlr Started");
	
	$scope.ownerList = [];
	$scope.brokerList = [];
	$scope.vtList = [];
	$scope.stateList = [];
	$scope.vehicleApp = {};
	$scope.rcNoList = [];
	$scope.ownCode = "";
	$scope.brkCode = "";
	
	$scope.owner = {};
	$scope.broker = {};
	$scope.vehVenMstr = {};
	
	$scope.vehVenMstrFormFlag = false;
	
	$scope.ownerDBFlag = true;
	$scope.ownerBrokerFlag = false;
	$scope.vehicleDBFlag = true;
	$scope.brokerDBFlag = true;
	$scope.vehTypeDBFlag = true;
	$scope.stateDBFlag = true;
	$scope.fitStateDBFlag = true;
	$scope.vehVenMstrDBFlag = true;
	
	$scope.getOwnNameCodeId = function(){
		console.log("getOwnNameCodeId Entered");
		var response = $http.post($scope.projectName+'/getOwnNameCodeId');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getOwnNameCodeId Success");
				$scope.ownerList = data.ownerList;
				
			/*	for ( var i = 0; i < $scope.ownerList.length; i++) {
					console.log("ownName: "+$scope.ownerList[i].ownName);
					console.log("ownId: "+$scope.ownerList[i].ownId);
					console.log("ownCode: "+$scope.ownerList[i].ownCode);
					console.log("ownPanNo: "+$scope.ownerList[i].ownPanNo);
				}*/
				
				$scope.getBrkNameCodeId();
			}else {
				$scope.alertToast("you have no owner");
				console.log("getOwnNameCodeId Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getOwnNameCodeId Error: "+data);
		});
	}
	
	$scope.openOwnerDB = function(){
		console.log("Entered into openOwnerDB");
		$scope.ownerDBFlag = false;
		$('div#ownerDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Owner",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.ownerDBFlag = true;
			}
		});

		$('div#ownerDB').dialog('open');
	}
	
	$scope.openVehicleDB = function(){
		console.log("Enter into openVehicleDB()...");
		$scope.vehicleDBFlag = false;
		$('div#vehicleDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Vehicle",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.ownerDBFlag = true;
			}
		});

		$('div#vehicleDB').dialog('open');
	}
	
	$scope.saveOwner = function(owner){
		$scope.owner = owner;
	}
	
	$scope.closeOwnDb =  function(){
		console.log("OwnerName----->"+$scope.owner.ownName);
		$('div#ownerDB').dialog('close');
	}
	
	$scope.getBrkNameCodeId = function(){
		console.log("getBrkNameCodeId Entered");
		var response = $http.post($scope.projectName+'/getBrkNameCodeId');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getBrkNameCodeId Success");
				$scope.brokerList = data.brokerList;
				
			}else {
				$scope.alertToast("you have no broker");
				console.log("getBrkNameCodeId Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBrkNameCodeId Error: "+data);
		});
	}
	
	$scope.openBrokerDB = function(){
		console.log("Entered into openBrokerDB");
		$scope.brokerDBFlag = false;
		$('div#brokerDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Broker",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.brokerDBFlag = true;
			}
		});

		$('div#brokerDB').dialog('open');
	}
	
	$scope.saveBroker = function(broker){
		$scope.broker = broker;
	}
	
	$scope.closeBrkDb =  function(){
		console.log("brokerName----->"+$scope.broker.brkName);
		$('div#brokerDB').dialog('close');
	}
	
	$scope.vehVenValid = function(vehVenValidForm){
		console.log("Enter into vehVenValid()...");
		if(vehVenValidForm.$invalid){
			if (vehVenValidForm.rcNoNm.$invalid) {
				$scope.alertToast("Please Enter Rc No.");
			} 
		}else{			
			var vehVendorService = {
					"vvRcNo"	:	$scope.vehVenMstr.vvRcNo
			}			
			var response = $http.post($scope.projectName+'/getPendingVehDt', vehVendorService);
			response.success(function(data, status, headers, config){				
				if (data.result === "success") {					
					$scope.alertToast("You can verify Vehicle mstr");
					$scope.vehVenMstrFormFlag = true;					
					if ($scope.vtList.length < 1) {
						$scope.getVehicleType();						
					}
					$scope.ownerBrokerFlag = true;
					$scope.vehicleApp = data.vehicle;
					$scope.owner.ownName = $scope.vehicleApp.vOwnName;
					$scope.broker.brkName = $scope.vehicleApp.vBrkName;
					// Set owner and broker code
					$scope.ownCode = $scope.vehicleApp.vOwnCode;
					$scope.brkCode = $scope.vehicleApp.vBrkCode;
					//  set vehicle detail
					$scope.vehVenMstr.vvType = $scope.vehicleApp.vTypeCode;
					$scope.vehVenMstr.vvRcIssueDt = $scope.vehicleApp.vRcIssueDt;
					$scope.vehVenMstr.vvRcValidDt = $scope.vehicleApp.vRcValidDt;
					$scope.vehVenMstr.vvDriverName = $scope.vehicleApp.vDriverName;
					$scope.vehVenMstr.vvDriverDLNo = $scope.vehicleApp.vDriverDlNo;					
					$scope.vehVenMstr.vvDriverDLIssueDt= $scope.vehicleApp.vDriverDlIssueDt;
					$scope.vehVenMstr.vvDriverDLValidDt = $scope.vehicleApp.vDriverDlValidDt;
					$scope.vehVenMstr.vvDriverMobNo = $scope.vehicleApp.vDriverMobNo;
					$scope.vehVenMstr.vvPerNo = $scope.vehicleApp.vPermitNo;
					$scope.vehVenMstr.vvPerIssueDt = $scope.vehicleApp.vPermitIssueDt;					
					$scope.vehVenMstr.vvPerValidDt = $scope.vehicleApp.vPermitValidDt;				
					$scope.vehVenMstr.vvPerState = $scope.vehicleApp.vPermitStateName;					
					$scope.vehVenMstr.vvFitNo = $scope.vehicleApp.vFitDocNo;
					$scope.vehVenMstr.vvFitIssueDt = $scope.vehicleApp.vFitDocIssueDt;
					$scope.vehVenMstr.vvFitValidDt = $scope.vehicleApp.vFitDocValidDt;
					$scope.vehVenMstr.vvFitState = $scope.vehicleApp.vFitDocStateName;					
					$scope.vehVenMstr.vvFinanceBankName = $scope.vehicleApp.vFinanceBankName;
					$scope.vehVenMstr.vvPolicyNo = $scope.vehicleApp.vPolicyNo;
					$scope.vehVenMstr.vvPolicyComp = $scope.vehicleApp.vPolicyCompany;
					$scope.vehVenMstr.vvTransitPassNo = $scope.vehicleApp.vTransitPassNo;
					
				}else {					
					$scope.alertToast(data.msg);
					$scope.ownerBrokerFlag = true;
					$scope.vehicleApp = data.vehicle;
					$scope.owner.ownName = $scope.vehicleApp.vOwnName;
					$scope.broker.brkName = $scope.vehicleApp.vBrkName;
				}
			});
			response.error(function(data, status, headers, config){
				console.log("vehVenValid Error: "+data);
			});
		}
	}
	
	$scope.openVehicleTypeDB = function(){
		$scope.vehTypeDBFlag = false;
    	$('div#vehTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Vehicle Type",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.vehTypeDBFlag = true;
		    }
			});
		
		$('div#vehTypeDB').dialog('open');
		}
	
	$scope.saveVehicleType = function(vt){
		$scope.vehVenMstr.vvType = vt.vtCode;
		$('div#vehTypeDB').dialog('close');
		$scope.vehicleDBFlag = true;
	}
	
	$scope.getVehicleType = function(){
		console.log("getVehicleType()");
		var response = $http.post($scope.projectName+'/getVehicleTypeCodeForOwner');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.vtList = data.list;
				
				if ($scope.stateList.length < 1) {
					$scope.getStateList();
					console.log("getStateList() called: ")
				}else{
					console.log("getStateList() not called: ")
				}
				
				
			}else{
				console.log(data);   
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	$scope.getStateList = function() {
		console.log("getStateList()");
		var response = $http.post($scope.projectName+'/getStateList');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.stateList = data.stateList;
				/*for ( var i = 0; i < $scope.stateList.length; i++) {
					console.log($scope.stateList[i]);
				}*/
			}else{
				console.log(data);   
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	$scope.openStateDB = function(){
		console.log("openStateDB()");
		$scope.stateDBFlag = false;
    	$('div#stateDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "States",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.stateDBFlag = true;
		    }
			});
		
		$('div#stateDB').dialog('open');
		}
	
	$scope.saveState = function(state){
		console.log("saveState()");
		$scope.vehVenMstr.vvPerState = state;
		$('div#stateDB').dialog('close');
		
	}
	
	$scope.openFitStateDB = function(){
		console.log("openFitStateDB()");
		$scope.fitStateDBFlag = false;
    	$('div#fitStateDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "States",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.fitStateDBFlag = true;
		    }
			});
		
		$('div#fitStateDB').dialog('open');
		}
	
	$scope.saveFitState = function(state){
		console.log("saveFitState()");
		$scope.vehVenMstr.vvFitState = state;
		$('div#fitStateDB').dialog('close');
		
	}
	
	
	$scope.vehVenMstrSubmit = function(vehVenMstrForm){
		if(vehVenMstrForm.$invalid){
			if (vehVenMstrForm.vvDriverName.$invalid) {
				$scope.alertToast("Please Enter Driver Name");
			} else if (vehVenMstrForm.vvDriverDLNoName.$invalid) {
				$scope.alertToast("Please Enter Valid DL No");
			} else if (vehVenMstrForm.vvDriverMobNoName.$invalid) {
				$scope.alertToast("Please Enter Valid Mobile No");
			}
		}else{
			
			console.log("final submittion");
			$scope.vehVenMstrDBFlag = false;
			$('div#vehVenMstrDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Vehicle Detail",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
					$scope.vehVenMstrDBFlag = true;
				}
			});
			$('div#vehVenMstrDB').dialog('open');
			
		}
	};
	
	$scope.back = function(){
		console.log("enter into back function");
		$('div#vehVenMstrDB').dialog('close');
	}
	
	
	$scope.saveVehVenMstr = function(){
		$('div#vehVenMstrDB').dialog('close');
		console.log("saveVehVenMstr()");
		
		var vehVendorService = {
				"ownCode"			:	$scope.ownCode,
				"brkCode"			:	$scope.brkCode,
				"vehVenMstr"		:	$scope.vehVenMstr
		}
		
		$('#saveVehVenMstrId').attr("disabled","disabled");
		
		//var response = $http.post($scope.projectName+'/saveVehVenMstr', vehVendorService);
		var response = $http.post($scope.projectName+'/verifyVehVenMstr', vehVendorService);
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				$scope.alertToast(data.msg);				
				$('#saveVehVenMstrId').removeAttr("disabled");			
				//empty the resources			
				$scope.owner = {};
				$scope.ownCode = "";
				$scope.brkCode = "";
				$scope.broker = {};
				$scope.vehVenMstr = {};
				$scope.getPendingVehicle();
				$scope.vehVenMstrFormFlag = false;
				
			}else {
				$scope.alertToast(data.msg);
			}
		});
		response.error(function(data, status, headers, config){
			console.log("saveVehVenMstr Error: "+data);
		});
	}
	
	$('#vvDriverMobNoId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#vvDriverMobNoId').keypress(function(e) {
		if (this.value.length == 10) {
			e.preventDefault();
		}
	});
	
	$('#rcNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvDriverNameId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvDriverDLNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvPerNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvFitNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvFinanceBankId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvPolicyNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvPolicyCompId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvTransitPassNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$scope.saveRcNo = function(rcNo){
		$scope.vehVenMstr.vvRcNo = rcNo;
		$scope.vehicleDBFlag = true;		
		$('div#vehicleDB').dialog('close');
	}
	
	$scope.getPendingVehicle = function(){
		console.log("Enter into getPendingVehicle()...");
		var response = $http.post($scope.projectName+'/getPendingVehicle');
		response.success(function(data, status, headers, config){
			if(data.result == "success"){
				$scope.rcNoList = data.rcNoList;
			}else if(data.result === "error"){
				$scope.alertToast(data.msg);
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
		console.log("Exit from getPendingVehicle()...");
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getOwnNameCodeId();
		$scope.getPendingVehicle();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("NewVehicleVendorCntlr Ended");
}]);
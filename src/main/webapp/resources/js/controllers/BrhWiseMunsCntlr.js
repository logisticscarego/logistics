'use strict';

var app = angular.module('application');

app.controller('BrhWiseMunsCntlr',['$scope','$location','$http','$window',
                                 function($scope,$location,$http,$window){
	
	
	$scope.brhList = [];
	$scope.brhWiseMuns = {};
	
	$scope.branchName = "";
	$scope.active = "";
	$scope.branchCodeDBFlag = true;
	
	$scope.getBrhFrMuns = function(){
		console.log("enter into getBrhFrMuns function");
		var response = $http.post($scope.projectName+'/getBrhFrMuns');
		  response.success(function(data, status, headers, config){
			   if(data.result === "success"){
				   $scope.brhList = data.list;
			   }else{
				   $scope.alertToast("Server Error");
				   console.log("error in fetching data from getBrhFrMuns");
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	$scope.openBranchDB = function(){
		console.log("enter into openBranchDB function");
		$scope.branchCodeDBFlag = false;
		$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			title:"Select Branch",
			show: UDShow,
			hide: UDHide,
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	            $scope.branchCodeDBFlag = true;
	        }
		});
	
		$('div#branchCodeDB').dialog('open');
	}
	
	
	$scope.saveBranch = function(brh){
		console.log("enter into saveBranch function");
		$scope.branchName = brh.branchName;
		$scope.brhWiseMuns.bwmBrhId = brh.brhId; 
		$('div#branchCodeDB').dialog('close');
	}
	
	
	$scope.submitMuns = function(BrhMunsForm){
		console.log("enter into submitMuns function = "+BrhMunsForm.$invalid);
		if(BrhMunsForm.$invalid){
			$scope.alertToast("Please enter correct form");
		}else{
			$('#saveId').attr("disabled","disabled");
			console.log("$scope.active  = "+$scope.active);
			if($scope.active === "true"){
				$scope.brhWiseMuns.bwmActive = true;
			}else{
				$scope.brhWiseMuns.bwmActive = false;
			}
			
			var response = $http.post($scope.projectName+'/submitMuns',$scope.brhWiseMuns);
			  response.success(function(data, status, headers, config){
				   if(data.result === "success"){
					   $('#saveId').removeAttr("disabled");
					   $scope.brhList = [];
					   $scope.brhWiseMuns = {};
					   $scope.branchName = "";
				   }else{
					  $scope.alertToast("Server Error");
				   }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
			   });
			
		}
	}
	
	
	 if($scope.superAdminLogin === true){
		 $scope.getBrhFrMuns();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 

}]);
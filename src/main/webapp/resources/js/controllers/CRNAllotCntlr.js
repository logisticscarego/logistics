'use strict';

var app = angular.module('application');

app.controller('CRNAllotCntlr',['$scope','$location','$http',
                                 function($scope,$location,$http){
	
	$scope.billDays = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
	$scope.branchList = [];
	$scope.empList = [];
	
	$scope.ems = {};
	
	$scope.branchCodeDBFlag = true;
	$scope.saveEmsFlag = true;
	$scope.empCodeDBFlag = true;
	
	$scope.showStaffCode = false;
	
	var max = 15;
	
	$('#advDepAmtId').keypress(function(key) {
        if(key.charCode < 46 || key.charCode > 57)
        	return false;
    });
	
	$('#advDepAmtId').keypress(function(e) {
     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
             e.preventDefault();
         } 
    });
	
	$('#refAmtId').keypress(function(key) {
        if(key.charCode < 46 || key.charCode > 57)
        	return false;
    });
	
	$('#refAmtId').keypress(function(e) {
     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
             e.preventDefault();
         } 
    });
	
	$('#dueDayId').keypress(function(key) {
        if(key.charCode < 46 || key.charCode > 57)
        	return false;
    });
	
	$('#dueDayId').keypress(function(e) {
     	if((this.value.length == 3)){
             e.preventDefault();
         } 
    });
	
	$scope.getBranchList = function(){
		console.log("enter into getBranchList function");
		var response = $http.post($scope.projectName+'/getBrListFrCrnA');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.branchList = data.brList;
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config){
			$scope.alertToast(data);
		});
	}

	
	$scope.openBranchDB = function(){
		console.log("enter into openBranchDB function");
		$scope.branchCodeDBFlag = false;
    	$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Branch List",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#branchCodeDB').dialog('open');
	}
	
	
	$scope.saveBrCode = function(branch){
		console.log("enter into saveBrCode function-->"+branch.branchFaCode);
		$scope.ems.branch = branch;
		$scope.branchCodeDBFlag = true;
		$('div#branchCodeDB').dialog('close');
	}
	
	
	$scope.selectPart = function(){
		console.log("enter into selectPart function");
		if($scope.ems.electricityMstr.emParticular === "Staff"){
			$scope.getEmpList();
			$scope.showStaffCode = true;
		}else{
			$scope.showStaffCode = false;
			$('#empId').attr("disabled","disabled");
			$scope.ems.employee = {};
		}
	}
	
	
	$scope.getEmpList = function(){
		console.log("enter into getEmpList function");
		var response = $http.post($scope.projectName+'/getEmpListFrCrnA');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.empList = data.empList;
				$('#empId').removeAttr("disabled");
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config){
			$scope.alertToast(data);
		});
	}
	
	$scope.openEmpDB = function(){
		console.log("enter into openEmpDB function");
		$scope.empCodeDBFlag = false;
    	$('div#empCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Employee List",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#empCodeDB').dialog('open');
	}
	
	
	$scope.saveEmpCode = function(emp){
		console.log("enter into saveEmpCode function");
		$scope.ems.employee = emp;
		$scope.empCodeDBFlag = true;
		$('div#empCodeDB').dialog('close');
	}
	
	
	$scope.submit = function(newPhoneAllotForm){
		console.log("enter into submit function = "+newPhoneAllotForm.$invalid);
		if(newPhoneAllotForm.$invalid){
			$scope.alertToast("please enter correct value");
		}else{
			console.log("final submittion");
			
			$scope.saveEmsFlag = false;
	    	$('div#saveEmsDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			$('div#saveEmsDB').dialog('open');
		}
	}
	
	
	$scope.back = function(){
		console.log("enter into back function");
		$scope.saveEmsFlag = true;
		$('div#saveEmsDB').dialog('close');
	}
	
	
	$scope.saveEms = function(){
		console.log("enter into saveEms function");
		$scope.saveEmsFlag = true;
		$('div#saveEmsDB').dialog('close');
		
		var response = $http.post($scope.projectName+'/saveEms',$scope.ems);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				if(data.result === "success"){
					$scope.ems = {};
					$scope.alertToast("success");
					$scope.getBranchList();
				}else{
					console.log(data);
				}
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config){
			$scope.alertToast(data);
		});
	}
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getBranchList();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
}]);
'use strict';

var app = angular.module('application');

app.controller('PetroCardCntlr',['$scope','$location','$http','$window','$filter',
                            function($scope,$location,$http,$window,$filter){
	
	console.log("**************  Fund Allocation Cntlr ***************");
	
	$scope.petro={};
	$scope.fromStationDBFlag=true;
	$scope.detailsDBFlag=true;
	$scope.toStationDBFlag=true;
	$scope.showPetroList=true;
	$scope.petroList=[];
	$scope.generateFundData=false;
	   $('#saveId').removeAttr("disabled");
	   
	
	$scope.savePetroAdv=function(petro)
	{
		$scope.petro=petro;
		
	    $scope.detailsDBFlag = false;
    	$('div#detailsDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			title:"LHPV ADVANCE FINAL SUBMISSION",
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.saveVsFlag = true;
		    }
			});
		$('div#detailsDB').dialog('open');
	}
	
	$scope.back=function()
	{
		$('div#detailsDB').dialog('close');
		$scope.detailsDBFlag=true;
	}
	
	$scope.saveFA=function()
	{
		 $('#saveId').attr("disabled","disabled");
		
			    var response=$http.post($scope.projectName+ '/savePetroFund', $scope.petro);
			       response.success(function(data,status,headers,config)
			    		   {
			    	   if(data.result="success"){
			    		   $('#saveId').removeAttr("disabled");
				    	   $scope.alertToast("success");
				    	   $scope.petro={};
				    		$('div#detailsDB').dialog('close');
				    		$scope.detailsDBFlag=true;
				    		    
			    	   }else{
			    		   $scope.alertToast("Please fill valid details and retry");
			    	   }
			    	    });
			       response.error(function(data,status,headers,config)
			    		   {
			    	   $scope.alertToast("Server Error");
			    	   console.log(data.msg)
			    		   });
				
	}
	
	
	
	
	
	
	$scope.getStationFrom = function(keyCode){
		console.log("Enter into getStationData()");
		if(keyCode === 8 || parseInt($scope.fromStCode.length) < 2)
			return;	
		
		var response = $http.post($scope.projectName+'/getStationByNameCode', $scope.fromStCode);
		response.success(function(data, status, headers, config){
			console.log(data);
			if(data.result==="success"){
				$scope.frmStationCode = "";
				$scope.stationList = data.stationList;
				$scope.OpenFromStationDB();
			}else{
				$scope.alertToast("you don't have any station");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("Error in hitting /getStationDataByNameCode");
		});
		console.log("Exit from getStationData()");
	}
	
	$scope.OpenFromStationDB = function(){
		$scope.fromStationDBFlag=false;
		$('div#fromStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "From Station",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#fromStationDB').dialog('open');
	}
	
	
	$scope.saveFrmStnCode = function(station){
		$scope.fromStCode = station.stnName;
		$scope.petro.fromStn=station.stnName;
		$('div#fromStationDB').dialog('close');
		$scope.fromStationDBFlag=true;
	}
	
	
	$scope.getStationTo = function(keyCode){
		console.log("Enter into getStationData()");
		if(keyCode === 8 || parseInt($scope.toStCode.length) < 2)
			return;	
		
		var response = $http.post($scope.projectName+'/getStationByNameCode', $scope.toStCode);
		response.success(function(data, status, headers, config){
			console.log(data);
			if(data.result==="success"){
				$scope.toStCode = "";
				$scope.stationList = data.stationList;
				$scope.OpenToStationDB();
			}else{
				$scope.alertToast("you don't have any station");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("Error in hitting /getStationDataByNameCode");
		});
		console.log("Exit from getStationData()");
	}
	
	$scope.OpenToStationDB = function(){
		$scope.toStationDBFlag=false;
		$('div#toStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "To Station",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#toStationDB').dialog('open');
	}
	
	
	$scope.saveToStnCode = function(station){
		$scope.toStCode = station.stnName;
		$scope.petro.toStn=station.stnName;
		// $scope.toStn=station.stnCode;
		$('div#toStationDB').dialog('close');
		$scope.toStationDBFlag=true;
	}
	
	
	
	$scope.checkCnmtCode = function(cnmtCode){
		
		 console.log("cnmtCode="+cnmtCode);
			var res = $http.post($scope.projectName+'/verifyCnmtFrAdv',cnmtCode);
			res.success(function(data, status, headers, config) {
				if (data.result === "success") {
					console.log(data.cnmtCode);
					$scope.petro.cnmtNo=data.cnmtCode;
					//$scope.alertToast(data.msg);
				} else {
					$scope.alertToast("Please Enter valid CNMT No.");
					$scope.petro.cnmtNo="";
				}
			});
			res.error(function(data, status, headers, config) {
				console.log("error in response verifyCnmtFrAdv: "+data);
			});
	}
	
	$scope.checkChlnCode = function(chlnCode){
		
		 console.log("chlnCode="+chlnCode);
		 
		 if(chlnCode==null || chlnCode=='')
			 return;
		 
		 var res = $http.post($scope.projectName+'/verifyDuplicateChlnFrAdvPetro',chlnCode);
			res.success(function(data, status, headers, config) {
				if (data.result === "success") {
					$scope.petro.challanNo="";
					$scope.alertToast(" This challan already requested for advance");
				}else{
					var res = $http.post($scope.projectName+'/verifyChlnFrAdv',chlnCode);
					res.success(function(data, status, headers, config) {
						if (data.result === "success") {
							
							if(data.chln != null){
								console.log(data.chln.chlnCode);
								$scope.chln=data.chln;
								$scope.petro.challanNo=$scope.chln.chlnCode;
								$scope.toStCode=data.toStn.stnName;
								$scope.petro.toStn=data.toStn.stnName;
								$scope.fromStCode=data.fromStn.stnName;
								$scope.petro.fromStn=data.fromStn.stnName;
								$scope.petro.chlnDt=$scope.chln.chlnDt;
								$scope.petro.loryNo=$scope.chln.chlnLryNo;
								$scope.petro.advance=$scope.chln.chlnRemAdv;
								$scope.petro.freight=$scope.chln.chlnTotalFreight;
								$scope.petro.rate=$scope.chln.chlnLryRate;
								$scope.broker=data.brk;
								$scope.owner=data.own;
								$scope.petro.brkCode=$scope.broker.brkCode;
								$scope.petro.ownCode=$scope.owner.ownCode;
								$scope.brkReadonlyFlag=true;
								//$scope.ownReadonlyFlag=true;
							}else{
								console.log(data.chlnCode);
								$scope.brkReadonlyFlag=false;
								$scope.petro.challanNo=data.chlnCode;
							}
							//$scope.challanNo=data.chln.chlnCode;
							//$scope.alertToast(data.msg);
						} else {
							$scope.alertToast("Please Enter valid Challan No.");
							$scope.petro.challanNo="";
						}
					});
					res.error(function(data, status, headers, config) {
						console.log("error in response verifyChlnFrAdv: "+data);
					});
				}
				});
			res.error(function(data, status, headers, config) {
				console.log("error in response verifyChlnFrAdv: "+data);
			});
		 
		 
			
	}
	
	$scope.vehDBFlag=true;
	$scope.vehAdvReqDBFlag=true;
	
	$scope.openVehicleDB = function(){
		  console.log("Enter into openVehicleDB()....");
		  var chlnLryNo = $scope.petro.loryNo;		 
		  var len = parseInt(chlnLryNo.length);
		  if(len < 4)
			  return;
//		  $scope.vehicleNo = "";	 
		  console.log("Hitting /getVehicleMstrFCByLryNo ");
		  var response = $http.post($scope.projectName+'/getVehicleMstrFCByLryNo', chlnLryNo);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.filterVehCode = chlnLryNo;
				 // $scope.ownReadonlyFlag=true;
				  $scope.vehList = data.list;
				  $scope.vehDBFlag=false;
				  $('div#vehDB').dialog({
					  autoOpen: false,
					  modal:true,
					  title: "Select Vehicle",
					  show: UDShow,
					  hide: UDHide,
					  position: UDPos,
					  resizable: false,
					  draggable: true,
					  close: function(event, ui) { 
						  $(this).dialog('destroy') ;
						  $(this).hide();
						  $scope.vehDBFlag=true;
					  }
				  });
				  $('div#vehDB').dialog('open');
			  }
		  });	  
		 
	  }
	
	
	$scope.selectVeh=function(vehicle){
		 var response = $http.post($scope.projectName+'/getVehclDetFrAdv',vehicle);
		  response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$('div#vehDB').dialog('close');
				$scope.petro.loryNo=data.vehNo;
				$scope.owner=data.own;
				$scope.petro.ownCode=$scope.owner.ownCode;
			}else{
				$('div#vehDB').dialog('close');
				$scope.alertToast(data.msg);				
			}
		  });
		  response.error(function(data, status, headers,config) {
			  $('div#vehDB').dialog('close');
			  $scope.errorToast(data);
		  });
		
	}
	
	
$scope.brokerDBFlag=true;
	
	$scope.getBrokerByName = function(brkCode){
		  console.log("Enter into openVehicleDB()....");
		  var len = parseInt(brkCode.length);
		  if(len < 4)
			  return;
		  console.log("Hitting /getBrokerByName ");
		  var response = $http.post($scope.projectName+'/getBrokerForAdvByBrkName', brkCode);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  
				  $scope.brkList = data.list;
				  $scope.brokerDBFlag=false;
				  $('div#brokerDB').dialog({
					  autoOpen: false,
					  modal:true,
					  title: "Select Broker",
					  show: UDShow,
					  hide: UDHide,
					  position: UDPos,
					  resizable: false,
					  draggable: true,
					  close: function(event, ui) { 
						  $(this).dialog('destroy') ;
						  $(this).hide();
						  $scope.vehDBFlag=true;
					  }
				  });
				  $('div#brokerDB').dialog('open');
			  }else{
				  $scope.alertToast("Broker not found !");
			  }
		  });	  
		
		 
	  }
	
	
	$scope.saveBrkCode=function(brk){
		 		$('div#brokerDB').dialog('close');
				$scope.broker=brk;
				console.log("brkCode="+$scope.broker.brkCode);
				$scope.petro.brkCode=$scope.broker.brkCode;
	}
	
	
	
	
 $scope.petroCardDBFlag=true;
	 
	 $scope.getCardNo=function(){
		 console.log("Enter into getCardNo()");
		 var res = $http.post($scope.projectName+'/getPetroCardNo',$scope.petro.cardType);
			res.success(function(data, status, headers, config) {
				if (data.result === "success") {
					console.log(data);
					$scope.petroList=data.list;
					
					$scope.petroCardDBFlag = false;
			    	$('div#petroCardDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						show: UDShow,
						title:"Petro Card FaCode",
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					        $scope.saveVsFlag = true;
					    }
						});
					$('div#petroCardDB').dialog('open');
					
					//$scope.alertToast("Successs");
					
				} else {
					$scope.alertToast("There is no petro card");
				}
			});
			res.error(function(data, status, headers, config) {
				console.log("error in response of getPetroCardNo: "+data);
				$scope.lodingFlag = false;
			});
			
	 }
	 
	 $scope.savePetroCode = function(petro){
		 console.log(petro);
		 $scope.petro.cardNo = petro.cardNo;
		 $('div#petroCardDB').dialog('close');
	 } 
	
	
	
	 $scope.generatePetroAllocation=function()
		{
			 var response=$http.post($scope.projectName+ '/generatePetroAllocation');
		       response.success(function(data,status,headers,config)
		    		   {
		    	  console.log(data)
		    	   if(data.msg==="No Petro Details for Current Date")
		    		   {
		    		   $scope.alertToast(data.msg);
		    		   }
		    	   else{
		    		   console.log("Hiiiiiiiiiiiiiiiiiiiiiiiiiii")
		    		   $scope.petroList=data.list;
		    		   $scope.alertToast(data.msg);
		    		   $scope.generateFundData=true;
		    		   $scope.showPetroList=false;
		    	   }
		    	 
		    		   });
		       response.error(function(data,status,headers,config)
		    		   {
		    	   console.log(data.msg)
		    		   });
			
		}
		
		$scope.updateFundAllocation=function(petroAll,index)
		{
			console.log(petroAll);
			 var response=$http.post($scope.projectName+ '/updatePetroAllocation',petroAll);
		       response.success(function(data,status,headers,config)
		    		   {
		    	   if(data.result==="success")
		    		   {
		    		   $scope.alertToast("success");
		    		   $scope.petroList.splice(index,1);
		    		   if($scope.petroList.length==0)
		    			   {
		    				$scope.generateFundData=false;
		    			    $scope.showFundList=true;
		    			   }
		    		   }
		    	   else if(data.result=="error")
		    		   {
		    		   $scope.alertToast(data.msg);
		    		   }
		    	   else{
		    		   $scope.alertToast("There is some problem");
		    		   console.log(data.msg);
		    	   }
		    		   });
		       response.error(function(data,status,headers,config)
		    		   {
		    	   $scope.alertToast("Server Error");
		    	   
		    		   });
			
		}
	 
	
		$scope.rejectPayment=function(ptro,index){
			
			var response=$http.post($scope.projectName+ '/rejectPetroAllocation',ptro);
		       response.success(function(data,status,headers,config)
		    		   {
		    	   if(data.result==="success")
		    		   {
		    		   $scope.alertToast(data.msg);
		    		   $scope.petroList.splice(index,1);
		    		   if($scope.petroList.length==0)
		    			   {
		    				$scope.generateFundData=false;
		    			    $scope.showFundList=true;
		    			   }
		    		   }
		    	  
		    	   else{
		    		   $scope.alertToast(data.msg);
		    	   }
		    		   });
		       response.error(function(data,status,headers,config)
		    		   {
		    	   $scope.alertToast("Server Error");
		    	   
		    		   });
		}
		
	
	$scope.printXls = function() {
		console.log("printXls()");
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "PetroAllocation.xls");
	};
	
}]);
'use strict';

var app = angular.module('application');

app.controller('StockRptCntlr', ['$scope', '$http', '$log', function($scope, $http, $log) {
	
	console.log("StockRptCntlr Started");
	
	$scope.branchList = [];
	$scope.branch = {};
	$scope.custList = [];
	$scope.cust = {};
	$scope.custGroup = {};
	//$scope.stockAndOsRptList = [];
	//$scope.stockAndOsRptTotals = {};
	
	$scope.branchDBFlag = true;
	$scope.custDBFlag = true;
	$scope.custGroupDBFlag=true;
	$scope.lodingFlag = false;
	$scope.printXlsFlag = false;
	
	

	$scope.printPayFlag=false;
	

$scope.payableRpt=function(fromDt,toDt){
    var formData={
    		"fromDt":fromDt,
    		"toDt":toDt
    };
    var response=$http.post($scope.projectName+"/getPayableData",formData);
    response.success(function(data,status){
    	if(data.result==="success"){
    		$scope.alertToast(data.result);
    		$scope.fromDt="";
    		$scope.toDt="";
    		$scope.printPayFlag=true;
    	}
    });	
	}
	
	
	$scope.getActiveBrList = function() {
		console.log("getActiveBrList Entered");
		var response = $http.post($scope.projectName+'/getUserBrCustNBrList');
		response.success(function(data, status, headers, config){
			console.log(data.result);
			if (data.result === "success") {
				console.log("getUserBrCustNBrList result Success");
				$scope.branchList = data.branchList;
				$scope.branch = data.userBranch;
				$scope.custList = data.custList;
				//$scope.getCustListByBranch();
				$scope.allCustGroupList=data.allCustGroupList;
			}else {
				$scope.alertToast("getUserBrCustNBrList: "+data.result);
				console.log("getUserBrCustNBrList result Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("Error in response getActiveBrList: "+data);
		});
	}
	
	$scope.openBranchDB = function() {
		console.log("openBranchDB()");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});
		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch = function(branch){
		console.log("saveBranch()");
		$scope.branch = branch;
		$('div#branchDB').dialog('close');
		$scope.getCustListByBranch();
		$scope.custList = [];
		$scope.cust = {};
		$scope.custGroup={};
	}
	
	$scope.getCustListByBranch = function() {
		console.log("getCustByBranch");
		var res = $http.post($scope.projectName+'/getCustListByBranch', $scope.branch.branchId);
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("getCustListByBranch result: "+data.result);
				$scope.custList = data.custList;
			} else {
				console.log("getCustListByBranch result: "+data.result);
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in response getCustListByBranch: "+data);
		});
	};
	
	
	$scope.clickCustGroup=function(custGroup){
		console.log("click CustGroup...");
		$scope.custGroup=custGroup;
		console.log($scope.custGroup);
		$scope.cust={};
		$scope.branch={};
		$('div#custGroupDB').dialog('close');
	};
	
	$scope.openCustomerGroup=function(){
		console.log("openCustomerGroup....");
		$scope.custGroupDBFlag=false;
		
		$('div#custGroupDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Customer group",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.custGroupDBFlag = false;
			}
		});
		$('div#custGroupDB').dialog('open');
	};
	
	
	
	$scope.openCustDB = function() {
		console.log("openCustDB()");
		$scope.custDBFlag = false;
		$('div#custDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Customer",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.custDBFlag = false;
			}
		});
		$('div#custDB').dialog('open');
	};
	
	$scope.saveCust = function(cust){
		console.log("saveCust()");
		$scope.cust = cust;
		$scope.custGroup={};
		$('div#custDB').dialog('close');
	}
	
	$scope.clearAll = function() {
		console.log("clearAll()");
		$scope.cust = {};
		//$scope.custList = [];
		$scope.branch = {};
		$scope.custGroup= {};
	}
	
	$scope.submitStockRpt = function(stockRptForm) {
		console.log("submitStockRpt()");
		$scope.clear();
		if (stockRptForm.$invalid) {
			console.log("invalidate: "+stockRptForm.$invalid);
			if (stockRptForm.branchName.$invalid) {
				$scope.alertToast("Please enter valid Branch")
			} else if (stockRptForm.custName.$invalid){
				$scope.alertToast("Please enter valid Customer");
			} else if (stockRptForm.upToDtName.$invalid) {
				$scope.alertToast("Please enter valid Date")
			}
		} else {
			console.log("invalidate: "+stockRptForm.$invalid);
			var stockRptService = {
					"branchId"	: $scope.branch.branchId,
					"custId"	: $scope.cust.custId,
					"upToDt"	: $scope.upToDt,
				"custGroupId"	: $scope.custGroup.groupId
			}
			
			$('#submitId').attr("disabled", "disabled");
			$scope.printXlsFlag = false;
			$scope.lodingFlag = true;
			var res = $http.post($scope.projectName+'/getStockRpt', stockRptService);
			res.success(function(data, status, headers, config) {
				$log.info(data);
				JSON.stringify(data);
				if (data.result === "success") {
					console.log("response of getStockRpt: "+data.result);
					$('#submitId').removeAttr("disabled");
					$('#printXlsId').removeAttr("disabled");
					$scope.printXlsFlag = true;
					$scope.lodingFlag = false;
					$scope.alertToast(data.result);
				} else {
					console.log("response of getStockRpt: "+data.result);
					$('#submitId').removeAttr("disabled");
					$scope.lodingFlag = false;
					$scope.alertToast("No Result Found");
				}
			});
			res.error(function(data, status, headers, config) {
				console.log("error in getStockRpt "+data);
				$('#submitId').removeAttr("disabled");
				$scope.lodingFlag = false;
				$scope.alertToast("Error");
			})
			
		}
	}
	
	$scope.printXls = function() {
		console.log("printXls()");
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "stockRpt.xls");
	};
	
	$scope.clear = function() {
		console.log("clear()");
		$scope.filterTextbox1 = "";
		$scope.filterTextbox2 = "";
		$scope.filterTextboxCustGroup ="";
	};
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getActiveBrList();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("StockRptCntlr Ended");
}])
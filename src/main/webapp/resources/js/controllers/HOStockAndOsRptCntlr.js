'use strict';

var app = angular.module('application');

app.controller('HOStockAndOsRptCntlr',['$scope','$location','$http','$filter','$window',  
                                     function($scope,$location,$http,$filter,$window){

	console.log("StockAndOsRptCntlr Started");
	
	$scope.branchList = [];
	$scope.branch = {};
	$scope.custList = [];
	$scope.cust = {};
	
	$scope.stockAndOsRptList = [];
	$scope.stockAndOsRptTotals = {};
	
	$scope.branchDBFlag = true;
	$scope.custDBFlag = true;
	$scope.lodingFlag = false;
	
	$scope.getActiveBrList = function() {
		console.log("getActiveBrList Entered");
		var response = $http.post($scope.projectName+'/getUserBrCustNBrList');
		response.success(function(data, status, headers, config){
			console.log(data.result);
			if (data.result === "success") {
				console.log("getUserBrCustNBrList result Success");
				$scope.branchList = data.branchList;
				$scope.branch = data.userBranch;
				//$scope.getCustListByBranch();
				$scope.custList = data.custList;
			}else {
				$scope.alertToast("getUserBrCustNBrList: "+data.result);
				console.log("getUserBrCustNBrList result Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("Error in response getUserBrCustNBrList: "+data);
		});
	}
	
	$scope.openBranchDB = function() {
		console.log("openBranchDB()");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});
		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch = function(branch){
		console.log("saveBranch()");
		$scope.branch = branch;
		$('div#branchDB').dialog('close');
		$scope.cust = {};
	}
	
	$scope.getCustListByBranch = function() {
		console.log("getCustByBranch");
		var res = $http.post($scope.projectName+'/getCustListByBranch', $scope.branch.branchId);
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("getCustListByBranch result: "+data.result);
				$scope.custList = data.custList;
			} else {
				console.log("getCustListByBranch result: "+data.result);
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in response getCustListByBranch: "+data);
		});
	}
	
	$scope.openCustDB = function() {
		console.log("openCustDB()");
		$scope.custDBFlag = false;
		$('div#custDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Customer",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.custDBFlag = false;
			}
		});
		$('div#custDB').dialog('open');
	}
	
	$scope.saveCust = function(cust){
		console.log("saveCust()");
		$scope.branch = {};
		$scope.cust = cust;
		$('div#custDB').dialog('close');
	}
	
	$scope.saveCustAtJS = function(cust) {
		console.log("saveCustAtJS()");
		$scope.cust = cust;
	}
	
	$scope.clearAll = function() {
		console.log("clearAll()");
		$scope.cust = {};
		//$scope.custList = [];
		$scope.branch = {};
	}
	
	$scope.submitStockAndOsRpt = function(stockAndOsRptForm) {
		console.log("submitStockAndOsRpt()");
		$scope.clear();
		if (stockAndOsRptForm.$invalid) {
			console.log("invalidate: "+stockAndOsRptForm.$invalid);
			if (stockAndOsRptForm.branchName.$invalid) {
				$scope.alertToast("Please enter valid Branch")
			} else if (stockAndOsRptForm.custName.$invalid){
				$scope.alertToast("Please enter valid Customer");
			} else if (stockAndOsRptForm.upToDtName.$invalid) {
				$scope.alertToast("Please enter valid Date")
			}
		} else {
			console.log("invalidate: "+stockAndOsRptForm.$invalid);
			var stockAndOsRptService = {
					"branchId"	: $scope.branch.branchId,
					"custId"	: $scope.cust.custId,
					"upToDt"	: $scope.upToDt
			}
			
			$('#submitId').attr("disabled", "disabled");
			$scope.lodingFlag = true;
			var res = $http.post($scope.projectName+'/getHoStockAndOsRpt', stockAndOsRptService);
			res.success(function(data, status, headers, config) {
				if (data.result === "success") {
					console.log("response of getStockAndRpt: "+data.result);
					console.log(JSON.stringify(data.stockAndOsRptList));
					$scope.stockAndOsRptList = data.stockAndOsRptList;
					$scope.stockAndOsRptTotals = data.stockAndOsRptTotals;
					$('#submitId').removeAttr("disabled");
					$('#printXlsId').removeAttr("disabled");
					$('#emailId').removeAttr("disabled");
					$scope.lodingFlag = false;
					$scope.alertToast(data.result);
				} else {
					console.log("response of getStockAndRpt: "+data.result);
					$('#submitId').removeAttr("disabled");
					$scope.lodingFlag = false;
					$scope.alertToast("No Result Found");
				}
			});
			res.error(function(data, status, headers, config) {
				console.log("error in getStockAndRpt "+data);
				$('#submitId').removeAttr("disabled");
				$scope.lodingFlag = false;
				$scope.alertToast("Error");
			})
			
		}
	}
	
	$scope.printXls = function() {
		console.log("printXls()");
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });		
        saveAs(blob, "stockNOSRpt.xls");
	}
	
	$scope.mail = function(){
		console.log("Enter into mail() !");
		
		$('#emailId').attr("disabled","disabled");
		
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
		var formData = new FormData();
		formData.append("report", blob);
		
		var response = $http.post($scope.projectName+'/sendStockAndOsRpt', formData,{
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        });
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.alertToast(data.msg);
				$('#emailId').removeAttr("disabled");
			}else{
				$scope.alertToast(data.msg);
			}
			
		});
		response.error(function(){
			console.log("Error in sending mail !");
		});
		console.log("Exit from mail() !");
	}
	
	$scope.clear = function() {
		console.log("clear()");
		$scope.filterTextbox1 = "";
		$scope.filterTextbox2 = "";
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getActiveBrList();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("StockAndOsRptCntlr Ended");
}]);
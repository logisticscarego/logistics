'use strict';

var app = angular.module('application');

app.controller('CustomerGroupCntlr',['$scope','$location','$http','$filter',
                                    function($scope,$location,$http,$filter){

	$scope.CustomerCodeDBFlag=true;
	$scope.show = "true";
	$scope.checkCustList=[];
	$scope.selectCustGroupId='';

	
	$scope.OpenCustomerCodeDB = function(){
		$scope.CustomerCodeDBFlag=false;
		$('div#customerCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Customer Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#customerCodeDB').dialog('open');
	}

	$scope.selectCustomerCode = function(cust){
		//console.log(cust.selected);
		 if(cust.selected=="true"){
			 $scope.checkCustList.push(cust.custId);
			 console.log("Add=");
			 console.log(JSON.stringify($scope.checkCustList));
		 }else if(cust.selected=="false"||angular.isUndefined(cust.selected)){
			 console.log("remove=");
			 var index = $scope.checkCustList.indexOf(cust.custId);
			 $scope.checkCustList.splice(index,1);
			 console.log("size of checkCustList="+$scope.checkCustList.length);
			 console.log(JSON.stringify($scope.checkCustList));
		 }
		 
	
	}

	$scope.clickCustomerGroup=function(){
		console.log("click group customer..");
	   console.log($scope.selectCustGroupId);
	}
	
	$scope.getCustomerCodeAndGroupList = function(){
		console.log("getCustomerCodeData------>");
		var response = $http.post($scope.projectName+'/getCustAndGroup');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.custCodeList = data.customerList;
				$scope.custGroupList = data.custGroupList;
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("error in CustomerRepCntlr");
			$scope.errorToast(data);
		});
	}

	$scope.submitCustGroupAndCustomer = function(){
		console.log("enter submitCustGroupAndCustomer.. ")
		if(angular.isUndefined($scope.selectCustGroupId)||angular.isUndefined($scope.checkCustList)){
			$scope.alertToast("please fill correct form ");
		}else if($scope.selectCustGroupId===''){
			$scope.alertToast("Please select Customer Group ");
		}else if($scope.checkCustList.length<=0){
			$scope.alertToast("please minimum check one customer ...");
		}else{
			$scope.show = "false";
			var req = {
						"custGroup"     : $scope.selectCustGroupId,
						"custIdList"    : $scope.checkCustList					
						
			};
			var response = $http.post($scope.projectName+'/submitCustGroupAndCust',req);
			response.success(function(data, status, headers, config) {
				if(data.result === "success"){
					$scope.successToast(data.result);
					$scope.clearAll();
					$scope.getCustomerCodeAndGroupList();
				}else{
					console.log(data);
					$scope.successToast(data.result);
					console.log($scope.checkCustList);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}
	}

	$scope.clearAll=function(){
		console.log("clearAll...");
		angular.forEach($scope.custCodeList, function(cust){
		     cust.selected=false;
		});
		
		$scope.selectCustGroupId='';					
		 $scope.checkCustList=[];
		 console.log($scope.selectCustGroup);
		 console.log($scope.checkCustList);
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getCustomerCodeAndGroupList();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
	

}]);
'use strict';

var app = angular.module('application');

app.controller('ViewChallanCntlr',['$scope','$location','$http','FileUploadService','$filter',
                                   function($scope,$location,$http,FileUploadService,$filter){

	$scope.show = true;
	$scope.ChallanDBFlag=true;
	$scope.showChallanDetailsFlag=true;
	$scope.BranchCodeDBFlag=true;
	$scope.ChallanFromStationDBFlag=true;
	$scope.ChallanToStationDBFlag=true;
	$scope.EmployeeCodeDBFlag=true;
	$scope.PayAtDBFlag=true;
	$scope.VehicleTypeDBFlag=true;
	$scope.ChdBrCodeDBFlag=true;
	$scope.ChdOwnCodeDBFlag=true;
	$scope.ChdPerStateCodeDBFlag=true;
	$scope.ChdPanIssueStDBFlag=true;

	$scope.challan = {};
	$scope.challanDetail = {};

	var chlnAdvc;


	$scope.OpenChallanDB = function(){
		$scope.ChallanDBFlag=false;
		$('div#challanDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Challan Codes",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#challanDB').dialog('open');
	}

	$scope.OpenBranchCodeDB = function(){
		$scope.BranchCodeDBFlag=false;
		$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Branch Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#branchCodeDB').dialog('open');
	}

	$scope.OpenChallanFromStationDB = function(){
		$scope.ChallanFromStationDBFlag=false;
		$('div#challanFromStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Challan From Station",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#challanFromStationDB').dialog('open');
	}

	$scope.OpenChallanToStationDB = function(){
		$scope.ChallanToStationDBFlag=false;
		$('div#challanToStationDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Challan To Station",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#challanToStationDB').dialog('open');
	}

	$scope.OpenEmployeeCodeDB = function(){
		$scope.EmployeeCodeDBFlag=false;
		$('div#employeeCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Employee Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#employeeCodeDB').dialog('open');
	}

	$scope.OpenPayAtDB = function(){
		$scope.PayAtDBFlag=false;
		$('div#payAtDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#payAtDB').dialog('open');
	}

	$scope.OpenVehicleTypeDB = function(){
		$scope.VehicleTypeDBFlag=false;
		$('div#vehicleTypeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Vihicle Type",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#vehicleTypeDB').dialog('open');
	}

	$scope.OpenBrokerCodeDB = function(){
		$scope.ChdBrCodeDBFlag=false;
		$('div#chdBrCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Broker Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#chdBrCodeDB').dialog('open');
	}

	$scope.OpenOwnerCodeDB = function(){
		$scope.ChdOwnCodeDBFlag=false;
		$('div#chdOwnCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Owner Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#chdOwnCodeDB').dialog('open');
	}

	$scope.OpenchdPerStateCodeDB = function(){
		$scope.ChdPerStateCodeDBFlag=false;
		$('div#chdPerStateCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Per State Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#chdPerStateCodeDB').dialog('open');
	}

	$scope.OpenchdPanIssueStDB = function(){
		$scope.ChdPanIssueStDBFlag=false;
		$('div#chdPanIssueStDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Pan Issue State",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#chdPanIssueStDB').dialog('open');
	}

	$scope.savePanIssueStateCode = function(state){
		$scope.challanDetail.chdPanIssueSt= state.stateCode;
		$('div#chdPanIssueStDB').dialog('close');
		$scope.ChdPanIssueStDBFlag=true;
	}

	$scope.savePerStateCode = function(state){
		$scope.challanDetail.chdPerState= state.stateCode;
		$('div#chdPerStateCodeDB').dialog('close');
		$scope.ChdPerStateCodeDBFlag=true;
	}

	$scope.saveBrokerCode = function(brk){
		$scope.challanDetail.chdBrCode= brk.brkCode;
		$('div#chdBrCodeDB').dialog('close');
		$scope.ChdBrCodeDBFlag=true;
	}

	$scope.saveOwnerCode = function(own){
		$scope.challanDetail.chdOwnCode= own.ownCode;
		$('div#chdOwnCodeDB').dialog('close');
		$scope.ChdOwnCodeDBFlag=true;
	}

	$scope.saveVehicleType = function(vt){
		$scope.challan.chlnVehicleType = vt.vtVehicleType;
		$('div#vehicleTypeDB').dialog('close');
		$scope.VehicleTypeDBFlag=true;
		$scope.chlnRRNo($scope.vtList);
	}

	$scope.chlnRRNo = function(chln){
		console.log("enter into chlnRRNo fucntion");
		if($scope.challan.chlnVehicleType === "train"){
			$('#chlnRrNo').removeAttr("disabled");
			$('#chlnTrainNo').removeAttr("disabled");
		}
	}	

	$scope.saveEmployeeCode =  function(employee){
		$scope.challan.chlnEmpCode = employee.empCode;
		$('div#employeeCodeDB').dialog('close');
		$scope.EmployeeCodeDBFlag=true;
	}

	$scope.saveFrmStnCode = function(station){
		$scope.challan.chlnFromStn = station.stnCode;
		$('div#challanFromStationDB').dialog('close');
		$scope.ChallanFromStationDBFlag=true;
	}

	$scope.saveToStnCode = function(station){
		$scope.challan.chlnToStn = station.stnCode;
		$('div#challanToStationDB').dialog('close');
		$scope.ChdPerStateCodeDBFlag=true;
	}


	$scope.saveBranchCode = function(branch){
		$scope.challan.branchCode = branch.branchCode;
		$('div#branchCodeDB').dialog('close');
		$scope.BranchCodeDBFlag=true;
	}

	$scope.saveChallanCode = function(value){
		$scope.chlnCode= value;
		$('div#challanDB').dialog('close');
		$scope.ChallanDBFlag=true;
	}

	$scope.getChallanList = function(){
		console.log("getChallanList------>");
		var response = $http.post($scope.projectName+'/getChallanList');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				//$scope.challanList = data.list;
				//$scope.getBranchData();
				console.log("data from server-->"+$scope.mobList);
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.getBranchData = function(){
		console.log("getBranchData------>");
		var response = $http.post($scope.projectName+'/getBranchDataForChallan');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.branchList = data.list;
				//$scope.getStationData();
			}else{
				$scope.alertToast("you don't have any active branch");
				console.log(data);
			}
		});
		response.error(function(data, status, headers,config) {
			$scope.errorToast(data);
		});
	}

	$scope.getStationData = function(){
		console.log("getStationData------>");
		var response = $http.post($scope.projectName+'/getStationDataForChallan');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.stationList = data.list;
				//$scope.getEmployeeList();
			}else{
				$scope.alertToast("you don't have any station");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}

	$scope.getEmployeeList = function(){
		console.log(" entered into getListOfEmployee------>");
		var response = $http.post($scope.projectName+'/getListOfEmployeeForChallan');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.employeeList = data.list;
				//$scope.getVehicleTypeCode();
			}else{
				$scope.alertToast("You don't have any employee list");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.getVehicleTypeCode = function(){
		console.log("getVehicleTypeCode------>");
		var response = $http.post($scope.projectName+'/getVehicleTypeCodeForChallan');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.vtList = data.list;
				//$scope.getBrokerCode();
			}else{
				$scope.alertToast("you don't have any vehicle type");
				console.log(data);   
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}

	$scope.getBrokerCode = function(){
		console.log("getBrokerCode------>");
		var response = $http.post($scope.projectName+'/getBrokerCodeForCD');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.brkList = data.list;
				//$scope.getOwnerCode();
			}else{
				$scope.alertToast("you don't have any broker");
				console.log(data);   
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}

	$scope.getOwnerCode = function(){
		console.log("getOwnerCode------>");
		var response = $http.post($scope.projectName+'/getOwnerCodeForCD');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.ownList = data.list;
				//$scope.getStateCodeList();
			}else{
				$scope.alertToast("you don't have any owner");
				console.log(data);   
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}

	$scope.getStateCodeList = function(){
		console.log("getStateCodeData------>");
		var response = $http.post($scope.projectName+'/getStateCodeDataForCD');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.stList = data.list;
				console.log("data from server-->"+$scope.stateList);
			}else{
				$scope.alertToast("you don't have any state");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.calTotalFreight = function(challan){
		challan.chlnTotalFreight=parseInt(challan.chlnFreight)+parseInt(challan.chlnLoadingAmt)+parseInt(challan.chlnExtra)-parseInt(challan.chlnStatisticalChg)-parseInt(challan.chlnTdsAmt);
	}

	$scope.calculateFreight = function(){
		var LryRate = 0;
		var ChgWt = 0;
		if($scope.chlnLryRatePer === "Ton"){
			if($scope.chlnChgWtPer === "Ton"){
				$scope.challan.chlnFreight = $scope.challan.chlnLryRate*$scope.challan.chlnChgWt;	
			}else if($scope.chlnChgWtPer === "Kg"){
				LryRate = $scope.challan.chlnLryRate/1000;
				$scope.challan.chlnFreight = LryRate * $scope.challan.chlnChgWt;
			}
		}else if($scope.chlnLryRatePer === "Kg"){
			if($scope.chlnChgWtPer === "Ton"){
				LryRate = $scope.challan.chlnLryRate * 1000;
				$scope.challan.chlnFreight = LryRate*$scope.challan.chlnChgWt;
			}else if($scope.chlnChgWtPer === "Kg"){
				$scope.challan.chlnFreight = $scope.challan.chlnLryRate * $scope.challan.chlnChgWt;
			}
		}
	}

	$scope.checkAdvance =  function(){
		chlnAdvc=0.8*parseInt($scope.challan.chlnTotalFreight);
		if($scope.challan.chlnAdvance>chlnAdvc){
			$scope.challan.chlnAdvance="";
		}
	}

	$scope.balance = function(challan){
		$scope.challan.chlnBalance=parseInt(challan.chlnTotalFreight)-parseInt(challan.chlnAdvance);
	}	

	$scope.uploadChallanImage = function(){
		console.log("enter into uploadChallanImage function");
		var file = $scope.challanImage;
		console.log("size of file ----->"+file.size);
		if(angular.isUndefined(file) || file === null || file === ""){
			$scope.alertToast("First choose the file----->");
		}else if(file.size > $scope.maxFileSize){
			$scope.alertToast("image size must be less than or equal to 1mb");
		}else{
			console.log('file is ' + JSON.stringify(file));
			var uploadUrl = $scope.projectName+"/uploadChallanImage";
			FileUploadService.uploadFileToUrl(file, uploadUrl);
			console.log("file save on server");
		}
	}

	$scope.ViewChallan = function(chlnCode){
		console.log("enter into viewChallan------>"+chlnCode);
		$scope.code=$('#challanCode').val();
		if($scope.code === ""){
			$scope.alertToast("Please enter challan code....");
		}else{
			$scope.showChallanDetailsFlag = false;
			var response = $http.post($scope.projectName+'/viewchallan',chlnCode);
			response.success(function(data, status, headers, config) {
				if(data.result === "success"){
					$scope.challan = data.challan;
					$scope.challanDetail = data.challanDetail;
					console.log("$scope.challan.chlnCode = "+$scope.challan.chlnCode);
					console.log("$scope.challan.bCode = "+$scope.challan.bCode);
					console.log("$scope.challan.userCode = "+$scope.challan.userCode);
					console.log("CreationTs of challan------>"+$scope.challan.creationTS);
					/*$scope.challan.chlnDt =  $filter('date')($scope.challan.chlnDt, 'MM/dd/yyyy hh:mm:ss');
			$scope.challan.chlnLryRepDT =  $filter('date')($scope.challan.chlnLryRepDT, 'MM/dd/yyyy hh:mm:ss');
			$scope.challanDetail.chdRcIssueDt =  $filter('date')($scope.challanDetail.chdRcIssueDt, 'MM/dd/yyyy hh:mm:ss');
			$scope.challanDetail.chdRcValidDt =  $filter('date')($scope.challanDetail.chdRcValidDt, 'MM/dd/yyyy hh:mm:ss');
			$scope.challanDetail.chdDlIssueDt =  $filter('date')($scope.challanDetail.chdDlIssueDt, 'MM/dd/yyyy hh:mm:ss');
			$scope.challanDetail.chdDlValidDt =  $filter('date')($scope.challanDetail.chdDlValidDt, 'MM/dd/yyyy hh:mm:ss');
			$scope.challanDetail.chdPerIssueDt =  $filter('date')($scope.challanDetail.chdPerIssueDt, 'MM/dd/yyyy hh:mm:ss');
			$scope.challanDetail.chdPerValidDt =  $filter('date')($scope.challanDetail.chdPerValidDt, 'MM/dd/yyyy hh:mm:ss');
			$scope.challanDetail.chdFitDocIssueDt =  $filter('date')($scope.challanDetail.chdFitDocIssueDt, 'MM/dd/yyyy hh:mm:ss');
			$scope.challanDetail.chdFitDocValidDt =  $filter('date')($scope.challanDetail.chdFitDocValidDt, 'MM/dd/yyyy hh:mm:ss');
			$scope.challan.creationTS =  $filter('date')($scope.challan.creationTS, 'MM/dd/yyyy hh:mm:ss');
			$scope.challanDetail.creationTS =  $filter('date')($scope.challanDetail.creationTS, 'MM/dd/yyyy hh:mm:ss');*/
					$scope.successToast(data.result);
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}	
	}

	$scope.updateChallan = function(ChallanForm){
		console.log("enter into updateChallan function ChallanForm ="+ChallanForm.$invalid);
		if(ChallanForm.$invalid){
			if(ChallanForm.branchCode.$invalid){
				$scope.alertToast("please enter Branch Code..");
			}else if(ChallanForm.chlnLryNo.$invalid){
				$scope.alertToast("please enter Challan Lorry No..");
			}else if(ChallanForm.chlnFromStn.$invalid){
				$scope.alertToast("Please enter Challan From Station..");
			}else if(ChallanForm.chlnToStn.$invalid){
				$scope.alertToast("please enter Challan To Station..");
			}else if(ChallanForm.chlnEmpCode.$invalid){
				$scope.alertToast("please enter Employee Code");
			}else if(ChallanForm.chlnLryRate.$invalid){
				$scope.alertToast("Please enter Lorry Rate..");
			}else if(ChallanForm.chlnChgWt.$invalid){
				$scope.alertToast("Please enter Charge Weight..");
			}else if(ChallanForm.chlnChgWt1.$invalid){
				$scope.alertToast("Please enter per Ton/Kg for Charge Weight..");
			}else if(ChallanForm.chlnNoOfPkg.$invalid){
				$scope.alertToast("Please enter No.Of Packages ..");
			}else if(ChallanForm.chlnTotalWt.$invalid){
				$scope.alertToast("Please enter Total Weight..");
			}else if(ChallanForm.chlnTotalWt1.$invalid){
				$scope.alertToast("Please enter per Ton/Kg for Total Weight..");
			}else if(ChallanForm.chlnFreight.$invalid){
				$scope.alertToast("Please enter Freight..");
			}else if(ChallanForm.chlnLoadingAmt.$invalid){
				$scope.alertToast("Please enter Loading Amount..");
			}else if(ChallanForm.chlnExtra.$invalid){
				$scope.alertToast("Please enter Extra..");
			}else if(ChallanForm.chlnAdvance.$invalid){
				$scope.alertToast("Please enter Advance..");
			}else if(ChallanForm.chlnPayAt.$invalid){
				$scope.alertToast("Please enter Pay At..");
			}else if(ChallanForm.chlnTimeAllow.$invalid){
				$scope.alertToast("Please enter Time Allowed..");
			}else if(ChallanForm.chlnDt.$invalid){
				$scope.alertToast("Please enter Challan Date..");
			}else if(ChallanForm.chlnBrRate.$invalid){
				$scope.alertToast("Please enter BR Rate..");
			}else if(ChallanForm.chlnWtSlip.$invalid){
				$scope.alertToast("Please enter Weight Slip..");
			}else if(ChallanForm.chlnVehicleType.$invalid){
				$scope.alertToast("Please enter Vehicle Type..");
			}else if(ChallanForm.chlnStatisticalChg.$invalid){
				$scope.alertToast("Please enter Statistical Charge..");
			}else if(ChallanForm.chlnTdsAmt.$invalid){
				$scope.alertToast("Please enter TDS Amount..");
			}else if(ChallanForm.chlnLryLoadTime.$invalid){
				$scope.alertToast("Please enter Lorry Load Time..");
			}else if(ChallanForm.chlnLryRepDT.$invalid){
				$scope.alertToast("Please enter Lorry Reporting Date..");
			}else if(ChallanForm.chlnLryRptTime.$invalid){
				$scope.alertToast("Please enter Lorry Reporting Time..");
			}	
		}else{ 
			if($scope.CDFlag===false){
				console.log("here v are -------------------------------->");
				if(chd.chdChlnCode===null || chd.chdChlnCode ==="" || angular.isUndefined(chd.chdChlnCode) || chd.chdBrCode=== null || chd.chdBrCode==="" || angular.isUndefined(chd.chdBrCode) || chd.chdOwnCode===null || chd.chdOwnCode==="" || angular.isUndefined(chd.chdOwnCode)
						|| chd.chdBrMobNo===null || chd.chdBrMobNo==="" || angular.isUndefined(chd.chdBrMobNo) || chd.chdOwnMobNo===null || chd.chdOwnMobNo==="" || angular.isUndefined(chd.chdOwnMobNo) || chd.chdDvrName===null || chd.chdDvrName==="" || angular.isUndefined(chd.chdDvrName)
						|| chd.chdDvrMobNo===null || chd.chdDvrMobNo==="" || angular.isUndefined(chd.chdDvrMobNo) || chd.chdRcNo===null || chd.chdRcNo==="" || angular.isUndefined(chd.chdRcNo)|| chd.chdRcIssueDt===null || chd.chdRcIssueDt==="" || angular.isUndefined(chd.chdRcIssueDt)
						|| chd.chdRcValidDt===null || chd.chdRcValidDt==="" || angular.isUndefined(chd.chdRcValidDt) || chd.chdDlNo===null || chd.chdDlNo==="" || angular.isUndefined(chd.chdDlNo) || chd.chdDlIssueDt===null || chd.chdDlIssueDt==="" || angular.isUndefined(chd.chdDlIssueDt)
						|| chd.chdDlValidDt===null || chd.chdDlValidDt==="" || angular.isUndefined(chd.chdDlValidDt) || chd.chdPerNo===null || chd.chdPerNo==="" || angular.isUndefined(chd.chdPerNo) || chd.chdPerIssueDt===null || chd.chdPerIssueDt==="" || angular.isUndefined(chd.chdPerIssueDt)
						|| chd.chdPerValidDt===null || chd.chdPerValidDt==="" || angular.isUndefined(chd.chdPerValidDt) || chd.chdPerStateCode===null || chd.chdPerStateCode==="" || angular.isUndefined(chd.chdPerStateCode) || chd.chdFitDocNo===null || chd.chdFitDocNo==="" || angular.isUndefined(chd.chdFitDocNo)
						|| chd.chdFitDocIssueDt===null || chd.chdFitDocIssueDt==="" || angular.isUndefined(chd.chdFitDocIssueDt) || chd.chdFitDocValidDt===null || chd.chdFitDocValidDt==="" || angular.isUndefined(chd.chdFitDocValidDt) || chd.chdFitDocPlace===null || chd.chdFitDocPlace==="" || angular.isUndefined(chd.chdFitDocPlace)
						|| chd.chdBankFinance===null || chd.chdBankFinance==="" || angular.isUndefined(chd.chdBankFinance) || chd.chdPolicyNo===null || chd.chdPolicyNo==="" || angular.isUndefined(chd.chdPolicyNo) || chd.chdPolicyCom===null || chd.chdPolicyCom==="" || angular.isUndefined(chd.chdPolicyCom)
						|| chd.chdTransitPassNo===null || chd.chdTransitPassNo==="" || angular.isUndefined(chd.chdTransitPassNo) || chd.chdPanHdrType===null || chd.chdPanHdrType==="" || angular.isUndefined(chd.chdPanHdrType) || chd.chdPanNo===null || chd.chdPanNo==="" || angular.isUndefined(chd.chdPanNo)
						|| chd.chdPanHdrName===null || chd.chdPanHdrName==="" || angular.isUndefined(chd.chdPanHdrName) || chd.chdPanIssueSt===null || chd.chdPanIssueSt==="" || angular.isUndefined(chd.chdPanIssueSt))
				{
					$scope.alertToast("please Enter challan details");
				}
			}else{	
				console.log("*******************");
				/*$scope.viewChallanDetailsFlag=false;
					$('div#viewChallanDetailsDB').dialog({
						autoOpen: false,
						modal:true,
						title: "Customer Info",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						resizable: false,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy') ;
					        $(this).hide();
					    }
					});

					$('div#viewChallanDetailsDB').dialog('open');*/
				$scope.saveChallan();
			}
		} 	
	}



	$scope.saveChallan = function(){
		console.log("enter into saveChallan function");
		console.log("$scope.challan.chlnCode = "+$scope.challan.chlnCode);
		console.log("$scope.challan.bCode = "+$scope.challan.bCode);
		console.log("$scope.challan.userCode = "+$scope.challan.userCode);
		var chln_ChlnDet = {
				"challan"      		: $scope.challan,
				"challanDetail"   	: $scope.challanDetail
		};
		var response = $http.post($scope.projectName+'/editChlnAndChlnDet',chln_ChlnDet);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				/*$scope.viewChallanDetailsFlag = true;
				$('div#viewChallanDetailsDB').dialog('close');*/
				$scope.successToast(data.result);
				/*for(var i=0 ; i < cnmtCodeList.length ; i++){
					cnmtCodeList.splice(i,1);
				}*/
			}else{
				console.log(data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}

	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getChallanList();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 

	
}]);
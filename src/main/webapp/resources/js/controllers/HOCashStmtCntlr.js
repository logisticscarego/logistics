'use strict';

var app = angular.module('application');

app.controller('HOCashStmtCntlr',['$scope','$location','$http','$filter','$window','$log',
                                 function($scope,$location,$http,$filter,$window,$log){
	
	console.log("HOCashStmtCntlr starts.......");
	
	$scope.brhList = [];
	$scope.cs = {};
	$scope.csList = [];
	
	// Flag
	$scope.brhFlag = true;
	$scope.printVsFlag = true;
	
	$scope.getBranch = function(){
		console.log("Enter into getBranch()");
		
		var response = $http.post($scope.projectName+'/getBrhFrCS');
		
		response.success(function(data, status, headers, config){
			console.log("Result = "+data.result);
			if(data.result === "success"){
				$scope.brhList = data.list;
			}else{
				$scope.alertToast(data.msg);
			}
		});
		
		response.error(function(data, status, headers, config){
			console.log("Error in hitting /getBrhFrCS");
		});
		
		console.log("Exit from getBranch()");
	}
	
	$scope.saveBranch = function(branch){
		console.log("Enter into saveBranch()");
		
		$scope.cs.csBranchName = branch.branchName;
		$scope.cs.csBranchId = branch.branchId;
		
		// Close branch dialog box
		$scope.brhFlag = true;
		$('div#brhId').dialog('close');
		
		console.log("Exit from saveBranch()");
	}
	
	$scope.submitCS = function(CashStmtForm){
		console.log("Enter into submitCS() : Form_Valid = "+CashStmtForm.$valid);
		
		if(CashStmtForm.$invalid){
			if(CashStmtForm.branchName.$invalid)
				$scope.alertToast("Select branch !");
			if(CashStmtForm.branchId.$invalid)
				$scope.alertToast("Select branch !");
			if(CashStmtForm.fromDate.$invalid)
				$scope.alertToast("Enter from date !");
			if(CashStmtForm.toDate.$invalid)
				$scope.alertToast("Enter to date !");
		}else{
			var response = $http.post($scope.projectName + '/getHoCs', $scope.cs);
			response.success(function(data, status, headers, config){
				console.log("Result = "+data.result);
				if(data.result === "success"){
					$scope.csList = data.csList;
					$scope.printVsFlag = false;
			    	$('div#printVsDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					        $scope.printVsFlag = true;
					    }
						});
					$('div#printVsDB').dialog('open');
				}else{
					$scope.alertToast(data.msg);
				}
			});
			response.error(function(data, status, headers, config){
				console.log("Error in hitting /getHoCs");
			});
		}
		
		console.log("Exit from submitCS()");
	}
             
	
	$scope.openBrhDB = function(){
		console.log("Enter into getBranch()");
		
		$scope.brhFlag = false;
    	$('div#brhId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.brhFlag = true;
		    }
			});
		$('div#brhId').dialog('open');
		
		console.log("Exit from getBranch()");
	}
	
	$scope.cancelPrint = function(){
		console.log("Enter into cancelPrint()");
		$('div#printVsDB').dialog('close');
		$scope.printVsFlag = true;	
		console.log("Exit from cancelPrint()");
	}
	
	$scope.print = function(){
		var content = document.getElementById("newCsPrint").innerHTML;
		var mywindow = window.open('', '_blank');
	    mywindow.document.write('<html><body>');
	    mywindow.document.write(content);
	    mywindow.document.write('</body></html>');
	    mywindow.document.close();
	    mywindow.print();
	    mywindow.close();
	    $scope.printVsFlag = true;	
	    $('div#printVsDB').dialog('close');
	}
	
	                               
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getBranch();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
	console.log("HOCashStmtCntlr ends.......");
	
}]);
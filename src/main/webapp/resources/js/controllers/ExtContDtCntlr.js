'use strict';

var app = angular.module('application');

app.controller('ExtContDtCntlr',['$scope','$location','$http','$window',
                                 function($scope,$location,$http,$window){
	
	$scope.contFaList = [];
	
	$scope.showCont = false;
	$scope.contCodeDBFlag = true;
	
	 $scope.getContCode = function(){
		  console.log("enter into getContCode function");
		  var response = $http.post($scope.projectName+'/getCCFrExDt');
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.contFaList = data.list;
					/*$scope.vehList = data.vehList;
					$scope.stnList = data.stnList;*/
				}else{
					$scope.alertToast("Contract are not avaliable");
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
	  }
	
	
	 $scope.openCodesDB = function(){
		  console.log("enter into openCodesDB function");
		  $scope.contCodeDBFlag = false;
			$('div#contCodeDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title:"select Contract Code",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.contCodeDBFlag = true;
			    }
				});
			$('div#contCodeDB').dialog('open');
	  }
	 
	 
	 
	 $scope.saveContCode = function(code){
		  console.log("enter into saveContCode function");
		  $scope.contCode = code;
		  $('div#contCodeDB').dialog('close');
	  }
	 
	 
	 $scope.viewContract = function(code,contForm){
		  console.log("enter into viewContract function");
		  if(contForm.$invalid){
			$scope.alertToast("please select a contract code");  
		  }else{
			  var response = $http.post($scope.projectName+'/getContFrExtDt',code);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						console.log("success");
						$scope.contract = data.cotract;
						$scope.showCont = true;
					}else{
						console.log("failure");
						$scope.showCont = false;
						$scope.alertToast(data.msg);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
		  }
	  }
	 
	
	 
	$scope.submit = function(){
		console.log("enter into submit function");
		var req = {
				"contCode" : $scope.contract.contFaCode,
				"date"     : $scope.contract.contToDt	
		};
		
		
		var response = $http.post($scope.projectName+'/extDtOfCont',req);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.showCont = false;
			}else{
				$scope.alertToast("Server Error");
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
		
	}
	 
	 
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getContCode();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 

}]);
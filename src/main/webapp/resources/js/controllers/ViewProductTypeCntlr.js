'use strict';

var app = angular.module('application');

app.controller('ViewProductTypeCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.getProductTypeDetails = function(){
		console.log("Entered into getProductTypeDetails function------>");
		var response = $http.post($scope.projectName+'/getProductTypeDetails');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.producttypeList = data.list;	
				$scope.successToast(data.result);
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
		
	 $scope.update = function(producttype){
			console.log("Entered into update function---");
			
			var ptNameLength = producttype.ptName.length;
			
			if(ptNameLength < 3 ||ptNameLength > 40){
				$scope.alertToast("Please enter product type between 3-40 characters.....");
			}else{
					var response = $http.post($scope.projectName+'/updateProductType', producttype);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
							$scope.successToast(data.result);
						}else{
							console.log(data);
						}	
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data);
					});
			 }
		  } 
	 
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getProductTypeDetails();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
	
	
}]);
'use strict';

var app = angular.module('application');

app.controller('ArvRepAlwCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){
	$scope.brhList = [];
	
	$scope.brhDBFlag = true;
	$scope.arCodeDBFlag = true;
	$scope.showAR = true;
	var arExtKmDB;
	var arRemExtKmDB;
	var arOHDB;
	var arRemOHDB;
	var arWtShrtgDB ;
	var arRemWtShrtgDB ;
	var arClaimDB;
	//var arRemClaimDB;
	var arUnloadingDB;
	var arRemUnloadingDB;
	var arDetentionDB;
	var arRemDetentionDB;
	var arLateDeliveryDB;
	var arRemLateDeliveryDB;
	var arLateAckDB;
	var arRemLateAckDB;
	var arDrRcvrWtDB;
	var arRemDrRcvrWtDB;
	
	
	$scope.getBranch = function(){
		console.log("enter inot getBranch funciton");
		var response = $http.post($scope.projectName+'/getBrhSedrAlw');
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.brhList = data.brhList;
			}else{
				$scope.alertToast("Server Error");
				console.log("error in fetching data from getBranch");
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	
	$scope.openBrhDB = function(){
		console.log("enter into openBrhDB funciton");
		$scope.brhDBFlag = false;
    	$('div#brhDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			title:"Select Branch",
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.brhDBFlag = true;
		    }
			});
    	$('div#brhDB').dialog('open'); 
	}
	
	
	$scope.saveBranch = function(branch){
		console.log("enter into saveBranch function = "+branch.brhId);
		var actBrhCode = "";
		$scope.actBrhName   = branch.branchName;
		$scope.actBrhFaCode = branch.branchFaCode;
		actBrhCode = branch.brhId.toString();
		$('div#brhDB').dialog('close'); 
		var response = $http.post($scope.projectName+'/getSedrFrAlw',actBrhCode);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.arCodeList = data.list;	
			}else{
				$scope.alertToast($scope.actBrhName+" branch does not have any sedr");
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	
	$scope.openArCodeDB = function(){
		console.log("enter into openArCodeDB funciton");
		if(angular.isUndefined($scope.actBrhName) || $scope.actBrhName === "" || $scope.actBrhName === null){
			$scope.alertToast("Please Select A Branch");
		}else{
			$scope.arCodeDBFlag = false;
	    	$('div#arCodeDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				title:"Select SEDR",
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.arCodeDBFlag = true;
			    }
				});
	    	$('div#arCodeDB').dialog('open'); 
		}
	}
	
	
	$scope.saveSedr = function(arCode){
		console.log("enter into saveSedr function = "+arCode);
		$scope.arCode=arCode;
		$('div#arCodeDB').dialog('close'); 
		var response = $http.post($scope.projectName+'/getSedrFrHoAlw',arCode);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				console.log(data);
				$scope.arrivalReport = data.arObj;
				 arExtKmDB = data.arObj.arExtKm;
				 arRemExtKmDB = data.arObj.arRemExtKm;
				 arOHDB = data.arObj.arOvrHgt;
				 arRemOHDB = data.arObj.arRemOvrHgt;
				 arWtShrtgDB = data.arObj.arWtShrtg;
				 arRemWtShrtgDB = data.arObj.arRemWtShrtg;
				 arClaimDB = data.arObj.arClaim;
				// arRemClaimDB = data.arObj.arRemClaim;
				 arUnloadingDB = data.arObj.arUnloading;
				 arRemUnloadingDB = data.arObj.arRemUnloading;
				 arDetentionDB = data.arObj.arDetention;
				 arRemDetentionDB = data.arObj.arRemDetention; 
				 arLateDeliveryDB = data.arObj.arLateDelivery;
				 arRemLateDeliveryDB = data.arObj.arRemLateDelivery;
				 arLateAckDB = data.arObj.arLateAck;
				 arRemLateAckDB = data.arObj.arRemLateAck;
				 arDrRcvrWtDB = data.arObj.arDrRcvrWt;
				 arRemDrRcvrWtDB = data.arObj.arRemDrRcvrWt;
				 
				 $scope.lorryNO=data.lorryNo;
				 $scope.chlnDt=data.chlnDt;
				$scope.showAR = false;
			}else{
				$scope.alertToast("Server Error");
				console.log("error in fetching result from saveSedr");
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	
	$scope.editARSubmit = function(arrivalReport){
		console.log("enter into editARSubmit function");
		$('#saveId').attr("disabled","disabled");
		var response = $http.post($scope.projectName+'/editArByHoAlw', arrivalReport);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.alertToast("success");
				$scope.arrivalReport = {};
				$scope.arCode = "";
				//$scope.actBrhName = "";
				$scope.actBrhFaCode = "";
				$scope.showAR = true;
				$('#saveId').removeAttr("disabled");
			}else{
				
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	
	
	$scope.calClaim = function(){
		console.log("enter into calClaim function");
		$scope.arrivalReport.arClaim = 0;
		//$scope.ar.arClaim = $scope.ar.arExtKm + $scope.ar.arOvrHgt + $scope.ar.arWtShrtg + $scope.ar.arPenalty + $scope.ar.arOther;
		$scope.arrivalReport.arClaim = $scope.arrivalReport.arWtShrtg + $scope.arrivalReport.arDrRcvrWt + $scope.arrivalReport.arLateAck + $scope.arrivalReport.arLateDelivery + $scope.arrivalReport.arOther;
		//$scope.chngClaim();
	}
	
	$scope.chngExKm=function(){
		console.log("enter into chngExKm function");
		$scope.arrivalReport.arRemExtKm=parseFloat(parseFloat($scope.arrivalReport.arExtKm - (arExtKmDB-arRemExtKmDB)).toFixed(2)); 
	}
	
	$scope.chngRemExKm=function(){
		console.log("enter into chngRemExKm function");
		$scope.arrivalReport.arExtKm=parseFloat(arExtKmDB-(parseFloat(arRemExtKmDB - $scope.arrivalReport.arRemExtKm)).toFixed(2));
		
	}
	
	$scope.chngOH=function(){
		console.log("enter into chngOH function");
		$scope.arrivalReport.arRemOvrHgt = parseFloat(parseFloat($scope.arrivalReport.arOvrHgt - (arOHDB-arRemOHDB)).toFixed(2)); 
	}
	
	$scope.chngRemOH=function(){
		console.log("enter into chngRemOH function");
		$scope.arrivalReport.arOvrHgt=parseFloat(arOHDB -(parseFloat(arRemOHDB - $scope.arrivalReport.arRemOvrHgt)).toFixed(2));
		
	}
	
	$scope.chngWtShrtg=function(){
		console.log("enter into chngWtShrtg function");
		$scope.arrivalReport.arRemWtShrtg=parseFloat(parseFloat($scope.arrivalReport.arWtShrtg - (arWtShrtgDB-arRemWtShrtgDB)).toFixed(2));
	}
	
	$scope.chngRemWtShrtg = function(){
		console.log("enter into chngRemWtShrtg function");
		$scope.arrivalReport.arWtShrtg=parseFloat(arWtShrtgDB -(parseFloat(arRemWtShrtgDB - $scope.arrivalReport.arRemWtShrtg)).toFixed(2));
	}
	
	/*$scope.chngClaim = function(){
		console.log("enter into chngClaim function");
		$scope.arrivalReport.arRemClaim=parseFloat(parseFloat($scope.arrivalReport.arClaim - (arClaimDB-arRemClaimDB)).toFixed(2));
	}*/
	
	/*$scope.chngRemClaim = function(){
		console.log("enter into chngRemClaim function");
		$scope.arrivalReport.arClaim =parseFloat(arClaimDB -(parseFloat(arRemClaimDB - $scope.arrivalReport.arRemClaim)).toFixed(2));
	}*/
	
	$scope.chngUnloading = function(){
		console.log("enter into chngUnloading function");
		$scope.arrivalReport.arRemUnloading =parseFloat(parseFloat($scope.arrivalReport.arUnloading - (arUnloadingDB-arRemUnloadingDB)).toFixed(2));
	}
	
	$scope.chngRemUnloading = function(){
		console.log("enter into chngRemUnloading function");
		$scope.arrivalReport.arUnloading =parseFloat(arUnloadingDB -(parseFloat(arRemUnloadingDB - $scope.arrivalReport.arRemUnloading)).toFixed(2));
	}
	
	$scope.chngDetention = function(){
		console.log("enter into chngDetention function");
		$scope.arrivalReport.arRemDetention =parseFloat(parseFloat($scope.arrivalReport.arDetention - (arDetentionDB-arRemDetentionDB)).toFixed(2));
	}
	
	$scope.chngRemDetention = function(){
		console.log("enter into chngRemDetention function");
		$scope.arrivalReport.arDetention =parseFloat(arDetentionDB -(parseFloat(arRemDetentionDB - $scope.arrivalReport.arRemDetention)).toFixed(2));
	}
	
	$scope.chngLateDelivery= function(){
		console.log("enter into chngLateDelivery function");
		$scope.arrivalReport.arRemLateDelivery =parseFloat(parseFloat($scope.arrivalReport.arLateDelivery - (arLateDeliveryDB-arRemLateDeliveryDB)).toFixed(2));
	}
	
	$scope.chngRemLateDelivery = function(){
		console.log("enter into chngRemLateDelivery function");
		$scope.arrivalReport.arLateDelivery =parseFloat(arLateDeliveryDB -(parseFloat(arRemLateDeliveryDB - $scope.arrivalReport.arRemLateDelivery)).toFixed(2));
	}
	
	$scope.chngLateAck = function(){
		console.log("enter into chngLateAck function");
		$scope.arrivalReport.arRemLateAck =parseFloat(parseFloat($scope.arrivalReport.arLateAck - (arLateAckDB-arRemLateAckDB)).toFixed(2));
	}
	
	$scope.chngRemLateAck = function(){
		console.log("enter into chngRemLateAck function");
		$scope.arrivalReport.arLateAck =parseFloat(arLateAckDB -(parseFloat(arRemLateAckDB - $scope.arrivalReport.arRemLateAck)).toFixed(2));
	}
	
	$scope.chngDrRcvrWt = function(){
		console.log("enter into chngDrRcvrWt function");
		$scope.arrivalReport.arRemDrRcvrWt =parseFloat(parseFloat($scope.arrivalReport.arDrRcvrWt - (arDrRcvrWtDB-arRemDrRcvrWtDB)).toFixed(2));
	}
	
	$scope.chngRemDrRcvrWt = function(){
		console.log("enter into chngRemDrRcvrWt function");
		$scope.arrivalReport.arDrRcvrWt =parseFloat(arDrRcvrWtDB -(parseFloat(arRemDrRcvrWtDB - $scope.arrivalReport.arRemDrRcvrWt)).toFixed(2));
	}
	
	//validate
	$scope.chkValExKm = function(){
		console.log("enter into chkValExKm function");
		
		if($scope.arrivalReport.arExtKm<parseFloat(arExtKmDB-arRemExtKmDB)){
			$scope.alertToast("Please Enter Extra Km Amt >= "+parseFloat(arExtKmDB-arRemExtKmDB));
			$scope.arrivalReport.arExtKm=arExtKmDB;
			$scope.chngExKm();
		}
		
		if($scope.arrivalReport.arExtKm==null){
			$scope.arrivalReport.arExtKm=arExtKmDB;
			$scope.chngExKm();
			$scope.calClaim();
		}
		if($scope.arrivalReport.arRemExtKm==null){
			$scope.arrivalReport.arRemExtKm=arRemExtKmDB;
			$scope.chngRemExKm();
			$scope.calClaim();
		}
		
	}
	$scope.chkValOH = function(){
		console.log("enter into chkValOH function");
		if($scope.arrivalReport.arOvrHgt < parseFloat(arOHDB-arRemOHDB)){
			$scope.alertToast("Please Enter Over Height Amt >= "+parseFloat(arOHDB-arRemOHDB));
			$scope.arrivalReport.arOvrHgt=arOHDB;
			$scope.chngOH();
		}
		
		if($scope.arrivalReport.arOvrHgt==null){
			$scope.arrivalReport.arOvrHgt=arOHDB;
			$scope.chngOH();
			$scope.calClaim();
		}
		if($scope.arrivalReport.arRemOvrHgt==null){
			$scope.arrivalReport.arRemOvrHgt =arRemOHDB;
			$scope.chngRemOH();
			$scope.calClaim();
		}
	}
	
	$scope.chkValWtShrtg = function(){
		console.log("enter into chkValWtShrtg function");
		if($scope.arrivalReport.arWtShrtg < parseFloat(arWtShrtgDB-arRemWtShrtgDB)){
			$scope.alertToast("Please Enter Wt Shrtg Amt >= "+parseFloat(arWtShrtgDB-arRemWtShrtgDB));
			$scope.arrivalReport.arWtShrtg=arWtShrtgDB;
			$scope.chngWtShrtg();
		}
		
		if($scope.arrivalReport.arWtShrtg==null){
			$scope.arrivalReport.arWtShrtg=arWtShrtgDB;
			$scope.chngWtShrtg();
			$scope.calClaim();
		}
		if($scope.arrivalReport.arRemWtShrtg==null){
			$scope.arrivalReport.arRemWtShrtg =arRemWtShrtgDB;
			$scope.chngRemWtShrtg();
			$scope.calClaim();
		}
	}
	
	/*$scope.chkValClaim = function(){
		console.log("enter into chkValClaim function");
		if($scope.arrivalReport.arClaim < parseFloat(arClaimDB - arRemClaimDB)){
			$scope.alertToast("Please Enter Claim Amt >= "+parseFloat(arClaimDB - arRemClaimDB));
			$scope.arrivalReport.arClaim = arClaimDB;
			$scope.chngClaim();
		}
		
		if($scope.arrivalReport.arClaim == null){
			$scope.arrivalReport.arClaim = arClaimDB;
			$scope.chngClaim();
			$scope.calClaim();
		}
		if($scope.arrivalReport.arRemClaim == null){
			$scope.arrivalReport.arRemClaim = arRemClaimDB;
			$scope.chngRemClaim();
			$scope.calClaim();
		}
	}*/
	
	$scope.chkValUnloading = function(){
		console.log("enter into chkValUnloading function");
		if($scope.arrivalReport.arUnloading < parseFloat(arUnloadingDB - arRemUnloadingDB)){
			$scope.alertToast("Please Enter Unloading Amt >= "+parseFloat(arUnloadingDB - arRemUnloadingDB));
			$scope.arrivalReport.arUnloading = arUnloadingDB;
			$scope.chngUnloading();
		}
		
		if($scope.arrivalReport.arUnloading == null){
			$scope.arrivalReport.arUnloading = arUnloadingDB;
			$scope.chngUnloading();
			$scope.calClaim();
		}
		if($scope.arrivalReport.arRemUnloading == null){
			$scope.arrivalReport.arRemUnloading = arRemUnloadingDB;
			$scope.chngRemUnloading();
			$scope.calClaim();
		}
	}
	
	$scope.chkValDetention = function(){
		console.log("enter into chkValDetention function");
		if($scope.arrivalReport.arDetention < parseFloat(arDetentionDB - arRemDetentionDB)){
			$scope.alertToast("Please Enter Detention Amt >= "+parseFloat(arDetentionDB - arRemDetentionDB));
			$scope.arrivalReport.arDetention = arDetentionDB;
			$scope.chngDetention();
		}
		
		if($scope.arrivalReport.arDetention == null){
			$scope.arrivalReport.arDetention = arDetentionDB;
			$scope.chngDetention();
			$scope.calClaim();
		}
		if($scope.arrivalReport.arRemDetention == null){
			$scope.arrivalReport.arRemDetention = arRemDetentionDB;
			$scope.chngRemDetention();
			$scope.calClaim();
		}
	}
	
	$scope.chkValLateDelivery = function(){
		console.log("enter into chkValLateDelivery function");
		if($scope.arrivalReport.arLateDelivery < parseFloat(arLateDeliveryDB - arRemLateDeliveryDB)){
			$scope.alertToast("Please Enter LateDelivery Amt >= "+parseFloat(arLateDeliveryDB - arRemLateDeliveryDB));
			$scope.arrivalReport.arLateDelivery = arLateDeliveryDB;
			$scope.chngLateDelivery();
		}
		
		if($scope.arrivalReport.arLateDelivery == null){
			$scope.arrivalReport.arLateDelivery = arLateDeliveryDB;
			$scope.chngLateDelivery();
			$scope.calClaim();
		}
		if($scope.arrivalReport.arRemLateDelivery == null){
			$scope.arrivalReport.arRemLateDelivery = arRemLateDeliveryDB;
			$scope.chngRemLateDelivery();
			$scope.calClaim();
		}
	}
	
	$scope.chkValLateAck = function(){
		console.log("enter into chkValLateAck function");
		if($scope.arrivalReport.arLateAck < parseFloat(arLateAckDB - arRemLateAckDB)){
			$scope.alertToast("Please Enter LateAck Amt >= "+parseFloat(arLateAckDB - arRemLateAckDB));
			$scope.arrivalReport.arLateAck = arLateAckDB;
			$scope.chngLateAck();
		}
		
		if($scope.arrivalReport.arLateAck == null){
			$scope.arrivalReport.arLateAck = arLateAckDB;
			$scope.chngLateAck();
			$scope.calClaim();
		}
		if($scope.arrivalReport.arRemLateAck == null){
			$scope.arrivalReport.arRemLateAck = arRemLateAckDB;
			$scope.chngRemLateAck();
			$scope.calClaim();
		}
	}
	
	$scope.chkValDrRcvrWt = function(){
		console.log("enter into chkValDrRcvrWt function");
		if($scope.arrivalReport.arDrRcvrWt < parseFloat(arDrRcvrWtDB - arRemDrRcvrWtDB)){
			$scope.alertToast("Please Enter DrRcvrWt Amt >= "+parseFloat(arDrRcvrWtDB - arRemDrRcvrWtDB));
			$scope.arrivalReport.arDrRcvrWt = arDrRcvrWtDB;
			$scope.chngDrRcvrWt();
		}
		
		if($scope.arrivalReport.arDrRcvrWt == null){
			$scope.arrivalReport.arDrRcvrWt = arDrRcvrWtDB;
			$scope.chngDrRcvrWt();
			$scope.calClaim();
		}
		if($scope.arrivalReport.arRemDrRcvrWt == null){
			$scope.arrivalReport.arRemDrRcvrWt = arRemDrRcvrWtDB;
			$scope.chngRemDrRcvrWt();
			$scope.calClaim();
		}
	}
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getBranch();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);
'use strict';

var app = angular.module('application');

app.controller('ServiceTaxCntlr',['$scope','$location','$http',
	                                 function($scope,$location,$http){
	
	$scope.viewServiceTaxFlag = true;
	$scope.servicetax = {};
	
	$scope.submit = function(ServiceTaxForm,servicetax){
		console.log("Entered into submit function");
		if(ServiceTaxForm.$invalid){
			$scope.alertToast("Form is not valid.");
		}else{

			 $scope.viewServiceTaxFlag = false;
				$('div#viewServiceTaxDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Service Tax Details",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
				});
				$('div#viewServiceTaxDB').dialog('open');
		}
	}
		
	$scope.saveServiceTax = function(servicetax){
		console.log("Entered into saveServiceTax function");
		var response = $http.post($scope.projectName+'/saveServiceTax', servicetax);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$('div#viewServiceTaxDB').dialog('close');
				$scope.viewServiceTaxFlag = true;
				$scope.successToast(data.result);
				$scope.servicetax.stTaxableRt = "";
				$scope.servicetax.stSerTaxRt = "";
				$scope.servicetax.stEduCessRt = "";
				$scope.servicetax.stHscCessRt = "";
				$scope.servicetax.stSwhBHCessRt = "";
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.closeViewServiceTaxDB = function(){
		$('div#viewServiceTaxDB').dialog('close');
		$scope.viewServiceTaxFlag = true;
	}
	
	 $('#stTaxableRt').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	 });
	 
	 $('#stTaxableRt').keypress(function(e) {
			if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 5)){
		           e.preventDefault();
			}
	 });
	 
	 $('#stSerTaxRt').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	 });
	 
	 $('#stSerTaxRt').keypress(function(e) {
			if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 5)){
		           e.preventDefault();
		    }
	 });
	 
	 $('#stEduCessRt').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	 });
	 
	 $('#stEduCessRt').keypress(function(e) {
			if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 5)){
		           e.preventDefault();
		    }
	 });
	 
	 $('#stHscCessRt').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	 });
		
	 $('#stHscCessRt').keypress(function(e) {
			if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 5)){
		           e.preventDefault();
		    }
	 });
	 
	 $('#stSwhBHCessRt').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	 });
		
	 $('#stSwhBHCessRt').keypress(function(e) {
			if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 5)){
		           e.preventDefault();
		    }
	 });
	 
	 if($scope.superAdminLogin === true || $scope.superAdminLogin === true){
		 }else if($scope.logoutStatus === true){
			 $location.path("/");
		 }else{
			 console.log("****************");
		 } 
	
	 
}]);
'use strict';

var app = angular.module('application');

app.controller('CancelMRNCntlr',['$scope','$location','$http','$filter','$sce',
                              function($scope,$location,$http,$filter,$sce){
	
	$scope.brhList = [];
	$scope.branch = {};
	$scope.openBrhFlag = true;
	$scope.isFormSubmitted = false;
	$('#submitId').removeAttr("disabled");
	$scope.getBranch = function(){
		console.log("enter into getBranch function");
		var response = $http.post($scope.projectName+'/gerMrCancelInfo');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.brhList = data.list;
			  }else{
				$scope.alertToast("SERVER ERROR");  
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	$scope.openBrhDB = function(){
		console.log("enter into openBrhDB function");
		$scope.openBrhFlag = false;
		$('div#openBrhId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Branch",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.openBrhFlag = true;
		    }
			});
		
		$('div#openBrhId').dialog('open');	
	}
	
	
	$scope.saveBranch = function(brh){
		console.log("enter into saveBranch function");
		$scope.branch = brh;
		$('div#openBrhId').dialog('close');
	}
	
	
	$scope.cancelMrSubmit = function(cancelMrForm){
		console.log("enter into cancelMrSubmit function = "+cancelMrForm.$invalid);
		
		$('#submitId').attr("disabled","disabled");
		if(cancelMrForm.$invalid){
			$scope.isFormSubmitted = true;
			$('#submitId').removeAttr("disabled");
		}else{
			$scope.isFormSubmitted = false;
			var req = {
					"brhId"  : $scope.branch.branchId,
					"mrType" : $scope.mrType,
					"mrNo"   : $scope.mrNo
			};
			
			var response = $http.post($scope.projectName+'/cancelMrSubmit',req);
			  response.success(function(data, status, headers, config){
				  if(data.result === "success"){
					  $scope.alertToast(data.msg);
					  $scope.mrNo = "";
					  $scope.branch = {};
					  $('#submitId').removeAttr("disabled");
				  }else{
					  $scope.alertToast(data.msg); 
					  $('#submitId').removeAttr("disabled");
				  }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
					$('#submitId').removeAttr("disabled");
			   });
			
		}
	}
	
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getBranch();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
}]);
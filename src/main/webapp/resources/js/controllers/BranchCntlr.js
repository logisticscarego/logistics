'use strict';

var app = angular.module('application');

app.controller('BranchCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	$scope.branch = {};
	$scope.employee = {};
	$scope.temp={};
	$scope.station = {};
	$scope.state = {};	
	
	var basic;
	var hra;
	var pf;
	var gross;
	var netsalary;
	var esi;
	var otherallowance = 0;
		
	$scope.result = "";
	$scope.currentEmployeeName = "";
	$scope.currentEmployeeCode = "";
		
	$scope.branchCodeOnBlur = "";
	
	$scope.branchDirectorDBFlag = true;
	$scope.branchMarketingHDDBFlag = true;
	$scope.branchOutStandingHDDBFlag = true;
	$scope.branchMarketingDBFlag = true;
	$scope.branchMngrDBFlag = true;
	$scope.branchCashierDBFlag = true;
	$scope.branchTrafficDBFlag = true;
	$scope.branchAreaMngrDBFlag = true;
	$scope.branchRegionalMngrDBFlag = true;
	$scope.branchStationCodeDBFlag = true;
	$scope.stateCodeDBFlag = true;
		 	
	$scope.clrBtnBranchDirectorNE=false;
	$scope.clrBtnbranchMarketingHDNE=false;
	$scope.clrBtnBranchOutStandingHDNE=false;
	$scope.clrBtnBranchMarketingNE=false;
	$scope.clrBtnBranchMngrNE=false;
	$scope.clrBtnBranchCashierNE=false;
	$scope.clrBtnBranchTrafficNE=false;
	$scope.clrBtnBranchAreaMngrNE=false;
	$scope.clrBtnBranchRegionalMngrNE=false;
	$scope.clrBtnBranchDirectorEE=false;
	$scope.clrBtnbranchMarketingHDEE=false;
	$scope.clrBtnBranchOutStandingHDEE=false;
	$scope.clrBtnBranchMarketingEE=false;
	$scope.clrBtnBranchMngrEE=false;
	$scope.clrBtnBranchCashierEE=false;
	$scope.clrBtnBranchTrafficEE=false;
	$scope.clrBtnBranchAreaMngrEE=false;
	$scope.clrBtnBranchRegionalMngrEE=false;
	
	$scope.branchDirectorFlag = false;
	$scope.branchMarketingHDFlag = false;
	$scope.branchOutStandingHDFlag = false;
	$scope.branchMarketingFlag = false;
	$scope.branchMngrFlag = false;
	$scope.branchCashierFlag = false;
	$scope.branchTrafficFlag = false;
	$scope.branchAreaMngrFlag = false;
	$scope.branchRegionalMngrFlag = false;
	
	$scope.presentAddrDBFlag = true;
	$scope.viewBranchDetailsFlag = true;
	$scope.viewEmpDetailsFlag = true;
	
	
	$scope.EmployeeDBFlag = true;
	$("#empESI").attr('disabled','disabled');
	
	$scope.SubmitBranch=function(BranchForm,branch){
		console.log("Entered into SubmitBranch function of branch="+BranchForm.$invalid);
		if(BranchForm.$invalid){
			if(BranchForm.branchName.$invalid){
				$scope.alertToast("Please enter branch name between 3-40 characters...");
			}else if(BranchForm.branchAdd.$invalid){
				$scope.alertToast("Please enter branch address between 3-255 characters...");
			}else if(BranchForm.branchCity.$invalid){
				$scope.alertToast("Please enter branch city between 3-40 characters...");
			}else if(BranchForm.branchState.$invalid){
				$scope.alertToast("Please enter branch state between 3-40 characters...");
			}else if(BranchForm.branchPin.$invalid){
				$scope.alertToast("Please enter PIN number of 6 digits...");
			}else if(BranchForm.branchStationName.$invalid){
				$scope.alertToast("Please enter Branch Station Code");
			}else if(BranchForm.branchPhone.$invalid){
				$scope.alertToast("Please enter correct phone number between 4-15 digits....");
			}else if(BranchForm.branchFax.$invalid){
				$scope.alertToast("Please enter correct branch fax number between 8-20 digits....");
			}else if(BranchForm.branchEmailId.$invalid){
				$scope.alertToast("Please enter correct Email id.....");
			}else if(BranchForm.branchWebsite.$invalid){
				$scope.alertToast("Please enter website between 3-255 characters.....");
			}
		}else{ 
			
			 $scope.viewBranchDetailsFlag = false;
				$('div#viewBranchDetailsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "View Branch Details",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
				});
				$('div#viewBranchDetailsDB').dialog('open');
					
		 }
	}
	
	$scope.saveBranch = function(branch){
		console.log("Entered into saveBranch function---->");
		 $('#saveId').attr("disabled","disabled");
		var response = $http.post($scope.projectName+'/submitBranch', branch);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$('div#viewBranchDetailsDB').dialog('close');
				$scope.successToast(data.result);
				$('#saveId').removeAttr("disabled");
				$scope.branch = {};
										
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		});
	}
	
	$scope.closeViewBranchDetailsDB = function(){
		$('div#viewBranchDetailsDB').dialog('close');
	}
	
	$scope.saveBranchStationCode =  function(station){
		console.log("enter into saveBranchStationCode----->"+station.stnCode);
		$scope.brStationCode = station.stnName;
		$scope.branch.branchStationCode = station.stnCode;
		$('div#branchStationCodeDB').dialog('close');
		$scope.branchStationCodeDBFlag = true;
	}
	
	$scope.getStationCodeData = function(){
		console.log("getStationCodeData------>");
		var response = $http.post($scope.projectName+'/getStationCodeData');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.stationList = data.list;
				$scope.successToast(data.result);
				console.log("data from server-->"+$scope.stationList);
				//$scope.getListOfEmployee();
			}else{
				console.log(data);
			}	
		});
		response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		});
	}
	

	
	$scope.openStateGSTCodeDB = function(){

		if($scope.stateList == null || $scope.stateList == []){

			var response = $http.post($scope.projectName+'/getStateGSTList');
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.stateList = data.stateList;
					$scope.successToast(data.result);
					console.log("data from server-->"+$scope.stateList);
					
					$scope.stateCodeDBFlag = false;
					$('div#stateCodeDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						position: UDPos,
						show: UDShow,
						hide: UDHide,
						title: "State GST Code",
						draggable: true,
						close: function(event, ui) { 
				            $(this).dialog('destroy');
				            $(this).hide();
				        }
					});
				
					$('div#stateCodeDB').dialog('open');
					
					
				}else{
					console.log(data);
				}	
			});
			response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
			});
		
		}else{
			$scope.stateCodeDBFlag = false;
			$('div#stateCodeDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "State GST Code",
				draggable: true,
				close: function(event, ui) { 
		            $(this).dialog('destroy');
		            $(this).hide();
		        }
			});
		
			$('div#stateCodeDB').dialog('open');
			
		}	
  }

	$scope.saveStateCode =  function(state){
		console.log("enter into saveBranchStationCode----->"+state.stateGST);
		$scope.stateName = state.stateName;
		$scope.stateGST = state.stateGST;
		$scope.branch.branchStateGST = state.stateGST;
		$('div#stateCodeDB').dialog('close');
		$scope.stateCodeDBFlag = true;
	}

	
	
	
	$scope.getListOfEmployee = function(){
		   console.log(" entered into getListOfEmployee------>");
		   var response = $http.post($scope.projectName+'/getListOfEmp');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
				    $scope.employeeList = data.list;
				    $scope.successToast(data.result);
				}else{
					console.log(data);
				}
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
	   }
		
	$scope.saveBranchDirector =  function(empBranchDirector){
		console.log("enter into saveBranchDirector----->");
		$scope.temp.branchDirector = empBranchDirector.empName;
		$scope.branch.branchDirector = empBranchDirector.empCode;
		$('div#branchDirectorDB').dialog('close');
		$scope.branchDirectorDBFlag = true;	
	}
	
	$scope.saveBranchMarketingHD =  function(empBranchMarketingHD){
		console.log("enter into saveBranchMarketingHD----->");
		$scope.temp.branchMarketingHD = empBranchMarketingHD.empName;
		$scope.branch.branchMarketingHD = empBranchMarketingHD.empCode;
		$('div#branchMarketingHDDB').dialog('close');
		$scope.branchMarketingHDDBFlag = true;
	}
	
	$scope.saveBranchOutStandingHD =  function(empBranchOutStandingHD){
		console.log("enter into saveBranchOutStandingHD----->");
		$scope.temp.branchOutStandingHD = empBranchOutStandingHD.empName;
		$scope.branch.branchOutStandingHD = empBranchOutStandingHD.empCode;
		$('div#branchOutStandingHDDB').dialog('close');
		$scope.branchOutStandingHDDBFlag = true;
		
	}
	
	$scope.saveBranchMarketing =  function(empBranchMarketing){
		console.log("enter into saveBranchMarketing----->");
		$scope.temp.branchMarketing = empBranchMarketing.empName;
		$scope.branch.branchMarketing = empBranchMarketing.empCode;
		$('div#branchMarketingDB').dialog('close');
		$scope.branchMarketingDBFlag = true;
		
	}
	
	$scope.saveBranchMngr =  function(empBranchMngr){
		console.log("enter into saveBranchMngr----->");
		$scope.temp.branchMngr = empBranchMngr.empName;
		$scope.branch.branchMngr = empBranchMngr.empCode;
		$('div#branchMngrDB').dialog('close');
		$scope.branchMngrDBFlag = true;
		
	}
	
	$scope.saveBranchCashier =  function(empBranchCashier){
		console.log("enter into saveBranchCashier----->");
		$scope.temp.branchCashier = empBranchCashier.empName;
		$scope.branch.branchCashier = empBranchCashier.empCode;
		$('div#branchCashierDB').dialog('close');
		 $scope.branchCashierDBFlag = true;
		
	}
	
	$scope.saveBranchTraffic =  function(empBranchTraffic){
		console.log("enter into saveBranchTraffic----->");
		$scope.temp.branchTraffic = empBranchTraffic.empName;
		$scope.branch.branchTraffic = empBranchTraffic.empCode;
		$('div#branchTrafficDB').dialog('close');
		$scope.branchTrafficDBFlag = true;
		
	}
	
	$scope.saveBranchAreaMngr =  function(empBranchAreaMngr){
		console.log("enter into saveBranchAreaMngr----->");
		$scope.temp.branchAreaMngr = empBranchAreaMngr.empName;
		$scope.branch.branchAreaMngr = empBranchAreaMngr.empCode;
		$('div#branchAreaMngrDB').dialog('close');
		$scope.branchAreaMngrDBFlag = true;
		
	}
	
	
	$scope.saveBranchRegionalMngr =  function(empBranchRegionalMngr){
		console.log("enter into saveBranchRegionalMngr----->"); 
		$scope.temp.branchRegionalMngr = empBranchRegionalMngr.empName;
		$scope.branch.branchRegionalMngr = empBranchRegionalMngr.empCode;
		$('div#branchRegionalMngrDB').dialog('close');
		 $scope.branchRegionalMngrDBFlag = true;
		 
	}
	
	
	$scope.branchDirectorRadio = function(){
		console.log("enter into branchDirectorRadio() function");
		
		$scope.branchDirectorFlag = true;
		$scope.branchMarketingHDFlag = false;
		$scope.branchOutStandingHDFlag = false;
		$scope.branchMarketingFlag = false;
		$scope.branchMngrFlag = false;
		$scope.branchCashierFlag = false;
		$scope.branchTrafficFlag = false;
		$scope.branchAreaMngrFlag = false;
		$scope.branchRegionalMngrFlag = false;
		
		$scope.getBranchCode();
		
	}
		
	$scope.branchMarketingHDRadio = function(){
		console.log("enter into branchMarketingHDRadio() function");
		$scope.branchDirectorFlag = false;
		$scope.branchMarketingHDFlag = true;
		$scope.branchOutStandingHDFlag = false;
		$scope.branchMarketingFlag = false;
		$scope.branchMngrFlag = false;
		$scope.branchCashierFlag = false;
		$scope.branchTrafficFlag = false;
		$scope.branchAreaMngrFlag = false;
		$scope.branchRegionalMngrFlag = false;
		
		$scope.getBranchCode();
		
	}
	
	$scope.branchOutStandingHDRadio = function(){
		console.log("enter into branchOutStandingHDRadio() function");
		$scope.branchDirectorFlag = false;
		$scope.branchMarketingHDFlag = false;
		$scope.branchOutStandingHDFlag = true;
		$scope.branchMarketingFlag = false;
		$scope.branchMngrFlag = false;
		$scope.branchCashierFlag = false;
		$scope.branchTrafficFlag = false;
		$scope.branchAreaMngrFlag = false;
		$scope.branchRegionalMngrFlag = false;
		
		$scope.getBranchCode();
		
	}
	
	$scope.branchMarketingRadio = function(){
		console.log("enter into branchMarketingRadio() function");
		$scope.branchDirectorFlag = false;
		$scope.branchMarketingHDFlag = false;
		$scope.branchOutStandingHDFlag = false;
		$scope.branchMarketingFlag = true;
		$scope.branchMngrFlag = false;
		$scope.branchCashierFlag = false;
		$scope.branchTrafficFlag = false;
		$scope.branchAreaMngrFlag = false;
		$scope.branchRegionalMngrFlag = false;
		
		$scope.getBranchCode();
		
	}
	
	$scope.branchMngrRadio = function(){
		console.log("enter into branchMngrRadio() function");
		$scope.branchDirectorFlag = false;
		$scope.branchMarketingHDFlag = false;
		$scope.branchOutStandingHDFlag = false;
		$scope.branchMarketingFlag = false;
		$scope.branchMngrFlag = true;
		$scope.branchCashierFlag = false;
		$scope.branchTrafficFlag = false;
		$scope.branchAreaMngrFlag = false;
		$scope.branchRegionalMngrFlag = false;
		
		$scope.getBranchCode();
		
	}
	
	$scope.branchCashierRadio= function(){
		console.log("enter into branchCashierRadio() function");
		$scope.branchDirectorFlag = false;
		$scope.branchMarketingHDFlag = false;
		$scope.branchOutStandingHDFlag = false;
		$scope.branchMarketingFlag = false;
		$scope.branchMngrFlag = false;
		$scope.branchCashierFlag = true;
		$scope.branchTrafficFlag = false;
		$scope.branchAreaMngrFlag = false;
		$scope.branchRegionalMngrFlag = false;
		
		$scope.getBranchCode();
		
	}
	
	$scope.branchTrafficRadio = function(){
		console.log("enter into branchTrafficRadio() function");
		$scope.branchDirectorFlag = false;
		$scope.branchMarketingHDFlag = false;
		$scope.branchOutStandingHDFlag = false;
		$scope.branchMarketingFlag = false;
		$scope.branchMngrFlag = false;
		$scope.branchCashierFlag = false;
		$scope.branchTrafficFlag = true;
		$scope.branchAreaMngrFlag = false;
		$scope.branchRegionalMngrFlag = false;
		
		$scope.getBranchCode();
		
	}
	
	$scope.branchAreaMngrRadio = function(){
		console.log("enter into branchAreaMngrRadio() function");
		$scope.branchDirectorFlag = false;
		$scope.branchMarketingHDFlag = false;
		$scope.branchOutStandingHDFlag = false;
		$scope.branchMarketingFlag = false;
		$scope.branchMngrFlag = false;
		$scope.branchCashierFlag = false;
		$scope.branchTrafficFlag = false;
		$scope.branchAreaMngrFlag = true;
		$scope.branchRegionalMngrFlag = false;
		
		$scope.getBranchCode();
		
	}
	
	$scope.branchRegionalMngrRadio = function(){
		console.log("enter into branchRegionalMngrRadio() function");
		$scope.branchDirectorFlag = false;
		$scope.branchMarketingHDFlag = false;
		$scope.branchOutStandingHDFlag = false;
		$scope.branchMarketingFlag = false;
		$scope.branchMngrFlag = false;
		$scope.branchCashierFlag = false;
		$scope.branchTrafficFlag = false;
		$scope.branchAreaMngrFlag = false;
		$scope.branchRegionalMngrFlag = true;
		
		$scope.getBranchCode();
		
	}
	
	$scope.getBranchCode = function(){
		console.log("enter into getBranchCode function------->");
		
		if(angular.isUndefined($scope.branch.branchName) || $scope.branch.branchName === null || $scope.branch.branchName ===""){
			$scope.alertToast("Please enter branch name of minimum 3 characters.... ");
			$scope.clrBtnBranchDirectorNE=true;
			$scope.clrBtnbranchMarketingHDNE=true;
			$scope.clrBtnBranchOutStandingHDNE=true;
			$scope.clrBtnBranchMarketingNE=true;
			$scope.clrBtnBranchMngrNE=true;
			$scope.clrBtnBranchCashierNE=true;
			$scope.clrBtnBranchTrafficNE=true;
			$scope.clrBtnBranchAreaMngrNE=true;
			$scope.clrBtnBranchRegionalMngrNE=true;
		}
		else{
			$scope.openEmployeeDB();
			$scope.employee.branchCode=$scope.branchCodeOnBlur;				
		}
	}
	
	$scope.submitEmployee = function(EmployeeForm,employee){
			console.log("enter into submitEmployee function--->");
			if(EmployeeForm.$invalid){
				console.log("enter into if part of submit function--->");
				if(EmployeeForm.empFirstName.$invalid){
					$scope.alertToast("Please enter employee first name between 3-40 characters.....");
				}else if(EmployeeForm.empMiddleName.$invalid){
					$scope.alertToast("Please enter employee middle name between 3-40 characters.....");
				}else if(EmployeeForm.empLastName.$invalid){
					$scope.alertToast("Please enter employee last name between 3-40 characters.....");
				}else if(EmployeeForm.empAdd.$invalid){
					$scope.alertToast("Please enter employee address between 3-255 characters.....");
				}else if(EmployeeForm.empDesignation.$invalid){
					$scope.alertToast("Please enter employee designation between 2-40 characters.....");
				}else if(EmployeeForm.empCity.$invalid){
					$scope.alertToast("Please enter employee city between 3-40 characters.....");
				}else if(EmployeeForm.empState.$invalid){
					$scope.alertToast("Please enter employee state between 3-40 characters.....");
				}else if(EmployeeForm.empPin.$invalid){
					$scope.alertToast("Please enter PIN number of 6 digits.....");
				}else if(EmployeeForm.empPFNo.$invalid){
					$scope.alertToast("Please enter employee PF number between 8-18 characters...");
				}else if(EmployeeForm.empESINo.$invalid){
					$scope.alertToast("Please enter employee ESI number between 3-40 characters...");
				}else if(EmployeeForm.empESIDispensary.$invalid){
					$scope.alertToast("Please enter employee ESI dispensary between 3-40 characters...");
				}else if(EmployeeForm.empNomineeFirstName.$invalid){
					$scope.alertToast("Please enter employee nominee first name between 3-40 characters...");
				}else if(EmployeeForm.empNomineeLastName.$invalid){
					$scope.alertToast("Please enter employee nominee last name between 3-40 characters...");
				}else if(EmployeeForm.empEduQuali.$invalid){
					$scope.alertToast("Please enter employee educational qualification between 2-255 characters...");
				}else if(EmployeeForm.empPresentAdd.$invalid){
					$scope.alertToast("Please enter present address of employee between 3-255 characters.....");
				}else if(EmployeeForm.empPresentCity.$invalid){
					$scope.alertToast("Please enter present city of employee between 3-40 characters.....");
				}else if(EmployeeForm.empPresentState.$invalid){
					$scope.alertToast("Please enter present state of employee between 3-40 characters.....");
				}else if(EmployeeForm.empPresentPin.$invalid){
					$scope.alertToast("Please enter present PIN number of employee of 6 digits...");
				}else if(EmployeeForm.empSex.$invalid){
					$scope.alertToast("Please enter employeee sex...");
				}else if(EmployeeForm.empFatherFirstName.$invalid){
					$scope.alertToast("Please enter father's first name of employee between 3-40 characters...");
				}else if(EmployeeForm.empFatherLastName.$invalid){
					$scope.alertToast("Please enter father's last name of employee between 3-40 characters...");
				}else if(EmployeeForm.empLicenseNo.$invalid){
					$scope.alertToast("Please enter employee licence number between 3-40 characters...");
				}else if(EmployeeForm.empBankAcNo.$invalid){
					$scope.alertToast("Please enter correct format of Bank Account number between 10-40 characters...");
				}else if(EmployeeForm.empPanNo.$invalid){
					$scope.alertToast("Please enter correct format of PAN number of 10 characters...");
				}else if(EmployeeForm.empMailId.$invalid){
					$scope.alertToast("Please enter correct format of email id...");
				}else if(EmployeeForm.empPhNo.$invalid){
					$scope.alertToast("Please enter phone number of 4-15 digits....");
				}else if(EmployeeForm.empBankIFS.$invalid){
					$scope.alertToast("Please enter employee Bank IFSC between 3-40 characters....");
				}else if(EmployeeForm.empBankName.$invalid){
					$scope.alertToast("Please employee bank name between 3-40 characters....");
				}else if(EmployeeForm.empBankBranch.$invalid){
					$scope.alertToast("Please employee bank branch between 3-40 characters....");
				}
			}else{
				if($scope.employee.empDOB>$scope.employee.empDOAppointment){
					$scope.alertToast("Please enter valid date of appointment...");
				}else{
					
					if(angular.isUndefined(employee.empmiddlename) || employee.empmiddlename === null || employee.empmiddlename === ""){
						$scope.employee.empName = employee.empfirstname+" "+employee.emplastname;
						employee.empName=employee.empName.toUpperCase();
					}else if(!(angular.isUndefined(employee.empmiddlename)) || !(employee.empmiddlename === null) || !(employee.empmiddlename === "")){
						$scope.employee.empName = employee.empfirstname+" "+employee.empmiddlename+" "+employee.emplastname;
						employee.empName=employee.empName.toUpperCase();
					}
					
					$scope.employee.empFatherName = employee.fatherFirstName+" "+employee.fatherLastName; 
					$scope.employee.empNominee = employee.nomineeFirstName+" "+employee.nomineeLastName;
					
					$scope.viewEmpDetailsFlag = false;
					$('div#viewEmpDetailsDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						position: UDPos,
						show: UDShow,
						hide: UDHide,
						title: "View Employee Details",
						draggable: true,
						close: function(event, ui) { 
							$(this).dialog('destroy');
							$(this).hide();
						}
					});
					$('div#viewEmpDetailsDB').dialog('open');
				}
			}
	}
	
	$scope.saveEmployee = function(employee){
		console.log("Entered into saveEmployee of BranchCntlr.js---" +employee);
		
		var response = $http.post($scope.projectName+'/submitEmployeeInBranch', employee);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$('div#viewEmpDetailsDB').dialog('close');
			    console.log("employee name is --->>"+data.empName)
				$scope.currentEmployeeName = data.empName;
				$scope.currentEmployeeCode = data.empCode;
				$scope.successToast(data.result);
				console.log("message from server --->"+data.result);
				//GLOBAL FUNCTION
				$scope.globalForBranch();
				$scope.closeEmployeeDB();
				$scope.getListOfEmployee();
			}else if(data.result === "error"){
				console.log(data.result);
			}
	     });
		 response.error(function(data, status, headers, config) {
			 $scope.errorToast(data);
	     });
	}
	

	$scope.closeViewEmpDetailsDB = function(){
		$('div#viewEmpDetailsDB').dialog('close');
	}
	
	
	$scope.globalForBranch = function(){
		console.log("enter into globalForBranch function");
		if($scope.branchDirectorFlag){
			console.log("$scope.branchDirectorFlag ---- >"+$scope.branchDirectorFlag);
			$scope.temp.branchDirector = $scope.currentEmployeeName;
			$scope.branch.branchDirector = $scope.currentEmployeeCode;
			$scope.currentEmployeeName = "";
			$scope.currentEmployeeCode = "";
			$('input[name=branchDirector]').attr('disabled','disabled');
		}else if($scope.branchMarketingHDFlag){
			console.log("$scope.branchMarketingHDFlag ---- >"+$scope.branchMarketingHDFlag);
			$scope.temp.branchMarketingHD = $scope.currentEmployeeName;
			$scope.branch.branchMarketingHD = $scope.currentEmployeeCode;
			$scope.currentEmployeeName = "";
			$scope.currentEmployeeCode = "";
			$('input[name=branchMarketingHD]').attr('disabled','disabled');
		}else if($scope.branchOutStandingHDFlag){
			console.log("$scope.branchOutStandingHDFlag ---- >"+$scope.branchOutStandingHDFlag);
			$scope.temp.branchOutStandingHD = $scope.currentEmployeeName;
			$scope.branch.branchOutStandingHD = $scope.currentEmployeeCode;
			$scope.currentEmployeeName = "";
			$scope.currentEmployeeCode = "";
			$('input[name=branchOutStandingHD]').attr('disabled','disabled');
		}else if($scope.branchMarketingFlag){
			console.log("$scope.branchMarketingFlag ---- >"+$scope.branchMarketingFlag);
			$scope.temp.branchMarketing = $scope.currentEmployeeName;
			$scope.branch.branchMarketing = $scope.currentEmployeeCode;
			$scope.currentEmployeeName = "";
			$scope.currentEmployeeCode = "";
			$('input[name=branchMarketing]').attr('disabled','disabled');
		}else if($scope.branchMngrFlag){
			console.log("$scope.branchMngrFlag ---- >"+$scope.branchMngrFlag);
			$scope.temp.branchMngr = $scope.currentEmployeeName;
			$scope.branch.branchMngr = $scope.currentEmployeeCode;
			$scope.currentEmployeeName = "";
			$scope.currentEmployeeCode = "";
			$('input[name=branchMngr]').attr('disabled','disabled');
		}else if($scope.branchCashierFlag){
			console.log("$scope.branchCashierFlag ---- >"+$scope.branchCashierFlag);
			$scope.temp.branchCashier = $scope.currentEmployeeName;
			$scope.branch.branchCashier = $scope.currentEmployeeCode;
			$scope.currentEmployeeName = "";
			$scope.currentEmployeeCode = "";
			$('input[name=branchCashier]').attr('disabled','disabled');
		}else if($scope.branchTrafficFlag){
			console.log("$scope.branchTrafficFlag ---- >"+$scope.branchTrafficFlag);
			$scope.temp.branchTraffic = $scope.currentEmployeeName;
			$scope.branch.branchTraffic = $scope.currentEmployeeCode;
			$scope.currentEmployeeName = "";
			$scope.currentEmployeeCode = "";
			$('input[name=branchTraffic]').attr('disabled','disabled');
		}else if($scope.branchAreaMngrFlag){
			console.log("$scope.branchAreaMngrFlag ---- >"+$scope.branchAreaMngrFlag);
			$scope.temp.branchAreaMngr = $scope.currentEmployeeName;
			$scope.branch.branchAreaMngr = $scope.currentEmployeeCode;
			$scope.currentEmployeeName = "";
			$scope.currentEmployeeCode = "";
			$('input[name=branchAreaMngr]').attr('disabled','disabled');
		}else if($scope.branchRegionalMngrFlag){
			console.log("$scope.branchRegionalMngr ---- >"+$scope.branchRegionalMngrFlag);
			$scope.temp.branchRegionalMngr = $scope.currentEmployeeName;
			$scope.branch.branchRegionalMngr = $scope.currentEmployeeCode;
			$scope.currentEmployeeName = "";
			$scope.currentEmployeeCode = "";
			$('input[name=branchRegionalMngr]').attr('disabled','disabled');
		}	
	}
		
	$scope.openEmployeeDB=function()
	{
		    $scope.EmployeeDBFlag = false;
		    $('div#EmployeeDB').dialog({
    		autoOpen: false,
    		modal:true,
    		resizable: true,
    		position: UDPos,
    		show: UDShow,
    		hide: UDHide,
    		title: "Employee Form",
    		draggable: true,
    		close: function(event, ui) { 
    			$(this).dialog('destroy');
    			$(this).hide();
    	    }
    	});
    			
    	$('div#EmployeeDB').dialog('open');
  }
	
	$scope.closeEmployeeDB = function(){
		
		$scope.employee.userCode = "";
		$scope.employee.isInNCR="";
		$scope.employee.empCode = "";
		//$scope.employee.empName = "";
		$scope.employee.empfirstname="";
		$scope.employee.empmiddlename="";
		$scope.employee.emplastname="";
		$scope.employee.empAdd = "";
		$scope.employee.empCity = "";
		$scope.employee.empState = "";
		$scope.employee.empPin = "";
		$scope.employee.empDOB = "";
		$scope.employee.empDOAppointment = "";
		$scope.employee.empDesignation = "";
		$scope.employee.empBasic = "";
		$scope.employee.empHRA = "";
		$scope.employee.empOtherAllowance = 0;
		$scope.employee.empPFNo = "";
		$scope.employee.empESINo = "";
		$scope.employee.empESIDispensary = "";
		//$scope.employee.empNominee = "";
		$scope.employee.nomineeFirstName="";
		$scope.employee.nomineeLastName="";
		$scope.employee.empEduQuali = "";
		$scope.employee.empPF = "";
		$scope.employee.empESI = "";
		$scope.employee.empLoanBal = "";
		$scope.employee.empLoanPayment = "";
		$scope.employee.empPresentAdd = "";
		$scope.employee.empPresentCity = "";
		$scope.employee.empPresentState = "";
		$scope.employee.empPresentPin = "";
		$scope.employee.empSex = "";
		//$scope.employee.empFatherName = "";
		$scope.employee.fatherFirstName="";
		$scope.employee.fatherLastName="";
		$scope.employee.empLicenseNo = "";
		$scope.employee.empLicenseExp = "";
		$scope.employee.empTelephoneAmt = "";
		$scope.employee.empMobAmt = "";
		$scope.employee.empBankAcNo = "";
		$scope.employee.empPanNo = "";
		$scope.employee.empMailId = "";
		$scope.employee.empPhNo = "";
		$scope.employee.empBankIFS = "";
		$scope.employee.empBankName = "";
		$scope.employee.empBankBranch = "";
		$scope.employee.empGross="";
		$scope.employee.netSalary="";
		$('div#EmployeeDB').dialog('close');
	}
	
	$scope.saveisNCR = function(){
        if($scope.checkIsNCR){
            $scope.branch.isNCR="yes";
        }else{
        	$scope.branch.isNCR="no";
        }  
    }
	
	$scope.generateBranchCode = function(BranchForm){
		console.log("enter into generateBranchCode ---->BranchForm.branchName.$invalid="+BranchForm.branchName.$invalid)
		if(BranchForm.branchName.$invalid){
			$scope.alertToast("please enter branch name of minimum 3 characters...");
		}else{
		   var response = $http.post($scope.projectName+'/getBranchCode', $scope.branch.branchName);
		   response.success(function(data, status, headers, config) {
			   if(data.result==="success"){
				   console.log("Branch code is generated ----------->"+data.branchCode);
				   $scope.branchCodeOnBlur=data.branchCode;
				   $scope.branch.branchCode=data.branchCode;
				   $scope.branch.branchCodeTemp = data.branchCodeTemp;
				   $scope.employee.branchCode=data.branchCode;
				}else{
						console.log(data);
				}
		     });
			 response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
			 });
		}
	}
	
	$scope.openBranchDirectorDB = function(){
		 $scope.branchDirectorDBFlag = false;
			$('div#branchDirectorDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchDirectorDB').dialog('open');
	}
	
	$scope.openBranchMarketingHDDB = function(){
		 $scope.branchMarketingHDDBFlag = false;
			$('div#branchMarketingHDDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchMarketingHDDB').dialog('open');
	}
	
	$scope.openBranchOutStandingHDDB = function(){
		 $scope.branchOutStandingHDDBFlag = false;
			$('div#branchOutStandingHDDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchOutStandingHDDB').dialog('open');
	}
	
	$scope.openBranchMarketingDB = function(){
		 $scope.branchMarketingDBFlag = false;
			$('div#branchMarketingDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchMarketingDB').dialog('open');
	}
	
	$scope.openBranchMngrDB = function(){
		 $scope.branchMngrDBFlag = false;
			$('div#branchMngrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchMngrDB').dialog('open');
	}
	
	$scope.openBranchCashierDB = function(){
		 $scope.branchCashierDBFlag = false;
			$('div#branchCashierDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchCashierDB').dialog('open');
	}
	
	$scope.openBranchTrafficDB = function(){
		 $scope.branchTrafficDBFlag = false;
			$('div#branchTrafficDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchTrafficDB').dialog('open');
	}
	
	$scope.openBranchAreaMngrDB = function(){
		 $scope.branchAreaMngrDBFlag = false;
			$('div#branchAreaMngrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchAreaMngrDB').dialog('open');
	}
	
	$scope.openBranchRegionalMngrDB = function(){
		 $scope.branchRegionalMngrDBFlag = false;
			$('div#branchRegionalMngrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee Name",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
			});
			
			$('div#branchRegionalMngrDB').dialog('open');
	}
	
	$scope.openBranchStationCodeDB = function(){
			$scope.branchStationCodeDBFlag = false;
			$('div#branchStationCodeDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Station Code",
				draggable: true,
				close: function(event, ui) { 
		            $(this).dialog('destroy');
		            $(this).hide();
		        }
			});
		
			$('div#branchStationCodeDB').dialog('open');
	}
	
	$scope.calculateOnBasic = function(){	   
		   
		   basic=$scope.employee.empBasic;
		   $scope.employee.isInNCR =  $scope.branch.isNCR;
		   if($scope.employee.isInNCR==="yes"){
			   hra=0.5*basic; 
		   }else if($scope.employee.isInNCR==="no"){
			   hra=0.4*basic;
		   }
		   	  
		   pf=0.24*basic;
		   gross=parseInt(basic)+parseInt(hra)+parseInt(pf)+parseInt(otherallowance);
		  
		   $scope.employee.empHRA=hra;
		   $scope.employee.empPF=pf;
		   $scope.employee.empGross=gross;
		   
		   if(gross<=14999)
		   {
		    esi=0.065*gross;
		    netsalary=((parseInt(basic)+parseInt(hra)+parseInt(otherallowance))-((parseInt(basic)+parseInt(hra)+parseInt(otherallowance))*0.0175+0.12*parseInt(basic)));
		    $scope.employee.empESI=esi;
		    $("#empESI").removeAttr('disabled');
		    $scope.employee.netSalary=netsalary;
		   } 
		   else if(gross>=15000){
			   $scope.employee.empESI="";
			   $("#empESI").attr('disabled','disabled');
			   netsalary=(parseInt(basic)+parseInt(hra)+parseInt(otherallowance))-(0.12*parseInt(basic));
			   $scope.employee.netSalary=netsalary;
			   
		   }
	}
	$scope.calculateOnOthers = function(value){
		   if(angular.isUndefined(value) || value === null || value ===""){
			   otherallowance =0;
		   }else{
			   otherallowance = parseInt(value);
	       }
		   $scope.calculateOnBasic();
	}
	 
	 $('#empPin').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });


	   $('#empPin').keypress(function(e) {
		   if (this.value.length == 6) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPresentPin').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });


	   $('#empPresentPin').keypress(function(e) {
	       if (this.value.length == 6) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empBasic').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });


	   $('#empBasic').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	   
	   $('#empLoanBal').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });


	   $('#empLoanBal').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	   
	   $('#empLoanPayment').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });


	   $('#empLoanPayment').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	     
	   $('#empTelephoneAmt').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });


	   $('#empTelephoneAmt').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	   
	   $('#empMobAmt').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });


	   $('#empMobAmt').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPhNo').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });


	   $('#empPhNo').keypress(function(e) {
	       if (this.value.length == 15) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empBankAcNo').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });


	   $('#empBankAcNo').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empOtherAllowance').keypress(function(key) {
	       if(key.charCode < 46 || key.charCode > 57)
	           return false;
	   });


	   $('#empOtherAllowance').keypress(function(e) {
		   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPanNo').keypress(function(e) {
	       if (this.value.length == 10) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empFirstName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empMiddleName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empLastName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empDesignation').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empEduQualification').keypress(function(e) {
	       if (this.value.length == 255) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empFatherFirstName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empFatherLastName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empNomineeFirstName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empNomineeLastName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	     
	   $('#empAddress').keypress(function(e) {
	       if (this.value.length == 255) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empCity').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empState').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPresentAdd').keypress(function(e) {
	       if (this.value.length == 255) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPresentCity').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPresentState').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empESINo').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empESIDispensary').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empPFNo').keypress(function(e) {
	       if (this.value.length == 18) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empLicenseNo').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empMailId').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empBankName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empBankBranch').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#empBankIFS').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#branchname').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#branchAddress').keypress(function(e) {
	       if (this.value.length == 255) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#branchcity').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#branchstate').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#branchEmailId').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#branchWebsite').keypress(function(e) {
	       if (this.value.length == 255) {
	           e.preventDefault();
	       }
	   });

	   
	      
	   $('#branchPhone').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });


	   $('#branchPhone').keypress(function(e) {
	       if (this.value.length == 15) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#branchFax').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });


	   $('#branchFax').keypress(function(e) {
	       if (this.value.length == 20) {
	           e.preventDefault();
	       }
	   });
	   
	   $('#branchpin').keypress(function(key) {
	       if(key.charCode < 48 || key.charCode > 57)
	           return false;
	   });


	   $('#branchpin').keypress(function(e) {
	       if (this.value.length == 6) {
	           e.preventDefault();
	       }
	   });
	   
	 $scope.refreshNewEmpList = function(){
		 console.log("enter into refreshNewEmpList function");
		 var response = $http.post($scope.projectName+'/refreshNewEmpList');
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$('input[name=branchDirector]').removeAttr('disabled');
					$('input[name=branchMarketingHD]').removeAttr('disabled');
					$('input[name=branchOutStandingHD]').removeAttr('disabled');
					$('input[name=branchMarketing]').removeAttr('disabled');
					$('input[name=branchMngr]').removeAttr('disabled');
					$('input[name=branchCashier]').removeAttr('disabled');
					$('input[name=branchTraffic]').removeAttr('disabled');
					$('input[name=branchAreaMngr]').removeAttr('disabled');
					$('input[name=branchRegionalMngr]').removeAttr('disabled');
					$scope.successToast(data.result);
					$scope.getStationCodeData();
				}else{
					console.log(data);
				}	
			});
			response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
			});
	} 
	 
	 $scope.chkPresentAddrDB = function(){
		 console.log("Entered into chkPresentAddrDB function----");
		   if(angular.isUndefined($scope.employee.empAdd) || $scope.employee.empAdd === null || $scope.employee.empAdd === ""){
			   $scope.alertToast("Please enter employee address");
			   $scope.employee.empPin="";
		   }else if(angular.isUndefined($scope.employee.empCity) || $scope.employee.empCity === null || $scope.employee.empCity === ""){
			   $scope.alertToast("Please enter employee city");
			   $scope.employee.empPin="";
		   }else if(angular.isUndefined($scope.employee.empState) || $scope.employee.empState === null || $scope.employee.empState === ""){
			   $scope.alertToast("Please enter employee state");
			   $scope.employee.empPin="";
		   }else if(angular.isUndefined($scope.employee.empPin) || $scope.employee.empPin === null || $scope.employee.empPin === ""){
			   $scope.alertToast("Please enter employee pin");
		   }else{
			   $scope.presentAddrDBFlag = false;
			   console.log("Entered to open the dialog box to check present address");
				$('div#presentAddrDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Employee Present Address",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
				});
				$('div#presentAddrDB').dialog('open');
		   }
	  }
	  
	  $scope.yesPresentAddr = function(){
		  console.log("Entered into yesPresentAddr function");
		  $scope.employee.empPresentAdd = $scope.employee.empAdd;
		  $scope.employee.empPresentCity = $scope.employee.empCity;
		  $scope.employee.empPresentState = $scope.employee.empState;
		  $scope.employee.empPresentPin = $scope.employee.empPin;
		  $('div#presentAddrDB').dialog('close');
		  $scope.presentAddrDBFlag = true;
	  }
	  
	  $scope.YesNoAddr = function(){
		  console.log("Entered into YesNoAddr function");
		  $('div#presentAddrDB').dialog('close');
		  $scope.presentAddrDBFlag = true;
	  }
	   
	  
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 //$scope.refreshNewEmpList();
		 $scope.getStationCodeData();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
	 	
}]);
'use strict';

var app = angular.module('application');

app.controller('LhpvBalBackCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){

	$scope.vs = {};
	$scope.lhpvBal = {};
	$scope.bnkList = [];
	$scope.chlnList = [];
	$scope.actBOList = [];
	$scope.lhpvBalList = [];
	
	$scope.bankCodeDBFlag = true;
	$scope.chqNoDBFlag = true;
	$scope.brkOwnDBFlag = true;
	$scope.chlnDBFlag = true;
	$scope.lhpvBalDBFlag = true;
	$scope.saveVsFlag = true;
	
	$scope.arHoAlw = false;
	
	$scope.tdsPerFlag = false;
	$scope.lodingFlag = false;
	//$scope.Math = window.Math;
	
	var faCodeTemp = '';
	
	$scope.getLhpvDet = function(){
		console.log("enter into getLhpvDet function");
		var response = $http.post($scope.projectName+'/getLhpvBalDetBack');
		  response.success(function(data, status, headers, config){
			   if(data.result === "success"){
				   $scope.vs.branch = data.branch;
				   $scope.vs.lhpvStatus = data.lhpvStatus;
				   $scope.vs.voucherType = $scope.LHPV_BAL_VOUCHER;
				   $scope.dateTemp = $filter('date')(data.lhpvStatus.lsDt, "yyyy-MM-dd'");
				   $scope.bnkList = data.bnkList;
				   
				   if($scope.vs.lhpvStatus.lsClose === true){
					   $scope.alertToast("You already close the LHPV of "+$scope.dateTemp);
					   $('#verfChln').attr("disabled","disabled");
				   }else{
					   //$scope.getChlnAndBrOwn();
				   }
			   }else{
				   console.log("error in fetching getLhpvDet data");
				   $scope.alertToast("Server Error");
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	$scope.getChlnAndBrOwn = function(){
		console.log("enter into getChlnAndBrOwn function");
		var response = $http.post($scope.projectName+'/getChlnAndBrOwnLB');
		  response.success(function(data, status, headers, config){
			   if(data.result === "success"){
				   $scope.chlnList = data.chlnList;
				   $scope.actBOList = data.actBOList;
			   }else{
				   console.log("error in fetching getChlnAndBrOwn data");
				   $scope.alertToast("No Challan avaliable for LHPV BALANCE");
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	$scope.selectPayBy = function(){
		console.log("enter into selectPayMod function with vs.payBy ="+$scope.vs.payBy);
		if($scope.vs.payBy === 'C'){
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
			$scope.vs.bankCode = "";
			$scope.stafCodeFlag=false;
			 $scope.stfCodeRqr=false;
			 $scope.chqNoRqr=false;
			 $scope.vs.stafCode="";
			 $scope.vs.payToStf="";
			$('#bankCodeId').attr("disabled","disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
		}else if($scope.vs.payBy === 'Q'){
			$('#chequeTypeId').removeAttr("disabled");
			$scope.vs.bankCode = "";
			$scope.stafCodeFlag=false;
			 $scope.stfCodeRqr=false;
			 $scope.chqNoRqr=true;
			 $scope.vs.stafCode="";
			 $scope.vs.payToStf="";
			$('#bankCodeId').attr("disabled","disabled");
		}else if($scope.vs.payBy === 'R'){
			$('#bankCodeId').removeAttr("disabled");
			$scope.vs.payToStf='B';
			$scope.vs.stafCode="";
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
			$scope.vs.chequeType = '';
			$scope.chqNoRqr=false;
			$('#chequeTypeId').attr("disabled","disabled");
		}else{
			console.log("invaid entry");
		}
	}
	
	
	$scope.selectChqType = function(){
		console.log("enter into selectChqType function");
		$('#bankCodeId').removeAttr("disabled");
	}
	
	
	$scope.openBankCodeDB = function(){
		console.log("enter into openBankCodeDB function");
		$scope.bankCodeDBFlag = false;
		$('div#bankCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Bank Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.bankCodeDBFlag = true;
		    }
			});
		
		$('div#bankCodeDB').dialog('open');	
	}
	
	
	
	$scope.saveBankCode =  function(bankCode){
		console.log("enter into saveBankCode----->"+bankCode);
		$scope.vs.bankCode = bankCode;
		$('div#bankCodeDB').dialog('close');
		$scope.bankCodeDBFlag = true;
			
		if($scope.vs.payBy === 'Q'){
			if(angular.isUndefined($scope.vs.chequeType) ||  $scope.vs.chequeType === null || $scope.vs.chequeType === ""){
				$scope.alertToast("please select cheque type");
			}else{
				var chqDet = {
						"bankCode" : bankCode,
						"CType"    : $scope.vs.chequeType
				};
				
				var response = $http.post($scope.projectName+'/getChequeNoFrLHB',chqDet);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						console.log("Cheque Leaves List--->>"+ data.chequeLeaves);
						$('#chequeNoId').removeAttr("disabled");
						$scope.vs.chequeLeaves = data.chequeLeaves;
						$scope.chqList = data.list;
						$scope.maxChqAmnt = data.chequeLeaves.chqLChqMaxLt;
					}else{
						$scope.alertToast("Branch does not have '"+$scope.vs.chequeType+"' type cheeque of Bank "+bankCode);
						console.log("Error in bringing data from getChequeNo");
					}
				});
				response.error(function(data, status, headers, config){
					console.log(data);
				});
			}
			
		}
		
 }
	
	
	
	$scope.openChqNoDB = function(){
		 console.log("enter into openChqNoDB function");
		 $scope.chqNoDBFlag = false;
	    	$('div#chqNoDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.chqNoDBFlag = true;
			    }
				});
			$('div#chqNoDB').dialog('open');
	 }
	
	
	$scope.saveChqNo = function(chq){
		 console.log("enter into saveChqNo function = "+chq.chqLChqNo);
		 $scope.vs.chequeLeaves = chq;
		 $('div#chqNoDB').dialog('close');
	 } 
	
	
	
	
	
	$scope.openChlnDB = function(){
		 console.log("enter into openChlnDB funciton");
		 if(angular.isUndefined($scope.vs.brkOwnFaCode) || $scope.vs.brkOwnFaCode === "" || $scope.vs.brkOwnFaCode === null){
			 $scope.alertToast("Please select Owner/Broker");
		 }else{
			 $scope.chlnDBFlag = false;
		    	$('div#chlnDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					Title: "Select Challan Code",
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.chlnDBFlag = true;
				    }
					});
				$('div#chlnDB').dialog('open'); 
		 }
	 }
	
	
	$scope.openBrkOwnDB = function(){
		 console.log("enter into openBrkOwnDB funciton");
		 $scope.brkOwnDBFlag = false;
	    	$('div#brkOwnDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.brkOwnDBFlag = true;
			    }
				});
			$('div#brkOwnDB').dialog('open'); 
	 }
	
	$scope.saveBOCode = function(faCode){
		console.log("enter into saveBOCode function");
		$scope.lbFinalTotSum=0;
		$scope.vs.brkOwnFaCode = faCode;
		//$('div#brkOwnDB').dialog('close'); 
		$scope.lhpvBalList = [];
	 }
	
	
	
	 $scope.saveChln = function(chlnCode){
		 console.log("enter into saveChln fucniton");
		 $scope.challan = chlnCode;
		 
		 var duplicate = false;
		 if($scope.lhpvBalList.length > 0){
			 for(var i=0;i<$scope.lhpvBalList.length;i++){
				if($scope.lhpvBalList[i].challan.chlnCode === chlnCode){
					duplicate = true;
					break;
				}
			 }
		 }
		 
		 if(duplicate === false){
			 var data = {
					 "chlnCode"     : $scope.challan,
					 "brkOwnFaCode" : $scope.vs.brkOwnFaCode
			 };
			 
			 var response = $http.post($scope.projectName+'/getChlnFrLB',data);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						
						if ($scope.vs.branch.branchFaCode === '0100023') {
							//$('div#chlnDB').dialog('close'); 
							console.log(data);
							//mohsin
							$scope.actOthExtKmP  = data.exKm;
							$scope.actOthOvrHgtP = data.ovrHgt;
							$scope.actUnpDetP    = data.det;
							$scope.actUnLoadingP = data.unLdg;
							
							console.log("data.arHoAlw === "+data.arHoAlw);
							console.log("data.munsAmt = "+data.munsAmt);
							$scope.actChln = data.chln;
							$scope.lhpvBal.challan = $scope.actChln;
							$scope.lhpvBal.lbLryBalP    = data.chlnBal;
							
							$scope.actBal = data.chlnBal;
							//$scope.lhpvBal.lbCashDiscR  = $scope.lhpvBal.lbLryBalP * 0.01;
							if(data.isCal === true){
								$scope.lhpvBal.lbCashDiscR = data.csDis;
								$scope.lhpvBal.lbMunsR      = data.munsAmt;
								//$scope.lhpvBal.lbTdsR       = data.tdsAmt;
								$scope.lhpvBal.lbTdsR       = window.Math.round(data.tdsAmt);
							}else{
								$scope.lhpvBal.lbCashDiscR  = 0;
								$scope.lhpvBal.lbMunsR      = 0;
								$scope.lhpvBal.lbTdsR       = 0;
							}
							
							//lhpv tds calculation
							$scope.tdsPer = data.tdsPer;
							if ($scope.tdsPer == 1) {
								console.log("tdsPer if: "+$scope.tdsPer);
								$scope.tdsPerFlag = true;
							} else {
								console.log("tdsPer else: "+$scope.tdsPer);
								$scope.tdsPerFlag = false;
							}
							
							$scope.actCashDisR = $scope.lhpvBal.lbCashDiscR;
							$scope.actMunsR = $scope.lhpvBal.lbMunsR;
							$scope.actTdsR = $scope.lhpvBal.lbTdsR;
							
							//if(data.arHoAlw === true){
								
								$scope.lhpvBal.lbWtShrtgCR  = 0;
								$scope.lhpvBal.lbDrRcvrWtCR = 0;
								$scope.lhpvBal.lbLateDelCR  = 0;
								$scope.lhpvBal.lbLateAckCR  = 0;
								$scope.lhpvBal.lbOthExtKmP  = 0;
								$scope.lhpvBal.lbOthOvrHgtP = 0;
								$scope.lhpvBal.lbOthPnltyP  = 0;
								$scope.lhpvBal.lbOthMiscP   = 0;
								$scope.lhpvBal.lbUnpDetP    = 0;
								$scope.lhpvBal.lbUnLoadingP = 0;
							
								
								
								$scope.actWtShrtgCR = $scope.lhpvBal.lbWtShrtgCR;
								$scope.actDrRcvrWtCR = $scope.lhpvBal.lbDrRcvrWtCR;
								
								/*$scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP + $scope.lhpvBal.lbOthExtKmP + $scope.lhpvBal.lbOthOvrHgtP 
																+ $scope.lhpvBal.lbOthPnltyP + $scope.lhpvBal.lbOthMiscP + $scope.lhpvBal.lbUnpDetP
																	+ $scope.lhpvBal.lbUnLoadingP;*/
								
								$scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP + $scope.lhpvBal.lbOthExtKmP + $scope.lhpvBal.lbOthOvrHgtP 
																+ $scope.lhpvBal.lbOthPnltyP + $scope.lhpvBal.lbUnpDetP + $scope.lhpvBal.lbUnLoadingP;
		
								$scope.lhpvBal.lbTotRcvrAmt = $scope.lhpvBal.lbCashDiscR + $scope.lhpvBal.lbMunsR + $scope.lhpvBal.lbTdsR 
																+ $scope.lhpvBal.lbWtShrtgCR + $scope.lhpvBal.lbDrRcvrWtCR + $scope.lhpvBal.lbLateDelCR 
																+ $scope.lhpvBal.lbLateAckCR + $scope.lhpvBal.lbOthMiscP;
		
		
								$scope.lhpvBal.lbFinalTot = $scope.lhpvBal.lbTotPayAmt - $scope.lhpvBal.lbTotRcvrAmt;
								
							/*}else{
								
								$scope.arHoAlw = false;
								$scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP;
								$scope.lhpvBal.lbTotRcvrAmt = $scope.lhpvBal.lbCashDiscR + $scope.lhpvBal.lbMunsR + $scope.lhpvBal.lbTdsR;
								$scope.lhpvBal.lbFinalTot = $scope.lhpvBal.lbTotPayAmt - $scope.lhpvBal.lbTotRcvrAmt;
							}*/
						} else {
							//$('div#chlnDB').dialog('close'); 
							console.log(data);
							//mohsin
							$scope.actOthExtKmP  = data.exKm;
							$scope.actOthOvrHgtP = data.ovrHgt;
							$scope.actUnpDetP    = data.det;
							$scope.actUnLoadingP = data.unLdg;
							
							console.log("data.arHoAlw === "+data.arHoAlw);
							console.log("data.munsAmt = "+data.munsAmt);
							$scope.actChln = data.chln;
							$scope.lhpvBal.challan = $scope.actChln;
							$scope.lhpvBal.lbLryBalP    = data.chlnBal;
							
							$scope.actBal = data.chlnBal;
							//$scope.lhpvBal.lbCashDiscR  = $scope.lhpvBal.lbLryBalP * 0.01;
							if(data.isCal === true){
								$scope.lhpvBal.lbCashDiscR = data.csDis;
								$scope.lhpvBal.lbMunsR      = data.munsAmt;
								//$scope.lhpvBal.lbTdsR       = data.tdsAmt;
								$scope.lhpvBal.lbTdsR       = window.Math.round(data.tdsAmt);
							}else{
								$scope.lhpvBal.lbCashDiscR  = 0;
								$scope.lhpvBal.lbMunsR      = 0;
								$scope.lhpvBal.lbTdsR       = 0;
							}
							
							//lhpv tds calculation
							$scope.tdsPer = data.tdsPer;
							if ($scope.tdsPer == 1) {
								console.log("tdsPer if: "+$scope.tdsPer);
								$scope.tdsPerFlag = true;
							} else {
								console.log("tdsPer else: "+$scope.tdsPer);
								$scope.tdsPerFlag = false;
							}
							
							$scope.actCashDisR = $scope.lhpvBal.lbCashDiscR;
							$scope.actMunsR = $scope.lhpvBal.lbMunsR;
							$scope.actTdsR = $scope.lhpvBal.lbTdsR;
							
							if(data.arHoAlw === true){
								
								$scope.arHoAlw = true;
								
								if(data.isCal === true){
									$scope.lhpvBal.lbWtShrtgCR  = data.wtShrtg;
									$scope.lhpvBal.lbDrRcvrWtCR = data.drRcvrWt;
									$scope.lhpvBal.lbLateDelCR  = data.ltDel;
									$scope.lhpvBal.lbLateAckCR  = data.ltAck;
									$scope.lhpvBal.lbOthExtKmP  = data.exKm;
									$scope.lhpvBal.lbOthOvrHgtP = data.ovrHgt;
									$scope.lhpvBal.lbOthPnltyP  = data.penalty;
									$scope.lhpvBal.lbOthMiscP   = data.oth;
									$scope.lhpvBal.lbUnpDetP    = data.det;
									$scope.lhpvBal.lbUnLoadingP = data.unLdg;
								}else{
									$scope.lhpvBal.lbWtShrtgCR  = 0;
									$scope.lhpvBal.lbDrRcvrWtCR = 0;
									$scope.lhpvBal.lbLateDelCR  = 0;
									$scope.lhpvBal.lbLateAckCR  = 0;
									$scope.lhpvBal.lbOthExtKmP  = 0;
									$scope.lhpvBal.lbOthOvrHgtP = 0;
									$scope.lhpvBal.lbOthPnltyP  = 0;
									$scope.lhpvBal.lbOthMiscP   = 0;
									$scope.lhpvBal.lbUnpDetP    = 0;
									$scope.lhpvBal.lbUnLoadingP = 0;
								}
								
								
								$scope.actWtShrtgCR = $scope.lhpvBal.lbWtShrtgCR;
								$scope.actDrRcvrWtCR = $scope.lhpvBal.lbDrRcvrWtCR;
								
								/*$scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP + $scope.lhpvBal.lbOthExtKmP + $scope.lhpvBal.lbOthOvrHgtP 
																+ $scope.lhpvBal.lbOthPnltyP + $scope.lhpvBal.lbOthMiscP + $scope.lhpvBal.lbUnpDetP
																	+ $scope.lhpvBal.lbUnLoadingP;*/
								
								$scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP + $scope.lhpvBal.lbOthExtKmP + $scope.lhpvBal.lbOthOvrHgtP 
																+ $scope.lhpvBal.lbOthPnltyP + $scope.lhpvBal.lbUnpDetP + $scope.lhpvBal.lbUnLoadingP;
		
								$scope.lhpvBal.lbTotRcvrAmt = $scope.lhpvBal.lbCashDiscR + $scope.lhpvBal.lbMunsR + $scope.lhpvBal.lbTdsR 
																+ $scope.lhpvBal.lbWtShrtgCR + $scope.lhpvBal.lbDrRcvrWtCR + $scope.lhpvBal.lbLateDelCR 
																+ $scope.lhpvBal.lbLateAckCR + $scope.lhpvBal.lbOthMiscP;
		
		
								$scope.lhpvBal.lbFinalTot = $scope.lhpvBal.lbTotPayAmt - $scope.lhpvBal.lbTotRcvrAmt;
								
							}else{
								
								$scope.arHoAlw = false;
								$scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP;
								$scope.lhpvBal.lbTotRcvrAmt = $scope.lhpvBal.lbCashDiscR + $scope.lhpvBal.lbMunsR + $scope.lhpvBal.lbTdsR;
								$scope.lhpvBal.lbFinalTot = $scope.lhpvBal.lbTotPayAmt - $scope.lhpvBal.lbTotRcvrAmt;
							
							
								$scope.lhpvBal.lbWtShrtgCR  = data.wtShrtg;
								$scope.lhpvBal.lbDrRcvrWtCR = data.drRcvrWt;
								$scope.lhpvBal.lbLateDelCR  = data.ltDel;
								$scope.lhpvBal.lbLateAckCR  = data.ltAck;
								$scope.lhpvBal.lbOthExtKmP  = data.exKm;
								$scope.lhpvBal.lbOthOvrHgtP = data.ovrHgt;
								$scope.lhpvBal.lbOthPnltyP  = data.penalty;
								$scope.lhpvBal.lbOthMiscP   = data.oth;
								$scope.lhpvBal.lbUnpDetP    = data.det;
								$scope.lhpvBal.lbUnLoadingP = data.unLdg;
							}
						}
						
						
						
						
						
						$scope.lhpvBalDBFlag = false;
					    	$('div#lhpvBalDB').dialog({
								autoOpen: false,
								modal:true,
								resizable: false,
								show: UDShow,
								title:"LHPV BALANCE FOR CHALLAN ("+$scope.challan+")",
								hide: UDHide,
								position: UDPos,
								draggable: true,
								close: function(event, ui) { 
							        $(this).dialog('destroy');
							        $(this).hide();
							        $scope.lhpvBalDBFlag = true;
							    }
								});
						$('div#lhpvBalDB').dialog('open'); 
					}else if(data.result==="SRTG"){
						$scope.alertToast("There is some Sortage/Damage in challan");
					}else if(data.result==="hold"){
						$scope.alertToast("This challan is on hold for payment");
					}else{
						$scope.alertToast("server error");
						console.log("error in fetching getChlnFrLA data");
					}
				});
				response.error(function(data, status, headers, config){
					console.log(data);
				});
		 }else{
			 $scope.alertToast("Already create the lhpv Balance for challan "+$scope.challan);
		 }
	 }
	 
	 $scope.changeTdsPer = function() {
		console.log("changeTdsPer()");
		if ($scope.tdsPer == 1) {
			console.log("tdsPer is 1%");
			$scope.lhpvBal.lbTdsR = (parseFloat($scope.lhpvBal.lbLryBalP) * 0.01);
			$scope.lhpvBal.lbTdsR=Math.round($scope.lhpvBal.lbTdsR);
		} else if ($scope.tdsPer == 2) {
			console.log("tdsPer is 2%");
			$scope.lhpvBal.lbTdsR = (parseFloat($scope.lhpvBal.lbLryBalP) * 0.02);
			$scope.lhpvBal.lbTdsR=Math.round($scope.lhpvBal.lbTdsR);
		}
		$scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP + $scope.lhpvBal.lbOthExtKmP + $scope.lhpvBal.lbOthOvrHgtP 
		+ $scope.lhpvBal.lbOthPnltyP + $scope.lhpvBal.lbUnpDetP + $scope.lhpvBal.lbUnLoadingP;

		$scope.lhpvBal.lbTotRcvrAmt = $scope.lhpvBal.lbCashDiscR + $scope.lhpvBal.lbMunsR + $scope.lhpvBal.lbTdsR 
		+ $scope.lhpvBal.lbWtShrtgCR + $scope.lhpvBal.lbDrRcvrWtCR + $scope.lhpvBal.lbLateDelCR 
		+ $scope.lhpvBal.lbLateAckCR + $scope.lhpvBal.lbOthMiscP;

		$scope.chngLryBal();
	}
	 
	 $scope.chngLryBal = function(){
		 
		 //change tds
		 if ($scope.tdsPer == 1) {
			 console.log("tdsPer is 1%");
			 $scope.lhpvBal.lbTdsR = Math.round(parseFloat($scope.lhpvBal.lbLryBalP) * 0.01);
		 } else if ($scope.tdsPer == 2) {
			 console.log("tdsPer is 2%");
			 $scope.lhpvBal.lbTdsR = Math.round(parseFloat($scope.lhpvBal.lbLryBalP) * 0.02);
		 }
		 /*$scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP + $scope.lhpvBal.lbOthExtKmP + $scope.lhpvBal.lbOthOvrHgtP 
		 + $scope.lhpvBal.lbOthPnltyP + $scope.lhpvBal.lbUnpDetP + $scope.lhpvBal.lbUnLoadingP;
		 
		 $scope.lhpvBal.lbTotRcvrAmt = $scope.lhpvBal.lbCashDiscR + $scope.lhpvBal.lbMunsR + $scope.lhpvBal.lbTdsR 
		 + $scope.lhpvBal.lbWtShrtgCR + $scope.lhpvBal.lbDrRcvrWtCR + $scope.lhpvBal.lbLateDelCR 
		 + $scope.lhpvBal.lbLateAckCR + $scope.lhpvBal.lbOthMiscP;*/
		 
		 console.log("enter into chngLryBal function");
		 if($scope.lhpvBal.lbLryBalP <= 0 || angular.isUndefined($scope.lhpvBal.lbLryBalP)){
			 $scope.alertToast("please fill the amount <= "+$scope.actBal);
			 $scope.lhpvBal.lbLryBalP = 0;
		 }else if($scope.lhpvBal.lbLryBalP <= $scope.actBal){
			 if($scope.arHoAlw === true){
				 $scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP + $scope.lhpvBal.lbOthExtKmP + $scope.lhpvBal.lbOthOvrHgtP 
				 + $scope.lhpvBal.lbOthPnltyP + $scope.lhpvBal.lbUnpDetP + $scope.lhpvBal.lbUnLoadingP;
				
				 $scope.lhpvBal.lbFinalTot = $scope.lhpvBal.lbTotPayAmt - $scope.lhpvBal.lbTotRcvrAmt;
			 }else{
				 $scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP;
				 $scope.lhpvBal.lbFinalTot = $scope.lhpvBal.lbTotPayAmt - $scope.lhpvBal.lbTotRcvrAmt;
			 }
		 }else{
			 $scope.lhpvBal.lbLryBalP = $scope.actBal;
			 $scope.alertToast("please fill the amount <= "+$scope.actBal);
		 }
		 
		 $scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP + $scope.lhpvBal.lbOthExtKmP + $scope.lhpvBal.lbOthOvrHgtP 
		 + $scope.lhpvBal.lbOthPnltyP + $scope.lhpvBal.lbUnpDetP + $scope.lhpvBal.lbUnLoadingP;
		 
		 $scope.lhpvBal.lbTotRcvrAmt = $scope.lhpvBal.lbCashDiscR + $scope.lhpvBal.lbMunsR + $scope.lhpvBal.lbTdsR 
		 + $scope.lhpvBal.lbWtShrtgCR + $scope.lhpvBal.lbDrRcvrWtCR + $scope.lhpvBal.lbLateDelCR 
		 + $scope.lhpvBal.lbLateAckCR + $scope.lhpvBal.lbOthMiscP;
		
	 }
	 
	 $scope.chngCD = function(){
		 console.log("enter into chngCD function");
		 if($scope.lhpvBal.lbCashDiscR < $scope.actCashDisR){
			 $scope.alertToast("Plaese enter greater than equal to "+$scope.actCashDisR);
			 $scope.lhpvBal.lbCashDiscR = $scope.actCashDisR;
		 }
		 $scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP + $scope.lhpvBal.lbOthExtKmP + $scope.lhpvBal.lbOthOvrHgtP 
		 + $scope.lhpvBal.lbOthPnltyP + $scope.lhpvBal.lbUnpDetP + $scope.lhpvBal.lbUnLoadingP;

		 $scope.lhpvBal.lbTotRcvrAmt = $scope.lhpvBal.lbCashDiscR + $scope.lhpvBal.lbMunsR + $scope.lhpvBal.lbTdsR 
		 + $scope.lhpvBal.lbWtShrtgCR + $scope.lhpvBal.lbDrRcvrWtCR + $scope.lhpvBal.lbLateDelCR 
		 + $scope.lhpvBal.lbLateAckCR + $scope.lhpvBal.lbOthMiscP;

		 $scope.chngLryBal();
	 }
	 
	 
	 $scope.chngMU = function(){
		 console.log("enter inot chngMU function");
		 if($scope.lhpvBal.lbMunsR < $scope.actMunsR){
			 $scope.alertToast("Plaese enter greater than equal to "+$scope.actMunsR);
			 $scope.lhpvBal.lbMunsR = $scope.actMunsR;
		 }
		 
		 $scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP + $scope.lhpvBal.lbOthExtKmP + $scope.lhpvBal.lbOthOvrHgtP 
		 + $scope.lhpvBal.lbOthPnltyP + $scope.lhpvBal.lbUnpDetP + $scope.lhpvBal.lbUnLoadingP;

		 $scope.lhpvBal.lbTotRcvrAmt = $scope.lhpvBal.lbCashDiscR + $scope.lhpvBal.lbMunsR + $scope.lhpvBal.lbTdsR 
		 + $scope.lhpvBal.lbWtShrtgCR + $scope.lhpvBal.lbDrRcvrWtCR + $scope.lhpvBal.lbLateDelCR 
		 + $scope.lhpvBal.lbLateAckCR + $scope.lhpvBal.lbOthMiscP;
		 
		 $scope.chngLryBal();
	 }
	 
	 
	 $scope.chngTDS = function(){
		 console.log("enter into chngTDS function");
		 if($scope.lhpvBal.lbTdsR < $scope.actTdsR){
			 $scope.alertToast("Plaese enter greater than equal to "+$scope.actTdsR);
			 $scope.lhpvBal.lbTdsR = $scope.actTdsR;
		 }
	 }
	 
	 
	 $scope.chngWS = function(){
		 console.log("enter into chngWS function");
		 if($scope.lhpvBal.lbWtShrtgCR > $scope.actWtShrtgCR){
			 $scope.alertToast("Plaese enter less than equal to "+$scope.actWtShrtgCR);
			 $scope.lhpvBal.lbWtShrtgCR = $scope.actWtShrtgCR;
		 }
		 $scope.resetAllAmt();
	 }
	 	 
	 $scope.chngDR = function(){
		 console.log("enter into chngDR function");
		 if($scope.lhpvBal.lbDrRcvrWtCR > $scope.actDrRcvrWtCR){
			 $scope.alertToast("Plaese enter less than equal to "+$scope.actDrRcvrWtCR);
			 $scope.lhpvBal.lbDrRcvrWtCR = $scope.actDrRcvrWtCR;
		 }
		 $scope.resetAllAmt();
	 }
	 
	 $scope.submitLhpvBal = function(lhpvBalForm,lhBal){
		 console.log("enter into submitLhpvBal function = "+lhpvBalForm.$invalid);
		 if(lhpvBalForm.$invalid){
			$scope.alertToast("Please fill correct form");
		 }else{
			 if($scope.lhpvBal.lbLryBalP <= 0 && $scope.actBal > 0){
				 $scope.alertToast("please fill the amount <= "+$scope.actBal);
			 }else{
				 $scope.lbFinalTotSum=$scope.lbFinalTotSum+$scope.lhpvBal.lbFinalTot;
				 $scope.lhpvBalList.push(lhBal);
				 $scope.lhpvBal = {};
				 $('div#lhpvBalDB').dialog('close'); 
			 }
		 }
	 }
	 
	
	 $scope.removeLhpvBal = function(index,lhpvbal){
		 console.log("enter into removeLhpvBal function");
		 $scope.lbFinalTotSum=$scope.lbFinalTotSum-lhpvbal.lbFinalTot;
		 $scope.lhpvBalList.splice(index,1);
	 }
	 
	 
	 $scope.voucherSubmit = function(voucherForm){
		 console.log("enter into voucherForm function = "+voucherForm.$invalid);
		 if(voucherForm.$invalid){
			$scope.alertToast("Please enter correct form");
		 }else{
			 console.log("final submittion");
			    $scope.saveVsFlag = false;
		    	$('div#saveVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					title:"LHPV BALANCE FINAL SUBMISSION",
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.saveVsFlag = true;
				    }
					});
				$('div#saveVsDB').dialog('open');
		 }
	 }
	 
	 
	 $scope.back = function(){
		 console.log("enter into back function");
		 $('div#saveVsDB').dialog('close');
	 } 
	 
	 
	 $scope.saveVS = function(){
		 console.log("enter into saveVS function ");
			$scope.vs.lhpvBalList = $scope.lhpvBalList;
			$scope.lbFinalTotSum=0;
			faCodeTemp='';
			
			$('#saveId').attr("disabled","disabled");
			var response = $http.post($scope.projectName+'/submitLhpvBal',$scope.vs);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$('div#saveVsDB').dialog('close');
					$('#saveId').removeAttr("disabled");
					
					$scope.alertToast("Successfully save LHPV BALANCE -->> "+data.lbNo);
					
					$scope.vs = {};
					$scope.lhpvBal = {};
					$scope.bnkList = [];
					$scope.chlnList = [];
					$scope.actBOList = [];
					$scope.lhpvBalList = [];
					$scope.vs.payToStf='';
					$scope.challan = "";
					$scope.vs.chequeType = '';
					$('#chequeTypeId').attr("disabled","disabled");
					$scope.vs.bankCode = "";
					$('#bankCodeId').attr("disabled","disabled");
					$scope.vs.chequeLeaves = {};
					$('#chequeNoId').attr("disabled","disabled");
					
					$scope.getLhpvDet();
				}else{
					console.log("Error in submitVoucher");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	 }
	 
	 $scope.chngOthExtKm = function() {
		 console.log("chngOthExtKm()");
		 if ($scope.lhpvBal.lbOthExtKmP > $scope.actOthExtKmP) {
			 $scope.alertToast("Plaese enter less than equal to "+$scope.actOthExtKmP);
			 $scope.lhpvBal.lbOthExtKmP = $scope.actOthExtKmP;
		 }
		 $scope.resetAllAmt();
	 }
	 
	 $scope.chngDet = function() {
		 console.log("chngDet()");
		 if ($scope.lhpvBal.lbUnpDetP > $scope.actUnpDetP) {
			 $scope.alertToast("Plaese enter less than equal to "+$scope.actUnpDetP);
			 $scope.lhpvBal.lbUnpDetP = $scope.actUnpDetP;
		 }
		 $scope.resetAllAmt();
	 }
	 

	 $scope.chngOH = function() {
		 console.log("chngOH()");
		 if ($scope.lhpvBal.lbOthOvrHgtP > $scope.actOthOvrHgtP) {
			 $scope.alertToast("Plaese enter less than equal to "+$scope.actOthOvrHgtP);
			 $scope.lhpvBal.lbOthOvrHgtP = $scope.actOthOvrHgtP;
		 }
		 $scope.resetAllAmt();
	 }
	 
	 $scope.chngUnldng = function() {
		 console.log("chngUnldng()");
		 if ($scope.lhpvBal.lbUnLoadingP > $scope.actUnLoadingP) {
			 $scope.alertToast("Plaese enter less than equal to "+$scope.actUnLoadingP);
			 $scope.lhpvBal.lbUnLoadingP = $scope.actUnLoadingP;
		 }
		 $scope.resetAllAmt();
	 }
	 
	 $scope.resetAllAmt = function() {
			console.log("resetAllAmt");
			
			 $scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP + $scope.lhpvBal.lbOthExtKmP + $scope.lhpvBal.lbOthOvrHgtP 
			 + $scope.lhpvBal.lbOthPnltyP + $scope.lhpvBal.lbUnpDetP + $scope.lhpvBal.lbUnLoadingP;

			 $scope.lhpvBal.lbTotRcvrAmt = $scope.lhpvBal.lbCashDiscR + $scope.lhpvBal.lbMunsR + $scope.lhpvBal.lbTdsR 
			 + $scope.lhpvBal.lbWtShrtgCR + $scope.lhpvBal.lbDrRcvrWtCR + $scope.lhpvBal.lbLateDelCR 
			 + $scope.lhpvBal.lbLateAckCR + $scope.lhpvBal.lbOthMiscP;
			 
			 $scope.chngLryBal();
	}
	 
	 $scope.verifyChallan = function() {
		console.log("verifyChallan()");
		if (angular.isUndefined($scope.challan) || $scope.challan === "" || $scope.challan === null) {
			$scope.alertToast("Please Enter Challan No");
		} else {
			
			$scope.lodingFlag = true;
			
			var res = $http.post($scope.projectName+'/verifyChlnForLhpvBal',$scope.challan);
			res.success(function(data, status, headers, config) {
				if (data.result === "success") {
					if (angular.isUndefined(faCodeTemp) || faCodeTemp === "" || faCodeTemp === null) {
						faCodeTemp = data.faCode;
						$scope.lbFinalTotSum=0;
						$scope.vs.brkOwnFaCode = data.faCode;
						$scope.lhpvBalList = [];
					} else {
						if (faCodeTemp === data.faCode) {
						} else {
							if($scope.vs.payBy=='R' && $scope.vs.payToStf=='S' && ($scope.vs.stafCode!=null && $scope.vs.stafCode !="")){
								faCodeTemp = data.faCode;
								$scope.vs.brkOwnFaCode = data.faCode;
							}else{
								if($scope.vs.payBy=='Q'){
									faCodeTemp = data.faCode;
									//$scope.lbFinalTotSum=0;
									$scope.vs.brkOwnFaCode = data.faCode;
									//$scope.lhpvBalList = [];
								}else{
									faCodeTemp = data.faCode;
									$scope.lbFinalTotSum=0;
									$scope.vs.brkOwnFaCode = data.faCode;
									$scope.lhpvBalList = [];
								}
							
					    	}
						}
					}
					$scope.saveChln($scope.challan);
				} else {
					$scope.alertToast(data.msg);
				}
				$scope.lodingFlag = false;
			});
			res.error(function(data, status, headers, config) {
				console.log("error in response of verifyChlnForLhpvBal: "+data);
				$scope.lodingFlag = false;
			});
		}
	}
	 
	 $scope.selectPayTo=function(){
		 if($scope.vs.payBy=='R' && $scope.vs.payToStf=='S'){
			 $scope.stafCodeFlag=true;
			 $scope.stfCodeRqr=true;
		 }else{
			 $scope.stafCodeFlag=false;
			 $scope.vs.stafCode="";
			 $scope.stfCodeRqr=false;
		 }
	 }
	
	 $scope.stfName=function(){
		 
		 console.log($scope.vs.stafCode);
		 if($scope.vs.stafCode.length==7){
			 console.log($scope.vs.stafCode);
				var res = $http.post($scope.projectName+'/getNameOfStf',$scope.vs.stafCode);
				res.success(function(data, status, headers, config) {
					if (data.result === "success") {
						console.log(data);
						$scope.alertToast(data.name);
						
					} else {
						$scope.alertToast("Name not found");
					}
				});
				res.error(function(data, status, headers, config) {
					console.log("error in response of verifyChlnForLhpvAdv: "+data);
					$scope.lodingFlag = false;
				});
					
			 
		 }
	 }
	
	 $scope.notifyMsg = function() {
		console.log("notifyMsg");
		if (angular.isUndefined($scope.vs.brkOwnFaCode) || $scope.vs.brkOwnFaCode === "" || $scope.vs.brkOwnFaCode === null) {
			$scope.alertToast("Please Entre Challan No and Verify Challan");
		}		
	}
	 
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getLhpvDet();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);
'use strict';

var app = angular.module('application');

app.controller('TelephoneVoucherBackCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.vs = {};
	$scope.tm_stm = {};
	$scope.telMList = [];
	$scope.chqList = [];
	$scope.tmAndStmList = [];
	//$scope.selectedTM = {};
	var temp;
    $scope.stmTotDedAmtSum=0;
	$scope.saveVsFlag = true;
	$scope.selChqDBFlag = true;
	
	
	$scope.phoneNoDBFlag = true;
	$scope.telVoucherDBFlag = true;
	$scope.bankCodeDBFlag = true;
	
	var max = 15;
	
	$('#payAmtId').keypress(function(key) {
        if(key.charCode < 46 || key.charCode > 57)
        	return false;
    });
	
	 $('#payAmtId').keypress(function(e) {
     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
             e.preventDefault();
         } 
     });
	 
	 $('#stmSmsDedAmtId').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	  });
		
	  $('#stmSmsDedAmtId').keypress(function(e) {
	     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	             e.preventDefault();
	         } 
	  });
	  
	  $('#stmCTDedAmtId').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	  });
		
	  $('#stmCTDedAmtId').keypress(function(e) {
	     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	             e.preventDefault();
	         } 
	  });
	  
	  $('#stmDDedAmtId').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	  });
		
	  $('#stmDDedAmtId').keypress(function(e) {
	     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	             e.preventDefault();
	         } 
	  });
	  
	  $('#stmGDedAmtId').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	  });
		
	  $('#stmGDedAmtId').keypress(function(e) {
	     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	             e.preventDefault();
	         } 
	  });
	  
	  $('#stmPCDedAmtId').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	  });
		
	  $('#stmPCDedAmtId').keypress(function(e) {
	     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	             e.preventDefault();
	         } 
	  });
	  
	  $('#stmRDedAmtId').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	  });
		
	  $('#stmRDedAmtId').keypress(function(e) {
	     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	             e.preventDefault();
	         } 
	  });
	  
	  $('#stmIsdDedAmtId').keypress(function(key) {
	        if(key.charCode < 46 || key.charCode > 57)
	        	return false;
	  });
		
	  $('#stmIsdDedAmtId').keypress(function(e) {
	     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
	             e.preventDefault();
	         } 
	  });
	
	
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		$scope.saveVsFlag = true;
		var response = $http.post($scope.projectName+'/getVDetFrTelVBack');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.bankCodeList = data.bankCodeList;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.vs.voucherType = $scope.TEL_VOUCHER;
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");
				
				$scope.getTelephoneList();
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.getTelephoneList = function(){
		console.log("enter into getTelephoneList function");
		var response = $http.post($scope.projectName+'/getTelephoneList');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.telMList = data.tmList;
				$scope.fetchTmAndStm();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.selectPayBy = function(){
		console.log("enter into selectPayMod function with vs.payBy ="+$scope.vs.payBy);
		if($scope.vs.payBy === 'C'){
			$('#phNoId').removeAttr("disabled");
			
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
			$scope.vs.bankCode = "";
			$('#bankCodeId').attr("disabled","disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
			$scope.vs.payTo = "";
			$('#payToId').attr("disabled","disabled");
			
		}else if($scope.vs.payBy === 'Q'){
			$('#chequeTypeId').removeAttr("disabled");
		}else if($scope.vs.payBy === 'O'){
			$('#bankCodeId').removeAttr("disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
		}else{
			console.log("invaid entry");
		}
	}
	
	
	$scope.openPhNoDB = function(){
		console.log("enter into openPhNoDB function");
		$scope.phoneNoDBFlag = false;
    	$('div#phoneNoDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Phone Number",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#phoneNoDB').dialog('open');	
	}
	
	$scope.savePhoneNo = function(telM){
		console.log("enter into savePhoneNo function");
		$scope.phNo = telM.tmPhNo;
		$scope.phoneNoDBFlag = true;
		$('div#phoneNoDB').dialog('close');	
		
		$scope.tm_stm.tMstr = telM;
		$scope.telVoucherDBFlag = false;
		$('div#telVoucherDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: telM.tmPhNo+" ("+ telM.employee.empName +") Bill day is "+telM.tmBillDay,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#telVoucherDB').dialog('open');	
		
	}
	
	
	
	$scope.submitTMVoucher = function(tmVoucherForm){
		console.log("enter into tmVoucherForm function --->"+tmVoucherForm.$invalid);
		if(tmVoucherForm.$invalid){
			$scope.alertToast("please enter correct value");
		}else{
			console.log("***********************");
			var response = $http.post($scope.projectName+'/submitTMVoucher',$scope.tm_stm);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					
					$scope.telVoucherDBFlag = true;
					$('div#telVoucherDB').dialog('close');	
					$scope.tm_stm = {};
					
					$scope.fetchTmAndStm();
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
		}
	}
	
	
	
	$scope.fetchTmAndStm = function(){
		console.log("enter into fetchTmAndStm function");
		var response = $http.post($scope.projectName+'/fetchTmAndStm');
		response.success(function(data, status, headers, config){
				$scope.tmAndStmList = data.list;
				 temp=0;
				// console.log(JSON.stringify(data.list));
					for(var i=0; i<data.list.length; i++){
						temp=temp+data.list[i].stMstr.stmTotDedAmt;
					}
					$scope.stmTotDedAmtSum=temp;
				console.log("length of tmAndStmList ="+$scope.tmAndStmList.length);
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.removeStmV = function(index,tmAndStm){
		console.log("enter into removeStmV function = "+index);
		$scope.stmTotDedAmtSum=$scope.stmTotDedAmtSum-tmAndStm.stMstr.stmTotDedAmt;
		//var rIndex = parseInt(index)
		var remIndex = {
				"index" : index
		};
		var response = $http.post($scope.projectName+'/removeStmV',remIndex);
		response.success(function(data, status, headers, config){
			//$scope.tmAndStmList = [];
			$scope.fetchTmAndStm();
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	$scope.selectChqType = function(){
		console.log("enter into selectChqType function");
		$('#bankCodeId').removeAttr("disabled");
	}
	
	
	$scope.openBankCodeDB = function(){
		console.log("enter into openBankCodeDB function");
		$scope.bankCodeDBFlag = false;
		$('div#bankCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Bank Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#bankCodeDB').dialog('open');	
	}
	
	
	 $scope.saveBankCode =  function(bankCode){
			console.log("enter into saveBankCode----->"+bankCode);
			$scope.vs.bankCode = bankCode;
			$('div#bankCodeDB').dialog('close');
			$scope.bankCodeDBFlag = true;
			

			$('#payToId').removeAttr("disabled");
			$('#phNoId').removeAttr("disabled");
					
			if($scope.vs.payBy === 'Q'){
				if(angular.isUndefined($scope.vs.chequeType) ||  $scope.vs.chequeType === null || $scope.vs.chequeType === ""){
					$scope.alertToast("please select cheque type");
				}else{
					var chqDet = {
							"bankCode" : bankCode,
							"CType"    : $scope.vs.chequeType
					};
					
					var response = $http.post($scope.projectName+'/getChequeNoFrTV',chqDet);
					response.success(function(data, status, headers, config){
						if(data.result === "success"){
							console.log("Cheque Leaves List--->>"+ data.chequeLeaves);
							$('#chequeNoId').removeAttr("disabled");
							$scope.vs.chequeLeaves = data.chequeLeaves;
							$scope.chqList = data.list;
							$scope.maxChqAmnt = data.chequeLeaves.chqLChqMaxLt;
						}else{
							$scope.alertToast("Branch does not have '"+$scope.vs.chequeType+"' type cheeque of Bank "+bankCode);
							console.log("Error in bringing data from getChequeNo");
						}
					});
					response.error(function(data, status, headers, config){
						console.log(data);
					});
				}
				
			}		
	 }
	 
	 $scope.openChqDB = function(){
		 console.log("enter into openChqDB function");
		    $scope.selChqDBFlag = false;
	    	$('div#selChqDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				title:"Select Cheque No.",
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.selChqDBFlag = true;
			    }
				});
			$('div#selChqDB').dialog('open');
	 }
	
	 
	 $scope.saveChqNo = function(chq){
		 console.log("enter into saveChqNo function");
		 $scope.vs.chequeLeaves = chq;
		 $('div#selChqDB').dialog('close');
	 }
	
	 $scope.calTotDedAmt = function(){
		 console.log("enter into calTotDedAmt function "+$scope.tm_stm.stMstr.stmPayAmt); 
		 var payAmt = 0;
		 var smsDedAmt = 0;
		 var cTDedAmt = 0;
		 var dDedAmt = 0;
		 var gDedAmt = 0;
		 var pCDedAmt = 0;
		 var rDedAmt = 0;
		 var isdDedAmt = 0;
		 
		 if(!(angular.isNumber($scope.tm_stm.stMstr.stmPayAmt)) && angular.isUndefined($scope.tm_stm.stMstr.stmPayAmt)){
			 payAmt = 0;
		 }else{
			 payAmt = $scope.tm_stm.stMstr.stmPayAmt;
		 }
		 
		 if(!(angular.isNumber($scope.tm_stm.stMstr.stmSmsDedAmt)) && angular.isUndefined($scope.tm_stm.stMstr.stmSmsDedAmt)){
			 smsDedAmt = 0;
		 }else{
			 smsDedAmt = $scope.tm_stm.stMstr.stmSmsDedAmt;
		 }
		 
		 if(!(angular.isNumber($scope.tm_stm.stMstr.stmCTDedAmt)) && angular.isUndefined($scope.tm_stm.stMstr.stmCTDedAmt)){
			 cTDedAmt = 0;
		 }else{
			 cTDedAmt = $scope.tm_stm.stMstr.stmCTDedAmt;
		 }
		 
		 if(!(angular.isNumber($scope.tm_stm.stMstr.stmDDedAmt)) && angular.isUndefined($scope.tm_stm.stMstr.stmDDedAmt)){
			 dDedAmt = 0;
		 }else{
			 dDedAmt =  $scope.tm_stm.stMstr.stmDDedAmt;
		 }
		 
		 if(!(angular.isNumber($scope.tm_stm.stMstr.stmGDedAmt)) && angular.isUndefined($scope.tm_stm.stMstr.stmGDedAmt)){
			 gDedAmt = 0;
		 }else{
			 gDedAmt =  $scope.tm_stm.stMstr.stmGDedAmt;
		 }
		 
		 if(!(angular.isNumber($scope.tm_stm.stMstr.stmPCDedAmt)) && angular.isUndefined($scope.tm_stm.stMstr.stmPCDedAmt)){
			 pCDedAmt = 0;
		 }else{
			 pCDedAmt =  $scope.tm_stm.stMstr.stmPCDedAmt;
		 }
		 
		 if(!(angular.isNumber($scope.tm_stm.stMstr.stmRDedAmt)) && angular.isUndefined($scope.tm_stm.stMstr.stmRDedAmt)){
			 rDedAmt = 0;
		 }else{
			 rDedAmt =  $scope.tm_stm.stMstr.stmRDedAmt;
		 }
		 
		 if(!(angular.isNumber($scope.tm_stm.stMstr.stmIsdDedAmt)) && angular.isUndefined($scope.tm_stm.stMstr.stmIsdDedAmt)){
			 isdDedAmt = 0;
		 }else{
			 isdDedAmt =  $scope.tm_stm.stMstr.stmIsdDedAmt;
		 }
		 
		 $scope.tm_stm.stMstr.stmTotDedAmt = parseFloat(parseFloat(parseFloat(payAmt) + parseFloat(smsDedAmt) + parseFloat(cTDedAmt) + parseFloat(dDedAmt) + parseFloat(gDedAmt) + parseFloat(pCDedAmt) + parseFloat(rDedAmt) + parseFloat(isdDedAmt)).toFixed(2));
		// $scope.tm_stm.stMstr.stmTotDedAmt = parseFloat(payAmt) + parseFloat(smsDedAmt) + parseFloat(cTDedAmt) + parseFloat(dDedAmt) + parseFloat(gDedAmt) + parseFloat(pCDedAmt) + parseFloat(rDedAmt) + parseFloat(isdDedAmt);  
		 console.log("$scope.tm_stm.stMstr.stmTotDedAmt = "+$scope.tm_stm.stMstr.stmTotDedAmt); 
	 }
	 
	 
	 $scope.voucherSubmit = function(newVoucherForm){
		 console.log("enter into voucherSubmit function ==>"+newVoucherForm.$invalid);
		 if(newVoucherForm.$invalid){
			 $scope.alertToast("please enter correct value");
		 }else{
			    console.log("final submittion");
			    $scope.saveVsFlag = false;
		    	$('div#saveVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				    }
					});
				$('div#saveVsDB').dialog('open');
		 }
	 }
	 
	 
	
	$scope.back = function(){
			console.log("enter into back function");
			$('div#saveVsDB').dialog('close');
			$scope.saveVsFlag = true;
	}
	 
	 
	$scope.saveVS = function(){
		console.log("enter into saveVs function");
		$scope.saveVsFlag = true;
		$('div#saveVsDB').dialog('close');
		$('#saveId').attr("disabled","disabled");
		var response = $http.post($scope.projectName+'/submitTelVoucher',$scope.vs);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$('#saveId').removeAttr("disabled");
				$scope.phNo = "";
				$scope.vs = {};
				$scope.tm_stm = {};
				$scope.telMList = [];
				$scope.tmAndStmList = [];
				$scope.empCode = "";
				
				$scope.vs.chequeType = '';
				$('#chequeTypeId').attr("disabled","disabled");
				$scope.vs.bankCode = "";
				$('#bankCodeId').attr("disabled","disabled");
				$scope.vs.chequeLeaves = {};
				$('#chequeNoId').attr("disabled","disabled");
				$scope.vs.payTo = "";
				$('#payToId').attr("disabled","disabled");
				
				$scope.getVoucherDetails();
			}else{
				console.log("Error in submitVoucher");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
}]);
'use strict';

var app = angular.module('application');

app.controller('AlwSrtgDmgArCntlr',['$scope','$location','$http','$filter','AuthorisationService',
                                     function($scope,$location,$http,$filter,AuthorisationService){
	
	
                          console.log("hiiiiiiiiiiiiiii");
                          $scope.chlnCodeDBFlag=true;
                          $scope.ar={};
                          
                          $scope.openChlnCodeDb=function(){
                        	  var response = $http.post($scope.projectName+'/getChlnFrAlwSrtgDmg');
                      		response.success(function(data, status, headers, config) {
                      			if(data.result === "success"){
                      				$scope.arChlnCodeList=data.list;
                      				console.log(data);
                      				$scope.chlnCodeDBFlag=false;
                      				$('div#chlnCodeDB').dialog({
                      					autoOpen: false,
                      					modal:true,
                      					resizable: false,
                      					show: UDShow,
                      					title:"Select Challan",
                      					hide: UDHide,
                      					position: UDPos,
                      					draggable: true,
                      					close: function(event, ui) { 
                      				        $(this).dialog('destroy');
                      				        $(this).hide();
                      				        $scope.arCodeDBFlag = true;
                      				    }
                      					});
                      		    	$('div#chlnCodeDB').dialog('open'); 
                      				
                      				
                      			}else{
                      				$scope.alertToast("There is no pending Challan for Aproval");
                      				console.log("error in fetching result from challan");
                      			}
                      		});
                      		response.error(function(data, status, headers, config) {
                      			$scope.errorToast(data.result);
                      		});
                          }
                          
                          
                          $scope.saveChln=function(arChlnCode){
								                        	  
                        	  $('div#chlnCodeDB').dialog('close');
								$scope.arChlnCode = arChlnCode;

								var response = $http.post($scope.projectName + '/getSedrFrSDAlw', arChlnCode);
								response.success(function(data, status,headers, config) {
											if (data.result === "success") {
												console.log(data);
												$scope.ar = data.arObj;
												 $scope.remWtSrtg=data.arObj.arRemWtShrtg;
												 $scope.claim=data.arObj.arClaim;
												 $scope.wtSrtg=data.arObj.arWtShrtg;
												 
												 $scope.ar.srtgDmgAlw=data.arObj.srtgDmgAlw;

											} else {
												$scope.alertToast("Server Error");
												console.log("error in fetching result from saveSedr");
											}
										});
								response.error(function(data, status, headers,config) {
									$scope.errorToast(data.result);
								});
                          }
                          
                          
                          $scope.changeAmt=function(arRemWtShrtg){
                        	  var temp;
                        	  console.log("Temp="+arRemWtShrtg);
                        	  temp=$scope.remWtSrtg-arRemWtShrtg;
                        	  $scope.ar.arWtShrtg=parseFloat($scope.wtSrtg - temp).toFixed(2);
                        	  $scope.ar.arClaim=parseFloat($scope.claim - temp).toFixed(2);
                        	  //parseFloat(parseFloat($scope.wtSrtg - temp).toFixed(2));
                        	  console.log("Temp="+temp);
                        	  console.log("claim="+$scope.ar.arClaim);
                        	  console.log("Wtsrtg="+$scope.ar.arWtShrtg);
                          }
                          
                          $scope.save = function(){
                      		console.log("enter into editARSubmit function");
                      		//$('#saveId').attr("disabled","disabled");
                      		console.log("arvalue="+$scope.ar.srtgDmgAlw);
                      		var response = $http.post($scope.projectName+'/editArByHoAlw', $scope.ar);
                      		response.success(function(data, status, headers, config) {
                      			if(data.result === "success"){
                      				$scope.alertToast("success");
                      				console.log("arvalue="+$scope.ar.srtgDmgAlw);
                      				$scope.ar = {};
                      				$scope.arCode = "";
                      				//$scope.actBrhName = "";
                      				//$scope.actBrhFaCode = "";
                      				//$scope.showAR = true;
                      			//	$('#saveId').removeAttr("disabled");
                      			}else{
                      				$scope.alertToast("Error");
                      			}
                      		});
                      		response.error(function(data, status, headers, config) {
                      			$scope.errorToast(data);
                      		});
                      	}
                          
                          $scope.allow=function(isSrtgDmgAlw){
                        	  console.log("asdf=="+isSrtgDmgAlw);
                        	  $scope.ar.srtgDmgAlw=isSrtgDmgAlw;
                          }
	
}]);
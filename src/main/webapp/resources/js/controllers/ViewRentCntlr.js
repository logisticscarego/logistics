'use strict';

var app = angular.module('application');

app.controller('ViewRentCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	console.log("ViewRentCntlr Started");
	$scope.branchList=[];
	$scope.branchDBFlag = true;
	$scope.rentDBFlag=true;
	$scope.updateForm=true;
	$scope.rentAddress=true;
	
	$scope.openBranchDB = function(){
		console.log("Entered into openBranchDB");
		var response = $http.post($scope.projectName+'/getAllBranchesList');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getAllBranchesList Success");
				$scope.branchList = data.branchCodeList;
				$scope.branchDBFlag = false;
				$('div#branchDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Branch",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
						$scope.branchDBFlag = true;
					}
				});

				$('div#branchDB').dialog('open');
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
	}
	
	$scope.saveBranch =  function(branch){
		console.log("saveBranch");
		console.log("enter into branchName----->"+branch.branchName);
		console.log("enter into branchFaCode----->"+branch.branchFaCode);
		$scope.branch = branch;
		$scope.selBranchId=branch.branchId;
		$('div#branchDB').dialog('close');
		var branchId=branch.branchId;
		console.log("Branch Id   "+branchId);
		var response = $http.post($scope.projectName+'/getRentByBranchId',branchId);
		response.success(function(data){
			if(data.result==="success"){
				$scope.rentMstrList=data.rentList;
				if($scope.rentMstrList.length>0){
					$scope.rentDBFlag=false;
					$('div#rentDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						position: UDPos,
						show: UDShow,
						hide: UDHide,
						title: "RentMstr",
						draggable: true,
						close: function(event, ui) { 
							$(this).dialog('destroy');
							$(this).hide();
							$scope.rentDBFlag=true;
						}
					});

					$('div#rentDB').dialog('open');
				}
			}else{
				$scope.alertToast(data.result);
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getRentMstr Error: "+data);
			$scope.alertToast("Error")
		});	
	}
	
	$scope.saveRentMstr=function(rentMstr){
		$('div#rentDB').dialog('close');
		$scope.rentDBFlag=true;
		$scope.updateForm=false;
		$scope.rent=rentMstr;
		console.log(rentMstr)
		
	}
	
	$scope.openRentMstr=function(){
		console.log($scope.branch);
		if(angular.isUndefined($scope.branch)){
			$scope.alertToast("Select Branch ");
		}else{
			
			var branchId=$scope.branch.branchId;
			console.log("Branch Id   "+branchId);
			var response = $http.post($scope.projectName+'/getRentByBranchId',branchId);
			response.success(function(data){
				if(data.result==="success"){
					$scope.rentMstrList=data.rentList;
					if($scope.rentMstrList.length>0){
						$scope.rentDBFlag=false;
						$('div#rentDB').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							position: UDPos,
							show: UDShow,
							hide: UDHide,
							title: "RentMstr",
							draggable: true,
							close: function(event, ui) { 
								$(this).dialog('destroy');
								$(this).hide();
								$scope.rentDBFlag=true;
							}
						});

						$('div#rentDB').dialog('open');
					}
				}else{
					$scope.alertToast(data.result);
				}
			});
			response.error(function(data, status, headers, config){
				console.log("getRentMstr Error: "+data);
				$scope.alertToast("Error")
			});	

		}
		
	}
	
	$scope.openAddDB=function(){
		$scope.rentAddress=false;
		$('div#addDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Address",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.rentAddress=true;
			}
		});

		$('div#addDB').dialog('open');
	}

	$scope.saveAdd=function(){
		$scope.rentAddress=true;
		$('div#addDB').dialog('close');
	}
	
	$scope.updateRentMstr=function(rent){
		console.log(rent);
		var response=$http.post($scope.projectName+'/updateRentMstr',rent);
		response.success(function(data,status){
			if(data.result==="success"){
				$scope.alertToast(data.result);
				$scope.rent={};
				$scope.branch={};
				$scope.updateForm=true;
				
			}else{
				$scope.alertToast(data.result);
			}
			
		});
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
	
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("ViewRentCntlr Ended");
	
}]);
'use strict';

var app = angular.module('application');

app.controller('AdminCntlr',['$scope','$location','$http',
                                 function($scope,$location,$http){
	
	$scope.operatorCodeFlag = true;
	$scope.bsd={};
	$scope.oprtrCodes=[];
	$scope.isStockRecieved=[];
	$scope.lengthBsdList;
	var getVal;
	
	$scope.openOperatorCodeDB = function(index){
		$scope.operatorCodeFlag = false;
		getVal = index;
		
		
    	$('div#operatorCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: [350,200],
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#operatorCodeDB').dialog('open');
		}
	
	$scope.savOperatorCode = function(opcode){
		console.log("Enter into saveOprtrCode"+opcode.userCode);
		console.log("--"+$scope.opcodeId);
		$scope.oprtrCodes[$scope.opcodeId] = opcode.userCode;
		
		$('#'+getVal).removeAttr("disabled");
		$('div#operatorCodeDB').dialog('destroy');
		$scope.operatorCodeFlag = true;
	}
	
		$scope.getBranchStockDispatchData = function(){
		console.log("Enter into getBranchStockDispatchData");
		   var response = $http.post($scope.projectName+'/getBranchStockDispatchData');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
				   $scope.bsdList = data.list;
				   $scope.lengthBsdList = $scope.bsdList.length;
				   for(var i=0;i<$scope.bsdList.length;i++){
					   console.log("msg is-"+$scope.bsdList[i].dispatchMsg);
					   $scope.isStockRecieved[i]=$scope.bsdList[i].dispatchMsg;
				   }
				   
				   /*for(var i=0;i<$scope.bsdList.length;i++){
					   console.log("value************"+$scope.bsdList[i].isAdminView);
					   if($scope.bsdList[i].isAdminView === "yes"){
						   console.log("enter into if function");
						   $('#isStockRecieved_id').removeAttr("disabled");
						   $('#submit_id').removeAttr("disabled");
					   }
				   }*/
				   $scope.getOperatorCode();
			   }else{
				   console.log(data);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
		
		$scope.getOperatorCode = function(){
			console.log("Enter into getOperatorCode");
			   var response = $http.post($scope.projectName+'/getOperatorCodeForAdmin');
			   response.success(function(data, status, headers, config){
				   if(data.result==="success"){
					   $scope.opCodeList = data.list;
					   console.log("---"+data.list);
				   }else{
					   console.log(data);
				   }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});	
		}
		
		$scope.sendToOperator = function(bsd){
			console.log("Enter into sendToOperator----->"+$scope.oprtrCodes[$scope.opcodeId]);
			var optr_Notification = {
					"dispatchCode" : bsd.brsDisCode,
					"dispatchDate" : bsd.brsDisDt,
					"operatorCode" : $scope.oprtrCodes[$scope.opcodeId]
 			};
			   var response = $http.post($scope.projectName+'/sendToOperator',optr_Notification);
			   response.success(function(data, status, headers, config){
				   if(data.result==="success"){
					   $scope.alertToast(data.result);					
				   }else{
					   console.log(data);
				   }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});	
		}
		
		
		$scope.confirmDispatch = function(bSDCode,index){
			console.log("enter into confirmDispatch function----"+bSDCode);
			console.log("index"+index);
			
				console.log("$scope.lengthBsdList--------------"+$scope.lengthBsdList);
				if($scope.isStockRecieved[index] === "received"){
					var response = $http.post($scope.projectName+'/confirmDispatch',bSDCode);
					   response.success(function(data, status, headers, config){
						   if(data.result==="success"){
							   $scope.alertToast(data.result);					
						   }else{
							   console.log(data);
						   }
					   });
					   response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
						});	
				}else{
					$scope.alertToast("disptach is not received");	
				}			
			}
		
		$scope.enableOpCode = function(id){
			$scope.opcodeId=id;
			console.log("id==="+id);
			if(!($scope.enable_OpCode)){
				$scope.oprtrCodes[$scope.opcodeId]="";
				
			$('#sendOperator').attr("disabled","disabled");
			}	
		}
		
		/*$scope.getCnmtDaysLast = function(){
			console.log("Enter into getCnmtDaysLast");
			   var response = $http.post($scope.projectName+'/getCnmtDaysLast');
			   response.success(function(data, status, headers, config){
				   console.log("msg from server --- >"+data);
				   if(data.result==="success"){
					   console.log("--daysLeft"+data.daysLeft);
					   $scope.getAvgMonthlyUsage();
				   }else{
					   console.log(data);
				   }
				   
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});	
		}*/
		
	/*	$scope.getAvgMonthlyUsage = function(){
			console.log("Enter into getAvgMonthlyUsage");
			   var response = $http.post($scope.projectName+'/getAvgMonthlyUsage');
			   response.success(function(data, status, headers, config){
				   console.log("msg from server --- >"+data);
				   if(data.result==="success"){
					   console.log("--avgData"+data.averageMonthlyUsage);
				   }else{
					   console.log(data);
				   }
				   $scope.getBranchStockDispatchData();
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});	
		}
		*/
		//$scope.getCnmtDaysLast();
		$scope.getBranchStockDispatchData();
}]);
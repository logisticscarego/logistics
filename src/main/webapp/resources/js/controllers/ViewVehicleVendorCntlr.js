'use strict';

var app = angular.module('application');

app.controller('ViewVehicleVendorCntlr',['$scope','$location','$http','$filter','$window','FileUploadService',
                                 function($scope,$location,$http,$filter,$window,FileUploadService){

	console.log("ViewVehicleVendorCntlr Started");
	
	$scope.vehVendorList = [];
	
	$scope.vehVenTemp = {};
	$scope.vehVendor = {};
	
	
	$scope.vehVenDetailFlag = false;
	$scope.transferButtonFlag=false;
	$scope.docUpldFlag=false;
	$scope.transferFlag=false;
	$scope.existOwnerDB=true;
	
	$scope.uploadPANFlag=true;
	$scope.uploadDecFlag=true;
	$scope.uploadCCFlag=true;
	$scope.uploadInsrncFlag=true;
	$scope.uploadPSFlag=true;
	$scope.uploadRCFlag=true;
	
	$scope.vehVenDBFlag = true;
	 $scope.ownId=0;
     $scope.vvId=0;
     $scope.vv={};
     $scope.add={};
     $scope.own={};
     
     var rc=0;
    // var tc=0;
     var pan=0;
     var ps=0;
     var policy=0;
     var dec=0;
     var chq=0;
	
	
	$scope.vehVenSubmit = function() {
		console.log("vehVenSubmit");
		
		var response = $http.post($scope.projectName+'/getVehicleVendorByRC', $scope.vehVenTemp.rcNo);
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getVehicleVendor Success");
				$scope.vt=data.vt;
				$scope.isUrOwner=data.isUrOwner;
				$scope.vehVenDetailFlag = true;
				if($scope.isUrOwner===true){
					$scope.transferButtonFlag=true;
				}else{
					$scope.transferButtonFlag=false;
				}
				
				$scope.vehVendor = data.vehVendor;
				$scope.vv.vvRcNo=$scope.vehVendor.vvRcNo;
			}else {
				$scope.alertToast("Vehicle not found");
				console.log(data.result);
				$scope.vehVenDetailFlag = false;
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getVehicleVendor Error: "+data);
			$scope.vehVenDetailFlag = false;
		});
	}
	
	
	$scope.newOwnerDetail=function(){
		$scope.transferFlag=true;
	}
	
	
	$scope.verifyPan=function(panNo){
		console.log("Enter into verifyPan()"+panNo);
		if(panNo==undefined){
			$scope.alertToast("Please Enter valid Pan No.");
		}else if(panNo==$scope.vehVendor.owner.ownPanNo){
			$scope.alertToast("Please Enter New Owner's PAN No.");
		}else{
			var response = $http.post($scope.projectName+'/checkOwnerPan',panNo);
			response.success(function(data, status, headers, config) {
				
				if(data.result=="success"){
					$scope.existOwnerDB=false;
					//$scope.newOwnerDetailDB=false;
					$scope.successToast("Pan No Already exist");
					console.log(data.list);
					$scope.ownList=data.list;
			    	$('div#existOwnerId').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					        //$scope.existOwnerId = true;
					    }
						});
				$('div#existOwnerId').dialog('open');
					
				}else{
					//$scope.newOwnerDetailDB=true;
					$scope.ownerDetailDBFlag=true;
					$scope.successToast("Create Owner");
					$scope.existOwnerDB=true;
					//$scope.own.ownPanNo=panNo;
				}
			});
			response.error(function(data, status, headers, config) {
				console.log(data.result);
				//$scope.newOwnerDetailDB=false;
				$scope.errorToast("Please retry");
				
			});
		}
		
		
	}
	
	
	$scope.saveVehicleOwner=function(vehicleOwnerForm,vv,own,add){
		console.log("saveVehicleOwner="+vehicleOwnerForm.$invalid);
		
		 $('#saveId').attr("disabled","disabled");
	    //  $('#saveId').removeAttr("disabled");
		
		if(vehicleOwnerForm.ownPanNoName.$invalid){
			$scope.alertToast("Please Enter valid Pan No.");
			// $('#saveId').attr("disabled","disabled");
		      $('#saveId').removeAttr("disabled");
		}
		
		 own.ownPhNoList = [];
		 own.ownPhNoList.push($scope.phNo);
		/*console.log(vv);
		console.log(own);
		console.log(add);*/
		
		var finalObject={
			"owner":own,
			"vehVenMstr":vv,
			"registerAddress":add
		};
		
		console.log(finalObject);
		 var response = $http.post($scope.projectName+'/submitVehilceTrfOwner',finalObject);
		 response.success(function(data, status, headers, config){
			 if(data.result=="success"){
				$scope.alertToast("success");
				$scope.ownId=data.ownId;
				$scope.vvId=data.vvId;
				$scope.alertToast("own"+$scope.ownId);
				$scope.phNo="";
				$scope.vtName="";
				$scope.vv={};
				$scope.add={};
				$scope.own={};
				$scope.rcImage=null;
				//$scope.tcImage=null;
				$scope.panImage=null;
				$scope.psImage=null;
				$scope.insrncImage=null;
				$scope.ccImage=null;
				$scope.decImage=null;
				$scope.ownerDetailDBFlag=false;
				$scope.vehicleDBFlag=true;
				$scope.transferFlag=false;
				$scope.vehVenDetailFlag = false;
				  rc=0;
				 // tc=0;
			      pan=0;
			      ps=0;
			      policy=0;
			      dec=0;
			      chq=0;
			     $('#saveId').attr("disabled","disabled");
			 //     $('#saveId').removeAttr("disabled");
			 }else {
				 $scope.alertToast(data.msg);
		//		 $('#saveId').attr("disabled","disabled");
			      $('#saveId').removeAttr("disabled");
			 }
		 });
		 response.error(function(data, status, headers, config) {
				console.log(data.result);
				$scope.errorToast("Please retry");
			//	 $('#saveId').attr("disabled","disabled");
			      $('#saveId').removeAttr("disabled");
			});
		
	}
	
	$scope.uploadImage=function(docUploadForm){
		console.log("uploadRCImage"+$scope.rcImage);
		
		
		if($scope.rcImage == null || rc==0 ){
			$scope.alertToast("Please upload RC image");
			return;
		}
		
		/*if($scope.tcImage == null || tc==0 ){
			$scope.alertToast("Please upload TC image");
			return;
		}*/
		if( pan==0 ){
			$scope.alertToast("Please upload PAN image");
			return;
		}
		if($scope.psImage != null && ps==0 ){
			$scope.alertToast("Please upload Permit Slip ");
			return;
		}
		if($scope.insrncImage != null && policy==0 ){
			$scope.alertToast("Please upload Policy Doc");
			return;
		}
		if($scope.ccImage != null && chq==0 ){
			$scope.alertToast("Please upload Cheque image");
			return;
		}
		if($scope.decImage != null && dec==0 ){
			$scope.alertToast("Please upload Declaration image");
			return;
		}
		
		
	      $('#saveId').removeAttr("disabled");
	      $('div#docUploadDBId').dialog('close');
			
	}
	
	$scope.openUploadDB = function(){
		$scope.docUpldFlag=true;
		$('div#docUploadDBId').dialog({
			autoOpen: false,
			modal:true,
			title: "Documents Upload",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#docUploadDBId').dialog('open');
	}
	
	
	
		$scope.editExistOwner=function(own){
		console.log("editExistOwner()");
		
		var response = $http.post($scope.projectName+'/getOwnerDetail',own.ownId);
		response.success(function(data, status, headers, config) {
			
			if(data.result=="success"){
				$scope.own=data.owner;
				console.log($scope.own);
				$scope.add=data.address;
			//	$scope.newOwnerDetailDB=false;
				$scope.ownerDetailDBFlag=true;
				console.log($scope.own.ownPhNoList);
				$scope.phNo=$scope.own.ownPhNoList[0];
				console.log($scope.phNo);
				if($scope.own.ownIsPanImg){
				       pan=1;
				}if($scope.own.ownIsDecImg){
				       dec=1;
				}
				
			}else{
				//$scope.newOwnerDetailDB=false;
				$scope.ownerDetailDBFlag=true;
				$scope.errorToast("Something missing");
			}
			
		});
		
		response.error(function(data, status, headers, config) {
			console.log(data.result);
			$scope.errorToast("Network Problem");
			
		});
		$('div#existOwnerId').dialog('destroy');
		$scope.existOwnerDB=true;
		
		}
	
	
		
		$scope.stateDBFlag=true;
		$scope.distDBFlag=true;
		$scope.cityDBFlag=true;
		$scope.stnDBFlag=true;
		
		
		$('#addPinId').keypress(function(key) {
			if(key.charCode < 48 || key.charCode > 57)
				return false;
		});
		
		$('#phNoId').keypress(function(key) {
			if(key.charCode < 48 || key.charCode > 57)
				return false;
		});
		
		$scope.getState=function(){
			console.log("getState()");
			 var response= $http.post($scope.projectName+'/getStateDetails');
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						//console.log(data.list);
						$scope.stateList=data.list;
						$scope.OpenStateDB();
					}else{
						$scope.alertToast("state not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching state");
				});
			
		}
		
		
		
		$scope.OpenStateDB = function(){
			$scope.stateDBFlag=false;
			$('div#stateDB').dialog({
				autoOpen: false,
				modal:true,
				title: "State",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#stateDB').dialog('open');
		}

		$scope.saveStateCode = function(state){
			$scope.stateCode = state.stateCode;
			$scope.stateName = state.stateName;
			$scope.stateGST=state.stateGST;
			$('div#stateDB').dialog('close');
			$scope.stateDBFlag=true;
			
			
			 var response= $http.post($scope.projectName+'/getADistByStateCode',state.stateCode);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						//console.log(data.list);
						$scope.distList=data.list;
						$scope.OpenDistDB();
					}else{
						$scope.alertToast("District not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching District");
				});
			
			
		}
		
		
		$scope.OpenDistDB = function(){
			$scope.distDBFlag=false;
			$('div#distDB').dialog({
				autoOpen: false,
				modal:true,
				title: "District",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#distDB').dialog('open');
		}
		
		
		
		
		$scope.saveDist = function(dist){
			$scope.distName = dist;
			$('div#distDB').dialog('close');
			$scope.distDBFlag=true;
			var map={
					"dist":$scope.distName,
					"state":$scope.stateCode
			};
			
			 var response= $http.post($scope.projectName+'/getACityByDistName',map);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						//console.log(data.list);
						$scope.cityList=data.list;
						$scope.OpenCityDB();
					}else{
						$scope.alertToast("city not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching city");
				});
			
			
		}
		
		
		
		$scope.OpenCityDB = function(){
			$scope.cityDBFlag=false;
			$('div#cityDB').dialog({
				autoOpen: false,
				modal:true,
				title: "City",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#cityDB').dialog('open');
		}
		
		
		$scope.saveCity = function(city){
			$scope.cityName = city;
			$('div#cityDB').dialog('close');
			$scope.cityDBFlag=true;
			var map={
					"city":$scope.cityName,
					"dist":$scope.distName,
					"state":$scope.stateCode
			};
			
			 var response= $http.post($scope.projectName+'/getAStnByCityDistName',map);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						//console.log(data.list);
						$scope.stnList=data.list;
						$scope.OpenStnDB();
					}else{
						$scope.alertToast("station not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching station");
				});
			
			
		}
		
		
		$scope.OpenStnDB = function(){
			$scope.stnDBFlag=false;
			$('div#stnDB').dialog({
				autoOpen: false,
				modal:true,
				title: "Station",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#stnDB').dialog('open');
		}
		
		
		
		
		$scope.saveStn = function(stn){
						
			$scope.add.addPost=stn.stationName;
			$scope.add.addDist=stn.district;
			$scope.add.addState=$scope.stateName;
			$scope.add.addPin=stn.pinCode;
			$scope.add.addCity=stn.city;
			
			$('div#stnDB').dialog('close');
			console.log("Station"+stn.stationName);
			$scope.stnDBFlag=true;
		}
		
		
		
		
		$scope.getStnByPin=function(){
			
			if($scope.add.addPin.length < 6)
				return;
			
			var response= $http.post($scope.projectName+'/getAStnByPin',$scope.add.addPin);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					//console.log(data.list +""+data.stateName);
					$scope.stnList=data.list;
					$scope.stateName=data.stateName;
					//$scope.stateGST=data.stateGST;
					//console.log("$scope.stateGST"+$scope.stateGST);
					$scope.OpenStnDB();
				}else{
					$scope.alertToast("station not found in database");
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.alertToast("There is some problem in fetching station");
			});
			
			
		}
		
		

// imageges upload 
		
		$scope.uploadPanImage = function(panImage){
			console.log("enter into uploadPanImg function");
			var file = panImage;
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else{
				console.log('file is ' + JSON.stringify(file));
				var uploadUrl = $scope.projectName+"/upldOwnPanImgN";
				FileUploadService.uploadFileToUrl(file, uploadUrl);
				console.log("file save on server");
			      pan=1;
			}
		}
		
		
		$scope.uploadDecImage = function(decImage){
			console.log("enter into uploadDecImg function");
			var file = decImage;
			
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else{
				console.log('file is ' + JSON.stringify(file));
				var uploadUrl = $scope.projectName+"/upldOwnDecImgN";
				FileUploadService.uploadFileToUrl(file, uploadUrl);
				console.log("file save on server");
			     dec=1;
			}	
		}
		
		$scope.uploadRCImage = function(rcImage){
			console.log("enter into uploadDecImg function");
			var file = rcImage;
			
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else{
				console.log('file is ' + JSON.stringify(file));
				var uploadUrl = $scope.projectName+"/upldOwnRcImgN";
				FileUploadService.uploadFileToUrl(file, uploadUrl);
				console.log("file save on server");
				 rc=1;
			}	
		}
		
		/*$scope.uploadTCImage = function(tcImage){
			console.log("enter into uploadTcImg function");
			var file = tcImage;
			
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else{
				console.log('file is ' + JSON.stringify(file));
				var uploadUrl = $scope.projectName+"/upldOwnTcImgN";
				FileUploadService.uploadFileToUrl(file, uploadUrl);
				console.log("file save on server");
				 tc=1;
			}	
		}*/
		
		
		$scope.uploadPSImage = function(psImage){
			console.log("enter into uploadDecImg function");
			var file = psImage;
			
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else{
				console.log('file is ' + JSON.stringify(file));
				var uploadUrl = $scope.projectName+"/upldOwnPsImgN";
				FileUploadService.uploadFileToUrl(file, uploadUrl);
				console.log("file save on server");
			       ps=1;
			}	
		}
		
		$scope.uploadInsrncImage = function(insrncImage){
			console.log("enter into uploadDecImg function");
			var file = insrncImage;
			
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else{
				console.log('file is ' + JSON.stringify(file));
				var uploadUrl = $scope.projectName+"/upldOwnInsImgN";
				FileUploadService.uploadFileToUrl(file, uploadUrl);
				console.log("file save on server");
			       policy=1;
			}	
		}
		
		
		$scope.uploadCCImage = function(ccImage){
			console.log("enter into uploadDecImg function");
			var file = ccImage;
			
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else{
				console.log('file is ' + JSON.stringify(file));
				var uploadUrl = $scope.projectName+"/upldOwnChqImgN";
				FileUploadService.uploadFileToUrl(file, uploadUrl);
				console.log("file save on server");
			       chq=1;
			}	
		}
		
		
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("ViewVehicleVendorCntlr Ended");
}]);
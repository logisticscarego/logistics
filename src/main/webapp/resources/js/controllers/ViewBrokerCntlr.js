'use strict';

var app = angular.module('application');

app.controller('ViewBrokerCntlr',['$scope','$location','$http','FileUploadService',
                                 function($scope,$location,$http,FileUploadService){
	$scope.brokerDetails=true;
	$scope.brokerCodesDBFlag=true;
	
	$scope.broker = {};
	$scope.add = {};
	
	
	
	$scope.openBrokerCodeDB = function(){
		$scope.brokerCodesDBFlag = false;
    	$('div#brokerCodesDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Broker Codes",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true
			});
    	
			$('div#brokerCodesDB').dialog('open');
		}
	
	
	
	$(document).ready(function() {
		
		    
		    $('#addressC').keypress(function(e) {
	            if (this.value.length == 255) {
	                e.preventDefault();
	            }
	        });
		    
		 
		    $('#brkPPNo').keypress(function(e) {
	            if (this.value.length ==25) {
	                e.preventDefault();
	            }
	        });
		    
		  
		    
		    $('#brkPanName').keypress(function(e) {
	            if (this.value.length ==50) {
	                e.preventDefault();
	            }
	        });
		    
		 
		    
		    $('#addPinC').keypress(function(key) {
		        if(key.charCode < 48 || key.charCode > 57)
		        	return false;
		    });
		    
		   
	});
	
	
	
	
	
	$scope.saveCode = function(codes){
		$scope.brkCode = codes.code;
		$('div#brokerCodesDB').dialog("destroy");
		$scope.brokerCodesDBFlag = true;	
	}
	
	
	
	$scope.brokerView = function(brkCode){
		console.log("After submit button");
		$scope.code= $( "#brokercode" ).val();
		
		$scope.broker = {};
		$scope.add = {};
		
		if($scope.code===""){
			$scope.alertToast("Please Enter code");
		}
		else{
			$scope.brokerDetails  = false;
			var response = $http.post($scope.projectName+'/brokerDetails',brkCode);
			response.success(function(data, status, headers, config) {
				if(data.result==="success"){
					$scope.broker = data.broker;	
					if($scope.broker.brkPhNoList == null){
						$scope.broker.brkPhNoList=[];
					}
					 $scope.getCurrentAddress();
				}else{
					console.log(data.result);
				}
				
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	}
	
	 $scope.getBrokerCodes = function(){
		 console.log("I have got the lists of codes");
		   var response = $http.post($scope.projectName+'/getBrKCodesFrV');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.codeList = data.list;
			   }else{
				   console.log(data);
			   }
	    });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	 }
	 
	

	 
	 $scope.getCurrentAddress = function(){
		   console.log("getCurrentAddress------>"+$scope.code);
		   var response = $http.post($scope.projectName+'/getCurrentAddress',$scope.code);
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
			   $scope.add = data.list;
			   }else{
				console.log(data);   
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	   }
	 
	
	 
	 
	 $scope.EditBrokerSubmit = function(ViewBrokerForm){
		console.log("Enter into EditBrokerSubmit function");
		 console.log($scope.add.completeAdd);
		 console.log($scope.broker.brkName);
		 if(ViewBrokerForm.$invalid){
			 if(ViewBrokerForm.brkName.$invalid){
				 $scope.alertToast("please Enter Broker Name");
			 }else if(ViewBrokerForm.brkPanName.$invalid){
				 $scope.alertToast("please Enter Broker PAN Name");
			 }else if(ViewBrokerForm.brkPanNo.$invalid){
				 $scope.alertToast("please Enter Valid PAN No.");
			 }else if(ViewBrokerForm.brkEmailId.$invalid){
				 $scope.alertToast("please Enter correct email-Id");
			 }else if(ViewBrokerForm.brkFirmType.$invalid){
				 $scope.alertToast("please Enter Firm Type");
			 }else if(ViewBrokerForm.addressC.$invalid){
				 $scope.alertToast("please Enter correct address in current address");
			 }else if(ViewBrokerForm.addCityC.$invalid){
				 $scope.alertToast("please Enter correct city in current address");
			 }else if(ViewBrokerForm.addStateC.$invalid){
				 $scope.alertToast("please Enter correct state in current address");
			 }else if(ViewBrokerForm.addPinC.$invalid){
				 $scope.alertToast("please Enter correct pin in current address");
			 }
		 }
			 else{
			
				 $scope.viewBrokerFlag = true;
			    	$('div#viewBrokerDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					    }
						});
					$('div#viewBrokerDB').dialog('open');
			  } 
		 }
	 
	 $scope.back=function(){
		 console.log("Inside back");
		 $('div#viewBrokerDB').dialog('close');
		 $scope.viewBrokerFlag =false;
	 }
	 
	 $scope.saveBroker=function(){
			console.log("Inside saveBroker");	
			var finalObject = {
					 "currentAddress" 		: $scope.add,
				 	 "broker"               : $scope.broker
				 };
			 
			 var response = $http.post($scope.projectName+'/submitEditBroker',finalObject);
			   response.success(function(data, status, headers, config){
				   if(data.result==="success"){
					   console.log(data.result);
					   $scope.brokerDetails=true;
					   $scope.brkCode="";
					   $scope.add={};
					   $('div#viewBrokerDB').dialog('close');
					   $('input[name=brokerCode]').attr('checked',false);
					   $scope.alertToast(data.result);
				   }else{
					   console.log(data.result);
					   }
			   });
			   response.error(function(data, status, headers, config) {
					console.log(data);
				});
			 }
	 
	
	 
	 
	 
	 $scope.showPanImage=function(brk){
			
			var img={
					"imageType":"BRK",
					"id" : brk.brkId,
					"ownBrkImgType": "PAN"
			};
			
			
			var response = $http.post($scope.projectName+'/isDownloadImg', img);
			response.success(function(data, status, headers, config){
				console.log("Result = "+data.result);
				if(data.result === "success"){
					$scope.alertToast(data.result);
					var form = document.createElement("form");
			        form.method = "POST";
			        form.action = $scope.projectName + '/downloadImg';
			        form.target = "_blank";
			        document.body.appendChild(form);
			        form.submit();
				}else
					$scope.alertToast(data.msg);
			});
			response.error(function(data){
				console.log("Error in hitting /downloadImg");
			});
			
			
		}
	 
	 $scope.showDecImage=function(brk){
			
			var img={
					"imageType":"BRK",
					"id" : brk.brkId,
					"ownBrkImgType": "DEC"
			};
			
			
			var response = $http.post($scope.projectName+'/isDownloadImg', img);
			response.success(function(data, status, headers, config){
				console.log("Result = "+data.result);
				if(data.result === "success"){
					$scope.alertToast(data.result);
					var form = document.createElement("form");
			        form.method = "POST";
			        form.action = $scope.projectName + '/downloadImg';
			        form.target = "_blank";
			        document.body.appendChild(form);
			        form.submit();
				}else
					$scope.alertToast(data.msg);
			});
			response.error(function(data){
				console.log("Error in hitting /downloadImg");
			});
			
			
		}
	 
	 
	 $scope.showBankImage=function(brk){
			
			var img={
					"imageType":"BRK",
					"id" : brk.brkId,
					"ownBrkImgType": "CHQ"
			};
			
			
			var response = $http.post($scope.projectName+'/isDownloadImg', img);
			response.success(function(data, status, headers, config){
				console.log("Result = "+data.result);
				if(data.result === "success"){
					$scope.alertToast(data.result);
					var form = document.createElement("form");
			        form.method = "POST";
			        form.action = $scope.projectName + '/downloadImg';
			        form.target = "_blank";
			        document.body.appendChild(form);
			        form.submit();
				}else
					$scope.alertToast(data.msg);
			});
			response.error(function(data){
				console.log("Error in hitting /downloadImg");
			});
			
			
		}
	 
	 
	 
	 $scope.uploadBrkPanImage = function(brkPanImage){
			console.log("enter into uploadOwnChqImage function");
			var file = brkPanImage;
			console.log("file name="+file.name);
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else{
				
				  console.log('file is ' + JSON.stringify(file) + "brkId"+$scope.broker.brkId);
					var uploadUrl = $scope.projectName+"/uploadBrkPanImage";
					FileUploadService.uploadIdFileToUrl(file, uploadUrl,$scope.broker.brkId);
					
					console.log("file save on server");
				
			}
			
		}
	 
	 $scope.uploadBrkDecImage = function(brkDecImage){
			console.log("enter into uploadBrkDecImage function");
			var file = brkDecImage;
			console.log("file name="+file.name);
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else{
				
				  console.log('file is ' + JSON.stringify(file) + "brkId"+$scope.broker.brkId);
					var uploadUrl = $scope.projectName+"/uploadBrkDecImage";
					FileUploadService.uploadIdFileToUrl(file, uploadUrl,$scope.broker.brkId);
					
					console.log("file save on server");
				
			}
			
		}
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
		$scope.stateDBFlag=true;
		$scope.distDBFlag=true;
		$scope.cityDBFlag=true;
		$scope.stnDBFlag=true;
		
		
		$('#addPinId').keypress(function(key) {
			if(key.charCode < 48 || key.charCode > 57)
				return false;
		});
		
		
		$scope.getState=function(){
			console.log("getState()");
			 var response= $http.post($scope.projectName+'/getStateDetails');
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						//console.log(data.list);
						$scope.stateList=data.list;
						$scope.OpenStateDB();
					}else{
						$scope.alertToast("state not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching state");
				});
			
		}
		
		
		
		$scope.OpenStateDB = function(){
			$scope.stateDBFlag=false;
			$('div#stateDB').dialog({
				autoOpen: false,
				modal:true,
				title: "State",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#stateDB').dialog('open');
		}

		$scope.saveStateCode = function(state){
			$scope.stateCode = state.stateCode;
			$scope.stateName = state.stateName;
			$scope.stateGST=state.stateGST;
			$('div#stateDB').dialog('close');
			$scope.stateDBFlag=true;
			
			
			 var response= $http.post($scope.projectName+'/getADistByStateCode',state.stateCode);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						//console.log(data.list);
						$scope.distList=data.list;
						$scope.OpenDistDB();
					}else{
						$scope.alertToast("District not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching District");
				});
			
			
		}
		
		
		$scope.OpenDistDB = function(){
			$scope.distDBFlag=false;
			$('div#distDB').dialog({
				autoOpen: false,
				modal:true,
				title: "District",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#distDB').dialog('open');
		}
		
		
		
		
		$scope.saveDist = function(dist){
			$scope.distName = dist;
			$('div#distDB').dialog('close');
			$scope.distDBFlag=true;
			var map={
					"dist":$scope.distName,
					"state":$scope.stateCode
			};
			
			 var response= $http.post($scope.projectName+'/getACityByDistName',map);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						//console.log(data.list);
						$scope.cityList=data.list;
						$scope.OpenCityDB();
					}else{
						$scope.alertToast("city not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching city");
				});
			
			
		}
		
		
		
		$scope.OpenCityDB = function(){
			$scope.cityDBFlag=false;
			$('div#cityDB').dialog({
				autoOpen: false,
				modal:true,
				title: "City",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#cityDB').dialog('open');
		}
		
		
		$scope.saveCity = function(city){
			$scope.cityName = city;
			$('div#cityDB').dialog('close');
			$scope.cityDBFlag=true;
			var map={
					"city":$scope.cityName,
					"dist":$scope.distName,
					"state":$scope.stateCode
			};
			
			 var response= $http.post($scope.projectName+'/getAStnByCityDistName',map);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						//console.log(data.list);
						$scope.stnList=data.list;
						$scope.OpenStnDB();
					}else{
						$scope.alertToast("station not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching station");
				});
			
			
		}
		
		
		$scope.OpenStnDB = function(){
			$scope.stnDBFlag=false;
			$('div#stnDB').dialog({
				autoOpen: false,
				modal:true,
				title: "Station",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#stnDB').dialog('open');
		}
		
		
		
		
		$scope.saveStn = function(stn){
						
			$scope.add.addPost=stn.stationName;
			$scope.add.addDist=stn.district;
			$scope.add.addState=$scope.stateName;
			$scope.add.addPin=stn.pinCode;
			$scope.add.addCity=stn.city;
			
			$('div#stnDB').dialog('close');
			console.log("Station"+stn.stationName);
			$scope.stnDBFlag=true;
		}
		
		
		
		
		$scope.getStnByPin=function(){
			
			if($scope.add.addPin.length < 6)
				return;
			
			var response= $http.post($scope.projectName+'/getAStnByPin',$scope.add.addPin);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					//console.log(data.list +""+data.stateName);
					$scope.stnList=data.list;
					$scope.stateName=data.stateName;
					//$scope.stateGST=data.stateGST;
					//console.log("$scope.stateGST"+$scope.stateGST);
					$scope.OpenStnDB();
				}else{
					$scope.alertToast("station not found in database");
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.alertToast("There is some problem in fetching station");
			});
			
			
		}
		
	 
	 
	 
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getBrokerCodes();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	 
	
	 
}]);
'use strict';

var app = angular.module('application');

app.controller('CustStationaryCntlr',['$scope','$location','$http','$filter','$window',
                                 function($scope,$location,$http,$filter,$window){

	console.log("CustStationaryCntlr Started");
	
	$scope.branchList = [];
	$scope.branch = {};
	
	$scope.frmStnryNo = "";
	$scope.toStnryNo = "";
	$scope.stnryType = "";
	$scope.stnryPrefix = "";
	$scope.stnrySuffix = "";
	$scope.noOfDgt=6;
	
	$scope.branchDBFlag = true;
	
	$scope.getActiveBrList = function() {
		console.log("getActiveBrList Entered");
		var response = $http.post($scope.projectName+'/getUserBrNBrList');
		response.success(function(data, status, headers, config){
			console.log(data.result);
			if (data.result === "success") {
				console.log("getActiveBrList result Success");
				$scope.branchList = data.branchList;
				$scope.branch = data.userBranch;
			}else {
				$scope.alertToast("getActiveBrNCI: "+data.result);
				console.log("getActiveBrList result Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("Error in response getActiveBrList: "+data);
		});
	}
	
	$scope.openBranchDB = function() {
		console.log("openBranchDB()");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});
		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch = function(branch){
		console.log("saveBranch()");
		$scope.branch = branch;
		$('div#branchDB').dialog('close');
	}
	
	$scope.submitCustStnry = function(custStnryForm) {
		console.log("submitCustStnry()");
		if (custStnryForm.$invalid) {
			console.log("invalidate: "+custStnryForm.$invalid);
			if (custStnryForm.branchName.$invalid) {
				$scope.alertToast("please enter valid branchName");
			} else if(custStnryForm.frmStnryNoName.$invalid){
				$scope.alertToast("please enter valid Type From Stationary No");
			} else if (custStnryForm.toStnryNoName.$invalid) {
				$scope.alertToast("please enter valid Type To Stationary No");
			} else if (custStnryForm.stnryTypeName.$invalid) {
				$scope.alertToast("please enter valid Type To Stationary Type");
			} else if (custStnryForm.noOfDgtName.$invalid) {
				$scope.alertToast("please enter valid No.of digit can't be greater than 9");
			} 
		} else {
			if($scope.noOfDgt>=10 || $scope.noOfDgt<=0){
				$scope.alertToast("please enter valid No.of digit should be 1 to 9");
			}else{

				console.log("invalidate: "+custStnryForm.$invalid);
				
				var custStnryService = {
						"branchId"		: $scope.branch.branchId,
						"frmStnryNo"	: $scope.frmStnryNo,
						"toStnryNo"		: $scope.toStnryNo,
						"stnryPrefix"	: $scope.stnryPrefix,
						"stnrySuffix"	: $scope.stnrySuffix,
						"stnryType"		: $scope.stnryType,
						"noOfDgt"		: $scope.noOfDgt
				}
				
				
				$('#submitId').attr("disabled","disabled");
				var res = $http.post($scope.projectName+'/saveCustStnry', custStnryService);
				res.success(function(data, status, headers, config) {
					if (data.result === "success") {
						console.log("saveCustStnry result: "+data.result);
						$scope.alertToast(data.result);
						//clear data
						$scope.frmStnryNo = "";
						$scope.toStnryNo = "";
						$scope.stnryType = 'cnmt';
						$scope.stnryPrefix = "";
						$scope.stnrySuffix = "";
						$scope.noOfDgt=6;
						$('#submitId').removeAttr("disabled");
					} else {
						console.log("saveCustStnry result: "+data.result);
						$scope.alertToast(data.msg);
						$scope.enquiryList = [];
						$('#submitId').removeAttr("disabled");
					}
				});
				res.error(function(data, status, headers, config) {
					console.log("error in response saveCustStnry: "+data);
					$('#submitId').removeAttr("disabled");
				});
			
			}
		}
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getActiveBrList();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("CustStationaryCntlr Ended");
}]);
'use strict';

var app = angular.module('application');

app.controller('CashDepositeCntlr',['$scope','$location','$http','$filter',
                                  function($scope,$location,$http,$filter){
	
	
	$scope.bankCodeDBFlag = true;
	$scope.vs = {};
	$scope.bankCodeList =[];
	$scope.saveVsFlag = true;
	
	$scope.printVsFlag = true;
	
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		$scope.saveVsFlag = true;
		var response = $http.post($scope.projectName+'/getCashDepositeDet');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.bankCodeList = data.bankCodeList;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");
				$scope.vs.voucherType = $scope.CASH_DEPOSITE;
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	
	$scope.openBankCodeDB = function(){
		$scope.bankCodeDBFlag = false;
		$('div#bankCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bank Code",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.bankCodeDBFlag = true;
			}
		});

		$('div#bankCodeDB').dialog('open');
	}  
	
	
	 $scope.saveBankCode =  function(bankCode){
			console.log("enter into saveBankCode----->"+bankCode);
			$scope.vs.bankCode = bankCode;
			$('div#bankCodeDB').dialog('close');
			$scope.bankCodeDBFlag = true;
	}
	 
	 
	 $scope.voucherSubmit = function(newVoucherForm){
			if(newVoucherForm.$invalid){
				$scope.alertToast("Error in form");
			}else{
				if($scope.vs.amount <= 0){
					$scope.alertToast("please enter a valid cheque amount");
				}else{
					console.log("final submittion");
					$scope.saveVsFlag = false;
			    	$('div#saveVsDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					    }
						});
					$('div#saveVsDB').dialog('open');
				}
			}
		};
		
	
	$scope.back = function(){
		console.log("enter into back function");
		$scope.saveVsFlag = true;
		$('div#saveVsDB').dialog('close');
	}	
	
	
	$scope.saveVS = function(){
		console.log("enter into saveVs function");
		$('div#saveVsDB').dialog('close');
		$('#saveId').attr("disabled","disabled");
		var response = $http.post($scope.projectName+'/submitCashD',$scope.vs);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("success");
				$('#saveId').removeAttr("disabled");
				$scope.alertToast("voucher generated successfully");
				$scope.vs.vhNo = data.vhNo;
				$scope.vs.tvNo = data.tvNo;
				
				$scope.vs = {};
				$scope.cNo = "";
				$scope.getVoucherDetails();
			}else{
				console.log("Error in submitVoucher");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}

	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
}]);
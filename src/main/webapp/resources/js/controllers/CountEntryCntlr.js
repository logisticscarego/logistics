'use strict';

var app = angular.module('application');

app.controller('CountEntryCntlr',['$scope','$location','$http','AuthorisationService',
                                 function($scope,$location,$http,AuthorisationService){

	console.log("CountEntryCntlr Started");
	
	$scope.branchList = [];
	$scope.count = {};
	$scope.branchDBFlag = true;
	$scope.countFlag = false;
	$scope.branchCode = "";
	$scope.date = "";
	
	$scope.openBranchDB = function() {
		console.log("openBranchDB()");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});
		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch = function(branch){
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog('close');
		$scope.branchCode = branch.branchCode;		
	}
	
	$scope.submitCount = function(){
		console.log("Enter into submitCount()...");
		var data = {
				"brhCode"	:	$scope.branchCode,
				"date"		:	$scope.date
		}
		var response = $http.post($scope.projectName+'/getEntryCount', data);
		response.success(function(data, status, headers, config){
			if(data.result = "success"){
				$scope.alertToast(data.result);
				$scope.countFlag = true;	
				$scope.count.cnmtCount = data.cnmtCount;
				$scope.count.chlnCount = data.chlnCount;
				$scope.count.ownCount = data.ownCount;
				$scope.count.brkCount = data.brkCount;
				$scope.count.vhCount = data.vhCount;
			}else{
				$scope.alertToast(data.msg);
			}
		});
		response.error(function(data, status, headers, config){
			$scope.errorToast(data);
		});
		console.log("Exit from submitCount()...");
	}
	
	$scope.getBranchData = function(){
		  console.log("getBranchData------>");
		  var response = $http.post($scope.projectName+'/getBranchDataForChallan');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.branchList = data.list;				  
			  }else{
				  $scope.alertToast("you don't have any active branch");
				  console.log(data);
			  }
		  });
		  response.error(function(data, status, headers,config) {
			  $scope.errorToast(data);
		  });
	}
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getBranchData();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("CountEntryCntlr Ended");
}]);
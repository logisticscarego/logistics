'use strict';

var app = angular.module('application');
app.controller('EmployeeCntlr',['$scope','$location','$http','$window','$filter',
                                function($scope,$location,$http,$window,$filter){
	
	$scope.employee = {};
	$scope.availableTags = [];
	$scope.branchCodeDBFlag = true;
    
	var emplrEsi;
	var basic;
	var hra;
	var pf;
	var gross;
	var netsalary;
	var esi;
	var otherallowance = 0;
	$scope.branch={};
	
	$scope.presentAddrDBFlag = true;
	$scope.viewEmpDetailsFlag=true;
	
	/*$("#empBasic").attr('disabled','disabled');
	$("#empESI").attr('disabled','disabled');*/
	
	$scope.submitEmployee = function(EmployeeForm,employee){
		console.log("enter into submit function--->"+EmployeeForm.$invalid);
		if(EmployeeForm.$invalid){
			console.log("enter into if part of submit function--->");
			/*if(EmployeeForm.empbranchName.$invalid){
				$scope.alertToast("Please enter Branch Code...");
			}else */if(EmployeeForm.empFirstName.$invalid){
				$scope.alertToast("Please enter employee first name between 3-40 characters.....");
			}else if(EmployeeForm.empMiddleName.$invalid){
				$scope.alertToast("Please enter employee middle name between 3-40 characters.....");
			}else if(EmployeeForm.empLastName.$invalid){
				$scope.alertToast("Please enter employee last name between 3-40 characters.....");
			}else if(EmployeeForm.empDesignation.$invalid){
				$scope.alertToast("Please enter employee designation between 2-40 characters.....");
			}else if(EmployeeForm.empAdd.$invalid){
				$scope.alertToast("Please enter employee address between 3-255 characters.....");
			}else if(EmployeeForm.empCity.$invalid){
				$scope.alertToast("Please enter employee city between 3-40 characters.....");
			}else if(EmployeeForm.empState.$invalid){
				$scope.alertToast("Please enter employee state between 3-40 characters.....");
			}else if(EmployeeForm.empPin.$invalid){
				$scope.alertToast("Please enter PIN number of 6 digits.....");
			}else if(EmployeeForm.empPFNo.$invalid){
				$scope.alertToast("Please enter employee PF number between 8-18 characters...");
			}else if(EmployeeForm.empESINo.$invalid){
				$scope.alertToast("Please enter employee ESI number between 3-40 characters...");
			}else if(EmployeeForm.empESIDispensary.$invalid){
				$scope.alertToast("Please enter employee ESI dispensary between 3-40 characters...");
			}else if(EmployeeForm.empNomineeFirstName.$invalid){
				$scope.alertToast("Please enter employee nominee first name between 3-40 characters...");
			}else if(EmployeeForm.empNomineeLastName.$invalid){
				$scope.alertToast("Please enter employee nominee last name between 3-40 characters...");
			}else if(EmployeeForm.empEduQuali.$invalid){
				$scope.alertToast("Please enter employee educational qualification between 2-255 characters...");
			}else if(EmployeeForm.empPresentAdd.$invalid){
				$scope.alertToast("Please enter present address of employee between 3-255 characters.....");
			}else if(EmployeeForm.empPresentCity.$invalid){
				$scope.alertToast("Please enter present city of employee between 3-40 characters.....");
			}else if(EmployeeForm.empPresentState.$invalid){
				$scope.alertToast("Please enter present state of employee between 3-40 characters.....");
			}else if(EmployeeForm.empPresentPin.$invalid){
				$scope.alertToast("Please enter present PIN number of employee of 6 digits...");
			}else if(EmployeeForm.empSex.$invalid){
				$scope.alertToast("Please enter employeee sex...");
			}else if(EmployeeForm.empFatherFirstName.$invalid){
				$scope.alertToast("Please enter father's first name of employee between 3-40 characters...");
			}else if(EmployeeForm.empFatherLastName.$invalid){
				$scope.alertToast("Please enter father's last name of employee between 3-40 characters...");
			}else if(EmployeeForm.empLicenseNo.$invalid){
				$scope.alertToast("Please enter employee licence number between 3-40 characters...");
			}else if(EmployeeForm.empBankAcNo.$invalid){
				$scope.alertToast("Please enter correct format of Bank Account number between 10-40 characters...");
			}else if(EmployeeForm.empPanNo.$invalid){
				$scope.alertToast("Please enter correct format of PAN number of 10 characters...");
			}else if(EmployeeForm.empMailId.$invalid){
				$scope.alertToast("Please enter correct format of email id...");
			}else if(EmployeeForm.empPhNo.$invalid){
				$scope.alertToast("Please enter phone number of 4-15 digits....");
			}else if(EmployeeForm.empBankIFS.$invalid){
				$scope.alertToast("Please enter employee Bank IFSC between 3-40 characters....");
			}else if(EmployeeForm.empBankName.$invalid){
				$scope.alertToast("Please enter employee bank name between 3-40 characters....");
			}else if(EmployeeForm.empBankBranch.$invalid){
				$scope.alertToast("Please enter employee bank branch between 3-40 characters....");
			}
		}else{
				if($scope.employee.empDOB>$scope.employee.empDOAppointment){
					$scope.alertToast("Please enter valid date of appointment...");
				}else{
						if(angular.isUndefined(employee.empmiddlename) || employee.empmiddlename === null || employee.empmiddlename === ""){
							if (angular.isUndefined(employee.emplastname) || employee.emplastname === null || employee.emplastname === "") {
								$scope.employee.empName = employee.empfirstname;
								employee.empName=employee.empName.toUpperCase();
							} else {
								$scope.employee.empName = employee.empfirstname+" "+employee.emplastname;
								employee.empName=employee.empName.toUpperCase();
							}
						}else {
							if (angular.isUndefined(employee.emplastname) || employee.emplastname === null || employee.emplastname === "") {
								$scope.employee.empName = employee.empfirstname+" "+employee.empmiddlename;
								employee.empName=employee.empName.toUpperCase();
							} else {
								$scope.employee.empName = employee.empfirstname+" "+employee.empmiddlename+" "+employee.emplastname;
								employee.empName=employee.empName.toUpperCase();
							}
							
						}
						
						if (angular.isUndefined(employee.fatherFirstName) || employee.fatherFirstName === null || employee.fatherFirstName === "") {
						
							if (angular.isUndefined(employee.fatherLastName) || employee.fatherLastName === null || employee.fatherLastName === "") {
								//do nothing
							} else {
								$scope.employee.empFatherName = employee.fatherLastName;
								employee.empFatherName=employee.empFatherName.toUpperCase();
							}
						} else {
							if (angular.isUndefined(employee.fatherLastName) || employee.fatherLastName === null || employee.fatherLastName === "") {
								$scope.employee.empFatherName = employee.fatherFirstName;
								employee.empFatherName=employee.empFatherName.toUpperCase();
							} else {
								$scope.employee.empFatherName = employee.fatherFirstName+" "+employee.fatherLastName;
								employee.empFatherName=employee.empFatherName.toUpperCase();
							}
						}
						
						if (angular.isUndefined(employee.nomineeFirstName) || employee.nomineeFirstName === null || employee.nomineeFirstName === "") {
							
							if (angular.isUndefined(employee.nomineeLastName) || employee.nomineeLastName === null || employee.nomineeLastName === "") {
								//do nothing
							} else {
								$scope.employee.empNominee = employee.nomineeLastName;
								employee.empNominee=employee.empNominee.toUpperCase();
							}
						} else {
							if (angular.isUndefined(employee.nomineeLastName) || employee.nomineeLastName === null || employee.nomineeLastName === "") {
								$scope.employee.empNominee = employee.nomineeFirstName;
								employee.empNominee=employee.empNominee.toUpperCase();
							} else {
								$scope.employee.empNominee = employee.nomineeFirstName+" "+employee.nomineeLastName;
								employee.empNominee=employee.empNominee.toUpperCase();
							}
						}
						
						 $scope.viewEmpDetailsFlag = false;
							$('div#viewEmpDetailsDB').dialog({
								autoOpen: false,
								modal:true,
								resizable: false,
								position: UDPos,
								show: UDShow,
								hide: UDHide,
								title: "View Employee Details",
								draggable: true,
								close: function(event, ui) { 
									$(this).dialog('destroy');
									$(this).hide();
								}
							});
							$('div#viewEmpDetailsDB').dialog('open');
				}
			}
	}
	
	$scope.saveEmployee = function(employee){
		 $('#saveId').attr("disabled","disabled");
		console.log("Entered into saveEmployee function----> ");
		var response = $http.post($scope.projectName+'/submitEmployee', employee);
		response.success(function(data, status, headers, config) {
	
			if(data.result==="success"){
				$('div#viewEmpDetailsDB').dialog('close');
				$scope.successToast(data.result);
				console.log("employee code is --->>"+data.result);
				$('#saveId').removeAttr("disabled");
				/*$("#empBasic").attr('disabled','disabled');
				$("#empESI").attr('disabled','disabled');*/
				/*$scope.employee.userCode = "";
				$scope.employee.branchCode="";
				$scope.employee.isNCR="";
				$scope.employee.empfirstname="";
				$scope.employee.empmiddlename="";
				$scope.employee.emplastname="";
				$scope.employee.empCode = "";
				$scope.employee.empDesignation="";
				$scope.employee.empSex="";
				$scope.employee.empDOB="";
				$scope.employee.empDOAppointment="";
				$scope.employee.empEduQuali="";
				$scope.employee.fatherFirstName="";
				$scope.employee.fatherLastName="";
				$scope.employee.nomineeFirstName="";
				$scope.employee.nomineeLastName="";
				$scope.employee.empAdd="";
				$scope.employee.empCity="";
				$scope.employee.empState="";
				$scope.employee.empPin="";
				$scope.employee.empPresentAdd="";
				$scope.employee.empPresentCity="";
				$scope.employee.empPresentState="";
				$scope.employee.empPresentPin="";
				$scope.employee.empBasic="";
				$scope.employee.empHRA="";
				$scope.employee.empOtherAllowance=0;
				$scope.employee.empGross="";
				$scope.employee.netSalary="";
				$scope.employee.empESI="";
				$scope.employee.empESINo="";
				$scope.employee.empESIDispensary="";
				$scope.employee.empPF="";
				$scope.employee.empPFNo="";
				$scope.employee.empLoanBal="";
				$scope.employee.empLoanPayment="";
				$scope.employee.empLicenseNo="";
				$scope.employee.empLicenseExp="";
				$scope.employee.empMailId="";
				$scope.employee.empPhNo="";
				$scope.employee.empTelephoneAmt="";
				$scope.employee.empMobAmt="";
				$scope.employee.empBankAcNo="";
				$scope.employee.empBankName="";
				$scope.employee.empBankBranch="";
				$scope.employee.empBankIFS="";
				$scope.employee.empPanNo="";
				$scope.empbranchName="";*/
				$scope.employee = {};
				
				//$('input[name=branchName]').attr('checked',false);
			}else{
				console.log(data);
			}		
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
	    });
	}
	
	$scope.closeViewEmpDetailsDB = function(){
		$('div#viewEmpDetailsDB').dialog('close');
	}
	   
	$scope.getEmpDesigList=function(employee){
	      $( "#empDesignation" ).autocomplete({
	    	  source: $scope.availableTags
	      });
	} 
	
	$scope.fillEmpDesigVal = function(){
		console.log("enter into myfun----------->>"+$( "#empDesignation" ).val());
		$scope.employee.empDesignation=$( "#empDesignation" ).val();
	}
		
	$scope.getDesignation = function(){
		var response = $http.get($scope.projectName+'/getDesignation');
		response.success(function(data, status, headers, config) {
			$scope.availableTags = data;
			console.log("******************");
	    });
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.saveBranchCode =  function(branch){
		console.log("enter into saveBranchCode----->"+branch.branchCode);
		$scope.employee.branchCode = branch.branchCode;
		$scope.employee.isInNCR=branch.isNCR;
		$scope.empbranchName=branch.branchName;
		$("#empBasic").removeAttr('disabled');
		console.log("Branch isNCR----->"+branch.isNCR);
		$('div#branchCodeDB').dialog('close');
		$scope.branchCodeDBFlag = true;
	}
	
	
	$scope.getBranchData = function(){
		   console.log("getBranchData------>");
		   var response = $http.post($scope.projectName+'/getBranchDataForEmp');
		   response.success(function(data, status, headers, config){
			   if(data.result==="success"){
					$scope.branchList = data.list;
					$scope.getDesignation();
				}else{
					console.log(data);
				}	
			 
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
		   });
	}
	
   $scope.openBranchPage = function(){
	   console.log("**************Entered into openBranchPage*************");
	   $('div#branchCodeDB').dialog('close');
	   $scope.branchCodeDBFlag = true;
	   $location.path('/operator/branch');
   }
   
   $scope.calculateOnBasic = function(){	   
	   
	   basic=$scope.employee.empBasic;
	   /*hra=0.4*basic;	   	   
	   if($scope.employee.isInNCR==="yes"){
		   hra=0.5*basic; 
	   }else if($scope.employee.isInNCR==="no"){
		   hra=0.4*basic;
	   }*/
	   	     
	   pf=0.12*basic;
	   //gross=parseInt(basic)+parseInt(hra)+parseInt(pf)+parseInt(otherallowance);
	  
	   //$scope.employee.empHRA=hra;
	   $scope.employee.empPF=pf;
	   $scope.employee.empPFEmplr = pf;
	   //$scope.employee.empGross=gross;
	   
	   /*if(gross<=14999)
	   {
	    esi=0.065*gross;
	    netsalary=((parseInt(basic)+parseInt(hra)+parseInt(otherallowance))-((parseInt(basic)+parseInt(hra)+parseInt(otherallowance))*0.0175+0.12*parseInt(basic)));
	    $scope.employee.empESI=esi;
	    $("#empESI").removeAttr('disabled');
	    $scope.employee.netSalary=netsalary;
	   } 
	   else if(gross>=15000){
		   $scope.employee.empESI="";
		   $("#empESI").attr('disabled','disabled');
		   netsalary=(parseInt(basic)+parseInt(hra)+parseInt(otherallowance))-(0.12*parseInt(basic));
		   $scope.employee.netSalary=netsalary;
	   }*/
   }
   
   $scope.calGross = function(){
	   console.log("enter into calGross function");
	   gross = parseFloat($scope.employee.empBasic) + parseFloat($scope.employee.empHRA) + parseFloat($scope.employee.empOtherAllowance);
	   $scope.employee.empGross = gross;
	   
	   if(gross <= 15000){
		   esi=0.0175*gross;
		   emplrEsi = 0.0475*gross;
		   $scope.employee.empESIEmplr = emplrEsi; 
		   $scope.employee.empESI = esi;
	   }
   }
   
   
   $scope.calNetSal = function(){
	   console.log("enter into calNetSal funciton");
	   netsalary = parseFloat($scope.employee.empGross) - parseFloat($scope.employee.empPF) - parseFloat($scope.employee.empESI) - parseFloat($scope.employee.empTds);
	   $scope.employee.netSalary = netsalary;
   }
   
   
   $scope.calculateOnOthers = function(value){
	   if(angular.isUndefined(value) || value === null || value ===""){
		   otherallowance =0;
	   }else{
		   otherallowance = parseInt(value);
       }
	   $scope.calculateOnBasic();
   }
   
   $scope.openBranchCodeDB = function(){
	   $scope.branchCodeDBFlag = false;
		$('div#branchCodeDB').dialog({
		autoOpen: false,
		modal:true,
		resizable: false,
		position: UDPos,
		show: UDShow,
		hide: UDHide,
		title: "Branch Code",
		draggable: true,
	    close: function(event, ui) { 
            $(this).dialog('destroy');
            $(this).hide();
        }
		});
		
		$('div#branchCodeDB').dialog('open');
   }
   
   $('#empPin').keypress(function(key) {
       if(key.charCode < 48 || key.charCode > 57)
           return false;
   });


   $('#empPin').keypress(function(e) {
	   if (this.value.length == 6) {
           e.preventDefault();
       }
   });
   
   $('#empPresentPin').keypress(function(key) {
       if(key.charCode < 48 || key.charCode > 57)
           return false;
   });

   $('#empHRAId').keypress(function(key) {
       if(key.charCode < 48 || key.charCode > 57)
           return false;
   });

   $('#empPresentPin').keypress(function(e) {
       if (this.value.length == 6) {
           e.preventDefault();
       }
   });
   
   $('#empBasic').keypress(function(key) {
       if(key.charCode < 46 || key.charCode > 57)
           return false;
   });


   $('#empBasic').keypress(function(e) {
	   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
           e.preventDefault();
       }
   });
   
   $('#empLoanBal').keypress(function(key) {
       if(key.charCode < 46 || key.charCode > 57)
           return false;
   });


   $('#empLoanBal').keypress(function(e) {
	   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
           e.preventDefault();
       }
   });
   
   $('#empLoanPayment').keypress(function(key) {
       if(key.charCode < 46 || key.charCode > 57)
           return false;
   });


   $('#empLoanPayment').keypress(function(e) {
	   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
           e.preventDefault();
       }
   });
     
   $('#empTelephoneAmt').keypress(function(key) {
       if(key.charCode < 46 || key.charCode > 57)
           return false;
   });


   $('#empTelephoneAmt').keypress(function(e) {
	   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
           e.preventDefault();
       }
   });
      
   $('#empMobAmt').keypress(function(key) {
       if(key.charCode < 46 || key.charCode > 57)
           return false;
   });

   $('#empMobAmt').keypress(function(e) {
	   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
           e.preventDefault();
       }
   });
   
   $('#empPhNo').keypress(function(key) {
       if(key.charCode < 48 || key.charCode > 57)
           return false;
   });


   $('#empPhNo').keypress(function(e) {
       if (this.value.length == 15) {
           e.preventDefault();
       }
   });
   
   $('#empBankAcNo').keypress(function(key) {
       if(key.charCode < 48 || key.charCode > 57)
           return false;
   });


   $('#empBankAcNo').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   $('#empOtherAllowance').keypress(function(key) {
       if(key.charCode < 46 || key.charCode > 57)
           return false;
   });


   $('#empOtherAllowance').keypress(function(e) {
	   if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
           e.preventDefault();
       }
   });
   
   $('#empPanNo').keypress(function(e) {
       if (this.value.length == 10) {
           e.preventDefault();
       }
   });
   
   $('#empFirstName').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   /*$('#empFirstName').bind('copy paste cut',function(e) { 
	   e.preventDefault(); 
   });*/
      
   $('#empMiddleName').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   $('#empLastName').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   $('#empDesignation').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   $('#empEduQualification').keypress(function(e) {
       if (this.value.length == 255) {
           e.preventDefault();
       }
   });
   
   $('#empFatherFirstName').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   $('#empFatherLastName').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   $('#empNomineeFirstName').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   $('#empNomineeLastName').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
     
   $('#empAdd').keypress(function(e) {
       if (this.value.length == 255) {
           e.preventDefault();
       }
   });
   
   $('#empCity').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   $('#empState').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   $('#empPresentAdd').keypress(function(e) {
       if (this.value.length == 255) {
           e.preventDefault();
       }
   });
   
   $('#empPresentCity').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   $('#empPresentState').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   $('#empESINo').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   $('#empESIDispensary').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   $('#empPFNo').keypress(function(e) {
       if (this.value.length == 18) {
           e.preventDefault();
       }
   });
   
   $('#empLicenseNo').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   $('#empMailId').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   $('#empBankName').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   $('#empBankBranch').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
   
   $('#empBankIFS').keypress(function(e) {
       if (this.value.length == 40) {
           e.preventDefault();
       }
   });
       
  $scope.chkPresentAddrDB = function(){
	   if(angular.isUndefined($scope.employee.empAdd) || $scope.employee.empAdd === null || $scope.employee.empAdd === ""){
		   $scope.alertToast("Please enter employee address");
	   }else if(angular.isUndefined($scope.employee.empCity) || $scope.employee.empCity === null || $scope.employee.empCity === ""){
		   $scope.alertToast("Please enter employee city");
	   }else if(angular.isUndefined($scope.employee.empState) || $scope.employee.empState === null || $scope.employee.empState === ""){
		   $scope.alertToast("Please enter employee state");
	   }else if(angular.isUndefined($scope.employee.empPin) || $scope.employee.empPin === null || $scope.employee.empPin === ""){
		   $scope.alertToast("Please enter employee pin");
	   }else{
		   $scope.presentAddrDBFlag = false;
			$('div#presentAddrDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "Employee Present Address",
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
				}
			});
			$('div#presentAddrDB').dialog('open');
	   }
  }
  
  $scope.yesPresentAddr = function(){
	  console.log("Entered into yesPresentAddr function");
	  $scope.employee.empPresentAdd = $scope.employee.empAdd;
	  $scope.employee.empPresentCity = $scope.employee.empCity;
	  $scope.employee.empPresentState = $scope.employee.empState;
	  $scope.employee.empPresentPin = $scope.employee.empPin;
	  $('div#presentAddrDB').dialog('close');
	  $scope.presentAddrDBFlag = true;
  }
  
  $scope.YesNoAddr = function(){
	  console.log("Entered into YesNoAddr function");
	  $('div#presentAddrDB').dialog('close');
	  $scope.presentAddrDBFlag = true;
  }
   
  
  if($scope.operatorLogin === true || $scope.superAdminLogin === true){
	  //$scope.getBranchData();
	  $scope.getDesignation();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
   
   
}]);
'use strict';

var app = angular.module('application');

app.controller('LhpvAdvTempCntlr',['$scope','$location','$http','$filter','$window','AuthorisationService',
                                  function($scope,$location,$http,$filter,$window,AuthorisationService){
	
	$scope.vs = {};
	$scope.lhpvAdv = {};
	$scope.bnkList = [];
	$scope.chlnList = [];
	$scope.actBOList = [];
	$scope.lhpvAdvList = [];
	
	$scope.bankCodeDBFlag = true;
	$scope.chqNoDBFlag = true;
	$scope.brkOwnDBFlag = true;
	$scope.chlnDBFlag = true;
	$scope.lhpvAdvDBFlag = true;
	$scope.saveVsFlag = true;
	
	
	$scope.curUsrCode = AuthorisationService.getUserCode();
	console.log("$scope.curUsrCode = "+$scope.curUsrCode);
	
	$scope.getLhpvDet = function(){
		console.log("enter into getLhpvDet function");
		var response = $http.post($scope.projectName+'/getLhpvDetTemp');
		  response.success(function(data, status, headers, config){
			   if(data.result === "success"){
				   $scope.vs.branch = data.branch;
				   //$scope.vs.lhpvStatus = data.lhpvStatus;
				   $scope.vs.voucherType = $scope.LHPV_ADV_VOUCHER;
				   //$scope.dateTemp = $filter('date')(data.lhpvStatus.lsDt, "yyyy-MM-dd'");
				   $scope.bnkList = data.bnkList;
				   $scope.getChlnAndBrOwn();
				   /*if($scope.vs.lhpvStatus.lsClose === true){
					   $scope.alertToast("You already close the LHPV of "+$scope.dateTemp);
				   }else{
					   $scope.getChlnAndBrOwn();
				   }*/
			   }else{
				   console.log("error in fetching getLhpvDet data");
				   $scope.alertToast("Server Error");
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	
	$scope.getChlnAndBrOwn = function(){
		console.log("enter into getChlnAndBrOwn function");
		var response = $http.post($scope.projectName+'/getChlnAndBrOwnTemp');
		  response.success(function(data, status, headers, config){
			   if(data.result === "success"){
				   $scope.chlnList = data.chlnList;
				   $scope.actBOList = data.actBOList;
			   }else{
				   console.log("error in fetching getChlnAndBrOwn data");
				   $scope.alertToast("Server Error");
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	$scope.selectPayBy = function(){
		console.log("enter into selectPayMod function with vs.payBy ="+$scope.vs.payBy);
		if($scope.vs.payBy === 'C'){
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
			$scope.vs.bankCode = "";
			$('#bankCodeId').attr("disabled","disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
		}else if($scope.vs.payBy === 'Q'){
			$('#chequeTypeId').removeAttr("disabled");
			$scope.vs.bankCode = "";
			$('#bankCodeId').attr("disabled","disabled");
		}else if($scope.vs.payBy === 'R'){
			$('#bankCodeId').removeAttr("disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
		}else{
			console.log("invaid entry");
		}
	}
	
	
	$scope.selectChqType = function(){
		console.log("enter into selectChqType function");
		$('#bankCodeId').removeAttr("disabled");
	}
	
	
	$scope.openBankCodeDB = function(){
		console.log("enter into openBankCodeDB function");
		$scope.bankCodeDBFlag = false;
		$('div#bankCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Bank Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.bankCodeDBFlag = true;
		    }
			});
		
		$('div#bankCodeDB').dialog('open');	
	}
	
	
	$scope.saveBankCode =  function(bankCode){
		console.log("enter into saveBankCode----->"+bankCode);
		$scope.vs.bankCode = bankCode;
		$('div#bankCodeDB').dialog('close');
		$scope.bankCodeDBFlag = true;
			
		if($scope.vs.payBy === 'Q'){
			if(angular.isUndefined($scope.vs.chequeType) ||  $scope.vs.chequeType === null || $scope.vs.chequeType === ""){
				$scope.alertToast("please select cheque type");
			}else{
				var chqDet = {
						"bankCode" : bankCode,
						"CType"    : $scope.vs.chequeType
				};
				
				var response = $http.post($scope.projectName+'/getChequeNoFrLHPVTemp',chqDet);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						console.log("Cheque Leaves List--->>"+ data.chequeLeaves);
						$('#chequeNoId').removeAttr("disabled");
						$scope.vs.chequeLeaves = data.chequeLeaves;
						$scope.chqList = data.list;
						$scope.maxChqAmnt = data.chequeLeaves.chqLChqMaxLt;
					}else{
						$scope.alertToast("Branch does not have '"+$scope.vs.chequeType+"' type cheeque of Bank "+bankCode);
						console.log("Error in bringing data from getChequeNo");
					}
				});
				response.error(function(data, status, headers, config){
					console.log(data);
				});
			}
			
		}
		
 }
	
	
	 $scope.openChqNoDB = function(){
		 console.log("enter into openChqNoDB function");
		 $scope.chqNoDBFlag = false;
	    	$('div#chqNoDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.chqNoDBFlag = true;
			    }
				});
			$('div#chqNoDB').dialog('open');
	 }
	 
	 
	 $scope.saveChqNo = function(chq){
		 console.log("enter into saveChqNo function = "+chq.chqLChqNo);
		 $scope.vs.chequeLeaves = chq;
		 $('div#chqNoDB').dialog('close');
	 } 
	 
	 
	 $scope.openBrkOwnDB = function(){
		 console.log("enter into openBrkOwnDB funciton");
		 $scope.brkOwnDBFlag = false;
	    	$('div#brkOwnDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.brkOwnDBFlag = true;
			    }
				});
			$('div#brkOwnDB').dialog('open'); 
	 }
	
	 
	 $scope.saveBOCode = function(faCode){
		 console.log("enter into saveBOCode function");
		 $scope.vs.brkOwnFaCode = faCode;
		 $('div#brkOwnDB').dialog('close'); 
		 $scope.lhpvAdvList = [];
	 }
	 
	 
	 $scope.openChlnDB = function(){
		 console.log("enter into openChlnDB funciton");
		 if(angular.isUndefined($scope.vs.brkOwnFaCode) || $scope.vs.brkOwnFaCode === "" || $scope.vs.brkOwnFaCode === null){
			 $scope.alertToast("Please select Owner/Broker");
		 }else{
			 $scope.chlnDBFlag = false;
		    	$('div#chlnDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.chlnDBFlag = true;
				    }
					});
				$('div#chlnDB').dialog('open'); 
		 }
	 }
	
	 
	 $scope.saveChln = function(chlnCode){
		 console.log("enter into saveChln fucniton");
		 $scope.challan = chlnCode;
		 
		 var duplicate = false;
		 if($scope.lhpvAdvList.length > 0){
			 for(var i=0;i<$scope.lhpvAdvList.length;i++){
				if($scope.lhpvAdvList[i].challan.chlnCode === chlnCode){
					duplicate = true;
					break;
				}
			 }
		 }
		 
		 if(duplicate === false){
			 var data = {
					 "chlnCode"     : $scope.challan,
					 "brkOwnFaCode" : $scope.vs.brkOwnFaCode
			 };
			 
			 var response = $http.post($scope.projectName+'/getChlnFrLATemp',data);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						$('div#chlnDB').dialog('close'); 
						$scope.actChln = data.chln;
						
						var adv = 0.0;
						adv = data.chlnAdv;
						$scope.lhpvAdv.laLryAdvP = adv;
						$scope.actAdv = adv;
						
						$scope.lhpvAdv.laTotPayAmt = adv;
						console.log("data.isCal = "+data.isCal);
						console.log("data.csDis = "+data.csDis);
						if(data.isCal === true){
							
							//var csDis = adv * 0.01;
							var csDis = data.csDis;
							if(csDis > 0){
								$scope.lhpvAdv.laCashDiscR = csDis;
								$scope.actCashDiscR = csDis;
							}else{
								$scope.lhpvAdv.laCashDiscR = 0;
								$scope.actCashDiscR = 0;
							}
							
							
							var muns = 0.0;
							muns = data.munsAmt;
							if(muns > 0){
								$scope.lhpvAdv.laMunsR = muns;
								$scope.actMunsR = muns;
							}else{
								$scope.lhpvAdv.laMunsR = 0;
								$scope.actMunsR = 0;
							}
							
			
							var tds = 0.0;
							tds = data.tdsAmt;
							if(tds > 0){
								$scope.lhpvAdv.laTdsR = tds;
								$scope.actTdsR = tds;
							}else{
								$scope.lhpvAdv.laTdsR = 0;
								$scope.actTdsR = 0;
							}
							 
							
							$scope.lhpvAdv.laTotRcvrAmt = $scope.lhpvAdv.laCashDiscR + $scope.lhpvAdv.laMunsR + $scope.lhpvAdv.laTdsR;
							if($scope.lhpvAdv.laTotRcvrAmt < 0){
								$scope.lhpvAdv.laTotRcvrAmt = 0;
							}
							
							$scope.lhpvAdv.laFinalTot = $scope.lhpvAdv.laTotPayAmt - $scope.lhpvAdv.laTotRcvrAmt;
							if($scope.lhpvAdv.laFinalTot < 0){
								$scope.lhpvAdv.laFinalTot = 0;
							}
							
						}else{
							$scope.lhpvAdv.laCashDiscR = 0;
							$scope.actCashDiscR = 0;
							$scope.lhpvAdv.laMunsR = 0;
							$scope.actMunsR = 0;
							$scope.lhpvAdv.laTdsR = 0;
							$scope.actTdsR = 0;
							$scope.lhpvAdv.laTotRcvrAmt = 0;
							
							$scope.lhpvAdv.laFinalTot = $scope.lhpvAdv.laTotPayAmt - $scope.lhpvAdv.laTotRcvrAmt;
							if($scope.lhpvAdv.laFinalTot < 0){
								$scope.lhpvAdv.laFinalTot = 0;
							}
						}
					
						$scope.lhpvAdv.challan = $scope.actChln;
						
						$scope.lhpvAdvDBFlag = false;
					    	$('div#lhpvAdvDB').dialog({
								autoOpen: false,
								modal:true,
								resizable: false,
								show: UDShow,
								title:"LHPV ADVANCE FOR CHALLAN ("+$scope.challan+")",
								hide: UDHide,
								position: UDPos,
								draggable: true,
								close: function(event, ui) { 
							        $(this).dialog('destroy');
							        $(this).hide();
							        $scope.lhpvAdvDBFlag = true;
							    }
								});
						$('div#lhpvAdvDB').dialog('open'); 
					}else{
						$scope.alertToast("server error");
						console.log("error in fetching getChlnFrLA data");
					}
				});
				response.error(function(data, status, headers, config){
					console.log(data);
				});
		 }else{
			 $scope.alertToast("Already create the lhpv advance for challan "+$scope.challan);
		 }
	 }
	 
	 
	 
	 $scope.chngLryCD = function(){
		 console.log("enter into chngLryCD function");
		 /*if($scope.lhpvAdv.laCashDiscR < $scope.actCashDiscR){
			 $scope.alertToast("Plaese enter greater than equal to "+$scope.actCashDisR);
			 $scope.lhpvAdv.laCashDiscR = $scope.actCashDisR;
		 }*/
		 $scope.lhpvAdv.laTotRcvrAmt = $scope.lhpvAdv.laCashDiscR + $scope.lhpvAdv.laMunsR + $scope.lhpvAdv.laTdsR;
		 $scope.chngLryAdv();
	 }
	 
	 
	 $scope.chngLryMU = function(){
		 console.log("enter inot chngMU function");
		 /*if($scope.lhpvAdv.laMunsR < $scope.actMunsR){
			 $scope.alertToast("Plaese enter greater than equal to "+$scope.actMunsR);
			 $scope.lhpvAdv.laMunsR = $scope.actMunsR;
		 }*/
		 $scope.lhpvAdv.laTotRcvrAmt = $scope.lhpvAdv.laCashDiscR + $scope.lhpvAdv.laMunsR + $scope.lhpvAdv.laTdsR;
		 $scope.chngLryAdv();
	 }
	 
	 
	 $scope.chngTDS = function(){
		 console.log("enter into chngTDS function");
		 /*if($scope.lhpvAdv.laTdsR < $scope.actTdsR){
			 $scope.alertToast("Plaese enter greater than equal to "+$scope.actTdsR);
			 $scope.lhpvAdv.laTdsR = $scope.actTdsR;
		 }*/
		 $scope.lhpvAdv.laTotRcvrAmt = $scope.lhpvAdv.laCashDiscR + $scope.lhpvAdv.laMunsR + $scope.lhpvAdv.laTdsR;
		 $scope.chngLryAdv();
	 }
	 
	 
	 
	 $scope.chngLryAdv = function(){
		console.log("enter into chngLryAdv function");
		if($scope.lhpvAdv.laLryAdvP <= 0 || angular.isUndefined($scope.lhpvAdv.laLryAdvP)){
			$scope.alertToast("please fill the amount <= "+$scope.actAdv);
			$scope.lhpvAdv.laLryAdvP = 0;
			$scope.lhpvAdv.laTotPayAmt = 0;
			$scope.lhpvAdv.laFinalTot = $scope.lhpvAdv.laTotPayAmt - $scope.lhpvAdv.laTotRcvrAmt;
		}else if($scope.lhpvAdv.laLryAdvP <= $scope.actAdv){
			$scope.lhpvAdv.laTotPayAmt = $scope.lhpvAdv.laLryAdvP;
			if($scope.lhpvAdv.laTotRcvrAmt < 0){
				$scope.lhpvAdv.laTotRcvrAmt = 0;
			}
			$scope.lhpvAdv.laFinalTot = $scope.lhpvAdv.laTotPayAmt - $scope.lhpvAdv.laTotRcvrAmt;
			if($scope.lhpvAdv.laFinalTot < 0){
				$scope.lhpvAdv.laFinalTot = 0;
			}
		}else{
			$scope.alertToast("please fill the amount <= "+$scope.actAdv);
			$scope.lhpvAdv.laLryAdvP = $scope.actAdv;
			$scope.lhpvAdv.laTotPayAmt = $scope.lhpvAdv.laLryAdvP;
			$scope.lhpvAdv.laFinalTot = $scope.lhpvAdv.laTotPayAmt - $scope.lhpvAdv.laTotRcvrAmt;
		}
		
	 }
	 
	 
	 $scope.submitLhpvAdv = function(lhpvAdvForm,lhAdv){
		 console.log("enter into submitLhpvAdv function = "+lhpvAdvForm.$invalid);
		 if(lhpvAdvForm.$invalid){
			$scope.alertToast("Please fill correct form");
		 }else{
			 if($scope.lhpvAdv.laLryAdvP <= 0 && $scope.actAdv > 0){
				 $scope.alertToast("please fill the amount <= "+$scope.actAdv);
			 }else{
				 $scope.lhpvAdvList.push(lhAdv);
				 $scope.lhpvAdv = {};
				 $('div#lhpvAdvDB').dialog('close'); 
			 } 
		 }
	 }
	 
	 
	 $scope.removeLhpvAdv = function(index){
		 console.log("enter into removeLhpvAdv funciton = "+index);
		 $scope.lhpvAdvList.splice(index,1);
	 }
	 
	 
	 $scope.voucherSubmit = function(voucherForm){
		 console.log("enter into voucherForm function = "+voucherForm.$invalid);
		 if(voucherForm.$invalid){
			$scope.alertToast("Please enter correct form");
		 }else{
			 console.log("final submittion");
			    $scope.saveVsFlag = false;
		    	$('div#saveVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					title:"LHPV ADVANCE FINAL SUBMISSION",
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.saveVsFlag = true;
				    }
					});
				$('div#saveVsDB').dialog('open');
		 }
	 }
	 
	 $scope.back = function(){
		 console.log("enter into back function");
		 $('div#saveVsDB').dialog('close');
	 } 
	 
	 
	 $scope.saveVS = function(){
		 console.log("enter into saveVS function ");
		    $scope.vs.dateTemp = $scope.dateTemp
			$scope.vs.lhpvAdvList = $scope.lhpvAdvList;
			$('#saveId').attr("disabled","disabled");
			var response = $http.post($scope.projectName+'/submitLhpvAdvTemp',$scope.vs);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$('div#saveVsDB').dialog('close');
					$('#saveId').removeAttr("disabled");
					
					$scope.alertToast("Successfully save LHPV ADVANCE -->> "+data.laNo);
					
					$scope.vs = {};
					$scope.vs.payBy = 'C';
					$scope.challan = "";
					$scope.lhpvAdv = {};
					$scope.bnkList = [];
					$scope.chlnList = [];
					$scope.actBOList = [];
					$scope.lhpvAdvList = [];
					
					$scope.vs.chequeType = '';
					$('#chequeTypeId').attr("disabled","disabled");
					$scope.vs.bankCode = "";
					$('#bankCodeId').attr("disabled","disabled");
					$scope.vs.chequeLeaves = {};
					$('#chequeNoId').attr("disabled","disabled");
					
					$scope.getLhpvDet();
				}else{
					$scope.alertToast(data.msg);
					console.log("Error in submitVoucher");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	 }
	 
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getLhpvDet();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
	
}]);
'use strict';

var app = angular.module('application');

app.controller('BillCntlr',['$scope','$location','$http','$filter','$sce','$window',
                              function($scope,$location,$http,$filter,$sce,$window){
	
	$scope.genBillNo = "";
	$scope.bill = {};
	$scope.customer = {};
	$scope.branch = "";
	$scope.serTax = {};
	$scope.custList = [];
	$scope.cnmtList2 = [];
	$scope.cnmtList = [];
	$scope.blList = [];
	
	$scope.actCnmt = {};
	$scope.actChlnList = [];
	
	$scope.frStn = "";
	$scope.toStn = "";
	$scope.billasis = "";
	
	$scope.billDetSerList = [];
	$scope.billServ = {};
	$scope.billDet = {};
	$scope.billDet.bdOthChgList = [];
	$scope.ot={};
	$scope.selChln = {};
	$scope.selBrh = {};
	//$scope.hillFlag=true;
	$scope.readFlag=true;
	$scope.selCustFlag = true;
	$scope.selCnmtFlag = true;
	$scope.cnmtBillFlag = true;
	$scope.otChgFlag = true;
	$scope.disChlnCodeFlag = true;
	$scope.disSelChlnFlag = true;
	$scope.finalSubmit = true;
	$scope.billGenFlag = true;
	$scope.fromToDateDBFlag = true;
	
	$scope.loadingFlag = false;
	$scope.selectAllCnmt = false;
	
	$scope.otChgTypeList = ["Storage" ,"Redelivery Charge" , "Crain Charge" , "Express Delivery Charge"];
	
	var cnmtDate,subTotal=0;
	
	$('#actWtId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#actWtId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	
	$('#recWtId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#recWtId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	$('#wtId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#wtId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	$('#rateId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#rateId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	$('#frgId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#frgId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	$('#ldId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#ldId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	$('#unldId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#unldId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	$('#bnsId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#bnsId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	$('#detId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#detId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	$('#totId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#totId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	$('#otChgValueId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#otChgValueId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	
	
	
/*	  $scope.getFYear = function(){
			var response = $http.get($scope.projectName+'/getCurntFYear');
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					console.log("Entered Date in current financial year");
					console.log(data.fYear.fyFrmDt);
					console.log(data.fYear.fyToDt);
					$scope.fyFrmDt=data.fYear.fyFrmDt;
					$scope.fyToDt=data.fYear.fyToDt;
				}else{
					$scope.alertToast("There is no any current Financial year");
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.alertToast("There is some problem in fetching date");
			});
		}();
*/
	
	
	
	$scope.getBillInfo = function(){
		$scope.loadingFlag = true;
		console.log("enter into getBillInfo function");
		var response = $http.post($scope.projectName+'/getBillInfo');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.loadingFlag = false;
				  $scope.bill.blBrhId =  data.brh.branchId;
				  $scope.branch = data.brh.branchName;
				  $scope.selBrh = data.brh;
				  $scope.csDt = data.cssDt;
				  $scope.bill.blBillDt = data.cssDt;
				  $scope.custList = data.list;
				  $scope.serTax = data.serTax;
				  console.log("$scope.serTax="+JSON.stringify(data.serTax));
			  }else{
				  $scope.alertToast("Server Error");
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	$scope.checkBlDt = function(billDate){
		console.log("enter into checkBlDt function "+billDate);
		if(billDate>$scope.csDt){
			$scope.bill.blBillDt=$scope.csDt;
		}
		   
	};
	
	
	$scope.chngBlType = function(){
		console.log("enter into chngBlType function");
		
		$scope.billService = {};
		$scope.billService.bdSerList = [];
		$scope.billDetSerList = [];
			
		$scope.bill.blSubTot = 0;
		$scope.bill.blTaxableSerTax = 0;
		$scope.bill.blSerTax = 0;
		$scope.bill.blSwachBhCess = 0;
		$scope.bill.blKisanKalCess=0;
		$scope.bill.blFinalTot = 0;
	}
	
	
	$scope.selectCust = function(){
		console.log("enter into selectCust function");
		$scope.selCustFlag = false;
		$('div#selCustId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Customer",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selCustFlag = true;
		    }
			});
		
		$('div#selCustId').dialog('open');	
	}
	
	
	$scope.takeCnmtFromToDate = function(CnmtFromToDate){
		console.log("Enter into takeCnmtFromToDate() Form = "+CnmtFromToDate.$valid);
		if(CnmtFromToDate.$invalid){
			if(CnmtFromToDate.cnmtFromDt.$invalid)
				$scope.alertToast("Please enter from date !");
			if(CnmtFromToDate.cnmtToDt.$invalid)
				$scope.alertToast("Please enter to date !");			
		}else{
			var req = {
				"custId"		:	$scope.bill.blCustId,
				"cnmtFromDt"	:	$scope.dateRange.cnmtFromDt,
				"cnmtToDt"		:	$scope.dateRange.cnmtToDt
			};
			 $scope.loadingFlag = true;
			var response = $http.post($scope.projectName+'/getCnmtFrBill',req);
			  response.success(function(data, status, headers, config){
				  if(data.result === "success"){
					  $scope.loadingFlag = false;
					  $scope.cnmtList = data.cnList1;
					  $scope.cnmtList2 = data.cnList2;
					  $scope.fromToDateDBFlag = true;						
					  $('div#fromToDataDB').dialog('close');	
				  }else{
					  $scope.loadingFlag = false;
					  $scope.alertToast("There is no cnmt of "+$scope.customer.custName+" for billing in "+$scope.branch);
				  }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
			   });	
		}
		console.log("Exit from takeCnmtFromToDate() Form = "+CnmtFromToDate.$valid);
	}
	
	$scope.saveCustomer = function(cust){
		console.log("Enter into saveCustomer() : custID = "+cust.custId);
		$('div#selCustId').dialog('close');
		$scope.customer = cust;
		$scope.bill.blCustId = cust.custId;
		$scope.cnmtList = []; 
		$scope.cnmtList2 = [];
		
		if($scope.bill.blCustId=='150'){   //for raigardhJindal customer..
			  $scope.raigardhJindal=true;
			  $scope.billDet.bdObdNum='';
			  console.log("$scope.raigardhJindal ="+$scope.raigardhJindal);
		}else{
			$scope.raigardhJindal=false;
			$scope.billDet.bdObdNum=' ';
		}
		
		
		//only hill customer...
		  if($scope.bill.blCustId=='10'){
			  console.log($scope.bill.blCustId);
			  $scope.hillFlag=true;
			  console.log("if"+$scope.hillFlag);
			  $scope.zeroCnmt='00000000';
			  console.log($scope.zeroCnmt);
		  }else{
			  $scope.hillFlag=false;
			  $scope.readFlag=true;
			  $scope.zeroCnmt='';
			  console.log("else"+$scope.bill.hillFlag);
		  }
		  
		  if(cust.custId === 642 || cust.custId === 863){
				$scope.fromToDateDBFlag = false;
				$('div#fromToDataDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					title: "Select From, To date",
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.selCustFlag = true;
				    }
				});
				$('div#fromToDataDB').dialog('open');	
				return ;
			}
		  
		var req = {
			"custId" : $scope.bill.blCustId 	
		};
		 $scope.loadingFlag = true;
		var response = $http.post($scope.projectName+'/getCnmtFrBill',req);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.loadingFlag = false;
				  $scope.cnmtList = data.cnList1;
				  $scope.cnmtList2 = data.cnList2;
			  }else{
				  $scope.loadingFlag = false;
				  $scope.alertToast("There is no cnmt of "+$scope.customer.custName+" for billing in "+$scope.branch);
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });	
	}
	
	$scope.seUnseAllCnmt = function(billType){
		console.log("Enter into seUnseAllCnmt() : BillType = "+billType+" : Select all = "+$scope.selectAllCnmt);
		if(billType === "normal"){
			if($scope.selectAllCnmt){
				for(var i=0; i<$scope.cnmtList.length; i++){
					$scope.saveAllCnmt($scope.cnmtList[i], "1");														
				}
				$("#selectAllCnmtNor").val("Unselect All");
			}else{
				$("#selectAllCnmtNor").val("Select All");
				$scope.bill.blSubTot = 0;
				$scope.bill.blTaxableSerTax = 0;
				$scope.bill.blSerTax = 0;
				$scope.bill.blSwachBhCess = 0;
				$scope.bill.blKisanKalCess=0;
				$scope.bill.blFinalTot = 0;
				$scope.billDetSerList = [];				
			}
		}else if(billType === "sup"){
			if($scope.selectAllCnmt){
				for(var i=0; i<$scope.cnmtList2.length; i++){
					$scope.saveAllCnmt($scope.cnmtList2[i], "1");															
				}
				$("#selectAllCnmtSup").val("Unselect All");
			}else{
				$("#selectAllCnmtSup").val("Select All");
				$scope.bill.blSubTot = 0;
				$scope.bill.blTaxableSerTax = 0;
				$scope.bill.blSerTax = 0;
				$scope.bill.blSwachBhCess = 0;
				$scope.bill.blKisanKalCess=0;
				$scope.bill.blFinalTot = 0;
				$scope.billDetSerList = [];					
			}
		}
	}
	
	$scope.selectCnmt = function(){
		console.log("enter into selectCnmt funciton");
		$scope.selCnmtFlag = false;
		
		$('div#selCnmtId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Cnmt",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selCnmtFlag = true;
		    }
			});
		
		$('div#selCnmtId').dialog('open');	
	}
	
	$scope.saveAllCnmt = function(cnmt, no){
		console.log("enter into saveCnmt function");
		if(no === "1")
			$('div#selCnmtId').dialog('close');	
		$scope.cnmtCode = cnmt;
		
		console.log("cnmtCode="+$scope.cnmtCode);
		console.log("zeroCnmt="+$scope.zeroCnmt);
		
		if($scope.cnmtCode==$scope.zeroCnmt){
			$scope.readFlag=false;
		}else{
			$scope.readFlag=true;
		}
		
		if($scope.bill.blType === 'N'){
			$('#supBlRsnId').attr("disabled","disabled");
		}else if($scope.bill.blType === 'S'){
			$('#supBlRsnId').removeAttr("disabled");
		}else{
			$('#supBlRsnId').attr("disabled","disabled");
		}
		
		$scope.adaniCustomer=false; 
		$scope.raigardhMsp=false;
		$scope.billDet.bdObdNum=' ';
		$scope.billDet.bdPoNum=' ';
		$scope.hillLimited=false;
		$scope.billDet.bdLoadAmt = 0;
		$scope.billDet.bdUnloadAmt = 0;
		$scope.billDet.bdBonusAmt = 0;
		$scope.billDet.bdDetAmt = 0;
		$scope.billDet.bdShipmentNum=0;
		$scope.billDet.bdEntryShNum=0;
		$scope.billDet.bdPoNum=0;
		$scope.bonaiBarbil=false;
		  
		  if($scope.bill.blCustId=='318'){  //for Adani customer 318 only..	
			  $scope.adaniCustomer=true; //$scope.raigardhMsp=false;
			  $scope.billDet.bdObdNum=' ';
			  $scope.raigardhJindal=false;
		  }
		  else if($scope.bill.blCustId=='150'){   //for raigardhJindal customer..
			  $scope.billDet.bdObdNum='';
			  console.log("$scope.raigardhJindal ="+$scope.raigardhJindal);
		  }
		/*  else if($scope.bill.blCustId=='46'){   //for raigardhMsp customer..
			  $scope.raigardhJindal=false; $scope.raigardhMsp=true;
			 // $scope.billDet.bdsalesOdrNum='';
			  $scope.billDet.bdObdNum=' ';
			  $scope.raigardhJindal=false;
		  }*/
		  else if($scope.bill.blCustId=='10'){   //for raigardhJindal customer..
			  $scope.billDet.bdPoNum='';
			  $scope.hillLimited=true;
			  console.log("$scope.raigardhJindal ="+$scope.billDet.bdPoNum);
		  }else if($scope.bill.blBrhId===23){
			  $scope.bonaiBarbil=true;
		  }
		 /* else{
			  $scope.adaniCustomer=false; $scope.raigardhMsp=false;
		      $scope.billDet.bdObdNum=' ';
		      //$scope.billDet.bdsalesOdrNum=' ';
		  }*/
		
		if($scope.billDetSerList.length > 0){
			console.log("++++++++++++++++++++++++++++++");
			var dupCnmt = false;
			for(var i=0;i<$scope.billDetSerList.length;i++){
				if($scope.billDetSerList[i].cnmt.cnmtCode === $scope.cnmtCode){
					dupCnmt = true;
					}
			}
			
			if(dupCnmt === true){
				$scope.alertToast("You already generate a bill for "+$scope.cnmtCode+" Cnmt");
			}else{
				var req = {
						"cnmtCode" : $scope.cnmtCode,
						"billDt"   : $scope.bill.blBillDt
				};
				
				if(angular.isUndefined($scope.cnmtCode)){
					$scope.alertToast("please select a CNMT no");
				}else{
					 $scope.loadingFlag = true;;
					var response = $http.post($scope.projectName+'/getCnInfoFrBlAll',req);
					  response.success(function(data, status, headers, config){
						  if(data.result === "success"){
							  $scope.loadingFlag = false;
							 if($scope.bill.blType === 'S'){
								 $("#actWtId").attr("readonly", false); 
								 $("#recWtId").attr("readonly", false);
								 $("#wtId").attr("readonly", false);
								 $("#rateId").attr("readonly", false);
							 }else{
								 $("#actWtId").attr("readonly", true); 
								 $("#recWtId").attr("readonly", true);
								 $("#wtId").attr("readonly", true);
								 $("#rateId").attr("readonly", true);
							 }
							  
							 $scope.actCnmt = data.cnmt;
							 $scope.billBasis = data.billBasis;
							 console.log("$scope.billBasis ="+$scope.billBasis);
							 console.log("$scope.actCnmt.cnmtBillNo = "+$scope.actCnmt.cnmtBillNo);
							 if($scope.actCnmt.cnmtBillNo === null){
								 if($scope.bill.blType === 'N'){
									 
									 $scope.actChlnList = data.chlnList;
									 $scope.frStn = data.frStn;
									 $scope.toStn = data.toStn;
									 $scope.lryNo = data.lryNo;
									 
									 $scope.billDet.bdLryNo = $scope.lryNo;
									 $scope.billDet.bdCnmtId = $scope.actCnmt.cnmtId;
									 $scope.billDet.bdChgWt = $scope.actCnmt.cnmtGuaranteeWt;
									 $scope.billDet.bdActWt = $scope.actCnmt.cnmtActualWt;
									 
									// $scope.billDet.bdRate = $scope.actCnmt.cnmtRate;
									 $scope.billDet.bdRate = parseFloat($scope.actCnmt.cnmtRate.toFixed(6));
									// $scope.billDet.bdFreight = $scope.actCnmt.cnmtFreight;
									 $scope.billDet.bdFreight = parseFloat($scope.actCnmt.cnmtFreight.toFixed(2));
									 $scope.billDet.bdBlBase = $scope.billBasis;
									 
									 if($scope.actChlnList.length > 0){
										 $scope.billDet.bdRecWt = $scope.actChlnList[$scope.actChlnList.length - 1].chlnRecWt * 1000; 
									 }
									 
									 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
									 
									 if(data.billBasis === "chargeWt"){
										 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdChgWt * $scope.billDet.bdRate));
									 }else if(data.billBasis === "receiveWt"){
										 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdRecWt * $scope.billDet.bdRate));
									 }else if(data.billBasis === "actualWt"){
										 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdActWt * $scope.billDet.bdRate));
									 }else{
										 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
									 }
									 
								 }else{
									 $scope.alertToast("You can't genrate the supplementary bill of "+$scope.actCnmt.cnmtCode);
								 }
							 }else{
								 if($scope.bill.blType === 'N'){
									 $scope.alertToast("You already genrate the normal bill of "+$scope.actCnmt.cnmtCode+" ----> "+$scope.actCnmt.cnmtBillNo);
								 }else{
									 $scope.actChlnList = data.chlnList;
									 $scope.frStn = data.frStn;
									 $scope.toStn = data.toStn;
									 $scope.lryNo = data.lryNo;
									 
									 $scope.billDet.bdLryNo = $scope.lryNo;
									 $scope.billDet.bdCnmtId = $scope.actCnmt.cnmtId;
									 $scope.billDet.bdChgWt = $scope.actCnmt.cnmtGuaranteeWt;
									 $scope.billDet.bdActWt = $scope.actCnmt.cnmtActualWt;
									// $scope.billDet.bdRate = $scope.actCnmt.cnmtRate;
									 $scope.billDet.bdRate = parseFloat($scope.actCnmt.cnmtRate.toFixed(6));
									// $scope.billDet.bdFreight = $scope.actCnmt.cnmtFreight;
									 $scope.billDet.bdFreight = parseFloat($scope.actCnmt.cnmtFreight.toFixed(2));
									 $scope.billDet.bdBlBase = $scope.billBasis;
									 
									 if($scope.actChlnList.length > 0){
										 $scope.billDet.bdRecWt = $scope.actChlnList[$scope.actChlnList.length - 1].chlnRecWt * 1000; 
									 }
									 
									 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
									 
									 
									 if(data.billBasis === "chargeWt"){
										 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdChgWt * $scope.billDet.bdRate));
									 }else if(data.billBasis === "receiveWt"){
										 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdRecWt * $scope.billDet.bdRate));
									 }else if(data.billBasis === "actualWt"){
										 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdActWt * $scope.billDet.bdRate));
									 }else{
										 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
									 }
									 
								 }
							 }				 
							 $scope.saveMyCnBillForm();
						  }else{
							  $scope.loadingFlag = false;
							  if(angular.isUndefined(data.msg)){
								$scope.alertToast("Server Error");  
							  }else{
								  $scope.alertToast(data.msg);  
							  }
						  }
					   });
					   response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
							$scope.loadingFlag = false;
					   });
				}
			}
		}else{
			console.log("^^^^^^^^^^^^^^^^^^^^^^^");
			var req = {
					"cnmtCode" : $scope.cnmtCode,
					"billDt"   : $scope.bill.blBillDt
			};
			
			if(angular.isUndefined($scope.cnmtCode)){
				$scope.alertToast("please select a CNMT no");
			}else{
				 $scope.loadingFlag = true;
				var response = $http.post($scope.projectName+'/getCnInfoFrBlAll',req);
				  response.success(function(data, status, headers, config){
					 // cosole.log("data.result="+data.result);
					  if(data.result === "success"){
						  $scope.loadingFlag = false;
						  if($scope.bill.blType === 'S'){
								 $("#actWtId").attr("readonly", false); 
								 $("#recWtId").attr("readonly", false);
								 $("#wtId").attr("readonly", false);
								 $("#rateId").attr("readonly", false);
						  }else{
								 $("#actWtId").attr("readonly", true); 
								 $("#recWtId").attr("readonly", true);
								 $("#wtId").attr("readonly", true);
								 $("#rateId").attr("readonly", true);
						  }
						  
						 $scope.actCnmt = data.cnmt;
						 $scope.billBasis = data.billBasis;
						 console.log("$scope.billBasis ="+$scope.billBasis);
						 console.log("$scope.actCnmt.cnmtBillNo = "+$scope.actCnmt.cnmtBillNo);
						 if($scope.actCnmt.cnmtBillNo === null){
							 if($scope.bill.blType === 'N'){
								
								 $scope.actChlnList = data.chlnList;
								 $scope.frStn = data.frStn;
								 $scope.toStn = data.toStn;
								 $scope.lryNo = data.lryNo;
								 
								 $scope.billDet.bdLryNo = $scope.lryNo;
								 $scope.billDet.bdCnmtId = $scope.actCnmt.cnmtId;
								 $scope.billDet.bdChgWt = $scope.actCnmt.cnmtGuaranteeWt;
								 $scope.billDet.bdActWt = $scope.actCnmt.cnmtActualWt;
								// $scope.billDet.bdRate = $scope.actCnmt.cnmtRate;
								 $scope.billDet.bdRate = parseFloat($scope.actCnmt.cnmtRate.toFixed(6));
								 //$scope.billDet.bdFreight = $scope.actCnmt.cnmtFreight;
								 $scope.billDet.bdFreight = parseFloat($scope.actCnmt.cnmtFreight.toFixed(2));
								 $scope.billDet.bdBlBase = $scope.billBasis;
								 
								 if($scope.actChlnList.length > 0){
									 $scope.billDet.bdRecWt = $scope.actChlnList[$scope.actChlnList.length - 1].chlnRecWt * 1000; 
								 }
								 
								 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
								 
								 if(data.billBasis === "chargeWt"){
									 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdChgWt * $scope.billDet.bdRate));
								 }else if(data.billBasis === "receiveWt"){
									 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdRecWt * $scope.billDet.bdRate));
								 }else if(data.billBasis === "actualWt"){
									 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdActWt * $scope.billDet.bdRate));
								 }else{
									 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
								 }
								 
							 }else{
								 $scope.alertToast("You can't genrate the supplementary bill of "+$scope.actCnmt.cnmtCode);
							 }
						 }else{
							 if($scope.bill.blType === 'N'){
								 $scope.alertToast("You already genrate the normal bill of "+$scope.actCnmt.cnmtCode+" ----> "+$scope.actCnmt.cnmtBillNo);
							 }else{
								 $scope.actChlnList = data.chlnList;
								 $scope.frStn = data.frStn;
								 $scope.toStn = data.toStn;
								 $scope.lryNo = data.lryNo;
								 
								 $scope.billDet.bdLryNo = $scope.lryNo;
								 $scope.billDet.bdCnmtId = $scope.actCnmt.cnmtId;
								 $scope.billDet.bdChgWt = $scope.actCnmt.cnmtGuaranteeWt;
								 $scope.billDet.bdActWt = $scope.actCnmt.cnmtActualWt;
								// $scope.billDet.bdRate = $scope.actCnmt.cnmtRate;
								 $scope.billDet.bdRate = parseFloat($scope.actCnmt.cnmtRate.toFixed(6));
								// $scope.billDet.bdFreight = $scope.actCnmt.cnmtFreight;
								 $scope.billDet.bdFreight = parseFloat($scope.actCnmt.cnmtFreight.toFixed(2));
								 $scope.billDet.bdBlBase = $scope.billBasis;
								 
								 if($scope.actChlnList.length > 0){
									 $scope.billDet.bdRecWt = $scope.actChlnList[$scope.actChlnList.length - 1].chlnRecWt * 1000; 
								 }
								 
								 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
								 
								 if(data.billBasis === "chargeWt"){
									 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdChgWt * $scope.billDet.bdRate));
								 }else if(data.billBasis === "receiveWt"){
									 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdRecWt * $scope.billDet.bdRate));
								 }else if(data.billBasis === "actualWt"){
									 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdActWt * $scope.billDet.bdRate));
								 }else{
									 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
								 }
								 
								 
							 }
						 }
						 $scope.saveMyCnBillForm();
					  }else{
						  $scope.loadingFlag = false;
						  if(angular.isUndefined(data.msg)){
							$scope.alertToast("Server Error");  
						  }else{
							  $scope.alertToast(data.msg);  
						  }
					  }
				   });
				   response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
						$scope.loadingFlag = false;
				   });
			}
		}
	
	}
	
	
	$scope.saveCnmt = function(cnmt){
		console.log("enter into saveCnmt function");
		$('div#selCnmtId').dialog('close');	
		$scope.cnmtCode = cnmt;
		
		console.log("cnmtCode="+$scope.cnmtCode);
		console.log("zeroCnmt="+$scope.zeroCnmt);
		
		if($scope.cnmtCode==$scope.zeroCnmt){
			$scope.readFlag=false;
		}else{
			$scope.readFlag=true;
		}
		
		if($scope.bill.blType === 'N'){
			$('#supBlRsnId').attr("disabled","disabled");
		}else if($scope.bill.blType === 'S'){
			$('#supBlRsnId').removeAttr("disabled");
		}else{
			$('#supBlRsnId').attr("disabled","disabled");
		}
		
		$scope.adaniCustomer=false; 
		$scope.raigardhMsp=false;
		$scope.billDet.bdObdNum=' ';
		$scope.billDet.bdPoNum=' ';
		$scope.hillLimited=false;
		$scope.billDet.bdLoadAmt = 0;
		$scope.billDet.bdUnloadAmt = 0;
		$scope.billDet.bdBonusAmt = 0;
		$scope.billDet.bdDetAmt = 0;
		$scope.billDet.bdShipmentNum=0;
		$scope.billDet.bdEntryShNum=0;
		$scope.billDet.bdPoNum=0;
		$scope.bonaiBarbil=false;
		  
		  if($scope.bill.blCustId=='318'){  //for Adani customer 318 only..	
			  $scope.adaniCustomer=true; //$scope.raigardhMsp=false;
			  $scope.billDet.bdObdNum=' ';
			  $scope.raigardhJindal=false;
		  }
		  else if($scope.bill.blCustId=='150'){   //for raigardhJindal customer..
			  $scope.billDet.bdObdNum='';
			  console.log("$scope.raigardhJindal ="+$scope.raigardhJindal);
		  }
		/*  else if($scope.bill.blCustId=='46'){   //for raigardhMsp customer..
			  $scope.raigardhJindal=false; $scope.raigardhMsp=true;
			 // $scope.billDet.bdsalesOdrNum='';
			  $scope.billDet.bdObdNum=' ';
			  $scope.raigardhJindal=false;
		  }*/
		  else if($scope.bill.blCustId=='10'){   //for raigardhJindal customer..
			  $scope.billDet.bdPoNum='';
			  $scope.hillLimited=true;
			  console.log("$scope.raigardhJindal ="+$scope.billDet.bdPoNum);
		  }else if($scope.bill.blBrhId===23){
			  $scope.bonaiBarbil=true;
		  }
		 /* else{
			  $scope.adaniCustomer=false; $scope.raigardhMsp=false;
		      $scope.billDet.bdObdNum=' ';
		      //$scope.billDet.bdsalesOdrNum=' ';
		  }*/
		
		if($scope.billDetSerList.length > 0){
			console.log("++++++++++++++++++++++++++++++");
			var dupCnmt = false;
			for(var i=0;i<$scope.billDetSerList.length;i++){
				if($scope.billDetSerList[i].cnmt.cnmtCode === $scope.cnmtCode){
					dupCnmt = true;
					}
			}
			
			if(dupCnmt === true){
				$scope.alertToast("You already generate a bill for "+$scope.cnmtCode+" Cnmt");
			}else{
				var req = {
						"cnmtCode" : $scope.cnmtCode,
						"billDt"   : $scope.bill.blBillDt
				};
				
				if(angular.isUndefined($scope.cnmtCode)){
					$scope.alertToast("please select a CNMT no");
				}else{
					 $scope.loadingFlag = true;;
					var response = $http.post($scope.projectName+'/getCnInfoFrBl',req);
					  response.success(function(data, status, headers, config){
						  if(data.result === "success"){
							  $scope.loadingFlag = false;
							 if($scope.bill.blType === 'S'){
								 $("#actWtId").attr("readonly", false); 
								 $("#recWtId").attr("readonly", false);
								 $("#wtId").attr("readonly", false);
								 $("#rateId").attr("readonly", false);
							 }else{
								 $("#actWtId").attr("readonly", true); 
								 $("#recWtId").attr("readonly", true);
								 $("#wtId").attr("readonly", true);
								 $("#rateId").attr("readonly", true);
							 }
							  
							 $scope.actCnmt = data.cnmt;
							 $scope.billBasis = data.billBasis;
							 console.log("$scope.billBasis ="+$scope.billBasis);
							 console.log("$scope.actCnmt.cnmtBillNo = "+$scope.actCnmt.cnmtBillNo);
							 if($scope.actCnmt.cnmtBillNo === null){
								 if($scope.bill.blType === 'N'){
									 $scope.actChlnList = data.chlnList;
									 $scope.frStn = data.frStn;
									 $scope.toStn = data.toStn;
									 $scope.lryNo = data.lryNo;
									 
									 $scope.billDet.bdLryNo = $scope.lryNo;
									 $scope.billDet.bdCnmtId = $scope.actCnmt.cnmtId;
									 $scope.billDet.bdChgWt = $scope.actCnmt.cnmtGuaranteeWt;
									 $scope.billDet.bdActWt = $scope.actCnmt.cnmtActualWt;
									// $scope.billDet.bdRate = $scope.actCnmt.cnmtRate;
									 $scope.billDet.bdRate = parseFloat($scope.actCnmt.cnmtRate.toFixed(6));
									// $scope.billDet.bdFreight = $scope.actCnmt.cnmtFreight;
									 $scope.billDet.bdFreight = parseFloat($scope.actCnmt.cnmtFreight.toFixed(2));
									 $scope.billDet.bdBlBase = $scope.billBasis;
									 
									 if($scope.actChlnList.length > 0){
										 $scope.billDet.bdRecWt = $scope.actChlnList[$scope.actChlnList.length - 1].chlnRecWt * 1000; 
									 }
									 
									 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
									 
									 if(data.billBasis === "chargeWt"){
										 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdChgWt * $scope.billDet.bdRate));
									 }else if(data.billBasis === "receiveWt"){
										 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdRecWt * $scope.billDet.bdRate));
									 }else if(data.billBasis === "actualWt"){
										 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdActWt * $scope.billDet.bdRate));
									 }else{
										 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
									 }
									 
									 $scope.cnmtBillFlag = false;
										$('div#cnmtBillId').dialog({
											autoOpen: false,
											modal:true,
											resizable: false,
											title: "Billing Detail Of "+$scope.actCnmt.cnmtCode+"("+$scope.billBasis+")",
											show: UDShow,
											hide: UDHide,
											position: UDPos,
											draggable: true,
											close: function(event, ui) { 
										        $(this).dialog('destroy');
										        $(this).hide();
										        $scope.cnmtBillFlag = true;
										    }
											});
										
										$('div#cnmtBillId').dialog('open');
								 }else{
									 $scope.alertToast("You can't genrate the supplementary bill of "+$scope.actCnmt.cnmtCode);
								 }
							 }else{
								 if($scope.bill.blType === 'N'){
									 $scope.alertToast("You already genrate the normal bill of "+$scope.actCnmt.cnmtCode+" ----> "+$scope.actCnmt.cnmtBillNo);
								 }else{
									 $scope.actChlnList = data.chlnList;
									 $scope.frStn = data.frStn;
									 $scope.toStn = data.toStn;
									 $scope.lryNo = data.lryNo;
									 
									 $scope.billDet.bdLryNo = $scope.lryNo;
									 $scope.billDet.bdCnmtId = $scope.actCnmt.cnmtId;
									 $scope.billDet.bdChgWt = $scope.actCnmt.cnmtGuaranteeWt;
									 $scope.billDet.bdActWt = $scope.actCnmt.cnmtActualWt;
									// $scope.billDet.bdRate = $scope.actCnmt.cnmtRate;
									 $scope.billDet.bdRate = parseFloat($scope.actCnmt.cnmtRate.toFixed(6));
									// $scope.billDet.bdFreight = $scope.actCnmt.cnmtFreight;
									 $scope.billDet.bdFreight = parseFloat($scope.actCnmt.cnmtFreight.toFixed(2));
									 $scope.billDet.bdBlBase = $scope.billBasis;
									 
									 if($scope.actChlnList.length > 0){
										 $scope.billDet.bdRecWt = $scope.actChlnList[$scope.actChlnList.length - 1].chlnRecWt * 1000; 
									 }
									 
									 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
									 
									 
									 if(data.billBasis === "chargeWt"){
										 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdChgWt * $scope.billDet.bdRate));
									 }else if(data.billBasis === "receiveWt"){
										 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdRecWt * $scope.billDet.bdRate));
									 }else if(data.billBasis === "actualWt"){
										 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdActWt * $scope.billDet.bdRate));
									 }else{
										 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
									 }
									 
									 $scope.cnmtBillFlag = false;
										$('div#cnmtBillId').dialog({
											autoOpen: false,
											modal:true,
											resizable: false,
											title: "Billing Detail Of "+$scope.actCnmt.cnmtCode+"("+$scope.billBasis+")",
											show: UDShow,
											hide: UDHide,
											position: UDPos,
											draggable: true,
											close: function(event, ui) { 
										        $(this).dialog('destroy');
										        $(this).hide();
										        $scope.cnmtBillFlag = true;
										    }
											});
										
										$('div#cnmtBillId').dialog('open');
								 }
							 }
							 							 
						  }else{
							  $scope.loadingFlag = false;
							  if(angular.isUndefined(data.msg)){
								$scope.alertToast("Server Error");  
							  }else{
								  $scope.alertToast(data.msg);  
							  }
						  }
					   });
					   response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
							$scope.loadingFlag = false;
					   });
				}
			}
		}else{
			console.log("^^^^^^^^^^^^^^^^^^^^^^^");
			var req = {
					"cnmtCode" : $scope.cnmtCode,
					"billDt"   : $scope.bill.blBillDt
			};
			
			if(angular.isUndefined($scope.cnmtCode)){
				$scope.alertToast("please select a CNMT no");
			}else{
				 $scope.loadingFlag = true;
				var response = $http.post($scope.projectName+'/getCnInfoFrBl',req);
				  response.success(function(data, status, headers, config){
					 // cosole.log("data.result="+data.result);
					  if(data.result === "success"){
						  $scope.loadingFlag = false;
						  if($scope.bill.blType === 'S'){
								 $("#actWtId").attr("readonly", false); 
								 $("#recWtId").attr("readonly", false);
								 $("#wtId").attr("readonly", false);
								 $("#rateId").attr("readonly", false);
						  }else{
								 $("#actWtId").attr("readonly", true); 
								 $("#recWtId").attr("readonly", true);
								 $("#wtId").attr("readonly", true);
								 $("#rateId").attr("readonly", true);
						  }
						  
						 $scope.actCnmt = data.cnmt;
						 $scope.billBasis = data.billBasis;
						 console.log("$scope.billBasis ="+$scope.billBasis);
						 console.log("$scope.actCnmt.cnmtBillNo = "+$scope.actCnmt.cnmtBillNo);
						 if($scope.actCnmt.cnmtBillNo === null){
							 if($scope.bill.blType === 'N'){
								
								 $scope.actChlnList = data.chlnList;
								 $scope.frStn = data.frStn;
								 $scope.toStn = data.toStn;
								 $scope.lryNo = data.lryNo;
								 
								 $scope.billDet.bdLryNo = $scope.lryNo;
								 $scope.billDet.bdCnmtId = $scope.actCnmt.cnmtId;
								 $scope.billDet.bdChgWt = $scope.actCnmt.cnmtGuaranteeWt;
								 $scope.billDet.bdActWt = $scope.actCnmt.cnmtActualWt;
								// $scope.billDet.bdRate = $scope.actCnmt.cnmtRate;
								 $scope.billDet.bdRate = parseFloat($scope.actCnmt.cnmtRate.toFixed(6));
								 //$scope.billDet.bdFreight = $scope.actCnmt.cnmtFreight;
								 $scope.billDet.bdFreight = parseFloat($scope.actCnmt.cnmtFreight.toFixed(2));
								 $scope.billDet.bdBlBase = $scope.billBasis;
								 
								 if($scope.actChlnList.length > 0){
									 $scope.billDet.bdRecWt = $scope.actChlnList[$scope.actChlnList.length - 1].chlnRecWt * 1000; 
								 }
								 
								 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
								 
								 if(data.billBasis === "chargeWt"){
									 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdChgWt * $scope.billDet.bdRate));
								 }else if(data.billBasis === "receiveWt"){
									 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdRecWt * $scope.billDet.bdRate));
								 }else if(data.billBasis === "actualWt"){
									 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdActWt * $scope.billDet.bdRate));
								 }else{
									 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
								 }
								 
								 $scope.cnmtBillFlag = false;
									$('div#cnmtBillId').dialog({
										autoOpen: false,
										modal:true,
										resizable: false,
										title: "Billing Detail Of "+$scope.actCnmt.cnmtCode+"("+$scope.billBasis+")",
										show: UDShow,
										hide: UDHide,
										position: UDPos,
										draggable: true,
										close: function(event, ui) { 
									        $(this).dialog('destroy');
									        $(this).hide();
									        $scope.cnmtBillFlag = true;
									    }
										});
									
									$('div#cnmtBillId').dialog('open');
							 }else{
								 $scope.alertToast("You can't genrate the supplementary bill of "+$scope.actCnmt.cnmtCode);
							 }
						 }else{
							 if($scope.bill.blType === 'N'){
								 $scope.alertToast("You already genrate the normal bill of "+$scope.actCnmt.cnmtCode+" ----> "+$scope.actCnmt.cnmtBillNo);
							 }else{
								 $scope.actChlnList = data.chlnList;
								 $scope.frStn = data.frStn;
								 $scope.toStn = data.toStn;
								 $scope.lryNo = data.lryNo;
								 
								 $scope.billDet.bdLryNo = $scope.lryNo;
								 $scope.billDet.bdCnmtId = $scope.actCnmt.cnmtId;
								 $scope.billDet.bdChgWt = $scope.actCnmt.cnmtGuaranteeWt;
								 $scope.billDet.bdActWt = $scope.actCnmt.cnmtActualWt;
								// $scope.billDet.bdRate = $scope.actCnmt.cnmtRate;
								 $scope.billDet.bdRate = parseFloat($scope.actCnmt.cnmtRate.toFixed(6));
								// $scope.billDet.bdFreight = $scope.actCnmt.cnmtFreight;
								 $scope.billDet.bdFreight = parseFloat($scope.actCnmt.cnmtFreight.toFixed(2));
								 $scope.billDet.bdBlBase = $scope.billBasis;
								 
								 if($scope.actChlnList.length > 0){
									 $scope.billDet.bdRecWt = $scope.actChlnList[$scope.actChlnList.length - 1].chlnRecWt * 1000; 
								 }
								 
								 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
								 
								 if(data.billBasis === "chargeWt"){
									 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdChgWt * $scope.billDet.bdRate));
								 }else if(data.billBasis === "receiveWt"){
									 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdRecWt * $scope.billDet.bdRate));
								 }else if(data.billBasis === "actualWt"){
									 $scope.billDet.bdTotAmt = Math.round(parseFloat($scope.billDet.bdActWt * $scope.billDet.bdRate));
								 }else{
									 $scope.billDet.bdTotAmt = Math.round($scope.billDet.bdFreight); 
								 }
								 
								 
								 $scope.cnmtBillFlag = false;
									$('div#cnmtBillId').dialog({
										autoOpen: false,
										modal:true,
										resizable: false,
										title: "Billing Detail Of "+$scope.actCnmt.cnmtCode+"("+$scope.billBasis+")",
										show: UDShow,
										hide: UDHide,
										position: UDPos,
										draggable: true,
										close: function(event, ui) { 
									        $(this).dialog('destroy');
									        $(this).hide();
									        $scope.cnmtBillFlag = true;
									    }
										});
									
									$('div#cnmtBillId').dialog('open');
							 }
						 }
						
					  }else{
						  $scope.loadingFlag = false;
						  if(angular.isUndefined(data.msg)){
							$scope.alertToast("Server Error");  
						  }else{
							  $scope.alertToast(data.msg);  
						  }
					  }
				   });
				   response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
						$scope.loadingFlag = false;
				   });
			}
		}
	
	}
	
	
	
	$scope.otChg = function(){
		console.log("enter into otChg function");
		$scope.otChgFlag = false;
    	$('div#otChgDB').dialog({
    		autoOpen: false,
			modal:true,
			resizable: false,
			title: "Other Charges",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.otChgFlag = true;
		    }
			});
		
		$('div#otChgDB').dialog('open');	
	}
	
	
	$scope.saveOtChg = function(OtherChargeForm,ot){
		console.log("enter into saveOtChg function--->");
		$('div#otChgDB').dialog('close');
		
		if($scope.billDet.bdOthChgList.length>0){
			if(OtherChargeForm.$invalid){
				if(OtherChargeForm.otChgTypeName.$invalid){
					$scope.alertToast("Please Enter Type");
				}else if(OtherChargeForm.otChgValueName.$invalid){
					$scope.alertToast("Please Enter Value");
				}else{
					console.log("**************");
				}
			}else{
				ot.otChgType=ot.otChgType.toUpperCase();
				var duplicate = false;
				for(var i=0;i<$scope.billDet.bdOthChgList.length;i++){
					if(ot.otChgType === $scope.billDet.bdOthChgList[i].otChgType){
						duplicate = true;
						break;
					}
				}
				console.log("duplicate = "+duplicate);
				if(duplicate === true){
					$scope.alertToast("You already enter the "+ot.otChgType+" amount");
				}else{
					$scope.addOtherCharge(ot);	
				}
			}
		}else{
			console.log("If list does not exist");
			if(OtherChargeForm.$invalid){
				if(OtherChargeForm.otChgTypeName.$invalid){
					$scope.alertToast("Please Enter Type");
				}else if(OtherChargeForm.otChgValueName.$invalid){
					$scope.alertToast("Please Enter Value");
				}else{
					console.log("**********************");
				}
			}else{
				ot.otChgType=ot.otChgType.toUpperCase();
				$scope.addOtherCharge(ot);
			}
		}
	}
	
	
	
	$scope.addOtherCharge = function(ot){
		console.log("enter into addOtherCharge function")
		$scope.billDet.bdOthChgList.push(ot); 
		console.log("size of $scope.billDet.bdOthChgList = "+$scope.billDet.bdOthChgList.length);
		$scope.ot={};
		$scope.calBillTot();
	}
	
	
	$scope.removeOth = function(index){
		console.log("enter into removeOth function = "+index);
		if($scope.billDet.bdOthChgList.length > 0){
			$scope.billDet.bdOthChgList.splice(index,1);
		}
		$scope.calBillTot();
	}
	
	
	$scope.calBillTot = function(){
		console.log("enter into calBillTot function");
		
		var tempFreight = 0;
		
		 if($scope.billBasis === "chargeWt"){
			 tempFreight = parseFloat($scope.billDet.bdChgWt * $scope.billDet.bdRate);
		 }else if($scope.billBasis === "receiveWt"){
			 tempFreight = parseFloat($scope.billDet.bdRecWt * $scope.billDet.bdRate);
		 }else if($scope.billBasis === "actualWt"){
			 tempFreight = parseFloat($scope.billDet.bdActWt * $scope.billDet.bdRate);
		 }else{
			 tempFreight = $scope.billDet.bdFreight; 
		 }
		 
		var bdLoadAmt = 0;
		var bdUnloadAmt = 0;
		var bdBonusAmt = 0;
		var bdDetAmt = 0;
	
		$scope.billDet.bdTotAmt = 0;
		if(!angular.isNumber($scope.billDet.bdLoadAmt)){
			bdLoadAmt = 0;
		}else{
			bdLoadAmt = $scope.billDet.bdLoadAmt;
		}
		
		if(!angular.isNumber($scope.billDet.bdUnloadAmt)){
			bdUnloadAmt = 0;
		}else{
			bdUnloadAmt = $scope.billDet.bdUnloadAmt;
		}
		
		if(!angular.isNumber($scope.billDet.bdBonusAmt)){
			bdBonusAmt = 0;
		}else{
			bdBonusAmt = $scope.billDet.bdBonusAmt;
		}
		
		if(!angular.isNumber($scope.billDet.bdDetAmt)){
			bdDetAmt = 0;
		}else{
			bdDetAmt = $scope.billDet.bdDetAmt;
		}
		
		if($scope.billDet.bdOthChgList.length > 0){
			var othAmt = 0;
			for(var i=0;i<$scope.billDet.bdOthChgList.length;i++){
				othAmt = othAmt + parseFloat($scope.billDet.bdOthChgList[i].otChgValue);
			}
			
			$scope.billDet.bdTotAmt = Math.round(parseFloat(tempFreight) + parseFloat(bdLoadAmt) + 
					parseFloat(bdUnloadAmt) + parseFloat(bdBonusAmt) + parseFloat(bdDetAmt)
					+ parseFloat(othAmt));
		
			console.log("inside if ***** $scope.billDet.bdTotAmt = "+$scope.billDet.bdTotAmt);
		}else{
			$scope.billDet.bdTotAmt = Math.round(parseFloat(tempFreight) + parseFloat(bdLoadAmt) + 
					parseFloat(bdUnloadAmt) + parseFloat(bdBonusAmt) +
					parseFloat(bdDetAmt));
			
			console.log("inside else #### $scope.billDet.bdTotAmt = "+$scope.billDet.bdTotAmt);
		}
		
	}
	
	
	$scope.displayChln = function(){
		console.log("enter into displayChln function");
		$scope.disChlnCodeFlag = false;
    	$('div#disChlnCodeDB').dialog({
    		autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Challan",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.disChlnCodeFlag = true;
		    }
			});
		
		$('div#disChlnCodeDB').dialog('open');	
	}
	
	
	$scope.selectChln = function(chln){
		console.log("enter into selectChln function");
		$scope.selChln = chln;
		$scope.disSelChlnFlag = false;
    	$('div#disSelChlnDB').dialog({
    		autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Challan",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.disSelChlnFlag = true;
		    }
			});
		
		$('div#disSelChlnDB').dialog('open');	
	}
	
	
	$scope.getCnImage = function(){
		console.log("enter into getCnImage function");
		var req = {
			"cnmtId" : $scope.actCnmt.cnmtId	
		};
		var response = $http.post($scope.projectName+'/getCnImageFrBill' ,req);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.image = data.cnImage;
				  console.log("**************");
				  window.open("data:application/pdf;base64, "+$scope.image);
				   /* var file = new Blob([$scope.image], {type: 'application/pdf'});
				    var fileURL = URL.createObjectURL(file);
				    $scope.content = $sce.trustAsResourceUrl(fileURL);*/
			  }else{
				  $scope.alertToast(data.msg);
				  console.log("################");
			  }
		  	});
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	$scope.saveMyCnBillForm = function(){
		console.log("enter into saveCnBillForm()");
			$scope.billServ = {};
			$scope.billServ.billDetail = $scope.billDet;
			var othTot = 0.0;
			if($scope.billServ.billDetail.bdOthChgList.length > 0){
				for(var i=0;i<$scope.billServ.billDetail.bdOthChgList.length;i++){
					console.log("$scope.billServ.billDetail.bdOthChgList["+i+"].othChgValue = "+$scope.billServ.billDetail.bdOthChgList[i].otChgValue);
					othTot = Math.round(othTot + parseFloat($scope.billServ.billDetail.bdOthChgList[i].otChgValue));
					console.log("othTot = "+othTot);
				}
			}
			
			$scope.billServ.billDetail.bdOthChgAmt = Math.round(othTot);
			
			$scope.billServ.frStn = $scope.frStn;
			$scope.billServ.toStn = $scope.toStn;
			$scope.billServ.cnmt = $scope.actCnmt;
			
			$scope.billDetSerList.push($scope.billServ);
			
			$scope.billDet = {};
			$scope.billDet.bdOthChgList = [];
			$scope.ot={};
			$scope.selChln = {};
			$scope.actCnmt = {};
			$scope.actChlnList = [];
			
			$scope.frStn = "";
			$scope.toStn = "";
			
			$scope.bill.blSubTot = 0;
			var parseYear,parseMonth,subtotalAmount,dateSplit=[];
			if($scope.billDetSerList.length > 0){
				for(var i=0;i<$scope.billDetSerList.length;i++){
					$scope.bill.blSubTot = $scope.bill.blSubTot + parseFloat($scope.billDetSerList[i].billDetail.bdTotAmt);
				}
			}
			

			$scope.bill.blSubTot = Math.round($scope.bill.blSubTot);
			console.log("$scope.bill.blSubTot="+$scope.bill.blSubTot);
			if($scope.bill.blSubTot > 0){
				$scope.bill.blTaxableSerTax = Math.round(parseFloat($scope.bill.blSubTot * ($scope.serTax.stTaxableRt / 100)));
				console.log("$scope.bill.blTaxableSerTax"+$scope.bill.blTaxableSerTax);
				if($scope.bill.blTaxableSerTax > 0){
					$scope.bill.blSerTax = Math.round(parseFloat($scope.bill.blTaxableSerTax * ($scope.serTax.stSerTaxRt / 100)));
					$scope.bill.blSwachBhCess = Math.round(parseFloat($scope.bill.blTaxableSerTax * ($scope.serTax.stSwhBHCessRt / 100)));
			     var checkblKisanKalCess=0;
					for(var i=0;i<$scope.billDetSerList.length;i++){
						console.log("billDetSerList="+$scope.billDetSerList[i].cnmt.cnmtDt);
						cnmtDate=$scope.billDetSerList[i].cnmt.cnmtDt;
						dateSplit=cnmtDate.split("-");
				         console.log("cnmtDt="+cnmtDate);
				        parseYear=parseInt(dateSplit[0]);
				        parseMonth=parseInt(dateSplit[1]);
						console.log("parseYear="+parseYear+"parseMonth="+parseMonth);
						if(parseYear>2015&&parseMonth>5||parseYear>2016){
							checkblKisanKalCess=1;
							subTotal=subTotal+parseFloat($scope.billDetSerList[i].billDetail.bdTotAmt);
							console.log("for subTotal="+subTotal);
							var taxAbleServiceTax=(($scope.serTax.stTaxableRt*subTotal)/100);
							console.log("taxAbleServiceTax="+taxAbleServiceTax);
						 $scope.bill.blKisanKalCess=Math.round($scope.serTax.stKissanCessRt*(taxAbleServiceTax/100));
						}
						else if(checkblKisanKalCess==0){
							$scope.bill.blKisanKalCess=0;
						}
							
					}
					subTotal=0;
				}
				else{
					$scope.bill.blTaxableSerTax = 0;
					$scope.bill.blSerTax = 0;
					$scope.bill.blSwachBhCess = 0;
					$scope.bill.blKisanKalCess=0;
				}
			}else{
				$scope.bill.blTaxableSerTax = 0;
				$scope.bill.blSerTax = 0;
				$scope.bill.blSwachBhCess = 0;
				$scope.bill.blKisanKalCess=0;
			}    
			
			console.log("$scope.bill.blSerTax = "+$scope.bill.blSerTax);
			console.log("$scope.bill.blSwachBhCess = "+$scope.bill.blSwachBhCess);
			console.log("$scope.customer.custSrvTaxBy = "+$scope.customer.custSrvTaxBy);
			console.log("$scope.bill.blKisanKalCess="+$scope.bill.blKisanKalCess);
			if($scope.customer.custSrvTaxBy === 'S'){
				$scope.bill.blFinalTot = Math.round($scope.bill.blSubTot + $scope.bill.blSerTax + $scope.bill.blSwachBhCess+$scope.bill.blKisanKalCess);
			}else{
				$scope.bill.blFinalTot = Math.round($scope.bill.blSubTot);
			}			
	}
	
	
	$scope.saveCnBillForm = function(cnmtBillForm){
		console.log("enter into saveCnBillForm function = "+cnmtBillForm.$invalid);
		if(cnmtBillForm.$invalid){
			$scope.alertToast("Please fill correct bill detail");
		}else{
			$scope.billServ = {};
			$scope.billServ.billDetail = $scope.billDet;
			var othTot = 0.0;
			if($scope.billServ.billDetail.bdOthChgList.length > 0){
				for(var i=0;i<$scope.billServ.billDetail.bdOthChgList.length;i++){
					console.log("$scope.billServ.billDetail.bdOthChgList["+i+"].othChgValue = "+$scope.billServ.billDetail.bdOthChgList[i].otChgValue);
					othTot = Math.round(othTot + parseFloat($scope.billServ.billDetail.bdOthChgList[i].otChgValue));
					console.log("othTot = "+othTot);
				}
			}
			
			$scope.billServ.billDetail.bdOthChgAmt = Math.round(othTot);
			
			$scope.billServ.frStn = $scope.frStn;
			$scope.billServ.toStn = $scope.toStn;
			$scope.billServ.cnmt = $scope.actCnmt;
			
			$scope.billDetSerList.push($scope.billServ);
			$('div#cnmtBillId').dialog('close');
			
			$scope.billDet = {};
			$scope.billDet.bdOthChgList = [];
			$scope.ot={};
			$scope.selChln = {};
			$scope.actCnmt = {};
			$scope.actChlnList = [];
			
			$scope.frStn = "";
			$scope.toStn = "";
			
			$scope.bill.blSubTot = 0;
			var parseYear,parseMonth,subtotalAmount,dateSplit=[];
			if($scope.billDetSerList.length > 0){
				for(var i=0;i<$scope.billDetSerList.length;i++){
					$scope.bill.blSubTot = $scope.bill.blSubTot + parseFloat($scope.billDetSerList[i].billDetail.bdTotAmt);
				}
			}
			

			$scope.bill.blSubTot = Math.round($scope.bill.blSubTot);
			console.log("$scope.bill.blSubTot="+$scope.bill.blSubTot);
			if($scope.bill.blSubTot > 0){
				$scope.bill.blTaxableSerTax = Math.round(parseFloat($scope.bill.blSubTot * ($scope.serTax.stTaxableRt / 100)));
				console.log("$scope.bill.blTaxableSerTax"+$scope.bill.blTaxableSerTax);
				if($scope.bill.blTaxableSerTax > 0){
					$scope.bill.blSerTax = Math.round(parseFloat($scope.bill.blTaxableSerTax * ($scope.serTax.stSerTaxRt / 100)));
					$scope.bill.blSwachBhCess = Math.round(parseFloat($scope.bill.blTaxableSerTax * ($scope.serTax.stSwhBHCessRt / 100)));
			     var checkblKisanKalCess=0;
					for(var i=0;i<$scope.billDetSerList.length;i++){
						console.log("billDetSerList="+$scope.billDetSerList[i].cnmt.cnmtDt);
						cnmtDate=$scope.billDetSerList[i].cnmt.cnmtDt;
						dateSplit=cnmtDate.split("-");
				         console.log("cnmtDt="+cnmtDate);
				        parseYear=parseInt(dateSplit[0]);
				        parseMonth=parseInt(dateSplit[1]);
						console.log("parseYear="+parseYear+"parseMonth="+parseMonth);
						if(parseYear>2015&&parseMonth>5||parseYear>2016){
							checkblKisanKalCess=1;
							subTotal=subTotal+parseFloat($scope.billDetSerList[i].billDetail.bdTotAmt);
							console.log("for subTotal="+subTotal);
							var taxAbleServiceTax=(($scope.serTax.stTaxableRt*subTotal)/100);
							console.log("taxAbleServiceTax="+taxAbleServiceTax);
						 $scope.bill.blKisanKalCess=Math.round($scope.serTax.stKissanCessRt*(taxAbleServiceTax/100));
						}
						else if(checkblKisanKalCess==0){
							$scope.bill.blKisanKalCess=0;
						}
							
					}
					subTotal=0;
				}
				else{
					$scope.bill.blTaxableSerTax = 0;
					$scope.bill.blSerTax = 0;
					$scope.bill.blSwachBhCess = 0;
					$scope.bill.blKisanKalCess=0;
				}
			}else{
				$scope.bill.blTaxableSerTax = 0;
				$scope.bill.blSerTax = 0;
				$scope.bill.blSwachBhCess = 0;
				$scope.bill.blKisanKalCess=0;
			}    
			
			console.log("$scope.bill.blSerTax = "+$scope.bill.blSerTax);
			console.log("$scope.bill.blSwachBhCess = "+$scope.bill.blSwachBhCess);
			console.log("$scope.customer.custSrvTaxBy = "+$scope.customer.custSrvTaxBy);
			console.log("$scope.bill.blKisanKalCess="+$scope.bill.blKisanKalCess);
			if($scope.customer.custSrvTaxBy === 'S'){
				$scope.bill.blFinalTot = Math.round($scope.bill.blSubTot + $scope.bill.blSerTax + $scope.bill.blSwachBhCess+$scope.bill.blKisanKalCess);
			}else{
				$scope.bill.blFinalTot = Math.round($scope.bill.blSubTot);
			} 
		}
	}
	
	
	$scope.removeBillDet = function(index){
		console.log("enter into removeBillDet function");
		var parseYear,parseMonth,dateSplit=[];
		if($scope.billDetSerList.length > 0){
			
			$scope.bill.blSubTot = Math.round($scope.bill.blSubTot-$scope.billDetSerList[index].cnmt.cnmtTOT);
			$scope.bill.blTaxableSerTax = Math.round(parseFloat($scope.bill.blSubTot * ($scope.serTax.stTaxableRt / 100)));
			$scope.bill.blSerTax = Math.round(parseFloat($scope.bill.blTaxableSerTax * ($scope.serTax.stSerTaxRt / 100)));
			$scope.bill.blSwachBhCess = Math.round(parseFloat($scope.bill.blTaxableSerTax * ($scope.serTax.stSwhBHCessRt / 100)));
			
			console.log($scope.bill.blSubTot);
			console.log($scope.bill.blTaxableSerTax);
			console.log($scope.bill.blSerTax);
			
			cnmtDate=$scope.billDetSerList[index].cnmt.cnmtDt;
			dateSplit=cnmtDate.split("-");
	         console.log("cnmtDt="+cnmtDate);
	        parseYear=parseInt(dateSplit[0]);
	        parseMonth=parseInt(dateSplit[1]);
			console.log("parseYear="+parseYear+"parseMonth="+parseMonth);
			if(parseYear>2015&&parseMonth>5||parseYear>2016){
			 $scope.bill.blKisanKalCess=Math.round($scope.serTax.stKissanCessRt*($scope.bill.blTaxableSerTax/100));
			}
			if($scope.customer.custSrvTaxBy === 'S'){
				$scope.bill.blFinalTot = Math.round($scope.bill.blFinalTot-($scope.bill.blSerTax + $scope.bill.blSwachBhCess+$scope.bill.blKisanKalCess));
			}else{
				$scope.bill.blFinalTot = Math.round($scope.bill.blSubTot);
			} 
			$scope.billDetSerList.splice(index,1);
			if($scope.billDetSerList.length===0){
				$scope.cnmtCode='';
			};
		}
	};
	
	
	$scope.billSubmit = function(billForm){
		console.log("enter into billSubmit function = "+billForm.$invalid);
		if(billForm.$invalid){
			$scope.alertToast("Please fill the correct form");
		}else{
			$scope.finalSubmit = false;
			$('div#finalSubId').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Final Submittion",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.finalSubmit = true;
			    }
				});
			
			$('div#finalSubId').dialog('open');	
		}
	}
	
	
	$scope.back = function(){
		console.log("enter into back function");
		$('div#finalSubId').dialog('close');
	}
	
	
	$scope.saveBill = function(){
		console.log("enter inot saveBill function");
		$('#saveId').attr("disabled","disabled");
		$scope.genBillNo = "";
			
		$scope.billService = {};
		$scope.billService.bdSerList = [];
		$scope.billService.bdSerList = $scope.billDetSerList;
		$scope.billService.bill = $scope.bill;
		
		console.log("size of $scope.billDetSerList = "+$scope.billDetSerList.length);
		console.log("size of $scope.billService.bdSerList = "+$scope.billService.bdSerList.length);
		$scope.loadingFlag = true;
		
		var response = $http.post($scope.projectName+'/submitFBill',$scope.billService);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.loadingFlag = false;
				  $scope.genBillNo = data.billNo;
				  $scope.blList = data.list;
				  console.log($scope.blList);
				  $scope.billService = {};
				  $scope.billService.bdSerList = [];
				  $scope.billDetSerList = [];
				  $scope.bill = {};
				  //$scope.branch = "";
				  $scope.customer = {};
				  $scope.cnmtCode = "";
				  $('div#finalSubId').dialog('close');
				  $('#saveId').removeAttr("disabled");
					
				  
				  $scope.billGenFlag = false;
					$('div#billGenId').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						title: "Bill No",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					        $scope.billGenFlag = true;
					    }
						});
					
				 $('div#billGenId').dialog('open');	
				  
				  $scope.bill.blType = 'N';
				  $scope.getBillInfo();
			  }else{
				  $('#saveId').removeAttr("disabled");
				 $scope.loadingFlag = false;
				 for(var i=0; i<data.msg.length; i++)
					 $scope.alertToast(data.msg[i]);
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
				$scope.loadingFlag = false;
		   });
	}
	
	
	$scope.cancelPrint = function(){
		console.log("enter into cancelPrint function");
		$('div#billGenId').dialog('close');
	}
	
	$scope.printVs = function(){
		console.log("enter into printVs function");
		$window.print();
	};	
	
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getBillInfo();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	 
}]);
var app = angular.module('application');

app.controller('BankDetBrkCntlr',['$scope','$location','FileUploadService','$http',
                                   function($scope,$location,FileUploadService,$http){
	console.log("BankDetBrkCntlr started");

	$scope.brokerCodeDBFlag =true;
	$scope.brokerCodeList = [];
	$scope.broker ={};
	
	var chq=0;
	
	$scope.openBrokerCodeDB = function(){
		$scope.brokerCodeDBFlag = false;
		$('div#brokerCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Broker Code",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.brokerCodeDBFlag = true;
			}
		});

		$('div#brokerCodeDB').dialog('open');
	}  

	$scope.saveBrokerCode = function(brkrCode){

		console.log("enter into saveBrokerCode----->"+brkrCode);
		$scope.broker.brkCode = brkrCode;
		$('div#brokerCodeDB').dialog('close');
		$scope.brokerCodeDBFlag = true;

	}

	$scope.addBankBrkDet = function(BankDetBrkForm){
		console.log("enter into addBankBrkDet"); 
		if(BankDetBrkForm.$invalid){
			console.log("Form Invalid");
			$scope.alertToast("Form Invalid");
		}else{
			
			console.log("Broker code "+$scope.broker.brkCode);
			console.log("Account number --"+$scope.broker.brkAccntNo);
			console.log("broker ifsc "+$scope.broker.brkIfsc);
			console.log("Broker bank micr" + $scope.broker.brkMicr);
			console.log("Broker bank branch "+$scope.broker.brkBnkBranch);
			
			if(chq==0){
				$scope.alertToast("Upload image first");
				return;
			}
				
			
			var response = $http.post($scope.projectName+'/updateBrkBnkD',$scope.broker);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					
					$scope.alertToast("Success");
					for(var i = 0 ; i < $scope.brokerCodeList.length;i++){
						if($scope.broker.brkCode === $scope.brokerCodeList[i] ){
							$scope.brokerCodeList.splice(i,1);
						}
					}
					$scope.broker = {};
					chq=0;
				}else{
					console.log("Error in bringing data from updateBrkBnkD");
					$scope.alertToast("Error");
				}

			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});	
		}
		
	}

	$scope.getBrkCodeByIsBnk = function(){
		
		var response = $http.post($scope.projectName+'/getBrkCodeByIsBnk');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log("Broker Code  List--->>"+data.brokerList);
				$scope.brokerCodeList = data.brokerCodeList;
			}else{
				console.log("Error in bringing data from getBrkCodeByIsBnk");
			}

		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
		
	}
	
	
	$scope.uploadCCImage = function(ccImage){
		console.log("enter into uploadDecImg function");
		var file = ccImage;
		
		if(angular.isUndefined(file) || file === null || file === ""){
			$scope.alertToast("First choose the file----->");
		}else{
			console.log('file is ' + JSON.stringify(file));
			var uploadUrl = $scope.projectName+"/upldBrkChqImgN";
			FileUploadService.uploadFileToUrl(file, uploadUrl);
			console.log("file save on server");
		       chq=1;
		}	
	}
	
	
	$scope.getBrkCodeByIsBnk();
	console.log("BankDetBrkCntlr ended");
}]);
'use strict';

var app = angular.module('application');

app.controller('ProductTypeCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	$scope.producttype = {};
	$scope.viewPTDetailsFlag = true;
	$scope.ptList =[];
	$scope.type=[];
	
	
	$scope.Submit=function(ProductTypeForm,producttype){
		console.log("Entered into Submit function");
		$scope.producttype.ptName = producttype.ptName.toUpperCase();
		var count = producttype.ptName.length;
			if(count < 3 || count > 40){
				$scope.alertToast("Please enter product type name between 3-40 characters.....");
			}else if($.inArray($scope.producttype.ptName,$scope.type)!==-1){
						$scope.alertToast("Product already exists...");
			}else{
				$scope.viewPTDetailsFlag = false;
				$('div#viewPTDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "View Product Type Details",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
				});
				$('div#viewPTDB').dialog('open');
			}
	}
	
	$scope.saveProductType = function(producttype){
		console.log("Entered into saveProductType function---");
		
		var response = $http.post($scope.projectName+'/submitProductType', producttype);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$('div#viewPTDB').dialog('close');
				$scope.successToast(data.result);
				$scope.producttype.ptName="";
				$scope.getAllProducts();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
		
	}
	
	$scope.closePTDetailsDB = function(){
		$('div#viewPTDB').dialog('close');
	}
		
	 $('#ptName').keypress(function(e) {
	       if (this.value.length == 40) {
	           e.preventDefault();
	       }
	});
	 
	 $scope.getAllProducts = function(){
		 console.log("Entered into getAllProducts function------>");
			var response = $http.post($scope.projectName+'/getProductTypeDetails');
			response.success(function(data, status, headers, config){
				if(data.result==="success"){
					$scope.ptList = data.list;
					for(var i=0;i<$scope.ptList.length;i++){
							$scope.type[i] = $scope.ptList[i].ptName;
							$scope.type.push($scope.ptList[i].ptName);
							console.log($scope.type[i]);
					}
					$scope.successToast(data.result);
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
	 }
	 
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getAllProducts();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
	
	
}]);
'use strict';

var app = angular.module('application');

app.controller('CloseLhpvTempCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){
	
	$scope.brhFlag = true;
	$scope.branch = {};
	$scope.brhList = [];
	
	$scope.getBranch = function(){
		console.log("enter into getBranch function");
		var response = $http.post($scope.projectName+'/getBrhFrCS');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.brhList = data.list;
			}else{
				console.log("error in fetching Branch Data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}     
	
	
	$scope.openBrhDB = function(){
		console.log("enter into openBrhDB function");
		$scope.brhFlag = false;
    	$('div#brhId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.brhFlag = true;
		    }
			});
		$('div#brhId').dialog('open');	
	}
	
	
	$scope.saveBranch = function(branch){
		console.log("enter into saveBranch function");
		$('div#brhId').dialog('close');	
		$scope.branch = branch;
	}
	
	
	$scope.closeLhpvTemp = function(closeTempForm){
		console.log("enter into closeLhpvTemp function = "+closeTempForm.$invalid);
		if(closeTempForm.$invalid){
			$scope.alertToast("Please fill correct form");
		}else{
			var req = {
					"brhId" : $scope.branch.branchId, 
					"date"  : $scope.lhpvDate
			};
			$('#saveId').attr("disabled","disabled");
			var response = $http.post($scope.projectName+'/closeLhpvTemp',req);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.alertToast("SUCCESS");
					$scope.branch = {};
					//$scope.brhList = [];
					$scope.lhpvDate = "";
					$('#saveId').removeAttr("disabled");
				}else{
					$scope.alertToast(data.msg);
					$('#saveId').removeAttr("disabled");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
		}
	}
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getBranch();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);
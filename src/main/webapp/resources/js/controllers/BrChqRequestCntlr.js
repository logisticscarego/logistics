'use strict';

var app = angular.module('application');

app.controller('BrChqRequestCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){

	console.log("BrChqRequestCntlr Started");
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		//$scope.viewBankMstr();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("BrChqRequestCntlr Ended");
}]);
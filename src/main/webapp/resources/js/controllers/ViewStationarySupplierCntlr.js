'use strict';

var app = angular.module('application');

app.controller('ViewStationarySupplierCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.supplierCodeDBFlag = true;
	$scope.showSupplierDetailsFlag = true;
	$scope.stsupplier ={};
	$scope.stateCodeDBFlag = true;
	$scope.supplier = {};
	
	 $scope.openSupplierCodeDB = function(){
		 $scope.supplierCodeDBFlag = false;
			$('div#supplierCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Supplier Code",
			draggable: true,
			close: function(event, ui) { 
	            $(this).dialog('destroy');
	            $(this).hide();
	        }
		});
		$('div#supplierCodeDB').dialog('open');
	 }
	 
	 $scope.getAllSupplierCodes = function(){
			console.log("entered into getAllSupplierCodes function------>");
			var response = $http.post($scope.projectName+'/getAllSupplierCodes');
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.supplierCodeList = data.supplierCodeList;
					$scope.successToast(data.result);
				}else{
					console.log(data);
				}	
				$scope.getStateForSupplier();
			});
			response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
			});
	}
	 
	 $scope.saveSupplierCode =  function(supplier){
			console.log("enter into saveSupplierCode----->"+supplier.stSupCode);
			$scope.stSupplierCode = supplier.stSupCode;
			$scope.stSupplierName = supplier.stSupName;
			$('div#supplierCodeDB').dialog('close');
			$scope.supplierCodeDBFlag = true;
			$scope.filterTextbox = "";
		}  	
	  
	  $scope.submitSupplierCode = function(stSupplierName,stSupplierCode){
		  console.log("enter into submitSupplierCode function--->"+$scope.stSupplierCode);
			$scope.code=$('#stSupName').val();
			if($scope.code === ""){
				$scope.alertToast("Please enter stationary supplier code....");
			}else{
				var response = $http.post($scope.projectName+'/stSupplierDetails', $scope.stSupplierCode);
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						$scope.stsupplier = data.stsupplier;
						$scope.stSupStateName = $scope.stsupplier.stSupState;
						$scope.showSupplierDetailsFlag = false;
						//$scope.stsupplier.creationTS =  $filter('date')($scope.stsupplier.creationTS, 'MM/dd/yyyy hh:mm:ss');
						$scope.time = $filter('date')($scope.stsupplier.creationTS, 'MM/dd/yyyy hh:mm:ss');
						$scope.successToast(data.result);
						
					}else{
						console.log(data);
					}				
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
				});
			}
		  
	  }
	  
	  
	  $scope.updateStSupplier = function(StSupplierForm){
		  console.log("Entered into updateStSupplier function--- ");
		  
		  if(StSupplierForm.$invalid){
				 if(StSupplierForm.stSupAdd.$invalid){
					$scope.alertToast("Please enter supplier address between 3-255 characters... ");
				}else if(StSupplierForm.stSupCity.$invalid){
					$scope.alertToast("Please enter supplier city between 3-40 characters... ");
				}else if(StSupplierForm.stSupState.$invalid){
					$scope.alertToast("Please enter supplier state... ");
				}else if(StSupplierForm.stSupPin.$invalid){
					$scope.alertToast("Please enter supplier Pin of 6 digits...");
				}else if(StSupplierForm.stSupPanNo.$invalid){
					$scope.alertToast("Please enter supplier PAN No. of 10 characters...");
				}else if(StSupplierForm.stSupTdsCode.$invalid){
					$scope.alertToast("Please enter supplier Tds Code between 3-40 characters... ");
				}else if(StSupplierForm.stSupGroup.$invalid){
					$scope.alertToast("Please enter supplier group between 3-40 characters... ");
				}else if(StSupplierForm.stSupSubGroup.$invalid){
					$scope.alertToast("Please entersupplier sub-group between 3-40 characters... ");
				}
			}else{				
				
				var response = $http.post($scope.projectName+'/editStSupplier',$scope.stsupplier);
				console.log("********************");
				response.success(function(data, status, headers, config) {
					if(data.result==="success"){
						console.log("success");
						$scope.successToast(data.result);
						$scope.stsupplier="";
						$scope.stSupplierCode ="";
						$scope.showSupplierDetailsFlag = true;
					}else{
						console.log(data);
					}		
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data);
			    });
				
			}
		  
	  }
	  
	  $scope.openStateCodeDB = function(){
			$scope.stateCodeDBFlag = false;
			$('div#stateCodeDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				draggable: true,
				position: UDPos,
				show: UDShow,
				hide: UDHide,
				title: "State Code",
				close: function(event, ui) { 
		            $(this).dialog('destroy');
		            $(this).hide();
		        }
				});
			
			$('div#stateCodeDB').dialog('open');
		}
		
		$scope.getStateForSupplier = function(){
			console.log("Entered into getStateForSupplier function------");
			var response = $http.post($scope.projectName+'/getStateForSupplier');
			response.success(function(data, status, headers, config){
				if(data.result==="success"){
					$scope.stateList = data.list;
					$scope.successToast(data.result);
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}
		
		$scope.saveStateCode =  function(state){
			console.log("enter into saveStateCode----->"+state.stateCode);
			$scope.stsupplier.stSupState = state.stateCode;
			$scope.stSupStateName = state.stateName;
			$('div#stateCodeDB').dialog('close');
			$scope.stateCodeDBFlag = true;
		 }
		
		
		 $('#supAdd').keypress(function(e) {
		       if (this.value.length == 255) {
		           e.preventDefault();
		       }
		 });
		 
		 $('#supCity').keypress(function(e) {
		       if (this.value.length == 40) {
		           e.preventDefault();
		       }
		 });
		 
		 $('#supPin').keypress(function(key) {
		       if(key.charCode < 48 || key.charCode > 57)
		           return false;
		 });

	     $('#supPin').keypress(function(e) {
			   if (this.value.length == 6) {
		           e.preventDefault();
		       }
		 });
		   
		 $('#supPanNo').keypress(function(e) {
			   if (this.value.length == 10) {
		           e.preventDefault();
		       }
		 });
		 
		 $('#supTdsCode').keypress(function(e) {
			   if (this.value.length == 40) {
		           e.preventDefault();
		       }
		 });
		 
		 $('#supGroup').keypress(function(e) {
			   if (this.value.length == 40) {
		           e.preventDefault();
		       }
		 });
		 
		 $('#supSubGroup').keypress(function(e) {
			   if (this.value.length == 40) {
		           e.preventDefault();
		       }
		 });
	 
		 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
			 $scope.getAllSupplierCodes();
		 }else if($scope.logoutStatus === true){
			 $location.path("/");
		 }else{
			 console.log("****************");
		 } 
		
	
	
}]);
'use strict';

var app = angular.module('application');

app.controller('HoChqReceiveCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){

	console.log("HoChqReceiveCntlr Started");

	$scope.byBranchRBFlag = false;
	$scope.byBankRBFlag = false;
	$scope.byRefNoRBFlag = false;
	
	$scope.showChqReqFlag = false;
	$scope.fromChqNoFlag = false;
	$scope.toChqNoFalg = false;
	$scope.showChqReqFlag = false;
	$scope.showTableFlag = false;
	$scope.chqBookValidFlag = true;
	
	$scope.branchList	= [];
	$scope.bankMstrList = [];
	$scope.bMstrListByBnk = [];
	$scope.chqReqList = [];
	$scope.chequeBookList = [];
	
	$scope.branch = {};
	$scope.bMstrByBnk = {};
	$scope.bankMstr = {};
	$scope.chqReq = {};
	$scope.chequeBook = {};
	
	$scope.bMstr = {};

	$scope.branchDBFlag = true;
	$scope.bankMstrDBFlag = true;
	$scope.chqReqDBFlag = true;
	$scope.bankByBnkDBFlag = true;
	$scope.enterChqDBFlag = true;
	
	$scope.sNo = "";
	$scope.totalChq = "";
	//$scope.totalChqCount = "";
	
	$scope.chequeBook.chqBkFromChqNo = "";
	$scope.chequeBook.chqBkToChqNo = "";
	$scope.chequeBook.chqBkNOChq = "";
	
	
	$scope.byBranchRB = function(){
		console.log("entered into: byBranchRB()");
		if ($scope.branchList.length < 1) {
			$scope.getAllBranchesList();
			console.log("getBankMstr() called: ")
		}else{
			console.log("getBankMstr() not called: ")
		}

		$scope.showChqReqFlag = false;
		$scope.showTableFlag = false;
		$scope.chequeBookList = [];
		
		$scope.byBranchRBFlag = true;
		$scope.byBankRBFlag = false;
		$scope.byRefNoRBFlag = false;

		console.log("byBranchRBFlag==>"+$scope.byBranchRBFlag);
		console.log("byBankRBFlag==>"+$scope.byBankRBFlag);
		console.log("byRefNoRBFlag==>"+$scope.byRefNoRBFlag);
	}

	$scope.byBankRB = function(){
		console.log("entered into: byBankRB");
		if ($scope.bMstrListByBnk.length < 1) {
			$scope.getAllBankList();
			console.log("byBankRB() called: ")
		}else{
			console.log("byBankRB() not called: ")
		}

		$scope.showChqReqFlag = false;
		$scope.showTableFlag = false;
		$scope.chequeBookList = [];
		
		$scope.byBranchRBFlag = false;
		$scope.byBankRBFlag = true;
		$scope.byRefNoRBFlag = false;

		console.log("byBranchRBFlag==>"+$scope.byBranchRBFlag);
		console.log("byBankRBFlag==>"+$scope.byBankRBFlag);
		console.log("byRefNoRBFlag==>"+$scope.byRefNoRBFlag);
	}

	$scope.byRefNoRB = function(){
		console.log("entered into: bankDissRB");
		/*if ($scope.assignedBankList.length < 1) {
			$scope.getAssignedBank();
			console.log("assignedBankList called: ")
		}else{
			console.log("assignedBankList not called: ")
		}*/
		
		$scope.byBranchRBFlag = false;
		$scope.byBankRBFlag = false;
		$scope.byRefNoRBFlag = true;

		console.log("byBranchRBFlag==>"+$scope.byBranchRBFlag);
		console.log("byBankRBFlag==>"+$scope.byBankRBFlag);
		console.log("byRefNoRBFlag==>"+$scope.byRefNoRBFlag);
	}
	
	$scope.getAllBranchesList = function(){
		console.log("getAllBranchesList Entered");
		var response = $http.post($scope.projectName+'/getAllBranchesList');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getAllBranchesList Success");
				$scope.branchList = data.branchCodeList;
				if ($scope.assignBankRBFlag) {
					$('#selectBranchId').removeAttr("disabled");
				}
				
				for ( var i = 0; i < $scope.branchList.length; i++) {
					console.log("Branch Name: "+$scope.branchList[i].branchName);
					console.log("Branch Code: "+$scope.branchList[i].branchCode);
					console.log("Branch Address: "+$scope.branchList[i].branchAdd);
				}
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
	}
	
	$scope.openBranchDB = function(){
		console.log("Entered into openBranchDB");
		$scope.showChqReqFlag = false;
		$scope.showTableFlag = false;
		$scope.chequeBookList = [];
		
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});

		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch =  function(branch){
		console.log("saveBranch");
		console.log("enter into branchName----->"+branch.branchName);
		console.log("enter into branchFaCode----->"+branch.branchFaCode);
		$('#bankMstrId').attr("disabled","disabled");
		$scope.bankMstr = {};
		$scope.branch = branch;
		$('div#branchDB').dialog('close');
		
		var response = $http.post($scope.projectName+'/getChildBankMstrList', $scope.branch.branchId);
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				$('#bankMstrId').removeAttr("disabled");
				$scope.bankMstrList = data.bankMstrList;
				for ( var i = 0; i < $scope.bankMstrList.length; i++) {
					console.log("bankName==>"+$scope.bankMstrList[i].bnkName);
					console.log("bankFaCode==>"+$scope.bankMstrList[i].bnkFaCode);
				}
			}else {
				$('#bankMstrId').attr("disabled","disabled");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
		
	}
	
	$scope.openBankDB = function(){
		console.log("Entered into openBankDB");
		$scope.bankMstrDBFlag = false;
		$scope.showChqReqFlag = false;
		$scope.showTableFlag = false;
		$scope.chequeBookList = [];
		
		$('div#bankMstrDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bank",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.bankMstrDBFlag = true;
			}
		});

		$('div#bankMstrDB').dialog('open');
	}
	
	$scope.saveBank =  function(bankMstr){
		console.log("saveBank");
		console.log("enter into branchName----->"+bankMstr.bnkName);
		console.log("enter into branchFaCode----->"+bankMstr.bnkFaCode);
		$scope.bankMstr = bankMstr;
		$('div#bankMstrDB').dialog('close');
	}
	
	$scope.openBankByBnkDB = function(){
		console.log("Entered into openBankByBnkDB");
		$scope.showChqReqFlag = false;
		$scope.showTableFlag = false;
		$scope.chequeBookList = [];
		
		$scope.bankByBnkDBFlag = false;
		$('div#bankByBnkDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Bank",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.bankByBnkDBFlag = true;
			}
		});

		$('div#bankByBnkDB').dialog('open');
	}
	
	$scope.saveBankByBnk =  function(bankMstr){
		console.log("saveBankByBnk");
		console.log("bank Name----->"+bankMstr.bnkName);
		console.log("bankFaCode----->"+bankMstr.bnkFaCode);
		$scope.bMstrByBnk = bankMstr;
		$('div#bankByBnkDB').dialog('close');
	}
	
	$scope.submitByBranch = function(byBranchForm){
		if(byBranchForm.$invalid){
			if (byBranchForm.branchName.$invalid) {
				$scope.alertToast("Please select branch");
			} else if (byBranchForm.bankMstrName.$invalid) {
				$scope.alertToast("Please select bank");
			}
		}else{
			var response = $http.post($scope.projectName+'/getChqReqByBank', $scope.bankMstr);
			response.success(function(data, status, headers, config){
				
				if (data.result === "success") {
					$scope.alertToast("Success");
					$scope.chqReqList = data.chqReqList;
					for ( var i = 0; i < $scope.chqReqList.length; i++) {
						console.log("refNo==>"+$scope.chqReqList[i].cReqRefNo);
						console.log("received==>"+$scope.chqReqList[i].cReqIsReceived);
						console.log("cancelled==>"+$scope.chqReqList[i].cReqIsCancelled);
					}
					$scope.openChqReqDB();
				}else {
					$scope.alertToast("no cheque request generated for this bank");
				}
			});
			response.error(function(data, status, headers, config){
				console.log("getBankMstr Error: "+data);
			});
		}
	};
	
	$scope.openChqReqDB = function(){
		console.log("Entered into openChqReqDB");
		$scope.chqReqDBFlag = false;
		$('div#chqReqDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Requested Cheques",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.chqReqDBFlag = true;
			}
		});

		$('div#chqReqDB').dialog('open');
	}
	
	$scope.selectChqReq =  function(chqReq){
		console.log("selectChqReq()");
		$scope.chqReq = chqReq;
		$('div#chqReqDB').dialog('close');
		$scope.showChqReqFlag = true;
		$scope.showTableFlag = false;
		$scope.chequeBookList = [];
		console.log("Ref No: "+$scope.chqReq.cReqRefNo);
	}
	
	$scope.getAllBankList =  function(){
		
		var response = $http.post($scope.projectName+'/getAllbankMstr');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				$scope.bMstrListByBnk = data.bankMstrList;
				for ( var i = 0; i < $scope.bMstrListByBnk.length; i++) {
					console.log("bankName==>"+$scope.bMstrListByBnk[i].bnkName);
					console.log("bankFaCode==>"+$scope.bMstrListByBnk[i].bnkFaCode);
				}
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getAllBankList Error: "+data);
		});
		
	}
	
	$scope.submitByBank = function(byBankForm){
		if(byBankForm.$invalid){
			if (byBankForm.bMstrByBnkName.$invalid) {
				$scope.alertToast("Please select bank");
			}
		}else{
			var response = $http.post($scope.projectName+'/getChqReqByBank', $scope.bMstrByBnk);
			response.success(function(data, status, headers, config){
				
				if (data.result === "success") {
					$scope.alertToast("Success");
					$scope.chqReqList = data.chqReqList;
					for ( var i = 0; i < $scope.chqReqList.length; i++) {
						console.log("refNo==>"+$scope.chqReqList[i].cReqRefNo);
						console.log("received==>"+$scope.chqReqList[i].cReqIsReceived);
						console.log("cancelled==>"+$scope.chqReqList[i].cReqIsCancelled);
					}
					$scope.openChqReqDB();
				}else {
					$scope.alertToast("no cheque request generated for this bank");
				}
			});
			response.error(function(data, status, headers, config){
				console.log("getBankMstr Error: "+data);
			});
		}
	};
	
	$scope.enterChq = function(){
		
		if ($scope.chequeBookList.length < 1) {
			//initial value for enterCheque
			$scope.sNo = 1;
			$scope.totalChq = parseInt($scope.chqReq.cReqNOBook)*parseInt($scope.chqReq.cReqNOChqPerBook);
		}
		console.log("enterChq()");
		$scope.alertToast("Enter cheque one by one");
		
		$scope.enterChqDBFlag = false;
		$('div#enterChqDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Enter Cheque",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.enterChqDBFlag = true;
			}
		});

		$('div#enterChqDB').dialog('open');
	}
	
	$scope.fromChqNo = function(){
		console.log("fromChqNo()");
		
		if(angular.isUndefined($scope.chequeBook.chqBkFromChqNo) || $scope.chequeBook.chqBkFromChqNo === "" || $scope.chequeBook.chqBkFromChqNo === null){
			$scope.fromChqNoFlag = false;
		}else{
			$scope.fromChqNoFlag = true;
		}
		$scope.totalNoOfChqs();
	}
	
	$scope.toChqNo = function(){
		console.log("toChqNo()");
		if(angular.isUndefined($scope.chequeBook.chqBkToChqNo) || $scope.chequeBook.chqBkToChqNo === "" || $scope.chequeBook.chqBkToChqNo === null){
			$scope.toChqNoFalg = false;
		}else{
			$scope.toChqNoFalg = true;
		}
		$scope.totalNoOfChqs();
	}
	
	$scope.totalNoOfChqs = function(){
		if ($scope.fromChqNoFlag && $scope.toChqNoFalg) {
			$scope.chequeBook.chqBkNOChq = parseInt($scope.chequeBook.chqBkToChqNo)-parseInt($scope.chequeBook.chqBkFromChqNo)+1; 
			if (parseInt($scope.chequeBook.chqBkNOChq)>0) {
				$scope.alertLongToast("Total no of cheques: "+$scope.chequeBook.chqBkNOChq);
			}
		}
	}
	
	$scope.okEnterChq = function(enterChqForm){
		console.log("okEnterChq()");
		$scope.chqBookValidFlag = true;
		$scope.showTableFlag = true;
		if (parseInt($scope.chequeBook.chqBkNOChq)>0) {
			if ($scope.chequeBookList.length == 0) {
				//$scope.totalChqCount = $scope.chequeBook.chqBkNOChq;
				if (parseInt($scope.chequeBook.chqBkNOChq)<=parseInt($scope.totalChq)) {
					$scope.chequeBookList.push($scope.chequeBook);
					$scope.totalChq = parseInt($scope.totalChq) - parseInt($scope.chequeBook.chqBkNOChq);
					console.log("push 1st book into list");
				}else {
					$scope.alertToast("Invalid");
					$scope.alertToast("Entered cheque greater than total cheques");
				}
			} else {
				if ($scope.chequeBookList.length > 0) {
					for ( var i = 0; i < $scope.chequeBookList.length; i++) {
						if (parseInt($scope.chequeBook.chqBkFromChqNo) >= $scope.chequeBookList[i].chqBkFromChqNo	
								&& parseInt($scope.chequeBook.chqBkFromChqNo) <= $scope.chequeBookList[i].chqBkToChqNo) {
							$scope.alertToast("please enter valid entry from");
							$scope.chqBookValidFlag = false;

						}
						if (parseInt($scope.chequeBook.chqBkToChqNo) >= $scope.chequeBookList[i].chqBkFromChqNo	
								&& parseInt($scope.chequeBook.chqBkToChqNo) <= $scope.chequeBookList[i].chqBkToChqNo) {
							$scope.alertToast("please enter valid entry to");
							$scope.chqBookValidFlag = false;
						}
						if (parseInt($scope.chequeBook.chqBkFromChqNo) < parseInt($scope.chequeBookList[i].chqBkFromChqNo)	
								&& parseInt($scope.chequeBook.chqBkToChqNo) > parseInt($scope.chequeBookList[i].chqBkToChqNo)) {
							$scope.alertToast("please enter valid entry to");
							$scope.chqBookValidFlag = false;
						}
					}
					if ($scope.chqBookValidFlag) {
						//$scope.totalChqCount = parseInt($scope.totalChqCount)+parseInt($scope.chequeBook.chqBkNOChq);
						if (parseInt($scope.chequeBook.chqBkNOChq)<=parseInt($scope.totalChq)) {
							$scope.chequeBookList.push($scope.chequeBook);
							$scope.totalChq = parseInt($scope.totalChq) - parseInt($scope.chequeBook.chqBkNOChq);
							console.log("push more than one book into list");
						}else {
							$scope.alertToast("Invalid");
							$scope.alertToast("Entered cheque greater than total cheques");
						}
					}
				}
			}
			
			$scope.chequeBook = {};
		}else {
			$scope.alertToast("Please Enter valid cheque no");
		}
		for ( var i = 0; i < $scope.chequeBookList.length; i++) {
			console.log("from chq no"+i+"===>"+$scope.chequeBookList[i].chqBkFromChqNo);
			console.log("from chq no"+i+"===>"+$scope.chequeBookList[i].chqBkToChqNo);
		}
		
	}
	
	$scope.closeEnterChqBtn = function(){
		console.log("closeEnterChqBtn()");
		$('div#enterChqDB').dialog('close');
	}
	
	$scope.removeChqBook = function(chqBook, index){
		console.log("removeChqBook: "+index);
		$scope.chequeBookList.splice(index, 1);
		$scope.totalChq = parseInt($scope.totalChq) + parseInt(chqBook.chqBkNOChq);
	}
	
	$scope.submitChequeBook = function(){
		console.log("submitChequeBook()");
		
		if (parseInt($scope.totalChq) === 0) {
			if ($scope.byBranchRBFlag) {
				console.log("branch bank object");
				$scope.bMstr = $scope.bankMstr;
			}else if ($scope.byBankRBFlag) {
				console.log("bank object");
				$scope.bMstr = $scope.bMstrByBnk;
			}
			
			var chequeService = {
					"chequeBookList"	: $scope.chequeBookList,
					"chqReq"			: $scope.chqReq,
					"bankMstr"			: $scope.bMstr
			};
			
			var response = $http.post($scope.projectName+'/submitChequeBook', chequeService);
			response.success(function(data, status, headers, config){
				
				if (data.result === "success") {
					$scope.alertToast("Success");
					//Release the resources
					$scope.byBranchRBFlag = false;
					$scope.byBankRBFlag = false;
					$scope.byRefNoRBFlag = false;
					
					$scope.showChqReqFlag = false;
					$scope.fromChqNoFlag = false;
					$scope.toChqNoFalg = false;
					$scope.showChqReqFlag = false;
					$scope.showTableFlag = false;
					$scope.chqBookValidFlag = true;
					
					$scope.chqReqList = [];
					$scope.chequeBookList = [];
					
					$scope.branch = {};
					$scope.bMstrByBnk = {};
					$scope.bankMstr = {};
					$scope.chqReq = {};
					$scope.chequeBook = {};
					$scope.bMstr = {};
					
					$scope.branchDBFlag = true;
					$scope.bankMstrDBFlag = true;
					$scope.chqReqDBFlag = true;
					$scope.bankByBnkDBFlag = true;
					$scope.enterChqDBFlag = true;
					
					$scope.sNo = "";
					$scope.totalChq = "";
					//$scope.totalChqCount = "";
					
					$scope.chequeBook.chqBkFromChqNo = "";
					$scope.chequeBook.chqBkToChqNo = "";
					$scope.chequeBook.chqBkNOChq = "";
					
					//clear radio button
					$('input[name=receiveRBG]').attr('checked',false);
				}else {
					$scope.alertToast("Error in chequeBookList"+data.result);
				}
			});
			response.error(function(data, status, headers, config){
				console.log("chequeBookList Error: "+data);
			}); 
		} else {
			$scope.alertToast("remaining cheques are: "+$scope.totalChq);
			$scope.alertToast("Please entered remaining cheques");
		}
	}
	
	$('#chqBkFromChqNoId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#chqBkFromChqNoId').keypress(function(e) {
		if (this.value.length == 10) {
			e.preventDefault();
		}
	});
	
	$('#chqBkToChqNoId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#chqBkToChqNoId').keypress(function(e) {
		if (this.value.length == 10) {
			e.preventDefault();
		}
	});
	
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		//$scope.viewBankMstr();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("HoChqReceiveCntlr Ended");
}]);
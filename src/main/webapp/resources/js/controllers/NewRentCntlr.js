'use strict';

var app = angular.module('application');

app.controller('NewRentCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	console.log("NewRentCntlr Started");
	
	$scope.branchList = [];
	$scope.employeeList = [];
	$scope.employee = {};
	$scope.rent = {};
	$scope.branch = {};
	//$scope.address = {};
	$scope.add = {};
	
	
	$scope.branchDBFlag = true;
	$scope.employeeDBFlag = true;
	$scope.addDBFlag = true;
	$scope.saveRentDBFlag = true;
	
	$scope.addFlag = false;
	$scope.employeeFlag = false;
	
	$scope.getAllBranchesList = function(){
		console.log("getAllBranchesList Entered");
		var response = $http.post($scope.projectName+'/getAllBranchesList');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getAllBranchesList Success");
				$scope.branchList = data.branchCodeList;
				
				for ( var i = 0; i < $scope.branchList.length; i++) {
					console.log("Branch Name: "+$scope.branchList[i].branchName);
					console.log("Branch Code: "+$scope.branchList[i].branchCode);
					console.log("Branch Address: "+$scope.branchList[i].branchAdd);
				}
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBankMstr Error: "+data);
		});
	}
	
	$scope.openBranchDB = function(){
		console.log("Entered into openBranchDB");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});

		$('div#branchDB').dialog('open');
	}
	
	$scope.saveBranch =  function(branch){
		console.log("saveBranch");
		console.log("enter into branchName----->"+branch.branchName);
		console.log("enter into branchFaCode----->"+branch.branchFaCode);
		$scope.branch = branch;
		$('div#branchDB').dialog('close');
	}
	
	$scope.openEmployeeDB = function(){
		console.log("Entered into openEmployeeDB");
		$scope.employeeDBFlag = false;
		$('div#employeeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Employee",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.employeeDBFlag = true;
			}
		});

		$('div#employeeDB').dialog('open');
	}
	
	$scope.saveEmployee =  function(employee){
		console.log("employee");
		console.log("enter into branchName----->"+employee.empName);
		console.log("enter into branchFaCode----->"+employee.empFaCode);
		$scope.employee = employee;
		$('div#employeeDB').dialog('close');
	}
	
	$scope.openAddDB = function(){
		$scope.addDBFlag = false;
			$('div#addDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Address",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy');
					$(this).hide();
					$scope.addDBFlag = true;
				}
			});

		$('div#addDB').dialog('open');
	}

	$scope.saveAdd = function(addForm) {
		console.log("completeAdd "+$scope.add.completeAdd);
		console.log("addCity "+$scope.add.addCity);
		console.log("addState "+$scope.add.addState);
		console.log("addPin "+$scope.add.addPin);
		$('div#addDB').dialog('close');
		$scope.addDBFlag = true;
		
		if (addForm.$invalid) {
			if (addForm.address.$invalid) {
				$scope.alertToast("enter correct address in current address");
			} else if (addForm.addCity.$invalid) {
				$scope.alertToast("city name should be between 3-15 characters in current address");
			} else if (addForm.addState.$invalid) {
				$scope.alertToast("enter correct State in current address");
			} else if (addForm.addPin.$invalid) {
				$scope.alertToast("Pin in Current address should be of 6 digits");
			} 
			$scope.addFlag = false;
		} else {
			$scope.alertToast("Your entry is saved Temporarily");
			//$scope.rent.address = $scope.add;
			$scope.addFlag = true;
		}
	}
	
	$scope.submit = function(newRentForm){
		console.log("enter into submit function = "+newRentForm.$invalid);
		if(newRentForm.$invalid){
			if (newRentForm.branchName.$invalid) {
				$scope.alertToast("enter correct Branch");
			} else if (newRentForm.rentMLandLordName.$invalid) {
				$scope.alertToast("enter correct Land Lord name");
			} else if (newRentForm.rentMRentPMName.$invalid) {
				$scope.alertToast("enter correct Rent per month");
			} else if (newRentForm.rentMAdvName.$invalid) {
				$scope.alertToast("enter correct Advance");
			} else if (newRentForm.rentMSecDepName.$invalid) {
				$scope.alertToast("enter correct Security Deposite");
			} else if (newRentForm.rentMAgreementFromName.$invalid) {
				$scope.alertToast("enter correct Agreement from date");
			} else if (newRentForm.rentMAgreementToName.$invalid) {
				$scope.alertToast("enter correct Agreement to date");
			} else if (newRentForm.rentMPanNoName.$invalid) {
				$scope.alertToast("enter correct Pan No.");
			} else if (newRentForm.rentMBankName.$invalid) {
				$scope.alertToast("enter correct Bank name");
			} else if (newRentForm.rentMIFSCName.$invalid) {
				$scope.alertToast("enter correct IFSC code");
			} 
		}else{
			console.log("final submission");
			
			if($scope.addFlag){
				if ($scope.employeeFlag) {
					if(angular.isUndefined($scope.employee.empName) || $scope.employee.empName === "" || $scope.employee.empName === null){
						$scope.alertToast("Please select Employee");
						
					} else {
						$scope.openDBTemp();
					}
				} else {
					$scope.openDBTemp();
				}
				
			} else {
				$scope.alertToast("Please enter address");
			}
		}
	}
	
	$scope.openDBTemp = function() {
		$scope.saveRentDBFlag = false;
    	$('div#saveRentDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Rent Detail",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#saveRentDB').dialog('open');
	}
	
	$scope.back = function(){
		console.log("enter into back function");
		$scope.saveRentDBFlag = true;
		$('div#saveRentDB').dialog('close');
	}
	
	
	$scope.saveRent = function(){
		console.log("enter into saveRent function");
		$scope.saveRentDBFlag = true;
		$('div#saveRentDB').dialog('close');
		
		var rentService = {
				"rentMstr"		: $scope.rent,
				"branchId"		: $scope.branch.branchId,
				"address"		: $scope.add,
				"empId"			: $scope.employee.empId
		}
			
		var response = $http.post($scope.projectName+'/saveRent', rentService);
		response.success(function(data, status, headers, config){
					
			if(data.result === "success"){
				$scope.alertToast(data.result);
				//empty the resources
				$scope.employee = {};
				$scope.rent = {};
				$scope.branch = {};
				$scope.add = {};
						
				$scope.branchDBFlag = true;
				$scope.employeeDBFlag = true;
				$scope.addDBFlag = true;
				$scope.saveRentDBFlag = true;
				
				$scope.addFlag = false;
				$scope.employeeFlag = false;
				
				$scope.rent.rentMFor = "office";
				$('#employeeId').attr("disabled","disabled");
			}else{
				$scope.alertToast(data.result);
			}
					
		});
		response.error(function(data, status, headers, config){
			alert(data);
		});
			
	} 	
		
	$scope.selectEmpType = function() {
		console.log("selectEmpType()")
		if ($scope.rent.rentMFor == 'office') {
			console.log("Office");
			$('#employeeId').attr("disabled","disabled");
			$scope.employee = "";
			$scope.employeeFlag = false;
		} else if($scope.rent.rentMFor == 'staff') {
			console.log("Staff");
			//if staff than load employee list
			$scope.employeeFlag = true;
			if ($scope.employeeList.length < 1) {
				$scope.getAllEmployees();
				console.log("getAllEmployees() called: ")
			}else{
				console.log("getAllEmployees() not called: ")
			}
			
			$('#employeeId').removeAttr("disabled");
		}
	}
	
	$scope.getAllEmployees = function() {
		var response = $http.post($scope.projectName+'/getAllEmployees');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.employeeList = data.empCodeList;
			}else{
				console.log(data.result);
				$scope.alertToast("Employee List"+data.result);
			}
		});
		response.error(function(data, status, headers, config){
			alert(data);
		});
	}
	
	$('#addPinId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#addPinId').keypress(function(e) {
		if (this.value.length == 6) {
			e.preventDefault();
		}
	});
	
	$('#addressId').keypress(function(e) {
        if (this.value.length == 255) {
            e.preventDefault();
        }
    });
	
	$('#addCityId').keypress(function(e) {
        if (this.value.length == 40) {
            e.preventDefault();
        }
    });
	
	$('#addStateId').keypress(function(e) {
        if (this.value.length == 40) {
            e.preventDefault();
        }
    });
	
	$('#rentMLandLordNameId').keypress(function(e) {
        if (this.value.length == 50) {
            e.preventDefault();
        }
    });
	
	$('#rentMRentPMId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#rentMRentPMId').keypress(function(e) {
		if (this.value.length == 9) {
			e.preventDefault();
		}
	});
	
	$('#rentMAdvId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#rentMAdvId').keypress(function(e) {
		if (this.value.length == 9) {
			e.preventDefault();
		}
	});
	
	$('#rentMSecDepId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#rentMSecDepId').keypress(function(e) {
		if (this.value.length == 9) {
			e.preventDefault();
		}
	});
	
	$('#rentMPanNoId').keypress(function(e) {
		if (this.value.length == 15) {
			e.preventDefault();
		}
	});
	
	$('#rentMBankNameId').keypress(function(e) {
		if (this.value.length == 50) {
			e.preventDefault();
		}
	});
	
	$('#rentMIFSCId').keypress(function(e) {
		if (this.value.length == 15) {
			e.preventDefault();
		}
	});
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getAllBranchesList();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
	console.log("NewRentCntlr Ended");
	
}]);
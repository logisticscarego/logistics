'use strict';

angular.module('application').service('FileUploadService', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl){
    	console.log("enter into FileUploadService fucntion");
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
        	console.log("message from server after image = "+data);
        	if (data.result === "success") {
        		toast("image successfully inserted", 4000);
			}else{
				toast("image not inserted at the time of entry",4000);
			}
        	/*alert(data);*/
        	
        })
        .error(function(data){
        	/*alert(data);*/
        	toast(data, 4000);
        });
    }
    
    this.uploadBfFileToUrl = function(file, uploadUrl,bfId){
    	console.log("enter into FileUploadService fucntion");
        var fd = new FormData();
        fd.append('file', file);
        fd.append('bfId', bfId);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
        	console.log("message from server after image = "+data);
        	if (data.result === "success") {
        		toast("image successfully inserted", 4000);
			}else{
				toast("image not inserted at the time of entry",4000);
			}
        	/*alert(data);*/
        	
        })
        .error(function(data){
        	/*alert(data);*/
        	toast(data, 4000);
        });
    }
    
    
    
    this.uploadExcelFileToUrl = function(file, uploadUrl){
    	console.log("enter into uploadExcelFileToUrl fucntion");
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
        	console.log("message from server after file = "+data);
        	if (data.result === "success") {
        		toast("image successfully inserted", 4000);
        		toast(data.msg,4000);
			}else{
				toast("image not inserted at the time of entry",4000);
				toast(data.msg,4000);
			}
        	/*alert(data);*/
        	
        })
        .error(function(data){
        	/*alert(data);*/
        	toast(data, 4000);
        	toast(data.msg);
        });
    }
    
    
    this.uploadIdFileToUrl = function(file, uploadUrl, id){
    	console.log("enter into uploadIdFileToUrl fucntion");
        var fd = new FormData();
        fd.append('file', file);
        fd.append('id', id);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
        	console.log("message from server after file = "+data);
        	if (data.result === "success") {
        		toast("image successfully inserted", 4000);
        		toast(data.msg,4000);
			}else{
				toast("image not inserted at the time of entry",4000);
				toast(data.msg,4000);
			}
        	/*alert(data);*/
        	
        })
        .error(function(data){
        	/*alert(data);*/
        	toast(data, 4000);
        	toast(data.msg);
        });
    }
    
    
    
}]);
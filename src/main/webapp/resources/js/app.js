'use strict';

var application = angular.module('application',['ngRoute','ngResource','ui.bootstrap', 'ngAnimate', 'ngMessages']);

application.config(function ($routeProvider,$locationProvider) {
	console.log("----app.js Started----");
    $routeProvider.when('/',{
    	templateUrl: 'index',
        controller: 'HeaderCntlr'
    })/*.when('/login',{
    	templateUrl: 'login',
        controller: 'UserCntlr'
    })*/.when('/operator',{
    	templateUrl: 'operator',
        controller: 'OperatorCntlr'
    }).when('/admin',{
    	templateUrl: 'admin',
        controller: 'AdminCntlr'
    }).when('/superAdmin',{
    	templateUrl: 'superAdmin',
        controller: 'SuperAdminCntlr'
    }).when('/operator/regularcontract',{
    	templateUrl: 'regularcontract',
        controller: 'RegularContractCntlr'
    }).when('/operator/dailycontract',{
    	templateUrl: 'dailycontract',
        controller: 'DailyContractCntlr'
    }).when('/operator/viewregularcontract',{
    	templateUrl: 'viewregularcontract',
        controller: 'RegularContractCntlr'
    }).when('/operator/viewcontract',{
    	templateUrl: 'viewcontract',
        controller: 'ViewContractCntlr'
    }).when('/operator/customer',{
    	templateUrl: 'customer',
        controller: 'CustomerCntlr'
    }).when('/operator/viewcustomer',{
    	templateUrl: 'viewcustomer',
        controller: 'ViewCustomerCntlr'
    }).when('/operator/displaycustomerrepresentative',{
    	templateUrl: 'displaycustomerrepresentative',
        controller: 'ViewCustomerRepCntlr'
    }).when('/operator/customerrepresentativenew',{
    	templateUrl: 'customerrepresentativenew',
        controller: 'CustomerRepNewCntlr'
    }).when('/operator/employee',{
    	templateUrl: 'employee',
        controller: 'EmployeeCntlr'
    }).when('/operator/viewemployee',{
    	templateUrl: 'viewemployee',
        controller: 'ViewEmployeeCntlr'
    }).when('/operator/cnmt',{
    	templateUrl: 'cnmt',
        controller: 'CnmtCntlr'
    }).when('/operator/stationary',{
        templateUrl: 'stationary',
        controller: 'StationaryCntlr'
    }).when('/admin/editadmincustomer',{
    	templateUrl: 'editadmincustomer',
        controller: 'CustomerCntlr'
    }).when('/admin/verifycontract',{
    	templateUrl: 'verifycontract',
        controller: 'VerifyContractCntlr'
    }).when('/admin/verifyregularcontract',{
    	templateUrl: 'verifyregularcontract',
        controller: 'RegularContractCntlr'
    }).when('/admin/editcontract',{
    	templateUrl: 'editcontract',
        controller: 'DailyContractCntlr'
    }).when('/admin/editRegularContract',{
    	templateUrl: 'editRegularContract',
        controller: 'RegularContractCntlr'
    }).when('/admin/verifycustomer',{
    	templateUrl: 'verifycustomer',
        controller: 'VerifyCustomerCntlr'
    }).when('/operator/viewcnmt',{
    	templateUrl: 'viewcnmt',
        controller: 'ViewCnmtCntlr' 
    }).when('/operator/addcustomerrepresentative',{
        templateUrl: 'addcustomerrepresentative',
        controller: 'CustomerRepCntlr'
    }).when('/superAdmin/addUser',{
    	templateUrl: 'addUser',
        controller: 'SuperAdminCntlr'
    }).when('/superAdmin/viewUser',{
    	templateUrl: 'viewUser',
        controller: 'SuperAdminCntlr'
    }).when('/superAdmin/changePassword',{
    	templateUrl: 'changePassword',
        controller: 'ChangePasswordCntlr'
    }).when('/operator/owner',{
    	templateUrl: 'owner',
        controller: 'OwnerCntlr'
    }).when('/operator/broker',{
    	templateUrl: 'broker',
        controller: 'BrokerCntlr'
    }).when('/operator/challan',{
    	templateUrl: 'challan',
        controller: 'ChallanCntlr'
    }).when('/operator/viewChallan',{
    	templateUrl: 'viewChallan',
        controller: 'ViewChallanCntlr'
    }).when('/operator/arrivalreport',{
    	templateUrl: 'arrivalreport',
        controller: 'ArrivalReportCntlr'
    }).when('/operator/viewarrivalreport',{
    	templateUrl: 'viewarrivalreport',
        controller: 'ViewArrivalReportCntlr'
    }).when('/operator/branch',{
    	templateUrl: 'branch',
        controller: 'BranchCntlr'
    }).when('/operator/state',{
    	templateUrl: 'state',
        controller: 'StateCntlr'
    }).when('/operator/viewstate',{
    	templateUrl: 'viewstate',
        controller: 'ViewStateCntlr'
    }).when('/operator/station',{
    	templateUrl: 'station',
        controller: 'StationCntlr'
    }).when('/operator/viewstation',{
    	templateUrl: 'viewstation',
        controller: 'ViewStationCntlr'
    }).when('/operator/vehicletype',{
    	templateUrl: 'vehicletype',
        controller: 'VehicleTypeCntlr'
    }).when('/operator/viewvehicletype',{
    	templateUrl: 'viewvehicletype',
        controller: 'ViewVehicleTypeCntlr'
    }).when('/operator/producttype',{
    	templateUrl: 'producttype',
        controller: 'ProductTypeCntlr'
    }).when('/operator/viewproducttype',{
    	templateUrl: 'viewproducttype',
        controller: 'ViewProductTypeCntlr'
    }).when('/operator/viewbranch',{
    	templateUrl: 'viewbranch',
        controller: 'ViewBranchCntlr'
    }).when('/operator/creatDispatchOrder',{
    	templateUrl: 'creatDispatchOrder',
        controller: 'CreatDispatchOrderCntlr'
    }).when('/admin/displayCustomer',{
    	templateUrl: 'displayCustomer',
        controller: 'DisplayCustomerCntlr'
    }).when('/admin/displayCustRep',{
    	templateUrl: 'displayCustRep',
        controller: 'DisplayCustRepCntlr'
    }).when('/admin/displayDailyContract',{
    	templateUrl: 'displayDailyContract',
        controller: 'DisplayDailyContractCntlr'
    }).when('/admin/displayCnmt',{
    	templateUrl: 'displayCnmt',
        controller: 'DisplayCnmtCntlr'
    }).when('/admin/displayChallan',{
    	templateUrl: 'displayChallan',
        controller: 'DisplayChallanCntlr'
    }).when('/admin/displayBranch',{
    	templateUrl: 'displayBranch',
        controller: 'DisplayBranchCntlr'
    }).when('/admin/displayBroker',{
    	templateUrl: 'displayBroker',
        controller: 'DisplayBrokerCntlr'
    }).when('/admin/displayOwner',{
    	templateUrl: 'displayOwner',
        controller: 'DisplayOwnerCntlr'
    }).when('/admin/displayEmployee',{
    	templateUrl: 'displayEmployee',
        controller: 'DisplayEmployeeCntlr'
    }).when('/superAdmin/displayBranch',{
    	templateUrl: 'displayBranch',
        controller: 'DisplayBranchCntlr'
    }).when('/admin/displayRegularContract',{
    	templateUrl: 'displayRegularContract',
        controller: 'DisplayRegularContractCntlr'
    }).when('/admin/displayArrivalReport',{
    	templateUrl: 'displayArrivalReport',
        controller: 'DisplayArrivalReportCntlr'
    }).when('/operator/viewOwner',{
    	templateUrl: 'viewOwner',
        controller: 'ViewOwnerCntlr'
    }).when('/operator/viewBroker',{
    	templateUrl: 'viewBroker',
        controller: 'ViewBrokerCntlr'
    }).when('/operator/dispatchDetails',{
    	templateUrl: 'dispatchDetails',
        controller: 'DispatchDetailsCntlr'
    }).when('/operator/stationary',{
    	templateUrl: 'stationary',
        controller: 'StationaryCntlr'
    }).when('/admin/displaySupplier',{
    	templateUrl: 'displaySupplier',
        controller: 'DisplaySupplierCntlr'    	
    }).when('/operator/supplier',{
    	templateUrl: 'supplier',
        controller: 'StationarySupplierCntlr'
    }).when('/operator/viewsupplier',{
    	templateUrl: 'viewsupplier',
        controller: 'ViewStationarySupplierCntlr'
    }).when('/operator/stnOdrInv',{
    	templateUrl: 'stnOdrInv',
        controller: 'StnOdrInvCntlr'
    }).when('/admin/allowAdvance',{
    	templateUrl: 'allowAdvance',
        controller: 'AllowAdvanceCntlr'
    }).when('/admin/disallowAdvance',{
    	templateUrl: 'disallowAdvance',
        controller: 'DisallowAdvanceCntlr'
    }).when('/superAdmin/serviceTax',{
    	templateUrl: 'serviceTax',
        controller: 'ServiceTaxCntlr'
    }).when('/admin/contractRights',{
    	templateUrl: 'contractRights',
        controller: 'ContractRightsCntlr'
    }).when('/operator/newVoucher',{
    	templateUrl: 'newVoucher',
        controller: 'NewVoucherCntlr'
    }).when('/operator/viewVoucher',{
    	templateUrl: 'viewVoucher',
        controller: 'ViewVoucherCntlr'
    }).when('/operator/brkbnkDet',{
    	templateUrl: 'brkbnkDet',
        controller: 'BankDetBrkCntlr'
    }).when('/operator/ownbnkDet',{
    	templateUrl: 'ownbnkDet',
        controller: 'BankDetOwnCntlr'
    }).when('/superAdmin/dataIntegration',{
    	templateUrl: 'dataIntegration',
        controller: 'DataIntegrationCntlr'
    }).when('/operator/dlyContAuth',{
    	templateUrl: 'dlyContAuth',
        controller: 'DlyContAuthCntlr'
    }).when('/operator/newBank',{
    	templateUrl: 'newBank',
        controller: 'NewBankCntlr'
    }).when('/operator/viewBank',{
    	templateUrl: 'viewBank',
        controller: 'ViewBankCntlr'
    }).when('/operator/assignBank',{
    	templateUrl: 'assignBank',
        controller: 'AssignBankCntlr'
    }).when('/operator/newCashReciept',{
    	templateUrl: 'newCashReciept',
        controller: 'CashRecieptCntlr'
    }).when('/operator/viewCashReciept',{
    	templateUrl: 'viewCashReciept',
        controller: 'ViewCashRecieptCntlr'
    }).when('/operator/closeVoucher',{
    	templateUrl: 'closeVoucher',
        controller: 'CloseVoucherCntlr'
    }).when('/operator/newCashDeposite',{
    	templateUrl: 'newCashDeposite',
        controller: 'CashDepositeCntlr'
    }).when('/operator/newCashPayment',{
    	templateUrl: 'newCashPayment',
        controller: 'CashPaymentCntlr'
    }).when('/operator/newTelephoneVoucher',{
    	templateUrl: 'newTelephoneVoucher',
        controller: 'TelephoneVoucherCntlr'
    }).when('/operator/newPhoneAllot',{
    	templateUrl: 'newPhoneAllot',
        controller: 'PhoneAllotCntlr'
    }).when('/operator/newCRNAllot',{
    	templateUrl: 'newCRNAllot',
        controller: 'CRNAllotCntlr'
    }).when('/operator/viewCRNAllot',{
    	templateUrl: 'viewCRNAllot',
        controller: 'ViewCRNAllotCntlr'
    }).when('/operator/newElectVoucher',{
    	templateUrl: 'newElectVoucher',
        controller: 'ElectVoucherCntlr'
    }).when('/operator/viewElectVoucher',{
    	templateUrl: 'viewElectVoucher',
        controller: 'ViewElectVoucherCntlr'
    }).when('/operator/newVehAllot',{
    	templateUrl: 'newVehAllot',
        controller: 'VehAllotCntlr'
    }).when('/operator/viewVehAllot',{
    	templateUrl: 'viewVehAllot',
        controller: 'ViewVehAllotCntlr'
    }).when('/operator/newVehVoucher',{
    	templateUrl: 'newVehVoucher',
        controller: 'VehVoucherCntlr'
    }).when('/operator/viewVehVoucher',{
    	templateUrl: 'viewVehVoucher',
        controller: 'ViewVehVoucherCntlr'
    }).when('/operator/newTravVoucher',{
    	templateUrl: 'newTravVoucher',
        controller: 'TravVoucherCntlr'
    }).when('/operator/viewTravVoucher',{
    	templateUrl: 'viewTravVoucher',
        controller: 'ViewTravVoucherCntlr'
    }).when('/operator/newBPVoucher',{
    	templateUrl: 'newBPVoucher',
        controller: 'BPVoucherCntlr'
    }).when('/operator/viewBPVoucher',{
    	templateUrl: 'viewBPVoucher',
        controller: 'ViewBPVoucherCntlr'
    }).when('/operator/hoChqRequest',{
    	templateUrl: 'hoChqRequest',
        controller: 'HoChqRequestCntlr'
    }).when('/operator/hoChqReceive',{
    	templateUrl: 'hoChqReceive',
        controller: 'HoChqReceiveCntlr'
    }).when('/operator/hoChqIssue',{
    	templateUrl: 'hoChqIssue',
        controller: 'HoChqIssueCntlr'
    }).when('/operator/hoChqStatus',{
    	templateUrl: 'hoChqStatus',
        controller: 'HoChqStatusCntlr'
    }).when('/operator/brChqRequest',{
    	templateUrl: 'brChqRequest',
        controller: 'BrChqRequestCntlr'
    }).when('/operator/brChqReceive',{
    	templateUrl: 'brChqReceive',
        controller: 'BrChqReceiveCntlr'
    }).when('/operator/brChqStatus',{
    	templateUrl: 'brChqStatus',
        controller: 'BrChqStatusCntlr'
    }).when('/operator/newChqPayVoucher',{
    	templateUrl: 'newChqPayVoucher',
        controller: 'ChqPayVoucherCntlr'
    }).when('/operator/viewChqPayVoucher',{
    	templateUrl: 'viewChqPayVoucher',
        controller: 'ViewChqPayVoucherCntlr'
    }).when('/operator/newJourVoucher',{
    	templateUrl: 'newJourVoucher',
        controller: 'JourVoucherCntlr'
    }).when('/operator/viewJourVoucher',{
    	templateUrl: 'viewJourVoucher',
        controller: 'ViewJourVoucherCntlr'
    }).when('/operator/newRevVoucher',{
    	templateUrl: 'newRevVoucher',
        controller: 'RevVoucherCntlr'
    }).when('/operator/viewRevVoucher',{
    	templateUrl: 'viewRevVoucher',
        controller: 'ViewRevVoucherCntlr'
    }).when('/operator/newIntBrTVCrDr',{
    	templateUrl: 'newIntBrTVCrDr',
        controller: 'IntBrTVCrDrCntlr'
    }).when('/operator/viewIntBrTVCrDr',{
    	templateUrl: 'viewIntBrTVCrDr',
        controller: 'ViewIntBrTVCrDrCntlr'
    }).when('/operator/clrPendingIBTV',{
    	templateUrl: 'clrPendingIBTV',
        controller: 'ClrPendingIBTVCntlr'
    }).when('/operator/empWiseAssgn',{
    	templateUrl: 'empWiseAssgn',
        controller: 'EmpWiseAssgnCntlr'
    }).when('/operator/brhWiseAssgn',{
    	templateUrl: 'brhWiseAssgn',
        controller: 'BrhWiseAssgnCntlr'
    }).when('/operator/asgnUser',{
    	templateUrl: 'asgnUser',
        controller: 'AsgnUserCntlr'
    }).when('/admin/addContToStn',{
    	templateUrl: 'addContToStn',
        controller: 'AddContToStnCntlr'
    }).when('/admin/cnDetReport',{
    	templateUrl: 'cnDetReport',
        controller: 'CNDetReportCntlr'
    }).when('/admin/cnMisReport',{
    	templateUrl: 'cnMisReport',
        controller: 'CNMisReportCntlr'
    }).when('/superAdmin/userRights',{
    	templateUrl: 'userRights',
        controller: 'UserRightsCntlr'
    }).when('/operator/newLhpvTemp',{
    	templateUrl: 'newLhpvTemp',
        controller: 'LhpvTempCntlr'
    }).when('/operator/bTBFundTrans',{
    	templateUrl: 'bTBFundTrans',
        controller: 'BTBFundTransCntlr'
    }).when('/operator/newRent',{
    	templateUrl: 'newRent',
        controller: 'NewRentCntlr'
    }).when('/operator/viewRent',{
    	templateUrl: 'viewRent',
        controller: 'ViewRentCntlr'
    }).when('/operator/newAtmCard',{
    	templateUrl: 'newAtmCard',
        controller: 'NewAtmCardCntlr'
    }).when('/operator/viewAtmCard',{
    	templateUrl: 'viewAtmCard',
        controller: 'ViewAtmCardCntlr'
    }).when('/operator/newRentVoucher',{
    	templateUrl: 'newRentVoucher',
        controller: 'RentVoucherCntlr'
    }).when('/operator/newAtmVoucher',{
    	templateUrl: 'newAtmVoucher',
        controller: 'AtmVoucherCntlr'
    }).when('/operator/viewAtmVoucher',{
    	templateUrl: 'viewAtmVoucher',
        controller: 'ViewAtmVoucherCntlr'
    }).when('/operator/cancelChq',{
    	templateUrl: 'cancelChq',
        controller: 'CancelChqCntlr'
    }).when('/operator/cashStmt',{
    	templateUrl: 'cashStmt',
        controller: 'CashStmtCntlr'
    }).when('/admin/dlyRtAllow',{
    	templateUrl: 'dlyRtAllow',
        controller: 'DlyRtAllowCntlr'
    }).when('/operator/newVehicleVendor',{
    	templateUrl: 'newVehicleVendor',
        controller: 'NewVehicleVendorCntlr'
    }).when('/operator/viewVehicleVendor',{
    	templateUrl: 'viewVehicleVendor',
        controller: 'ViewVehicleVendorCntlr'
    }).when('/operator/editVehicleVendor',{
    	templateUrl: 'editVehicleVendor',
        controller: 'EditVehicleVendorCntlr'
    }).when('/operator/addPan',{
    	templateUrl: 'addPan',
        controller: 'AddPanCntlr'
    }).when('/operator/billTemp',{
    	templateUrl: 'billTemp',
        controller: 'BillTempCntlr'
    }).when('/operator/newLhpvAdv',{
    	templateUrl: 'newLhpvAdv',
        controller: 'LhpvAdvCntlr'
    }).when('/operator/rvrsLhpvAdv',{
    	templateUrl: 'rvrsLhpvAdv',
        controller: 'RvrsLhpvAdvCntlr'
    /*.when('/operator/viewLhpvAdv',{
    	templateUrl: 'viewLhpvAdv',
        controller: 'ViewLhpvAdvCntlr'*/
    }).when('/operator/newLhpvBal',{
    	templateUrl: 'newLhpvBal',
        controller: 'LhpvBalCntlr'
    }).when('/operator/viewLhpvBal',{
    	templateUrl: 'viewLhpvBal',
        controller: 'ViewLhpvBalCntlr'
    }).when('/operator/newArvRepAlw',{
    	templateUrl: 'newArvRepAlw',
        controller: 'ArvRepAlwCntlr'
    }).when('/operator/viewArvRepAlw',{
    	templateUrl: 'viewArvRepAlw',
        controller: 'ViewArvRepAlwCntlr'
    }).when('/operator/newLhpvSup',{
    	templateUrl: 'newLhpvSup',
        controller: 'LhpvSupCntlr'
    }).when('/operator/viewLhpvSup',{
    	templateUrl: 'viewLhpvSup',
        controller: 'ViewLhpvSupCntlr'
    }).when('/operator/ledger',{
    	templateUrl: 'ledger',
        controller: 'LedgerCntlr'
    }).when('/operator/trial',{
    	templateUrl: 'trial',
        controller: 'TrialCntlr'
    }).when('/operator/closeLhpv',{
    	templateUrl: 'closeLhpv',
        controller: 'CloseLhpvCntlr'
    }).when('/superAdmin/brhWiseMuns',{
    	templateUrl: 'brhWiseMuns',
        controller: 'BrhWiseMunsCntlr'
    }).when('/superAdmin/viewBrhWiseMuns',{
    	templateUrl: 'viewBrhWiseMuns',
        controller: 'ViewBrhWiseMunsCntlr'
    }).when('/operator/newChqCancelVoucher',{
    	templateUrl: 'newChqCancelVoucher',
        controller: 'ChqCancelVoucherCntlr'
    }).when('/operator/editCashStmt',{
    	templateUrl: 'editCashStmt',
        controller: 'EditCashStmtCntlr'
    }).when('/operator/lhpvPrint',{
    	templateUrl: 'lhpvPrint',
        controller: 'LhpvPrintCntlr'
    }).when('/operator/extContDt',{
    	templateUrl: 'extContDt',
        controller: 'ExtContDtCntlr'
    }).when('/operator/editCnmt',{
    	templateUrl: 'editCnmt',
        controller: 'EditCnmtCntlr'
    }).when('/operator/panValid',{
    	templateUrl: 'panValid',
        controller: 'PanValidCntlr'
    }).when('/operator/newBill',{
    	templateUrl: 'newBill',
        controller: 'BillCntlr'
    }).when('/operator/newBillHO',{
    	templateUrl: 'newBillHO',
        controller: 'BillHOCntlr'
    }).when('/superAdmin/custBillPrmsn',{
    	templateUrl: 'custBillPrmsn',
        controller: 'CustBillPrmsnCntlr'
    }).when('/operator/editChln',{
    	templateUrl: 'editChln',
        controller: 'EditChlnCntlr'
    }).when('/operator/billFrwd',{
    	templateUrl: 'billFrwd',
        controller: 'BillFrwdCntlr'
    }).when('/operator/billSbmsn',{
    	templateUrl: 'billSbmsn',
        controller: 'BillSbmsnCntlr'
    }).when('/operator/newOnAccMR',{
    	templateUrl: 'newOnAccMR',
        controller: 'OnAccMRCntlr'
    }).when('/operator/newOnAccMRN',{
    	templateUrl: 'newOnAccMRN',
        controller: 'OnAccMRCntlrN'
    }).when('/operator/payDetMR',{
    	templateUrl: 'payDetMR',
        controller: 'PayDetMRCntlr'
    }).when('/operator/payDetMRN',{
    	templateUrl: 'payDetMRN',
        controller: 'PayDetMRNCntlr'
    }).when('/operator/dirPayMR',{
    	templateUrl: 'dirPayMR',
        controller: 'DirPayMRCntlr'
    }).when('/operator/dirPayMRN',{
    	templateUrl: 'dirPayMRN',
        controller: 'DirPayMRNCntlr'
    }).when('/admin/chlnNCnmt',{
    	templateUrl: 'chlnNCnmt',
        controller: 'ChlnNCnmtCntlr'
    }).when('/operator/lhpvStartTemp',{
    	templateUrl: 'lhpvStartTemp',
        controller: 'LhpvStartTempCntlr'
    }).when('/operator/enquiry',{
    	templateUrl: 'enquiry',
        controller: 'EnquiryCntlr'
    }).when('/operator/billPrint',{
    	templateUrl: 'billPrint',
        controller: 'BillPrintCntlr'
    }).when('/operator/brReconRpt',{
    	templateUrl: 'brReconRpt',
        controller: 'BrReconRptCntlr'
    }).when('/operator/reverseMR',{
    	templateUrl: 'reverseMR',
        controller: 'ReverseMRCntlr'
    }).when('/operator/reverseMRN',{
    	templateUrl: 'reverseMRN',
        controller: 'ReverseMRNCntlr'
    }).when('/operator/custStationary',{
    	templateUrl: 'custStationary',
    	controller: 'CustStationaryCntlr'
    }).when('/operator/mrPrint',{
    	templateUrl: 'mrPrint',
    	controller: 'MrPrintCntlr'
    }).when('/operator/mrrPrint',{
    	templateUrl: 'mrrPrint',
    	controller: 'MrrPrintCntlr'
    }).when('/operator/stockAndOsRpt',{
    	templateUrl: 'stockAndOsRpt',
    	controller: 'StockAndOsRptCntlr'
    }).when('/operator/stockRpt',{
    	templateUrl: 'stockRpt',
    	controller: 'StockRptCntlr'
    }).when('/operator/billFrwdPrint',{
    	templateUrl: 'billFrwdPrint',
    	controller: 'BillFrwdPrintCntlr'
    }).when('/operator/billCancel',{
    	templateUrl: 'billCancel',
    	controller: 'BillCancelCntlr'
    }).when('/operator/osRpt',{
    	templateUrl: 'osRpt',
    	controller: 'OsRptCntlr'
    }).when('/operator/relOsRpt',{//relianceStock
    	templateUrl: 'relOsRpt',
    	controller: 'RelOsRptCntlr'
    }).when('/operator/relianceStock',{//
    	templateUrl: 'relianceStock',
    	controller: 'RelianceStockCntlr'
    }).when('/operator/osRptN',{
    	templateUrl: 'osRptN',
    	controller: 'OsRptCntlrN'
    }).when('/operator/cancelMR',{
    	templateUrl: 'cancelMR',
    	controller: 'CancelMRCntlr'
    }).when('/operator/cancelMRN',{
    	templateUrl: 'cancelMRN',
    	controller: 'CancelMRNCntlr'
    }).when('/operator/editBill',{
    	templateUrl: 'editBill',
    	controller: 'EditBillCntlr'
    }).when('/operator/relBill',{
    	templateUrl: 'relBill',
    	controller: 'BillPrintCntlr'
    }).when('/operator/lhpvAdvTemp',{
    	templateUrl: 'lhpvAdvTemp',
    	controller: 'LhpvAdvTempCntlr'
    }).when('/operator/lhpvBalTemp',{
    	templateUrl: 'lhpvBalTemp',
    	controller: 'LhpvBalTempCntlr'
    }).when('/operator/closeLhpvTemp',{
    	templateUrl: 'closeLhpvTemp',
    	controller: 'CloseLhpvTempCntlr'
    }).when('/operator/cnmtBbl',{
    	templateUrl: 'cnmtBbl',
    	controller: 'CnmtBblCntlr'
    }).when('/operator/checkCNMTApp',{
    	templateUrl: 'cnmtApp',
    	controller: 'CnmtCntlrApp'    	
    }).when('/operator/checkChallanApp',{
    	templateUrl: 'challanApp',
    	controller: 'ChallanCntlrApp'    
    }).when('/operator/cancelChallan',{
    	templateUrl: 'cancelChallan',
    	controller: 'CancelChallanCntlr'    	
    }).when('/operator/cancelCnmt',{
    	templateUrl: 'cancelCnmt',
    	controller: 'CancelCNMTCntlr'    	
    }).when('/operator/cancelSedr',{
    	templateUrl: 'cancelSedr',
    	controller: 'CancelSedrCntlr'    	
    }).when('/operator/rvrsLhpvBal',{
    	templateUrl: 'rvrsLhpvBal',
    	controller: 'RvrsLhpvBalCntlr'    	
    }).when('/operator/vryOwner',{
    	templateUrl: 'vryOwner',
    	controller: 'OwnerCntlrApp'    		
    }).when('/operator/vryBroker',{
    	templateUrl: 'vryBroker',
    	controller: 'BrokerCntlrApp'    		
    }).when('/operator/vryVehicle',{
    	templateUrl: 'vryVehicle',
    	controller: 'VehicleCntlrApp'    		
    }).when('/operator/shortExcessRpt',{
    	templateUrl:'shortExcessRpt',
    	controller:'ShortExcessRptCntlr'
    }).when('/operator/countEntry',{
    	templateUrl:'countEntry',
    	controller:'CountEntryCntlr'    	
    }).when('/operator/brhStkInfm',{
    	templateUrl:'brhStkInfm',
    	controller:'BrhStkInfmCntlr'
    }).when('/operator/cnmtCosting',{
    	templateUrl:'cnmtCosting',
    	controller:'CnmtCostingCntlr'
    }).when('/operator/cnmtFrmToDtl',{
    	templateUrl:'cnmtFrmToDtl',
    	controller:'CnmtFrmToDtlCntlr'
    }).when('/operator/allImg',{
    	templateUrl:'allImg',
    	controller:'AllImgCntlr'    	
    }).when('/operator/stnTransfer',{
    	templateUrl:'stnTransfer',
    	controller:'StnTransferCntlr'    	
    }).when('/operator/customerGroup',{
    	templateUrl:'customerGroup',
    	controller:'CustomerGroupCntlr'
    }).when('/operator/bankStmtData',{
    	templateUrl:'bankStmtData',
    	controller:'BankStmtDataCntlr'
    }).when('/operator/trafficOff',{
    	templateUrl:'trafficOff',
    	controller:'TrafficOffCntlr'
    }).when('/operator/allowSrtgDmg',{
    	templateUrl:'allowSrtgDmg',
       	controller:'AlwSrtgDmgArCntlr'
    }).when('/operator/imgReader',{
    	templateUrl:'imgReader',
       	controller:'ImageReaderCntlr'
    }).when('/operator/relAnnextureRpt',{
    	 templateUrl:'relAnnextureRpt',
    	 controller:'RelAnnextureRptCntlr'
    }).when('/operator/holdDocs',{
    	templateUrl:'holdDocs',
       	controller:'HoldDocsCntlr'
    }).when('/operator/unHoldDocs',{
    	templateUrl:'unHoldDocs',
       	controller:'UnholdDocsCntlr'
    }).when('/operator/memo',{
    	templateUrl:'memo',
       	controller:'MemoCntlr'
    }).when('/operator/sendMsgToBranchs',{
    	templateUrl:'sendMsgToBranchs',
       	controller:'SendMsgToBranchsCntlr'
    }).when('/operator/hoStockAndOsRpt',{
    	templateUrl:'hoStockAndOsRpt',
       	controller:'HOStockAndOsRptCntlr'
    }).when('/operator/hoCnmtCosting',{
    	templateUrl:'hoCnmtCosting',
       	controller:'HOCnmtCostingCntlr'
    }).when('/operator/cnmtChlnSedrNarr',{
    	templateUrl:'cnmtChlnSedrNarr',
       	controller:'CnmtChlnSedrCntlr'
    }).when('/operator/cnmtChlnSedrNarr',{
    	templateUrl:'cnmtChlnSedrNarr',
       	controller:'CnmtChlnSedrCntlr'
    }).when('/operator/hoCashStmt',{
    	templateUrl:'hoCashStmt',
       	controller:'HOCashStmtCntlr'
    }).when('/operator/groupBill',{
    	templateUrl:'groupBill',
       	controller:'GroupBillCntlr'
    }).when('/operator/newVoucherBack',{
    	templateUrl: 'newVoucherBack',
        controller: 'NewVoucherBackCntlr'
    }).when('/operator/newLhpvAdvBack',{
    	templateUrl: 'newLhpvAdvBack',
        controller: 'LhpvAdvBackCntlr'
    }).when('/operator/closeLhpvBack',{
    	templateUrl: 'closeLhpvBack',
        controller: 'CloseLhpvBackCntlr'
    }).when('/operator/closeVoucherBack',{
    	templateUrl: 'closeVoucherBack',
        controller: 'CloseVoucherBackCntlr'
    }).when('/operator/newAtmVoucherBack',{
    	templateUrl: 'newAtmVoucherBack',
        controller: 'AtmVoucherBackCntlr'
    }).when('/operator/newCashRecieptBack',{
    	templateUrl: 'newCashRecieptBack',
        controller: 'CashRecieptBackCntlr'
    }).when('/operator/newCashDepositeBack',{
    	templateUrl: 'newCashDepositeBack',
        controller: 'CashDepositeBackCntlr'
    }).when('/operator/newCashPaymentBack',{
    	templateUrl: 'newCashPaymentBack',
        controller: 'CashPaymentBackCntlr'
    }).when('/operator/newChqPayVoucherBack',{
    	templateUrl: 'newChqPayVoucherBack',
        controller: 'ChqPayVoucherBackCntlr'
    }).when('/operator/newChqCancelVoucherBack',{
    	templateUrl: 'newChqCancelVoucherBack',
        controller: 'ChqCancelVoucherBackCntlr'
    }).when('/operator/newTelephoneVoucherBack',{
    	templateUrl: 'newTelephoneVoucherBack',
        controller: 'TelephoneVoucherBackCntlr'
    }).when('/operator/newElectVoucherBack',{
    	templateUrl: 'newElectVoucherBack',
        controller: 'ElectVoucherBackCntlr'
    }).when('/operator/newVehVoucherBack',{
    	templateUrl: 'newVehVoucherBack',
        controller: 'VehVoucherBackCntlr'
    }).when('/operator/newTravVoucherBack',{
    	templateUrl: 'newTravVoucherBack',
        controller: 'TravVoucherBackCntlr'
    }).when('/operator/newRentVoucherBack',{
    	templateUrl: 'newRentVoucherBack',
        controller: 'RentVoucherBackCntlr'
    }).when('/operator/newLhpvBalBack',{
    	templateUrl: 'newLhpvBalBack',
        controller: 'LhpvBalBackCntlr'
    }).when('/operator/newLhpvSupBack',{
    	templateUrl: 'newLhpvSupBack',
        controller: 'LhpvSupBackCntlr'
    }).when('/operator/newBPVoucherBack',{
    	templateUrl: 'newBPVoucherBack',
        controller: 'BPVoucherBackCntlr'
    }).when('/operator/newIntBrTVCrDrBack',{
    	templateUrl: 'newIntBrTVCrDrBack',
        controller: 'IntBrTVCrDrBackCntlr'
    }).when('/operator/clrPendingIBTVBack',{
    	templateUrl: 'clrPendingIBTVBack',
        controller: 'ClrPendingIBTVBackCntlr'
    }).when('/operator/newRevVoucherBack',{
    	templateUrl: 'newRevVoucherBack',
        controller: 'RevVoucherBackCntlr'
    }).when('/operator/newOnAccMRBack',{
    	templateUrl: 'newOnAccMRBack',
        controller: 'OnAccMRBackCntlr'
    }).when('/operator/payDetMRBack',{
    	templateUrl: 'payDetMRBack',
        controller: 'PayDetMRBackCntlr'
    }).when('/operator/reverseMRBack',{
    	templateUrl: 'reverseMRBack',
        controller: 'ReverseMRBackCntlr'
    }).when('/operator/dirPayMRBack',{
    	templateUrl: 'dirPayMRBack',
        controller: 'DirPayMRBackCntlr'
    }).when('/operator/addGST',{
    	templateUrl: 'addGST',
        controller: 'AddGSTCntlr'
    }).when('/operator/manualBill',{
    	templateUrl: 'manualBill',
        controller: 'ManualBillCntlr'
    }).when('/operator/gstBill',{
    	templateUrl: 'gstBill',
        controller: 'GstBillCntlr'
    }).when('/operator/gstManualBill',{
    	templateUrl: 'gstManualBill',
        controller: 'GstManualBillCntlr'
    }).when('/operator/gstRelBill',{
    	templateUrl: 'gstRelBill',
        controller: 'GstRelBillCntlr'
    }).when('/operator/reIssue',{
    	templateUrl: 'reIssue',
        controller: 'ReIssueCntlr'
    }).when('/admin/chlnMisReport',{
    	templateUrl: 'chlnMisReport',
        controller: 'CHLNMisReportCntlr'
    }).when('/admin/sedrMisReport',{
    	templateUrl: 'sedrMisReport',
        controller: 'SEDRMisReportCntlr'
    }).when('/operator/viewRelCNStatus',{
    	templateUrl: 'viewRelCNStatus',
        controller: 'RelianceCNStatusCntlr'
    }).when('/operator/relCNStatus',{
    	templateUrl: 'relCNStatus',
        controller: 'RelianceCNStatusCntlr'
    }).when('/operator/allowAr',{
    	templateUrl: 'allowAr',
        controller: 'AllowArCntlr'
    }).when('/operator/relOs',{
    	templateUrl: 'relOs',
        controller: 'RelOsCntlr'
    }).when('/operator/editBillGST',{
    	templateUrl: 'editBillGST',
    	controller: 'EditBillGSTCntlr'
    }).when('/operator/lhpvBalPay',{
    	templateUrl: 'lhpvBalPay',
        controller: 'LhpvBalPayCntlr'
    }).when('/operator/lhpvBalPayHo',{
    	templateUrl: 'lhpvBalPayHo',
        controller: 'LhpvBalPayHOCntlr'
    }).when('/operator/ackValidator',{
    	templateUrl: 'ackValidator',
        controller: 'AckValidatorCntlr'
    }).when('/operator/newRelianceStock',{//
    	templateUrl: 'newRelianceStock',
    	controller: 'RelianceStockNewCntlr'
    }).when('/operator/payLhpvBalance',{//
    	templateUrl: 'payLhpvBalance',
    	controller: 'PayLhpvBalanceCntlr'
    }).when('/operator/relianceExcel',{//
    	templateUrl: 'relianceExcel',
    	controller: 'RelianceExcelCntlr'
    }).when('/operator/bankStmtUpdate',{
    	templateUrl:'bankStmtUpdate',
    	controller:'BankStmtUpdateCntlr'
    }).when('/operator/fundAllocation',{
    	templateUrl:'fundAllocation',
    	controller:'FundAllocationCntlr'
    }).when('/operator/generatedFundAllocation',{
    	templateUrl:'generatedFundAllocation',
    	controller:'FundAllocationCntlr'
    }).when('/operator/petroCard',{
    	templateUrl:'petroCard',
    	controller:'PetroCardCntlr'
    }).when('/operator/petroCardExcel',{
    	templateUrl:'petroCardExcel',
    	controller:'PetroCardCntlr'
    }).when('/operator/newRentDetail',{
    	templateUrl:'newRentDetail',
    	controller:'NewRentDetailCntlr'
    }).when('/operator/rentXlsx',{
    	templateUrl:'rentXlsx',
    	controller:'NewRentDetailCntlr'
    }).when('/operator/payableRpt',{
    	templateUrl: 'payableRpt',
    	controller: 'StockRptCntlr'
    }).when('/operator/removeFrmShrtExcess',{
    	templateUrl:'removeFrmShrtExcess',
    	controller:'ShortExcessRptCntlr'
    }).when('/operator/circular',{
    	templateUrl:'circular',
    	controller:'CircularCntlr'
    }).when('/operator/newLhpvBalCash',{
    	templateUrl: 'newLhpvBalCash',
        controller: 'LhpvBalCashCntlr'
    }).when('/operator/fileUpload',{
    	templateUrl: 'fileUpload',
        controller: 'FileUploadCntlr'
    }).when('/operator/challanEnquiry',{
    	templateUrl: 'challanEnquiry',
        controller: 'ChallanEnquiryCntlr'
    }).when('/operator/verifyAccount',{
    	templateUrl: 'verifyAccount',
        controller: 'VerifyAccountCntlr'
    }).when('/operator/invalidAcc',{
    	templateUrl: 'invalidAcc',
        controller: 'InvalidAccCntlr'
    }).when('/operator/alwLryPymnt',{
    	templateUrl: 'alwLryPymnt',
        controller: 'AlwLryPymntCntlr'
    });
    
       console.log("----app.js ENDED----");
    
});


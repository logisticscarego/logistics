'use strict';

angular.module('application').factory('DataFactoryService',
		[ '$http', '$resource', function($http, $resource) {
			var dataFactory = {};
			
			dataFactory.reset=function(){
				dataFactory.isPermissionForOperator=false;
				dataFactory.isPermissionForAdmin=false;
				dataFactory.isPermissionForSuperAdmin=false;
				dataFactory.isLogout = true;
				dataFactory.username='';
				dataFactory.roles=[];
			}
			
			dataFactory.operator=function(){
				dataFactory.isPermissionForOperator=true;
			}
			
			dataFactory.admin=function(){
				dataFactory.isPermissionForAdmin=true;
			}
			
			dataFactory.superAdmin=function(){
				dataFactory.isPermissionForSuperAdmin=true;
			}
			
			dataFactory.logout=function(){
				dataFactory.isLogout=false;
			}
			/*dataFactory.clearTokenHeader = function() {
				dataFactory.reset();
				localStorage.clear();
			}

			dataFactory.saveResult=function(result){
				dataFactory.saveIsPermissionLoaded();
			}	
			
			dataFactory.loadDataFromCache=function(){
				if(localStorage["isPermissionLoaded"]!=null){
					dataFactory.isPermissionLoaded = localStorage["isPermissionLoaded"] ;
				}
				if(localStorage["authToken"]!=null){
					$http.defaults.headers.common['X-Auth-Token'] = localStorage["authToken"];
				}
				if(localStorage["username"]!=null){
					dataFactory.username=localStorage["username"];
				}
				if(localStorage["roles"]!=null){
					dataFactory.roles=JSON.parse(localStorage["roles"]);
				}
			}
			
			dataFactory.saveIsPermissionLoaded=function(){
				dataFactory.isPermissionLoaded=true;
				localStorage["isPermissionLoaded"] = dataFactory.isPermissionLoaded;
			}
			
			dataFactory.getIsPermissionLoaded=function(){
				return dataFactory.isPermissionLoaded;
			}
			
			dataFactory.saveTokenHeader = function(token) {
				$http.defaults.headers.common['X-Auth-Token'] = token;
				localStorage["authToken"] = token;
			}
			
			dataFactory.saveUsername=function(name){
				localStorage["username"] = name;
				dataFactory.username=name;
			}
			
			dataFactory.getUsername=function(){
				return dataFactory.username;
			}
			
			dataFactory.saveUserRole = function(roles) {
				dataFactory.roles = roles;
				localStorage.setItem('roles', JSON.stringify(roles));
			}
			
			dataFactory.getUserRole=function(){
				return dataFactory.roles;
			}*/
			dataFactory.reset();
			//dataFactory.loadDataFromCache();	
			
			return dataFactory;
		} ]);
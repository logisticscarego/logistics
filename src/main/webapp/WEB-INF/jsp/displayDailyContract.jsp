<div ng-show="adminLogin || superAdminLogin">
<title>Display Contract</title>
		<div class="container">
		<div class="row">
			<div class="col s2"><input type="radio" name="dlyContList" ng-model="dlyContByModal" ng-click="enableModalTextBox()"></div>
			<div class="col s5"><input type = "text" id="dlyContByMId" name="dlyContByM" ng-model="dlyContByM" disabled="disabled" ng-click="OpenDlyContCodeDB()" readonly></div>	
			<div class="col s5"><input type="text" id="oldDlyContId" ng-model="oldDlyCont" ng-click="openOldContractDB()" disabled="disabled" readonly></div>
		</div>
		
		<div class="row">
			<div class="col s2"><input type="radio" name="dlyContList" ng-model="dlyContByAuto" ng-click="enableAutoTextBox()"></div>
			<div class="col s8"><input type = "text" id="dlyContByAId" name="dlyContByA" ng-model="dlyContByA" disabled="disabled" ng-keyup="getContCodeList()"></div>
			<div class="col s2"><input class="col s12 btn" type ="button" id="ok" value = "OK" disabled="disabled" ng-click="getContractList()"></div>
		</div>
		
		<div class="row">
			<div class="col s12"><input type ="button" value = "Back To list" ng-click="backToList()"></div>
		</div>
		</div>
		
		<div id ="dlyContCodeDB" ng-hide = "dlyContCodeFlag">
 	  	 <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search by Contract Code">
 	   	  <table>
		  <tr ng-repeat="dlyContCodes in dlyContCodeList | filter:filterTextbox">
		 	  <td><input type="radio"  name="contCodesName"  value="{{ dlyContCodes}}" ng-model="contCodes" ng-click="saveContCode(dlyContCodes)"></td>
              <td>{{ dlyContCodes}}</td>
          </tr>
    </table>       
	</div>
	
		<div ng-hide = "showContractDetails" class="container">		
			 <form  name="EditDailyContractForm" ng-submit="EditDailyContractSubmit(EditDailyContractForm,dlyCnt,tnc,pbdP,pbdB,pbdD)">
			
			 <div class="row">
			 	<div class="input-field col s3">
        				<input type ="hidden" id="branchCode" name ="branchCode" ng-model="dlyCnt.branchCode" ng-click="OpenBranchCodeDB()" readonly>
        				<!-- <label>Branch Code</label> -->
      				</div>
      				<div class="input-field col s3">
      					<input type ="text" id="dlyContBLPMCode" name ="dlyContBLPMCode" ng-model="dlyCnt.dlyContBLPMCode"  ng-click="OpendlyContBLPMCodeDB()" readonly>
      					<label>BLPM Code</label>
      				</div>
      				<div class="input-field col s3">
        				<input type ="text" id="dlyContCngrCode" name ="dlyContCngrCode" ng-model="dlyCnt.dlyContCngrCode" ng-click="OpendlyContCngrCodeDB()" readonly>
        				<label>Consignor Code</label>
      				</div>
      				<div class="input-field col s3">

        				<input type ="text" id="dlyContCrName" name ="dlyContCrName" ng-model="dlyCnt.dlyContCrName" readonly ng-click="OpendlyContCrNameDB()">
      					<label>CR Name</label>
      				</div>
 				</div>
 				
 				<div class="row">
      				<div class="input-field col s3">
      					<select name="metricType" id="metricType" ng-model="metricType" ng-blur="sendMetricType(metricType)">
							<option value="Ton">Ton</option>
							<option value="Kg">Kg</option>
						</select>
							<label>Metric Type</label>
					</div>
					
					<div class="input-field col s3">
        				<input id="dlyContStartDt" type="date" name ="dlyContStartDt" ng-model="dlyCnt.dlyContStartDt">
        				<label>Start Date</label>
      				</div>	
      				<div class="input-field col s3">
      					<input id="dlyContEndDt" type="date" name ="dlyContEndDt" ng-model="dlyCnt.dlyContEndDt">
      					<label>End Date</label>
      				</div>
 				</div>
 				
 			<!-- 	<div class="row">
      		<div class="col s4">
      						<div class="input-field col s6">	
        						<input type="radio" id="dlyContProportionate" name ="dlyContProportionate" value="P" ng-model="dlyCnt.dlyContProportionate" ng-click="clickP()">
								<label>Proportionate</label>
							</div>
							<div class="input-field col s6">
								<input type ="radio" id="dlyContProportionate1" name ="dlyContProportionate" value="F" ng-model="dlyCnt.dlyContProportionate" ng-click="clickF()">
        						<label>Fixed</label>
        					</div>
      				</div>      				
      				<div class="input-field col s4">
        				<input type ="number" id="dlyContAdditionalRate" name ="dlyContAdditionalRate" ng-model="dlyCnt.dlyContAdditionalRate"  min="1" disabled="disabled" ng-blur="setFflagTrue(dlyCnt)">
        				<label>Additional Rate</label>
      				</div>
      				
      			</div> -->
      			<div class="row">
      					<!-- 	<div class="col s3">	
      					<div class=" input-field col s3"> 
      						<input type="radio" id="dlyContType" name ="dlyContType" ng-model="dlyCnt.dlyContType"  value="W"  ng-click="clickW(metricType,dlyCnt.dlyContProportionate)">
      						<label>W</label>
      					</div>
      					<div class="input-field col s3">
							<input  type ="radio" id="dlyContType1" name ="dlyContType" ng-model="dlyCnt.dlyContType"  value="Q" ng-click="clickQ(metricType,dlyCnt.dlyContProportionate)">
							<label>Q</label>
						</div>
      					<div class="input-field col s3">
							<input type="radio" id="dlyContType2" name ="dlyContType" ng-model="dlyCnt.dlyContType"  value="K"  ng-click="clickK(metricType,dlyCnt.dlyContProportionate)">
      						<label>K</label>
      					</div>
					</div> -->
					
      				<div class="input-field col s3">
        				<input type ="text" id="dlyContFromStation" name ="dlyContFromStation" ng-model="dlyCnt.dlyContFromStation"  ng-click="OpendlyContFromStationDB()" readonly>
        				<label>From Station</label>
      					<input class="col s12 btn teal white-text" type ="button"  id="addStation" name ="addStation" ng-model="addStation" ng-click="openAddNewStnDB()">
      				</div>
      				<div class="input-field col s3" ng-show="onWQClick">
        				
        				<input type ="button" id="ToStations" name ="ToStations" ng-model="ToStations" ng-click="contToStnDB(dlyCnt,metricType)">
        				<label>To Station</label>
      				</div>
      				
      				<div class="input-field col s3" ng-show="onKClick">
        				<input type ="text" id="dlyContToStation" name ="dlyContToStation" ng-model="dlyCnt.dlyContToStation" ng-click="OpendlyContToStationDB()" readonly ng-blur="setToStnFlag(dlyCnt)">
        				<label>To Station</label>
      				</div>
      				
      				<div class="col s3">
						<input type ="button" class="btn col s12 teal white-text" id="dlyContRbkmId" value="Add RBKM"  ng-click="OpendlyContRbkmIdDB(metricType)" disabled="disabled">
					 </div>
					 
					 <!-- <div class="input-field col s3">
        				<input style="width: calc(100% - 65px); position: absolute; left: 10px" type ="text" id="dlyContProductType" name ="dlyContProductType" ng-model="dlyCnt.dlyContProductType" ng-click="OpendlyContProductTypeDB()" disabled="disabled" readonly>
						<label>Product type</label>
						<a style="width: 40px; position: absolute; right: -10px; padding: 0 1rem" class="btn waves-effect teal" id="openProductTypeDB" ng-model="savProducttype" ng-click="OpendlyContProductTypeDB1()" disabled="disabled"><i class="mdi-content-add white-text" style="font-size: 24px; margin-left: -5px;"></i></a>
      				</div> -->
      				
      				
      	<div class="row" ng-show="newCtsFlagW" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in ctsList">
					<td>{{dlyCnt.dlyContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		          	<td>{{cts.ctsRate}}</td>
		            
		            <td><input type ="button" ng-click="removeNewCTS(cts)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		<input type ="button" ng-show="ctsList.length > 1" ng-click="removeAllCTS()" value="Remove All CTS">
		</div>
		</div>
		
		<div class="row" ng-show="ctsFlagW" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in oldCtsList">
					<td>{{dlyCnt.dlyContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		          	<td>{{cts.ctsRate}}</td>
		            
		            <td><input type ="button" ng-click="removeCTS(cts)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
		
		   	<div class="row" ng-show="ctsFlagQ" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Product Type</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in oldCtsList">
					<td>{{dlyCnt.dlyContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		            <td>{{cts.ctsProductType}}</td>
		          	<td>{{cts.ctsRate}}</td>
		            
		            <td><input type ="button" ng-click="removeCTS(cts)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
   		
   		   	<div class="row" ng-show="newCtsFlagQ" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Product Type</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in ctsList">
					<td>{{dlyCnt.dlyContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		            <td>{{cts.ctsProductType}}</td>
		          	<td>{{cts.ctsRate}}</td>
		            
		            <td><input type ="button" ng-click="removeNewCTS(cts)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		<input type ="button" ng-show="ctsList.length > 1" ng-click="removeAllCTS()" value="Remove All CTS">
		</div>
		</div>
      			</div>
      			
      			<div class="row">
      		<div class="input-field col s3">
      						<input class="prefix" type="checkbox" id ="checkLoad" ng-model="checkLoad" ng-change="saveTncLoad()">
      						<input type ="number" id="dlyContLoad" name ="dlyContLoad" ng-model="dlyCnt.dlyContLoad" ng-disabled="!checkLoad"  step="0.01" min="0.01" ng-blur="setLoadFlagTrue(dlyCnt)">
      						<label>Load</label>
      	    				<input type ="hidden" ng-model="tnc.tncLoading" id="tncLoading" ng-init="tnc.tncLoading='no'" >
      	    		</div>
      	    		
      				<div class="input-field col s3">
							<input class="prefix" type="checkbox" id ="checkUnLoad" ng-model="checkUnLoad"  ng-change="saveTncUnload()">
       						<input type ="number" id="dlyContUnLoad" name ="dlyContUnLoad" ng-model="dlyCnt.dlyContUnLoad" ng-disabled="!checkUnLoad"  step="0.01" min="0.01" ng-blur="setUnLoadFlagTrue(dlyCnt)">
							<label>UnLoad</label>
      						<input type ="hidden" ng-model="tnc.tncUnLoading" id="tncUnLoading"  ng-init="tnc.tncUnLoading='no'">
      				</div>
 				</div>
 				
 				<div class="row">
      			
      				<div class="input-field col s3">
							<input class="prefix" type="checkbox" id ="checkTransitDay" ng-model="checkTransitDay" ng-change="saveTncTransitDay()">
      						<input type ="number" id="dlyContTransitDay" name ="dlyContTransitDay" ng-model="dlyCnt.dlyContTransitDay" ng-disabled="!checkTransitDay"  min="1" ng-blur="setTransitFlagTrue(dlyCnt)" >
							<label>Transit Day</label>	
							<input type ="hidden" ng-model="tnc.tncTransitDay" id="tncTransitDay" ng-init="tnc.tncTransitDay='no'" >
        			</div>
        			<div class="input-field col s3">
							<input class="prefix" type="checkbox" id ="checkStatisticalCharge" ng-model="checkStatisticalCharge" ng-change="saveTncStatisticalCharge()">	
      						<input type ="number" id="dlyContStatisticalCharge" name ="dlyContStatisticalCharge" ng-model="dlyCnt.dlyContStatisticalCharge" ng-disabled="!checkStatisticalCharge" step="0.01" min="0.01" ng-blur="setSCFlagTrue(dlyCnt)">
							<label>Statistical Charge</label>
							<input type ="hidden" ng-model="tnc.tncStatisticalCharge" id="tncStatisticalCharge" ng-init="tnc.tncStatisticalCharge='no'" >
      				</div>
      			</div>
      			
	<div class="row"  ng-show="rbkmFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		    	<tr><td><h5>RBKM</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>State Code</th>
		            <th>From Km</th>
		            <th>To Km</th>
		            <th>Vehicle Type</th>
		            <th>Rate</th>
		            <th>Action</th>   
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="rbkm in rbkmList">
		            <td>{{rbkm.rbkmFromStation}}</td>
		            <td>{{rbkm.rbkmToStation}}</td>
		            <td>{{rbkm.rbkmStateCode}}</td>
		            <td>{{rbkm.rbkmFromKm}}</td>
		            <td>{{rbkm.rbkmToKm}}</td>
		            <td>{{rbkm.rbkmVehicleType}}</td>
		            <td>{{rbkm.rbkmRate}}</td>
		            <td><input type ="button" ng-click="removeRbkm(rbkm)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		<!-- <input type ="button" ng-show="rbkmList.length > 1" ng-click="removeAllRbkm()" value="Remove All RBKM"> -->
		</div>
	</div>
	
	
	<div class="row"  ng-show="newRbkmFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		    <tr><td><h5>RBKM</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>State Code</th>
		            <th>From Km</th>
		            <th>To Km</th>
		            <th>Vehicle Type</th>
		            <th>Rate</th>
		            <th>Action</th>   
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="rbkm in rateByKmList">
		            <td>{{rbkm.rbkmFromStation}}</td>
		            <td>{{rbkm.rbkmToStation}}</td>
		            <td>{{rbkm.rbkmStateCode}}</td>
		            <td>{{rbkm.rbkmFromKm}}</td>
		            <td>{{rbkm.rbkmToKm}}</td>
		            <td>{{rbkm.rbkmVehicleType}}</td>
		            <td>{{rbkm.rbkmRate}}</td>
		            <td><input type ="button" ng-click="removeNewRbkm(rbkm)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		<input type ="button" ng-show="rateByKmList.length > 1" ng-click="removeAllRbkm()" value="Remove All RBKM">
		</div>
	</div>
	
	
				<div class="row">
					<div class="input-field col s4">
        				<select name="dlyContDc" id="dlyContDc" ng-model="dlyCnt.dlyContDc" ng-init="dlyCnt.dlyContDc" >
							<option value="01">One bill</option>
							<option value="02">Two bill</option>
							<option value="10">Direct Payment through CNMT</option>
							<option value="20">Twice Payment</option>
							<option value="11">Partially through bill Partially through CNMT</option>
						</select>
						<label>DC</label>
      				</div>
					
 					<div class="input-field col s4">
      					<select name="dlyContCostGrade" id="dlyContCostGrade" ng-model="dlyCnt.dlyContCostGrade" ng-init="dlyCnt.dlyContCostGrade">
							<option value="A">Heavy Material</option>
							<option value="B">Natural Rubber</option>
							<option value="C">General Goods</option>
							<option value="D">Light Goods</option>
						</select>
						<label>Cost Grade</label>
      				</div>

					<div class="input-field col s4">
        				<input class="prefix" type="checkbox" id ="checkDetention" ng-model="checkDetention" ng-change="saveTncDetention()">
						<input class="btn teal white-text" type ="button" id="dlyContDetention" value="Add Detention" ng-disabled="!checkDetention" ng-click="OpenDetentionDB()">
						<input type ="hidden" ng-model="tnc.tncDetention" id="tncDetention" ng-init="tnc.tncDetention='no'">
      				</div>
	   				
				
				</div>
 				
 				<div class="row">
      				<div class="input-field col s4">
      					<select name="dlyContHourOrDay" id="dlyContHourOrDay" ng-model="dlyCnt.dlyContHourOrDay" ng-init="dlyCnt.dlyContHourOrDay">
							<option value="Hour">Hour</option>
							<option value="Day">Day</option>
						</select>
      					<label>Hour / Day</label>
      				</div>
      				<div class="input-field col s4">
        				<select name="dlyContDdl" id="dlyContDdl" ng-model="dlyCnt.dlyContDdl" ng-init="dlyCnt.dlyContDdl">
							<option value="Yes">Yes</option>
							<option value="No">No</option>
						</select>
						<label>DDL</label>
      				</div>
      				<div class="input-field col s4">
						<input class="prefix" type="checkbox" id ="checkPenalty" ng-model="checkPenalty" ng-change="saveTncPenalty()">
						<input class="btn teal white-text" type ="button" value="Add Penalty" id="dlyContPenalty"  ng-disabled="!checkPenalty" ng-click="OpenPenaltyDB()">
			    		<input type ="hidden" ng-model="tnc.tncPenalty" id="tncPenalty" ng-init="tnc.tncPenalty='no'">
      				</div>
      				
 				</div>
 				<div class="row">
      				<!-- <div class="col s4">
      						<div class="input-field col s6">	
        						<input type="radio" id="dlyContProportionate" name ="dlyContProportionate" value="P" ng-model="dlyCnt.dlyContProportionate" ng-click="clickP()">
								<label>Proportionate</label>
							</div>
							<div class="input-field col s6">
								<input type ="radio" id="dlyContProportionate1" name ="dlyContProportionate" value="F" ng-model="dlyCnt.dlyContProportionate" ng-click="clickF()">
        						<label>Fixed</label>
        					</div>
      				</div>    -->   				
      				<!-- <div class="input-field col s4">
        				<input type ="number" id="dlyContAdditionalRate" name ="dlyContAdditionalRate" ng-model="dlyCnt.dlyContAdditionalRate" value="{{dlyCnt.dlyContAdditionalRate}}"  min="1" disabled="disabled" ng-blur="setFflagTrue(dlyCnt)">
        				<label>Additional Rate</label>
      				</div> -->
      				<div class="input-field col s4">
        				<input class="prefix" type="checkbox" id ="checkBonus" ng-model="checkBonus" ng-change="saveTncBonus()">
        				<input class="btn teal white-text" type ="button" id="dlyContBonus" value="Add Bonus" ng-disabled="!checkBonus" ng-click="OpenBonusDB()">
        				<input type ="hidden" ng-model="tnc.tncBonus" id="tncBonus" ng-init="tnc.tncBonus='no'">
      				</div>
      				
      			</div>	
	<div class="row" ng-show="pbdFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		    	<tr><td><h5>PBD</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Start Date</th>
		            <th>End Date</th>
		            <th>From Dlay</th>
		            <th>To Dlay</th>
		            <th>Vehicle Type</th>
		            <th>P/B/D</th>
		            <th>Hour N Day</th>
		            <th>Amount</th>
		            <th>Action</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="pbd in pbdList">
		            
		            <td>{{pbd.pbdFromStn}}</td>
		            <td>{{pbd.pbdToStn}}</td>
		            <td>{{pbd.pbdStartDt}}</td>
		            <td>{{pbd.pbdEndDt}}</td>
		            <td>{{pbd.pbdFromDay}}</td>
		            <td>{{pbd.pbdToDay}}</td>
		            <td>{{pbd.pbdVehicleType}}</td>
		            <td>{{pbd.pbdPenBonDet}}</td>
		            <td>{{pbd.pbdHourNDay}}</td>
		            <td>{{pbd.pbdAmt}}</td>
		            <td><input type ="button" ng-click="removePbd(pbd)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		<!-- <input type ="button" ng-show="pbdList.length > 1" ng-click="removeAllPbd()" value="Remove All PBD"> -->
		</div>
		</div>
		
		
			<div class="row" ng-show="newPbdFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		    	<tr><td><h5>PBD</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Start Date</th>
		            <th>End Date</th>
		            <th>From Dlay</th>
		            <th>To Dlay</th>
		            <th>Vehicle Type</th>
		            <th>P/B/D</th>
		            <th>Hour N Day</th>
		            <th>Amount</th>
		            <th>Action</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="pbd in newPbdList">
		            
		            <td>{{pbd.pbdFromStn}}</td>
		            <td>{{pbd.pbdToStn}}</td>
		            <td>{{pbd.pbdStartDt}}</td>
		            <td>{{pbd.pbdEndDt}}</td>
		            <td>{{pbd.pbdFromDay}}</td>
		            <td>{{pbd.pbdToDay}}</td>
		            <td>{{pbd.pbdVehicleType}}</td>
		            <td>{{pbd.pbdPenBonDet}}</td>
		            <td>{{pbd.pbdHourNDay}}</td>
		            <td>{{pbd.pbdAmt}}</td>
		            <td><input type ="button" ng-click="removeNewPbd(pbd)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		<input type ="button" ng-show="newPbdList.length > 1" ng-click="removeAllPbd()" value="Remove All PBD">
		</div>
		</div>
		

 				<div class="row">
      				<div class="input-field col s12">
        				<i class="mdi-editor-mode-edit prefix"></i>
        				<textarea id="textarea2" class="materialize-textarea"  rows="3" cols="92"  name ="dlyContRemark" ng-model="dlyCnt.dlyContRemark" value="{{dlyCnt.dlyContRemark}}"></textarea>
        				<label>Remarks</label>
      				</div>
			 		
	            <div class="input-field col s12 m4 l3">
					<div><input type="submit" value="Submit!"></div>
				</div>
	       </div>
	       </form>
		</div>
		
		
		<div id="dlyContRbkmIdDB" ng-hide = "RbkmIdFlag">	
	<form name="RbkmForm" ng-submit="saveRbkmId(RbkmForm,newRbkm)"> 
	
		<table>
			<tr>
				<td>From Station *</td>
				<td><input type ="text" id="rbkmFromStationTemp" name ="rbkmFromStationTemp" ng-model="newRbkm.rbkmFromStationTemp"  ng-click="OpenrbkmFromStationDB()"  readonly></td>
				<td><input type ="hidden" id="rbkmFromStation" name ="rbkmFromStation" ng-model="newRbkm.rbkmFromStation" ></td>
			</tr>
			
			<tr>
				<td>To Station *</td>
				<td><input type ="text"  id="rbkmToStationTemp" name ="rbkmToStationTemp" ng-model="newRbkm.rbkmToStationTemp"  ng-click="OpenrbkmToStationDB()"  readonly></td>
				<td><input type ="hidden"  id="rbkmToStation" name ="rbkmToStation" ng-model="newRbkm.rbkmToStation" ></td>
			</tr>
			
			<tr>
				<td>Start Date *</td>
				<td><input type ="date"  id="rbkmStartDate" name ="rbkmStartDate" ng-model="newRbkm.rbkmStartDate" required></td>
			</tr>
			
			<tr>
				<td>End Date *</td>
				<td><input type ="date"  id="rbkmEndDate" name ="rbkmEndDate" ng-model="newRbkm.rbkmEndDate" required></td>
			</tr>
			
			<tr>
				<td>State Code *</td>
				<td><input type ="text"  id="rbkmStateCodeTemp" name ="rbkmStateCodeTemp" ng-model="newRbkm.rbkmStateCodeTemp" ng-click="OpenrbkmStateCodeDB()" readonly required></td>
				<td><input type ="hidden"  id="rbkmStateCode" name ="rbkmStateCode" ng-model="newRbkm.rbkmStateCode"></td>
			</tr>
			
			<tr>
				<td>From Kilometer *</td>
				<td><input type ="number"  id="rbkmFromKm" name ="rbkmFromKm" ng-model="newRbkm.rbkmFromKm" ng-minlength="1" ng-maxlength="7" min="1" required></td>
			</tr>
			
			<tr>
				<td>To Kilometer *</td>
				<td><input type ="number"  id="rbkmToKm" name ="rbkmToKm" ng-model="newRbkm.rbkmToKm" ng-minlength="1" ng-maxlength="7" min="1" required></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text"  id="rbkmVehicleType" name ="rbkmVehicleType" ng-model="newRbkm.rbkmVehicleType" ng-click="OpenRbkmVehicleTypeDB()" required readonly></td>
			</tr>
			
			<tr>
				<td>Rbkm Rate *</td>
				<td><input type ="number"  id="rbkmRate" name ="rbkmRate" ng-model="newRbkm.rbkmRate"   step="0.01" min="0.01" required></td>
			</tr>
			
		 	<tr>
				<td colspan="2" class="center"><input type="submit" id="submit3" value="Submit" ></td>
			</tr>
		</table>
	</form>
	</div>	
	
	
	
		<div id="PenaltyDB" ng-hide="PenaltyFlag">
	<form name="PenaltyForm" ng-submit="savePbdForPenalty(PenaltyForm,pbdP)"> 
		<table>
		
			<tr>
				<td>From Station *</td>
				<td><input type ="text" id="pbdFromStnTempP" name ="pbdFromStnTempP" ng-model="pbdP.pbdFromStnTemp" ng-click="OpenPenaltyFromStationDB()" readonly required></td>
				<td><input type ="hidden" id="pbdFromStnP" name ="pbdFromStnP" ng-model="pbdP.pbdFromStn"  ></td>
			</tr>
		
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="pbdToStnTempP" name ="pbdToStnTempP" ng-model="pbdP.pbdToStnTemp" ng-click="OpenPenaltyToStationDB()" readonly required></td>
				<td><input type ="hidden" id="pbdToStnP" name ="pbdToStnP" ng-model="pbdP.pbdToStn"></td>
			
			</tr>
			
			<tr>
				<td>Start Date *</td>
				<td><input type ="date" id="pbdStartDtP" name ="pbdStartDtP" ng-model="pbdP.pbdStartDt" required></td>
			</tr>
			
			
			<tr>
				<td>End Date *</td>
				<td><input type ="date" id="pbdEndDtP" name ="pbdEndDtP" ng-model="pbdP.pbdEndDt" required></td>
			</tr>
			
			<tr>
				<td>From Day*</td>
				<td><input type ="number" id="pbdFromDayP" name ="pbdFromDayP" ng-model="pbdP.pbdFromDay" required  min="1" ></td>
			</tr>
			
			<tr>
				<td>To Day *</td>
				<td><input type ="number" id="pbdToDayP" name ="pbdToDayP" ng-model="pbdP.pbdToDay" required  min="1" ></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="pbdVehicleTypeP" name ="pbdVehicleTypeP" ng-model="pbdP.pbdVehicleType" ng-click="OpenPenaltyVehicleTypeDB()" readonly required></td>
			</tr>
			
			<tr>
				<td> Hour Or Day *</td>
				<td>
				<select name="pbdHourNDayP" id="pbdHourNDayP" ng-model="pbdP.pbdHourNDay" required>
				<option value="Hour">Hour</option>
				<option value="Day">Day</option>
				</select></td>
			</tr>
			
			<!-- <tr>
				<td>P/B/D</td>
				<td><input type="text" id="pbdPenBonDetP" name ="pbdPenBonDetP" ng-model="pbdP.pbdPenBonDet"  readonly></td>
			</tr> -->
			
			<tr>
				<td>Amount *</td>
				<td><input type ="number" id="pbdAmtP" name ="pbdAmtP" ng-model="pbdP.pbdAmt" required  step="0.01" min="0.01" ></td>
			</tr>
			
			<tr>
				<td><input type="submit" value="Submit" id="savePenalty"  ></td>
			</tr>
		</table>
		</form>
	</div>	
	
	<div id="BonusDB" ng-hide="BonusFlag">
	<form name="BonusForm" ng-submit="savePbdForBonus(BonusForm,pbdB)">
		<table>
		
			<tr>
				<td>From Station *</td>
				<td><input type ="text" id="pbdFromStnTempB" name ="pbdFromStnTempB" ng-model="pbdB.pbdFromStnTemp" ng-click="OpenBonusFromStationDB()" readonly required ></td>
				<td><input type ="hidden" id="pbdFromStnB" name ="pbdFromStnB" ng-model="pbdB.pbdFromStn" ></td>
			</tr>
		
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="pbdToStnTempB" name ="pbdToStnTempB" ng-model="pbdB.pbdToStnTemp" ng-click="OpenBonusToStationDB()" readonly required></td>
				<td><input type ="hidden" id="pbdToStnB" name ="pbdToStnB" ng-model="pbdB.pbdToStn"></td>
			</tr>
			
			<tr>
				<td>Start Date *</td>
				<td><input type ="date" id="pbdStartDtB" name ="pbdStartDtB" ng-model="pbdB.pbdStartDt" required></td>
			</tr>
			
			
			<tr>
				<td>End Date *</td>
				<td><input type ="date" id="pbdEndDtB" name ="pbdEndDtB" ng-model="pbdB.pbdEndDt" required ></td>
			</tr>
			
			<tr>
				<td>From Day *</td>
				<td><input type ="text" id="pbdFromDayB" name ="pbdFromDayB" ng-model="pbdB.pbdFromDay" min="1" required></td>
			</tr>
			
			<tr>
				<td>To Day *</td>
				<td><input type ="text" id="pbdToDayB" name ="pbdToDayB" ng-model="pbdB.pbdToDay" min="1" required></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="pbdVehicleTypeB" name ="pbdVehicleTypeB" ng-model="pbdB.pbdVehicleType" ng-click="OpenBonusVehicleTypeDB()" readonly required></td>
			</tr>
			
			<tr>
				<td> Hour Or Day *</td>
				<td>
				<select name="pbdHourNDayB" id="pbdHourNDayB" ng-model="pbdB.pbdHourNDay" required>
				<option value="Hour">Hour</option>
				<option value="Day">Day</option>
				</select></td>
			</tr>
			
			<!-- <tr>
				<td>P/B/D</td>
				<td><input type="text" id="pbdPenBonDetB" name ="pbdPenBonDetB" ng-model="pbdB.pbdPenBonDet"  readonly></td>
			</tr> -->
			
			<tr>
				<td>Amount *</td>
				<td><input type ="number" id="pbdAmtB" name ="pbdAmtB" ng-model="pbdB.pbdAmt"  step="0.01" min="0.01" required></td>
			</tr>
			
		 	<tr>
				<td colspan="2" class="center"><input type="submit" value="Submit"  id="saveBonus" ></td>
			</tr>
		</table>
		</form>
	</div>	
	
	<div id="DetentionDB" ng-hide="DetentionFlag">
	<form name="DetentionForm"  ng-submit="savePbdForDetention(DetentionForm,pbdD)">
		<table>
		
			<tr>
				<td>From Station *</td>
				<td><input type ="text" id="pbdFromStnTempD" name ="pbdFromStnTempD" ng-model="pbdD.pbdFromStnTemp" ng-click="OpenDetentionFromStationDB()" readonly required></td>
				<td><input type ="hidden" id="pbdToStnD" name ="pbdToStnD" ng-model="pbdD.pbdToStn" ></td>
			</tr>
		
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="pbdToStnTempD" name ="pbdToStnTempD" ng-model="pbdD.pbdToStnTemp" ng-click="OpenDetentionToStationDB()" readonly required></td>
				<td><input type ="hidden" id="pbdToStnD" name ="pbdToStnD" ng-model="pbdD.pbdToStn"></td>
			</tr>
			
			<tr>
				<td>Start Date *</td>
				<td><input type ="date" id="pbdStartDtD" name ="pbdStartDtD" ng-model="pbdD.pbdStartDt" required></td>
			</tr>
			
			
			<tr>
				<td>End Date *</td>
				<td><input type ="date" id="pbdEndDtD" name ="pbdEndDtD" ng-model="pbdD.pbdEndDt" required></td>
			</tr>
			
			<tr>
				<td>From Day *</td>
				<td><input type ="number" id="pbdFromDayD" name ="pbdFromDayD" ng-model="pbdD.pbdFromDay" required min="1"></td>
			</tr>
			
			<tr>
				<td>To Day *</td>
				<td><input type ="number" id="pbdToDayD" name ="pbdToDayD" ng-model="pbdD.pbdToDay" required min="1"></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="pbdVehicleTypeD" name ="pbdVehicleTypeD" ng-model="pbdD.pbdVehicleType" ng-click="OpenDetentionVehicleTypeDB()" readonly required></td>
			</tr>
			
			<tr>
				<td> Hour Or Day *</td>
				<td>
				<select name="pbdHourNDayD" id="pbdHourNDayD" ng-model="pbdD.pbdHourNDay" required>
				<option value="Hour">Hour</option>
				<option value="Day">Day</option>
				</select></td>
			</tr>
			
			<!-- <tr>
				<td>P/B/D</td>
				<td><input type="text" id="pbdPenBonDetD" name ="pbdPenBonDetD" ng-model="pbdD.pbdPenBonDet" readonly> </td>
			</tr> -->
			
			<tr>
				<td>Amount *</td>
				<td><input type ="number" id="pbdAmtD" name ="pbdAmtD" ng-model="pbdD.pbdAmt" required  step="0.01" min="0.01"></td>
			</tr>
			
		 	<tr>
				<td colspan="2" class="center"><input type="submit" value="Submit" id="saveDetention" ></td>
			</tr>
		</table>
		</form>
	</div>
	
	<div id="AddProductTypeDB" ng-hide = "AddProductTypeFlag">
	<form name="ProductTypeDB1Form" ng-submit="saveproducttype(ProductTypeDB1Form,pt)">
		<table>
			<tr>
				<td>Product Name</td>
				<td><input type ="text" id="ptName" name ="ptName" ng-model="pt.ptName" required></td>
			</tr>
			
		 	<tr>
				<td><input type="submit"  value="Submit" ></td>
			</tr>
		</table>
		</form>
	</div>	
		
	<!-- 	<div id="dlyContVehicleTypeDB1" ng-hide = "VehicleTypeFlag1">
	<form name="VehicleForm" ng-submit="savevehicletype(VehicleForm,vt)">
		<table>
			<tr>
				<td>Service Type *</td>
				<td><input type ="text" id="vtServiceType" name ="vtServiceType" ng-model="vt.vtServiceType" ng-minlength="1" ng-maxlength="30" required></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="vtVehicleType" name ="vtVehicleType" ng-model="vt.vtVehicleType"  required></td>
			</tr>
			
			<tr>
				<td>Code *</td>
				<td><input type ="text" id="vtCode" name ="vtCode" ng-model="vt.vtCode"  required></td>
			</tr>
			
			<tr>
				<td>Load Limit *</td>
				<td><input type ="number" id="vtLoadLimit" name ="vtLoadLimit" ng-model="vt.vtLoadLimit"  min ="1"  required></td>
			</tr>
			
			
			<tr>
				<td>Guarantee Weight *</td>
				<td><input type ="number" id="vtGuaranteeWt" name ="vtGuaranteeWt" ng-model="vt.vtGuaranteeWt"  step="0.01" min="0.01" required></td>
			</tr>
			
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>
	 -->
		<div id="addVehicleTypeDB1" ng-hide = "VehicleTypeFlag1">
	<form name="VehicleForm" ng-submit="savevehicletype(VehicleForm,vt)">
		<table>
			<tr>
				<td>Service Type *</td>
				<td><input type ="text" id="vtServiceType" name ="vtServiceType" ng-model="vt.vtServiceType" ng-minlength="1" ng-maxlength="30" required></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="vtVehicleType" name ="vtVehicleType" ng-model="vt.vtVehicleType"  required></td>
			</tr>
			
			<tr>
				<td>Code *</td>
				<td><input type ="text" id="vtCode" name ="vtCode" ng-model="vt.vtCode"  required></td>
			</tr>
			
			<tr>
				<td>Load Limit *</td>
				<td><input type ="number" id="vtLoadLimit" name ="vtLoadLimit" ng-model="vt.vtLoadLimit"  min ="1"  required></td>
			</tr>
			
			
			<tr>
				<td>Guarantee Weight *</td>
				<td><input type ="number" id="vtGuaranteeWt" name ="vtGuaranteeWt" ng-model="vt.vtGuaranteeWt"  step="0.01" min="0.01" required></td>
			</tr>
			
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>
	
	<div id="contToStationW" ng-hide = "contToStationFlagW">
	<form name="ContToStationFormW" ng-submit="saveContToStnW(ContToStationFormW,cts)">
		<table>
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="ctsToStnTemp" name ="ctsToStnTemp" ng-model="cts.ctsToStnTemp" ng-click="openToStationsDB()" readonly required></td>
				<td><input type ="hidden" id="ctsToStn" name ="ctsToStn" ng-model="cts.ctsToStn"></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="ctsVehicleType" name ="ctsVehicleType" ng-model="cts.ctsVehicleType"  required ng-click="openCtsVehicleType()" readonly></td>
				<td><a class="btn-floating suffix waves-effect teal" type="button" id="openVehicleTypeDB" ng-model="savVehicletype" ng-click="OpenAddVehicleTypeDB()" ><i class="mdi-content-add white-text"></i></a></td>
			</tr>
			
			<tr>
				<!-- <td>Product Type*</td> -->
				<td><input type ="hidden" id="ctsProductType" name ="ctsProductType" ng-model="cts.ctsProductType"  readonly></td>
			</tr> 
			
			<tr>
				<td>From Weight *</td>
				<td><input type ="text" id="ctsFromWt" name ="ctsFromWt" ng-model="cts.ctsFromWt"  required></td>
			</tr>
			
			<tr>
				<td>To Weight *</td>
				<td><input type ="text" id="ctsToWt" name ="ctsToWt" ng-model="cts.ctsToWt"  required></td>
			</tr>
			
			<tr>
				<td>Rate *</td>
				<td><input type ="text" id="ctsRate" name ="ctsRate" ng-model="cts.ctsRate"  required></td>
			</tr>
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>
	
	
	<div id="contToStationQ" ng-hide = "contToStationFlagQ">
	<form name="ContToStationFormQ" ng-submit="saveContToStnQ(ContToStationFormQ,cts)">
		<table>
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="ctsToStnTemp" name ="ctsToStnTemp" ng-model="cts.ctsToStnTemp" ng-click="openToStationsDB()" readonly required></td>
				<td><input type ="hidden" id="ctsToStn" name ="ctsToStn" ng-model="cts.ctsToStn"></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="ctsVehicleType" name ="ctsVehicleType" ng-model="cts.ctsVehicleType"  required ng-click="openCtsVehicleType()" readonly></td>
				<td><a class="btn-floating suffix waves-effect teal" type="button" id="openVehicleTypeDB" ng-model="savVehicletype" ng-click="OpenAddVehicleTypeDB()" ><i class="mdi-content-add white-text"></i></a></td>
			</tr>
			
			<tr>
				<td>Product Type*</td>
				<td><input type ="text" id="ctsProductType" name ="ctsProductType" ng-model="cts.ctsProductType" ng-click="openProductTypeDB()" readonly required></td>
				<td><a style="width: 40px; position: absolute; right: -10px; padding: 0 1rem" class="btn waves-effect teal" id="openProductTypeDB" ng-model="savProducttype" ng-click="OpenProductTypeDB1()" ><i class="mdi-content-add white-text" style="font-size: 24px; margin-left: -5px;"></i></a></td>
			</tr>
			
			<tr>
				<td>From Weight *</td>
				<td><input type ="number" id="ctsFromWtQ" name ="ctsFromWt" ng-model="cts.ctsFromWt"  required></td>
			</tr>
			
			<tr>
				<td>To Weight *</td>
				<td><input type ="number" id="ctsToWtQ" name ="ctsToWt" ng-model="cts.ctsToWt"  required></td>
			</tr>
			
			<tr>
				<td>Rate *</td>
				<td><input type ="text" id="ctsRateQ" name ="ctsRate" ng-model="cts.ctsRate"  required></td>
			</tr>
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>
	
		<div id="addStation" ng-hide="addStationFlag">
	<form name="StationForm" ng-submit="saveNewStation(StationForm,station)">
		<table>
			<tr>
				<td>Station Name*</td>
				<td><input id="stnName" type ="text" name ="stnName" ng-model="station.stnName" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
			</tr>
			
			<tr>
				<td>Station District*</td>
				<td><input class="validate" id="district" type ="text" name ="stnDistrict" ng-model="station.stnDistrict" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
			</tr>
			
			<tr>
				<td>State Code *</td>
				<td><input class="validate" id="code" type ="text" name ="stateCode"  ng-model="station.stateCode" readonly ng-click="openStateCodeDB()" required></td>
			</tr>
			<tr>
				<td>Pin</td>
				<td><input class="validate"  type ="text" id="stnPin" name ="stnPin" ng-model="station.stnPin" ng-minlength="6" ng-maxlength="6" ng-required="true" ></td>
			</tr>
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>		
		
	<div ng-hide="isViewNo" class="container">
	 <input type="text" name="filterContractCode" ng-model="filterContractCode.dlyContCode" placeholder="Search by Contract Code">		
 	  <table class="table-hover table-bordered table-condensed">
 	 		<tr>
				<th></th>
				
				<th>Contract Code</th>
				
				<th>Customer Code</th>
				
				 <th>User Code</th>
				
				<th>Time Stamp</th>
			</tr>
 	  
		  
		  <tr ng-repeat="dc in ContractList | filter:filterContractCode">
		 	  <td><input id="{{ dc.dlyContCode }}" type="checkbox" value="{{ dc.dlyContCode }}"  ng-checked="selection.indexOf(dc.dlyContCode) > -1" ng-click="toggleSelection(dc.dlyContCode)" /></td>
			      <td>{{ dc.dlyContCode }}</td>
	              <td>{{ dc.dlyContBLPMCode }}</td>
	              <td>{{ dc.userCode }}</td>
	              <td>{{ dc.creationTS }}</td>
          </tr>
           </table>
		<table>
         	 <tr>
				<td class="center"><input type="button" value="Submit" ng-click="verifyContracts()"></td>
			</tr>
		</table>	
        </div>
		
		<div id ="branchCodeDB" ng-hide = "branchCodeFlag">
	
	<input type="text" name="filterBranch" ng-model="filterBranch.branchCode" placeholder="Search by Branch Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Branch Code</th>
				
				<th>Branch Name</th>
			
				<th>Branch Pin</th>
			</tr>
 	 
		  
		  <tr ng-repeat="branch in branchList  | filter:filterBranch">
		 	  <td><input type="radio"  name="branchCode" class="branchCode"  value="{{ branch}}" ng-model="branchCode" ng-click="savBranchCode(branch)"></td>
              <td>{{ branch.branchCode }}</td>
              <td>{{ branch.branchName }}</td>
              <td>{{ branch.branchPin }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="dlyContBLPMCodeDB" ng-hide = "blpmCodeFlag">
 	  
 	    <input type="text" name="filterBlpm" ng-model="filterBlpm.custCode" placeholder="Search by Customer Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Customer Code</th>
				
				<th>Customer Name</th>
			
				<th>Customer Pin</th>
				
				<th>Daily Contract Allowed</th>
			</tr>
 	  
		  
		  <tr ng-repeat="customer in customerList | filter:filterBlpm">
		 	  <td><input type="radio"  name="customerName" class="custCode"  value="{{ customer }}" ng-model="custCode" ng-click="savBLPMCode(customer)"></td>
              <td>{{ customer.custCode }}</td>
              <td>{{ customer.custName }}</td>
              <td>{{ customer.custPin }}</td>
              <td>{{ customer.custIsDailyContAllow }}</td>
          </tr>
      </table>   
	</div>
		
	
	<div id ="dlyContCngrCodeDB" ng-hide="CngrCodeFlag">
	
	<input type="text" name="filterCngr" ng-model="filterCngr.custCode" placeholder="Search by Customer Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Customer Code</th>
				
				<th>Customer Name</th>
			
				<th>Customer Pin</th>
				
				<th>Daily Contract Allowed</th>
			</tr>
 	  
		  
		  <tr ng-repeat="cngrCode in customerList  | filter:filterCngr">
		 	  <td><input type="radio"  name="cngrCodeName"  class="custCode"  value="{{ cngrCode }}" ng-model="custCode1" ng-click="savCngrCode(cngrCode)"></td>
              <td>{{ cngrCode.custCode }}</td>
              <td>{{ cngrCode.custName }}</td>
              <td>{{ cngrCode.custPin }}</td>
               <td>{{cngrCode.custIsDailyContAllow }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="dlyContCrNameDB" ng-hide = "CrNameFlag">
	
	<input type="text" name="filterCr" ng-model="filterCr.custRepCode" placeholder="Search by CR Code">
 	  
 	   	  <table>
 	 		<tr>
 	 			<th></th>
 	 			
 	 			<th>CR Code</th>
 	 			
				<th>Customer Name</th>
				
				<th>CR Name</th>
			
				<th>CR Designation</th>
			</tr>
 	 
		  
		  <tr ng-repeat="cr in list  | filter:filterCr">
		 	  <td><input type="radio"  name="crName"  class="custRepCode"  value="{{cr }}" ng-model="custRepCode" ng-click="savCrName(cr)"></td>
              <td>{{ cr.custRepCode }}</td>
              <td>{{ cr.custName }}</td>
              <td>{{ cr.custRepName }}</td>
              <td>{{ cr.custRepDesig }}</td>
          </tr>
         </table>
	</div>
	
	<div id ="dlyContFromStationDB" ng-hide = "FromStationFlag">
	
	<input type="text" name="filterFromStn" ng-model="filterFromStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	  
		  <tr ng-repeat="fromStation in stationList  | filter:filterFromStn">
		 	  <td><input type="radio"  name="fromStationName"  class="stnName"  value="{{ fromStation }}" ng-model="stnName"  ng-click="savFrmStnCode(fromStation)"></td>
              <td>{{ fromStation.stnCode }}</td>
              <td>{{ fromStation.stnName }}</td>
              <td>{{ fromStation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="dlyContToStationDB" ng-hide = "ToStationFlag">
	
	<input type="text" name="filterToStn" ng-model="filterToStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	  
		  
		  <tr ng-repeat="toStation in stationList  | filter:filterToStn">
		 	  <td><input type="radio"  name="toStationName"  class="stnName"  value="{{toStation}}" ng-model="stnName1" ng-click="savToStnCode(toStation)"></td>
              <td>{{ toStation.stnCode }}</td>
              <td>{{ toStation.stnName }}</td>
              <td>{{ toStation.stnDistrict }}</td>
          </tr>
         </table>
	</div>
	
	<div id ="productTypeDB" ng-hide = "ProductTypeFlag"> 
	
	<input type="text" name="filterProduct" ng-model="filterProduct" placeholder="Search by Product Name">
 	   	<table>
 	 		<tr>
				<th></th>
				<th>Product Name</th>
			</tr>
 	  
		  
		  <tr ng-repeat="pt in ptList  | filter:filterProduct">
		 	  <td><input type="radio"  name="ptName" class="ptName"  value="{{ pt }}" ng-model="ptName" ng-click="savProductName(pt)"></td>
              <td>{{ pt }}</td>
          </tr>
         </table>
   </div>
   
  <div id ="ctsVehicleTypeDB" ng-hide = "VehicleTypeFlag">
	
	<input type="text" name="filterVehicleType" ng-model="filterVehicleType.vtCode" placeholder="Search by Vehicle Type Code">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
				
				<th>Service Type</th>
			</tr>
 	  
		  
		  <tr ng-repeat="VT in vtList | filter:filterVehicleType">
		 	  <td><input type="radio"  name="VTName" class="vtCode"  value="{{ VT }}" ng-model="vtCode" ng-click="savVehicleType(VT)"></td>
              <td>{{ VT.vtCode }}</td>
              <td>{{ VT.vtVehicleType }}</td>
              <td>{{VT.vtServiceType}}</td>
          </tr>
         </table>       
	</div>
	
	<div id ="rbkmFromStationDB" ng-hide = "rbkmFromStationFlag">
 	  
 	  <input type="text" name="filterRbkmFStn" ng-model="filterRbkmFStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th> 
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="rbkmFStation in stationList  | filter:filterRbkmFStn">
		 	  <td><input type="radio"  name="rbkmFStationName"  class="station"  value="{{rbkmFStation}}" ng-model="stnCode2" ng-click="savRbkmFromStnCode(rbkmFStation)"></td>
              	<td>{{ rbkmFStation.stnCode }}</td>
              	<td>{{ rbkmFStation.stnName }}</td>
              	<td>{{ rbkmFStation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="rbkmToStationDB" ng-hide = "rbkmToStationFlag">
	
	<input type="text" name="filterRbkmTStn" ng-model="filterRbkmTStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="rbkmTStation in stationList  | filter:filterRbkmTStn">
		 	  <td><input type="radio"  name="rbkmTStationName" value="{{rbkmTStation}}" ng-model="stnCode3" ng-click="saveRbkmToStnCode(rbkmTStation)"></td>
          		<td>{{ rbkmTStation.stnCode }}</td>
              	<td>{{ rbkmTStation.stnName }}</td>
              	<td>{{ rbkmTStation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="rbkmVehicleTypeDB" ng-hide = "rbkmVehicleTypeFlag">
	
	<input type="text" name="filterRbkmVehicle" ng-model="filterRbkmVehicle.vtCode" placeholder="Search by Vehicle Type Code">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
				
				<th>Service Type</th>
			
				
			</tr>
 	  
		  
		 <tr ng-repeat="rbkmVType in vtList  | filter:filterRbkmVehicle">
		 	  <td><input type="radio"  name="rbkmVTypeName"  value="{{rbkmVType}}" ng-model="vtCode" ng-click="savRbkmVehicleType(rbkmVType)"></td>
              <td>{{ rbkmVType.vtCode }}</td>
              <td>{{ rbkmVType.vtVehicleType }}</td>
              <td>{{rbkmVType.vtServiceType}}</td>
          </tr>
         </table>       
	</div>
	
	<div id ="rbkmStateCodeDB" ng-hide = "rbkmStateCodeFlag">
	
	<input type="text" name="filterRbkmState" ng-model="filterRbkmState.stateCode" placeholder="Search by State Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>State Code</th>
				<th>State Name</th>
			</tr>
 	 	<tr ng-repeat="state in listState  | filter:filterRbkmState">
		 	  <td><input type="radio"  name="state"  value="{{ state }}" ng-model="stateCode" ng-click="saveStateCode(state)"></td>
               <td>{{ state.stateCode }}</td>
              <td>{{ state.stateName }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="PenaltyVehicleTypeDB" ng-hide = "PenaltyVehicleTypeFlag">
	
	<input type="text" name="filterPenaltyVehicle" ng-model="filterPenaltyVehicle.vtCode" placeholder="Search by Vehicle Code">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
				
				<th>Service Type</th>
				
			</tr>
 	  
		  
		 <tr ng-repeat="vtPenalty in vtList  | filter:filterPenaltyVehicle">
		 	  <td><input type="radio"  name="vtPenaltyName" class="vtCode"  value="{{vtPenalty}}" ng-model="vtCode" ng-click="savPenaltyVehicleType(vtPenalty)"></td>
              <td>{{ vtPenalty.vtCode }}</td>
              <td>{{ vtPenalty.vtVehicleType }}</td>
              <td>{{vtPenalty.vtServiceType}}</td>
          </tr>
         </table>       
	</div>
	
	<div id ="PenaltyToStationDB" ng-hide = "PenaltyToStationFlag">
	
	<input type="text" name="filterPenaltyTStn" ng-model="filterPenaltyTStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Station Code</th>
				<th>Station Name</th>
				
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="pTostation in stationList  | filter:filterPenaltyTStn">
		 	  <td><input type="radio"  name="pTostationName"   value="{{ pTostation }}" ng-model="stnCode3" ng-click="savPenaltyToStn(pTostation)"></td>
              <td>{{ pTostation.stnCode }}</td>
              <td>{{ pTostation.stnName }}</td>
              <td>{{ pTostation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="PenaltyFromStationDB" ng-hide = "PenaltyFromStationFlag">
	
	<input type="text" name="filterPenaltyFStn" ng-model="filterPenaltyFStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th> 
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="pFromstation in stationList  | filter:filterPenaltyFStn">
		 	  <td><input type="radio"  name="pFromstationName" class="station"  value="{{ pFromstation }}" ng-model="stnCode2" ng-click="savPenaltyFromStn(pFromstation)"></td>
              <td>{{ pFromstation.stnCode }}</td>
              <td>{{ pFromstation.stnName }}</td>
              <td>{{ pFromstation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="BonusFromStationDB" ng-hide = "BonusFromStationFlag">
		
		<input type="text" name="filterBonusFStn" ng-model="filterBonusFStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th> 
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="bFromstation in stationList  | filter:filterBonusFStn">
		 	  <td><input type="radio"  name="bFromstationName" class="station"  value="{{bFromstation }}" ng-model="stnCode2" ng-click="savBonusFromStn(bFromstation)"></td>
              <td>{{ bFromstation.stnCode }}</td>
              <td>{{ bFromstation.stnName }}</td>
              <td>{{ bFromstation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="BonusToStationDB" ng-hide = "BonusToStationFlag">
	
	<input type="text" name="filterBonusTStn" ng-model="filterBonusTStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="bTostation in stationList  | filter:filterBonusTStn">
		 	  <td><input type="radio"  name="bTostationName"   value="{{ bTostation }}" ng-model="stnCode3" ng-click="savBonusToStn(bTostation)"></td>
              <td>{{ bTostation.stnCode }}</td>
              <td>{{ bTostation.stnName }}</td>
              <td>{{ bTostation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="BonusVehicleTypeDB" ng-hide = "BonusVehicleTypeFlag">
	
	<input type="text" name="filterBonusVehicle" ng-model="filterBonusVehicle.vtCode" placeholder="Search by Vehicle Code">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
			
				<th>Service Type</th>
				
			</tr>
 	  
		  
		 <tr ng-repeat="vtBonus in vtList  | filter:filterBonusVehicle">
		 	  <td><input type="radio"  name="vtBonusName" class="vtCode"  value="{{ vtBonus }}" ng-model="vtCode" ng-click="savBonusVehicleType(vtBonus)"></td>
              <td>{{ vtBonus.vtCode }}</td>
              <td>{{ vtBonus.vtVehicleType }}</td>
              <td>{{vtBonus.vtServiceType}}</td>
          </tr>
         </table>       
	</div>
	
			<div id ="DetentionFromStationDB" ng-hide = "DetentionFromStationFlag">
			
			<input type="text" name="filterDetentionFStn" ng-model="filterDetentionFStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th> 
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="dFromstation in stationList  | filter:filterDetentionFStn">
		 	  <td><input type="radio"  name="dFromstationName"  class="station"  value="{{ dFromstation }}" ng-model="stnCode2" ng-click="savDetentionFromStn(dFromstation)"></td>
             <td>{{ dFromstation.stnCode }}</td>
              <td>{{ dFromstation.stnName }}</td>
              <td>{{ dFromstation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="DetentionToStationDB" ng-hide = "DetentionToStationFlag">
	
	<input type="text" name="filterDetentionTStn" ng-model="filterDetentionTStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="dTostation in stationList  | filter:filterDetentionTStn">
		 	  <td><input type="radio"  name="dTostationName"  value="{{ dTostation }}" ng-model="stnCode3" ng-click="savDetentionToStn(dTostation)"></td>
             <td>{{ dTostation.stnCode }}</td>
              <td>{{ dTostation.stnName }}</td>
              <td>{{ dTostation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="DetentionVehicleTypeDB" ng-hide = "DetentionVehicleTypeFlag">
	
	<input type="text" name="filterDetentionVehicle" ng-model="filterDetentionVehicle.vtCode" placeholder="Search by Vehicle Code">
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
			
				<th>Service Type</th>
			</tr>
		 <tr ng-repeat="vtDetention in vtList  | filter:filterDetentionVehicle">
		 	  <td><input type="radio"  name="vtDetentionName" class="vtCode"  value="{{ vtDetention }}" ng-model="vtCode" ng-click="savDetentionVehicleType(vtDetention)"></td>
              <td>{{ vtDetention.vtCode }}</td>
              <td>{{ vtDetention.vtVehicleType }}</td>
              <td>{{vtDetention.vtServiceType}}</td>
          </tr>
         </table>       
	</div>
	
	<div id ="ToStationsDB" ng-hide ="ToStationsDBFlag">
 	  	<input type="text" name="filterToStns" ng-model="filterToStns.stnCode" placeholder="Search by Station Code">
 	  <table>
 	 		<tr>
				<th></th> 
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="toStations in stationList | filter:filterToStns">
		 	  <td><input type="radio"  name="toStations"  class="station"  value="{{ toStations }}" ng-model="toStns" ng-click="saveToStations(toStations)"></td>
             <td>{{ toStations.stnCode }}</td>
              <td>{{ toStations.stnName }}</td>
              <td>{{ toStations.stnDistrict }}</td>
          </tr>
         </table> 
	</div>
	
	<div id ="StateCodeDB" ng-hide="StateCodeFlag">
	<input type="text" name="filterState" ng-model="filterState.stateCode" placeholder="Search by State Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>State Code</th>
			</tr>
 	 	<tr ng-repeat="statecode in listState  | filter:filterState">
		 	  <td><input type="radio"  name="statecode"  value="{{ statecode }}" ng-model="stateCode1" ng-click="savStateCode(statecode)"></td>
               <td>{{ statecode.stateCode }}</td>
          </tr>
         </table> 
	</div>
	
	<div id ="OldDailyContractDB" ng-hide ="OldDailyContractFlag">
	
	<input type="text" name="filterOldDailyContract" ng-model="filterOldDailyContract.creationTS" placeholder="Search by Time">
 	   	  <table>
 	 		<tr>
				<th></th>
			
				<th>Creation Time</th>
			</tr>
		 <tr ng-repeat="oldDC in dlyCntOld  | filter:filterOldDailyContract">
		 	  <td><input type="radio"  name="oldDCName" class="oldDCClass"  value="{{ oldDC }}" ng-model="oldDCTime" ng-click="saveOldDC(oldDC)"></td>
              <td>{{ oldDC.creationTS | date : 'medium' }}</td>
          </tr>
         </table>       
	</div>
	
	
	<div id="dlyContDB" ng-hide = "dlyContFlag">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div> 
	<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
	<table class="table-hover table-bordered table-bordered">
		<tr>
			<td><h3>Daily Contract Details</h3></td>
		</tr>
		<tr>
			<td>Branch Code</td>
			<td>{{dlyCnt.branchCode}}</td>
		</tr>
		<tr>
			<td>BLPM Code</td>
			<td>{{dlyCnt.dlyContBLPMCode}}</td>
		</tr>
		<tr>
			<td>Consignor Code</td>
			<td>{{dlyCnt.dlyContCngrCode}}</td>
		</tr>
		<tr>
			<td>CR Name</td>
			<td>{{dlyCnt.dlyContCrName}}</td>
		</tr>
		<tr>
			<td>From Station</td>
			<td>{{dlyCnt.dlyContFromStation}}</td>
		</tr>
		<tr>
			<td>To Station</td>
			<td>{{dlyCnt.dlyContToStation}}</td>
		</tr>
		<tr>
			<td>Start Date</td>
			<td>{{dlyCnt.dlyContStartDt}}</td>
		</tr>
		<tr>
			<td>End Date</td>
			<td>{{dlyCnt.dlyContEndDt}}</td>
		</tr>
		<tr>
			<td>From Weight</td>
			<td>{{dlyCnt.dlyContFromWt}}</td>
		</tr>
		<tr>
			<td>To Weight</td>
			<td>{{dlyCnt.dlyContToWt}}</td>
		</tr>
		<tr>
			<td>Rate</td>
			<td>{{dlyCnt.dlyContRate}}</td>
		</tr>
		<tr>
			<td>DC</td>
			<td>{{dlyCnt.dlyContDc}}</td>
		</tr>
		<tr>
			<td>Cost Grade</td>
			<td>{{dlyCnt.dlyContCostGrade}}</td>
		</tr>
		<tr>
			<td>Hour/Day</td>
			<td>{{dlyCnt.dlyContHourOrDay}}</td>
		</tr>
		<tr>
			<td>DDL</td>
			<td>{{dlyCnt.dlyContDdl}}</td>
		</tr>
		<tr>
			<td>Additional Rate</td>
			<td>{{dlyCnt.dlyContAdditionalRate}}</td>
		</tr>
		<tr>
			<td>Product Type</td>
			<td>{{dlyCnt.dlyContProductType}}</td>
		</tr>
		<tr>
			<td>Vehicle Type</td>
			<td>{{dlyCnt.dlyContVehicleType}}</td>
		</tr>
		<tr>
			<td>Load</td>
			<td>{{dlyCnt.dlyContLoad}}</td>
		</tr>
		<tr>
			<td>Unload</td>
			<td>{{dlyCnt.dlyContUnLoad}}</td>
		</tr>
		<tr>
			<td>Transit Day</td>
			<td>{{dlyCnt.dlyContTransitDay}}</td>
		</tr>
		<tr>
			<td>Statistical Charge</td>
			<td>{{dlyCnt.dlyContStatisticalCharge}}</td>
		</tr>
		
		<tr>
			<td>Remarks</td>
			<td>{{dlyCnt.dlyContRemark}}</td>
		</tr> 
	</table>

		
		<div class="row"  ng-show="rbkmFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		    <tr><td><h5>RBKM</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>State Code</th>
		            <th>From Km</th>
		            <th>To Km</th>
		            <th>Vehicle Type</th>
		            <th>Rate</th>   
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="rbkm in rbkmList">
		            <td>{{rbkm.rbkmFromStation}}</td>
		            <td>{{rbkm.rbkmToStation}}</td>
		            <td>{{rbkm.rbkmStateCode}}</td>
		            <td>{{rbkm.rbkmFromKm}}</td>
		            <td>{{rbkm.rbkmToKm}}</td>
		            <td>{{rbkm.rbkmVehicleType}}</td>
		            <td>{{rbkm.rbkmRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
	</div>

			<div class="row" ng-show="pbdFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		    <tr><td><h5>PBD</h5></td></tr>
		        <tr>
		            
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Start Date</th>
		            <th>End Date</th>
		            <th>From Dlay</th>
		            <th>To Dlay</th>
		            <th>Vehicle Type</th>
		            <th>P/B/D</th>
		            <th>Hour N Day</th>
		            <th>Amount</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="pbd in pbdList">
		            
		            <td>{{pbd.pbdFromStn}}</td>
		            <td>{{pbd.pbdToStn}}</td>
		            <td>{{pbd.pbdStartDt}}</td>
		            <td>{{pbd.pbdEndDt}}</td>
		            <td>{{pbd.pbdFromDay}}</td>
		            <td>{{pbd.pbdToDay}}</td>
		            <td>{{pbd.pbdVehicleType}}</td>
		            <td>{{pbd.pbdPenBonDet}}</td>
		            <td>{{pbd.pbdHourNDay}}</td>
		            <td>{{pbd.pbdAmt}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
	
	
			<div class="row" ng-show="newPbdFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		    <tr><td><h5>PBD</h5></td></tr>
		        <tr>
		            
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Start Date</th>
		            <th>End Date</th>
		            <th>From Dlay</th>
		            <th>To Dlay</th>
		            <th>Vehicle Type</th>
		            <th>P/B/D</th>
		            <th>Hour N Day</th>
		            <th>Amount</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="pbd in newPbdList">
		            
		            <td>{{pbd.pbdFromStn}}</td>
		            <td>{{pbd.pbdToStn}}</td>
		            <td>{{pbd.pbdStartDt}}</td>
		            <td>{{pbd.pbdEndDt}}</td>
		            <td>{{pbd.pbdFromDay}}</td>
		            <td>{{pbd.pbdToDay}}</td>
		            <td>{{pbd.pbdVehicleType}}</td>
		            <td>{{pbd.pbdPenBonDet}}</td>
		            <td>{{pbd.pbdHourNDay}}</td>
		            <td>{{pbd.pbdAmt}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
		<div class="row"  ng-show="newRbkmFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		    <tr><td><h5>RBKM</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>State Code</th>
		            <th>From Km</th>
		            <th>To Km</th>
		            <th>Vehicle Type</th>
		            <th>Rate</th>   
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="rbkm in rateByKmList">
		            <td>{{rbkm.rbkmFromStation}}</td>
		            <td>{{rbkm.rbkmToStation}}</td>
		            <td>{{rbkm.rbkmStateCode}}</td>
		            <td>{{rbkm.rbkmFromKm}}</td>
		            <td>{{rbkm.rbkmToKm}}</td>
		            <td>{{rbkm.rbkmVehicleType}}</td>
		            <td>{{rbkm.rbkmRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
	</div>
	
	<div class="row" ng-show="newCtsFlagW" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in ctsList">
					<td>{{dlyCnt.dlyContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		          	<td>{{cts.ctsRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
		<div class="row" ng-show="ctsFlagW" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in oldCtsList">
					<td>{{dlyCnt.dlyContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		          	<td>{{cts.ctsRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
		
		   	<div class="row" ng-show="ctsFlagQ" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Product Type</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in oldCtsList">
					<td>{{dlyCnt.dlyContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		            <td>{{cts.ctsProductType}}</td>
		          	<td>{{cts.ctsRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
   		
   		   	<div class="row" ng-show="newCtsFlagQ" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Product Type</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in ctsList">
					<td>{{dlyCnt.dlyContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		            <td>{{cts.ctsProductType}}</td>
		          	<td>{{cts.ctsRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
	
	
		<input type="button" value="Cancel" ng-click="back()">
		<input type="button" value="Save" ng-click="saveContract(dlyCnt,tnc)">
	</div>
	</div>
	
	
			<div class="row" ng-hide = "olddlyCont">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
	<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
		<h4>Here's the details of Daily Contract <span class="teal-text text-lighten-2">{{dailyContract.dlyContCode}}</span>:</h4>
		<table class="table-hover table-bordered table-bordered">
				<tr>
	                <td>Contract Code:</td>
	                <td>{{oldCont.dlyContCode}}</td>
	            </tr>
	            
	             <tr>
	                <td>Branch Code:</td>
	                <td>{{oldCont.branchCode}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract BLPM Code:</td>
	                <td>{{oldCont.dlyContBLPMCode}}</td>
	            </tr>
	           
	            <tr>
	                <td>Consignor Code:</td>
	                <td>{{oldCont.dlyContCngrCode}}</td>
	            </tr>
	            
	            <tr>
	                <td>CR Name:</td>
	                <td>{{oldCont.dlyContCrName}}</td>
	            </tr>
	            
	             <tr>
	                <td>Start Date:</td>
	                <td>{{oldCont.dlyContStartDt}}</td>
	            </tr>
	            
	            <tr>
	                <td>End Date:</td>
	                <td>{{oldCont.dlyContEndDt}}</td>
	            </tr>
	            
	            <tr>
	                <td>From Station:</td>
	                <td>{{oldCont.dlyContFromStation}}</td>
	            </tr>
	            
	            <tr>
	                <td>To Station:</td>
	                <td>{{oldCont.dlyContToStation}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract Rate:</td>
	                <td>{{oldCont.dlyContRate}}</td>
	            </tr>
	            
	             <tr>
	                <td>From Weight:</td>
	                <td>{{oldCont.dlyContFromWt}}</td>
	            </tr>
	            
	            <tr>
	                <td>To Weight:</td>
	                <td>{{oldCont.dlyContToWt}}</td>
	            </tr>
	           
	            <tr>
	                <td>Contract Type:</td>
	                <td>{{oldCont.dlyContType}}</td>
	            </tr>
	           
	            <tr>
	                <td>Product Type:</td>
	                <td>{{oldCont.dlyContProductType}}</td>
	            </tr>
	            
	              <tr>
	                <td>Vehicle Type:</td>
	                <td>{{oldCont.dlyContVehicleType}}</td>
	           	 </tr>
	            
	             <tr>
	                <td>Contract DC:</td>
	                <td>{{oldCont.dlyContDc}}</td>
	            </tr>
	            
	             <tr>
	                <td>Cost Grade:</td>
	                <td>{{oldCont.dlyContCostGrade}}</td>
	            </tr>
	            
	            <tr>
	                <td>Transit Day:</td>
	                <td>{{oldCont.dlyContTransitDay}}</td>
	            </tr>
	            
	            <tr>
	                <td>Statistical Charge:</td>
	                <td>{{oldCont.dlyContStatisticalCharge}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract Load:</td>
	                <td>{{oldCont.dlyContLoad}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract UnLoad:</td>
	                <td>{{oldCont.dlyContUnLoad}}</td>
	            </tr>
	            
	            <tr>
	                <td>Hour Or Day:</td>
	                <td>{{oldCont.dlyContHourOrDay}}</td>
	            </tr>
	           
	            <tr>
	                <td> Proportionate:</td>
	                <td>{{oldCont.dlyContProportionate}}</td>
	            </tr>
	            
	            <tr>
	                <td>Additional Rate:</td>
	                <td>{{oldCont.dlyContAdditionalRate}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract DDL:</td>
	                <td>{{oldCont.dlyContDdl}}</td>
	            </tr>
	         
	            <tr>
	                <td>Remarks:</td>
	                <td>{{oldCont.dlyContRemark}}</td>
	            </tr>
	            
	            <tr>
	                <td>Creation Time</td>
	                <td>{{oldCont.creationTS}}</td>
	            </tr>
	          </table>
		</div>
	</div>
</div>	
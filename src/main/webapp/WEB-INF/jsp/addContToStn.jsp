<div ng-show="adminLogin || superAdminLogin" >
<div class="row">
	<div class="col s3 hide-on-med-and-down"> &nbsp; </div>
	<form class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="contForm" ng-submit="viewContract(contCode,contForm)">
	
			<div class="input-field col s6">
   				<input type="text" id="displayDetails" name="ContCode" ng-model="contCode" ng-click="openCodesDB()" ng-required="true" readonly/>
   				<label>Enter Contract Code </label>
 			</div>
			
		 	<div class="input-field col s6 center">
		 		<input class="btn waves-effect waves-light" type="submit" value="Submit">
		 	</div>
			
	</form>
</div>



	<div id ="contCodeDB" ng-hide="contCodeDBFlag">
		<input type="text" name="filterContCode" ng-model="filterContCode" placeholder="Search....">
	 	  
	 	  <table>
	 	 		<tr>
					<th></th>
					
					<th>Contract Code</th>				
					
				</tr>
	 	 		  
			  <tr ng-repeat="contFa in contFaList | filter:filterContCode">
			 	  <td><input type="radio"  name="contFa" value="{{ contFa }}" ng-model="contFaMod" ng-click="saveContCode(contFa)"></td> 
	              <td>{{ contFa }}</td>
	          </tr>
	         </table>
         
	</div>


<div class="row" ng-show="ctosList.length > 0">
<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
	<h4>Here's the details of Contract <span class="teal-text text-lighten-2">{{ contCode }}</span>:</h4>
		<table class="table-hover table-bordered table-bordered" ng-show="ctosList.length > 0">
				<tr>
	                <td>Contract Code:</td>
	                <td>{{contract.contFaCode}}</td>
	            </tr>
	            
	             <tr>
	                <td>Customer Name:</td>
	                <td>{{contract.custName}}</td>
	            </tr>
	            
	            <tr>
	                <td>Customer Code:</td>
	                <td>{{contract.custFaCode}}</td>
	            </tr>
	            
	             <tr>
	                <td>Branch Name:</td>
	                <td>{{contract.contBranch}}</td>
	            </tr>
	            
	            <tr>
	                <td>From Station:</td>
	                <td>{{contract.contFrStn}}</td>
	            </tr>
	            
	            <tr>
	                <td>From Date:</td>
	                <td>{{contract.contFrDt | date : format : timezone}}</td>
	            </tr>
	            
	            <tr>
	                <td>To Date:</td>
	                <td>{{contract.contToDt | date : format : timezone}}</td>
	            </tr>
	            
	             <tr>
	                <td>Contract Type:</td>
	                <td>{{contract.contType}}</td>
	            </tr>
	            
	             <tr>
	                <td>Contract Value:</td>
	                <td>{{contract.contValue}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract Weight:</td>
	                <td>{{contract.contWeight}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract DDL:</td>
	                <td>{{contract.contDDL}}</td>
	            </tr>
	            
	            <tr>
	                <td>Insaured By:</td>
	                <td>{{contract.contInsBy}}</td>
	            </tr>
	            
	             <tr>
	                <td>Remarks:</td>
	                <td>{{contract.contRemarks}}</td>
	            </tr>
	            
	            <tr>
	                <td>Bill Basis:</td>
	                <td>
	                	<select name="contBB" id="contBBId" ng-model="contract.contBB" ng-init="contract.contBB = 'chargeWt'" ng-change="chgBillBasis(contract.contBB)" required>
							<option value="chargeWt">Charge Weight</option>
							<option value="receiveWt">Receive Weight</option>
							<option value="actualWt">Actual Weight</option>
							<option value="fixed">Fixed</option>
						</select>
	                </td>
	            </tr>
	            
	            <tr>
	                <td>Renew Date:</td>
	                <td>
	                	<input class="validate" type ="date" id="renDtId" name ="renDtName" ng-model="renDt" ng-blur="chkRenDt()">
	                </td>
	            </tr>
	            
	            <!--  <tr>
	                <td>Extend Date:</td>
	                <td>
	                	<input class="validate" type ="radio" id="extndDtId" name ="extndDtName" value="false" ng-model="extndDt" ng-click="chkExtndDt()">
	                </td>
	            </tr> -->

	          </table>
		</div>
	</div>
	
	
<div class="row" ng-show="ctosList.length > 0" style="margin-bottom: -30px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;">	
 		<table class="tblrow mrg" ng-show="ctosList.length > 0">
			<caption class="coltag tblrow">CTS</caption>
		    <thead>
		        <tr class="rowclr">
		            <th class="colclr">From Station</th>
		            <th class="colclr">To Station</th>
		            <th class="colclr">Vehicle Type</th>
		            <th class="colclr">From Weight</th>
		            <th class="colclr">To Weight</th>
		            <th class="colclr">From Date</th>
		            <th class="colclr">To Date</th>
		            <!-- <th ng-show="newDtshow" class="colclr">New Date</th> -->
		            <th class="colclr">Rate</th>
		            <th class="colclr">New Rate</th>
		            <th class="colclr">Action</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr class="tbl" ng-repeat="ctos in ctosList">
					<td class="rowcel">{{ctos.frStn}}</td>
				    <td class="rowcel">{{ctos.toStn}}</td>
				    <td class="rowcel">{{ctos.vehType}}</td>
				    <td class="rowcel">{{ctos.frWt}}</td>
				    <td class="rowcel">
				    	<input type="number" id="toWtId{{$index}}" name="toWtName{{$index}}" ng-model="ctos.toWt" step="0.01" min="0.00" />
				    </td>
		            <td class="rowcel">{{ctos.ctsFrDt | date : format : timezone}}</td>
		            <td class="rowcel">{{ctos.ctsToDt | date : format : timezone}}</td>
		            <!-- <td ng-show="newDtshow" class="rowcel">
		            	<input type="date" id="newDateId{{$index}}" name="newDateId{{$index}}" ng-model="ctos.extendDate"/>	
		            </td> -->
		            <td class="rowcel">{{ctos.rate}}</td>
		            <td class="rowcel">{{newRate[$index]}}
						<input type="number" id="newRateId{{$index}}" name="newRateName{{$index}}" ng-model="newRate" 
						ng-blur="chgCtsRate(ctos.ctsId,newRate)" step="0.01" min="0.00" disabled="disabled"/>		            	
		            </td>
		            <td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="changeToWt(ctos, $index)" >
						<i class="mdi-content-send white-text"></i>
					</a>
				</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		
		
		
		<!-- <div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >
			<table>
				<tr>
					<td>To Station</td>
					<td><input type ="button" id="ToStations" name ="ToStations" ng-model="ToStations" ng-click="contToStnDB()" value="ADD TO STATION"></td>
				</tr>
			</table>
        </div> -->
	   <div class="input-field col s12 center">
		 		<input type ="button" id="ToStations" name ="ToStations" ng-model="ToStations" ng-click="contToStnDB()" value="ADD TO STATION">
	   </div>
</div>


	<div id="contToStationW" ng-hide = "contToStationFlagW">
	<form name="ContToStationFormW" ng-submit="saveContToStnW(ContToStationFormW,cts,regCnt,metricType)">
		<table>
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="ctsVehicleType" name ="ctsVehicleType" ng-model="cts.ctsVehicleType"  required ng-click="openCtsVehicleType()" readonly></td>
			</tr>
			
			<tr ng-show="prodctTypFlag">
				<td>Product Type *</td>
				<td><input type ="text" id="ctsProductType" name ="ctsProductType" ng-model="productType"  required ng-click="openCtsProductType()" readonly></td>
			</tr>
			
			<tr>
				<td>From Weight *</td>
				<td><input type ="number" id="ctsFromWt" name ="ctsFromWt" ng-model="cts.ctsFromWt" step="0.00001" required></td>
			</tr>
			
			<tr>
				<td>To Weight *</td>
				<td><input type ="number" id="ctsToWt" name ="ctsToWt" ng-model="cts.ctsToWt" step="0.00001" required></td>
			</tr>
			
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="ctsToStnTemp" name ="ctsToStnTemp" ng-model="cts.ctsToStnTemp" ng-click="openToStationsDB()" readonly required>
			</tr>
			
			
			
			<!-- <tr>
				<td>Product Type*</td>
				<td><input type ="hidden" id="ctsProductType" name ="ctsProductType" ng-model="cts.ctsProductType" readonly></td>
			</tr> -->
			
		
			<tr>
				<td>Rate *</td>
				<td><input type ="number" id="ctsRate" name ="ctsRate" ng-model="cts.ctsRate" step="0.00001" required></td>
			</tr>
			
			<tr>
				<td>Additional Rate *</td>
				<td><input type ="number" id="ctsAdditionalRate" name ="ctsAdditionalRate" ng-model="cts.ctsAdditionalRate" step="0.00001" required disabled="disabled"></td>
			</tr>
			
			<tr>
				<td> 
				<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkLoad" ng-model="checkLoad" ng-change="saveTncLoad()">
					<input class="validate" type ="number" id="ctsToLoad" name ="ctsToLoad" ng-model="cts.ctsToLoad" ng-disabled="!checkLoad"  step="0.00001" min="0.00001"  ng-blur="setLoadFlagTrue(cts)">				
					<label>Load</label>
					<input type ="hidden" ng-model="tnc.tncLoading" id="tncLoading" ng-init="tnc.tncLoading='no'" >
      	   		 </div>
      	   		 </td>
      	   
      	   		<td>	 
      			<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkUnLoad" ng-model="checkUnLoad" ng-change="saveTncUnload()">
					<input class="validate" type ="number" id="ctsToUnLoad" name ="ctsToUnLoad" ng-model="cts.ctsToUnLoad" ng-disabled="!checkUnLoad"  step="0.00001" min="0.00001"  ng-blur="setUnLoadFlagTrue(cts)">
					<label>UnLoad</label>
					<input type ="hidden" ng-model="tnc.tncUnLoading" id="tncUnLoading"  ng-init="tnc.tncUnLoading='no'">	
      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
				<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkTransitDay" ng-model="checkTransitDay" ng-change="saveTncTransitDay()">
      				<input class="validate" type ="number" id="ctsToTransitDay" name ="ctsToTransitDay" ng-model="cts.ctsToTransitDay" ng-disabled="!checkTransitDay"  min="1" ng-blur="setTransitFlagTrue(cts)" >
					<label for="regContTransitDay">Transit Day</label>
					<input type ="hidden" ng-model="tnc.tncTransitDay" id="tncTransitDay" ng-init="tnc.tncTransitDay='no'" >
        		</div>
        		</td>
        		
      			<td><div class="input-field col s3">	
      				<input class="prefix" type="checkbox" id ="checkStatisticalCharge" ng-model="checkStatisticalCharge" ng-change="saveTncStatisticalCharge()">
      				<input class="validate" type ="number" id="ctsToStatChg" name ="ctsToStatChg" ng-model="cts.ctsToStatChg" ng-disabled="!checkStatisticalCharge"  step="0.00001" min="0.00001"  ng-blur="setSCFlagTrue(cts)">
					<label for="regContStatisticalCharge">Statistical Charge</label>
					<input type ="hidden" ng-model="tnc.tncStatisticalCharge" id="tncStatisticalCharge" ng-init="tnc.tncStatisticalCharge='no'" >
      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
					<div class="input-field col s3">
				   		<input class="prefix" type="checkbox" id ="checkPenalty" ng-model="checkPenalty" ng-change="saveTncPenalty()">
				   		<input class="btn teal white-text nopadding" type ="button" value="Penalty" id="regContPenalty"  ng-disabled="!checkPenalty" ng-click="OpenPenaltyDB()">
				   		<label>Add</label>
				   		<input type ="hidden" ng-model="tnc.tncPenalty" id="tncPenalty" ng-init="tnc.tncPenalty='no'">
	      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
					<div class="input-field col s3">
				    	<input class="prefix" type="checkbox" id ="checkBonus" ng-model="checkBonus" ng-change="saveTncBonus()">
				    	<input class="btn teal white-text nopadding" type ="button" id="regContBonus" value="Bonus" ng-disabled="!checkBonus" ng-click="OpenBonusDB()">
				    	<label>Add</label>
				    	<input type ="hidden" ng-model="tnc.tncBonus" id="tncBonus" ng-init="tnc.tncBonus='no'">
        			</div>
				</td>
			</tr>	
			
			<tr>
				<td>
					<div class="input-field col s3">
						<input class="prefix" type="checkbox" id ="checkDetention" ng-model="checkDetention" ng-change="saveTncDetention()">
						<input class="btn teal white-text nopadding" type ="button" id="regContDetention" value="Detention" ng-disabled="!checkDetention" ng-click="OpenDetentionDB()">
						<label>Add</label>
						<input type ="hidden" ng-model="tnc.tncDetention" id="tncDetention" ng-init="tnc.tncDetention='no'">
      				</div>
				</td>
			</tr>
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>


<div id ="ctsVehicleTypeDB" ng-hide = "VehicleTypeFlag">
	
	<input type="text" name="filterVehicleType" ng-model="filterVehicleType" placeholder="Search by Vehicle Type Code">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
				
				<th>Service Type</th>
			</tr>
 	  
		  
		  <tr ng-repeat="VT in vehList | filter:filterVehicleType">
		 	  <td><input type="radio"  name="VTName" class="vtCode"  value="{{ VT }}" ng-model="vtCode" ng-click="savVehicleType(VT)"></td>
              <td>{{ VT.vtCode }}</td>
              <td>{{ VT.vtVehicleType }}</td>
              <td>{{VT.vtServiceType}}</td>
          </tr>
         </table>       
</div>

<div id ="ctsProductTypeDB" ng-hide = "ProductTypeFlag">
	
	<input type="text" name="filterProductType" ng-model="filterProductType" placeholder="Search by Product Name">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Product Name</th>
			</tr>
 	  
		  
		  <tr ng-repeat="ptName in productTypeList | filter:filterProductType">
		 	  <td><input type="radio"  name="PTName"  ng-click="savProductType(ptName)"></td>
              <td>{{ ptName }}</td>
          </tr>
         </table>       
</div>



<div id ="ToStationsDB" ng-hide ="ToStationsDBFlag">
 	  	<input type="text" name="filterToStns" ng-model="filterToStns" placeholder="Search by Station Code">
 	  <table>
 	 		<tr>
				<th></th> 
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="toStations in stnList | filter:filterToStns">
		 	  <td><input type="radio"  name="toStations"  class="station"  value="{{ toStations }}" ng-model="toStns" ng-click="saveToStations(toStations)"></td>
             <td>{{ toStations.stnCode }}</td>
              <td>{{ toStations.stnName }}</td>
              <td>{{ toStations.stnDistrict }}</td>
          </tr>
         </table>
</div>


<div id="PenaltyDB" ng-hide="PenaltyFlag">
	<form name="PenaltyForm" ng-submit="savePbdForPenalty(PenaltyForm,pbdP)"> 
		<table>
		
			<tr>
				<td>From Station *</td>
				<td><input type ="text" id="pbdFromStnTempP" name ="pbdFromStnTempP" ng-model="pbdP.pbdFromStnTemp" readonly></td>
				<td><input type ="hidden" id="pbdFromStnP" name ="pbdFromStnP" ng-model="pbdP.pbdFromStn"  ></td>
			</tr>
		
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="pbdToStnTempP" name ="pbdToStnTempP" ng-model="pbdP.pbdToStnTemp" readonly required></td>
				<td><input type ="hidden" id="pbdToStnP" name ="pbdToStnP" ng-model="pbdP.pbdToStn"></td>
			
			</tr>
			
			<tr>
				<td>Start Date *</td>
				<td><input type ="date" id="pbdStartDtP" name ="pbdStartDtP" ng-model="pbdP.pbdStartDt" required readonly></td>
			</tr>
			
			<tr>
				<td>End Date *</td>
				<td><input type ="date" id="pbdEndDtP" name ="pbdEndDtP" ng-model="pbdP.pbdEndDt" required readonly></td>
			</tr>
			
			<tr>
				<td>From Day*</td>
				<td><input type ="number" id="pbdFromDayP" name ="pbdFromDayP" ng-model="pbdP.pbdFromDay" required min="1" ng-minlength="1" ng-maxlength="7"></td>
			</tr>
			
			<tr>
				<td>To Day *</td>
				<td><input type ="number" id="pbdToDayP" name ="pbdToDayP" ng-model="pbdP.pbdToDay" required min="1" ng-minlength="1" ng-maxlength="7"></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="pbdVehicleTypeP" name ="pbdVehicleTypeP" ng-model="pbdP.pbdVehicleType" readonly required></td>
			</tr>
			
			<tr>
				<td> Hour Or Day *</td>
				<td>
				<select name="pbdHourNDayP" id="pbdHourNDayP" ng-model="pbdP.pbdHourNDay" required>
				<option value="Hour">Hour</option>
				<option value="Day">Day</option>
				</select></td>
			</tr>
			
			<!-- <tr>
				<td>P/B/D</td>
				<td><input type="text" id="pbdPenBonDetP" name ="pbdPenBonDetP" ng-model="pbdP.pbdPenBonDet"  readonly></td>
			</tr> -->
			
			<tr>
				<td>Amount *</td>
				<td><input type ="number" id="pbdAmtP" name ="pbdAmtP" ng-model="pbdP.pbdAmt" required step="0.01" min="0.01"></td>
			</tr>
			
			<tr>
				<td><input type="submit" value="Submit" id="savePenalty"  ></td>
			</tr>
		</table>
		</form>
	</div>	
	
	
<div class="row"  ng-show="pbdList.length > 0" style="margin-bottom: 50px; margin-top: -100px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >
	<table class="tblrow">
					<caption class="coltag tblrow">P B D</caption>
		    <thead>
		        <tr class="rowclr">
		            <th class="colclr">From Station</th>
		            <th class="colclr">To Station</th>
		            <th class="colclr">Start Date</th>
		            <th class="colclr">End Date</th>
		            <th class="colclr">From Day</th>
		            <th class="colclr">To Day</th>
		            <th class="colclr">Vehicle Type</th>
		            <th class="colclr">P/B/D</th>
		            <th class="colclr">Hour N Day</th>
		            <th class="colclr">Amount</th>
		            <th class="colclr">Action</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr class="tbl" ng-repeat="pbd in pbdList">
		            <td class="rowcel">{{pbdStnList[$index].FromStn}}</td>
		            <td class="rowcel">{{pbdStnList[$index].ToStn}}</td>
		            <td class="rowcel">{{pbd.pbdStartDt}}</td>
		            <td class="rowcel">{{pbd.pbdEndDt}}</td>
		            <td class="rowcel">{{pbd.pbdFromDay}}</td>
		            <td class="rowcel">{{pbd.pbdToDay}}</td>
		            <td class="rowcel">{{pbd.pbdVehicleType}}</td>
		            <td class="rowcel">{{pbd.pbdPenBonDet}}</td>
		            <td class="rowcel">{{pbd.pbdHourNDay}}</td>
		            <td class="rowcel">{{pbd.pbdAmt}}</td>
		            <td class="rowcel"><input type="button" class="btn" ng-click="removePbd(pbd)" value="Remove"></td>
		        </tr>
		    </tbody>
	</table>
		<input type="button" ng-show="pbdList.length > 1" ng-click="removeAllPbd()" value="Remove All PBD">
          </div>
</div>
          
        
<div id="BonusDB" ng-hide="BonusFlag">
	<form name="BonusForm" ng-submit="savePbdForBonus(BonusForm,pbdB)">
		<table>
		
			<tr>
				<td>From Station *</td>
				<td><input type ="text" id="pbdFromStnTempB" name ="pbdFromStnTempB" ng-model="pbdB.pbdFromStnTemp" readonly></td>
				<td><input type ="hidden" id="pbdFromStnB" name ="pbdFromStnB" ng-model="pbdB.pbdFromStn" ></td>
			</tr>
		
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="pbdToStnTempB" name ="pbdToStnTempB" ng-model="pbdB.pbdToStnTemp" readonly required></td>
				<td><input type ="hidden" id="pbdToStnB" name ="pbdToStnB" ng-model="pbdB.pbdToStn"></td>
			</tr>
			
			<tr>
				<td>Start Date *</td>
				<td><input type ="date" id="pbdStartDtB" name ="pbdStartDtB" ng-model="pbdB.pbdStartDt" required readonly></td>
			</tr>
			
			<tr>
				<td>End Date *</td>
				<td><input type ="date" id="pbdEndDtB" name ="pbdEndDtB" ng-model="pbdB.pbdEndDt" required readonly></td>
			</tr>
			
			<tr>
				<td>From Day *</td>
				<td><input type ="number" id="pbdFromDayB" name ="pbdFromDayB" ng-model="pbdB.pbdFromDay" min="1" required></td>
			</tr>
			
			<tr>
				<td>To Day *</td>
				<td><input type ="number" id="pbdToDayB" name ="pbdToDayB" ng-model="pbdB.pbdToDay" min="1" required></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="pbdVehicleTypeB" name ="pbdVehicleTypeB" ng-model="pbdB.pbdVehicleType" readonly required></td>
			</tr>
			
			<tr>
				<td> Hour Or Day *</td>
				<td>
				<select name="pbdHourNDayB" id="pbdHourNDayB" ng-model="pbdB.pbdHourNDay" required>
				<option value="Hour">Hour</option>
				<option value="Day">Day</option>
				</select></td>
			</tr>
			
			<!-- <tr>
				<td>P/B/D</td>
				<td><input type="text" id="pbdPenBonDetB" name ="pbdPenBonDetB" ng-model="pbdB.pbdPenBonDet"  readonly></td>
			</tr> -->
			
			<tr>
				<td>Amount *</td>
				<td><input type ="text" id="pbdAmtB" name ="pbdAmtB" ng-model="pbdB.pbdAmt" step="0.01" min="0.01" required></td>
			</tr>
			
		 	<tr>
				<td><input type="submit" value="Submit"  id="saveBonus" ></td>
			</tr>
		</table>
		</form>
</div>	 



<div id="DetentionDB" ng-hide="DetentionFlag">
	<form name="DetentionForm"  ng-submit="savePbdForDetention(DetentionForm,pbdD)">
		<table>
		
			<tr>
				<td>From Station *</td>
				<td><input type ="text" id="pbdFromStnTempD" name ="pbdFromStnTempD" ng-model="pbdD.pbdFromStnTemp" readonly></td>
				<td><input type ="hidden" id="pbdToStnD" name ="pbdToStnD" ng-model="pbdD.pbdToStn" ></td>
			</tr>
		
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="pbdToStnTempD" name ="pbdToStnTempD" ng-model="pbdD.pbdToStnTemp" readonly required></td>
				<td><input type ="hidden" id="pbdToStnD" name ="pbdToStnD" ng-model="pbdD.pbdToStn"></td>
			</tr>
			
			<tr>
				<td>Start Date *</td>
				<td><input type ="date" id="pbdStartDtD" name ="pbdStartDtD" ng-model="pbdD.pbdStartDt" required readonly></td>
			</tr>
			
			<tr>
				<td>End Date *</td>
				<td><input type ="date" id="pbdEndDtD" name ="pbdEndDtD" ng-model="pbdD.pbdEndDt" required readonly></td>
			</tr>
			
			<tr>
				<td>From Day *</td>
				<td><input type ="number" id="pbdFromDayD" name ="pbdFromDayD" ng-model="pbdD.pbdFromDay" required min="1"></td>
			</tr>
			
			<tr>
				<td>To Day *</td>
				<td><input type ="number" id="pbdToDayD" name ="pbdToDayD" ng-model="pbdD.pbdToDay" required min="1"></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="pbdVehicleTypeD" name ="pbdVehicleTypeD" ng-model="pbdD.pbdVehicleType" readonly required></td>
			</tr>
			
			<tr>
				<td> Hour Or Day *</td>
				<td>
				<select name="pbdHourNDayD" id="pbdHourNDayD" ng-model="pbdD.pbdHourNDay" required>
				<option value="Hour">Hour</option>
				<option value="Day">Day</option>
				</select></td>
			</tr>
			
			<tr>
				<td>Amount *</td>
				<td><input type ="number" id="pbdAmtD" name ="pbdAmtD" ng-model="pbdD.pbdAmt" required step="0.01" min="0.01"></td>
			</tr>
			
		 	<tr>
				<td><input type="submit" value="Submit" id="saveDetention" ></td>
			</tr>
		</table>
		</form>
</div>	


<div class="row"  ng-show="ctsList.length > 0" style="margin-bottom: 50px; margin-top: -100px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >
	<table class="tblrow">
			<caption class="coltag tblrow">NEW CTS LIST</caption>
		   <thead>
		        <tr class="rowclr">
		            <th class="colclr">From Station</th>
		            <th class="colclr">To Station</th>
		            <th class="colclr">Vehicle Type</th>
		            <th ng-show="prodctTypFlag" class="colclr">Product Type</th>
		            <th class="colclr">From Weight</th>
		            <th class="colclr">To Weight</th>
		            <th class="colclr">Rate</th>	
		            <th class="colclr">Action</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr class="tbl" ng-repeat="ctst in ctsList">
					<td class="rowcel">{{contract.contFrStn}}</td>
				    <td class="rowcel">{{contToStnTemp[$index].ToStnTemp}}</td>
				    <td class="rowcel">{{ctst.ctsVehicleType}}</td>
				    <td ng-show="prodctTypFlag" class="rowcel">{{productType}}</td>
				    <td class="rowcel">{{ctst.ctsFromWt}}</td>
		            <td class="rowcel">{{ctst.ctsToWt}}</td>
		            <td class="rowcel">{{ctst.ctsRate}}</td>
		            <td class="rowcel"><input type ="button" ng-click="removeCTS(ctst)" value="Remove"></td>
		        </tr>
		    </tbody>
	</table>
		<div class="input-field col s6 center">
		<input type ="button" ng-show="ctsList.length > 1" ng-click="removeAllCTS()" value="Remove All CTS">
		</div>
    </div>
</div>


	<div ng-show="ctsList.length > 0 || chgRate.length > 0 || chgBB">
		<input type="button" value="SUBMIT" ng-click="saveCts()"/>
	</div>
 
          
</div>
<div ng-show="operatorLogin || superAdminLogin">
<title>Insert title here</title>


	<!-- Angular.js UI design start -->	
		<div class="row">
			<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="EmployeeForm" ng-submit="submitEmployee(EmployeeForm,employee)">
			<!-- <div class="row">
      				<div class="input-field col s3">
      				<input class="validate " type="text"  name="empbranchName" ng-model="empbranchName"  readonly="true" ng-click="openBranchCodeDB()" required>
        				<input class="validate " type="hidden"  name="branchCode" ng-model="employee.branchCode" >
        				<label>Branch Code</label>
        				<input type="hidden" ng-model="employee.isInNCR">
        			</div>
        			<div class="input-field col s3">
        				<input type ="hidden" name ="userCode" ng-model="employee.userCode">
        			</div>
      		</div>	 -->
      		
      		<div class="row">
      				<div class="input-field col s3">
        				<input class="validate " type ="text" id="empFirstName" name ="empFirstName" ng-model="employee.empfirstname" ng-minlength="3" ng-maxlength="40">
        				<label for="empname">Employee First Name</label>
        			</div>
      				<div class="input-field col s3">
        				<input type ="text" id="empMiddleName" name="empMiddleName" ng-model="employee.empmiddlename" ng-minlength="2" ng-maxlength="40">
        				<label for="empname">Middle Name</label>
        			</div>
      				<div class="input-field col s3">
        				<input type ="text" id="empLastName" name="empLastName" ng-model="employee.emplastname"  ng-minlength="3" ng-maxlength="40">
        				<label for="empname">Last Name</label>
        			</div>
        			<div class="input-field col s3">	
        				<input class="validate " type ="text" id="empDesignation" name ="empDesignation" ng-model="employee.empDesignation"  ng-model-options="{ debounce: 200 }"  ng-keyup="getEmpDesigList(employee)"  ng-blur="fillEmpDesigVal()"  ng-minlength="2" ng-maxlength="40">
        				<label for="empDesignation">Designation</label>
      				</div>
      				
 					
      		</div>
      			
      		<div class="row">
      				<div class="input-field col s3">
        				<select name="empSex" ng-model="employee.empSex" required>
                    	<option value="Male">Male</option>
                    	<option value="Female">Female</option>   
                     	</select>
        				<label for="empsex">Employee Sex</label>
        			</div>
        			<div class="input-field col s3">	
        					<input type ="date" name ="empDOB" ng-model="employee.empDOB" >
        					<label>Date Of Birth</label>
      				</div>
      				
      				<div class="input-field col s3">	
        				<input type ="date" name ="empDOAppointment" ng-model="employee.empDOAppointment" >
        					<label>Date Of Appt</label>
      				</div>
      				
      				<div class="input-field col s3">	
        					<input id="empEduQualification" class="validate" type ="text" name ="empEduQuali" ng-model="employee.empEduQuali"  ng-minlength="2" ng-maxlength="255">
        					<label>Qualification</label>
      				</div>
      		</div>
      		
      		<div class="row">
      				<div class="input-field col s3">
        				<input id="empFatherFirstName" type ="text" name ="empFatherFirstName" ng-model="employee.fatherFirstName"  ng-minlength="3" ng-maxlength="40">
        				<label>Father's First Name</label>
        			</div>
      				<div class="input-field col s3">
        				<input id="empFatherLastName" type ="text" name="empFatherLastName" ng-model="employee.fatherLastName"  ng-minlength="3" ng-maxlength="40">
        				<label>Father's Last Name</label>
        			</div>
	      			<div class="input-field col s3">
        				<input id="empNomineeFirstName" type ="text" name ="empNomineeFirstName" ng-model="employee.nomineeFirstName"  ng-minlength="3" ng-maxlength="40">
        				<label>Nominee's First Name</label>
        			</div>
      				<div class="input-field col s3">
        				<input id="empNomineeLastName" type ="text" name="empNomineeLastName"  ng-model="employee.nomineeLastName" ng-minlength="3" ng-maxlength="40">
        				<label>Nominee's Last Name</label>
        			</div>
      		</div>
      		<div class="row">
      				<div class="input-field col s3">
        				<input class="validate " type ="text" id="empAdd" name ="empAdd" ng-model="employee.empAdd"  ng-minlength="3" ng-maxlength="255">
        				<label>Address</label>
        			</div>
        			<div class="input-field col s3">	
        					<input type ="text" id="empCity" name ="empCity" ng-model="employee.empCity"  ng-minlength="3" ng-maxlength="40">
        					<label>City</label>
      				</div>
      				
      				<div class="input-field col s3">	
        					<input type ="text" id="empState" name ="empState" ng-model="employee.empState"  ng-minlength="3" ng-maxlength="40">
        					<label>State</label>
      				</div>
      				
      				<div class="input-field col s3">	
        					<input class="validate" type ="text" name ="empPin" id="empPin" ng-model="employee.empPin"  ng-minlength="6" ng-maxlength="6" ng-blur="chkPresentAddrDB()">
        					<label>Pin</label>
      				</div>
      		</div>
      		
      		<div class="row">
      				<div class="input-field col s3">
        				<input class="validate " type ="text" id="empPresentAdd" name ="empPresentAdd" ng-model="employee.empPresentAdd"  ng-minlength="3" ng-maxlength="255" >
        				<label>Present Address</label>
        			</div>
        			<div class="input-field col s3">	
        					<input class="validate" type ="text" id="empPresentCity" name ="empPresentCity" ng-model="employee.empPresentCity"  ng-minlength="3" ng-maxlength="40">
        					<label>Present City</label>
      				</div>
      				
      				<div class="input-field col s3">	
        					<input class="validate" type ="text" id="empPresentState" name ="empPresentState" ng-model="employee.empPresentState"  ng-minlength="3" ng-maxlength="40">
        					<label>Present State</label>
      				</div>
      				
      				<div class="input-field col s3">	
        					<input class="validate" type ="text" id="empPresentPin" name ="empPresentPin" ng-model="employee.empPresentPin"  ng-minlength="6" ng-maxlength="6" >
        					<label>Present Pin</label>
      				</div>
      		</div>
      		
      		<div class="row">
      				<div class="input-field col s3">
        				<input class="validate " type ="number" name ="empBasic" id="empBasic" ng-model="employee.empBasic" ng-keyup="calculateOnBasic()"  step="0.01" min="0.00" >
        				<label>Basic</label>
        			</div>
        			<div class="input-field col s3">	
        					<input class="validate" type ="text" id="empHRAId" name ="empHRA" ng-model="employee.empHRA" ng-init="employee.empHRA=0" step="0.01" min="0.00">
        					<label>HRA</label>
      				</div>
      				
      				<div class="input-field col s3">	
        					<!-- <input class="validate" type ="number" name ="empOtherAllowance" id="empOtherAllowance" ng-model="employee.empOtherAllowance" ng-init="employee.empOtherAllowance=0.00" ng-keyup="calculateOnOthers(employee.empOtherAllowance)"  step="0.01" min="0.00" > -->
        					<input class="validate" type ="number" name ="empOtherAllowance" id="empOtherAllowance" ng-model="employee.empOtherAllowance" ng-init="employee.empOtherAllowance=0.00" step="0.01" min="0.00" >
        					<label>Other Allowance</label>
      				</div>
      				<div class="input-field col s3">
        				<input type ="text"  name ="empGross" ng-model="employee.empGross" ng-click="calGross()" readonly ng-required="true">
        				<label>Gross Salary</label>
        			</div>
       
      		</div>
      		
      		<div class="row">
      				<div class="input-field col s3">	
        				<input type ="text" name ="tdsName" ng-model="employee.empTds" ng-init="employee.empTds=0">	
        					<label>TDS</label>
      				</div>
      				
      				<div class="input-field col s3">
        				<input class="validate " type ="text" id="empESI" name ="empESI" ng-model="employee.empESI" ng-init="employee.empESI=0" readonly>
        				<label>ESI</label>
        			</div>
        			<div class="input-field col s3">	
        					<input class="validate" type ="text" id="empESINo" name ="empESINo" ng-model="employee.empESINo" ng-minlength="3" ng-maxlength="40" >
        					<label>ESI Number</label>
      				</div>
      				
      				<div class="input-field col s3">	
        					<input class="validate" type ="text" id="empESIDispensary" name ="empESIDispensary" ng-model="employee.empESIDispensary" ng-minlength="3" ng-maxlength="40">
        					<label>ESI Dispensary</label>
      				</div>
      		</div>
      		
      		
      		<div class="row">
      				<div class="input-field col s3">	
        					<input class="validate" type ="text" name ="empPF" ng-model="employee.empPF" readonly >
        					<label>PF</label>
      				</div>
      				
      				<!-- <div class="input-field col s3">	
        					<input class="validate" type ="text" id="empPFNo" name ="empPFNo" ng-model="employee.empPFNo"  ng-minlength="8" ng-maxlength="18">
        					<label>PF Number</label>	
      				</div> -->
      				<div class="input-field col s3">	
        				<input type ="text" name ="netSalary" ng-model="employee.netSalary" ng-click="calNetSal()" readonly ng-required="true">	
        					<label>Net Salary</label>
      				</div>
      				
      				<div class="input-field col s3">	
        					<input class="validate" type ="text" name ="emplrPF" ng-model="employee.empPFEmplr" readonly >
        					<label>Employer PF</label>
      				</div>
      				
      				<div class="input-field col s3">
        				<input class="validate " type ="text" id="emplrESI" name ="emplrESI" ng-model="employee.empESIEmplr" readonly>
        				<label>Employer ESI</label>
        			</div>
      		</div>
      		
      		
      		
      		<div class="row">
      				
        			<div class="input-field col s3">	
        					<input class="validate" type ="number" id="empLoanBal" name ="empLoanBal" ng-model="employee.empLoanBal"  step="0.01" min="0.00">
        					<label>Loan Balance</label>
      				</div>
      				
      				<div class="input-field col s3">	
        					<input class="validate" type ="number" id="empLoanPayment" name ="empLoanPayment" ng-model="employee.empLoanPayment"  step="0.01" min="0.00">
        					<label>Loan Payment</label>
      				</div>
      				<div class="input-field col s3">
        				<input class="validate" type ="text" id="empLicenseNo" name ="empLicenseNo" ng-model="employee.empLicenseNo"  ng-minlength="3" ng-maxlength="40">
        				<label>Licence No</label>
        			</div>
        			<div class="input-field col s3">	
        				<input class="validate" type ="date" name ="empLicenseExp" ng-model="employee.empLicenseExp" >
        					<label>Licence Exp</label>
      				</div>
      		</div>
      		
      		<div class="row">
      				<div class="input-field col s3">	
        					<input class="validate" type ="email" id="empMailId" name ="empMailId" ng-model="employee.empMailId"  ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" ng-maxlength="40">
        					<label>Mail ID</label>
      				</div>
      				
      				<div class="input-field col s3">	
        					<input class="validate" type ="text" id="empPhNo" name ="empPhNo" ng-model="employee.empPhNo" ng-minlength="4" ng-maxlength="15" min="0">
        					<label>Phone</label>
      				</div>
      				<div class="input-field col s3">	
        					<input class="validate" type ="number" id="empTelephoneAmt" name ="empTelephoneAmt" ng-model="employee.empTelephoneAmt"  step="0.01" min="0.00">
        					<label>Telephone Amt</label>
      				</div>
      				
      				<div class="input-field col s3">	
        				<input class="validate" type ="number" id="empMobAmt" name ="empMobAmt" ng-model="employee.empMobAmt" step="0.01" min="0.00">
        				<label>Mobile Amt</label>	
      				</div>
      		</div>
      		
      		<div class="row">
      				<div class="input-field col s4">
        				<input class="validate" type ="number" id="empBankAcNo" name ="empBankAcNo" ng-model="employee.empBankAcNo"  ng-minlength="10" ng-maxlength="40" min="0">
        				<label>Bank A/c Number</label>
        			</div>
        			
        			<div class="input-field col s4">	
        					<input class="validate" type ="text" id="empBankName" name ="empBankName" ng-model="employee.empBankName" ng-minlength="3" ng-maxlength="40">
        					<label>Bank Name</label>
      				</div>
      				<div class="input-field col s4">	
        					<input class="validate" type ="text" id="empPFNo" name ="empPFNo" ng-model="employee.empPFNo"  ng-minlength="8" ng-maxlength="18">
        					<label>PF Number</label>	
      				</div>
      		</div>

			<div class="row">
      				<div class="input-field col s4">
        				<input class="validate" type ="text" id="empBankBranch" name ="empBankBranch" ng-model="employee.empBankBranch" ng-minlength="3" ng-maxlength="40">
        				<label>Bank Branch</label>
        			</div>
        			<div class="input-field col s4">	
        					<input class="validate" type ="text" id="empBankIFS" name ="empBankIFS" ng-model="employee.empBankIFS"  ng-minlength="3" ng-maxlength="40">
        					<label>IFSC</label>
      				</div>
      				<div class="input-field col s4">
        				<input class="validate" type ="text" id="empPanNo" name ="empPanNo" ng-model="employee.empPanNo"  ng-minlength="10" ng-maxlength="10" ng-pattern="/^[a-zA-Z0-9]*$/">
        				<label>PAN No</label>
        			</div>
      		</div>
			
			<div class="row">
				<div class="col s12 center">
      			 		<input type="submit" value="Submit">
      			</div>
      		</div>
			
		</form>
	</div> 
<!-- Angular.js UI design end -->	

<!-- 		<form name="EmployeeForm" ng-submit="submitEmployee(EmployeeForm,employee)" >
			<table>
			
			    <tr>
					<td>User Code:</td>
					<td><input type ="hidden" name ="userCode" ng-model="employee.userCode"></td>
				</tr>
			
				<tr>
					<td>Employee Code:</td>
					<td><input type ="hidden" name ="empCode" ng-model="employee.empCode"></td>
				</tr>
				
				<tr>
			        <td>Employee Branch Code: *</td>
			        <td><input type="text"  name="branchCode" ng-model="employee.branchCode"  readonly="true" ng-click="openBranchCodeDB()" required></td>
			        <td><input type="hidden" ng-model="employee.isInNCR"></td>
			    </tr>
			
				
				<tr>
					<td>Employee Name: *</td>
					<td><input type ="text" name ="empName" ng-model="employee.empName" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
				</tr>
				
				<tr>
					<td>Employee Address: *</td>
					<td><input type ="text" name ="empAdd" ng-model="employee.empAdd" ng-required="true" ng-minlength="3" ng-maxlength="255"></td>
				</tr>
				
				<tr>
					<td>Employee City: *</td>
					<td><input type ="text" name ="empCity" ng-model="employee.empCity" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
				</tr>
				
				<tr>
					<td>Employee State: *</td>
					<td><input type ="text" name ="empState" ng-model="employee.empState" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
				</tr>
				
				<tr>
					<td>Employee Pin: *</td>
					<td><input type ="number" name ="empPin" ng-model="employee.empPin" ng-required="true" ng-minlength="6" ng-maxlength="6" min="0"></td>
				</tr>
				
				<tr>
					<td>Employee Date Of Birth: *</td>
					<td><input type ="date" name ="empDOB" ng-model="employee.empDOB" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Employee Date Of Appointment: *</td>
					<td><input type ="date" name ="empDOAppointment" ng-model="employee.empDOAppointment" ng-required="true"></td>
				</tr>
				
				<tr>
					<div class="ui-widget"> 
					<td><label for="tags">Employee Designation:</label></td>
					<td><input type ="text" id="empDesignation" name ="empDesignation" ng-model="employee.empDesignation"  ng-model-options="{ debounce: 200 }"  ng-keyup="getEmpDesigList(employee)"  ng-blur="fillEmpDesigVal()"></td>
					 </div> 
				</tr>
				
				<tr>
					<td>Employee Basic: *</td>
					<td><input type ="number" name ="empBasic" id="empBasic" ng-model="employee.empBasic" ng-keyup="calculateOnBasic()" ng-required="true" ng-pattern="/(^[0-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.00"></td>
				</tr>
				
				<tr>
					<td>Employee Other Allowance:</td>
					<td><input type ="number" name ="empOtherAllowance" ng-model="employee.empOtherAllowance" ng-init="employee.empOtherAllowance=0.00" ng-keyup="calculateOnOthers(employee.empOtherAllowance)"   ng-required="true" ng-pattern="/(^[0-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.00" ></td>
				</tr>
								
				<tr>
					<td>Employee HRA: * </td>
					<td><input type ="text" name ="empHRA" ng-model="employee.empHRA" readonly ></td>
				</tr>
				
				<tr>
					<td>Employee PF: *</td>
					<td><input type ="text" name ="empPF" ng-model="employee.empPF" readonly ></td>
				</tr>
				
				<tr>
					<td>Employee Gross: *</td>
					<td><input type ="text" name ="empGross" ng-model="employee.empGross" ></td>
				</tr>
				
				<tr>
					<td>Employee ESI:</td>
					<td><input type ="text" name ="empESI" id="empESI" ng-model="employee.empESI" readonly></td>
				</tr>
				
				<tr>
					<td>Employee Net Salary: *</td>
					<td><input type ="text" name ="netSalary" ng-model="employee.netSalary" readonly ></td>
				</tr>
				
				<tr>
					<td>Employee PF No: *</td>
					<td><input type ="text" name ="empPFNo" ng-model="employee.empPFNo" ng-required="true" ng-minlength="12" ng-maxlength="18"></td>
				</tr>
				
				<tr>
					<td>Employee ESI No:</td>
					<td><input type ="text" name ="empESINo" ng-model="employee.empESINo" ng-maxlength="40" ></td>
				</tr>
				
				<tr>
					<td>Employee ESI Dispensary:</td>
					<td><input type ="text" name ="empESIDispensary" ng-model="employee.empESIDispensary" ng-minlength="3" ng-maxlength="40"></td>
				</tr>
				
				<tr>
					<td>Employee Nominee: *</td>
					<td><input type ="text" name ="empNominee" ng-model="employee.empNominee" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
				</tr>
				
				<tr>
					<td>Employee Educational Qualification: *</td>
					<td><input type ="text" name ="empEduQuali" ng-model="employee.empEduQuali" ng-required="true" ng-minlength="3" ng-maxlength="255"></td>
				</tr>
															
				<tr>
					<td>Employee Loan Balance: *</td>
					<td><input type ="number" name ="empLoanBal" ng-model="employee.empLoanBal" ng-required="true" ng-pattern="/(^[0-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.00"></td>
				</tr>
				
				<tr>
					<td>Employee Loan Payment: *</td>
					<td><input type ="number" name ="empLoanPayment" ng-model="employee.empLoanPayment" ng-required="true" ng-pattern="/(^[0-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.00"></td>
				</tr>
				
				<tr>
					<td>Employee Present Address: *</td>
					<td><input type ="text" name ="empPresentAdd" ng-model="employee.empPresentAdd" ng-required="true" ng-minlength="3" ng-maxlength="255" ></td>
				</tr>
				
				<tr>
					<td>Employee Present City: *</td>
					<td><input type ="text" name ="empPresentCity" ng-model="employee.empPresentCity" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
				</tr>
				
				<tr>
					<td>Employee Present State: *</td>
					<td><input type ="text" name ="empPresentState" ng-model="employee.empPresentState" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
				</tr>
				
				<tr>
					<td>Employee Present Pin: *</td>
					<td><input type ="number" name ="empPresentPin" ng-model="employee.empPresentPin" ng-required="true" ng-minlength="6" ng-maxlength="6" min="0"></td>
				</tr>
				
				<tr>
                <td>Employee Sex: *</td>
                <td><select name="empSex" id="empSex" ng-model="employee.empSex" required>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>   
                     </select>
                </td>
                </tr>
				
				<tr>
					<td>Employee Father Name: *</td>
					<td><input type ="text" name ="empFatherName" ng-model="employee.empFatherName" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
				</tr>
				
				<tr>
					<td>Employee License No: *</td>
					<td><input type ="text" name ="empLicenseNo" ng-model="employee.empLicenseNo" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
				</tr>
				
				<tr>
					<td>Employee License Exp: *</td>
					<td><input type ="date" name ="empLicenseExp" ng-model="employee.empLicenseExp" ng-required="true"></td>
				</tr>
				
				<tr>
					<td>Employee Telephone Amount:</td>
					<td><input type ="number" name ="empTelephoneAmt" ng-model="employee.empTelephoneAmt" ng-pattern="/(^[0-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.00"></td>
				</tr>
				
				<tr>
					<td>Employee Mobile Amount:</td>
					<td><input type ="number" name ="empMobAmt" ng-model="employee.empMobAmt" ng-pattern="/(^[0-9]{1,7})+(\.[0-9]{1,2})?$/" step="0.01" min="0.00"></td>
				</tr>
				
				<tr>
					<td>Employee Bank Account No: *</td>
					<td><input type ="number" name ="empBankAcNo" ng-model="employee.empBankAcNo" ng-required="true" ng-minlength="10" ng-maxlength="40" min="0"></td>
				</tr>
				
				<tr>
					<td>Employee Pan No: *</td>
					<td><input type ="text" name ="empPanNo" ng-model="employee.empPanNo" ng-required="true" ng-minlength="10" ng-maxlength="10" ng-pattern="/[a-zA-Z0-9]/"></td>
				</tr>
				
				<tr>
					<td>Employee Mail Id: *</td>
					<td><input type ="email" name ="empMailId" ng-model="employee.empMailId" ng-required="true" ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" ng-maxlength="40"></td>
				</tr>
				
				<tr>
					<td>Employee Phone No:</td>
					<td><input type ="number" name ="empPhNo" ng-model="employee.empPhNo" ng-minlength="4" ng-maxlength="15" ></td>
				</tr>
				
				<tr>
					<td>Employee Bank IFS:</td>
					<td><input type ="text" name ="empBankIFS" ng-model="employee.empBankIFS"  ng-minlength="3" ng-maxlength="40"></td>
				</tr>
				
				<tr>
					<td>Employee Bank Name:</td>
					<td><input type ="text" name ="empBankName" ng-model="employee.empBankName" ng-minlength="3" ng-maxlength="40"></td>
				</tr>
				
				<tr>
					<td>Employee Bank Branch:</td>
					<td><input type ="text" name ="empBankBranch" ng-model="employee.empBankBranch" ng-minlength="3" ng-maxlength="40"></td>
				</tr>
								
				<tr>
					<td><input type="submit" value="Submit"></td>
				</tr>
			</table>
			
		</form> -->
		
	<div id ="branchCodeDB" ng-hide="branchCodeDBFlag">
	
	<input type="text" name="filterTextbox" ng-model="filterTextbox.branchCodeTemp" placeholder="Search by Branch Code Temp">
 	  
 	  <table>
 	 		<tr>	
 	 		    <th></th>
 	 		    		
				<th>Branch Code</th>
				
				<th>Branch Name</th>
			
				<th>Branch Pin</th>
				
				<th>Branch Code Temp</th>
				
				<!-- <th>Branch IsNCR</th> -->
			</tr>
		  
		  <tr ng-repeat="branch in branchList | filter:filterTextbox">
		 	  <td><input type="radio"  name="branchName" class="branchCls"  value="{{ branch.branchCode }}" ng-model="$parent.branchCode" ng-click="saveBranchCode(branch)"></td>
              <td>{{ branch.branchCode }}</td>
              <td>{{ branch.branchName }}</td>
              <td>{{ branch.branchPin }}</td>
              <td>{{ branch.branchCodeTemp }}</td>
              <!-- <td>{{ branch.isNCR }}</td> -->
          </tr>
		
		  <tr>
			  <td colspan="3"><input type="button" value="Create Branch Code"  ng-click="openBranchPage()"></td>
		  </tr>
          
         </table>
    </div>

    
    <div id="presentAddrDB" ng-hide="presentAddrDBFlag">
    	<div class="row">
    		<div class="col s12 center" style="margin: 20px 0px;">
    			<span class="white-text">Is present address same as correspondence address?</span></div>
    		<div class="col s6 center"><a class="white-text" ng-click="yesPresentAddr()">Yes</a></div>
   	    	<div class="col s6 center"><a class="white-text" ng-click="YesNoAddr()">No</a></div>
   	    </div>
    </div>
    
    <div id="viewEmpDetailsDB" ng-hide="viewEmpDetailsFlag">
    
   	<div class="row">
	
	<div ng-hide = "show" class="col s12 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
				<h4>Here's the details of Employee <span class="teal-text text-lighten-2">{{employee.empName}}</span>:</h4>
		<table class="table-bordered table-hover table-condensed">
	            
	           <!--  <tr>
	                <td>Branch Code:</td>
	                <td>{{employee.branchCode}}</td>
	            </tr> -->
	            
	            <tr>
	                <td>Name:</td>
	                <td>{{employee.empName}}</td>
	            </tr>
	            
	            <tr>
	                <td>Address:</td>
	                <td>{{employee.empAdd}}</td>
	            </tr>
	            <tr>
	                <td>City:</td>
	                <td>{{employee.empCity}}</td>
	            </tr>
	            <tr>
	                <td>State:</td>
	                <td>{{employee.empState}}</td>
	            </tr>
	            <tr>
	                <td>Pin:</td>
	                <td>{{employee.empPin}}</td>
	            </tr>
	            <tr>
	                <td>Date Of Birth:</td>
	                <td>{{employee.empDOB}}</td>
	            </tr>
	            <tr>
	                <td>Date Of Appointment:</td>
	                <td>{{employee.empDOAppointment}}</td>
	            </tr>
	            <tr>
	                <td>Designation:</td>
	                <td>{{employee.empDesignation}}</td>
	            </tr>
	            <tr>
	                <td>Basic:</td>
	                <td>{{employee.empBasic}}</td>
	                
	            </tr>
	            <tr>
	                <td>HRA:</td>
	                <td>{{employee.empHRA}}</td>
	            </tr>
	            <tr>
	                <td>Other Allowance:</td>
	                <td>{{employee.empOtherAllowance}}</td>
	            </tr>
	            <tr>
	                <td>PF No:</td>
	                <td>{{employee.empPFNo}}</td>
	            </tr>
	            <tr>
	                <td>ESI No:</td>
	                <td>{{employee.empESINo}}</td>
	            </tr>
	            <tr>
	                <td>ESI Dispensary:</td>
	                <td>{{employee.empESIDispensary}}</td>
	            </tr>
	            <tr>
	                <td>Nominee:</td>
	                <td>{{employee.empNominee}}</td>
	            </tr>
	            <tr>
	                <td>Educational Qualification:</td>
	                <td>{{employee.empEduQuali}}</td>
	            </tr>
	            <tr>
	                <td>PF:</td>
	                <td>{{employee.empPF}}</td>
	            </tr>
	            <tr>
	                <td>ESI:</td>
	                <td>{{employee.empESI}}</td>
	            </tr>
	            <tr>
	                <td>Loan Balance:</td>
	                <td>{{employee.empLoanBal}}</td>
	            </tr>
	            
	            <tr>
	                <td>Loan Payment:</td>
	                <td>{{employee.empLoanPayment}}</td>
	            </tr>
	            
	            <tr>
	                <td>Present Address:</td>
	                <td>{{employee.empPresentAdd}}</td>
	            </tr>
	            
	            <tr>
	                <td>City:</td>
	                <td>{{employee.empPresentCity}}</td>
	            </tr>
	            
	            <tr>
	                <td>State:</td>
	                <td>{{employee.empPresentState}}</td>
	            </tr>
	            
	            <tr>
	                <td>Present Pin:</td>
	                <td>{{employee.empPresentPin}}</td>
	            </tr>
	            
	            <tr>
	                <td>Sex:</td>
	                <td>{{employee.empSex}}</td>
	            </tr>
	            
	            <tr>
	                <td>Father Name:</td>
	                <td>{{employee.empFatherName}}</td>
	            </tr>
	            
	            <tr>
	                <td>License No:</td>
	                <td>{{employee.empLicenseNo}}</td>
	            </tr>
	            
	            <tr>
	                <td>License Exp:</td>
	                <td>{{employee.empLicenseExp}}</td>
	            </tr>
	            
	             <tr>
	                <td>Telephone Amount:</td>
	                <td>{{employee.empTelephoneAmt}}</td>
	            </tr>
	            
	             <tr>
	                <td>Mobile Amount:</td>
	                <td>{{employee.empMobAmt}}</td>
	            </tr>
	            
	             <tr>
	                <td>Bank Account No:</td>
	                <td>{{employee.empBankAcNo}}</td>
	            </tr>
	            
	             <tr>
	                <td>Pan No:</td>
	                <td>{{employee.empPanNo}}</td>
	            </tr>
	            
	             <tr>
	                <td>Mail Id:</td>
	                <td>{{employee.empMailId}}</td>
	            </tr>
	            
	             <tr>
	                <td>Phone No:</td>
	                <td>{{employee.empPhNo}}</td>
	            </tr>
	            
	             <tr>
	                <td>Bank IFSC:</td>
	                <td>{{employee.empBankIFS}}</td>
	            </tr>
	            
	             <tr>
	                <td>Bank Name:</td>
	                <td>{{employee.empBankName}}</td>
	            </tr>
	            
	             <tr>
	                <td>Bank Branch:</td>
	                <td>{{employee.empBankBranch}}</td>
	            </tr>
</table>

<input type="button" value="Save" id="saveId" ng-click="saveEmployee(employee)">
<input type="button" value="Cancel" ng-click="closeViewEmpDetailsDB()">
		
		
</div>
</div>
</div>
 </div>   
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Verify Contracts</title>
</head>
<body>

<form ng-submit="verifyContracts()" >

 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Contract Code</th>
				
				<th>Branch Code</th>
			
				<th>CR Name</th>
			</tr>
 	  
		  
		  <tr ng-repeat="dc in ContractList">
		 	  <td><input id="{{dc.contCode }}" type="checkbox" value="{{dc.contCode }}" ng-checked="selection.indexOf(dc.contCode) > -1" ng-click="toggleSelection(dc.contCode)" /></td>
			      <td>{{ dc.contCode }}</td>
	              <td>{{ dc.branchCode }}</td>
	              <td>{{ dc.crName }}</td>
          </tr>
          
         	 <tr>
				<td><input type="submit" value="Submit"></td>
			</tr>
			
         </table>
        </form> 
      
</body>
</html>
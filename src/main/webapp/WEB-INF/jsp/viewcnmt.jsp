<div ng-show="operatorLogin || superAdminLogin">
<title>View CNMT</title>
<div class="row">
	<div class="col s1 hide-on-med-and-down">&nbsp;</div>
	<div class="col s10 card"
		style="align: center; margin-top: 40px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">

	
<!-- 		<div class="input-field col s6"> -->
<!-- 			<input type="text" id="cnmtCode" name="cnmtCode" ng-model="cnmtCode" -->
<!-- 				required readonly ng-click="OpenCnmtDB()" /> <label>Enter -->
<!-- 				CNMT Code</label> -->
<!-- 		</div> -->

		<div class="input-field col s6">
			<input type="text" id="cnmtCode" name="cnmtCode" ng-model="cnmtCode"
				required maxlength="6" ng-change="OpenCnmtDB()" /> <label>Enter
				CNMT Code</label>
		</div>

		<div class="input-field col s6 center">
			<input class="btn waves-effect waves-light" type="submit"
				value="Submit" ng-click="viewCnmt(cnmtCode)">
		</div>

		<div id="showCnmtDetails" ng-hide="showCnmtDetailsFlag">
			<h4>
				Here's the review of <span class="teal-text text-lighten-2">{{cnmtCode}}</span>:
			</h4>
			<form name="CnmtForm" ng-submit="updateCnmts(CnmtForm)">
				<table class="table-hover table-condensed table-bordered">
					<tr>
						<td>Cnmt Code:</td>
						<td><input type="text" id="cnmtCode" name="cnmtCode"
							ng-model="cnmt.cnmtCode" value={{cnmt.cnmtCode}} required readonly></td>
					</tr>
					
					<tr ng-repeat="chlnNSedr in chlnNSedrList">
						<td>Chln & Sedr  No: {{$index + 1}}</td>
						<td>{{chlnNSedr.chlnCode}}&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp{{chlnNSedr.arCode}}</td>
						
					</tr>
					<!-- <tr>
	                <td>Branch Code:</td>
	                <td><input type ="text" id="branchCode" name ="branchCode" ng-model="cnmt.branchCode" value={{cnmt.branchCode}} ng-click="OpenBranchCodeDB()" required readonly></td>
	            </tr> -->
					<tr>
						<td>Customer Code:</td>
						<td><input type="text" name="custCode" id="custCode"
							ng-model="cnmt.custCode" value={{cnmt.custCode}}
							 readonly></td>
					</tr>
					<tr>
						<td>CNMT Date:</td>
						<td><!-- {{cnmt.cnmtDt | date : format : timezone}} --><input
							type="date" id="cnmtDt" name="cnmtDt" ng-model="cnmt.cnmtDt"
							value={{cnmt.cnmtDt}} ng-blur="dateSelect(cnmt.cnmtDt)"></td>
					</tr>
					<tr>
						<td>CNMT From Station:</td>
						<td><input type="text" name="cnmtFromSt" id="cnmtFromSt"
							ng-model="cnmt.cnmtFromSt" value={{cnmt.cnmtFromSt}}
							required readonly></td>
					</tr>
					<tr>
						<td>CNMT To Station:</td>
						<td><input type="text" name="cnmtToSt" id="cnmtToSt"
							ng-model="cnmt.cnmtToSt" value={{cnmt.cnmtToSt}}
							required readonly></td>
					</tr>
					<tr>
						<td>CNMT Consignor:</td>
						<td><input type="text" name="cnmtConsignor"
							id="cnmtConsignor" ng-model="cnmt.cnmtConsignor"
							value={{cnmt.cnmtConsignor}} 
							required readonly></td>
					</tr>
					<tr>
						<td>CNMT Consignee:</td>
						<td><input type="text" name="cnmtConsignee"
							id="cnmtConsignee" ng-model="cnmt.cnmtConsignee"
							value={{cnmt.cnmtConsignee}} 
							required readonly></td>
					</tr>
					<tr>
						<td>Contract Code:</td>
						<td><input type="text" name="contractCode" id="contractCode1"
							ng-model="cnmt.contractCode" value={{cnmt.contractCode}}
							required readonly></td>
					</tr>
					<tr>
						<td>CNMT DC:</td>
						<td><input type="text" name="cnmtDC" id="cnmtDC"
							ng-model="cnmtDC" value={{cnmtDC}} readonly readonly></td>
					</tr>
					<tr>
						<td>NO. Of Package:</td>
						<td><input type="number" name="cnmtNoOfPkg" id="cnmtNoOfPkg"
							ng-model="cnmt.cnmtNoOfPkg" ng-minlength="1" ng-maxlength="7" value={{cnmt.cnmtNoOfPkg}}
							ng-blur="chkPkg()" ng-required="true" readonly></td>
					</tr>
					<tr>
						<td>CNMT Actual Weight:</td>
						<td><input class="col s6" type="number" name="cnmtActualWt"
							id="cnmtActualWt" ng-model="cnmt.cnmtActualWt"
							value={{cnmt.cnmtActualWt}} step="0.000000001" min="0.00000000"
							ng-blur="chngActCnmtWt(cnmt.cnmtActualWt)" required readonly>
							<select class="col s6" name="cnmtActualWt1" id="cnmtActualWt1"
							ng-model="cnmtActualWtPer" ng-init="cnmtActualWtPer='Ton'"
							required>
								<option value="Ton">Ton</option>
								<option value="Kg">Kg</option>
						</select></td>
					</tr>
					<tr>
						<td>CNMT Guarantee Weight:</td>
						<td><input class="col s6" type="number"
							name="cnmtGuaranteeWt" id="cnmtGuaranteeWt"
							ng-model="cnmt.cnmtGuaranteeWt" value={{cnmt.cnmtGuaranteeWt}}
							step="0.000000001" min="0.00000000" required readonly> <select class="col s6" name="cnmtGuaranteeWt1"
							id="cnmtGuaranteeWt1" ng-model="cnmtGuaranteeWtPer"
							ng-init="cnmtGuaranteeWtPer='Ton'" required>
								<option value="Ton">Ton</option>
								<option value="Kg">Kg</option>
						</select></td>
					</tr>
					<tr>
						<td>Door to Door Delivery:</td>
						<td><input type="text" name="cnmtDDL" id="cnmtDDL"
							ng-model="cnmtDDL" value={{cnmtDDL}} readonly></td>
					</tr>
					<tr>
						<td>CNMT Pay ON:</td>
						<td><input type="text" name="cnmtPayAt" id="cnmtPayAt"
							ng-model="cnmt.cnmtPayAt" value={{cnmt.cnmtPayAt}}
							readonly></td>
					</tr>
					<tr>
						<td>CNMT Bill ON:</td>
						<td><input type="text" name="cnmtBillAt" id="cnmtBillAt"
							ng-model="cnmt.cnmtBillAt" value={{cnmt.cnmtBillAt}}
							readonly></td>
					</tr>
					<tr>
						<td>CNMT Rate:</td>
						<td><input class="col s6" type="number" name="cnmtRate"
							id="cnmtRate" ng-model="cnmt.cnmtRate" value={{cnmt.cnmtRate}}
							readonly> <select class="col s6" name="chlnLryRate1"
							id="chlnLryRate1" ng-model="chlnLryRatePer"
							ng-init="chlnLryRatePer='Kg'"
							ng-change="chngRate(chlnLryRatePer)">
								<option value="Ton">Ton</option>
								<option value="Kg">Kg</option>
						</select></td>
					</tr>
					<tr>
						<td>CNMT Freight:</td>
						<td><input type="number" name="cnmtFreight" id="cnmtFreight"
							ng-model="cnmt.cnmtFreight" value={{cnmt.cnmtFreight}}
							readonly></td>
					</tr>
					<tr>
						<td>CNMT VOG:</td>
						<td><input type="number" name="cnmtVOG" id="cnmtVOG"
							ng-model="cnmt.cnmtVOG" value={{cnmt.cnmtVOG}} 
							ng-minlength="1" step="0.01"
					        min="0.00" ng-maxlength="7" ng-required="true" readonly></td>
					</tr>
					<tr>
						<td>CNMT Extra Exp.:</td>
						<td><input type="number" name="cnmtExtraExp"
							id="cnmtExtraExp" ng-model="cnmt.cnmtExtraExp"
							value={{cnmt.cnmtExtraExp}} step="0.01" min="0.00"
							ng-minlength="1" ng-maxlength="7" readonly></td>
					</tr>
					<tr>
						<td>CNMT Cost Grade:</td>
						<td><input type="text" name="cnmtCostGrade"
							id="cnmtCostGrade" ng-model="cnmtCostGrade"
							value={{cnmtCostGrade}} readonly></td>
					</tr>
					<tr>
						<td>Date of Dly:</td>
						<td>{{cnmt.cnmtDtOfDly | date:'shortDate'}}<!-- <input type="date"
							name="cnmtDtOfDly" id="cnmtDtOfDly" ng-model="cnmt.cnmtDtOfDly"
							value={{cnmt.cnmtDtOfDly}}> --></td>
					</tr>
					<tr>
						<td>Employee Code:</td>
						<td><input type="text" name="cnmtEmpCode" id="cnmtEmpCode"
							ng-model="cnmt.cnmtEmpCode" value={{cnmt.cnmtEmpCode}}
							required readonly></td>
					</tr>
					<tr>
						<td>CNMT KM:</td>
						<td><input type="number" name="cnmtKm" id="cnmtKm"
							ng-model="cnmt.cnmtKm" value={{cnmt.cnmtKm}} ng-minlength="1"
							ng-maxlength="7" readonly></td>
					</tr>
					<tr>
						<td>Vehicle Type:</td>
						<td><input type="text" name="cnmtVehicleType"
							id="cnmtVehicleType" ng-model="cnmt.cnmtVehicleType"
							value={{cnmt.cnmtVehicleType}} readonly></td>
					</tr>
					<tr>
						<td>Product Type:</td>
						<td><input type="text" name="cnmtProductType"
							id="cnmtProductType" ng-model="cnmt.cnmtProductType"
							value={{cnmt.cnmtProductType}} readonly
							></td>
					</tr>
					<tr>
						<td>CNMT State:</td>
						<td><input type="text" name="cnmtState" id="cnmtState"
							ng-model="cnmt.cnmtState" value={{cnmt.cnmtState}}
							readonly></td>
					</tr>
					<tr>
						<td>CNMT Total:</td>
						<td><input type="number" name="cnmtTOT" id="cnmtTOT"
							ng-model="cnmt.cnmtTOT" value={{cnmt.cnmtTOT}} readonly></td>
					</tr>
					<tr ng-repeat="invoices in cnmt.cnmtInvoiceNo">
						<td>CNMT Invoice No {{$index + 1}}:</td>
						<td>{{invoices.invoiceNo}}</td>
						<td>{{invoices.date | date : format : timezone}}</td>
						<td><input type="button" value="Remove"
							ng-click="removeCnmtInv($index)"></td>
					</tr>
					
					<tr>
						<td>Bill No:</td>
						<td><input type="text" name="billName" id="billId"
							ng-model="cnmt.cnmtBillNo" value={{cnmt.cnmtBillNo}} readonly></td>
					</tr>
				</table>

				<div class="row">
					<div class="col s12 center">
						<input type="button" value="Add Invoice Number"
							ng-click="openInvoiceNo()">
					</div>
				</div>
				
				<!-- <div class="row">
					<div class="input-field col s6">
						<input type="file" class="teal white-text btn"
							file-model="cnmtImage" /> <label>Choose File</label>
					</div>

					<div class="col s6">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadCnmtImage()">Upload ACK</a>
					</div>

				</div>

				<div class="row">
					<div class="input-field col s6">
						<input type="file" class="teal white-text btn"
							file-model="cnmtConfirmImage" /> <label>Choose File</label>
					</div>

					<div class="col s6">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadCnmtConfImage()">Upload Confirm
							ACK</a>
					</div>

				</div>
 -->
				<!-- <input type="submit" value="Submit!"> -->
			</form>
		</div>
	</div>
	<div class="col s3">&nbsp;</div>
</div>


<div id="cnmtDB" ng-hide="CnmtDBFlag">
	<input type="text" name="filterTextbox" ng-model="filterTextbox"
		placeholder="Search CNMT Codes">
	<table>
		<tr ng-repeat="cnmtCode in cnmtList | filter:filterTextbox">
			<td><input type="radio" name="cnmtCode" id="cnmtId"
				value="{{ cnmtCode }}" ng-model="$parent.cnmtCode"
				ng-click="saveCnmt(cnmtCode)"></td>
			<td>{{ cnmtCode }}</td>
		</tr>
	</table>
</div>


<div id="branchCodeDB" ng-hide="BranchCodeDBFlag">
	<input type="text" name="filterTextbox"
		ng-model="filterTextbox.branchCode" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>

			<th>Branch Code</th>

			<th>Branch Name</th>

			<th>Branch Pin</th>
		</tr>
		<tr ng-repeat="branch in branchList | filter:filterTextbox"">
			<td><input type="radio" name="branchCode" id="branchId"
				value="{{ branch.branchCode }}" ng-model="branchCode"
				ng-click="saveBranchCode(branch)"></td>
			<td>{{ branch.branchCode }}</td>
			<td>{{ branch.branchName }}</td>
			<td>{{ branch.branchPin }}</td>
		</tr>
	</table>

</div>


<div id="cnmtCodeDB" ng-hide="CnmtCodeDBFlag">
	<input type="text" name="filterCnmtCode"
		ng-model="filterCnmtCode.brsLeafDetSNo" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>
			<th>Cnmt Code</th>
		</tr>
		<tr ng-repeat="cnmt in cnmtCodeList | filter:filterCnmtCode">
			<td><input type="radio" name="cnmtCode" id="cnmtId"
				value="{{ cnmt.brsLeafDetSNo }}" ng-model="cnmtCode"
				ng-click="saveCnmtCode(cnmt)"></td>
			<td>{{ cnmt.brsLeafDetSNo }}</td>
		</tr>
	</table>
</div>


<div id="customerCodeDB" ng-hide="CustomerCodeDBFlag">
	<input type="text" name="filterCustomerCode"
		ng-model="filterCustomerCode.custCode" placeholder="Search by Code">

	<table>
		<tr>
			<th></th>
			<th>Customer Code</th>

			<th>Customer Name</th>

			<th>Daily Contract Allowed</th>
		</tr>

		<tr ng-repeat="customer in customerList | filter:filterCustomerCode">
			<td><input type="radio" name="custCode" id="custId"
				value="{{ customer.custCode }}" ng-model="custCode"
				ng-click="saveCustomerCode(customer)"></td>
			<td>{{ customer.custCode }}</td>
			<td>{{ customer.custName }}</td>
			<td>{{ customer.custIsDailyContAllow }}</td>
		</tr>
	</table>
</div>


<div id="CNMTConsignorDB" ng-hide="CNMTConsignorDBFlag">
	<input type="text" name="filterCNMTConsignor"
		ng-model="filterCNMTConsignor.custCode" placeholder="Search by Code">

	<table>
		<tr>
			<th></th>
			<th>Customer Code</th>

			<th>Customer Name</th>

			<th>Daily Contract Allowed</th>
		</tr>

		<tr ng-repeat="consignor in customerList | filter:filterCNMTConsignor">
			<td><input type="radio" name="custCode" id="custId"
				value="{{ consignor.custCode }}" ng-model="custCode"
				ng-click="saveConsignorCustomerCode(consignor)"></td>
			<td>{{ consignor.custCode }}</td>
			<td>{{ consignor.custName }}</td>
			<td>{{ consignor.custIsDailyContAllow }}</td>
		</tr>
	</table>
</div>


<div id="CNMTConsigneeDB" ng-hide="CNMTConsigneeDBFlag">
	<input type="text" name="filterCNMTConsignee"
		ng-model="filterCNMTConsignee.custCode" placeholder="Search by Code">

	<table>
		<tr>
			<th></th>
			<th>Customer Code</th>

			<th>Customer Name</th>

			<th>Daily Contract Allowed</th>
		</tr>

		<tr ng-repeat="consignee in customerList | filter:filterCNMTConsignee">
			<td><input type="radio" name="custCode" id="custId"
				value="{{ consignee.custCode }}" ng-model="custCode"
				ng-click="saveConsigneeCustomerCode(consignee)"></td>
			<td>{{ consignee.custCode }}</td>
			<td>{{ consignee.custName }}</td>
			<td>{{ consignee.custIsDailyContAllow }}</td>
		</tr>
	</table>
</div>


<div id="contractCodeDB" ng-hide="ContractCodeDBFlag">
	<input type="text" name="filterContractCode"
		ng-model="filterContractCode.contCode" placeholder="Search by Code">

	<table class="table" style="border-bottom: 1px solid white;">
		<tr>
			<th style="border-right: 1px solid grey"><br />
				<div class="hori-td">&nbsp;</div> <br />
				<div class="hori-td">Customer Code</div> <br />
				<div class="hori-td">Contract Code</div> <br />
				<div class="hori-td">From Station</div> <br />
				<div class="hori-td">To Station</div> <br />
				<div class="hori-td">From Date</div> <br />
				<div class="hori-td">To Date</div> <br />
				<div class="hori-td">Rate</div> <br />
				<div class="hori-td">Vehicle Type</div> <br />
				<div class="hori-td">Daily Contract</div> <br />
				<div class="hori-td">Door To Door Delivery</div> <br />
				<div class="hori-td">Cost Grade</div> <br />
				<div class="hori-td">Contract Type</div></th>


			<td ng-repeat="contract in contractList | filter:filterContractCode">
				<br />
				<div class="hori-td">
					<input type="radio" name="contCode" id="contId"
						value="{{ contract.contCode }}" ng-model="contCode"
						ng-click="saveContractCode(contract)">
				</div> <br />
				<div class="hori-td">{{ contract.custCode }}</div> <br />
				<div class="hori-td">{{ contract.contCode }}</div> <br />
				<div class="hori-td">{{ contract.fromStation }}</div> <br />
				<div class="hori-td">{{ contract.toStation }}</div> <br />
				<div class="hori-td">{{ contract.fromDate }}</div> <br />
				<div class="hori-td">{{ contract.toDate }}</div> <br />
				<div class="hori-td">{{ contract.rate }}</div> <br />
				<div class="hori-td">{{ contract.vehicleType }}</div> <br />
				<div class="hori-td">{{ contract.cnmtDC }}</div> <br />
				<div class="hori-td">{{ contract.cnmtDDL }}</div> <br />
				<div class="hori-td">{{ contract.cnmtCostGrade }}</div> <br />
				<div class="hori-td">{{ contract.contType }}</div>
			</td>
		</tr>
	</table>
</div>


<div id="CNMTPayAtDB" ng-hide="CNMTPayAtDBFlag">
	<input type="text" name="filterCNMTPay"
		ng-model="filterCNMTPay.branchCode" placeholder="Search by Code">

	<table>
		<tr>
			<th></th>

			<th>Branch Code</th>

			<th>Branch Name</th>

			<th>Branch Pin</th>
		</tr>
	</table>
	<table>

		<tr ng-repeat="payat in branchList | filter:filterCNMTPay">
			<td><input type="radio" name="branchCode" id="branchId"
				value="{{ payat.branchCode }}" ng-model="branchCode"
				ng-click="savePayAtBranchCode(payat)"></td>
			<td>{{ payat.branchCode }}</td>
			<td>{{ payat.branchName }}</td>
			<td>{{ payat.branchPin }}</td>
		</tr>
	</table>
</div>


<div id="CNMTBillAtDB" ng-hide="CNMTBillAtDBFlag">
	<input type="text" name="filterCNMTBill"
		ng-model="filterCNMTBill.branchCode" placeholder="Search by Code">

	<table>
		<tr>
			<th></th>

			<th>Branch Code</th>

			<th>Branch Name</th>

			<th>Branch Pin</th>
		</tr>
	</table>
	<table>

		<tr ng-repeat="billat in branchList | filter:filterCNMTBill">
			<td><input type="radio" name="branchCode" id="branchId"
				value="{{ billat.branchCode }}" ng-model="branchCode"
				ng-click="saveBillAtBranchCode(billat)"></td>
			<td>{{ billat.branchCode }}</td>
			<td>{{ billat.branchName }}</td>
			<td>{{ billat.branchPin }}</td>
		</tr>
	</table>
</div>


<div id="employeeCodeDB" ng-hide="EmployeeCodeDBFlag">
	<input type="text" name="filterEmployeeCode"
		ng-model="filterEmployeeCode.empCode" placeholder="Search by Code">

	<table>
		<tr>
			<th></th>

			<th>Employee Code</th>

			<th>Employee Name</th>
		</tr>

		<tr ng-repeat="employee in employeeList | filter:filterEmployeeCode">
			<td><input type="radio" name="empCode" id="empId"
				value="{{ employee.empCode }}" ng-model="empCode"
				ng-click="saveEmployeeCode(employee)"></td>
			<td>{{ employee.empCode }}</td>
			<td>{{ employee.empName }}</td>
		</tr>
	</table>
</div>


<div id="stateCodeDB" ng-hide="StateCodeDBFlag">
	<input type="text" name="filterStateCode"
		ng-model="filterStateCode.stateCode" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>
			<th>State Code</th>

			<th>State Name</th>

			<th>State Lorry Prefix</th>
		</tr>
	</table>
	<table>

		<tr ng-repeat="state in stateList | filter:filterStateCode">
			<td><input type="radio" name="stateCode" id="stateId"
				value="{{ state.stateCode }}" ng-model="stateCode"
				ng-click="saveStateCode(state)"></td>
			<td>{{ state.stateCode }}</td>
			<td>{{ state.stateName }}</td>
			<td>{{ state.stateLryPrefix }}</td>
		</tr>
	</table>
</div>


<div id="saveInvoiceNo" ng-hide="saveInvoiceNoFlag">
	<form name="saveInvoiceNoForm"
		ng-submit="saveInvoiceNum(saveInvoiceNoForm,invoiceNo,invoiceDt)">
		<table>
			<tr>
				<td>Invoice No:</td>
				<td><input type="text" id="invoiceNo" name="invoiceNo"
					ng-model="invoiceNo" ng-minlength="3" ng-maxlength="15" required></td>
				<td><input type="date" id="invoiceDt" name="invoiceDt"
					ng-model="invoiceDt" required></td>
			</tr>
			<tr>
				<td><input type="submit" value="Submit"></td>
			</tr>

		</table>
	</form>
</div>


<div id="cnmtToStationDB" ng-hide="CnmtToStationDBFlag">
	<input type="text" name="filterCnmtToStation"
		ng-model="filterCnmtToStation.stnCode" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>

			<th>Station Code</th>

			<th>Station Name</th>

			<th>Station District</th>
		</tr>
		<tr ng-repeat="station in stationList | filter:filterCnmtToStation">
			<td><input type="radio" name="stnName" id="stnName"
				class="stnName" value="{{ station}}" ng-model="stnName1"
				ng-click="saveToStnCode(station)"></td>
			<td>{{ station.stnCode }}</td>
			<td>{{ station.stnName }}</td>
			<td>{{ station.stnDistrict }}</td>
		</tr>
	</table>
</div>

<div id="cnmtFromStationDB" ng-hide="CnmtFromStationDBFlag">
	<input type="text" name="filterCnmtFromStation"
		ng-model="filterCnmtFromStation.stnCode" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>

			<th>Station Code</th>

			<th>Station Name</th>

			<th>Station District</th>
		</tr>


		<tr ng-repeat="station in stationList | filter:filterCnmtFromStation">
			<td><input type="radio" name="stnName" id="stnName"
				class="stnName" value="{{ station }}" ng-model="stnName"
				ng-click="saveFrmStnCode(station)"></td>
			<td>{{ station.stnCode }}</td>
			<td>{{ station.stnName }}</td>
			<td>{{ station.stnDistrict }}</td>
		</tr>
	</table>
</div>


<div id="cnmtProductTypeDB" ng-hide="ProductTypeFlag">
	<input type="text" name="filterProductType"
		ng-model="filterProductType" placeholder="Search by Product Name">
	<table class="noborder">
		<tr>
			<th></th>
			<th>Product Name</th>
		</tr>


		<tr ng-repeat="pt in ptList | filter:filterProductType">
			<td><input type="radio" name="ptName" class="ptName"
				value="{{ pt }}" ng-model="ptName" ng-click="savProductName(pt)"></td>
			<td>{{ pt }}</td>
		</tr>
	</table>
</div>



<div id="vehicleTypeDB" ng-hide="VehicleTypeDBFlag">
	<input type="text" name="filterVehicleType"
		ng-model="filterVehicleType.vtCode" placeholder="Search by Code">

	<table>
		<tr>
			<th></th>
			<th>Vehicle Type</th>
			<th>Vehicle Type Code</th>
		</tr>
		<tr ng-repeat="vt in vtList | filter:filterVehicleType">
			<td><input type="radio" name="vtCode" id="vtId" value="{{ vt }}"
				ng-model="vtCode" ng-click="saveVehicleType(vt)"></td>
			<td>{{ vt.vtVehicleType }}</td>
			<td>{{ vt.vtCode }}</td>
		</tr>
	</table>
</div>
</div>
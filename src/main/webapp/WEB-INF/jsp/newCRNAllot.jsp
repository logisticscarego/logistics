<div ng-show="operatorLogin || superAdminLogin">
	<div class="row noprint">
		<form name="newCrnAllotForm" ng-submit=submit(newCrnAllotForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchId" name ="branchName" ng-model="ems.branch.branchFaCode" ng-click="openBranchDB()" readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="spNameId" name ="spName" ng-model="ems.electricityMstr.emSPName" ng-required="true" >
		       			<label for="code">Service Provider</label>	
		       		</div>
		       	  
		       	    <div class="col s4 input-field">
			    		<select name="particular" id="particularId" ng-model="ems.electricityMstr.emParticular" ng-change="selectPart()" ng-required="true"">
							<option value="Office">Office</option>
							<option value="Staff">Staff</option>
						</select>
						<label>Particular</label>
					</div>
		    </div>
		    <div class="row">
		    		 <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="empId" name ="empName" ng-model="ems.employee.empFaCode" ng-click="openEmpDB()" readonly disabled="disabled">
		       			<label for="code">Employee Code</label>	
		       		</div>
		       		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="crnId" name ="crn" ng-model="ems.electricityMstr.emCrnNo" ng-required="true" >
		       			<label for="code">Consumer No</label>	
		       		</div>
		  
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="instDtId" name ="instDt" ng-model="ems.electricityMstr.emInstDt">
		       			<label for="code">Initiate Date</label>	
		       		</div>
		  
		    </div>
		    
	      	 <div class="row">
	      	 	<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="discDtId" name ="discDt" ng-model="ems.electricityMstr.emDiscDt">
		       			<label for="code">Disconnect Date</label>	
		       	</div>
	     		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="advDepDtId" name ="advDepDt" ng-model="ems.electricityMstr.emAdvDepDt">
		       			<label for="code">Advance Deposit Date</label>	
		       	</div>
		       	
		       	<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="advDepAmtId" name ="advDepAmt" ng-model="ems.electricityMstr.emAdvDepAmt">
		       			<label for="code">Advance Deposit Amount</label>	
		       	</div>
	   		 </div>
	   	
	   		 <div class="row">
				<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="refDtId" name ="refDt" ng-model="ems.electricityMstr.emRefDt">
		       			<label for="code">Refund Date</label>	
		        </div>
		       	<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="refAmtId" name ="refAmt" ng-model="ems.electricityMstr.emRefAmt">
		       			<label for="code">Refund Amount</label>	
		       	</div>
		       	<div class="col s4 input-field">
		       				<select name="billDay" ng-model="ems.electricityMstr.emBillDay" ng-options="billDay as billDay for billDay in billDays" required></select>
						<label>Bill Day</label>
		       	</div> 	
    		 </div>	
    		 
    		 <div class="row">
    		 	<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="dueDayId" name ="dueDay" ng-model="ems.electricityMstr.emDueDay">
		       			<label for="code">Due Day</label>	
		       	</div>
    		 </div>
     		 
	   		  <div class="row">
	     		<div class="col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	   		 </div>
		  </form>
    </div>
    
    <div id ="branchCodeDB" ng-hide="branchCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Branch Name</th>
 	  	 	  <th>Branch FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in branchList | filter:filterTextbox">
		 	  <td><input type="radio"  name="branch"   value="{{ branch }}" ng-model="brCode" ng-click="saveBrCode(branch)"></td>
              <td>{{ branch.branchName }}</td>
              <td>{{ branch.branchFaCode }}</td>
          </tr>
      </table> 
	</div>

	<div id ="empCodeDB" ng-hide="empCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Employee Name</th>
 	  	 	  <th>Employee FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="emp in empList | filter:filterTextbox">
		 	  <td><input type="radio"  name="emp"   value="{{ emp }}" ng-model="empFaCode" ng-click="saveEmpCode(emp)"></td>
              <td>{{ emp.empName }}</td>
              <td>{{ emp.empFaCode }}</td>
          </tr>
      </table> 
	</div>
	
		
	<div id="saveEmsDB" ng-hide="saveEmsFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{ems.branch.branchFaCode}}</td>
				</tr>
				
				<tr ng-show="showStaffCode">
					<td>Staff Code</td>
					<td>{{ems.employee.empFaCode}}</td>
				</tr>
				
				<tr>
					<td>Service Provider</td>
					<td>{{ems.electricityMstr.emSPName}}</td>
				</tr>
				
				<tr>
					<td>Initiate Date</td>
					<td>{{ems.electricityMstr.emInstDt}}</td>
				</tr>
				
				<tr>
					<td>Disconnect Date</td>
					<td>{{ems.electricityMstr.emDiscDt}}</td>
				</tr>
				
				<tr>
					<td>Advance Deposit Date</td>
					<td>{{ems.electricityMstr.emAdvDepDt}}</td>
				</tr>
				
				<tr>
					<td>Advance Deposite Amount</td>
					<td>{{ems.electricityMstr.emAdvDepAmt}}</td>
				</tr>
				
				<tr>
					<td>Refund Date</td>
					<td>{{ems.electricityMstr.emRefDt}}</td>
				</tr>
				
				<tr>
					<td>Refund Amount</td>
					<td>{{ems.electricityMstr.emRefAmt}}</td>
				</tr>
				
				<tr>
					<td>Bill Day</td>
					<td>{{ems.electricityMstr.emBillDay}}</td>
				</tr>
				
				<tr>
					<td>Due Day</td>
					<td>{{ems.electricityMstr.emDueDay}}</td>
				</tr>
				
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" ng-click="saveEms()"/>
		</div>
	</div>

</div>

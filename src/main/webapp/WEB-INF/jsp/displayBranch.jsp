<div ng-show="superAdminLogin">
<title>Display Branch</title>
<div class="">
<div class="card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
<div class="col s12">
<div class="row">
		<div class="input-field col s6">
			<input class="prefix" type="radio" id="branchModalRadio" name="branchRadio" ng-model="branchModalRadio" ng-click="enableBranchCodeTextbox()">
			<input type ="text" name="branchTextList" id="branchTextList" ng-model="branchCodeModalList" disabled="disabled" ng-click="OpenBranchCodeDB()" readonly>	
		</div>
		<div class="col s6"><input type ="text" id="creationTSId" name="creationTSName" ng-model="creationTSModel" disabled="disabled" ng-click="OpenCreationTSDB()" readonly></div>
</div>
<div class="row">		
		 <div class="col s2">
			<input type="radio" id="branchAutoRadio" name="branchRadio" ng-model="branchAutoRadio" ng-click="enableAutoBranchCodeTextBox()">
		</div>
		<div class="col s8">
			<input type = "text" name="branchAutoTextList" id="branchAutoTextList"  ng-model="branchCodeAutoList" disabled="disabled" ng-keyup="getbranchCodeList()">
		</div>
		<div class="col s2">
			<input class="col s12" type ="button" id="okId" value = "OK" ng-click="getBranchByBrCodeTemp(branchCodeAutoList)">
		</div> 
</div>
<div class="row">
		<div class="col s2">
			<input type ="button" id="bkToList" value = "Back To list" ng-click="backToList()">
		</div>
</div>

<div id="branchTableDB" ng-hide="branchTableDBFlag">
<input type="text" name="filterBrCode" ng-model="filterBrCode.branchCodeTemp" placeholder="Search by Branch Code">

		<table>
 	 		<tr>
 	 			<th></th>
 	 			
 	 			<!-- <th>Branch Id</th> -->
 	 		 	 		    	 		    		
				<th>Branch Code</th>
				
				<th>Branch Name</th>
				
				<th>Branch Code Temp</th>
			
				<th>Operator Id</th>
				
				<th>Creation TS</th>				
			</tr>
			
			 <tr ng-repeat="branch in branchList | filter:filterBrCode ">
               	<td><input type="checkbox" value="{{  branch.branchId }}" ng-checked="selection.indexOf(branch.branchId) > -1" ng-click="toggleSelection( branch.branchId)" /></td>
               	<!-- <td>{{ branch.branchId }}</td> -->
                <td>{{ branch.branchCode }}</td>
               	<td>{{ branch.branchName }}</td>
               	<td>{{ branch.branchCodeTemp }}</td>
               	<td>{{ branch.userCode }}</td>
              	<td>{{ branch.creationTS }}</td>
              	<td>
					<input type="button" value="Branch Stock" ng-click="addBranchStock(branch)"/>
 				</td>  
            </tr>
            
            <tr>
              <td><input type="button" value="Submit" ng-click="verifyBranch()"></td>
            </tr> 
		</table>
</div>	

<div id ="branchCodeDB" ng-hide = "branchCodeDBFlag">
<input type="text" name="filterBranchCode" ng-model="filterBranchCode.branchCodeTemp" placeholder="Search by branch code">
 	   <table>
 	   		
 	   		<tr>
 	 			<th></th>
 	 				 		 	 		    	 		    		
				<th>Branch Code</th>
				
				<th>Branch Name</th>
				
				<th>Branch Code Temp</th>
				
			</tr>	
 	   		
			 <tr ng-repeat="branch in branchCodeList | filter:filterBranchCode ">
		 	  <td><input type="radio"  name="branchName"  class="branchCls"  value="{{ branch }}" ng-model="brCode" ng-click="saveBranchCode(branch)"></td>
              <td>{{branch.branchCode}}</td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchCodeTemp}}</td>
          </tr>
       </table>
 	     
</div>	

<div id ="creationTSDB" ng-hide = "creationTSDBFlag">
        <input type="text" name="filterCreationTS" ng-model="filterCreationTS" placeholder="Search">
 	   <table>
 	   		<tr ng-repeat="brnch in branchOldList | filter:filterCreationTS">
		 	  	<td><input type="radio"  name="brnchName"  class="brnchCls"  value="{{ brnch.creationTS }}" ng-model="$parent.creationTS" ng-click="saveCreationTS(brnch.creationTS)"></td>
              	<td>{{ brnch.creationTS | date : 'medium'}}</td>
          	</tr>
         </table>
</div>

<div id ="addBranchStockModel" ng-hide = "addBranchStockFlag">
 	  <form name="branchStockForm" ng-submit="saveBranchStock(hbon)">
	 	  	<div class="row" style="margin-top: 40px;">
	 	  	<div class="input-field col s6">
	        	<input type ="text" name ="hbon.operatorCode" ng-model="hbon.operatorCode" ng-click="selectOperatorCode()" required readOnly>
	      			<label>Operator Code : </label>
	      	</div>
	      	
	      	<div class="input-field col s6">
	        	<input type ="text" name ="hbon.disBranchCode" ng-model="hbon.disBranchCode" readonly required>
	      			<label>Dispatch Branch Code : </label>
	      	</div>
	      	
	      	<div class="input-field col s6">
	        	<input type ="number" name ="hbon.noCnmt" ng-model="hbon.noCnmt" required>
	      			<label>No. Of CNMT : </label>
	      	</div>
	      	<div class="input-field col s6">
	        	<input type ="number" name ="hbon.noChln" ng-model="hbon.noChln" required>
	      			<label>No. Of Challan : </label>
	      	</div>
	      	<div class="input-field col s6">
	        	<input type ="number" name ="hbon.noSedr" ng-model="hbon.noSedr" required>
	      			<label>No. Of SEDR : </label>
	      	</div>
	      	<div class="col s12 center">
	      	<input type="submit">
	      	</div>
	      	</div>
 	  </form>    
</div>	

<div id ="selectOperatorCodeModel" ng-hide = "operatorCodeFlag">
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Operator Code</th>	
				<th>User Name</th>		
				
			</tr>
			
			 <tr ng-repeat="operator in operatorList">
		 	  <td><input type="radio"  name="operatorName" value="{{ operator.userCode }}"  ng-click="selectOperator(operator.userCode)"></td>
              <td>{{ operator.userCode }}</td>
              <td>{{ operator.userName }}</td>
          </tr>
         </table>
</div>	

<div ng-hide = "editBranchDetailsDBFlag" >	
 	<form name="BranchForm" ng-submit="editBranchSubmit(BranchForm,branch)">	
	            <div class="input-field col s12">
	            <h3>Here's the details<span class="teal-text text-lighten-2"></span>:</h3>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="branchname" name ="branchName" ng-model="branch.branchName" value={{branch.branchName}} ng-minlength="3" ng-maxlength="40" ng-required="true"  ng-pattern="/[a-zA-Z]/">
	                <label>Branch Name</label>
	            </div>
	            
	             <div class="input-field col s12 m4 l3">
	                <input type ="text" name ="branchCode" ng-model="branch.branchCode" value={{branch.branchCode}} readonly required>
	                <label>Branch Code</label>
	            </div>
	            	            
	            <div class="input-field col s12 m4 l3">
	                <select name="isNCR" ng-model="branch.isNCR" value={{branch.isNCR}} required readonly>
                    	<option value="yes">Yes</option>
                    	<option value="no">No</option>   
                     </select>
        				<label for="isNCR">Branch IsNCR</label>
	            </div>
	            
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="branchAdd" name ="branchAdd" ng-model="branch.branchAdd" value={{branch.branchAdd}} ng-required="true" ng-minlength="3" ng-maxlength="255">	                
	                <label>Branch Address</label>
	            </div>
	           
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="branchCity" name ="branchCity" ng-model="branch.branchCity" value={{branch.branchCity}} ng-required="true" ng-minlength="3" ng-maxlength="40">
	                <label>Branch City</label>
	            </div>
	            
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="branchState" name ="branchState" ng-model="branch.branchState" value={{branch.branchState}} ng-required="true" ng-minlength="3" ng-maxlength="40">
	                <label>Branch State</label>
	            </div>
	            
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="branchPin" name ="branchPin" ng-model="branch.branchPin" value={{branch.branchPin}} ng-minlength="6" ng-maxlength="6" ng-required="true">
	                <label>Branch Pin</label>
	            </div>
	            
	            <div class="input-field col s12 m4 l3">
	  				{{branch.branchOpenDt | date :'shortDate'}}<input type ="date" name ="branchOpenDt" ng-model="branch.branchOpenDt" value={{branch.branchOpenDt}}>
	                <label>Branch Open Date:</label>
	            </div>  
	           
	            
	             <div class="input-field col s12 m4 l3">
	             <input type ="text" name ="branchStationName" ng-model="branchStationName" value={{branchStationName}} required readonly ng-click="openBranchStationCodeDB()" >
	                <input type ="hidden" name ="branchStationCode" ng-model="branch.branchStationCode" >
	                <label>Station Code</label>
	            </div>
	            
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" name ="branchDirector" ng-model="branch.branchDirector" value={{branch.branchDirector}} required readonly ng-click="openBranchDirectorDB()">
	                <label>Branch Director</label>
	            </div>
	           
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" name ="branchMarketingHD" ng-model="branch.branchMarketingHD" value={{branch.branchMarketingHD}} required readonly ng-click="openBranchMarketingHDDB()">
	                <label>Branch Marketing HD</label>
	            </div>
	            
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" name ="branchOutStandingHD" ng-model="branch.branchOutStandingHD" value={{branch.branchOutStandingHD}} required readonly ng-click="openBranchOutStandingHDDB()">
	                <label>Branch Outstanding HD</label>
	            </div>
	            
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" name ="branchMarketing" ng-model="branch.branchMarketing" value={{branch.branchMarketing}} required readonly  ng-click="openBranchMarketingDB()">
	                <label>Branch Marketing</label>
	           </div>
	            
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" name ="branchMngr" ng-model="branch.branchMngr" value={{branch.branchMngr}} required readonly ng-click="openBranchMngrDB()">
	                <label>Branch Manager</label>
	            </div>
	            
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" name ="branchCashier" ng-model="branch.branchCashier" value={{branch.branchCashier}} required readonly ng-click="openBranchCashierDB()">
	                <label>Branch Cashier</label>
	            </div>
	           
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" name ="branchTraffic" ng-model="branch.branchTraffic" value={{branch.branchTraffic}} required readonly ng-click="openBranchTrafficDB()">
	                <label>Branch Traffic</label>
	            </div>
	            
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" name ="branchAreaMngr" ng-model="branch.branchAreaMngr" value={{branch.branchAreaMngr}} required readonly ng-click="openBranchAreaMngrDB()">
	                <label>Branch Area Manager</label>
	            </div>
	            
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" name ="branchRegionalMngr" ng-model="branch.branchRegionalMngr" value={{branch.branchRegionalMngr}} required readonly ng-click="openBranchRegionalMngrDB()">
	                <label>Branch Regional Manager</label>
	            </div>
	           
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="branchPhone" name ="branchPhone" ng-model="branch.branchPhone" value={{branch.branchPhone}} ng-required="true"  ng-minlength="4" ng-maxlength="15">
	                <label>Branch Phone</label>
	            </div>
	            
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="branchFax" name ="branchFax" ng-model="branch.branchFax" value={{branch.branchFax}} ng-minlength="8" ng-maxlength="20">
	                <label>Branch Fax</label>
	            </div>
	            
	            <div class="input-field col s12 m4 l3">
	                <input type ="email" id="branchEmailId" name ="branchEmailId" ng-model="branch.branchEmailId" value={{branch.branchEmailId}} ng-required="true" ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" ng-maxlength="40">
	                <label>Branch Email Id</label>
	            </div>
	            
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="branchWebsite" name ="branchWebsite" ng-model="branch.branchWebsite" value={{branch.branchWebsite}} ng-minlength="3" ng-maxlength="255">
	                <label>Branch Website</label>
	           </div>
	           
	           <div class="input-field col s12 m4 l3">
	                <select name="isOpen" ng-model="branch.isOpen" value={{branch.isOpen}} required readonly>
                    	<option value="yes">Yes</option>
                    	<option value="no">No</option>   
                     </select>
        				<label for="isOpen">Branch Open</label>
	            </div>
	           
				 <div class="input-field col s12 m4 l3" ng-hide="showCloseDateFlag">
	            	{{branch.branchCloseDt  | date:'shortDate'}} 
	            	<label>Branch Close Date</label>
	            </div>

	           <div class="col s12 center">
					<input type="submit" value="Update">
			   </div>
	</form>
		
</div>
</div>
</div>
</div>

<div id ="branchStationCodeDB" ng-hide="branchStationCodeDBFlag">
 	  <input type="text" name="filterbox" ng-model="filterbox.stnCode" placeholder="Search by Station Code">
 	  <table>
 	 		<tr>
 	 		    <th></th>				
				<th>Station Code</th>
				<th>Station Name</th>
			</tr>
 	  	  
		  <tr ng-repeat="station in stationList | filter:filterbox">
		 	  <td><input type="radio"  name="stationName"  class="stationCls"  value="{{ station }}" ng-model="brStationCode" ng-click="saveBranchStationCode(station)"></td>
              <td>{{station.stnCode}}</td>
              <td>{{station.stnName}}</td>
          </tr>
         </table>
         
	</div>
	
	
	<div id ="branchDirectorDB" ng-hide="branchDirectorDBFlag">
 	 <input type="text" name="filterBrDirector" ng-model="filterBrDirector.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp</th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchDirector in employeeList | filter:filterBrDirector">
		 	  <td><input type="radio"  name="empBranchDirectorName" class="empBranchDirectorCls" value="{{ empBranchDirector }}" ng-model="brDirector" ng-click="saveBranchDirector(empBranchDirector)"></td>
              <td>{{ empBranchDirector.empCode }}</td>
              <td>{{ empBranchDirector.empName }}</td>
              <td>{{ empBranchDirector.branchCode }}</td>
              <td>{{ empBranchDirector.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchMarketingHDDB" ng-hide="branchMarketingHDDBFlag">
	<input type="text" name="filterBrMarketingHD" ng-model="filterBrMarketingHD.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp</th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchMarketingHD in employeeList | filter:filterBrMarketingHD">
		 	  <td><input type="radio"  name="empBranchMarketingHDName" class="empBranchMarketingHDCls" value="{{ empBranchMarketingHD }}" ng-model="brMarketingHD" ng-click="saveBranchMarketingHD(empBranchMarketingHD)"></td> 
              <td>{{ empBranchMarketingHD.empCode }}</td>
              <td>{{ empBranchMarketingHD.empName }}</td>
              <td>{{ empBranchMarketingHD.branchCode }}</td>
              <td>{{ empBranchMarketingHD.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchOutStandingHDDB" ng-hide="branchOutStandingHDDBFlag">
	<input type="text" name="filterBrOutStandingHD" ng-model="filterBrOutStandingHD.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp</th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchOutStandingHD in employeeList | filter:filterBrOutStandingHD">
		 	  <td><input type="radio"  name="empBranchOutStandingHDName" class="empBranchOutStandingHDCls" value="{{ empBranchOutStandingHD }}" ng-model="brOutStandingHD" ng-click="saveBranchOutStandingHD(empBranchOutStandingHD)"></td>
              <td>{{ empBranchOutStandingHD.empCode }}</td>
              <td>{{ empBranchOutStandingHD.empName }}</td>
              <td>{{ empBranchOutStandingHD.branchCode }}</td>
              <td>{{ empBranchOutStandingHD.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchMarketingDB" ng-hide="branchMarketingDBFlag">
	<input type="text" name="filterBrMarketing" ng-model="filterBrMarketing.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp</th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchMarketing in employeeList | filter:filterBrMarketing">
		 	  <td><input type="radio"  name="empBranchMarketingName" class="empBranchMarketingCls" value="{{ empBranchMarketing }}" ng-model="brMarketing" ng-click="saveBranchMarketing(empBranchMarketing)"></td>
              <td>{{ empBranchMarketing.empCode }}</td>
              <td>{{ empBranchMarketing.empName }}</td>
              <td>{{ empBranchMarketing.branchCode }}</td>
              <td>{{ empBranchMarketing.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchMngrDB" ng-hide="branchMngrDBFlag">
	<input type="text" name="filterBrMngr" ng-model="filterBrMngr.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp</th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchMngr in employeeList | filter:filterBrMngr">
		 	  <td><input type="radio"  name="empBranchMngrName" class="empBranchMngrCls" value="{{ empBranchMngr }}" ng-model="brMngr" ng-click="saveBranchMngr(empBranchMngr)"></td>
              <td>{{ empBranchMngr.empCode }}</td>
              <td>{{ empBranchMngr.empName }}</td>
              <td>{{ empBranchMngr.branchCode }}</td>
              <td>{{ empBranchMngr.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchCashierDB" ng-hide="branchCashierDBFlag">
	<input type="text" name="filterBrCashier" ng-model="filterBrCashier.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp</th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchCashier in employeeList | filter:filterBrCashier">
		 	  <td><input type="radio"  name="empBranchCashierName"  class="empBranchCashierCls" value="{{ empBranchCashier }}" ng-model="brCashier" ng-click="saveBranchCashier(empBranchCashier)"></td>
              <td>{{ empBranchCashier.empCode }}</td>
              <td>{{ empBranchCashier.empName }}</td>
              <td>{{ empBranchCashier.branchCode }}</td>
              <td>{{ empBranchCashier.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchTrafficDB" ng-hide="branchTrafficDBFlag">
	<input type="text" name="filterBrTraffic" ng-model="filterBrTraffic.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp</th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchTraffic in employeeList | filter:filterBrTraffic">
		 	  <td><input type="radio"  name="empBranchTrafficName" class="empBranchTrafficCls" value="{{ empBranchTraffic }}" ng-model="brTraffic" ng-click="saveBranchTraffic(empBranchTraffic)"></td>
              <td>{{ empBranchTraffic.empCode }}</td>
              <td>{{ empBranchTraffic.empName }}</td>
              <td>{{ empBranchTraffic.branchCode }}</td>
              <td>{{ empBranchTraffic.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchAreaMngrDB" ng-hide="branchAreaMngrDBFlag">
	<input type="text" name="filterBrAreaMngr" ng-model="filterBrAreaMngr.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp</th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchAreaMngr in employeeList | filter:filterBrAreaMngr">
		 	  <td><input type="radio"  name="empBranchAreaMngrName" class="empBranchAreaMngrCls" value="{{ empBranchAreaMngr }}" ng-model="brAreaMngr" ng-click="saveBranchAreaMngr(empBranchAreaMngr)"></td>
              <td>{{ empBranchAreaMngr.empCode }}</td>
              <td>{{ empBranchAreaMngr.empName }}</td>
              <td>{{ empBranchAreaMngr.branchCode }}</td>
              <td>{{ empBranchAreaMngr.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchRegionalMngrDB" ng-hide="branchRegionalMngrDBFlag">
	<input type="text" name="filterBrRegionalMngr" ng-model="filterBrRegionalMngr.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp</th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchRegionalMngr in employeeList | filter:filterBrRegionalMngr">
		 	  <td><input type="radio"  name="empBranchRegionalMngrName" class="empBranchRegionalMngrCls" value="{{ empBranchRegionalMngr }}" ng-model="brRegionalMngr" ng-click="saveBranchRegionalMngr(empBranchRegionalMngr)"></td>
              <td>{{ empBranchRegionalMngr.empCode }}</td>
              <td>{{ empBranchRegionalMngr.empName }}</td>
              <td>{{ empBranchRegionalMngr.branchCode }}</td>
              <td>{{ empBranchRegionalMngr.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>



<div id="viewBranchDetailsDB" ng-hide="viewBranchDetailsFlag">
	<div class="row">
	<div class="col s12 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
		<h4>Here's the details of <span class="teal-text text-lighten-2">{{branch.branchName}}</span> branch:</h4>
		<table class="table-hover table-bordered table-condensed">
				
				<tr>
	                <td>Name:</td>
	                <td>{{branch.branchName}}</td>
	            </tr>
	            
	            <tr>
	                <td>IsNCR:</td>
	                <td>{{branch.isNCR}}</td>
	            </tr>
	            
	            <tr>
	                <td>Address:</td>
	                <td>{{branch.branchAdd}}</td>
	            </tr>
	            <tr>
	                <td>City:</td>
	                <td>{{branch.branchCity}}</td>
	            </tr>
	            <tr>
	                <td>State:</td>
	                <td>{{branch.branchState}}</td>
	            </tr>
	            <tr>
	                <td>Pin:</td>
	                <td>{{branch.branchPin}}</td>
	            </tr>
	            <tr>
	                <td>Open Date:</td>
	                <td>{{branch.branchOpenDt  | date:'medium'}}</td>
	            </tr>
	            <tr>
	                <td>Station Code:</td>
	                <td>{{branch.branchStationCode}}</td>
	            </tr>
	            <tr>
	                <td>Director:</td>
	                <td>{{branch.branchDirector}}</td>
	                
	            </tr>
	            <tr>
	                <td>Marketing HD:</td>
	                <td>{{branch.branchMarketingHD}}</td>
	            </tr>
	            <tr>
	                <td>Outstanding HD:</td>
	                <td>{{branch.branchOutStandingHD}}</td>
	            </tr>
	            <tr>
	                <td>Marketing:</td>
	                <td>{{branch.branchMarketing}}</td>
	            </tr>
	            <tr>
	                <td>Manager:</td>
	                <td>{{branch.branchMngr}}</td>
	            </tr>
	            <tr>
	                <td>Cashier:</td>
	                <td>{{branch.branchCashier}}</td>
	            </tr>
	            <tr>
	                <td>Traffic:</td>
	                <td>{{branch.branchTraffic}}</td>
	            </tr>
	            <tr>
	                <td>Area Manager:</td>
	                <td>{{branch.branchAreaMngr}}</td>
	            </tr>
	            <tr>
	                <td>Regional Manager:</td>
	                <td>{{branch.branchRegionalMngr}}</td>
	            </tr>
	            <tr>
	                <td>Phone:</td>
	                <td>{{branch.branchPhone}}</td>
	            </tr>
	            <tr>
	                <td>Fax:</td>
	                <td>{{branch.branchFax}}</td>
	            </tr>
	            
	            <tr>
	                <td>Email Id:</td>
	                <td>{{branch.branchEmailId}}</td>
	            </tr>
	            
	            <tr>
	                <td>Website:</td>
	                <td>{{branch.branchWebsite}}</td>
	            </tr>
	            
	             <tr>
	                <td>Branch IsOpen: </td>
	                <td>{{branch.isOpen}}</td>
	            </tr>
	            
	            <tr ng-hide="closeDateFlag">
	                <td>Branch Close Date:</td>
	                <td>{{branch.branchCloseDt  | date:'medium'}}</td>
	            </tr>
	            
	          
	          
	   </table>
	   
<input type="button" value="Save" ng-click="saveEdittedBranch(branch)">
<input type="button" value="Cancel" ng-click="closeViewBranchDetailsDB()">
		
</div>	
</div>

</div>

<div ng-hide="viewBranchOldDetailsFlag">
	<div class="row">
	<div class="col s12 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
		<h4>Modification details <span class="teal-text text-lighten-2"></span></h4>
		<table class="table-hover table-bordered table-condensed">
				
				<tr>
	                <td>Name:</td>
	                <td>{{branchOld.branchName}}</td>
	            </tr>
	            
	            <tr>
	                <td>IsNCR:</td>
	                <td>{{branchOld.isNCR}}</td>
	            </tr>
	            
	            <tr>
	                <td>Address:</td>
	                <td>{{branchOld.branchAdd}}</td>
	            </tr>
	            <tr>
	                <td>City:</td>
	                <td>{{branchOld.branchCity}}</td>
	            </tr>
	            <tr>
	                <td>State:</td>
	                <td>{{branchOld.branchState}}</td>
	            </tr>
	            <tr>
	                <td>Pin:</td>
	                <td>{{branchOld.branchPin}}</td>
	            </tr>
	            <tr>
	                <td>Open Date:</td>
	                <td>{{branchOld.branchOpenDt  | date:'medium'}}</td>
	            </tr>
	            <tr>
	                <td>Station Code:</td>
	                <td>{{branchOld.branchStationCode}}</td>
	            </tr>
	            <tr>
	                <td>Director:</td>
	                <td>{{branchOld.branchDirector}}</td>
	                
	            </tr>
	            <tr>
	                <td>Marketing HD:</td>
	                <td>{{branchOld.branchMarketingHD}}</td>
	            </tr>
	            <tr>
	                <td>Outstanding HD:</td>
	                <td>{{branchOld.branchOutStandingHD}}</td>
	            </tr>
	            <tr>
	                <td>Marketing:</td>
	                <td>{{branchOld.branchMarketing}}</td>
	            </tr>
	            <tr>
	                <td>Manager:</td>
	                <td>{{branchOld.branchMngr}}</td>
	            </tr>
	            <tr>
	                <td>Cashier:</td>
	                <td>{{branchOld.branchCashier}}</td>
	            </tr>
	            <tr>
	                <td>Traffic:</td>
	                <td>{{branchOld.branchTraffic}}</td>
	            </tr>
	            <tr>
	                <td>Area Manager:</td>
	                <td>{{branchOld.branchAreaMngr}}</td>
	            </tr>
	            <tr>
	                <td>Regional Manager:</td>
	                <td>{{branchOld.branchRegionalMngr}}</td>
	            </tr>
	            <tr>
	                <td>Phone:</td>
	                <td>{{branchOld.branchPhone}}</td>
	            </tr>
	            <tr>
	                <td>Fax:</td>
	                <td>{{branchOld.branchFax}}</td>
	            </tr>
	            
	            <tr>
	                <td>Email Id:</td>
	                <td>{{branchOld.branchEmailId}}</td>
	            </tr>
	            
	            <tr>
	                <td>Website:</td>
	                <td>{{branchOld.branchWebsite}}</td>
	            </tr>
	            
	             <tr>
	                <td>Branch IsOpen: </td>
	                <td>{{branchOld .isOpen}}</td>
	            </tr>
	            
	            <tr>
	                <td>Branch Close Date:</td>
	                <td>{{branchOld.branchCloseDt  | date:'medium'}}</td>
	            </tr>
	            
	             
	            <tr>
	                <td>Creation Timestamp:</td>
	                <td>{{branchOld.creationTS}}</td>
	            </tr>
	            
	    </table>

</div>	
</div>

</div>
</div>
 
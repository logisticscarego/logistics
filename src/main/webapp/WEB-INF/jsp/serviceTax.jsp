<div ng-show="superAdminLogin">
<title>Service Tax</title>
<div class="row">
	<div class="col s2 hide-on-med-and-down"> &nbsp; </div>
		<form class="col s8 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="ServiceTaxForm" ng-submit="submit(ServiceTaxForm,servicetax)">
			<div class="row">
      			<div class="input-field col s6">
        			<input class="validate" type ="number" id="stTaxableRt" name ="stTaxableRt" ng-model="servicetax.stTaxableRt" ng-required="true" min="0" max="100" step="0.01">
        			<label for="name">Taxable Rate(%)</label>	
      			</div>
      			<div class="input-field col s6">
        			<input class="validate"  type ="number" id="stSerTaxRt" name ="stSerTaxRt" ng-model="servicetax.stSerTaxRt" ng-required="true" min="0" max="100" step="0.01">
        			<label for="district">Service Tax Rate(%)</label>	
      			</div>
      		</div>
      		<div class="row">
      			<div class="input-field col s6">
        			<input class="validate" type ="number" id="stEduCessRt" name ="stEduCessRt" ng-model="servicetax.stEduCessRt" ng-required="true" min="0" max="100" step="0.01">
        			<label for="code">EduCess Rate(%)</label>	
      			</div>
      			<div class="input-field col s6">
        			<input class="validate"  type ="number" id="stHscCessRt" name ="stHscCessRt" ng-model="servicetax.stHscCessRt" ng-required="true" min="0" max="100" step="0.01">
        			<label for="pin">HScCess Rate(%)</label>	
      			</div>
      			<div class="input-field col s6">
        			<input class="validate"  type ="number" id="stSwhBHCessRt" name ="stSwhBHCessRt" ng-model="servicetax.stSwhBHCessRt" ng-required="true" min="0" max="100" step="0.01">
        			<label for="pin">Swachh Bharat Cess(%)</label>	
      			</div>
      		</div>
      		<div class="row">
      			 	<div class="input-field col s12 center">
      			 		<input class="btn waves-effect waves-light" type="submit" value="Submit">
      			 	</div>
      			</div>
			</form>
			<div class="col s2"> &nbsp; </div>
   </div>
   
<div id="viewServiceTaxDB" ng-hide="viewServiceTaxFlag">

<div class="row">
	
	<div class="col s12 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
				<h4>Here's the details of Service Tax <span class="teal-text text-lighten-2"></span>:</h4>
		<table class="table-bordered table-hover table-condensed">
	            
	            <tr>
	                <td>Taxable Rate(%):</td>
	                <td>{{servicetax.stTaxableRt}}</td>
	            </tr>
	            
	            <tr>
	                <td>Service Tax Rate(%):</td>
	                <td>{{servicetax.stSerTaxRt}}</td>
	            </tr>
	            
	            <tr>
	                <td>EduCess Rate(%):</td>
	                <td>{{servicetax.stEduCessRt}}</td>
	            </tr>
	            <tr>
	                <td>HScCess Rate(%):</td>
	                <td>{{servicetax.stHscCessRt}}</td>
	            </tr>
	            <tr>
	                <td>Swachh Bharat Cess Rate(%):</td>
	                <td>{{servicetax.stSwhBHCessRt}}</td>
	            </tr>
	      </table>
	      
<input type="button" value="Save" ng-click="saveServiceTax(servicetax)">
<input type="button" value="Cancel" ng-click="closeViewServiceTaxDB()">
	      
	  </div>
	</div>         
</div>
</div>

<div ng-show="operatorLogin || superAdminLogin">
	<title>Fund Allocation</title>
	<div class="row">

		<form ng-submit="saveFundAllocation(fundAllocation)"
			class="col s12 card"
			style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">

			<div class="row">
				<div class="input-field col s2">
					<select ng-model="fundAllocation.transactionType"
						ng-init="fundAllocation.transactionType='I'"
						ng-change="changeTransactionType()" required="required">
						<option value="I">HDFC BANK</option>
						<option value="N">NEFT</option>
						<option value="R">RTGS</option>
<!-- 						<option value="ATM">ATM</option> -->
					</select> <label>Transaction Type</label>
				</div>


				<div class="input-field col s4">
					<input type="button" value="Add Cnmt and Challan Details"
						ng-click="openCnmtChlnForm()">

				</div>

				<div class="input-field col s2">
					<select ng-model="fundAllocation.payTo" ng-change="changePayTo()"
						required="required" ng-disabled="payToDisableFlag">
						<!-- ng-init="fundAllocation.payTo='owner'" -->
						<option value="owner" ng-hide="staffCodeRequireFlag">Owner</option>
						<option value="broker" ng-hide="staffCodeRequireFlag">Broker</option>
						<option value="others" ng-show="staffCodeRequireFlag">Others</option>
						<option value="unRegistered" ng-hide=true>unRegistered</option>
					</select> <label>Pay To</label>
				</div>

			</div>

			<div class="row">

				<div class="input-field col s4" ng-show="staffCodeRequireFlag">
					<input class="validate" type="text"
						ng-model="fundAllocation.staffCode" ng-keyup="getStaffCode()"
						ng-blur="checkLength()" ng-required="staffCodeRequireFlag">
					<label>Staff Code</label>
				</div>

				<div class="input-field col s4">
					<input class="validate" type="text"
						ng-model="fundAllocation.beneficiaryCode" ng-click="readBranch()"
						readonly="readonly" required="required"> <label>Beneficiary
						Code</label>
				</div>

				<!-- <div class="input-field col s4">
					<input type="text" ng-model="fundAllocation.beneficiaryAccountNo"
						ng-readonly="readonlyFlag" required="required"> <label>Beneficiary
						Account Number</label>
				</div> -->
				<div class="input-field col s4">
					<input type="text" ng-model="fundAllocation.beneficiaryAccountNo"
						 required="required" readonly> <label>Beneficiary
						Account Number</label>
				</div>
			</div>

			<div class="row">

				<div class="input-field col s3">
					<input type="number" ng-min="0"
						ng-model="fundAllocation.instrumentAmount" required="required">
					<label>Instrument Amount</label>
				</div>
				
				<div class="input-field col s3">
					<input type="number" ng-min="0"
						ng-model="fundAllocation.tdsAmt" required="required">
					<label>TDS Amount</label>
				</div>

				<!-- <div class="input-field col s4">
					<input type="text" ng-model="fundAllocation.beneficiaryName"
						ng-change="custRefNo(fundAllocation.beneficiaryName)"
						ng-readonly="readonlyFlag" required="required"> <label>Beneficiary
						Name</label>
				</div> -->
				
				<div class="input-field col s3">
					<input type="text" ng-model="fundAllocation.beneficiaryName"
						ng-change="custRefNo(fundAllocation.beneficiaryName)"
						 required="required" readonly> <label>Beneficiary
						Name</label>
				</div>

				<div class="input-field col s3">
					<input class="validate" type="text"
						ng-model="fundAllocation.beneAddress1"
						ng-change="beneAddress1(fundAllocation.beneAddress1)"
						required="required">
					<p style="color: red;">{{bene1}}</p>
					<label>Bene Address 1</label>
				</div>
			</div>

			<div class="row">

				<div class="input-field col s4">
					<input type="text" ng-model="fundAllocation.beneAddress2"
						ng-change="beneAddress2(fundAllocation.beneAddress2)">
					<p style="color: red;">{{bene2}}</p>
					<label>Bene Address 2</label>
				</div>
				<div class="input-field col s4">
					<input type="text" ng-model="fundAllocation.beneAddress3"
						ng-change="beneAddress3(fundAllocation.beneAddress3)">
					<p style="color: red;">{{bene3}}</p>
					<label>Bene Address 3</label>
				</div>

				<div class="input-field col s4">
					<input class="validate" type="text"
						ng-model="fundAllocation.beneAddress4"
						ng-change="beneAddress4(fundAllocation.beneAddress4)">
					<p style="color: red;">{{bene4}}</p>
					<label>Bene Address 4</label>
				</div>

			</div>

			<div class="row">
				<div class="input-field col s4">
					<input type="text" ng-model="fundAllocation.beneAddress5"
						ng-change="beneAddress5(fundAllocation.beneAddress5)">
					<p style="color: red;">{{bene5}}</p>
					<label>Bene Address 5</label>
				</div>
				<div class="input-field col s4">
					<input type="text" ng-model="fundAllocation.customerRefNo"
						readonly="readonly"> <label>Customer Reference No</label>
				</div>

				<div class="input-field col s4">
					<input class="validate" type="text"
						ng-model="fundAllocation.paymentDetail1"> <label>Payment
						Detail 1</label>
				</div>
			</div>

			<div class="row">

				<div class="input-field col s4">
					<input type="text" ng-model="fundAllocation.paymentDetail2">
					<label>Payment Detail 2</label>
				</div>
				<div class="input-field col s4">
					<input type="text" ng-model="fundAllocation.paymentDetail3">
					<label>Payment Detail 3</label>
				</div>

				<div class="input-field col s4">
					<input class="validate" type="text"
						ng-model="fundAllocation.paymentDetail4"> <label>Payment
						Detail 4</label>
				</div>

			</div>

			<div class="row">
				<div class="input-field col s4">
					<input type="text" ng-model="fundAllocation.paymentDetail5">
					<label>Payment Detail 5</label>
				</div>
				<div class="input-field col s4">
					<input type="text" ng-model="fundAllocation.paymentDetail6">
					<label>Payment Detail 6</label>
				</div>

				<div class="input-field col s4">
					<input class="validate" type="text"
						ng-model="fundAllocation.paymentDetail7"> <label>Payment
						Detail 7</label>
				</div>

			</div>

			<div class="row">
				<div class="input-field col s4">
					<input type="text" ng-model="fundAllocation.micrNo"> <label>MICR
						No</label>
				</div>

				<!-- <div class="input-field col s4">
					<input class="validate" type="text"
						ng-model="fundAllocation.ifscCode"
						ng-change="chkIfscCode(fundAllocation.ifscCode)"
						ng-readonly="readonlyFlag" required="required">
					<p style="color: red;">{{ifscMsg}}</p>
					<label>IFSC Code</label>
				</div> -->
				
				<div class="input-field col s4">
					<input class="validate" type="text"
						ng-model="fundAllocation.ifscCode"
						ng-change="chkIfscCode(fundAllocation.ifscCode)"
						 required="required" readonly>
					<p style="color: red;">{{ifscMsg}}</p>
					<label>IFSC Code</label>
				</div>

				<!-- <div class="input-field col s4">
					<input type="text" ng-model="fundAllocation.beneBnkName"
						ng-readonly="readonlyFlag" required="required"> <label>Bene
						Bank Name</label>
				</div> -->
				<div class="input-field col s4">
					<input type="text" ng-model="fundAllocation.beneBnkName"
						 required="required" readonly> <label>Bene
						Bank Name</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s4">
					<input type="text" ng-model="fundAllocation.beneBnkBranchName"
						required="required"> <label>Bene Bank Branch Name</label>
				</div>

				<div class="input-field col s4">
					<input class="validate" type="text"
						ng-model="fundAllocation.beneficiaryEmailId" required="required">
					<label> Branch Email Id</label>
				</div>


			</div>
			<table
				style="border-collapse: collapse; border: 1px solid black; width: 100%">
				<tr ng-repeat="alDetails in allocationDetailList track by $index">
					<td>{{alDetails.cnmtCode}}</td>
					<td>{{alDetails.challanNo}}</td>
					<td>{{alDetails.chlnDt}}</td>
					<td>{{alDetails.fromStCode}}</td>
					<td>{{alDetails.toStCode}}</td>
					<td>{{alDetails.lryAmount}}</td>
					<td>{{alDetails.vehicleNo}}</td>
				</tr>
			</table>


			<div class="row">
				<div class="input-field col s12 center">
					<input id="sbmtId" type="submit" value="Submit">
				</div>
			</div>

		</form>
	</div>

	<div id="fundAllocationDetailDB" ng-hide="fundAllocationDetail">
		<div class="row">

			<form
				ng-submit="pushFundAllDetails(cnmtCode,challanNo,chlnDt,fromStCode,toStCode,lryAmount,vehicleNo,brkCode,ownCode,lryRate,frtAmount)"
				class="col s12 card"
				style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">

				<div class="row">
					<div class="input-field col s4">
						<input type="text" ng-model="cnmtCode" id="cnmtCode"
							name="cnmtCode" ng-blur="checkCnmtCode(cnmtCode)" required> <label>Cnmt
							No</label>
					</div>

					<div class="input-field col s4">
						<input class="validate" type="text" ng-model="challanNo"
							ng-blur="checkChlnCode(challanNo)" required> <label>Challan
							No</label>
					</div>

					<div class="input-field col s4">
						<input type="date" ng-model="chlnDt" required="required" ng-readonly="brkReadonlyFlag">
						<label>Challan Date</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s4">
						<input type="text" ng-model="fromStCode"
							ng-keyup="getStationFrom($event.keyCode)" required="required" ng-readonly="brkReadonlyFlag">
						<label>From Station</label>
					</div>

					<div class="input-field col s4">
						<input class="validate" type="text" ng-model="toStCode"
							ng-keyup="getStationTo($event.keyCode)" required="required" ng-readonly="brkReadonlyFlag">
						<label>To Station</label>
					</div>

					<div class="input-field col s4">
						<input type="text" ng-model="vehicleNo"
							ng-blur="openVehicleDB()" required="required" ng-readonly="brkReadonlyFlag"> <label>Vehicle
							No</label>
					</div>
					


				</div>

				<div class="row">


					<div class="input-field col s4">
						<input type="text" ng-model="ownCode"  readonly>
						<label>Owner</label>
					</div>

					<div class="input-field col s4">
						<input type="text" ng-model="brkCode"  ng-change="getBrokerByName(brkCode)"
							ng-readonly="brkReadonlyFlag"> <label>Broker</label>
					</div>

				</div>
				
				<div class="row">

					<div class="input-field col s4">
						<input type="number" ng-model="frtAmount" required="required">
						<label>Freight</label>
					</div>
					
					<div class="input-field col s4">
						<input type="text" ng-model="lryRate" required="required">
						<label>Rate</label>
					</div>
					
					<div class="input-field col s4">
						<input type="number" ng-model="lryAmount" required="required">
						<label>Advance</label>
					</div>
					
					</div>
				<div class="input-field col s12 center">
					<input type="submit" value="Submit">

				</div>

			</form>
		</div>
	</div>

	<div id="vehDB" ng-hide="vehDBFlag">

		<input type="text" name="filterVehCode" ng-model="filterVehCode"
			placeholder="Search....">

		<table>
			<tr>
				<th></th>

				<th>Vehicle No</th>
			</tr>
			<tr ng-repeat="vehicle in vehList | filter:filterVehCode">
				<td><input type="radio" name="vehicleName" id="vehicleId"
					value="{{ vehicle }}" ng-model="vehicleCode"
					ng-click="selectVeh(vehicle)"></td>
				<td>{{ vehicle }}</td>
			</tr>
		</table>
	</div>


	<!-- <div id="vehAdvReqDB" ng-hide="vehAdvReqDBFlag">
		<table class="table-hover table-bordered table-bordered">
				<tr>
					<td>Date</td>
					<td>{{chlnDt}}</td>
				</tr>
				<tr>
					<td>From</td>
					<td>{{fromStCode}}</td>
				</tr>
				<tr>
					<td>To</td>
					<td>{{toStCode}}</td>
				</tr>
				<tr>
					<td>Lorry No.</td>
					<td>{{vehicleNo}}</td>
				</tr> 
		</table>
		<input type="button" value="Send" id="sendId" ng-click="vehAdvRequest()"
				 /> 
	</div> -->


	<div id="fromStationDB" ng-hide="fromStationDBFlag">
		<input type="text" name="filterFromStation"
			ng-model="filterFromStation" placeholder="Search by Code">
		<table>
			<tr>
				<th></th>

				<th>Station Code</th>

				<th>Station Name</th>

				<th>Station District</th>
			</tr>


			<tr ng-repeat="station in stationList | filter:filterFromStation">
				<td><input type="radio" name="stnName" id="stnName"
					class="stnName" value="{{ station }}" ng-model="frmStationCode"
					ng-click="saveFrmStnCode(station)"></td>
				<td>{{ station.stnCode }}</td>
				<td>{{ station.stnName }}</td>
				<td>{{ station.stnDistrict }}</td>
			</tr>
		</table>
	</div>


	<div id="toStationDB" ng-hide="toStationDBFlag">
		<input type="text" name="filterToStation" ng-model="filterToStation"
			placeholder="Search by Code">
		<table>
			<tr>
				<th></th>

				<th>Station Code</th>

				<th>Station Name</th>

				<th>Station District</th>
			</tr>


			<tr ng-repeat="station in stationList | filter:filterToStation">
				<td><input type="radio" name="stnName" id="stnName"
					class="stnName" value="{{ station }}" ng-model="toStationCode"
					ng-click="saveToStnCode(station)"></td>
				<td>{{ station.stnCode }}</td>
				<td>{{ station.stnName }}</td>
				<td>{{ station.stnDistrict }}</td>
			</tr>
		</table>
	</div>


<div id="brokerDB" ng-hide="brokerDBFlag">
		<input type="text" name="filterBroker" ng-model="filterBroker"
			placeholder="Search by Code">
		<table>
			<tr>
				<th></th>

				<th>Broker Code</th>

				<th>Broker Name</th>

				<th>Broker PAN No.</th>
			</tr>


			<tr ng-repeat="brk in brkList | filter:filterBroker">
				<td><input type="radio" name="brokerName" id="brokerName"
					class="brokerName" value="{{ brk }}" ng-model="brokerCode"
					ng-click="saveBrkCode(brk)"></td>
				<td>{{ brk.brkCode }}</td>
				<td>{{ brk.brkName }}</td>
				<td>{{ brk.brkPanNo }}</td>
			</tr>
		</table>
	</div>



	<div id="branchDB" ng-hide="branchDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1"
			placeholder="Search">
		<table>
			<tr>
				<th></th>
				<th>Branch Name</th>
			</tr>
			<tr ng-repeat="branch in branchList | filter:filterTextbox1">
				<td><input type="radio" name="branch" value="{{ branch }}"
					ng-click="saveBranch(branch)"></td>
				<td>{{branch.branchName}}</td>
			</tr>
		</table>
	</div>

	<div id="detailsDB" ng-hide="detailsDBFlag">
		<div class="col s6 card"
			style="align: center; margin-top: 0px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<table class="table-hover table-bordered table-bordered">
				<tr>
					<td>Transaction Type</td>
					<td>{{fund.transactionType}}</td>
				</tr>
				<tr>
					<td>Beneficiary Code</td>
					<td>{{fund.beneficiaryCode}}</td>
				</tr>
				<tr>
					<td>Beneficiary Account Number</td>
					<td>{{fund.beneficiaryAccountNo}}</td>
				</tr>
				<tr>
					<td>Instrument Amount</td>
					<td>{{fund.instrumentAmount}}</td>
				</tr>
				<tr>
					<td>Beneficiary Name</td>
					<td>{{fund.beneficiaryName}}</td>
				</tr>
				<tr>
					<td>Bene Address 1</td>
					<td>{{fund.beneAddress1}}</td>
				</tr>
				<tr>
					<td>Customer Reference No</td>
					<td>{{fund.customerRefNo}}</td>
				</tr>
				<tr>
					<td>IFSC Code</td>
					<td>{{fund.ifscCode}}</td>
				</tr>
				<tr>
					<td>Bene Bank Name</td>
					<td>{{fund.beneBnkName}}</td>
				</tr>
				<tr>
					<td>Bene Bank Branch Name</td>
					<td>{{fund.beneBnkBranchName}}</td>
				</tr>
				<tr>
					<td>Branch Email Id</td>
					<td>{{fund.beneficiaryEmailId}}</td>
				</tr>
			</table>
			<input type="button" value="Save" id="saveId" ng-click="saveFA()"
				disabled="disabled" /> <input type="button" value="Cancel"
				ng-click="back()" />

		</div>
	</div>
</div>
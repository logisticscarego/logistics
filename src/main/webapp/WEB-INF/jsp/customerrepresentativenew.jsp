<div ng-if="operatorLogin || superAdminLogin">
<title>Add Customer Representative</title>

<div class="row">
	<div class="col s3 hide-on-med-and-down">&nbsp;</div>
	<form class="col s6 card"
		style="align: center; margin-top: 40px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
		name="NewCustomerRepForm" form
		ng-submit="nextAddNewCR(NewCustomerRepForm,custrep)">
		<div class="row">
			<div class="input-field col s12 m4 l6">
				<input type="text" id="custCode" name="custCode"
					ng-model="custCodeTemp" ng-click="OpenCustomerCodeDB()"
					required readonly> 
					<input class="validate" type ="hidden" name ="branchCode" ng-model="custrep.custCode" >
					<label>Customer Code</label>
			</div>
			<div class="input-field col s12 m6 l6">
				<input type="text" id="custRefNo" name="custRefNo"
					ng-model="custrep.custRefNo" ng-minlength="3" ng-maxlength="40"
					ng-required="true"> <label>Customer Reference
					Number</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12">
				<input type="text" id="crName" name="crName"
					ng-model="custrep.crName" ng-minlength="3" ng-maxlength="40"
					ng-required="true"> <label>Name</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12 m4 l4">
				<input type="text" id="crDesignation" name="crDesignation"
					ng-model="custrep.crDesignation" ng-minlength="3" ng-maxlength="40"
					ng-model-options="{ debounce: 200 }"
					ng-keyup="getCustDesigList(custrep)" ng-blur="fillCustDesigVal()"
					ng-required="true"> <label>Designation</label>
			</div>
			<div class="input-field col s12 m4 l4">
				<input type="text" id="crMobileNo" name="crMobileNo"
					ng-model="custrep.crMobileNo" ng-minlength="4" ng-maxlength="15"
					ng-required="true"> <label>Mobile No</label>
			</div>
			<div class="input-field col s12 m4 l4">
				<input type="email" name="crEmailId" ng-model="custrep.crEmailId"
					ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/"
					ng-maxlength="40" ng-required="true"> <label>Email
					Id</label>
			</div>

		</div>

		<div class="row">
			<div class="input-field col s12 center">
				<input class="btn waves-effect waves-light" type="submit" id="saveId"
					value="Submit">
			</div>
		</div>

	</form>
</div>
</div>
<%-- 		<form name="NewCustomerRepForm" form ng-submit="nextAddNewCR(NewCustomerRepForm,custrep)">
	
		<table border="0">
		
		<tr>
			<td>Customer Code: *</td>
			<td><input type ="text" id="custCode" name ="custCode" ng-model="custrep.custCode" ng-click="OpenCustomerCodeDB()" required readonly></td>
		</tr>

		<tr>
			<td>Customer Representative Name: *</td>
			<td><input type="text" name ="crName"  ng-model="custrep.crName" ng-minlength="3" ng-maxlength="40" ng-required="true"></td>
		</tr>
	
		<tr>
			<td>Customer Representative Designation: *</td>
			<td><input type="text" name ="crDesignation" ng-model="custrep.crDesignation" ng-minlength="3" ng-maxlength="40"  ng-required="true"></td>
		</tr>
	
		<tr>
			<td>Customer Representative Mobile No.: *</td>
			<td><input type="number" name ="crMobileNo" ng-model="custrep.crMobileNo" ng-minlength="4" ng-maxlength="15" ng-required="true"></td>
		</tr>
	
		<tr>
			<td>Customer Representative Email Id: *</td>
			<td><input type="email" name ="crEmailId" ng-model="custrep.crEmailId" ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" ng-maxlength="40" ng-required="true"></td>	
		</tr>
	
		<tr>
			<td>Customer Reference Number: *</td>
			<td><input type="text" name ="custRefNo" ng-model="custrep.custRefNo"></td>		
		</tr>
	
		<tr>
			<td colspan="2"><input type="submit" value="Submit"></td>
		</tr>
		</table>

		</form> --%>


<div id="customerCodeDB" ng-hide="CustomerCodeDBFlag">

	<input type="text" name="filterTextbox"
		ng-model="filterTextbox.custCode" placeholder="Search by Code">
	<table>
		<tr>
		<th></th>
		<th>Customer Code</th>
		<th>Customer Name</th>
		<th>Customer FACode</th>
		</tr>
		<tr ng-repeat="cust in custCodeList | filter:filterTextbox">
			<td><input type="radio" name="custCode" value="{{ cust.custCode }}"
				ng-model="$parent.custCode" ng-change="saveCustomerCode(cust)"></td>
			<td>{{ cust.custCode }}</td>
			<td>{{ cust.custName }}</td>
			<td>{{ cust.custFaCode }}</td>
		</tr>
	</table>
</div>
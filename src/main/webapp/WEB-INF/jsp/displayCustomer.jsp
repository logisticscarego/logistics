<div ng-show="adminLogin || superAdminLogin" >

<title>Display Customer</title>
<div class="container">
	<div class="row">
		<div class="col s2">
			<input type="radio" name="custList" ng-model="custByModal"
				ng-click="enableModalTextBox()">
		</div>
		<div class="col s10">
			<input type="text" id="custByMId" name="custByM" ng-model="custByM"
				disabled="disabled" ng-click="OpenCustCodeDB()">
		</div>
	</div>

	<div class="row">
		<div class="col s2">
			<input type="radio" name="custList" ng-model="custByAuto"
				ng-click="enableAutoTextBox()">
		</div>
		<div class="col s8">
			<input type="text" id="custByAId" name="custByA" ng-model="custByA"
				disabled="disabled" ng-keyup="getCustCodeList()">
		</div>
		<div class="col s2">
			<input class="col s12 btn" type="button" id="ok" disabled="disabled"
				value="OK" ng-click="getCustomerList()">
		</div>
	</div>

	<div class="row">
		<div class="col s12">
			<input id="bkToList" type="button" value="Back To list"
				ng-click="backToList()">
		</div>
	</div>
</div>

<div id="customerCodeDB" ng-hide="customerCodeDBFlag">
	<input type="text" name="filterCustomerCode"
		ng-model="filterCustomerCode" Placeholder="Search by Customer Codes">
	<table>
		<tr ng-repeat="custCodes in custCodeList | filter:filterCustomerCode ">
			<td><input type="radio" name="custCodeName"
				value="{{ custCodes }}" ng-model="custCodes1"
				ng-click="saveCustomerCode(custCodes)"></td>
			<td>{{ custCodes }}</td>
		</tr>
	</table>
</div>

<div ng-hide="showCustomer">

	<title>Customer</title>
	<div class="row">
		<div class="col s2 hide-on-med-and-down">&nbsp;</div>
		<form name=CustomerForm class="col s12 m12 l8 card"
			style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
			ng-submit="updateCustomer(CustomerForm,customer)">
			<div class="row">
				<div class="input-field col s12 m4 l4">
					<input type="text" name="branchCode" id="branchCode"
						ng-model="customer.branchCode" ng-click="OpenBranchCodeDB()"
						required readonly> <label>Branch Code</label>
				</div>
				<div class="input-field col s12 m8 l8">
					<input id="fname" type="text" name="custName"
						ng-model="customer.custName" ng-minlength="3" ng-maxlength="40"
						ng-required="true"> <label> Name</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<input id="add" type="text" name="custAdd"
						ng-model="customer.custAdd" ng-minlength="3" ng-maxlength="255"
						ng-required="true"> <label>Address</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12 m4 l4">
					<input id="city" type="text" name="custCity"
						ng-model="customer.custCity" ng-minlength="3" ng-maxlength="40"
						ng-required="true"> <label>City</label>
				</div>

				<div class="input-field col s12 m4 l4">
					<input id="state" type="text" name="custState"
						ng-model="customer.custState" ng-minlength="3" ng-maxlength="40"
						ng-required="true"> <label>State</label>
				</div>
				<div class="input-field col s12 m4 l4">
					<input id="pin" type="text" name="custPin"
						ng-model="customer.custPin" ng-minlength="6" ng-maxlength="6"
						ng-required="true"> <label>Pin</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12 m6 l3">
					<input id="circle" type="text" name="custTdsCircle"
						ng-model="customer.custTdsCircle" ng-minlength="3"
						ng-maxlength="40"> <label>TDS Circle</label>
				</div>
				<div class="input-field col s12 m6 l3">
					<input id="tdscity" type="text" name="custTdsCity"
						ng-model="customer.custTdsCity" ng-minlength="3" ng-maxlength="40">
					<label>TDS City</label>
				</div>
				<div class="input-field col s12 m6 l3">
					<input type="text" id="custPanNo" name="custPanNo"
						ng-model="customer.custPanNo" ng-minlength="10" ng-maxlength="10"
						ng-pattern="/[a-zA-Z0-9]/" > <label>PAN
						Number</label>
				</div>
				<div class="input-field col s12 m6 l3">
					<input id="custTinNo" type="text" name="custTinNo"
						ng-model="customer.custTinNo" ng-minlength="8" ng-maxlength="12"
						min="0"> <label>TIN Number</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12 m6 l6">
					<select name="custStatus" ng-model="customer.custStatus" required>
						<option value="Private Limited">Private Limited</option>
						<option value="Limited">Limited</option>
						<option value="Partnership">Partnership</option>
						<option value="Others">Others</option>
					</select> <label>Customer Status</label>
				</div>

				<div class="input-field col col s12 m6 l6">
					<input type="checkbox" ng-model="checkcustIsDailyContAllow"
						ng-change="savecustIsDailyContAllow()"> <label>Daily
						Contract Allowed</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12 m6 l4">
					<input id="custDirector" type="text" name="custDirector"
						ng-model="customer.custDirector" ng-click="OpenDirectorDB()"
						required readonly> <label>Director</label>
				</div>
				<div class="input-field col s12 m6 l4">
					<input id="custMarketingHead" type="text" name="custMarketingHead"
						ng-model="customer.custMktHead" ng-click="OpenMarketingDB()"
						required readonly> <label>Marketing Head</label>
				</div>
				<div class="input-field col s12 m6 l4">
					<input type="text" id="custCorpExecutive" name="custCorpExecutive"
						ng-model="customer.custCorpExc" ng-click="OpenExecutiveDB()"
						required readonly> <label>Corporate Executive</label>
				</div>
				<div class="input-field col s12 m6 l4">
					<input id="custBranchPerson" type="text" name="custBranchPerson"
						ng-model="customer.custBrPerson" ng-click="OpenBranchPersonDB()"
						required readonly> <label>Branch Head</label>
				</div>
				<div class="input-field col s12 m6 l4">
					<input id="custCRPeriod" type="number" name="custCRPeriod"
						ng-model="customer.custCrPeriod" ng-required="true"> <label>CR_Period</label>
				</div>
				<div class="input-field col s12 m6 l4">
					<select name="custCrBase" ng-model="customer.custCrBase" required>
						<option value="BF">BF</option>
						<option value="BL">BL</option>
						<option value="CN">CN</option>
					</select> <label>CR_Base</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12 m6 l6">
					<select name="custSrvTaxBy" ng-model="customer.custSrvTaxBy"
						required>
						<option value="C">Customer</option>
						<option value="S">SECL</option>
					</select> <label>Srv_Tax_By</label>
				</div>
				<div class="input-field col s12 m6 l6">
					<select name="custBillCycle" ng-model="customer.custBillCycle"
						ng-change="setCustBillValue()" required>
						<option value='D'>D</option>
						<option value='W'>W</option>
						<option value='M'>M</option>
						<option value='F'>F</option>
					</select> <label>Bill Cycle</label>
				</div>
				<div class="input-field col s12 m6 l6">
					<input type="number" name="custBillValueName" id="custBillValueId"
						ng-model="customer.custBillValue" disabled="disabled"
						/ ng-blur="checkBillValue()"> <label>Customer Bill
						Value</label>
				</div>
				<!-- <div class="input-field col s12 m6 l6">
       					 <input class="btn col s12" type ="button" name="acrd" id="acrd" value="Add CR Details" ng-model="custRepDetail" ng-click="OpenCRDB()">
       					<label>Customer Rep Details</label>
     			    </div> -->
			</div>

			<div class="row">
				<div class="col s12 center">
					<input class="btn waves-effect waves-light" type="submit"
						value="update">
				</div>
			</div>
		</form>

	</div>

	<!-- 	<div class="row" id ="crdCb" ng-hide="CrdCbFlag">	
     	<form class="col s12 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="AddCustomerRepForm" form ng-submit="   nextAddCR(AddCustomerRepForm,custrep)">
		<div class="row">
		 <div class="col s3 hide-on-med-and-down"> &nbsp; </div>
    			<div class="row">
      				<div class="input-field col s12 m6 l6">
        				<input type ="text" id="text2" name ="custCode" ng-model="custrep.custCode" readonly>
        				<label>Customer Code</label>
      				</div>
        			<div class="input-field col s12 m6 l6">
        				<input type="text" name ="custRefNo" id="custRefNo" ng-model="custrep.custRefNo" ng-minlength="3" ng-maxlength="40">
        				<label>Customer Reference Number</label>
        			</div>
      			</div>
      			
      			<div class="row">
      				<div class="input-field col s12">
        					<input type="text" id="crFirstName" name ="crFirstName"  ng-model="custrep.crFirstName" ng-minlength="3" ng-maxlength="40" ng-required="true">
      						<label>First Name</label>
      			   </div>
      			   
      			   <div class="input-field col s12">
        					<input type="text" id="crLastName" name ="crLastName"  ng-model="custrep.crLastName" ng-minlength="3" ng-maxlength="40" ng-required="true">
      						<label>Last Name</label>
      			   </div>
      			</div>
      			<div class="row">
      				<div class="input-field col s12 m4 l4">
      					<input type="text" name ="crDesignation" id="crDesignation" ng-model="custrep.crDesignation" ng-minlength="3" ng-maxlength="40" ng-model-options="{ debounce: 200 }"  ng-keyup="getCustDesigList(custrep)"  ng-blur="fillCustDesigVal()" ng-required="true">
      					<label>Designation</label>
        			</div>
      				<div class="input-field col s12 m4 l4">
        				<input type="text" name ="crMobileNo" id="crMobileNo" ng-model="custrep.crMobileNo" ng-minlength="4" ng-maxlength="15" ng-required="true">
      					<label>Mobile No</label>
      				</div>
      				<div class="input-field col s12 m4 l4">
        				<input type="email" name ="crEmailId" ng-model="custrep.crEmailId" ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" ng-maxlength="40" ng-required="true">
      					<label>Email ID</label>
      				</div>
      			</div>	
      			
		      <div class="row">
		      <div class="col s12 center">
		        <input class="btn" type="submit" value="Submit">
		      </div>
		     </div>
     	 </div>		
         </form>
     </div> -->

	<div id="branchCodeDB" ng-hide="BranchCodeDBFlag">

		<input type="text" name="filterTextbox"
			ng-model="filterTextbox.branchCode" placeholder="Search by Code">
		<table>
			<tr>
				<th></th>

				<th>Branch Code</th>

				<th>Branch Name</th>

				<th>Branch Pin</th>
			</tr>
			<tr ng-repeat="branch in branchList | filter:filterTextbox"">
				<td><input type="radio" name="branchCode" id="branchId"
					value="{{ branch.branchCode }}" ng-model="branchCode"
					ng-click="saveBranchCode(branch)"></td>
				<td>{{ branch.branchCode }}</td>
				<td>{{ branch.branchName }}</td>
				<td>{{ branch.branchPin }}</td>
			</tr>
		</table>

	</div>

	<div id="custDirectorDB" ng-hide="custDirectorDBFlag">
		<input type="text" name="filterDirector"
			ng-model="filterDirector.empCode"
			placeholder="Search by Employee Code">
		<table>
			<tr>
				<th></th>

				<th>Employee Code</th>

				<th>Employee Name</th>

				<th>Branch Code</th>

			</tr>

			<tr
				ng-repeat="empCustDirector in employeeList | filter:filterDirector">
				<td><input type="radio" name="empCustDirectorName"
					class="empCustDirectorCls" value="{{ empCustDirector }}"
					ng-model="custDirector"
					ng-click="saveCustDirector(empCustDirector)"></td>
				<td>{{ empCustDirector.empCode }}</td>
				<td>{{ empCustDirector.empName }}</td>
				<td>{{ empCustDirector.branchCode }}</td>
			</tr>
		</table>

	</div>

	<div id="custMktHeadDB" ng-hide="custMktHeadDBFlag">
		<input type="text" name="filterMktHead"
			ng-model="filterMktHead.empCode"
			placeholder="Search by Employee Code">
		<table>
			<tr>
				<th></th>

				<th>Employee Code</th>

				<th>Employee Name</th>

				<th>Branch Code</th>

			</tr>

			<tr ng-repeat="empCustMktHead in employeeList | filter:filterMktHead">
				<td><input type="radio" name="empCustMktHeadName"
					class="empCustMktHeadCls" value="{{ empCustMktHead }}"
					ng-model="custMktHead" ng-click="saveCustMktHead(empCustMktHead)"></td>
				<td>{{ empCustMktHead.empCode }}</td>
				<td>{{ empCustMktHead.empName }}</td>
				<td>{{ empCustMktHead.branchCode }}</td>
			</tr>
		</table>

	</div>

	<div id="custCorpExcDB" ng-hide="custCorpExcDBFlag">
		<input type="text" name="filterCorpExc"
			ng-model="filterCorpExc.empCode"
			placeholder="Search by Employee Code">
		<table>
			<tr>
				<th></th>

				<th>Employee Code</th>

				<th>Employee Name</th>

				<th>Branch Code</th>

			</tr>

			<tr ng-repeat="empCustCorpExc in employeeList | filter:filterCorpExc">
				<td><input type="radio" name="empCustCorpExcName"
					class="empCustCorpExcCls" value="{{ empCustCorpExc }}"
					ng-model="custCorpExc" ng-click="saveCustCorpExc(empCustCorpExc)"></td>
				<td>{{ empCustCorpExc.empCode }}</td>
				<td>{{ empCustCorpExc.empName }}</td>
				<td>{{ empCustCorpExc.branchCode }}</td>
			</tr>
		</table>

	</div>

	<div id="custBrPersonDB" ng-hide="custBrPersonDBFlag">
		<input type="text" name="filteBrPerson"
			ng-model="filterBrPerson.empCode"
			placeholder="Search by Employee Code">
		<table>
			<tr>
				<th></th>

				<th>Employee Code</th>

				<th>Employee Name</th>

				<th>Branch Code</th>

			</tr>

			<tr
				ng-repeat="empCustBrPerson in employeeList | filter:filteBrPerson">
				<td><input type="radio" name="empCustBrPersonName"
					class="empCustempCustBrPersonCls" value="{{ empCustBrPerson }}"
					ng-model="custBrPerson"
					ng-click="saveCustBrPerson(empCustBrPerson)"></td>
				<td>{{ empCustBrPerson.empCode }}</td>
				<td>{{ empCustBrPerson.empName }}</td>
				<td>{{ empCustBrPerson.branchCode }}</td>
			</tr>
		</table>

	</div>

	<div id="viewCustDetailsDB" ng-hide="viewCustDetailsFlag">

		<div class="row">

			<div class="col s12 card"
				style="align: center; margin-top: 0px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
				<h4>
					Here's the details of Customer <span
						class="teal-text text-lighten-2">{{customer.custName}}</span>:
				</h4>
				<table class="table-bordered table-hover table-condensed">

					<tr>
						<td>Customer Name:</td>
						<td>{{customer.custName}}</td>
					</tr>
					<tr>
						<td>Customer Address:</td>
						<td>{{customer.custAdd}}</td>
					</tr>
					<tr>
						<td>Customer City:</td>
						<td>{{customer.custCity}}</td>
					</tr>

					<tr>
						<td>Customer State:</td>
						<td>{{customer.custState}}</td>
					</tr>

					<tr>
						<td>Customer Pin:</td>
						<td>{{customer.custPin}}</td>
					</tr>

					<tr>
						<td>Customer TDS Circle:</td>
						<td>{{customer.custTdsCircle}}</td>
					</tr>

					<tr>
						<td>Customer TDS City:</td>
						<td>{{customer.custTdsCity}}</td>
					</tr>

					<tr>
						<td>Customer PAN Number:</td>
						<td>{{customer.custPanNo}}</td>
					</tr>

					<tr>
						<td>Customer TIN Number:</td>
						<td>{{customer.custTinNo}}</td>
					</tr>

					<tr>
						<td>Customer Status:</td>
						<td>{{customer.custStatus}}</td>
					</tr>

					<tr>
						<td>Daily Contract Allowed:</td>
						<td>{{customer.custIsDailyContAllow}}</td>
					</tr>

					<tr>
						<td>Director:</td>
						<td>{{customer.custDirector}}</td>
					</tr>

					<tr>
						<td>Marketing Head:</td>
						<td>{{customer.custMktHead}}</td>
					</tr>

					<tr>
						<td>Corp. Executive:</td>
						<td>{{customer.custCorpExc}}</td>
					</tr>

					<tr>
						<td>Branch Person:</td>
						<td>{{customer.custBrPerson}}</td>
					</tr>

					<tr>
						<td>CR Period</td>
						<td>{{customer.custCrPeriod}}</td>
					</tr>

					<tr>
						<td>CR Base:</td>
						<td>{{customer.custCrBase}}</td>
					</tr>

					<tr>
						<td>Service Tax By:</td>
						<td>{{customer.custSrvTaxBy}}</td>
					</tr>

					<tr>
						<td>Bill Cycle:</td>
						<td>{{customer.custBillCycle}}</td>
					</tr>

					<tr>
						<td>Bill Value:</td>
						<td>{{customer.custBillValue}}</td>
					</tr>

					<!-- 	             <tr>
	                <td>Customer Referance No.:</td>
	                <td>{{custrep.custRefNo}}</td>
	            </tr>
	
				<tr>
	                <td>Customer Representative Name:</td>
	                <td>{{custrep.crName}}</td>
	            </tr>
	            
	         
	            <tr>
	                <td>Customer Representative Designation:</td>
	                <td>{{custrep.crDesignation}}</td>
	            </tr>
	            
	            <tr>
	                <td>Customer Representative Mobile No.:</td>
	                <td>{{custrep.crMobileNo}}</td>
	            </tr>
	            
	            <tr>
	                <td>Customer Representative Email Id:</td>
	                <td>{{custrep.crEmailId}}</td>
	            </tr>-->
					<tr ng-repeat="CRCode in custRepList">
						<td>Customer Representative Codes:</td>
						<td>{{CRCode}}</td>
					</tr>
				</table>

				<input type="button" value="Save" ng-click="saveCustomer(customer)">
				<input type="button" value="Cancel"
					ng-click="closeViewCustDetailsDB()">


			</div>
		</div>
	</div>


	<!-- 	<div class="container">
	             <div class="row">
	             <div class="input-field col s12 m4 l3"><input type ="text" id="custCode" name ="custCode" ng-model="customer.custCode" value={{customer.custCode}} readonly><label>Customer Code</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custName" name ="custName" ng-model="customer.custName" value={{customer.custName}}><label>Customer Name</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custAdd" name ="custAdd" ng-model="customer.custAdd" value={{customer.custAdd}}><label>Customer Address</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custCity" name ="custCity" ng-model="customer.custCity" value={{customer.custCity}}><label>Customer City</label></div>
	                <div  class="input-field col s12 m4 l3"><input type ="text" id="custState" name ="custState" ng-model="customer.custState" value={{customer.custState}}><label>Customer State</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custPin" name ="custPin" ng-model="customer.custPin" value={{customer.custPin}}><label>Customer Pin</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custTdsCircle" name ="custTdsCircle" ng-model="customer.custTdsCircle" value={{customer.custTdsCircle}}><label>Customer TDS Circle</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custTdsCity" name ="custTdsCity" ng-model="customer.custTdsCity" value={{customer.custTdsCity}}><label>Customer TDS City</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custPanNo" name ="custPanNo" ng-model="customer.custPanNo" value={{customer.custPanNo}}><label>Customer PAN Number</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custTinNo" name ="custTinNo" ng-model="customer.custTinNo" value={{customer.custTinNo}}><label>Customer TIN Number</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custStatus" name ="custStatus" ng-model="customer.custStatus" value={{customer.custStatus}}><label>Customer Status</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custIsDailyContAllow" name ="custIsDailyContAllow" ng-model="customer.custIsDailyContAllow" value={{customer.custIsDailyContAllow}}><label>Daily Contract Allowed</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custDirector" name ="custDirector" ng-model="customer.custDirector" value={{customer.custDirector}}><label>Director</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custMktHead" name ="custMktHead" ng-model="customer.custMktHead" value={{customer.custMktHead}}><label>Marketing Head</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custCorpExc" name ="custCorpExc" ng-model="customer.custCorpExc" value={{customer.custCorpExc}}><label>Corporate Executive</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custBrPerson" name ="custBrPerson" ng-model="customer.custBrPerson" value={{customer.custBrPerson}}><label>Branch Person</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custCrPeriod" name ="custCrPeriod" ng-model="customer.custCrPeriod" value={{customer.custCrPeriod}}><label>Credit Period</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custCrBase" name ="custCrBase" ng-model="customer.custCrBase" value={{customer.custCrBase}}><label>Credit Base</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custSrvTaxBy" name ="custSrvTaxBy" ng-model="customer.custSrvTaxBy" value={{customer.custSrvTaxBy}}><label>Service Tax By</label></div>
	         		<div class="input-field col s12 m4 l3"><input type ="text" id="custBillCycle" name ="custBillCycle" ng-model="customer.custBillCycle" value={{customer.custBillCycle}}><label>Bill Cycle</label></div>
	                <div class="input-field col s12 m4 l3"><input type ="text" id="custBillValue" name ="custBillValue" ng-model="customer.custBillValue" value={{customer.custBillValue}}><label>Bill Value</label></div>
	                
	                
	             </div>
	          </div>
       	 	<div class="row">
				<div class="col s12 center"><input type="button" value="Submit!"  ng-click="editCustomerSubmit(customer)"></div>
			</div> -->

</div>

<div ng-hide="isViewNo" id="CustomerTableDB" class="container">
	<input type="text" name="filterCust" ng-model="filterCust.custCode"
		placeholder="Search by Customer Codes">
	<table class="table-hover table-bordered table-condensed">
		<tr>
			<th></th>

			<th>Customer Name</th>

			<th>Customer Code</th>

			<th>User Code</th>

			<th>Time Stamp</th>

		</tr>

		<tr ng-repeat="cust in custList | filter:filterCust">
			<td><input id="{{ cust.custCode }}" type="checkbox"
				value="{{ cust.custCode }}"
				ng-checked="selection.indexOf(cust.custCode) > -1"
				ng-click="toggleSelection(cust.custCode)" /></td>
			<td>{{ cust.custName }}</td>
			<td>{{ cust.custCode }}</td>
			<td>{{ cust.userCode }}</td>
			<td>{{ cust.creationTS }}</td>
		</tr>

	</table>
	<table>
		<tr>
			<td class="center"><input type="submit" value="Submit"
				ng-click="verifyCustomer()"></td>
		</tr>
	</table>
</div>
</div>
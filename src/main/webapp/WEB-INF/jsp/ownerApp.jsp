
<title>Owner</title>

	<!-- <table class="noborder" ng-show="stateList.length > 0">
		    <thead>
		        <tr>
		            <th>Action</th>
		            <th>State Code</th>   
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="state in stateList">
		            <td><input type="button" ng-click="removeState(state)" value="Remove">
		            <td>{{state.mulStateStateCode}}</td>
		        </tr>
		    </tbody>
		</table>
		<input type="button" ng-show="stateList.length > 1" ng-click="removeAllState()" value="Remove All State">
		
		<table class="noborder" ng-show="stnList.length > 0">
		    <thead>
		        <tr>
		            <th>Action</th>
		            <th>Station Code</th>   
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="station in stnList">
		            <td><input type="button" ng-click="removeStation(station)" value="Remove">
		            <td>{{station.mulStnStnCode}}</td>
		        </tr>
		    </tbody>
		</table>
		<input type="button" ng-show="stnList.length > 1" ng-click="removeAllStation()" value="Remove All Station"> -->
		
<div class="row">
		<form class="col s12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" name ="OwnerForm" ng-submit="SubmitOwner(OwnerForm,CurrentAddressForm,ContactPersonForm,OtherAddressForm,owner,contPerson,currentAddress)">	
			
			<div class="row">      			
      			<div class="input-field col s3">
        			<input id="OwnerName" type ="text" name ="ownName" ng-model="owner.ownName" ng-click="OpenPendingOwnerDB()"  ng-minlength="3" ng-maxlength="255" ng-required="true" readonly>
        			<input id="OwnerId" type ="hidden" name ="ownId" ng-model="owner.ownId">
        			<label>Name</label>	
      			</div>
      			<div class="input-field col s3">
        			<input id="BranchCode" type ="text" name ="branchCode" ng-model="tempBrName" ng-click="OpenBranchCodeDB()" required readonly>
        			<label>Branch Code</label>	
      			</div>
      			<div class="input-field col s3">
        			<input class="col s12 btn teal white-text" type ="button" name ="currentOffcAddress" value="Fill Address" ng-click="OpenCurrentAddressDB()"  readonly>
      					<label>Current Office Address</label>
        		</div>
      			<div class="input-field col s3">
        			<input class="col s12 btn teal white-text" type ="button" value="Fill Address" ng-click="OpenRegisterAddressDB()">
      					<label>Register Office Address</label>
        		</div>
      		</div>
      		
      		<div class="row">
      			
      			<div class="input-field col s3">
        			<input id="ownUnion" type ="text" name ="ownUnion" ng-model="owner.ownUnion">
        			<label>Union</label>	
      			</div>
      			<div class="input-field col s3">
        			<input id="EmailId" type ="text" name ="ownEmailId" ng-model="owner.ownEmailId"  ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" ng-maxlength="40">
        			<label>Email ID</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input class="col s12 btn teal white-text" type ="button" value="Fill Address" ng-click="OpenOtherAddressDB()">
      				<label>Other Office Address</label>
        		</div>
      			<!-- <div class="input-field col s3">
        			<input class="col s12 btn teal white-text" type ="button" name="contPerson" value="Fill Details" ng-click="OpenContactPersonDB()" redaonly >
      				<label>Contact Person</label>
        		</div> -->
        		
        		<div class="input-field col s3">
      				<input class="col s12 btn teal white-text" type ="button" value="Add State" ng-click="OpenStateDB()" >
      				<label>Add State</label>
      			</div>
      		</div>
      		
      		<div class="row">
      			<!-- <div class="input-field col s3">
        			<input id="panIntId" type ="number" name ="panIntName" ng-model="owner.ownPanIntRt" ng-minlength="1" ng-maxlength="40">
        			<label>Pan Interest Rate</label>	
      			</div> -->
      			
      			<!-- <div class="input-field col s3">
        			<input id="pan" type ="date" name ="ownPanDt" ng-model="owner.ownPanDt">
        			<label>Pan Date</label>	
      			</div>
      			<div class="input-field col s3">
        			<input id="pan_name" type ="text" name ="ownPanName" ng-model="owner.ownPanName" ng-minlength="3" ng-maxlength="40">
        			<label>Pan Name</label>	
      			</div> -->
      			
      			<div class="input-field col s3">
        			<input id="ownPhId" type ="text" name ="ownPhName" ng-model="phNo" ng-minlength="1" ng-maxlength="10">
        			<label>Owner Mobile No.</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input style="width: calc(100% - 40px);" id="vehicle_type" type ="text" name ="ownVehicleType" ng-model="owner.ownVehicleType" ng-click="OpenVehicleTypeDB()"  readonly>
        			<label>Vehicle Type</label>
        			<a style="width: 40px; position: absolute; right: -10px; padding: 0 1rem;" class="btn waves-effect teal" ng-click="OpenAddVehicleTypeDB()"><i class="mdi-content-add white-text" style="font-size: 24px; position: absolute; left: 9px"></i></a>
      			</div>
      			
      			<div class="input-field col s3">	
      				<input class="col s12 btn teal white-text" type ="button" value="Pan Detail" ng-click="openPanDetail()" >
      				<label>Pan Card Detail</label>
      			</div>	
  
      			<div class="input-field col s3">	
      				<input class="col s12 btn teal white-text" type ="button" value="Add Station" ng-click="OpenStationDB()" >
      				<label>Add Station</label>
      			</div>	
      		</div>
      		
      		<div class="row">
      			<!-- <div class="input-field col s3">
					<input type ="text" id="ownPanNoId" name ="ownPanNoName" ng-model="owner.ownPanNo" ng-minlength="10" ng-maxlength="10">
					  <label>Pan No.</label>
      			</div> -->
      			<div class="input-field col s3">
        			<input id="ownSrvTaxNo" type ="text" name ="ownSrvTaxNo" ng-model="owner.ownSrvTaxNo" ng-maxlength="15">
        			<label>Service Tax No</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="ownPPNo" type ="text" name ="ownPPNo" ng-model="owner.ownPPNo" ng-minlength="8" ng-minlength="25">
        			<label>Passport No</label>	
      			</div>
      			<div class="input-field col s3">
        			<input id="ownVoterId" type ="text" name ="ownVoterId" ng-model="owner.ownVoterId" ng-minlength="10" ng-maxlength="10">
        			<label>Voter ID</label>	
      			</div>
      			<div class="input-field col s3">
        			<input id="ownFirmRegNo" type ="text" name ="ownFirmRegNo" ng-model="owner.ownFirmRegNo" >
        			<label>Firm Reg No</label>	
      			</div>
      		</div>
      		
      		<div class="row">
      			<div class="input-field col s3">
        			<input id="ownRegPlace" type ="text" name ="ownRegPlace" ng-model="owner.ownRegPlace"  ng-maxlength="40">
        			<label>Reg Place</label>	
      			</div>
      			<div class="input-field col s3">
        			<input id="ownCIN" type ="text" name ="ownCIN" ng-model="owner.ownCIN">
        			<label>Comp. ID No</label>	
      			</div>
      			<div class="input-field col s3">	
					<select name="ownBsnCard" id="ownBsnCard" ng-model="owner.ownBsnCard" >
						<option value="Yes">Yes</option>
						<option value="No">No</option>
					</select>
					<label>Business Card</label>
      			</div>
      			<div class="input-field col s3">
        		<select name="ownFirmType" id="ownFirmType" ng-model="owner.ownFirmType" >
                    <option value="Propritor">Propritor</option>
                    <option value="Partner">Partner</option>
                    <option value="Pvt Ltd">Pvt Ltd</option>
                    <option value="Ltd">Limited</option>
                </select>	
                <label>Firm Type</label>
      			</div>
      		</div>
      		
      		<div class="row">
      			<div class="input-field col s3">
        			<input type ="date" name ="ownComStbDt" ng-model="owner.ownComStbDt" >
        			<label>Comp. Est. Date</label>	
      			</div>	
      			<!-- <div class="input-field col s3">
        			<input type ="date" name ="ownPanDOB" ng-model="owner.ownPanDOB">
        			<label>Pan DOB</label>	
      			</div> -->
				<div class="input-field col s3">
        			<input type ="date" name ="ownActiveDt" ng-model="owner.ownActiveDt" >
        			<label>Activate Date</label>	
      			</div>
      		</div>	

			<!-- <div class="row">
				
      		</div>	 -->
      			      		
      		<div class="row">
      			<div class="col s6" ng-show="stateList.length > 0">
      				<div class="card blue-grey darken-1" style="margin: 0 20px;">
      					<div class="card-content white-text">
      						<table>
		    					<thead>
		        					<tr>
		            					<th>State Code</th>
		            					<th style="text-align:right;">Action</th>   
		        					</tr>
		    					</thead>
		    					<tbody>
		        					<tr ng-repeat="state in stateList">
		            					<td>{{state.mulStateStateCode}}</td>
		            					<td style="text-align:right;"><input type="button" ng-click="removeState(state)" value="Remove">
		        					</tr>
		        					<tr ng-show="stateList.length > 1">
		        						<td colspan="2"><input type="button" ng-click="removeAllState()" value="Remove All State"></td>
		        					</tr>
		    					</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col s6" ng-show="stnList.length > 0">
					<div class="card blue-grey darken-1" style="margin: 0 20px;">
			 			<div class="card-content white-text">
							<table>
		    					<thead>
		        					<tr>
		            					<th>Station Code</th>
		            					<th style="text-align:right;">Action</th>   
		        					</tr>
		    					</thead>
		    					<tbody>
		        					<tr ng-repeat="station in stnList">
		            					<td>{{station.mulStnStnCode}}</td>
		            					<td style="text-align:right;"><input type="button" ng-click="removeStation(station)" value="Remove">
		        					</tr>
		        					<tr ng-show="stnList.length > 1">
		        						<td colspan="2"><input type="button" ng-click="removeAllStation()" value="Remove All Station"></td>
		        					</tr>
		    					</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
      		
      		
      		<!-- <div class="row">
      			<div class="input-field col s9">
					<input type="file" class="teal white-text btn" file-model="panImg">
					<label>Choose Pan Card Image</label>
				</div>
				<div class="col s3">
					<a class="btn waves-effect waves-light white-text col s12"
						type="button" ng-click="uploadPanImg()">Upload</a>
				</div>
      		</div>
      		
      		<div class="row">
      			<div class="input-field col s9">
					<input type="file" class="teal white-text btn" file-model="decImg">
					<label>Choose Deceleration Image</label>
				</div>
				<div class="col s3">
					<a class="btn waves-effect waves-light white-text col s12"
						type="button" ng-click="uploadDecImg()">Upload</a>
				</div>
      		</div> -->
      		
      		<div class="row">
      			 	<div class="col s12 center">
      			 		<input type="submit" value="Submit">
      			 	</div>
      		</div>
      		
      	</form>
 </div>

<!-- <form name ="OwnerForm" ng-submit="SubmitOwner(OwnerForm,owner,contPerson,currentAddress)">
<table>
			
			<tr>
				    <td>Branch Code *</td> 
					<td><input type ="text" name ="branchCode" ng-model="owner.branchCode" ng-click="OpenBranchCodeDB()" required readonly></td>
			</tr>
			
			<tr>
					<td>Owner Name *</td> 
					<td><input type ="text" name ="ownName" ng-model="owner.ownName" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
			</tr>
			
			<tr>
					<td>Current Office Address *</td> 
					<td><input type ="button" name ="currentOffcAddress" value="Fill Address" ng-click="OpenCurrentAddressDB()" required readonly></td>
			</tr>
			
			<tr>
					<td>Contact Person *</td> 
					<td><input type ="button" name="contPerson" value="Fill Details" ng-click="OpenContactPersonDB()" redaonly required></td>
			</tr>
			
			<tr>
					<td>Owner Com Estabilishing Date *</td> 
					<td><input type ="date" name ="ownComStbDt" ng-model="owner.ownComStbDt" ng-required="true"></td>
			</tr>
			
			<tr>
					<td>Register Office Address</td> 
					<td><input type ="button" value="Fill Address" ng-click="OpenRegisterAddressDB()"></td>
			</tr>
			
			<tr>
					<td>Other Office Address</td> 
					<td><input type ="button" value="Fill Address" ng-click="OpenOtherAddressDB()"></td>
			</tr>
			
			<tr>
					<td>Owner Pan DOB</td> 
					<td><input type ="date" name ="ownPanDOB" ng-model="owner.ownPanDOB"></td>
			</tr>
			
			<tr>
					<td>Owner Pan Date</td> 
					<td><input type ="date" name ="ownPanDt" ng-model="owner.ownPanDt"></td>
			</tr>
			
			<tr>
					<td>Owner Pan Name</td> 
					<td><input type ="text" name ="ownPanName" ng-model="owner.ownPanName" ng-minlength="3" ng-maxlength="40"></td>
			</tr>
			
			<tr>
					<td>Add State</td> 
					<td><input type ="button" value="Add State" ng-click="OpenStateDB()"></td>
			</tr>
			
			<tr>
					<td>Add Station</td> 
					<td><input type ="button" value="Add Station" ng-click="OpenStationDB()"></td>
			</tr>
			
			<tr>
					<td>Owner Vehicle Type *</td> 
					<td><input type ="text" name ="ownVehicleType" ng-model="owner.ownVehicleType" ng-click="OpenVehicleTypeDB()" required readonly></td>
			</tr>
			
			<tr>
					<td>Owner PPNo</td> 
					<td><input type ="text" name ="ownPPNo" ng-model="owner.ownPPNo" ng-minlength="8" ng-minlength="25"></td>
			</tr>
			
			<tr>
					<td>Owner Voter Id</td> 
					<td><input type ="text" name ="ownVoterId" ng-model="owner.ownVoterId" ng-minlength="10" ng-maxlength="10"></td>
			</tr>
			
			<tr>
					<td>Owner Service Tax No</td> 
					<td><input type ="text" name ="ownSrvTaxNo" ng-model="owner.ownSrvTaxNo" ng-maxlength="15"></td>
			</tr>
			
			<tr>
					<td>Owner Firm Reg No. *</td> 
					<td><input type ="text" name ="ownFirmRegNo" ng-model="owner.ownFirmRegNo" ng-required="true"></td>
			</tr>
			
			<tr>
					<td>Owner Reg Place *</td> 
					<td><input type ="text" name ="ownRegPlace" ng-model="owner.ownRegPlace" ng-required="true" ng-maxlength="40"></td>
			</tr>
			
			<tr>
                <td>Owner Firm Type *</td>
                <td><select name="ownFirmType" id="ownFirmType" ng-model="owner.ownFirmType" required>
                    <option value="Propritor">Propritor</option>
                    <option value="Partner">Partner</option>
                    <option value="Pvt Ltd">Pvt Ltd</option>
                    <option value="Ltd">Limited</option>
                </select></td>
            </tr>
			
			<tr>
					<td>Owner CIN</td> 
					<td><input type ="text" name ="ownCIN" ng-model="owner.ownCIN"></td>
			</tr>
			
			<tr>
				<td>Owner Bsn Card *</td> 
				<td><select name="ownBsnCard" id="ownBsnCard" ng-model="owner.ownBsnCard" required>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
                </select></td>
			</tr>
			
			<tr>
					<td>Owner Union</td> 
					<td><input type ="text" name ="ownUnion" ng-model="owner.ownUnion"></td>
			</tr>
			
			<tr>
					<td>Owner Email Id *</td> 
					<td><input type ="text" name ="ownEmailId" ng-model="owner.ownEmailId" ng-required="true" ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" ng-maxlength="40"></td>
			</tr>
			
			<tr>
					<td>Owner Activate Date *</td> 
					<td><input type ="date" name ="ownActiveDt" ng-model="owner.ownActiveDt" required></td>
			</tr>
			
			<tr>
					<td><input type="submit" value="Submit"></td>
			</tr>
	</table>
</form>
 -->
<div id="CurrentAddressDB" ng-hide = "CurrentAddressFlag">
<form name = "CurrentAddressForm" ng-submit="saveCurrentAddress(CurrentAddressForm,currentAddress)">
		<table>
			<tr>
				<td>Address *</td>
				<td><input type ="text" id="addressC" name ="address" ng-model="currentAddress.completeAdd"  ng-minlength="3" ng-minlength="255"></td>
			</tr>
			
			<tr>
				<td>Address City *</td>
				<td><input type ="text" id="addCityC" name ="addCity" ng-model="currentAddress.addCity"  ng-minlength="3" ng-minlength="15"></td>
			</tr>
			
			<tr>
				<td>Address State *</td>
				<td><input type ="text" id="addStateC" name ="addState" ng-model="currentAddress.addState"  ng-minlength="3" ng-minlength="40"></td>
			</tr>
			
			<tr>
				<td>Address Pin *</td>
				<td><input type ="text" id="addPinC" name ="addPin" ng-model="currentAddress.addPin"  ng-minlength="6" ng-maxlength="6"></td>
			</tr>
			
			<tr>
				<!-- <td>Address Type</td> -->
				<td><input type ="hidden" id="addTypeC" name ="addType" ng-model="currentAddress.addType" readonly></td>
			</tr>
			
		 	<tr>
				<td><input type="submit"  value="Submit" ></td>
			</tr>
		</table>
</form>
</div>	
	
	
	<div id="panCardDB" ng-hide = "panCardDBFlag">
	<form name="panCardForm" ng-submit="savePanCard(panCardForm)">
		<table>
			<tr>
				<td>Pan Name *</td>
				<td><input id="pan_name" type ="text" name ="ownPanName" ng-model="owner.ownPanName" ng-minlength="3" ng-maxlength="40" ng-required="true"></td>
			</tr>
			
			<tr>
				<td>Pan Date</td>
				<td><input id="pan" type ="date" name ="ownPanDt" ng-model="owner.ownPanDt"></td>
			</tr>
			
			<tr>
				<td>Pan No. *</td>
				<td><input type ="text" id="ownPanNoId" name ="ownPanNoName" ng-model="owner.ownPanNo" ng-minlength="10" ng-maxlength="10" ng-required="true" ng-pattern="/^[A-Z]{3}[C,P,H,F,A,T,B,L,J,G][A-Z]\d{4}[A-Z]$/" ></td>
			</tr>
			
			<tr>
				<td>Pan DOB</td>
				<td><input type ="date" name ="ownPanDOB" ng-model="owner.ownPanDOB"></td>
			</tr>
			
			<tr>
				<td><input type="file" class="teal white-text btn" file-model="panImg"></td>
				<td><a class="btn waves-effect waves-light white-text col s12"
						type="button" ng-click="uploadPanImg()">Upload Pan Card Image</a></td>
			</tr>
			
			<tr>
				<td><input type="file" class="teal white-text btn" file-model="decImg"></td>
				<td><a class="btn waves-effect waves-light white-text col s12"
						type="button" ng-click="uploadDecImg()">Upload Dec Image</a></td>
			</tr>
      			
		 	<tr>
				<td><input type="submit"  value="Submit" ></td>
			</tr>
		</table>
		</form>
	</div>	
	
	
	<div id="ContactPersonDB" ng-hide = "ContactPersonFlag">
	<form name="ContactPersonForm" ng-submit="saveContactPerson(ContactPersonForm,contPerson,mobNo)">
		<table>
			<tr>
				<td>Name *</td>
				<td><input type ="text" id="cpName" name ="cpName" ng-model="contPerson.cpName"  ng-minlength="2" ng-max-length="40"></td>
			</tr>
			
			<tr>
				<td>Mobile Number *</td>
				<td><input type ="text" id="cpMobile" name ="mobNo" ng-model="mobNo"  ng-minlength="10" ng-maxlength="10"></td>
			</tr>
			
			<tr>
				<td>Phone Number</td>
				<td><input type ="text" id="cpPhone" name ="cpPhone" ng-model="contPerson.cpPhone" ng-minlength="4" ng-maxlength="15"></td>
			</tr>
			
			<tr>
				<td>Pan Number *</td>
				<td><input type ="text" id="cpPanNo" name ="cpPanNo" ng-model="contPerson.cpPanNo" ng-minlength="10" ng-maxlength="10" ></td>
			</tr>
			
		 	<tr>
				<td><input type="submit"  value="Submit" ></td>
			</tr>
		</table>
		</form>
	</div>	
	
	<div id="RegisterAddressDB" ng-hide = "RegisterAddressFlag">
	<form name="RegisterAddressForm" ng-submit="saveRegisterAddress(RegisterAddressForm,registerAddress)">
		<table>
			<tr>
				<td>Address *</td>
				<td><input type ="text" id="addressR" name ="address" ng-model="registerAddress.completeAdd"  ng-minlength="3" ng-minlength="255"></td>
			</tr>
			
			<tr>
				<td>Address City *</td>
				<td><input type ="text" id="addCityR" name ="addCity" ng-model="registerAddress.addCity"  ng-minlength="3" ng-minlength="40"></td>
			</tr>
			
			<tr>
				<td>Address State *</td>
				<td><input type ="text" id="addStateR" name ="addState" ng-model="registerAddress.addState"  ng-minlength="3" ng-minlength="40"></td>
			</tr>
			
			<tr>
				<td>Address Pin *</td>
				<td><input type ="text" id="addPinR" name ="addPin" ng-model="registerAddress.addPin"  ng-minlength="6" ng-maxlength="6"></td>
			</tr>
			
			<tr>
				<!-- <td>Address Type</td> -->
				<td><input type ="hidden" id="addTypeR" name ="addType" ng-model="registerAddress.addType" readonly></td>
			</tr>
			
		 	<tr>
				<td><input type="submit"  value="Submit" ></td>
			</tr>
		</table>
		</form>
	</div>	
	
	<div id="OtherAddressDB" ng-hide = "OtherAddressFlag">
	<form name="OtherAddressForm" ng-submit="saveOtherAddress(OtherAddressForm,otherAddress)">
		<table>
			<tr>
				<td>Address *</td>
				<td><input type ="text" id="addressO" name ="address" ng-model="otherAddress.completeAdd"  ng-minlength="3" ng-minlength="255" ng-required="true"></td>
			</tr>
			
			<tr>
				<td>Address City *</td>
				<td><input type ="text" id="addCityO" name ="addCity" ng-model="otherAddress.addCity" ng-minlength="3" ng-minlength="40" ng-required="true"></td>
			</tr>
			
			<tr>
				<td>Address State *</td>
				<td><input type ="text" id="addStateO" name ="addState" ng-model="otherAddress.addState" ng-minlength="3" ng-minlength="40" ng-required="true"></td>
			</tr>
			
			<tr>
				<td>Address Pin *</td>
				<td><input type ="text" id="addPinO" name ="addPin" ng-model="otherAddress.addPin" ng-minlength="6" ng-maxlength="6" ng-required="true"></td>
			</tr>
			
			<tr>
				<!-- <td>Address Type</td> -->
				<td><input type ="hidden" id="addTypeO" name ="addType" ng-model="otherAddress.addType" readonly></td>
			</tr>
			
		 	<tr>
				<td><input type="submit"  value="Submit" ></td>
			</tr>
		</table>
		</form>
	</div>	
	
	<div id="StateDB" ng-hide = "StateFlag">
	<form name="StateForm" ng-submit="saveState(StateForm,state)">
		<table>
			<tr>
				<td>State *</td>
				<td><input type ="text" id="mulStateStateCode" name ="mulStateStateCode" ng-model="state.mulStateStateCode" ng-click="OpenStateList()"  readonly></td>
				<td><input type="button" value="Add New State" ng-click="openAddStateDB()"></td>
			</tr>
			
		 	<tr>
				<td><input type="submit"  value="Submit"  ></td>
			</tr>
		</table>
		</form>
	</div>	
	
	<div id="AddStateDB" ng-hide = "AddStateFlag">
	<form name="AddStateForm" ng-submit="saveNewState(AddStateForm,newState)">
		<table>
			<tr>
				<td>State Name*</td>
				<td><input type ="text" id="stateName" name ="stateName" ng-model="newState.stateName"   ></td>
			</tr>
			
			<tr>
				<td>State Code*</td>
				<td><input type ="text" id="stateCode" name ="stateCode" ng-model="newState.stateCode"   ></td>
			</tr>
			
			<tr>
				<td>Lorry Prefix*</td>
				<td><input type ="text" id="stateLryPrefix" name ="stateLryPrefix" ng-model="newState.stateLryPrefix"   ></td>
			</tr>
			
			<tr>
				<td>STD*</td>
				<td><input type ="text" id="stateSTD" name ="stateSTD" ng-model="newState.stateSTD"   ></td>
			</tr>
			
		 	<tr>
				<td><input type="submit"  value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>	
	
	
	
	<div id="StationDB" ng-hide = "StationFlag">
	<form name="StationForm" ng-submit="saveStation(StationForm,station)">
		<table>
			<tr>
				<td>Station *</td>
				<td><input type ="text" id="mulStnStnCode" name ="mulStnStnCode" ng-model="station.mulStnStnCode" ng-click="OpenStationList()"  readonly></td>
				<td><input type="button" value="Add New Station" ng-click="openAddStationDB()"></td>
			</tr>
			
		 	<tr>
				<td><input type="submit"  value="Submit" ></td>
			</tr>
		</table>
		</form>
	</div>	
	

	<div id="AddStationDB" ng-hide = "AddStationFlag">
	<form name="AddStationForm" ng-submit="saveNewStation(AddStationForm,newStation)">
		<table>
			<tr>
				<td>Station Name*</td>
				<td><input type ="text" id="stnName" name ="stnName" ng-model="newStation.stnName"   ></td>
			</tr>
			
			<tr>
				<td>Station Code*</td>
				<td><input type ="text" id="stnCode" name ="stnCode" ng-model="newStation.stnCode"   ></td>
			</tr>
			
			<tr>
				<td>State Code*</td>
				<td><input type ="text" id="stateCode" name ="stateCode" ng-model="newStation.stateCode"   ></td>
			</tr>
			
			<tr>
				<td>Station District*</td>
				<td><input type ="text" id="stnDistrict" name ="stnDistrict" ng-model="newStation.stnDistrict"   ></td>
			</tr>
			
			<tr>
				<td>Station PIN*</td>
				<td><input type ="text" id="stnPin" name ="stnPin" ng-model="newStation.stnPin"   ></td>
			</tr>
			
		 	<tr>
				<td><input type="submit"  value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>
		
	<div id="AddVehicleTypeDB" ng-hide = "AddVehicleTypeFlag">
	<form name="AddVehicleForm" ng-submit="saveNewvehicletype(AddVehicleForm,vt)">
		<table>
			<tr>
				<td>Service Type *</td>
				<td><input type ="text" id="vtServiceType" name ="vtServiceType" ng-model="vt.vtServiceType" ng-minlength="1" ng-maxlength="30" ></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="vtVehicleType" name ="vtVehicleType" ng-model="vt.vtVehicleType"  ></td>
			</tr>
			
			<tr>
				<td>Code *</td>
				<td><input type ="text" id="vtCode" name ="vtCode" ng-model="vt.vtCode"  ></td>
			</tr>
			
			<tr>
				<td>Load Limit *</td>
				<td><input type ="number" id="vtLoadLimit" name ="vtLoadLimit" ng-model="vt.vtLoadLimit"  min ="1"  ></td>
			</tr>
			
			
			<tr>
				<td>Guarantee Weight *</td>
				<td><input type ="number" id="vtGuaranteeWt" name ="vtGuaranteeWt" ng-model="vt.vtGuaranteeWt"  step="0.01" min="0.01" ></td>
			</tr>
			
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>
	

<div id ="pendingOwnerDB" ng-hide = "pendingOwnerDBFlag">
	<input type="text" name="filterOwnBrkName" ng-model="filterOwnBrkName" placeholder="Search"> 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Name</th>
				<th>Mobile No.</th>	
				<th>Type</th>			
			</tr> 	
		  	<tr ng-repeat="ownBrk in pendingOwnBrkList  | filter:filterOwnBrkName">
		 	 	<td><input type="radio"  name="ownBrkName" id="ownBrkName" class="branchCode"  value="{{ ownBrk.ownBrkName }}" ng-model="ownBrkName" ng-click="savOwnBrk(ownBrk)"></td>
              	<td>{{ ownBrk.ownBrkName }}</td>
              	<td>{{ ownBrk.ownBrkPhoneNo }}</td>
              	<td>{{ ownBrk.type }}</td>              	
          	</tr>
       </table>         
</div>

<div id ="branchCodeDB" ng-hide = "branchCodeFlag">
	<input type="text" name="filterBranchCode" ng-model="filterBranchCode" placeholder="Search"> 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Branch FaCode</th>
				<th>Branch Name</th>
				<th>Branch Pin</th>
			</tr> 	
		  	<tr ng-repeat="branch in branchList  | filter:filterBranchCode">
		 	 	<td><input type="radio"  name="branchCode" id="branchCode" class="branchCode"  value="{{ branch.branchCode }}" ng-model="branchCode" ng-click="savBranchCode(branch)"></td>
              	<td>{{ branch.branchFaCode }}</td>
              	<td>{{ branch.branchName }}</td>
              	<td>{{ branch.branchPin }}</td>
          	</tr>
       </table>         
</div>
	
	<div id ="StationCodeDB" ng-hide = "StationCodeFlag">
	
	<input type="text" name="filterStationCode" ng-model="filterStationCode" placeholder="Search">
 	  
 	  <table>
 	 		<tr>
				<th></th> 
				<th>Station Code</th>
				<th>Station Name</th>
				<th>Station District</th>
			</tr>
 	 
		  <tr ng-repeat="station in stationList  | filter:filterStationCode">
		 	  <td><input type="radio"  name="station" id="station" class="station"  value="{{ station }}" ng-model="stnCode2" ng-click="saveStationCode(station)"></td>
              <td>{{ station.stnCode }}</td>
              <td>{{ station.stnName }}</td>
              <td>{{ station.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="StateCodeDB" ng-hide = "StateCodeFlag">
	
	<input type="text" name="filterStateCode" ng-model="filterStateCode" placeholder="Search">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>State Code</th>			
				<th>State Name</th>
			</tr>
		  
		  <tr ng-repeat="state in listState  | filter:filterStateCode">
		 	  <td><input type="radio"  name="state" id="state"  value="{{ state }}" ng-model="stateCode" ng-click="saveStateCode(state)"></td>
              <td>{{ state.stateCode }}</td>
              <td>{{ state.stateName }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="VehicleTypeDB" ng-hide = "VehicleTypeFlag">
	
	<input type="text" name="filterVehicleType" ng-model="filterVehicleType" placeholder="Search">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				<th>Vehicle Type</th>
				<th>Service Type</th>
			</tr>
 	  
		  <tr ng-repeat="vt in vtList  | filter:filterVehicleType">
		 	  <td><input type="radio"  name="vtCode" id="vtCode" class="vtCode"  value="{{ vt }}" ng-model="vtCode" ng-click="saveVehicleType(vt)"></td>
              <td>{{ vt.vtCode }}</td>
              <td>{{ vt.vtVehicleType }}</td>
              <td>{{vt.vtServiceType}}</td>
          </tr>
         </table>       
	</div>
	
<div id="ownerDB" ng-hide="ownerFlag">
<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div> 
	<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
	<table class="table-hover table-bordered table-bordered">
		<h2>Owner Details</h2>
		<tr>
			<td>Branch Code</td>
			<td>{{owner.branchCode}}</td>
		</tr>
		
		<tr>
			<td>Name</td>
			<td>{{owner.ownName}}</td>
		</tr>
		
		<tr>
			<td>Union</td>
			<td>{{owner.ownUnion}}</td>
		</tr>
		
		<tr>
			<td>Email-ID</td>
			<td>{{owner.ownEmailId}}</td>
		</tr>
		
		<tr>
			<td>Pan Date</td>
			<td>{{owner.ownPanDt}}</td>
		</tr>
		
		<tr>
			<td>Pan Name</td>
			<td>{{owner.ownPanName}}</td>
		</tr>
		
		<tr>
			<td>Vehicle Type</td>
			<td>{{owner.ownVehicleType}}</td>
		</tr>
		
		<tr>
			<td>PassPort Number </td>
			<td>{{owner.ownPPNo}}</td>
		</tr>
		
		<tr>
			<td>Voter Id</td>
			<td>{{owner.ownVoterId}}</td>
		</tr>
		
		<tr>
			<td>Service Tax No</td>
			<td>{{owner.ownSrvTaxNo}}</td>
		</tr>
		
		<tr>
			<td>Firm Reg No</td>
			<td>{{owner.ownFirmRegNo}}</td>
		</tr>
		
		<tr>
			<td>Reg Place</td>
			<td>{{owner.ownRegPlace}}</td>
		</tr>
		
		<tr>
			<td>Company Id Number </td>
			<td>{{owner.ownCIN}}</td>
		</tr>
		
		<tr>
			<td>Business Card </td>
			<td>{{owner.ownBsnCard}}</td>
		</tr>
		
		<tr>
			<td>Firm Type</td>
			<td>{{owner.ownFirmType}}</td>
		</tr>
		
		<tr>
			<td>Company Established Date</td>
			<td>{{owner.ownComStbDt}}</td>
		</tr>
		
		<tr>
			<td>PAN DOB</td>
			<td>{{owner.ownPanDOB}}</td>
		</tr>
		
		<tr>
			<td>Activate Date</td>
			<td>{{owner.ownActiveDt}}</td>
		</tr> 
		</table>
		
		<table  class="table-hover table-bordered table-bordered">
		<h2>Current Address</h2>
		<tr>
			<td>Address</td>
			<td>{{currentAddress.completeAdd}}</td>
		</tr>
		
		<tr>
			<td>City</td>
			<td>{{currentAddress.addCity}}</td>
		</tr>
		
		<tr>
			<td>State</td>
			<td>{{currentAddress.addState}}</td>
		</tr>
		
		<tr>
			<td>Pin</td>
			<td>{{currentAddress.addPin}}</td>
		</tr>
		</table>
		
		<div ng-show="showRAFlag">
		<table  class="table-hover table-bordered table-bordered">
		<h2>Register Address</h2>
		<tr>
			<td>Address</td>
			<td>{{registerAddress.completeAdd}}</td>
		</tr>
		
		<tr>
			<td>City</td>
			<td>{{registerAddress.addCity}}</td>
		</tr>
		
		<tr>
			<td>State</td>
			<td>{{registerAddress.addState}}</td>
		</tr>
		
		<tr>
			<td>Pin</td>
			<td>{{registerAddress.addPin}}</td>
		</tr>
		</table>
		</div>
		
		<div ng-show="showOAFlag">
		<table  class="table-hover table-bordered table-bordered">
		<h2>Other Address</h2>
		<tr>
			<td>Address</td>
			<td>{{otherAddress.completeAdd}}</td>
		</tr>
		
		<tr>
			<td>City</td>
			<td>{{otherAddress.addCity}}</td>
		</tr>
		
		<tr>
			<td>State</td>
			<td>{{otherAddress.addState}}</td>
		</tr>
		
		<tr>
			<td>Pin</td>
			<td>{{otherAddress.addPin}}</td>
		</tr>
		</table>
		</div>
		
		<table  class="table-hover table-bordered table-bordered">
		<h2>Contact Person</h2>
		
		<tr>
			<td>Name</td>
			<td>{{contPerson.cpName}}</td>
		</tr>
		
		<tr>
			<td>Phone</td>
			<td>{{contPerson.cpPhone}}</td>
		</tr>
		
		<tr>
			<td>Pan No</td>
			<td>{{contPerson.cpPanNo}}</td>
		</tr>
		
		<tr>
			<td>Mobile </td>
			<td>{{contPerson.cpMobile}}</td>
		</tr>
	</table>
	
	<table>
		<tr>
			<td><h4>Station</h4></td>
		</tr>
		<tr ng-repeat="station in stnList">
		    <td>{{station.mulStnStnCode}}</td>
		 </tr>   
	</table>
	
	<table>
		<tr>
			<td><h4>State</h4></td>
		</tr>
		<tr ng-repeat="state in stateList">
		    <td>{{state.mulStateStateCode}}</td>
		 </tr>   
	</table>
	
	<input type="button" value="Cancel" ng-click="back()">
	<input type="button" value="Save" id="saveBtnId" ng-click="saveOwner(owner,currentAddress,contPerson)">
</div>
</div>
</div>
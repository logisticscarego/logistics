<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="newVoucherForm" ng-submit=voucherSubmit(newVoucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly  >
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    <div class="row">
		     	
		   
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.cashStmtStatus.cssCsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div>
			   		 <div class="col s4 input-field">
				    		<select name="voucherType" id="voucherTypeId" ng-model="vs.voucherType" ng-required="true">
								<option value="CashWithdraw">Cash Withdrawal</option>
								<option value="CashReceipt">Cash Receipt</option>
							</select>
							<label>Voucher Type </label>
					</div>
			      	<div class="col s4 input-field">
			    		<select name="chequeType" id="chequeTypeId" ng-model="vs.chequeType" required  ng-change="selectChqType()">
							<option value='C'>Computerized</option>
							<option value='M'>Manual</option>
						</select>
						<label>Cheque Type </label>
					</div>
		    </div>
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bankCodeId" name ="bankCode" ng-model="vs.bankCode"  ng-click = " openBankCodeDB()" readonly ng-required="true" >
		       			<label for="code">Bank Code</label>	
		       		</div>
		  
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="chequeNoId" name ="chequeNo" ng-model="vs.chequeLeaves.chqLChqNo" readonly disabled="disabled">
		       			<label for="code">Cheque No</label>	
		       		</div>
		  
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="amountId" name ="amount" ng-model="vs.amount" step = "0.01"  ng-blur="checkAmount()" ng-required="true" >
		       			<label for="code">Amount</label>	
		       		</div>
		    </div>
		    
	      	 <div class="row">
	     		<div class="col s4 input-field">
	       				<input type ="text" id="payToId" name ="payToName" ng-model="vs.payTo"  ng-required="true" >
	       			<label for="code">Pay To</label>	
	       		</div>
	   		 </div>
	   	
	   		 <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="vs.desc" ng-required="true"></textarea>
 					<label>Description</label>
				</div>
    		 </div>	
	   		 
	   		  <div class="row">
	     		<div class="col s4 input-field">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       		<div class="col s4 input-field">
	       				<input type="button" value="closing voucher" ng-click="closeVoucher()">
	       		</div>
	   		 </div>
		  </form>
    </div>
    
   
</div>

<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="revMrForm" ng-submit=revMrSubmit(revMrForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
			
					<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="csDtId" name ="csDtName" ng-model="csDt" ng-blur="chngDt()" readonly ng-required="true" >
		       			<label for="code">CS Date</label>	
		       		</div>
		       			
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="actCustId" name ="actCustName" ng-model="actCust.custName" ng-click="selectActCust()" ng-required="true" readonly>
		       			<label for="code">Customer Name</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="brhId" name ="brhName" ng-model="branch.branchName" ng-click="selectBrh()" ng-required="true" readonly>
		       			<label for="code">MR Branch</label>	
		       		</div>
		       		
		    </div>
		    
		    <div class="row">
		    	
		    	<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="oaId" name ="oaName" ng-model="onAcc.mrNo" ng-click="selectOnAcc()" ng-required="true" readonly>
		       			<label for="code">OnAcc MR No.</label>	
		       	</div>
		       		
		       		
		    	<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="custId" name ="custName" ng-model="customer.custName" ng-click="selectCust()" ng-required="true" readonly>
		       			<label for="code">Reversal Customer</label>	
		       	</div>
		    </div>
		    
		     <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="mrDesc"></textarea>
 					<label>Description</label>
				</div>
         	</div>	
         
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" id="saveId" value="submit" >
	       		</div>
	      </div>		
	     
	</form>
	</div> 
	
	
	<div id ="selCustId" ng-hide="selCustFlag">
		  <input type="text" name="filterCust1" ng-model="filterCust1" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Customer Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cust in custList | filter:filterCust1">
		 	  <td><input type="radio"  name="cust"   value="{{ cust }}" ng-model="custCode" ng-click="saveCust(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="selActCustId" ng-hide="selActCustFlag">
		  <input type="text" name="filterCust" ng-model="filterCust" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Customer Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cust in custList | filter:filterCust">
		 	  <td><input type="radio"  name="cust"  value="{{ cust }}" ng-model="custCode" ng-click="saveActCust(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="selBrhId" ng-hide="selBrhFlag">
		  <input type="text" name="filterBrh" ng-model="filterBrh" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Branch Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="brh in brhList | filter:filterBrh">
		 	  <td><input type="radio"  name="brh"  value="{{ brh }}" ng-model="brhCode" ng-click="saveBranch(brh)"></td>
              <td>{{brh.branchName}}</td>
              <td>{{brh.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="selOAId" ng-hide="selOAFlag">
		  <input type="text" name="filterMR" ng-model="filterMR" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>MR NO</th>
 	  	  	  <th>Date</th>
 	  	  	  <th>Net Payment</th>
 	  	  	  <th>Rem Amt</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="onAcc in onAccList | filter:filterMR">
		 	  <td><input type="radio"  name="onAcc"   value="{{ onAcc }}" ng-model="onAccCode" ng-click="saveMR(onAcc)"></td>
              <td>{{onAcc.mrNo}}</td>
              <td>{{onAcc.mrDate | date:'dd/MM/yyyy'}}</td>
              <td>{{onAcc.mrNetAmt}}</td>
              <td>{{onAcc.mrRemAmt}}</td>
          </tr>
      </table> 
	</div>
</div>	
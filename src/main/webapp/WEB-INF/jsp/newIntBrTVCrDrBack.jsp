<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="newVoucherForm" ng-submit=voucherSubmit(newVoucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly  >
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.cashStmtStatus.cssCsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div>
			   		 <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code">Voucher Type</label>	
		       		</div>
		       		<div class="col s4 input-field">
		       			<input class="validate" type ="button" id="vouchEntId" value="Single Voucher Entry" ng-click="singleVoucher()" readonly ng-required="true" >
		       		</div>
		       		
		    </div>
		    
		     <div class="row">
		     	<div ng-show="tibList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">Voucher Details</caption>
						<tr class="rowclr">
							<th class="colclr">S.No.</th>
							<th class="colclr">FACODE</th>
							<th class="colclr">AMOUNT</th>
							<th class="colclr">DORC</th>
							<th class="colclr">DESCRIPTION</th>
							<th class="colclr">ACTION</th>
						</tr>
						<tr class="tbl" ng-repeat="tibTv in tibList">
							<td class="rowcel">{{$index+1}}</td>
							<td class="rowcel">{{tibTv.tibFaCode}}</td>
							<td class="rowcel">{{tibTv.tibAmt}}</td>
							<td class="rowcel">{{tibTv.tibDOrC}}</td>
							<td class="rowcel">{{tibTv.tibDesc}}</td>
							<td class="rowcel"><input type="button" value="remove" ng-click="removeTIB($index,tibTv)" /></td>
						</tr>
						<tr class="tbl" >							
							<td class="rowcel" colspan="2">Total Amount</td>
							<td class="rowcel">{{tibAmtSum}}</td>
							<td class="rowcel"></td>
							<td class="rowcel"></td>
							<td class="rowcel"></td>							
						</tr>
					</table>
				</div>
		     </div>
	
	   		<!--  <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="vs.desc" ng-required="true"></textarea>
 					<label>Description</label>
				</div>
    		 </div>	 -->
	   		 
	   		  <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	   		 </div>
		  </form>
    </div>
    
    
    <div id="sVouchDB" ng-hide="sVouchDBFlag">
   		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="sVouchForm" ng-submit="submitSVouch(sVouchForm)">
			 <div class="row">
					<table>
						<tr>
							<td>Branch FACode</td>
							<td><input class="validate" type ="text" name="brFaCodeName" id="brFaCodeId" ng-model="tib.tibFaCode" ng-click="openFaCodeBD()" ng-required="true" readonly></td>
						</tr>
						<tr>
							<td>Amount</td>
							<td><input class="validate" type ="number" name="crAmtName" id="crAmtId" ng-model="tib.tibAmt" ng-required="true"></td>
						</tr>
						<tr>
							<td>Debit/Credit</td>
							<td><input class="validate" type ="text" name="dOrCName" id="dOrCId" ng-model="tib.tibDOrC" ng-init="tib.tibDOrC = 'C'" ng-required="true" readonly></td>
						</tr>
						<tr>
							<td>Description</td>
							<td>
								<i class="mdi-editor-mode-edit prefix"></i>
 								<textarea id="descId" name ="descName" ng-model="tib.tibDesc" ng-required="true"></textarea>
 							</td>
						</tr>
					</table>
		     </div>
		     <div class="row">
				    <div class="input-field col s12 center">
						<input class="validate" type ="submit" value="submit">
					</div>
		     </div>
	     </form>
   </div>
   
   
   <div id="brListDB" ng-hide=brListDBFlag>
    	<input type="text" name="filterTextbox" ng-model="filterTextbox2" placeholder="Search....">
	 	  <table>
	 	  	  <tr>
	 	  	  	  <th></th>
	 	  	  	  <th>Branch Name</th>
	 	  	  	  <th>FaCode</th>
	 	  	  </tr>
	 	  
			  <tr ng-repeat="branch in branchList | filter:filterTextbox2">
			 	  <td><input type="radio"  name="branch"   value="{{ branch }}" ng-model="branchCode" ng-click="saveBrCode(branch)"></td>
	              <td>{{branch.branchName}}</td>
	              <td>{{branch.branchFaCode}}</td>
	          </tr>
	      </table> 
    </div>
    
    
    
    <div id="saveVsDB" ng-hide="saveVsFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{vs.branch.branchFaCode}}</td>
				</tr>
				
				<tr>
					<td>Branch Name</td>
					<td>{{vs.branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Date</td>
					<td>{{dateTemp}}</td>
				</tr>
				
				<tr>
					<td>Sheet No</td>
					<td>{{vs.cashStmtStatus.cssCsNo}}</td>
				</tr>
				
				<tr>
					<td>Voucher Type</td>
					<td>{{vs.voucherType}}</td>
				</tr>
			
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" id="saveId" ng-click="saveVS()"/>
		</div>
	</div>
	
</div>
    
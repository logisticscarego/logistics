<div ng-show="operatorLogin || superAdminLogin" title="Branch Stock Information" style="background-color: white;">
	<style>
	
	.tbl{text-align:center; 
		  color: #B5DAD4;
		
		border:1px solid rgba(255, 255, 255, 0.47);
		-moz-border-right: 1px solid rgba(255, 255, 255, 0.47);
		-webkit-border-right: 1px solid rgba(255, 255, 255, 0.47);}
		
		.rowcelwhite
		{
		text-align: center;
		color: #fff;
		border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    padding: 0px;
	    
	    }
	    .rowcelheader
	    {
		text-align: center;
	    color: #B5DAD4;
	    border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    font-weight:bold;
	    padding:3px;
	    
	    }
	    
		.align{
	    text-align:center;
	    font-size:20px;
	    }
	    .cursor
	    {
	    cursor:pointer;
	    }
		:hover.cursor
		{
		background-color:rgba(73, 156, 145, 0.25);
		}
		
		input.ng-invalid {
   		 	background-color:#B7F3EE;
		}
				
	</style>
	<div class="row noprint" style="background-color: white;">
		
		<!-- Find By Branch Form -->
		<form name="StkEnqForm" ng-submit="submitStkEnq(StkEnqForm)" class="col s12 card" style="align: center; padding-top: 40px;">
			<div class="row" style="background-color: white;">
		       	<div class="col s3 input-field" style="width: 17%;">
		       		<input class="validate" style="background-color : #f1f1f1; color:black; " type ="text" id="branchName" name="branchName" ng-model="memo.branchName" ng-init="memo.branchName=currentBranch" required="required" readonly="readonly">		       		
					<label style="color:black;">Branch Name</label>
				</div>
				<div class="col s3 input-field" style="width: 17%;">
		       		<input class="validate" style="background-color: #f1f1f1; color:black;" type ="text" id="memoNo" name="memoNo" ng-model="memo.memoNo" required="required" readonly="readonly">		       		
					<label style="color:black;">Memo No.</label>
				</div>       	
				<div class="col s3 input-field" style="width: 18%;">
		       		<input class="validate" style="background-color: #f1f1f1; color:black;" type ="date" id="memoDate" name="memoDate" ng-model="memo.memoDate" required="required" readonly="readonly">		       		
					<label style="color:black;">Memo Date</label>
				</div>
				<div class="col s3 input-field" style="width: 17%;">
		       		<input class="validate" style="background-color: #f1f1f1; color:black;" type ="text" id="memoFrom" name="memoFrom" ng-model="memo.fromBranch" ng-init="memo.fromBranch = currentBranch" required="required" readonly="readonly">		       		
					<label style="color:black;">From</label>
				</div>
				<div class="col s3 input-field" style="width: 17%;">
		       		<input class="validate" type ="text" id="memoTo" style="" name="memoTo" ng-model="memo.toBranch" ng-click="OpenBranchDB()" ng-required="true" readonly="readonly">
					<label style="color:black;">To</label>
				</div>
				
			</div>
			
			
			<div class="row" style="background-color: white;">
		       	<div class="col s3 input-field" style="width: 17%;">
		       		<input class="validate" type ="text" ng-model="memoNoToPrint" style = "width: 65%; background-color: #E6E6FA;color:black;" palceholder="AR No.">		       		
					<label style="color:black;">Memo No. To take Print</label>
				</div>			
				<div class="col s3 input-field" style="width: 17%;">
		       		<input class="validate" type ="submit" value="Take Print" ng-click="takePrintDB()" style="background-color: #800000; font-style: oblique;">		       		
				</div>			
			</div>
			
		</form>
		
	</div>
	
	<div id ="branchDB" ng-hide="branchDBFlag" class="noprint">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="branch in branchList | filter:filterTextbox1">
		 	  <td><input type="radio" name="branch" value="{{ branch }}" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id="memoPrint" ng-hide="memoPrintFlag">
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">Do you want to take print ?</h5></div>
		</div>
		<div>
			<div class="col s12 center">				
				<a class="btn white-text" ng-click="printVs()">Yes</a>
				<a class="btn white-text" ng-click="cancelPrint()">Cancel</a>
			</div>
		</div>
	</div>
	
	
	<div id="finalSubmitDB" ng-hide="finalSubmitDBFlag">
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">Do you want to save this AR Memo ?</h5></div>
		</div>
		<div>
			<div class="col s12 center">				
				<a class="btn white-text" ng-click="submitFinalAr()">Yes</a>
				<a class="btn white-text" ng-click="cancelAr()">Cancel</a>
			</div>
		</div>
	</div>
	

	
	
	
	<div class = "noprint">
		<form name="arForm">
			<table class="table">
	 	  	  	<tr>
	 	  	  		<th class="rowcelheader" style="background-color: #D3D3D3; color:black; width: 8%;">Delete</th>
	 	  	  		<th class="rowcelheader" style="background-color: #D3D3D3; color:black; width: 8%;">AR/SDER</th>
	 	  	  		<th class="rowcelheader" style="background-color: #D3D3D3; color:black; width: 8%;">Dly. Date</th>
	 	  	  		<th class="rowcelheader" style="background-color: #D3D3D3; color:black;">
	 	  	  			<table class="table" style="margin: 0;">
	 	  	  				<tr>
	 	  	  					<th class="rowcelheader" style="background-color: #D3D3D3; color:black;width: 16%; ">Cnmt No.</th>
	 	  	  					<th class="rowcelheader" style="background-color: #D3D3D3; color:black;width: 8%;">Cnmt Date</th>
	 	  	  					<th class="rowcelheader" style="background-color: #D3D3D3; color:black;width: 14%;">From Sta.</th>
	 	  	  					<th class="rowcelheader" style="background-color: #D3D3D3; color:black;width: 14%;">To Sta.</th>
	 	  	  					<th class="rowcelheader" style="background-color: #D3D3D3; color:black;width: 6%;">PKG</th>
	 	  	  					<th class="rowcelheader" style="background-color: #D3D3D3; color:black;width: 6%;">Weight</th>
	 	  	  					<th class="rowcelheader" style="background-color: #D3D3D3; color:black;width: 20%;">Remark</th>
	 	  	  				</tr>
	 	  	  			</table>
	 	  	  		</th>	 	  	  		
	 	  	  	</tr>
	 	  
	 	  
	 	  	  	<tr ng-repeat = "ar in arList" style="border:3px solid #f1f1f1;"> 
	 	  	  		<td class="rowcelwhite" style="color:white; width: 8%;">
	 	  	  			<input type="button" value="Delete" ng-click="deleteAr($index)" style="background-color : #800000;">
	 	  	  		</td>
					<td class="rowcelwhite" style="color:white; width: 8%;">{{ar.arCode}}</td>
					<td class="rowcelwhite" style="color:white; width: 8%;">{{ar.arDt}}</td>
					<td class="rowcelwhite">
						<table class="table" style="margin: 0;">
							<tr ng-repeat="cnmt in ar.cnmtDt">								
								<td class="rowcelwhite" style="color:white; width: 16%;">{{cnmt.cnmtCode}}</td>								
								<td class="rowcelwhite" style="color:white; width: 8%;">{{cnmt.cnmtDt}}</td>
								<td class="rowcelwhite" style="color:white; width: 14%;">{{cnmt.fromStation}}</td>								
								<td class="rowcelwhite" style="color:white; width: 14%;">{{cnmt.toStation}}</td>
								<td class="rowcelwhite" style="color:white; width: 6%;">{{cnmt.cnmtNoOfPkg}}</td>
								<td class="rowcelwhite" style="color:white; width: 6%;">{{cnmt.cnmtActualWt}}</td>
								<td class="rowcelwhite" style="color:white; width: 20%;">
									<input class="validate" type ="text" required="required" ng-model="cnmt.arRemark" ng-value="cnmt.arRemark">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
				<tr style="height: 30px;">					
				</tr>	
							
	          	<tr>
					<td class="rowcelwhite" colspan="10" style="text-align: left;">
						<div style="float:left;">
							<input class="validate" type ="text" id="memoArNoFetch" ng-model="memoArNo" ng-blur="getArDetail()" style = "width: 65%; background-color: #E6E6FA;color:black;" palceholder="AR No.">							
						</div>					
						<div style="float: right;">
							<input class="validate" id="finalSubmit" ng-show="arList.length > 0" type ="submit" value="Final Submit" ng-click="saveAr(arForm)" style="background-color: #800000;">						
						</div>
					</td>
	          	</tr>
	      	</table> 
	      </form>
	</div>	
	
	
	
	
		
	
	<div id="printTable" style="width: 100%;display: none;">
	
		<center>
			<h5 style="color:black;">Care Go Logistics Pvt. Ltd.</h5>
			<h6 style="color:black;">Delivery Acknowledge Forwarding Memo</h6>
			<div style="width: 100%">
				<div style="width:50%; float: left; text-align: left;">
					<table style="width: 55%;">
						<tr><th style="padding: 5px; color: black; font-size: 12px;text-align: left;">Branch Name</th><th style="padding: 5px; color: black;font-size: 12px;"> : </th><td style="padding: 5px; color: black;font-size: 12px;text-align: left;">{{memo.branchName}}</td></tr>
						<tr><th style="padding: 5px; color: black; font-size: 12px;text-align: left;">Memo No.</th><th style="padding: 5px; color: black;font-size: 12px;"> : </th><td style="padding: 5px; color: black;font-size: 12px;text-align: left;">{{printMemoNo}}</td></tr>
						<tr><th style="padding: 5px; color: black; font-size: 12px;text-align: left;">Memo Date</th><th style="padding: 5px; color: black;font-size: 12px;"> : </th><td style="padding: 5px; color: black;font-size: 12px;text-align: left;">{{date}}</td></tr>
					</table>
				</div>
				<div style="width:50%; float: right; text-align: right;">
					<table style="width: 55%; float: right;">
						<tr><th style="padding: 5px; color: black;font-size: 12px;">From Branch</th><th style="padding: 5px; color: black;font-size: 12px;"> : </th><td style="padding: 5px; color: black;font-size: 12px;">{{memo.branchName}}</td></tr>
						<tr><th style="padding: 5px; color: black;font-size: 12px;">To Branch</th><th style="padding: 5px; color: black;font-size: 12px;"> : </th><td style="padding: 5px; color: black;font-size: 12px;">{{memo.toBranch}}</td></tr>
					</table>
				</div>
			</div>
			
		<table style="width: 100%; width: 100%;border-collapse: separate;border-spacing: 0px;">				
	 	  	  	<tr>
	 	  	  		<td style="border-top: 1px solid grey;border-bottom: 1px solid grey; border-collapse: collapse; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;text-align: left;">AR/SDER</td>
	 	  	  		<td style="border-top: 1px solid grey;border-bottom: 1px solid grey; border-collapse: collapse; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;text-align: left;">Dly. Date</td>
	 	  	  		<td style="border-top: 1px solid grey;border-bottom: 1px solid grey; border-collapse: collapse; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;text-align: left;">Cnmt No.</td>
	 	  	  		<td style="border-top: 1px solid grey;border-bottom: 1px solid grey; border-collapse: collapse; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;text-align: left;">Cnmt Date</td>
	 	  	  		<td style="border-top: 1px solid grey;border-bottom: 1px solid grey; border-collapse: collapse; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;text-align: left;">From Station</td>
	 	  	  		<td style="border-top: 1px solid grey;border-bottom: 1px solid grey; border-collapse: collapse; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;text-align: left;">To Station</td>
	 	  	  		<td style="border-top: 1px solid grey;border-bottom: 1px solid grey; border-collapse: collapse; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;text-align: left;">PKG</td>
	 	  	  		<td style="border-top: 1px solid grey;border-bottom: 1px solid grey; border-collapse: collapse; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;text-align: left;">Weight</td>
	 	  	  		<td style="border-top: 1px solid grey;border-bottom: 1px solid grey; border-collapse: collapse; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;text-align: left;">Remark</td>
	 	  	  	</tr>	 	  
	 	  	  	<tr ng-repeat = "ar in printList"> 	  	  		
					<td style="border-bottom: 1px solid grey; border-collapse: separate; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;">{{ar.arCode}}</td>
					<td style="border-bottom: 1px solid grey; border-collapse: separate; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;">{{ar.arDt}}</td>
					<td style="border-bottom: 1px solid grey; border-collapse: separate; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;">{{ar.cnmtCode}}</td>
					<td style="border-bottom: 1px solid grey; border-collapse: separate; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;">{{ar.cnmtDt}}</td>
					<td style="border-bottom: 1px solid grey; border-collapse: separate; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;">{{ar.fromStation}}</td>
					<td style="border-bottom: 1px solid grey; border-collapse: separate; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;">{{ar.toStation}}</td>
					<td style="border-bottom: 1px solid grey; border-collapse: separate; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;">{{ar.cnmtNoOfPkg}}</td>
					<td style="border-bottom: 1px solid grey; border-collapse: separate; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;">{{ar.cnmtActualWt}}</td>
					<td style="border-bottom: 1px solid grey; border-collapse: separate; padding: 5px; color: black; width: 10%;font-size:9px;font-family:Arial;">{{ar.arRemark}}</td>
				</tr>	
							
	      	</table> 
	      	</center>
	      	
	      	<div style="margin-top: 30px;">
	      		<table>
	      			<tr><td style="color: black;font-size: 12px;">Signature</td><td> : </td></tr>
	      			<tr style="margin-top: 15px;"><td style="color: black;font-size: 12px;">Staff Code</td><td> : </td></tr>
	      		</table>
	      	</div>
	
		
	</div>

</div>
<div ng-show="operatorLogin || superAdminLogin">
<title>View CNMT</title>
<div class="row">
	<div class="col s1 hide-on-med-and-down">&nbsp;</div>
	<div class="col s10 card"
		style="align: center; margin-top: 40px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">

		<div class="input-field col s6">
			<input type="text" id="cnmtCodeId" name="cnmtCodeName" ng-model="cnmt.cnmtCode" required" /> 
			<label>Enter CNMT Code</label>
		</div>

		<div class="input-field col s6 center">
			<input class="btn waves-effect waves-light" type="submit" value="Submit" ng-click="getCnmt()">
		</div>
	</div>

</div>

<div class="row" ng-show="showCnmtFlag">
	<div class="row noprint col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
		<form name="cnmtForm" ng-submit="submitCnmt(cnmtForm,cnmt)">
			<div class="row">
				<div class="input-field col s4">
					<input type="text" id="branch" name="branchName" ng-model="branch.branchName" ng-click="openBranchDB()" required readonly>
					<label>Branch</label>
				</div>
				
				<div class="input-field col s4">
					<input class="validate" type ="text" name ="custName" ng-model="cust.custName" ng-click="openCustDB()" readonly required>
	       			<label>Customer Code</label>
				</div>
				<div class="input-field col s4">
					<input type="number" name="cnmtVOGName" step="0.01" min="0.00" ng-model="cnmt.cnmtVOG" ng-minlength="1" ng-maxlength="7" ng-required="true"> 
					<label>Value Of Goods</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s3">
					<input type="date" name="cnmtDtName" id="cnmtDt" ng-model="cnmt.cnmtDt"	ng-required="true" > 
					<label>Date</label>
				</div>
				<div class="input-field col s3">
				 	<input class="validate" type ="text" name ="frmStnName" ng-model="frmStn.stnName" ng-click="openFrmStnDB()" readonly  ng-required="true">
	       			<label>From Station</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type ="text" name ="toStnName" ng-model="toStn.stnName" ng-click="openToStnDB()" readonly  ng-required="true">
	       			<label>To Station</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type="text" name="contCodeName" ng-model="cont.contCode" ng-click="openContDB()" readonly ng-required="true"> 
					<label>Contract	Code</label>
				</div>
				
	
			</div>
			<div class="row">
				<div class="input-field col s3">
					<input class="validate" type ="text" name ="cngnorName" ng-model="cngnor.custName" ng-click="openCngnorDB()" readonly required>
		        	<label>Consignor</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type ="text" name ="cngneeName" ng-model="cngnee.custName" ng-click="openCngneeDB()" readonly required>
		        	<label>Consignee</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type ="text" name ="vehicleName" ng-model="cnmt.cnmtVehicleType" ng-click="openVehicleTypeDB()" readonly required>
					<label>Vehicle Type</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type="text" name="cnmtProductTypeName" ng-model="cnmt.cnmtProductType" ng-click="openProductTypeDB()" readonly >
					<label>Product Type</label>
				</div>
			</div>
	
			<div class="row">
				<div class="input-field col s3">
					<input type="number" name="cnmtActualWtName" id="cnmtActualWt" ng-model="cnmt.cnmtActualWt" step="0.00001" min="0.00000" required>
					<label>Actual Weight</label>
				</div>
				<div class="input-field col s3">
					<input type="text" value="Ton" readonly="readonly">
					<label>Unit</label>
				</div>
				<div class="input-field col s3">
					<input type="number" name="cnmtGuaranteeWtName" id="cnmtGuaranteeWt" ng-model="cnmt.cnmtGuaranteeWt" ng-keyup="calFreight()" step="0.000000001" min="0.00000000" required>
					<label>Guarantee Weight</label>
				</div>
				<div class="input-field col s3">
					<input type="text" value="Ton" readonly="readonly">
					<label>Unit</label>
				</div>
			</div>
	
			<div class="row">
				<div class="input-field col s3">
					<input type="number" name="cnmtKmName" id="cnmtKmId" ng-model="cnmt.cnmtKm" ng-minlength="1" ng-maxlength="7"> 
					<label>Kilometer</label>
				</div>
				<div class="input-field col s3">
					<input type="text" name="cnmtStateName" id="cnmtStateId" ng-model="cnmt.cnmtState">
					<label>State</label>
				</div>
				<div class="input-field col s3">
					<input type="text" name="cnmtDDLName" ng-model="cont.contDdl" readonly>
					<label>Door-to-Door Delivery</label>
				</div>
				<div class="input-field col s3">
				<input class="validate" type ="text" name ="cnmtPayAtName" ng-model="payAt.branchName" ng-click="openPayAtDB()" readonly="readonly">
	        		<label>Pay On</label>
				</div>
			</div>
	
			<div class="row">
				<div class="input-field col s3">
				<input class="validate" type ="text" name ="cnmtBillAtName" ng-model="billAt.branchName" ng-click="openBillAtDB()" readonly="readonly">
	        		<label>Bill	On</label>
				</div>
				<div class="input-field col s3">
					<input type="number" name="cnmtRateName" ng-model="cnmt.cnmtRate" ng-keyup="calFreight()" step="0.000000001" min="0.00000000" ng-required="true">
					<label>Rate</label>
				</div>
				<div class="input-field col s3">
					<input type="text" value="Per Ton" readonly="readonly">
					<label>Unit</label>
				</div>
				<div class="input-field col s3">
					<input type="number" name="cnmtFreightName" ng-model="cnmt.cnmtFreight" step="0.001" min="0.000" ng-required="true"> 
					<label>Freight</label>
				</div>
				
			</div>
	
			<div class="row">
				<div class="input-field col s3">
					<input type="number" name="cnmtTOTName" ng-model="cnmt.cnmtTOT" step="0.001" min="0.000">
					<label>Cnmt Total</label>
				</div>
				
				<div class="input-field col s3">
	     				
	       			<select name="cnmtDCName" ng-model="cnmt.cnmtDC" ng-init="cnmt.cnmtDC='cnmt.cnmtDC'" required>
						<option value="0 bill">FOC</option>
						<option value="1 bill">One bill</option>
						<option value="2 bill">Two bill</option>
						<option value="Direct Payment through CNMT">Direct Payment through CNMT</option>
						<option value="Twice Payment">Twice Payment</option>
						<option value="Partially through bill Partially through CNMT">Partially through bill Partially through CNMT</option>
					</select>
					<label>DC</label>
	   			</div>
				
				<div class="input-field col s3">
					<input type="number" name="cnmtNoOfPkgName" ng-model="cnmt.cnmtNoOfPkg" ng-minlength="1" ng-maxlength="7" ng-required="true">
					<label>No. Of Packages</label>
				</div>
	
				<div class="input-field col s3">
					<input type="text" name="cnmtCostGradeName" ng-model="cont.contCostGrade" readonly="readonly">
					<label>Cost Grade</label>
				</div>
				
			</div>
	
			<div class="row">
				<div class="input-field col s3">
					<input class="validate" type ="text" name ="empName" ng-model="emp.empName" ng-click="openEmpDB()" readonly required>
		        	<label>Employee Code</label>
				</div>
				
				<div class="input-field col s3">
					<input type="number" name="cnmtExtraExpName" ng-model="cnmt.cnmtExtraExp" step="0.01" min="0.00" ng-minlength="1" ng-maxlength="7">
					<label>Extra Expenses</label>
				</div>
				
				<div class="input-field col s3">
					<input type="date" name="cnmtDtOfDlyName" ng-model="cnmt.cnmtDtOfDly">
					<label>Date Of Delivery</label>
				</div>
				<div class="input-field col s3">
					<input type="button" value="Add Invoice Number" ng-click="openInvoiceNo()">
					<label>Invoice Number</label>
				</div>
			</div>
			
			<div class="row">
				<div class="input-field col s3">
					<input class="validate" type ="Date" name ="cnmtGateOutDt" ng-model="cnmt.cnmtGtOutDt" >
		        	<label>Gate Out Date</label>
				</div>
				
				<div class="input-field col s3">
	       			<select name="prBlCodeName" ng-model="cnmt.prBlCode" ng-init="cnmt.prBlCode='cnmt.prBlCode'" ng-change="changePrBlCode()" >
						<option value="1754">Primary</option>
						<option value="1755">Secoundary</option>
						<option value="1756">O&M</option>
					</select>
					<label>Rel(Primry/Secndry/O$M)</label>
	   			</div>
				
				<div class="input-field col s3">
					<input type="number" name="whCodeName" ng-model="cnmt.whCode" ng-click="getRelianceWhHouse()" ng-readonly="true" ng-required="whRequreFlag" >
					<label>Reliance WhareHouse Code</label>
				</div>
				
				<div class="input-field col s3">
					<input type="text" name="relChlnName" ng-model="cnmt.relChln">
					<label>Reliance Challan</label>
				</div>
				
			</div>

			
			<div class="row">
			
			<div class="input-field col s3">
	       			<select name="isTransName" ng-model="cnmt.trans" ng-init="cnmt.trans='cnmt.trans'"  >
						<option value="true">YES</option>
						<option value="false">NO</option>
					</select>
					<label>Transit</label>
	   			</div>
				
			
				<div class="col s3 input-field">
					<input class="teal white-text btn" type="file" file-model="cnmtImg" accept="application/pdf" > 
				</div>
				
				<div class="col s3 input-field">
					<a class="btn waves-effect waves-light white-text col s12" type="button" ng-click="uploadCnmtImg()">Upload Cnmt Image</a> 
				</div>
				
				<div class="col s3 input-field" ng-show="cnmtImageFlag">
					<a class="btn waves-effect waves-light white-text col s12" type="button" ng-click="showCnmtImg()">Show Ack</a> 
				</div>
				
				<div class="col s3 input-field" ng-hide="cnmtImageFlag">
					<a class="btn waves-effect waves-light white-text col s12" type="button" >No Ack</a> 
				</div>
			</div>
	
			<div class="row">
				<div ng-if="cnmt.cnmtInvoiceNo.length > 0">
					<table class="tblrow">
					
					<caption class="coltag tblrow">Invoices</caption>
						<tr class="rowclr">
							<th class="colclr">Cnmt Invoice Number</th>
							<th class="colclr">Cnmt Invoice Date</th>
							<th class="colclr">Action</th>
						</tr class="rowclr">
						
						<tr ng-repeat="invoices in cnmt.cnmtInvoiceNo">
							<td class="rowcel">{{invoices.invoiceNo}}</td>
							<td class="rowcel">{{invoices.date | date:'dd/MM/yy'}}</td>
							<td class="rowcel">
				            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="removeCnmtInv($index)" >
									<i class="mdi-action-delete white-text"></i>
								</a>
							</td>
						</tr>
					</table>
				</div>
			</div>
			
			<div class="row">
				<div class="col s12 center">
					<input type="submit" id="submitId" value="Submit">
				</div>
	
			</div>
		</form>
	</div>


</div>

	<div id ="cnmtDB" ng-hide="cnmtDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> CNMT No </th>
 	  	  </tr>
		  <tr ng-repeat="cnmtCode in cnmtList | filter:filterTextbox1">
		 	  <td><input type="radio" name="cnmtCode" value="{{ cnmt }}" ng-click="saveCnmtCode(cnmtCode)"></td>
              <td>{{cnmtCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="branchDB" ng-hide="branchDBFlag">
		<input type="text" name="filterTextbox2" ng-model="filterTextbox2" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="branch in branchList | filter:filterTextbox2">
		 	  <td><input type="radio" name="branch" value="{{ branch }}" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="custDB" ng-hide="custDBFlag">
		<input type="text" name="filterTextbox3" ng-model="filterTextbox3" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Cust Name </th>
 	  	  	  <th> Cust FaCode </th>
 	  	  	  <th> DailyContAllow </th>
 	  	  </tr>
		  <tr ng-repeat="cust in custList | filter:filterTextbox3">
		 	  <td><input type="radio" name="cust" value="{{ cust }}" ng-click="saveCust(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
              <td>{{cust.custIsDailyContAllow}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="frmStnDB" ng-hide="frmStnDBFlag">
		<input type="text" name="filterTextbox4" ng-model="filterTextbox4" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Stn Name </th>
 	  	  	  <th> Stn District </th>
 	  	  	  <th> Stn Pin </th>
 	  	  	  <th> Stn Code </th>
 	  	  </tr>
		  <tr ng-repeat="frmStn in stnList | filter:filterTextbox4">
		 	  <td><input type="radio" name="frmStn" value="{{ frmStn }}" ng-click="saveFrmStn(frmStn)"></td>
              <td>{{frmStn.stnName}}</td>
              <td>{{frmStn.stnDistrict}}</td>
              <td>{{frmStn.stnPin}}</td>
              <td>{{frmStn.stnCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="toStnDB" ng-hide="toStnDBFlag">
		<input type="text" name="filterTextbox5" ng-model="filterTextbox5" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Stn Name </th>
 	  	  	  <th> Stn District </th>
 	  	  	  <th> Stn Pin </th>
 	  	  	  <th> Stn Code </th>
 	  	  </tr>
		  <tr ng-repeat="toStn in stnList | filter:filterTextbox5">
		 	  <td><input type="radio" name="toStn" value="{{ toStn }}" ng-click="saveToStn(toStn)"></td>
              <td>{{toStn.stnName}}</td>
              <td>{{toStn.stnDistrict}}</td>
              <td>{{toStn.stnPin}}</td>
              <td>{{toStn.stnCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="contDB" ng-hide="contDBFlag">
		<input type="text" name="filterTextbox6" ng-model="filterTextbox6" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Code </th>
 	  	  	  <th> FrmDt </th>
 	  	  	  <th> ToDt </th>
 	  	  	  <th> FrmStn </th>
 	  	  	  <th> Rate&nbsp&nbsp&nbsp&nbsp&nbsp </th>
 	  	  	  
 	  	  </tr>
		  <tr ng-repeat="cont in contList | filter:filterTextbox6">
		 	  <td><input type="radio" name="cont" value="{{ cont }}" ng-click="saveCont(cont)"></td>
              <td>{{cont.contCode}}</td>
              <td>{{cont.contFromDt | date:'dd/MM/yy'}}</td>
              <td>{{cont.contToDt | date:'dd/MM/yy'}}</td>
              <td>{{cont.contFrmStnName}}</td>
              <td>
	           	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="getRate(cont, $index)" >
					<i class="mdi-content-send white-text"></i>
				</a>
			  </td>
          </tr>
      </table> 
	</div>
	
	<div id ="rateDB" ng-hide="rateDBFlag">
		<input type="text" name="filterTextbox7" ng-model="filterTextbox7" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <!-- <th></th> -->
 	  	  	  <th> Rate </th>
 	  	  	  <th> ToStn </th>
 	  	  	  <th> VType</th>
 	  	  	  <th> FrmWt </th>
 	  	  	  <th> ToWt </th>
 	  	  	  <th> FrmDt</th>
 	  	  	  <th> ToDt</th>
 	  	  	  
 	  	  </tr>
		  <tr ng-repeat="rate in rateList | filter:filterTextbox7">
		 	  <!-- <td><input type="radio"  name="contName" value="{{ cont }}" ng-click="saveCont(cont)"></td> -->
              <td>{{rate.ctsRate}}</td>
              <td>{{rate.ctsToStnName}}</td>
              <td>{{rate.ctsVehicleType}}</td>
              <td>{{rate.ctsFromWt}}</td>
              <td>{{rate.ctsToWt}}</td>
              <td>{{rate.ctsFrDt | date:'dd/MM/yy'}}</td>
              <td>{{rate.ctsToDt | date:'dd/MM/yy'}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id ="cngnorDB" ng-hide="cngnorDBFlag">
		<input type="text" name="filterTextbox8" ng-model="filterTextbox8" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> FaCode </th>
 	  	  	  <th> DailyContAllow </th>
 	  	  </tr>
		  <tr ng-repeat="cngnor in custList | filter:filterTextbox8">
		 	  <td><input type="radio" name="cngnor" value="{{ cngnor }}" ng-click="saveCngnor(cngnor)"></td>
              <td>{{cngnor.custName}}</td>
              <td>{{cngnor.custFaCode}}</td>
              <td>{{cngnor.custIsDailyContAllow}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="cngneeDB" ng-hide="cngneeDBFlag">
		<input type="text" name="filterTextbox9" ng-model="filterTextbox9" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> FaCode </th>
 	  	  	  <th> DailyContAllow </th>
 	  	  </tr>
		  <tr ng-repeat="cngnee in custList | filter:filterTextbox9">
		 	  <td><input type="radio" name="cngnee" value="{{ cngnee }}" ng-click="saveCngnee(cngnee)"></td>
              <td>{{cngnee.custName}}</td>
              <td>{{cngnee.custFaCode}}</td>
              <td>{{cngnee.custIsDailyContAllow}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="vehicleTypeDB" ng-hide="vehicleTypeDBFlag">
		<input type="text" name="filterTextbox10" ng-model="filterTextbox10" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Code </th>
 	  	  	  <th> Service Type </th>
 	  	  	  <td> Vehicle Type</td>
 	  	  </tr>
		  <tr ng-repeat="vehicleType in vehTypeList | filter:filterTextbox10">
		 	  <td><input type="radio" name="vehicleType" value="{{ vehicleType }}" ng-click="saveVehicleType(vehicleType)"></td>
              <td>{{vehicleType.vtCode}}</td>
              <td>{{vehicleType.vtServiceType}}</td>
              <td>{{vehicleType.vtVehicleType}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="productTypeDB" ng-hide="productTypeDBFlag">
		<input type="text" name="filterTextbox11" ng-model="filterTextbox11" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Product Type </th>
 	  	  </tr>
		  <tr ng-repeat="productType in productTypeList | filter:filterTextbox11">
		 	  <td><input type="radio" name="productType" value="{{ productType }}" ng-click="saveProductType(productType)"></td>
              <td>{{productType}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="payAtDB" ng-hide="payAtDBFlag">
		<input type="text" name="filterTextbox12" ng-model="filterTextbox12" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="payAt in branchList | filter:filterTextbox12">
		 	  <td><input type="radio" name="payAt" value="{{ payAt }}" ng-click="savePayAt(payAt)"></td>
              <td>{{payAt.branchName}}</td>
              <td>{{payAt.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="billAtDB" ng-hide="billAtDBFlag">
		<input type="text" name="filterTextbox13" ng-model="filterTextbox13" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="billAt in branchList | filter:filterTextbox13">
		 	  <td><input type="radio" name="billAt" value="{{ billAt }}" ng-click="saveBillAt(billAt)"></td>
              <td>{{billAt.branchName}}</td>
              <td>{{billAt.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="empDB" ng-hide="empDBFlag">
		<input type="text" name="filterTextbox14" ng-model="filterTextbox14" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> Code </th>
 	  	  </tr>
		  <tr ng-repeat="emp in empList | filter:filterTextbox14">
		 	  <td><input type="radio" name="emp" value="{{ emp }}" ng-click="saveEmp(emp)"></td>
              <td>{{emp.empName}}</td>
              <td>{{emp.empCodeTemp}}</td>
          </tr>
      </table> 
	</div>
	
	<div id="saveInvoiceNo" ng-hide="saveInvoiceNoFlag">
		<form name="saveInvoiceNoForm" ng-submit="saveInvoiceNum(saveInvoiceNoForm,invoiceNo,invoiceDt)">
			<table>
				<tr>
					<td>Invoice No:</td>
					<td><input type="text" id="invoiceNo" name="invoiceNo" ng-model="invoiceNo" ng-minlength="3" ng-maxlength="15" required></td>
					<td><input type="date" id="invoiceDt" name="invoiceDt" ng-model="invoiceDt" required></td>
				</tr>
				<tr>
					<td><input type="submit" value="Submit"></td>
				</tr>
	
			</table>
		</form>
	</div>

	<!-- rel wherehouse show -->
<div id="warehouseId" ng-show="whShowFlag">

	<input type="text" name="filterwarehouseId"
		ng-model="filterWhareHouse" placeholder="Search ">

	<table>
		<tr>
			<th></th>
			<th>ID</th>
			<th>SR No</th>
			<th>Count</th>
			<th>State</th>
			<th>Location</th>
			<th>Address1</th>
			<th>Address2</th>
		</tr>

		<tr ng-repeat="wh in warehouseList | filter:filterWhareHouse">
			<td><input type="radio" name="whCode" id="whId"
				value="{{ wh.whName }}" ng-model="whName"
				ng-click="saveWarehouseCode(wh)"></td>
			<td>{{ wh[0] }}</td>
			<td>{{ wh[1] }}</td>
			<td>{{ wh[2] }}</td>
			<td>{{ wh[3]}}</td>
			<td>{{ wh[4] }}</td>
			<td>{{ wh[5] }}</td>
			<td>{{ wh[6] }}</td>
		</tr>
	</table>

</div>
	



</div>
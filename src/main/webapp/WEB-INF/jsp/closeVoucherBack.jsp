<style type="text/css">
            .printable {display: none;}
            @media print {
                .noprint, .menuwrapper, .ui-dialog, footer, #toast-container {display: none !important;}
                .printable {display: block;
                		width: 11in !important;
                		position: fixed;
                		top: -10px;
                		left:0px;
                		z-index: 999999;
                		border: 0px solid black;
                		}
                		
                		
                		body  {
						      width: 11in;
						      height:210in;
						      }
            }
            @PAGE {
				   size: A4 portrait;
				   margin: 2cm 0.20cm 2cm 0.20cm;
			      }
			           
}
</style>



<div ng-show="operatorLogin || superAdminLogin">
	<div class="noprint">
		<form name="newVoucherForm" class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly  >
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.cashStmtStatus.cssCsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div>
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="openBalId" name ="openBal" ng-model="vs.cashStmtStatus.cssOpenBal" readonly ng-required="true" >
		       			<label for="code">Opening Balance</label>	
		       		</div>
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="closeBalId" name ="closeBal" ng-model="closeBal" readonly ng-required="true" >
		       			<label for="code">Closing Balance</label>	
		       		</div>
		       		
		       		<!-- <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bkOpenBalId" name ="bkOpenBal" ng-model="vs.cashStmtStatus.cssBkOpenBal" readonly ng-required="true" >
		       			<label for="code">Bank Opening Bal</label>	
		       		</div> -->
		    </div>
		    
		    <div class="row">
		    	    <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bkOpenBalId" name ="bkOpenBal" ng-model="vs.cashStmtStatus.cssBkOpenBal" readonly ng-required="true" >
		       			<label for="code">Bank Opening Bal</label>	
		       		</div>
		       		
		       		 <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bkCloseBalId" name ="bkCloseBal" ng-model="bkCloseBal" readonly ng-required="true" >
		       			<label for="code">Bank Closing Bal</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
	       				<input type="button" value="closing voucher" id="closeVHId" ng-click="closeVoucher()">
	       		    </div>
		    </div>
		  
  	
	   		 
	   		 <!--  <div class="row">
	       		<div class="col s4 input-field">
	       				<input type="button" value="closing voucher" ng-click="closeVoucher()">
	       		</div>
	   		 </div> -->
	   		 
	   		 
	   		  <div class="row">
	   		  		<div ng-show="penCsList.length > 0">
			   			<table class="tblrow">
			   				   <caption class="coltag tblrow">Pending CS Details</caption>
			   				 <tr class="rowclr">
		                          <th class="colclr">FaCode</th>
		                          <th class="colclr">C/D</th>
		                          <th class="colclr">Amount</th>
		                          <th class="colclr">Type</th>       
		                     </tr>
		                     <tr class="tbl" ng-repeat="penCs in penCsList">
		                     	 <td class="rowcel">{{penCs.csFaCode}}</td>
		                     	 <td class="rowcel">{{penCs.csDrCr}}</td>
		                     	 <td class="rowcel">{{penCs.csAmt}}</td>
		                     	 <td class="rowcel">{{penCs.csVouchType}}</td>
		                 	</tr>
		                 </table>
	             	</div>    	
	   		  </div>
	   		  
	   		  
	   		  	  <div class="row">
	   		  		<div ng-show="csList.length > 0">
			   			<table class="tblrow">
			   				   <caption class="coltag tblrow">CS Details</caption>
			   				 <tr class="rowclr">
		                          <th class="colclr">VH No</th>
		                          <th class="colclr">VH Type</th>
		                          <th class="colclr">Entry Type</th>
		                          <th class="colclr">C/D</th>
		                          <th class="colclr">FaCode</th>
		                          <th class="colclr">Amount</th>               
		                     </tr>
		                     <tr class="tbl" ng-repeat="cs in csList">
			                     	 <td class="rowcel">{{cs.csVouchNo}}</td>
			                     	 <td class="rowcel">{{cs.csType}}</td>
			                     	 <td class="rowcel">{{cs.csVouchType}}</td>
			                     	 <td class="rowcel">{{cs.csDrCr}}</td>
			                     	 <td class="rowcel">{{cs.csFaCode}}</td>
			                     	 <td class="rowcel">{{cs.csAmt}}</td>
		                 	</tr>
		                 </table>
	             	</div>    	
	   		  </div>
	   		  
	   		  
		  </form>
    </div>
    

	<div id="printVsDB" ng-hide="printVsFlag" class="noprint">
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">{{PRINT_STMT}}</h5></div>
		</div>
		<div>
			<div class="col s12 center">
				<a class="btn white-text" ng-click="cancelPrint()">Cancel</a>
				<a class="btn white-text" ng-click="printVs()">Yes</a>
			</div>
		</div>
	</div>
	


	<div class="printable"> 
		<div class="col s12 card" style="align: center; margin-top: 0px; padding-top:0px;  background-color: rgba(125, 125, 125, 0.3);" >
			<div class="container" style="width: 100% !important;">
				<div class="row" style="  border-top:0px solid black; border-left:0px solid black;border-right:0px solid black;">
					<div class="col s8">
						<div class="row">
							<div class="col s12" style="font-weight:bold; margin-top:5px;"><span style=" font-size:22px;">{{cmpnyName}}</span><br/>
												<!-- Voucher Type: {{vs.voucherType}} -->
												Cash Statement<br/>
												CS Print Date: {{date | date:'dd/MM/yyyy'}}  Time:{{date | date:'HH:mm:ss'}}<br/>
												<!-- <br/> -->
												<!-- Pay to: {{vs.payTo}} --></div>
						</div>
					</div>
					<div class="col s4">
						<div class="row">
							<div class="col s12" style="font-weight:bold; margin-top:5px;">Branch Code:{{brhName}}<br/>
												Sheet No: {{sheetNo}}<br/>
												Date: {{csDate | date:'dd/MM/yyyy'}}<!-- <br/> -->
												<!-- Voucher No: {{vs.vhNo}}<br/> -->
												<!-- Cheque No: {{vs.chequeLeaves.chqLChqNo}} --></div>
						</div>						
					</div>
				</div>
				<table style="width: 100% !important;  border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black; ">
					<tr style="border-top: 3px solid black; border-bottom: 1px solid black;">
						<td style="width: 1in;"></td>
						<td style="width: 1in; font-weight:bold; font-size:16px;" colspan="5">VOUCHER TYPE : </td>
						<td style="width: 1in; font-weight:bold; font-size:16px;" colspan="2">CASH</td>
					</tr>
					<tr style="border-top: 1px solid black; border-bottom: 1px solid black; font-weight:bold;">
						<td style="width: 40px;">CODE</td>
						<td style="width: 40px;">ACHEAD</td>
						<td style="width: 80px;">VCH TYPE</td>
						<td style="width: 292px; text-align:center;">DESCRIPTION</td>
						<td style="width: 100px;">TvNo</td>
						<td style="width: 20px; text-align:right;">VNo</td>
						<td style="width: 115px; text-align:right;">RECEIPT AMT</td>
						<td style="width: 115px; text-align:right;">PAYMENT AMT</td>
					</tr>
					<tr style="border-top: 1px solid black; border-bottom: 1px solid black;">
						<td style="width: 1in;"></td>
						<td style="width: 1in; font-weight:bold;" colspan="5">CASH OPENING BALANCE</td>
						<td style="width: 1in; font-weight:bold; text-align:right;">{{ openBal | number:2}}</td>
						<td style="width: 1in;"></td>
					</tr>
					<tr style="border-bottom: 1px solid black;" ng-repeat="cashCs in cashCsList">
						<!-- <div ng-if="cs.csVochType == 'cash' || cs.csVochType == 'CASH'"> -->
							<td style="width: 40px;">{{cashCs.csFaCode}}</td>
							<td style="width: 40px;">
								<div ng-repeat="acHdCash in acHdList">
									<div ng-if="acHdCash.faCode == cashCs.csFaCode">
										{{ acHdCash.faName }}
									</div>
								</div>
							</td>		
							<td style="width: 80px;">{{cashCs.csType}}</td>
							<td style="width: 292px; text-align:center;">{{cashCs.csDescription}}</td>
							<td style="width: 100px;">{{cashCs.csTvNo}}</td>
							<td style="width: 20px; text-align:right;">{{cashCs.csVouchNo}}</td>
							<td style="width: 115px; text-align:right;">
								<div ng-if="cashCs.csDrCr == 'C'">
									{{cashCs.csAmt | number:2}}
									<!-- 999999999.99 -->
								</div>
							</td>
							<td style="width: 115px; text-align:right;">
								<div ng-if="cashCs.csDrCr == 'D'">
									{{cashCs.csAmt | number:2}}
									<!-- 999999999.99 -->
								</div>
							</td>
						<!-- </div> -->
					</tr>
					<tr style="border-top: 1px solid black; border-bottom: 1px solid black; font-weight:bold;">
						<td style="width: 1in;"></td>
						<td style="width: 1in;" colspan="5">CASH CLOSING BALANCE</td>
						<td style="width: 1in;"></td>
						<td style="width: 1in; text-align:right;">{{ closeBal | number:2}}</td>
					</tr>
					<tr style="border-top: 1px solid black; border-bottom: 3px solid black; font-weight:bold;">
						<td style="width: 1in;"></td>
						<td style="width: 1in;" colspan="5">VOUCHER TOTAL</td>
						<td style="width: 1in; text-align:right;">{{ recAmt | number:2}}</td>
						<td style="width: 1in; text-align:right;">{{ payAmt | number:2}}</td>
					</tr>
				</table>
		<!-- 		<div class="row">
					<div class="col s12">Net Cash Withdrawal Amount {{}}</div>
				</div>
				<div class="row">
					<div class="col s12">Total Amount </div>
				</div>
				<div class="row">
					<div class="col s12">Note: Cashier has confirmed that he has obtained signature of passing authority & Payee on this payment by putting his signature on the same. </div>
				</div>
				<div class="row">
					<div class="col s3">Passed by<br/></>Code No</div><div class="col s3">Cash by<br/></>Code No</div><div class="col s3">Vouchered by<br/></>Code No</div><div class="col s3">Payees Signature</div>
				</div> -->
				
				
				<table style="width: 100% !important; border: 1px solid black;" ng-repeat="bkCsMap in bkCsMapList">
					<tr style="border-top: 3px solid black; border-bottom: 1px solid black;">
						<td style="width: 1in;"></td>
						<td style="width: 1in; font-weight:bold; font-size:16px;" colspan="2">VOUCHER TYPE : </td>
						<td style="width: 1in; font-weight:bold; font-size:16px;" colspan="1">BANK</td>
						<td style="width: 1in; font-weight:bold; font-size:16px;" colspan="3">
							<div ng-repeat="acHead in acHdList">
									<div ng-if="acHead.faCode == bkCsMap.bank.bcsBankCode">
										{{ acHead.faName }}
									</div>
							</div>
						</td>
						<td style="width: 1in; font-weight:bold; font-size:18px;" colspan="1">{{bkCsMap.bank.bcsBankCode}}</td>
					</tr>
					<tr style="border-top: 1px solid black; font-weight:bold; border-bottom: 1px solid black;">
						<td style="width: 40px;">CODE</td>
						<td style="width: 40px;">ACHEAD</td>
						<td style="width: 80px;">VCH TYPE</td>
						<td style="width: 292px; text-align:center;">DESCRIPTION</td>
						<td style="width: 100px;">TvNo</td>
						<td style="width: 20px; text-align:right;">VNo</td>
						<td style="width: 115px; text-align:right;">RECEIPT AMT</td>
						<td style="width: 115px; text-align:right;">PAYMENT AMT</td>
					</tr>
					<tr style="border-top: 1px solid black; border-bottom: 1px solid black;">
						<td style="width: 1in;"></td>
						<td style="width: 1in; font-weight:bold;" colspan="5">BANK OPENING BALANCE</td>
						<td style="width: 1in; font-weight:bold; text-align:right;">{{ bkCsMap.bank.bcsOpenBal | number:2 }}</td>
						<td style="width: 1in;"></td>
					</tr>
					<tr style="border-bottom: 1px solid black;" ng-repeat="bankCs in bkCsMap.bkCsList">
						<!-- <div ng-if="cs.csVochType == 'cash' || cs.csVochType == 'CASH'"> -->
							<td style="width: 40px;">{{bankCs.csFaCode}}</td>
							<td style="width: 40px;">
								<div ng-repeat="acHdBnk in acHdList">
									<div ng-if="acHdBnk.faCode == bankCs.csFaCode">
										{{ acHdBnk.faName }}
									</div>
								</div>
							</td>		
							<td style="width: 80px;">{{bankCs.csType}}</td>
							<td style="width: 292px; text-align:center;">{{bankCs.csDescription}}</td>
							<td style="width: 100px;">{{bankCs.csTvNo}}</td>
							<td style="width: 20px; text-align:right;">{{bankCs.csVouchNo}}</td>
							<td style="width: 115; text-align:right;">
								<div ng-if="bankCs.csDrCr == 'C'">
									{{bankCs.csAmt | number:2}}
								</div>
							</td>
							<td style="width: 115px; text-align:right;">
								<div ng-if="bankCs.csDrCr == 'D'">
									{{bankCs.csAmt | number:2}}
								</div>
							</td>
						<!-- </div> -->
					</tr>
					<tr style="border-top: 1px solid black; font-weight:bold; border-bottom: 1px solid black;">
						<td style="width: 1in;"></td>
						<td style="width: 1in;" colspan="5">BANK CLOSING BALANCE</td>
						<td style="width: 1in;"></td>
						<td style="width: 1in; text-align:right;">{{ bkCsMap.bank.bcsCloseBal | number:2}}</td>
					</tr>
					<tr style="border-top: 1px solid black; font-weight:bold; border-bottom: 3px solid black;">
						<td style="width: 1in;"></td>
						<td style="width: 1in;" colspan="5">VOUCHER TOTAL</td>
						<td style="width: 1in; text-align:right;">{{ bkCsMap.recBnkAmt | number:2}}</td>
						<td style="width: 1in; text-align:right;">{{ bkCsMap.payBnkAmt | number:2}}</td>
					</tr>
					
				</table>
				
				<table style="width: 100% !important; border:1px solid black;  border-bottom: 3px solid black;">
					<tr style="border-top: 3px solid black; border-bottom: 1px solid black;">
						<td style="width: 1in;"></td>
						<td style="width: 1in; font-weight:bold; font-size:16px;" colspan="5">VOUCHER TYPE : </td>
						<td style="width: 1in; font-weight:bold;  font-size:16px;" colspan="2">CONTRA</td>
					</tr>
					<tr style="border-top: 1px solid black; font-weight:bold; border-bottom: 1px solid black;">
						<td style="width: 40px;">CODE</td>
						<td style="width: 40px;">ACHEAD</td>
						<td style="width: 80px;">VCH TYPE</td>
						<td style="width: 292px; text-align:center;">DESCRIPTION</td>
						<td style="width: 100px;">TvNo</td>
						<td style="width: 20px; text-align:right;">VNo</td>
						<td style="width: 115px; text-align:right;">RECEIPT AMT</td>
						<td style="width: 115px; text-align:right;">PAYMENT AMT</td>
					</tr>
					
					<tr style="border-bottom: 1px solid black;" ng-repeat="contraCs in contraCsList">
						<!-- <div ng-if="cs.csVochType == 'cash' || cs.csVochType == 'CASH'"> -->
							<td style="width: 40px;">{{contraCs.csFaCode}}</td>
							<td style="width: 40px;">
								<div ng-repeat="acHdCont in acHdList">
									<div ng-if="acHdCont.faCode == contraCs.csFaCode">
										{{ acHdCont.faName }}
									</div>
								</div>
							</td>		
							<td style="width: 80px;">{{contraCs.csType}}</td>
							<td style="width: 292px; text-align:center;">{{contraCs.csDescription}}</td>
							<td style="width: 100px;">{{contraCs.csTvNo}}</td>
							<td style="width: 20px; text-align:right;">{{contraCs.csVouchNo}}</td>
							<td style="width: 115px; text-align:right;">
								<div ng-if="contraCs.csDrCr == 'C'">
									{{contraCs.csAmt | number:2}}
								</div>
							</td>
							<td style="width: 115px; text-align:right;">
								<div ng-if="contraCs.csDrCr == 'D'">
									{{contraCs.csAmt | number:2}}
								</div>
							</td>
						<!-- </div> -->
					</tr>
					
				</table>
				
			</div>
			
			<div class="container" style="width: 100% !important;">
				<div class="row" style="  border-top:0px solid black; font-weight:bold; border-left:0px solid black;border-right:0px solid black;">
					<div class="col s6">
					<div style="margin-top:20px;">CS Verfied by:</div>
					<div style="margin-top:25px;">Branch Incharge:</div>
					<div style="margin-top:20px;">Staff Code:</div>
					
					</div>
				
				    <div class="col s6" align="right" style="margin-top:-75px; text-align:left;  margin-left:70%;">
					<div style="margin-top:10px;">Cashier:</div>
					<div style="margin-top:20px;">Staff Code:</div>
					</div>
				
				</div>
			</div>
			
		</div>
	</div>
	
</div>

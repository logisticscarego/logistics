 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Admin Edit Customer</title>
	</head>
	
	<body>
	<form ng-submit="SubmitEditCustomerCode(custCode)">
		<div id ="custCode">
				<table>
					<tr>
						<td>Enter Customer Code</td>
						<td><input type="text" name="custCode" id="custCode" ng-model="custCode"/></td>
					</tr>
					<tr>
						<td><input type="submit" value="Submit"/></td>
					</tr>
				</table>
			</div>
		</form>
		<div ng-hide = "show" >	
		
			<form ng-submit="SubmitEditCustomer(customer)">
			<div align="center">
		
				<table border="0">
				 <tr>
	                <td colspan="2" align="center">
	                    <h3>Here's the review of your Customer:</h3>
	                </td>
	            </tr>
	             <tr>
					<td>Customer Name:</td>
					<td><input type ="text" name ="custName" value={{customer.custName}} ng-model="customer.custName"></td>
				</tr>
				
				<tr>
					<td>Customer Address:</td>
					<td><input type ="text" name ="custAdd" value={{customer.custAdd}} ng-model="customer.custAdd"></td>
				</tr>
				
				<tr>
					<td>Customer City:</td>
					<td><input type ="text" name ="custCity" value={{customer.custCity}} ng-model="customer.custCity"></td>
				</tr>
				
				<tr>
					<td>Customer State:</td>
					<td><input type ="text" name ="custState" value={{customer.custState}} ng-model="customer.custState"></td>
				</tr>
				
				<tr>
					<td>Customer Pin:</td>
					<td><input type ="text" name ="custPin" value={{customer.custPin}} ng-model="customer.custPin"></td>
				</tr>
				
				<tr>
					<td>Customer TDS Circle :</td>
					<td><input type ="text" name ="custTdsCircle" value={{customer.custTdsCircle}} ng-model="customer.custTdsCircle"></td>
				</tr>
				
				<tr>
					<td>Customer TDS City:</td>
					<td><input type ="text" name ="custTdsCity" value={{customer.custTdsCity}} ng-model="customer.custTdsCity"></td>
				</tr>
				
				<tr>
					<td>Customer PAN Number:</td>
					<td><input type ="text" name ="custPanNo" value={{customer.custPanNo}} ng-model="customer.custPanNo"></td>
				</tr>
				
				<tr>
					<td>Customer TIN Number:</td>
					<td><input type ="text" name ="custTinNo" value={{customer.custTinNo}} ng-model="customer.custTinNo"></td>
				</tr>
				
				<tr>
					<td>Customer Status:</td>
					<td><input type ="text" name ="custStatus" value={{customer.custStatus}} ng-model="customer.custStatus"></td>
				</tr>
				
				<tr>
					<td>Daily Contract Allowed</td>
					<td><input type ="text" name ="custIsDailyContAllow" value={{customer.custIsDailyContAllow}} ng-model="customer.custIsDailyContAllow"></td>
				</tr>
				<tr>
	                <td><input type="hidden" name ="custCode" value={{customer.custCode}} ng-model="customer.custCode"></td>
	            </tr>
	            <tr>
					<td align ="center"><input type="submit" value="Submit!" ></td>
				</tr>
	       </table>
	      </div>
	   </form>
	</div>
	</div> 
	</body>
</html>
<title>Generated Fund Allocation</title>
<div ng-show="operatorLogin || superAdminLogin">
<div class="row" ng-hide="generateFundData">
 <form ng-submit="generateFundAllocation()" class="col s12 card"
style="align: center; padding-top: 40px;
 background-color: rgba(125, 125, 125, 0.3);">
 
 	<div class="row">
			<div class="input-field col s12 center">
				<input type="submit" value="View Current Date Fund Details">
			</div>
		</div>

</form> 
</div>

<!-- <div class="row" ng-hide="showFundList">
 <table style="border: 1px solid black; border-collapse: collapse;width: 100%">
 <tr style="background-color:white;">
 <th style="width: 150px; color:black;">Beneficiary Name</th>
 <th style="width: 150px; color:black;">Bene Address 1</th>
 <th style="width: 150px; color:black;">Beneficiary Account No</th>
 <th style="width: 150px; color:black;">Beneficiary Code</th>
 <th style="width: 150px; color:black;">Beneficiary Email Id</th>
 <th style="width: 150px; color:black;">IFSC Code</th>
 <th style="width: 150px; color:black;">Instrument Amount</th>
 <th style="width: 150px; color:black;">Update</th>
 </tr>
<tr ng-repeat="fundAll in fundList">
<form>
<td style="width: 150px"><input style="width: 150px;background-color: gray;color: white;" type="text" ng-model="fundAll.beneficiaryName" readonly="readonly"></td>
<td style="width: 150px"><input style="width: 150px;background-color: gray;color: white;" type="text" ng-model="fundAll.beneAddress1" readonly="readonly"></td>
<td style="width: 200px"><input style="width: 200px;background-color: gray;color: white;" type="text" ng-model="fundAll.beneficiaryAccountNo" readonly="readonly"></td>
<td style="width: 180px"><input style="width: 180px;background-color: gray;color: white;" type="text" ng-model="fundAll.beneficiaryCode" readonly="readonly"></td>
<td style="width: 220px"><input style="width: 220px;background-color: gray;color: white;" type="text" ng-model="fundAll.beneficiaryEmailId" readonly="readonly"></td>
<td style="width: 180px"><input style="width: 180px;background-color: gray;color: white;" type="text" ng-model="fundAll.ifscCode" readonly="readonly"></td>
<td style="width: 250px"><input style="width: 150px;" type="number" ng-model="fundAll.instrumentAmount" min="0"></td>
<td><input type="submit" value="Update" ng-click="updateFundAllocation(fundAll,$index)"></td>
</form>
</tr>
</table>

</div> -->


<div class="row" ng-hide="showFundList">

<table class="tblrow" >
       		
       		<caption class="coltag tblrow">Verification Details</caption> 
       		
            <tr class="rowclr">
            	<th class="colclr">S.No</th>
            	<th class="colclr">Brnach</th>
            	<th class="colclr">CNMT No.</th>
            	<th class="colclr">Challan No.</th>
            	<th class="colclr">Chln Date</th>
            	<th class="colclr">Lorry No.</th>
            	<th class="colclr">From</th>
            	<th class="colclr">To</th>
            	<th class="colclr">Beneficiary Name</th>
            	<th class="colclr">Bene Address</th>
            	<th class="colclr">IFSC Code</th>
            	<th class="colclr">Account No.</th>
            	<th class="colclr">Chln Freight</th>
            	<th class="colclr">Advance</th>
            	<th class="colclr">TDS</th>
            	<th class="colclr">Transfer Amount</th>
<!--             	<th class="colclr">Update</th> -->
            	<th class="colclr">Reject</th>
            	<th class="colclr">Approved</th>
            	 </tr>	
	         
	        <tr ng-repeat="fundAll in mapList">
	        	<td class="rowcel">{{$index+1}}</td>
	        	<td class="rowcel">{{fundAll.fa.bCode}}</td>
	        	<td class="rowcel">{{fundAll.fd.cnmtCode}}</td>
	        	<td class="rowcel">{{fundAll.fd.challanNo}}</td>
	        	<td class="rowcel">{{fundAll.fd.chlnDt}}</td>
	        	<td class="rowcel">{{fundAll.fd.vehicleNo}}</td>
	        	<td class="rowcel">{{fundAll.fd.fromStCode}}</td>
	        	<td class="rowcel">{{fundAll.fd.toStCode}}</td>
	        	<td class="rowcel">{{fundAll.fa.beneficiaryName}}</td>
	        	<td class="rowcel">{{fundAll.fa.beneAddress1}}</td>
	        	<td class="rowcel">{{fundAll.fa.ifscCode}}</td>
	        	<td class="rowcel">{{fundAll.fa.beneficiaryAccountNo}}</td>
	        	<td class="rowcel">{{fundAll.fd.chlnFreight}}</td>
	        	<td class="rowcel">{{fundAll.fd.lryAmount}}</td>
	        	<td class="rowcel">{{fundAll.fa.tdsAmt}}</td>
	        	<td class="rowcel">{{fundAll.fa.amount}}</td>
 			<!-- <td class="rowcel" >
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="updateDetail(ptro)" >
						<i class="mdi-content-send white-text"></i>
					</a>
				</td> -->
				
				<td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="rejectPayment(fundAll,$index)" >
						<i class="mdi-content-send white-text"></i>
					</a>
				</td>
				
				<td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="updateFundAllocation(fundAll,$index)" >
						<i class="mdi-content-send white-text"></i>
					</a>
				</td>
				
			</tr>
	</table>

</div>


<div class="col s12 center">
			<form method="post" action="downloadFundAllExcel" enctype="multipart/form-data">
				<input type="submit" id="printXlsId" value="Print XLS">
		  	</form>
		</div>

</div>
<div ng-show="adminLogin || superAdminLogin">
<title>Challan & Cnmt</title>
<div class="row">
	<div class="col s1 hide-on-med-and-down">&nbsp;</div>
	<div class="col s10 card" style="align: center; margin-top: 40px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">

		<div class="input-field col s6">
			<input type="text" id="chlnCodeId" name="chlnCodeName" ng-model="chlnCode" required readonly ng-click="openChlnDB()" /> 
			<label>Enter Challan Code</label>
		</div>
		
		<div class="input-field col s6 center">
			<input class="btn waves-effect waves-light" type="submit" value="Add CNMT" ng-click="openCnmtDB()">
		</div>
	</div>
</div>

<div ng-show="showTableFlag">
	<form name="chlnForm" ng-submit="submitChlnNCnmt(chlnForm)" >
	
	<div class="row">
		<div class="col s1 hide-on-med-and-down">&nbsp;</div>
		<div class="col s10 card" style="align: center; margin-top: 0px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			
			<table class="tblrow" >
       		
       		<caption class="coltag tblrow">CNMT LIST</caption> 
       		
            <tr class="rowclr">
	            <th class="colclr">CNMT No.</th>
	            <th class="colclr">CNMT Id</th>
	            <th class="colclr">Remove</th>
	        </tr>	
	         
	        <tr ng-repeat="cnmt in cnmtCodeIdByChlnList">
	            <td class="rowcel">{{cnmt.cnmtCode}}</td>
	            <td class="rowcel">{{cnmt.cnmtId}}</td>
	            
				<td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="removeCnmt($index, cnmt)" >
						<i class="mdi-action-delete white-text"></i>
					</a>
				</td>
	        </tr>
	        
	
    	</table>
			<div class="col s1 hide-on-med-and-down">&nbsp;</div>
			<div class="row">
				<div class="col s12 center">
					<input class="btn" type="submit" value="Submit">
				</div>
			</div>
		</div>
		</div>
	</form>
</div>

<div id ="chlnDB" ng-hide="chlnDBFlag">
	<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
	    <table>
	  	  <tr>
	  	  	  <th></th>
	  	  	  <th> Challan No </th>
	  	  </tr>
	  <tr ng-repeat="chlnCode in chlnNoList | filter:filterTextbox1">
	 	  <td><input type="radio" name="chlnCode" value="{{ chln }}" ng-click="selectChlnCode(chlnCode)"></td>
             <td>{{chlnCode}}</td>
         </tr>
     </table> 
</div>

<div id ="cnmtDB" ng-hide="cnmtDBFlag">
	<input type="text" name="filterTextbox2" ng-model="filterTextbox2" placeholder="Search">
	    <table>
	  	  <tr>
	  	  	  <th></th>
	  	  	  <th> CNMT No </th>
	  	  </tr>
	  <tr ng-repeat="cnmt in cnmtCodeIdList | filter:filterTextbox2">
	 	  <td><input type="radio" name="cnmtCode" value="{{ cnmt }}" ng-click="addCnmt(cnmt)"></td>
             <td>{{cnmt.cnmtCode}}</td>
         </tr>
     </table> 
</div>

<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="voucherForm" ng-submit=voucherSubmit(voucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly  >
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		    		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.lhpvStatus.lsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div>
			   		 <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code">Voucher Type</label>	
		       		</div>
		       
		    </div>
		    
		     <div class="row"> 		
				 <div ng-show="lhpvAdvList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">LHPV ADVANCE DETAILS</caption>
						<tr class="rowclr">
							<th class="colclr">SNo.</th>
							<th class="colclr">Chln No.</th>
							<th class="colclr">Lry Adv</th>
							<th class="colclr">Cash Dis</th>
							<th class="colclr">Munsiana</th>
							<th class="colclr">TDS</th>
							<th class="colclr">Payment By</th>
							<th class="colclr">Description</th>
							<th class="colclr">Payment Amt</th>
							<th class="colclr">Recovery Amt</th>
							<th class="colclr">Final Total Amt</th>
						</tr>
						<tr class="tbl" ng-repeat="lhpvadv in lhpvAdvList">
							<td class="rowcel">{{$index + 1}}</td>
							<td class="rowcel">{{chlnAdvList[$index]}}</td>
							<td class="rowcel">{{lhpvadv.laLryAdvP}}</td>
							<td class="rowcel">{{lhpvadv.laCashDiscR}}</td>
							<td class="rowcel">{{lhpvadv.laMunsR}}</td>
							<td class="rowcel">{{lhpvadv.laTdsR}}</td>
							<td class="rowcel">{{lhpvadv.laPayMethod}}</td>
							<td class="rowcel">{{lhpvadv.laDesc}}</td>
							<td class="rowcel">{{lhpvadv.laTotPayAmt}}</td>
							<td class="rowcel">{{lhpvadv.laTotRcvrAmt}}</td>
							<td class="rowcel">{{lhpvadv.laFinalTot}}</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td class="rowcel">Total Amt</td>
							<td class="rowcel">{{totAdv}}</td>
						</tr>
					</table>
				</div>
			</div>	
		    
		    
		     <div class="row"> 		
				 <div ng-show="lhpvBalList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">LHPV BALANCE DETAILS</caption>
						<tr class="rowclr">
							<th class="colclr">SNo.</th>
							<th class="colclr">Chln No.</th>
							<th class="colclr">Lry Bal</th>
							<th class="colclr">Cash Dis</th>
							<th class="colclr">Muns</th>
							<th class="colclr">TDS</th>
							<th class="colclr">Wt Sh</th>
							<th class="colclr">Late Del</th>
							<th class="colclr">Late Ack.</th>
							<th class="colclr">Ex Km</th>
							<th class="colclr">Ovr Hgt</th>
							<th class="colclr">Penalty</th>
							<th class="colclr">Oth</th>
							<th class="colclr">Det</th>
							<th class="colclr">Unld</th>
							<th class="colclr">Payment By</th>
							<th class="colclr">Description</th>
							<!-- <th class="colclr">Pay Amt</th>
							<th class="colclr">Recvr Amt</th> -->
							<th class="colclr">Final Amt</th>
						</tr>
						<tr class="tbl" ng-repeat="lhpvbal in lhpvBalList">
							<td class="rowcel">{{$index + 1}}</td>
							<td class="rowcel">{{chlnBalList[$index]}}</td>
							<td class="rowcel">{{lhpvbal.lbLryBalP}}</td>
							<td class="rowcel">{{lhpvbal.lbCashDiscR}}</td>
							<td class="rowcel">{{lhpvbal.lbMunsR}}</td>
							<td class="rowcel">{{lhpvbal.lbTdsR}}</td>
							<td class="rowcel">{{lhpvbal.lbWtShrtgCR}}</td>
							<td class="rowcel">{{lhpvbal.lbLateDelCR}}</td>
							<td class="rowcel">{{lhpvbal.lbLateAckCR}}</td>
							<td class="rowcel">{{lhpvbal.lbOthExtKmP}}</td>
							<td class="rowcel">{{lhpvbal.lbOthOvrHgtP}}</td>
							<td class="rowcel">{{lhpvbal.lbOthPnltyP}}</td>
							<td class="rowcel">{{lhpvbal.lbOthMiscP}}</td>
							<td class="rowcel">{{lhpvbal.lbUnpDetP}}</td>
							<td class="rowcel">{{lhpvbal.lbUnLoadingP}}</td>
							<td class="rowcel">{{lhpvbal.lbPayMethod}}</td>
							<td class="rowcel">{{lhpvbal.lbDesc}}</td>
							<!-- <td class="rowcel">{{lhpvbal.lbTotPayAmt}}</td>
							<td class="rowcel">{{lhpvbal.lbTotRcvrAmt}}</td> -->
							<td class="rowcel">{{lhpvbal.lbFinalTot}}</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td class="rowcel">Total Amt</td>
							<td class="rowcel">{{totBal}}</td>
						</tr>
					</table>
				</div>
			</div>	
		    
		    
		    <div class="row"> 		
				 <div ng-show="lhpvSupList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">LHPV SUPPLEMENTARY DETAILS</caption>
						<tr class="rowclr">
							<th class="colclr">SNo.</th>
							<th class="colclr">Chln No.</th>
							<th class="colclr">Wt Sh</th>
							<th class="colclr">Late Del</th>
							<th class="colclr">Late Ack.</th>
							<th class="colclr">Ex Km</th>
							<th class="colclr">Ovr Hgt</th>
							<th class="colclr">Penalty</th>
							<th class="colclr">Oth</th>
							<th class="colclr">Det</th>
							<th class="colclr">Unld</th>
							<th class="colclr">Payment By</th>
							<th class="colclr">Description</th>
							<th class="colclr">Pay Amt</th>
							<th class="colclr">Recvr Amt</th>
							<th class="colclr">Final Amt</th>
						</tr>
						<tr class="tbl" ng-repeat="lhpvsup in lhpvSupList">
							<td class="rowcel">{{$index + 1}}</td>
							<td class="rowcel">{{chlnSupList[$index]}}</td>
							<td class="rowcel">{{lhpvsup.lspWtShrtgCR}}</td>
							<td class="rowcel">{{lhpvsup.lspLateDelCR}}</td>
							<td class="rowcel">{{lhpvsup.lspLateAckCR}}</td>
							<td class="rowcel">{{lhpvsup.lspOthExtKmP}}</td>
							<td class="rowcel">{{lhpvsup.lspOthOvrHgtP}}</td>
							<td class="rowcel">{{lhpvsup.lspOthPnltyP}}</td>
							<td class="rowcel">{{lhpvsup.lspOthMiscP}}</td>
							<td class="rowcel">{{lhpvsup.lspUnpDetP}}</td>
							<td class="rowcel">{{lhpvsup.lspUnLoadingP}}</td>
							<td class="rowcel">{{lhpvsup.lspPayMethod}}</td>
							<td class="rowcel">{{lhpvsup.lspDesc}}</td>
							<td class="rowcel">{{lhpvsup.lspTotPayAmt}}</td>
							<td class="rowcel">{{lhpvsup.lspTotRcvrAmt}}</td>
							<td class="rowcel">{{lhpvsup.lspFinalTot}}</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td class="rowcel">Total Amt</td>
							<td class="rowcel">{{totSup}}</td>
						</tr>
					</table>
				</div>
			</div>	
		   
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" id="saveId" value="CLOSE LHPV" >
	       		</div>
	      </div>		
	     
	</form>

	<div id="printLhpvDB" ng-hide="printLhpvFlag">
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">{{PRINT_STMT}}</h5></div>
		</div>
		<div>
			<div class="col s12 center">
				<a class="btn white-text" ng-click="cancelPrint()">Cancel</a>
				<a class="btn white-text" ng-click="printVs()">Yes</a>
			</div>
		</div>
	</div>

  </div> 
</div>

<style>
.level1{height: 40px; width: 200px; margin-left: -5px; margin-right: 0px; margin-top: 0px; margin-bottom: 0px; padding-left: 5px;}
.level2{height: 40px; width: 200px; margin-left: -5px; margin-right: 0px; margin-top: 0px; margin-bottom: 0px; padding-left: 5px;}
.level2data{height: 40px; width: 200px; margin-left: 0px; margin-right: 0px; padding-left: 25px !important; text-decoration:none;}
.level2data{text-decoration:none;}
.level3data{height: 40px; width: 220px; margin-left: 0px; margin-right: 10px; padding-left: 40px !important; text-decoration:none;}
.level3data{text-decoration:none;}
.collapsible-header{padding-left: 15px;}
.collection-item {font-size: 15px; line-height: 40px;}
.nobottomborder {border-bottom: none;}
  .greyborder {
border: 1px solid #000;
height: 40px; width: 190px;
color: teal;
margin-left: -5px;
}
.greyborder:hover {
background-color: transparent;
} 
.menuwrapper {height: 100%; background-color:#fff; width: 205px; overflow-x: hidden; position: fixed; left:-5px; top: 50px; bottom: 20px; padding-top: 20px; padding-bottom: 100px;}
</style>

<div class="Content menuwrapper hide-on-small-only">
   	
  	<accordion  style="margin: 0px 0px;" close-others="oneAtATime">
    	<accordion-group is-open="status.open.operator">
        	<accordion-heading>
			<div class="level1 l1" ng-class="{'greyborder': status.open.operator, 'row': !status.open.operator}"><b>Operational</b></div>
        	</accordion-heading>  	
        	
        	<div ng-show="userRights.urEditCnmt">
        		<h6 class="collection level2data"><a href="#/operator/editCnmt" class="collection-item"><div class="l2">Edit CNMT</div></a></h6>
        	</div>
        	<div ng-show="userRights.urCancelCnmt">
        		<h6 class="collection level2data"><a href="#/operator/cancelCnmt" class="collection-item"><div class="l2">Cancel CNMT</div></a></h6>
        	</div>
        	<div ng-show="userRights.urEditChln">
        		<h6 class="collection level2data"><a href="#/operator/editChln" class="collection-item"><div class="l2">Edit Challan</div></a></h6>
        	</div>
        	<div ng-show="userRights.urCancelChln">
        		<h6 class="collection level2data"><a href="#/operator/cancelChallan" class="collection-item"><div class="l2">Cancel Challan</div></a></h6>
        	</div>
        	<div ng-show="userRights.urLedger">
        		<h6 class="collection level2data"><a href="#/operator/ledger" class="collection-item"><div class="l2">Ledger</div></a></h6>
  			</div>
        	<div ng-show="userRights.urTrial">
        		<h6 class="collection level2data"><a href="#/operator/trial" class="collection-item"><div class="l2">Trial</div></a></h6>
        	</div>
        	<div ng-show="userRights.urCustStationary">
        		<h6 class="collection level2data"><a href="#/operator/custStationary" class="collection-item"><div class="l2">Cust Stationary</div></a></h6>
        	</div>
        	
        	<div ng-show="userRights.urCustStationary">
        		<h6 class="collection level2data"><a href="#/operator/brhStkInfm" class="collection-item"><div class="l2">Branch Stock Information</div></a></h6>
        	</div>
        	
        	<div ng-show="userRights.urCustStationary">
        		<h6 class="collection level2data"><a href="#/operator/stnTransfer" class="collection-item"><div class="l2">Stationary Transfer</div></a></h6>
        	</div>
        	
        	<!-- <h6 class="collection level2data"><a href="#/operator/billTemp" class="collection-item"><div class="l2">Bill Entry</div></a></h6> -->
       <accordion  style="margin: 0px 0px;" close-others="oneAtATime">
       
       <div ng-show="userRights.urEnquiry">
	       	<accordion-group is-open="status.open.enquiry">
	  			<accordion-heading>
					<div class="level1 l2" ng-class="{'greyborder': status.open.enquiry, 'row': !status.open.enquiry}"><b>Enquiry</b></div>
	  			</accordion-heading>
	               	<h6 class="collection level2data"><a href="#/operator/enquiry" class="collapsible-header collection-item "><div class="l5">Enquiry</div></a></h6>
			</accordion-group>
       </div>
       
       <div ng-show="userRights.urReports">
	       	<accordion-group is-open="status.open.report">
		   		<accordion-heading>
					<div class="level1 l2" ng-class="{'greyborder': status.open.report, 'row': !status.open.report}"><b>Reports</b></div>
		  		</accordion-heading>
		           	<h6 class="collection level2data"><a href="#/operator/brReconRpt" class="collapsible-header collection-item "><div class="l5">Reconciliation</div></a></h6>
<!-- 		           	<h6 class="collection level2data"><a href="#/operator/stockAndOsRpt" class="collapsible-header collection-item "><div class="l5">Stock & O/S</div></a></h6> -->
		           	<h6 class="collection level2data"><a href="#/operator/hoStockAndOsRpt" class="collapsible-header collection-item "><div class="l5">H.O. Stock&O/S</div></a></h6>
		           	<h6 class="collection level2data"><a href="#/operator/stockRpt" class="collapsible-header collection-item "><div class="l5">Stock</div></a></h6>
<!-- 		           	<h6 class="collection level2data"><a href="#/operator/osRpt" class="collapsible-header collection-item "><div class="l5">O/S</div></a></h6> -->
	      			<h6 class="collection level2data"><a href="#/operator/osRptN" class="collapsible-header collection-item "><div class="l5">New O/S</div></a></h6>
                    <h6 class="collection level2data"><a href="#/operator/relOsRpt" class="collapsible-header collection-item "><div class="l5">Reliance O/S</div></a></h6>
                    <h6 class="collection level2data"><a href="#/operator/relOs" class="collapsible-header collection-item "><div class="l5">Reliance O/S New</div></a></h6>
                    <h6 class="collection level2data"><a href="#/operator/relianceStock" class="collapsible-header collection-item "><div class="l5">Reliance Stock</div></a></h6>
                    <h6 class="collection level2data"><a href="#/operator/newRelianceStock" class="collapsible-header collection-item "><div class="l5">Reliance Stock New</div></a></h6>
                    <h6 class="collection level2data"><a href="#/operator/shortExcessRpt" class="collapsible-header collection-item "><div class="l5">ShortExcess</div></a></h6>
	      			<h6 class="collection level2data"><a href="#/operator/relAnnextureRpt" class="collapsible-header collection-item"><div class="l5">RelAnnexture</div></a></h6> 
	      			<h6 class="collection level2data"  ng-show="userRights.urCosting"><a href="#/operator/cnmtCosting" class="collapsible-header collection-item"><div class="l5" >Costing</div></a></h6>  
                     <h6 class="collection level2data"><a href="#/operator/cnmtFrmToDtl" class="collapsible-header collection-item"><div class="l5">Cnmt List</div></a></h6>
                     <h6 class="collection level2data"  ng-show="userRights.urMisngStnry"><a href="#/admin/cnMisReport" class="collapsible-header collection-item"><div class="l5">CN Misssing Report</div></a></h6>
                <h6 class="collection level2data"  ng-show="userRights.urMisngStnry"><a href="#/admin/chlnMisReport" class="collapsible-header collection-item"><div class="l5">CHLN Misssing Report</div></a></h6>
                <h6 class="collection level2data"  ng-show="userRights.urMisngStnry"><a href="#/admin/sedrMisReport" class="collapsible-header collection-item"><div class="l5">SEDR Misssing Report</div></a></h6>
              
	       </accordion-group>
       </div>
       
       
       <div ng-show="userRights.urBranch">
		 <accordion-group is-open="status.open.branch">
        	<accordion-heading>
			<div class="level1 l2" ng-class="{'greyborder': status.open.branch, 'row': !status.open.branch}"><b>Branch</b></div>
        	</accordion-heading>
                        <h6 class="collection"><a href="#/operator/branch" class="collapsible-header collection-item level2data"><div class="l5">New</div></a></h6>
                        <h6 class="collection"><a href="#/operator/viewbranch" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>
    	</accordion-group>	 
    	</div>
    	 
    	<div ng-show="userRights.urEmployee"> 	 	
    	<accordion-group is-open="status.open.emp">
        	<accordion-heading>
			<div class="level1 l2" ng-class="{'greyborder': status.open.emp, 'row': !status.open.emp}"><b>Employee</b></div>
        	</accordion-heading>
                        <h6 class="collection"><a href="#/operator/employee" class="collapsible-header collection-item level2data"><div class="l5">New</div></a></h6>
                        <h6 class="collection"><a href="#/operator/viewemployee" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>
    	</accordion-group>	
    	</div>
    	
    	<div ng-show="userRights.urAssigned">
    		<accordion-group is-open="status.open.asgnUser">
        	<accordion-heading>
			<div class="level1 l2" ng-class="{'greyborder': status.open.asgnUser, 'row': !status.open.asgnUser}"><b>Assigned User</b></div>
        	</accordion-heading>
                        <h6 class="collection"><a href="#/operator/asgnUser" class="collapsible-header collection-item level2data"><div class="l5">Assign</div></a></h6>
                        <h6 class="collection"><a href="#/operator/viewemployee" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>
    	</accordion-group>
    	
    	<accordion-group is-open="status.open.assigned">
        	<accordion-heading>
			<div class="level1 l2" ng-class="{'greyborder': status.open.assigned, 'row': !status.open.assigned}"><b>Assigned</b></div>
        	</accordion-heading>
                        <h6 class="collection"><a href="#/operator/empWiseAssgn" class="collapsible-header collection-item level2data"><div class="l5">Employee Wise</div></a></h6>
						<h6 class="collection"><a href="#/operator/brhWiseAssgn" class="collapsible-header collection-item level2data"><div class="l5">Branch Wise</div></a></h6>                       
    	</accordion-group>	
    	</div>
       	
       	<div ng-show="userRights.urCustomer">
		<accordion-group is-open="status.open.cust">
        	<accordion-heading>
			<div class="level1 l2" ng-class="{'greyborder': status.open.cust, 'row': !status.open.cust}"><b>Customer</b></div>
        	</accordion-heading>
            
                        <h6 class="collection level2data"><a href="#/operator/customer" class="collapsible-header collection-item"><div class="l5">New</div></a></h6>
                        <h6 class="collection level2data"><a href="#/operator/addGST" class="collapsible-header collection-item "><div class="l5">Add GST</div></a></h6>
                        <h6 class="collection level2data"><a href="#/operator/viewcustomer" class="collapsible-header collection-item "><div class="l5">View</div></a></h6>
  				<accordion close-others="oneAtATime">
    				<accordion-group is-open="status.open.custR">
        				<accordion-heading>
            			<div class="level2 l3" ng-class="{'grey lighten-2': status.open.custR, 'row': !status.open.custR}"><b>Cust. Rep.</b></div>
        				</accordion-heading>
                        <h6 class="collection level2data"><a href="#/operator/customerrepresentativenew" class="collapsible-header collection-item "><div class="l5">New</div></a></h6>
                        <h6 class="collection level2data"><a href="#/operator/displaycustomerrepresentative" class="collapsible-header collection-item "><div class="l5">View</div></a></h6>
    				</accordion-group>
  				</accordion>		
					<accordion close-others="oneAtATime">
    				<accordion-group is-open="status.open.custGroup">
        				<accordion-heading>
            			<div class="level2 l3" ng-class="{'grey lighten-2': status.open.custGroup, 'row': !status.open.custGroup}"><b>CustomerGroup</b></div>
        				</accordion-heading>
                        <h6 class="collection level2data"><a href="#/operator/customerGroup" class="collapsible-header collection-item "><div class="l5">New</div></a></h6>
                      </accordion-group>
  				</accordion>
    	</accordion-group> 	
		</div>
		
		<div ng-show="userRights.urContract || userRights.urDlyContract">
	  	<accordion-group is-open="status.open.contract">
        	<accordion-heading>
			<div class="level1 l2" ng-class="{'greyborder': status.open.contract, 'row': !status.open.contract}"><b>Contract</b></div>
        	</accordion-heading>
  				<accordion close-others="oneAtATime">
    				<accordion-group is-open="status.open.newcontract">
        				<accordion-heading>
                        <div class="level2 l3" ng-class="{'grey lighten-2': status.open.newcontract, 'row': !status.open.newcontract}"><b><div class="">New</div></b></div>
        				</accordion-heading>
        					<div ng-show="userRights.urContract">
	                        	<h6 class="collection level2data"><a href="#/operator/regularcontract" class="collapsible-header collection-item "><div class="l5">Regular</div></a></h6>
	                        </div>	
	                        <div ng-show="userRights.urDlyContract">
	                        	<h6 class="collection level2data"><a href="#/operator/dailycontract" class="collapsible-header collection-item " style="text-decoration:none;"><div class="l5">Daily</div></a></h6>
	                        </div>	
    				</accordion-group>
    				<div ng-show="userRights.urContract || userRights.urDlyContract">
    					<h6 class="collection level2data"><a href="#/operator/viewcontract" class="collapsible-header collection-item "><div class="l5">View</div></a></h6>
    				</div>	
    				<div ng-show="userRights.urContract">
    					<h6 class="collection level2data"><a href="#/operator/dlyContAuth" class="collapsible-header collection-item "><div class="l5">Daily Contract Auth</div></a></h6>
    				<h6 class="collection level2data"><a href="#/operator/extContDt" class="collapsible-header collection-item "><div class="l5">Extend Contract Date</div></a></h6>
    				</div>	
  					 <!-- <accordion-group is-open="status.open.viewcontract">
        				<accordion-heading>
						<div class="level2" ng-class="{'grey lighten-2': status.open.viewcontract, 'row': !status.open.viewcontract}"><b>View Contract</b></div>
        				</accordion-heading>
	                        <h6 class="collection level3data"><a href="#/operator/viewregularcontract" class="collapsible-header collection-item">View Regular Contract</a></h6>
	                        <h6 class="collection level3data"><a href="#/operator/viewcontract" class="collapsible-header collection-item">View Contracts</a></h6>
    				</accordion-group> -->
  				</accordion>
    	</accordion-group>
      	</div>
      	
      	
      	<div ng-show="userRights.urCnmt">		
    	<accordion-group is-open="status.open.cnmt">
        	<accordion-heading>
			<div class="level1 l2" ng-class="{'greyborder': status.open.cnmt, 'row': !status.open.cnmt}"><b>CNMT/Bilty</b></div>
        	</accordion-heading>
                        <h6 class="collection level2data"><a href="#/operator/cnmt" class="collapsible-header collection-item "><div class="l5">New</div></a></h6>
                        <h6 class="collection level2data"><a href="#/operator/viewcnmt" class="collapsible-header collection-item "><div class="l5">View/Edit</div></a></h6>
                        <div ng-show="userRights.urCnmtBbl">
                        	<h6 class="collection level2data"><a href="#/operator/cnmtBbl" class="collapsible-header collection-item "><div class="l5">New Barbil</div></a></h6>
                        </div>
    	</accordion-group>
		</div>	
	   	
	   	<div ng-show="userRights.urChallan">
    	<accordion-group is-open="status.open.challan">
        	<accordion-heading>
			<div class="level1 l2" ng-class="{'greyborder': status.open.challan, 'row': !status.open.challan}"><b>Challan</b></div>
        	</accordion-heading>
                        <h6 class="collection level2data"><a href="#/operator/challan" class="collapsible-header collection-item "><div class="l5">New</div></a></h6>
                        <h6 class="collection level2data"><a href="#/operator/viewChallan" class="collapsible-header collection-item "><div class="l5">View/Edit</div></a></h6>
                        <!-- <h6 class="collection level2data"><a href="#/operator/stationaryorder" class="collapsible-header collection-item">Stationary Order</a></h6> -->
    	</accordion-group>
    	</div>

    	<accordion-group is-open="status.open.relCNStatus">
        	<accordion-heading>
			<div class="level1 l2" ng-class="{'greyborder': status.open.relCNStatus, 'row': !status.open.relCNStatus}"><b>Reliance CN Status</b></div>
        	</accordion-heading>
                        <h6 class="collection level2data"><a href="#/operator/relCNStatus" class="collapsible-header collection-item "><div class="l5">New</div></a></h6>
                        <h6 class="collection level2data"><a href="#/operator/viewRelCNStatus" class="collapsible-header collection-item "><div class="l5">View/Edit</div></a></h6>
    	</accordion-group>

	 	
	 	<div ng-show="userRights.urSedr">
    	<accordion-group is-open="status.open.arrivalReport">
        				<accordion-heading>
						<div class="level1 l2" ng-class="{'greyborder': status.open.arrivalReport, 'row': !status.open.arrivalReport}"><b>Arrival Report</b></div>
        				</accordion-heading>
	                       <h6 class="collection level2data"><a href="#/operator/arrivalreport" class="collapsible-header collection-item "><div class="l5">New</div></a></h6>
	                       <h6 class="collection level2data"><a href="#/operator/viewarrivalreport" class="collapsible-header collection-item "><div class="l5">View/Edit</div></a></h6>
	                       <h6 class="collection level2data"><a href="#/operator/memo" class="collapsible-header collection-item "><div class="l5">Memo</div></a></h6>
						<h6 class="collection level2data" ng-show="userRights.allowAr"><a href="#/operator/allowAr" class="collapsible-header collection-item "><div class="l5">Validate AR</div></a></h6>
	                       <h6 class="collection level2data" ng-show="userRights.urAckValidation"><a href="#/operator/ackValidator" class="collapsible-header collection-item "><div class="l5">Ack Validation</div></a></h6>
    	</accordion-group>
    	</div>
    	
    	<div ng-show="userRights.urSedrAlw">
    	<accordion-group is-open="status.open.arvRepAlw">
        				<accordion-heading>
						<div class="level1 l2" ng-class="{'greyborder': status.open.arvRepAlw, 'row': !status.open.arvRepAlw}"><b>Arrival Report Allowance</b></div>
        				</accordion-heading>
	                       <h6 class="collection level2data"><a href="#/operator/newArvRepAlw" class="collapsible-header collection-item "><div class="l5">New</div></a></h6>
	                       <h6 class="collection level2data"><a href="#/operator/viewArvRepAlw" class="collapsible-header collection-item "><div class="l5">View</div></a></h6>
    	</accordion-group>
    	</div>
    	
    	<div ng-show="userRights.urStnSup">		
    	<accordion-group is-open="status.open.supplier">
        				<accordion-heading>
						<div class="level1 l2" ng-class="{'greyborder': status.open.supplier, 'row': !status.open.supplier}"><b>Stationary Supplier</b></div>
        				</accordion-heading>
	                       <h6 class="collection"><a href="#/operator/supplier" class="collapsible-header collection-item level2data"><div class="l5">New</div></a></h6>
	                       <h6 class="collection"><a href="#/operator/viewsupplier" class="collapsible-header collection-item level2data"><div class="l5">View/Edit</div></a></h6>
    	</accordion-group>
		</div>
    	<!-- <accordion-group is-open="status.open.stationary">
    		<accordion-heading>
			<div class="level1" ng-class="{'greyborder': status.open.stationary, 'row': !status.open.stationary}"><b>Stationary</b></div>
        	</accordion-heading>
        		<h6 class="collection"><a href="#/operator/stationary" class="collapsible-header collection-item level2data">Create Order</a></h6>
    	</accordion-group> -->
   				  		
			    	<!-- <accordion-group is-open="status.open.toprates">
	        			<accordion-heading>
						<div class="level1" ng-class="{'greyborder': status.open.misc, 'row': !status.open.misc}"><b>Top Rates</b></div>
	        		</accordion-heading>
	        		 <h6 class="collection"><a href="#/operator/toprates" class="collapsible-header collection-item level3data">Add Top Rates</a></h6>
	        	</accordion-group> -->
    	
    	
    	<!-- <accordion-group is-open="status.open.dispatchOrder">
        	<accordion-heading>
			<div class="level1" ng-class="{'greyborder': status.open.dispatchOrder, 'row': !status.open.dispatchOrder}"><b>Dispatch Order</b></div>
        	</accordion-heading>
                        <h6 class="collection level2data"><a href="#/operator/creatDispatchOrder" class="collapsible-header collection-item">Create Order</a></h6>
                        <h6 class="collection level2data nobottomborder"><a href="#/operator/viewOwner" class="collapsible-header collection-item">View Owner</a></h6>
    	</accordion-group> -->
    	   	<div ng-show="userRights.urOwner">	
			    	<accordion-group is-open="status.open.owner">
        				<accordion-heading>
						<div class="level1 l2" ng-class="{'greyborder': status.open.owner, 'row': !status.open.owner}"><b>Owner</b></div>
        				</accordion-heading>
                        <h6 class="collection level2data"><a href="#/operator/owner" class="collapsible-header collection-item "><div class="l5">Add Vehicle/Owner</div></a></h6>
                         <h6 class="collection level2data"><a href="#/operator/ownbnkDet" class="collapsible-header collection-item "><div class="l5">Add Bank Details</div></a></h6>
                        <h6 class="collection level2data"><a href="#/operator/viewOwner" class="collapsible-header collection-item "><div class="l5">View/Edit</div></a></h6>
    				</accordion-group>
    		</div>
    		
    		<div ng-show="userRights.urBroker">
			    	<accordion-group is-open="status.open.broker">
			        	<accordion-heading>
						<div class="level1 l2" ng-class="{'greyborder': status.open.broker, 'row': !status.open.broker}"><b>Broker</b></div>
        				</accordion-heading>
                        <h6 class="collection level2data"><a href="#/operator/broker" class="collapsible-header collection-item "><div class="l5">Add</div></a></h6>
                         <h6 class="collection level2data"><a href="#/operator/brkbnkDet" class="collapsible-header collection-item "><div class="l5">Add Bank Details</div></a></h6>
                        <h6 class="collection level2data"><a href="#/operator/viewBroker" class="collapsible-header collection-item "><div class="l5">View/Edit</div></a></h6>
    				</accordion-group>
    		</div>
    		
    		<div ng-show="userRights.urVehVen">
	    		<accordion-group is-open="status.open.vehicleVendor">
		        	<accordion-heading>
						<div class="level1 l2" ng-class="{'greyborder': status.open.vehicleVendor, 'row': !status.open.vehicleVendor}"><b>Vehicle Vendor</b></div>
		     		</accordion-heading>
<!-- 		                <h6 class="collection level2data"><a href="#/operator/newVehicleVendor" class="collapsible-header collection-item "><div class="l5">Add Vehicle</div></a></h6> -->
	                    <h6 class="collection level2data"><a href="#/operator/viewVehicleVendor" class="collapsible-header collection-item "><div class="l5">View Vehicle</div></a></h6>
<!-- 	                    <h6 class="collection level2data"><a href="#/operator/editVehicleVendor" class="collapsible-header collection-item "><div class="l5">Edit Vehicle</div></a></h6> -->
	                    <h6 class="collection level2data"><a href="#/operator/addPan" class="collapsible-header collection-item "><div class="l5">Add Pan</div></a></h6>
	    		</accordion-group>		
    		</div>
    				<!-- <accordion-group is-open="status.open.bill">
        				<accordion-heading>
						<div class="level1" ng-class="{'greyborder': status.open.bill, 'row': !status.open.bill}"><b>Bill</b></div>
        				</accordion-heading>
                        <h6 class="collection"><a href="#/operator/bill" class="collapsible-header collection-item level2data">New</a></h6>
                        <h6 class="collection"><a href="#/operator/viewbill" class="collapsible-header collection-item level2data">View</a></h6>
    				</accordion-group> -->
		
		<div ng-show="userRights.urBill">		
			<accordion-group is-open="status.open.bill">
      				<accordion-heading>
						<div class="level1 l2" ng-class="{'greyborder': status.open.bill, 'row': !status.open.bill}"><b>Bill</b></div>
      				</accordion-heading>
      					<h6 class="collection"><a href="#/operator/gstBill" class="collapsible-header collection-item level2data"><div class="l5">new GstBill</div></a></h6>
                      	<h6 class="collection"><a href="#/operator/gstRelBill" class="collapsible-header collection-item level2data"><div class="l5">Reliance GstBill</div></a></h6>
                      		<h6 class="collection" ng-show="userRights.urManualBill"><a href="#/operator/gstManualBill" class="collapsible-header collection-item level2data"><div class="l5">Manual GstBill</div></a></h6>
                      		<h6 class="collection" ng-show="userRights.urManualBill"><a href="#/operator/manualBill" class="collapsible-header collection-item level2data"><div class="l5">Manual</div></a></h6>
<!--       					<h6 class="collection" ng-show="userRights.urBillHO"><a href="#/operator/newBillHO" class="collapsible-header collection-item level2data"><div class="l5">Bill(HO)</div></a></h6> -->
      					<h6 class="collection"><a href="#/operator/groupBill" class="collapsible-header collection-item level2data"><div class="l5">Group Bill</div></a></h6>
<!--                       	<h6 class="collection"><a href="#/operator/newBill" class="collapsible-header collection-item level2data"><div class="l5">new</div></a></h6> -->
                      	<!-- <h6 class="collection"><a href="#/operator/viewBill" class="collapsible-header collection-item level2data"><div class="l5">view</div></a></h6> -->
                      	<h6 class="collection"><a href="#/operator/billFrwd" class="collapsible-header collection-item level2data"><div class="l5">Bill Forwarding</div></a></h6>
                      	<h6 class="collection"><a href="#/operator/billSbmsn" class="collapsible-header collection-item level2data"><div class="l5">Bill Submission</div></a></h6>                      	
                      	<div ng-show="userRights.urBillCancel">
                      	<h6 class="collection"><a href="#/operator/billCancel" class="collapsible-header collection-item level2data"><div class="l5">Bill Cancel</div></a></h6>
    		            </div>	
    		            <div ng-show="userRights.urEditBill">
    		            <h6 class="collection"><a href="#/operator/editBill" class="collapsible-header collection-item level2data"><div class="l5">Bill Editing</div></a></h6>
    		            <h6 class="collection"><a href="#/operator/editBillGST" class="collapsible-header collection-item level2data"><div class="l5">GST Bill Editing</div></a></h6>
    		            </div>
    		            <h6 class="collection" ng-show="userRights.urGenrateRelExcel"><a href="#/operator/relianceExcel" class="collapsible-header collection-item level2data"><div class="l5">Reliance CN Excel</div></a></h6>
    		            <h6 class="collection" ng-show="userRights.urShrtExcessUpdate"><a href="#/operator/removeFrmShrtExcess" class="collapsible-header collection-item level2data"><div class="l5">Edit Short Excess</div></a></h6>
										
    			</accordion-group>	
		</div>
		<accordion-group is-open="status.open.print">
      				<accordion-heading>
						<div class="level1 l2" ng-class="{'greyborder': status.open.print, 'row': !status.open.print}"><b>Print </b></div>
      				</accordion-heading>
				<h6 class="collection"><a href="#/operator/billPrint" class="collapsible-header collection-item level2data"><div class="l5">Bill Print</div></a></h6>
						<div ng-show="userRights.urRelBl">
						<h6 class="collection"><a href="#/operator/relBill" class="collapsible-header collection-item level2data"><div class="l5">Reliance Bill</div></a></h6>
						</div>
						<h6 class="collection"><a href="#/operator/billFrwdPrint" class="collapsible-header collection-item level2data"><div class="l5">Bill Forwarding Print</div></a></h6>
				</accordion-group>			
		<!-- <accordion-group is-open="status.open.bill">
        	<accordion-heading>
			<div class="level1 l2" ng-class="{'greyborder': status.open.bill, 'row': !status.open.bill}"><b>Bill</b></div>
        	</accordion-heading>
  				<accordion close-others="oneAtATime">
    				
    				<accordion-group is-open="status.open.billbycnmt">
        				<accordion-heading>
						<div class="level2 l3" ng-class="{'grey lighten-2': status.open.billbycnmt, 'row': !status.open.billbycnmt}"><b><div class="">Bill By Cnmt</div></b></div>
        				</accordion-heading>
	                        <h6 class="collection"><a href="#/operator/bill" class="collapsible-header collection-item level2data"><div class="l5">Add</div></a></h6>
	                        <h6 class="collection"><a href="#/operator/viewbill" class="collapsible-header collection-item level2data"><div class="l5">View/Edit</div></a></h6>
    				</accordion-group>
    				
    				<accordion-group is-open="status.open.billbychln">
        				<accordion-heading>
						<div class="level2 l3" ng-class="{'grey lighten-2': status.open.billbychln, 'row': !status.open.billbychln}"><b><div class="">Bill By Chln</div></b></div>
        				</accordion-heading>
	                        <h6 class="collection"><a href="#/operator/billByChln" class="collapsible-header collection-item level2data"><div class="l5">Add</div></a></h6>
	                        <h6 class="collection"><a href="#/operator/viewBillByChln" class="collapsible-header collection-item level2data"><div class="l5">View/Edit</div></a></h6>
    				</accordion-group>
    			</accordion-group>	
    				<accordion-group is-open="status.open.supbill">
        				<accordion-heading>
						<div class="level1 l2" ng-class="{'greyborder': status.open.supbill, 'row': !status.open.supbill}"><b>Supplimentary Bill</b></div>
        				</accordion-heading>
                        	<h6 class="collection"><a href="#/operator/supBillWCnmt" class="collapsible-header collection-item level2data"><div class="l5">With Cnmt</div></a></h6>
                        	<h6 class="collection"><a href="#/operator/supBillWOCnmt" class="collapsible-header collection-item level2data"><div class="l5">Without</div></a></h6>
    				</accordion-group> -->
    	 		
<!--     	<accordion-group is-open="status.open.billforwarding">
        	<accordion-heading>
			<div class="level1 l2" ng-class="{'greyborder': status.open.billforwarding, 'row': !status.open.billforwarding}"><b>Bill Forwarding</b></div>
        	</accordion-heading>
  				<accordion close-others="oneAtATime">
    				
    				<accordion-group is-open="status.open.billforwardbycnmt">
        				<accordion-heading>
						<div class="level2 l3" ng-class="{'grey lighten-2': status.open.billforwardbycnmt, 'row': !status.open.billforwardbycnmt}"><b><div class="">By Cnmt</div></b></div>
        				</accordion-heading>
	                        <h6 class="collection"><a href="#/operator/bfByCnmt" class="collapsible-header collection-item level2data"><div class="l5">Add</div></a></h6>
	                        <h6 class="collection"><a href="#/operator/viewBfByCnmt" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>
    				</accordion-group>
    				
    				<accordion-group is-open="status.open.billforwardwocnmt">
        				<accordion-heading>
						<div class="level2 l3" ng-class="{'grey lighten-2': status.open.billforwardwocnmt, 'row': !status.open.billforwardwocnmt}"><b><div class="">Without Cnmt</div></b></div>
        				</accordion-heading>
	                        <h6 class="collection"><a href="#/operator/bfWOCnmt" class="collapsible-header collection-item level2data"><div class="l5">Add</div></a></h6>
	                        <h6 class="collection"><a href="#/operator/viewBfWOCnmt" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>
    				</accordion-group>
    			</accordion-group>	 -->
    			
    		
    		<div ng-show="userRights.urPanVal">			
   				<accordion-group is-open="status.open.panValid">
       				<accordion-heading>
						<div class="level1 l2" ng-class="{'greyborder': status.open.panValid, 'row': !status.open.panValid}"><b>Validation</b></div>
       				</accordion-heading>
                       	<h6 class="collection level2data"><a href="#/operator/panValid" class="collapsible-header collection-item "><div class="l5">PAN Validation</div></a></h6>
                       	<h6 class="collection level2data"><a href="#/operator/verifyAccount" class="collapsible-header collection-item "><div class="l5">Account Validation</div></a></h6>
              			 <h6 class="collection level2data"  ng-show="userRights.urAccValid"><a href="#/operator/invalidAcc" class="collapsible-header collection-item "><div class="l5">Edit/verify invalid A/C</div></a></h6>
                    </accordion-group>	
             </div>   
    	
    	<div ng-show="userRights.urMisc">			
    	   	<accordion-group is-open="status.open.misc">
        	<accordion-heading>
			<div class="level1 l2" ng-class="{'greyborder': status.open.misc, 'row': !status.open.misc}"><b>Miscellaneous</b></div>
        	</accordion-heading>
  				<accordion close-others="oneAtATime">

    				<accordion-group is-open="status.open.product">
        				<accordion-heading>
						<div class="level2 l3" ng-class="{'grey lighten-2': status.open.product, 'row': !status.open.product}"><b><div class="">Product Type</div></b></div>
        				</accordion-heading>
	                        <h6 class="collection"><a href="#/operator/producttype" class="collapsible-header collection-item level2data"><div class="l5">Add</div></a></h6>
	                        <h6 class="collection"><a href="#/operator/viewproducttype" class="collapsible-header collection-item level2data"><div class="l5">View/Edit</div></a></h6>
    				</accordion-group>

    				<accordion-group is-open="status.open.vehicle">
        				<accordion-heading>
						<div class="level2 l3" ng-class="{'grey lighten-2': status.open.vehicle, 'row': !status.open.vehicle}"><b><div class="">Vehicle Type</div></b></div>
        				</accordion-heading>
	                        <h6 class="collection"><a href="#/operator/vehicletype" class="collapsible-header collection-item level2data"><div class="l5">Add</div></a></h6>
	                        <h6 class="collection"><a href="#/operator/viewvehicletype" class="collapsible-header collection-item level2data"><div class="l5">View/Edit</div></a></h6>
    				</accordion-group>

    				<accordion-group is-open="status.open.state">
        				<accordion-heading>
                        <div class="level2 l3" ng-class="{'grey lighten-2': status.open.state, 'row': !status.open.state}"><b><div class="">State</div></b></div>
        				</accordion-heading>
	                        <h6 class="collection"><a href="#/operator/state" class="collapsible-header collection-item level2data"><div class="l5">Add</div></a></h6>
	                        <h6 class="collection"><a href="#/operator/viewstate" class="collapsible-header collection-item level2data"><div class="l5">View/Edit</div></a></h6>
    				</accordion-group>

  					<accordion-group is-open="status.open.station">
        				<accordion-heading>
						<div class="level2 l3" ng-class="{'grey lighten-2': status.open.station, 'row': !status.open.station}"><b><div class="">Station</div></b></div>
        				</accordion-heading>
	                        <h6 class="collection"><a href="#/operator/station" class="collapsible-header collection-item level2data"><div class="l5">Add</div></a></h6>
	                        <h6 class="collection"><a href="#/operator/viewstation" class="collapsible-header collection-item level2data"><div class="l5">View/Edit</div></a></h6>
    				</accordion-group>
    				
    				

  				</accordion>
		   </accordion-group> 
		    		
		   </div>   	
		    		
    			   	</accordion>
    	</accordion-group>						
					
					
			
				
				
				<!-- Telephone -->
				<div ng-show="userRights.urTel">			
					<accordion-group is-open="status.open.telephone">
						<accordion-heading>
							<div class="level1 l2" ng-class="{'greyborder': status.open.telephone, 'row': !status.open.telephone}"><b>Telephone</b></div>
						</accordion-heading>
							<h6 class="collection"><a href="#/operator/newPhoneAllot" class="collapsible-header collection-item level2data"><div class="l5">Phone Allotment</div></a></h6>
							<h6 class="collection"><a href="#/operator/viewPhoneAllot" class="collapsible-header collection-item level2data"><div class="l5">View Phone Allotment</div></a></h6>
					</accordion-group>
				</div>
				<!-- Electricity -->
				<div ng-show="userRights.urEle">			
					<accordion-group is-open="status.open.electricity">
						<accordion-heading>
							<div class="level1 l2" ng-class="{'greyborder': status.open.electricity, 'row': !status.open.electricity}"><b>Electricity</b></div>
						</accordion-heading>
							<h6 class="collection"><a href="#/operator/newCRNAllot" class="collapsible-header collection-item level2data"><div class="l5">CRN Allotment</div></a></h6>
							<h6 class="collection"><a href="#/operator/viewCRNAllot" class="collapsible-header collection-item level2data"><div class="l5">View CRN Allotment</div></a></h6>
					</accordion-group>
				</div>	
				
				<!-- Vehicle Mstr -->
				<div ng-show="userRights.urVeh">			
					<accordion-group is-open="status.open.vehicleMstr">
						<accordion-heading>
							<div class="level1 l2" ng-class="{'greyborder': status.open.vehicleMstr, 'row': !status.open.vehicleMstr}"><b>Vehicle</b></div>
						</accordion-heading>
							<h6 class="collection"><a href="#/operator/newVehAllot" class="collapsible-header collection-item level2data"><div class="l5">Vehicle Allotment</div></a></h6>
							<h6 class="collection"><a href="#/operator/viewVehAllot" class="collapsible-header collection-item level2data"><div class="l5">View Vehicle Allotment</div></a></h6>
					</accordion-group>
				</div>
				
				<!-- Rent Mstr -->
				<div ng-show="userRights.urRent">
				<accordion-group is-open="status.open.rentMstr">
					<accordion-heading>
						<div class="level1 l2" ng-class="{'greyborder': status.open.rentMstr, 'row': !status.open.rentMstr}"><b>Rent</b></div>
					</accordion-heading>
						<h6 class="collection"><a href="#/operator/newRent" class="collapsible-header collection-item level2data"><div class="l5">New Rent</div></a></h6>
						<h6 class="collection"><a href="#/operator/viewRent" class="collapsible-header collection-item level2data"><div class="l5">View Rent</div></a></h6>
				</accordion-group>
				</div>
				
				<!-- Rent Mstr -->
				<div ng-show="userRights.urRent">
				<accordion-group is-open="status.open.rentMstr">
					<accordion-heading>
						<div class="level1 l2" ng-class="{'greyborder': status.open.rentMstr, 'row': !status.open.rentMstr}"><b>Rent</b></div>
					</accordion-heading>
						<h6 class="collection"><a href="#/operator/newRent" class="collapsible-header collection-item level2data"><div class="l5">New Rent</div></a></h6>
						<h6 class="collection"><a href="#/operator/viewRent" class="collapsible-header collection-item level2data"><div class="l5">View Rent</div></a></h6>
				</accordion-group>
				</div>	
					
    	
    	 
						
										
						
						
  
		<accordion-group is-open="status.open.financial">
			<accordion-heading>
				<div class="level1 l1" ng-class="{'greyborder': status.open.financial, 'row': !status.open.financial}"><b>Financial</b></div>
			</accordion-heading>
							<!-- Voucher -->
				<accordion  style="margin: 0px 0px;" close-others="oneAtATime">
				<div ng-show="userRights.urVouch">			
					<accordion-group is-open="status.open.voucher">
						<accordion-heading>
							<div class="level1 l2" ng-class="{'greyborder': status.open.voucher, 'row': !status.open.voucher}"><b>Voucher</b></div>
						</accordion-heading>
							<accordion  style="margin: 0px 0px;" close-others="oneAtATime">
							
								<accordion-group is-open="status.open.cashWithdraw">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.cashWithdraw, 'row': !status.open.cashWithdraw}"><b>Cash Withdrawl (CHQ)</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/newVoucher" class="collapsible-header collection-item level2data"><div class="l5">New</div></a></h6>
										<h6 class="collection"><a href="#/operator/viewVoucher" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>
								</accordion-group>
								
								<accordion-group is-open="status.open.atmVoucher">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.atmVoucher, 'row': !status.open.atmVoucher}"><b>Cash Withdrawl (ATM)</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/newAtmVoucher" class="collapsible-header collection-item level2data"><div class="l5">New</div></a></h6>
										<h6 class="collection"><a href="#/operator/viewAtmVoucher" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>
								</accordion-group>
								
								<accordion-group is-open="status.open.cashReciept">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.cashReciept, 'row': !status.open.cashReciept}"><b>Cash Reciept</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/newCashReciept" class="collapsible-header collection-item level2data"><div class="l5">New</div></a></h6>
										<h6 class="collection"><a href="#/operator/viewCashReciept" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>
								</accordion-group>
									
								<accordion-group is-open="status.open.cashDeposite">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.cashDeposite, 'row': !status.open.cashDeposite}"><b>Cash Deposite</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/newCashDeposite" class="collapsible-header collection-item level2data"><div class="l5">New</div></a></h6>
										<h6 class="collection"><a href="#/operator/viewCashDeposite" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>
								</accordion-group>
									 
								<accordion-group is-open="status.open.cashPayment">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.cashPayment, 'row': !status.open.cashPayment}"><b>Cash Payment</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/newCashPayment" class="collapsible-header collection-item level2data"><div class="l5">New</div></a></h6>
										<h6 class="collection"><a href="#/operator/viewCashPayment" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>
								</accordion-group>	
								
								<accordion-group is-open="status.open.chqPayVoucher">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.chqPayVoucher, 'row': !status.open.chqPayVoucher}"><b>Cheque Payment</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/newChqPayVoucher" class="collapsible-header collection-item level2data">New</a></h6>
										<h6 class="collection"><a href="#/operator/viewChqPayVoucher" class="collapsible-header collection-item level2data">View</a></h6>
								</accordion-group>
								
								<accordion-group is-open="status.open.chqCancelVoucher">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.chqCancelVoucher, 'row': !status.open.chqPayVoucher}"><b>Cheque Cancel</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/newChqCancelVoucher" class="collapsible-header collection-item level2data">New</a></h6>
										<h6 class="collection"><a href="#/operator/viewChqCancelVoucher" class="collapsible-header collection-item level2data">View</a></h6>
								</accordion-group>
								
								<accordion-group is-open="status.open.telephoneVoucher">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.telephoneVoucher, 'row': !status.open.telephoneVoucher}"><b>Telephone</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/newTelephoneVoucher" class="collapsible-header collection-item level2data"><div class="l5">New</div></a></h6>
										<h6 class="collection"><a href="#/operator/viewTelephoneVoucher" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>
								</accordion-group>
								
								<accordion-group is-open="status.open.electricityVoucher">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.electricityVoucher, 'row': !status.open.electricityVoucher}"><b>Electricity</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/newElectVoucher" class="collapsible-header collection-item level2data"><div class="l5">New</div></a></h6>
										<h6 class="collection"><a href="#/operator/viewElectVoucher" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>
								</accordion-group>
								
								<accordion-group is-open="status.open.vehicleVoucher">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.vehicleVoucher, 'row': !status.open.vehicleVoucher}"><b>Vehicle</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/newVehVoucher" class="collapsible-header collection-item level2data"><div class="l5">New</div></a></h6>
										<h6 class="collection"><a href="#/operator/viewVehVoucher" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>
								</accordion-group>
								
								<accordion-group is-open="status.open.travelVoucher">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.travelVoucher, 'row': !status.open.travelVoucher}"><b>Travel</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/newTravVoucher" class="collapsible-header collection-item level2data"><div class="l5">New</div></a></h6>
										<h6 class="collection"><a href="#/operator/viewTravVoucher" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>
								</accordion-group>
								
								<accordion-group is-open="status.open.rentVoucher">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.rentVoucher, 'row': !status.open.rentVoucher}"><b>Rent</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/newRentVoucher" class="collapsible-header collection-item level2data"><div class="l5">New</div></a></h6>
										<h6 class="collection" ng-show="userRights.urRentExcel"><a href="#/operator/newRentDetail" class="collapsible-header collection-item level2data"><div class="l5">Rent Pay</div></a></h6>
										<h6 class="collection" ng-show="userRights.urRentExcel"><a href="#/operator/rentXlsx" class="collapsible-header collection-item level2data"><div class="l5">Download Excel</div></a></h6>
								</accordion-group>
								
  								<accordion-group is-open="status.open.lhpvVouch">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.lhpvVouch, 'row': !status.open.lhpvVouch}"><b>Lhpv</b></div>
									</accordion-heading>
										<accordion  style="margin: 0px 0px;" close-others="oneAtATime">
											<accordion-group is-open="status.open.lhpvAdv">
												<accordion-heading>
													 <div class="level1 l4" ng-class="{'greyborder': status.open.lhpvAdv, 'row': !status.open.lhpvAdv}"><b>Lhpv Advance</b></div>
												</accordion-heading>
													<h6 class="collection"><a href="#/operator/newLhpvAdv" class="collapsible-header collection-item level2data"><div class="l5">New</div></a></h6>
<!-- 													<h6 class="collection"><a href="#/operator/viewLhpvAdv" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>
 -->											  <h6 class="collection"><a href="#/operator/rvrsLhpvAdv" class="collapsible-header collection-item level2data"><div class="l5">Revers</div></a></h6>
										</accordion-group>
											<accordion-group is-open="status.open.lhpvBal">
												<accordion-heading>
													<div class="level1 l4" ng-class="{'greyborder': status.open.lhpvBal, 'row': !status.open.lhpvBal}"><b>Lhpv Balance</b></div>
												</accordion-heading>
												 <h6 class="collection" ng-show="userRights.urOldLhpvBal"><a href="#/operator/newLhpvBal" class="collapsible-header collection-item level2data"><div class="l5">New(Old)</div></a></h6>											
												 <h6 class="collection" ng-show="userRights.urLhpvBalCash"><a href="#/operator/newLhpvBalCash" class="collapsible-header collection-item level2data"><div class="l5">Cash</div></a></h6>											
												 <h6 class="collection"><a href="#/operator/lhpvBalPay" class="collapsible-header collection-item level2data"><div class="l5">New (New )</div></a></h6>
											     <h6 class="collection"><a href="#/operator/rvrsLhpvBal" class="collapsible-header collection-item level2data"><div class="l5">Revers</div></a></h6>
												 <h6 class="collection" ng-show="userRights.urHoLhpv"><a href="#/operator/lhpvBalPayHo" class="collapsible-header collection-item level2data"><div class="l5">HO Balance Pay</div></a></h6>
												 <h6 class="collection" ng-show="userRights.urGenrateLbExcel"><a href="#/operator/payLhpvBalance" class="collapsible-header collection-item level2data"><div class="l5">Generate EXCEL</div></a></h6>
											</accordion-group>	
											<accordion-group is-open="status.open.lhpvSup">
												<accordion-heading>
													<div class="level1 l4" ng-class="{'greyborder': status.open.lhpvSup, 'row': !status.open.lhpvSup}"><b>Lhpv Supplementary</b></div>
												</accordion-heading>
													<h6 class="collection"><a href="#/operator/newLhpvSup" class="collapsible-header collection-item level2data"><div class="l5">New</div></a></h6>
													<h6 class="collection"><a href="#/operator/viewLhpvSup" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>											       
											</accordion-group>	 
											<accordion-group is-open="status.open.lhpvClose">
												<accordion-heading>
													<div class="level1 l4" ng-class="{'greyborder': status.open.lhpvClose, 'row': !status.open.lhpvClose}"><b>Close Lhpv</b></div>
												</accordion-heading>
													<h6 class="collection"><a href="#/operator/closeLhpv" class="collapsible-header collection-item level2data"><div class="l5">Closing Lhpv</div></a></h6>
											</accordion-group>
<!-- 											<accordion-group is-open="status.open.lhpvModify"> -->
<!-- 												<accordion-heading> -->
<!-- 													<div class="level1 l4" ng-class="{'greyborder': status.open.lhpvModify, 'row': !status.open.lhpvModify}"><b>ModiFy</b></div> -->
<!-- 												</accordion-heading> -->
<!-- 													<h6 class="collection" ><a href="#/operator/lhpvModify" class="collapsible-header collection-item level2data"><div class="l5">Lhpv Modify</div></a></h6> -->
<!-- 											</accordion-group>	  -->
										</accordion>
								</accordion-group>
								
								<accordion-group is-open="status.open.bPromVoucher">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.bPromVoucher, 'row': !status.open.bPromVoucher}"><b>Business Promotion</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/newBPVoucher" class="collapsible-header collection-item level2data">New</a></h6>
										<h6 class="collection"><a href="#/operator/viewBPVoucher" class="collapsible-header collection-item level2data">View</a></h6>
								</accordion-group>
								
								
								
								<accordion-group is-open="status.open.intBrTVCrDr">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.intBrTVCrDr, 'row': !status.open.intBrTVCrDr}"><b>Inter Branch TV C/D</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/newIntBrTVCrDr" class="collapsible-header collection-item level2data">New</a></h6>
										<h6 class="collection"><a href="#/operator/clrPendingIBTV" class="collapsible-header collection-item level2data">Clear Pending IBTV</a></h6>
										<h6 class="collection"><a href="#/operator/viewIntBrTVCrDr" class="collapsible-header collection-item level2data">View</a></h6>
								</accordion-group>
								
								<accordion-group is-open="status.open.jourVoucher">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.jourVoucher, 'row': !status.open.jourVoucher}"><b>Journal Voucher</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/newJourVoucher" class="collapsible-header collection-item level2data">New</a></h6>
										<h6 class="collection"><a href="#/operator/viewJourVoucher" class="collapsible-header collection-item level2data">View</a></h6>
								</accordion-group>
								
								<accordion-group is-open="status.open.revVoucher">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.revVoucher, 'row': !status.open.revVoucher}"><b>Voucher Reversal</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/newRevVoucher" class="collapsible-header collection-item level2data">New</a></h6>
										<h6 class="collection"><a href="#/operator/viewRevVoucher" class="collapsible-header collection-item level2data">View</a></h6>
								</accordion-group>
							
							<div ng-show="userRights.urBackLhpv">		
							<accordion-group is-open="status.open.lhpvTempVoucher">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.lhpvTempVoucher, 'row': !status.open.lhpvTempVoucher}"><b>PREV LHPV Voucher</b></div>
									</accordion-heading>
										<!-- <h6 class="collection"><a href="#/operator/newLhpvTemp" class="collapsible-header collection-item level2data">New</a></h6>
										<h6 class="collection"><a href="#/operator/viewLhpvTemp" class="collapsible-header collection-item level2data">View</a></h6> --> 
										<!-- <h6 class="collection"><a href="#/operator/lhpvStartTemp" class="collapsible-header collection-item level2data">Lhpv Start(Back)</a></h6> -->
										<h6 class="collection"><a href="#/operator/lhpvAdvTemp" class="collapsible-header collection-item level2data">Lhpv Advance(Back)</a></h6>
										<h6 class="collection"><a href="#/operator/lhpvBalTemp" class="collapsible-header collection-item level2data">Lhpv Balance(Back)</a></h6>
										<h6 class="collection"><a href="#/operator/closeLhpvTemp" class="collapsible-header collection-item level2data">Close Lhpv(Back)</a></h6>
								</accordion-group>
							</div>	
								
<!-- 								<accordion-group is-open="status.open.moneyReceipt"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.moneyReceipt, 'row': !status.open.moneyReceipt}"><b>Money Receipt</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<accordion  style="margin: 0px 0px;" close-others="oneAtATime"> -->
<!-- 											<accordion-group is-open="status.open.onAccMR"> -->
<!-- 												<accordion-heading> -->
<!-- 													 <div class="level1 l4" ng-class="{'greyborder': status.open.onAccMR, 'row': !status.open.onAccMR}"><b>On Accounting</b></div> -->
<!-- 												</accordion-heading> -->
<!-- 													<h6 class="collection"><a href="#/operator/newOnAccMR" class="collapsible-header collection-item level2data"><div class="l5">New MR</div></a></h6> -->
<!-- 													<h6 class="collection"><a href="#/operator/payDetMR" class="collapsible-header collection-item level2data"><div class="l5">Payment Detail</div></a></h6> -->
<!-- 													<h6 class="collection"><a href="#/operator/reverseMR" class="collapsible-header collection-item level2data"><div class="l5">Reverse MR</div></a></h6> -->
<!-- 											</accordion-group> -->
<!-- 											<accordion-group is-open="status.open.dirPayMR"> -->
<!-- 												<accordion-heading> -->
<!-- 													<div class="level1 l4" ng-class="{'greyborder': status.open.dirPayMR, 'row': !status.open.lhpvBal}"><b>Direct Payment</b></div> -->
<!-- 												</accordion-heading> -->
<!-- 													<h6 class="collection"><a href="#/operator/dirPayMR" class="collapsible-header collection-item level2data"><div class="l5">New DPMR</div></a></h6> -->
<!-- 											</accordion-group>	 -->
											
<!-- 											<div ng-show="userRights.urCancelMr">	 -->
<!-- 												<accordion-group is-open="status.open.cancelMR"> -->
<!-- 												<accordion-heading> -->
<!-- 													<div class="level1 l4" ng-class="{'greyborder': status.open.cancelMR, 'row': !status.open.cancelMR}"><b>Cancel MR</b></div> -->
<!-- 												</accordion-heading> -->
<!-- 													<h6 class="collection"><a href="#/operator/cancelMR" class="collapsible-header collection-item level2data"><div class="l5">Cancel</div></a></h6> -->
<!-- 											</accordion-group> -->
<!-- 											</div> -->
<!-- 										</accordion> -->
<!-- 							</accordion-group>	 -->
									
										<accordion-group is-open="status.open.moneyReceiptN">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.moneyReceipt, 'row': !status.open.moneyReceipt}"><b>New Money Receipt</b></div>
									</accordion-heading>
										<accordion  style="margin: 0px 0px;" close-others="oneAtATime">
											<accordion-group is-open="status.open.onAccMRN">
												<accordion-heading>
													 <div class="level1 l4" ng-class="{'greyborder': status.open.onAccMRN, 'row': !status.open.onAccMRN}"><b>New On Accounting</b></div>
												</accordion-heading>
													<h6 class="collection"><a href="#/operator/newOnAccMRN" class="collapsible-header collection-item level2data"><div class="l5">New MR</div></a></h6>
													<h6 class="collection"><a href="#/operator/payDetMRN" class="collapsible-header collection-item level2data"><div class="l5">Payment Detail</div></a></h6>
													<h6 class="collection"><a href="#/operator/reverseMRN" class="collapsible-header collection-item level2data"><div class="l5">Reverse MR</div></a></h6>
											</accordion-group>
											<accordion-group is-open="status.open.dirPayMRN">
												<accordion-heading>
													<div class="level1 l4" ng-class="{'greyborder': status.open.dirPayMRN, 'row': !status.open.dirPayMRN}"><b>New Direct Payment</b></div>
												</accordion-heading>
													<h6 class="collection"><a href="#/operator/dirPayMRN" class="collapsible-header collection-item level2data"><div class="l5">New DPMR</div></a></h6>
											</accordion-group>	
											
											<div ng-show="userRights.urCancelMr">	
												<accordion-group is-open="status.open.cancelMRN">
												<accordion-heading>
													<div class="level1 l4" ng-class="{'greyborder': status.open.cancelMRN, 'row': !status.open.cancelMRN}"><b>New Cancel MR</b></div>
												</accordion-heading>
													<h6 class="collection"><a href="#/operator/cancelMRN" class="collapsible-header collection-item level2data"><div class="l5">Cancel</div></a></h6>
											</accordion-group>
											</div>
										</accordion>
							</accordion-group>	
								
									
									
								<accordion-group is-open="status.open.closeVoucher">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.closeVoucher, 'row': !status.open.closeVoucher}"><b>Close Voucher</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/closeVoucher" class="collapsible-header collection-item level2data"><div class="l5">Closing</div></a></h6>
								</accordion-group>
								
								<accordion-group is-open="status.open.miscellaneous">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.miscellaneous, 'row': !status.open.miscellaneous}"><b>Miscellaneous</b></div>
									</accordion-heading>
										<h6 class="collection"><a href="#/operator/cancelChq" class="collapsible-header collection-item level2data"><div class="l5">Cancel (Single)</div></a></h6>
										<h6 class="collection"><a href="#/operator/cashStmt" class="collapsible-header collection-item level2data"><div class="l5">Cash Statement</div></a></h6>
										<h6 class="collection"><a href="#/operator/lhpvPrint" class="collapsible-header collection-item level2data"><div class="l5">Lorry Hire Payment</div></a></h6>
										<div ng-show="userRights.urEditCS">
											<h6 class="collection"><a href="#/operator/editCashStmt" class="collapsible-header collection-item level2data"><div class="l5">Edit Cash Statement</div></a></h6>
										</div>
										<h6 class="collection"><a href="#/operator/mrPrint" class="collapsible-header collection-item level2data"><div class="l5">MR Print</div></a></h6>
										<h6 class="collection"><a href="#/operator/mrrPrint" class="collapsible-header collection-item level2data"><div class="l5">MRR Print</div></a></h6>
								</accordion-group>	 
							</accordion>
						
					</accordion-group>
				</div>
				
<!-- 				fund allocation -->
					<accordion-group is-open="status.open.fundAllocation">
									<accordion-heading>
										<div class="level1 l4" ng-class="{'greyborder': status.open.fundTrans, 'row': !status.open.fundTrans}"><b>Fund Allocation</b></div>
									</accordion-heading>
											<h6 class="collection"><a href="#/operator/fundAllocation" class="collapsible-header collection-item level2data"><div class="l5">Advance</div></a></h6>
											<h6 class="collection"><a href="#/operator/lhpvBalPay" class="collapsible-header collection-item level2data"><div class="l5">Balance</div></a></h6>
											<h6 class="collection" ng-show="userRights.urFundAllocation"><a href="#/operator/generatedFundAllocation" class="collapsible-header collection-item level2data"><div class="l5">Update/Genrate </div></a></h6>
					</accordion-group>
				
					<!-- 				Petro Card -->					
					<accordion-group is-open="status.open.petro">
									<accordion-heading>
										<div class="level1 l4" ng-class="{'greyborder': status.open.fundTrans, 'row': !status.open.fundTrans}"><b>PetroCard</b></div>
									</accordion-heading>
											<h6 class="collection"><a href="#/operator/petroCard" class="collapsible-header collection-item level2data"><div class="l5">Details</div></a></h6>
											<h6 class="collection"><a href="#/operator/petroCardExcel" class="collapsible-header collection-item level2data"><div class="l5">Update/Genrate </div></a></h6>
					</accordion-group>
					
					
						<!-- 				fund allocation Allow -->
						<div ng-show="userRights.urLhpvAlw">
					<accordion-group is-open="status.open.alwLhpv">
							<accordion-heading>
										<div class="level1 l4" ng-class="{'greyborder': status.open.alwLhpv, 'row': !status.open.alwLhpv}"><b>Allow</b></div>
							</accordion-heading>
									<h6 class="collection"><a href="#/operator/alwLryPymnt" class="collapsible-header collection-item level2data"><div class="l5">Allow LHPV</div></a></h6>
					</accordion-group>
					</div>
				<!-- Bank -->
							
					<accordion-group is-open="status.open.bank">
						<accordion-heading>
							<div class="level1 l2" ng-class="{'greyborder': status.open.bank, 'row': !status.open.bank}"><b>Banking</b></div>
						</accordion-heading>	
						
						<accordion  style="margin: 0px 0px;" close-others="oneAtATime">
						<div ng-show="userRights.urFT">
										<accordion-group is-open="status.open.fundTrans">
											<accordion-heading>
												<div class="level1 l4" ng-class="{'greyborder': status.open.fundTrans, 'row': !status.open.fundTrans}"><b>Fund Transfer</b></div>
											</accordion-heading>
												<h6 class="collection"><a href="#/operator/bTBFundTrans" class="collapsible-header collection-item level2data"><div class="l5">Branch To Branch</div></a></h6>
												<h6 class="collection"  ng-show="userRights.urFundAllocation"><a href="#/operator/fileUpload" class="collapsible-header collection-item level2data"><div class="l5">File Upload/Download </div></a></h6>
										</accordion-group>
						 </div>					
					<!--     </accordion>
					   
					  
							<accordion  style="margin: 0px 0px;" close-others="oneAtATime"> -->
							  <div ng-show="userRights.urBank">			
								<accordion-group is-open="status.open.bankingBank">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.bankingBank, 'row': !status.open.bankingBank}"><b>Bank</b></div>
									</accordion-heading>
									
									<h6 class="collection"><a href="#/operator/newBank" class="collapsible-header collection-item level2data"><div class="l5">New</div></a></h6>
									<h6 class="collection"><a href="#/operator/viewBank" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>
									<h6 class="collection"><a href="#/operator/assignBank" class="collapsible-header collection-item level2data"><div class="l5">Assign/Dissociate</div></a></h6>	
									
								
								</accordion-group>	 
								</div>
								
								<div ng-show="userRights.urAtm">		
									<accordion-group is-open="status.open.bankingAtm">
										<accordion-heading>
											<div class="level1 l3" ng-class="{'greyborder': status.open.bankingAtm, 'row': !status.open.bankingAtm}"><b>Atm</b></div>
										</accordion-heading>
										
										<h6 class="collection"><a href="#/operator/newAtmCard" class="collapsible-header collection-item level2data"><div class="l5">New</div></a></h6>
										<h6 class="collection"><a href="#/operator/viewAtmCard" class="collapsible-header collection-item level2data"><div class="l5">View/Transfer</div></a></h6>
										
									</accordion-group>
								</div>
									
								<div ng-show="userRights.urChq">		
								<accordion-group is-open="status.open.bankingCheque">
									<accordion-heading>
										<div class="level1 l3" ng-class="{'greyborder': status.open.bankingCheque, 'row': !status.open.bankingCheque}"><b>Cheque</b></div>
									</accordion-heading>
										<accordion  style="margin: 0px 0px;" close-others="oneAtATime">
											<accordion-group is-open="status.open.bankChequeHo">
												<accordion-heading>
													<div class="level1 l4" ng-class="{'greyborder': status.open.bankChequeHo, 'row': !status.open.bankChequeHo}"><b>HO</b></div>
												</accordion-heading>
													<h6 class="collection"><a href="#/operator/hoChqRequest" class="collapsible-header collection-item level2data"><div class="l5">Request</div></a></h6>
													<h6 class="collection"><a href="#/operator/hoChqReceive" class="collapsible-header collection-item level2data"><div class="l5">Receive</div></a></h6>
													<h6 class="collection"><a href="#/operator/hoChqIssue" class="collapsible-header collection-item level2data"><div class="l5">Issue</div></a></h6>
													<h6 class="collection"><a href="#/operator/hoChqStatus" class="collapsible-header collection-item level2data"><div class="l5">Status</div></a></h6>
											</accordion-group>
											<accordion-group is-open="status.open.bankChequeBranch">
												<accordion-heading>
													<div class="level1 l4" ng-class="{'greyborder': status.open.bankChequeBranch, 'row': !status.open.bankChequeBranch}"><b>branch</b></div>
												</accordion-heading>
													<!-- <h6 class="collection"><a href="#/operator/brChqRequest" class="collapsible-header collection-item level2data"><div class="l5">Request</div></a></h6> -->
													<h6 class="collection"><a href="#/operator/brChqReceive" class="collapsible-header collection-item level2data"><div class="l5">Receive</div></a></h6>
													<h6 class="collection"><a href="#/operator/brChqStatus" class="collapsible-header collection-item level2data"><div class="l5">Status</div></a></h6>
											</accordion-group>	 
										</accordion>
										
								</accordion-group>	 
							</div>	
							</accordion>
					
					</accordion-group>	 
				
				
				<!-- Telephone -->
				<div ng-show="userRights.urTel">			
					<accordion-group is-open="status.open.telephone">
						<accordion-heading>
							<div class="level1 l2" ng-class="{'greyborder': status.open.telephone, 'row': !status.open.telephone}"><b>Telephone</b></div>
						</accordion-heading>
							<h6 class="collection"><a href="#/operator/newPhoneAllot" class="collapsible-header collection-item level2data"><div class="l5">Phone Allotment</div></a></h6>
							<h6 class="collection"><a href="#/operator/viewPhoneAllot" class="collapsible-header collection-item level2data"><div class="l5">View Phone Allotment</div></a></h6>
					</accordion-group>
				</div>
				<!-- Electricity -->
				<div ng-show="userRights.urEle">			
					<accordion-group is-open="status.open.electricity">
						<accordion-heading>
							<div class="level1 l2" ng-class="{'greyborder': status.open.electricity, 'row': !status.open.electricity}"><b>Electricity</b></div>
						</accordion-heading>
							<h6 class="collection"><a href="#/operator/newCRNAllot" class="collapsible-header collection-item level2data"><div class="l5">CRN Allotment</div></a></h6>
							<h6 class="collection"><a href="#/operator/viewCRNAllot" class="collapsible-header collection-item level2data"><div class="l5">View CRN Allotment</div></a></h6>
					</accordion-group>
				</div>	
				
				<!-- Vehicle Mstr -->
				<div ng-show="userRights.urVeh">			
					<accordion-group is-open="status.open.vehicleMstr">
						<accordion-heading>
							<div class="level1 l2" ng-class="{'greyborder': status.open.vehicleMstr, 'row': !status.open.vehicleMstr}"><b>Vehicle</b></div>
						</accordion-heading>
							<h6 class="collection"><a href="#/operator/newVehAllot" class="collapsible-header collection-item level2data"><div class="l5">Vehicle Allotment</div></a></h6>
							<h6 class="collection"><a href="#/operator/viewVehAllot" class="collapsible-header collection-item level2data"><div class="l5">View Vehicle Allotment</div></a></h6>
					</accordion-group>
				</div>
				
				<!-- Rent Mstr -->
				<div ng-show="userRights.urRent">
				<accordion-group is-open="status.open.rentMstr">
					<accordion-heading>
						<div class="level1 l2" ng-class="{'greyborder': status.open.rentMstr, 'row': !status.open.rentMstr}"><b>Rent</b></div>
					</accordion-heading>
						<h6 class="collection"><a href="#/operator/newRent" class="collapsible-header collection-item level2data"><div class="l5">New Rent</div></a></h6>
						<h6 class="collection"><a href="#/operator/viewRent" class="collapsible-header collection-item level2data"><div class="l5">View Rent</div></a></h6>
				</accordion-group>
				</div>
				
				<!-- Rent Mstr -->
				<div ng-show="userRights.urRent">
				<accordion-group is-open="status.open.rentMstr">
					<accordion-heading>
						<div class="level1 l2" ng-class="{'greyborder': status.open.rentMstr, 'row': !status.open.rentMstr}"><b>Rent</b></div>
					</accordion-heading>
						<h6 class="collection"><a href="#/operator/newRent" class="collapsible-header collection-item level2data"><div class="l5">New Rent</div></a></h6>
						<h6 class="collection"><a href="#/operator/viewRent" class="collapsible-header collection-item level2data"><div class="l5">View Rent</div></a></h6>
				</accordion-group>
				</div>	
					
    	</accordion-group>
    	
</accordion>


 <accordion  style="margin: 0px 0px;" close-others="oneAtATime"> 
     <accordion-group>
	   		<accordion-heading>
				<div class="level1 l1"><b>App</b></div>
	        </accordion-heading>    
	        <h6 class="collection"><a href="#/operator/checkCNMTApp" class="collapsible-header collection-item level2data">Verify CNMT</a></h6>  
	        <h6 class="collection"><a href="#/operator/checkChallanApp" class="collapsible-header collection-item level2data">Verify Challan</a></h6>
	        <h6 class="collection"><a href="#/operator/vryOwner" class="collapsible-header collection-item level2data">Verify Owner</a></h6>	        
	        <h6 class="collection"><a href="#/operator/vryBroker" class="collapsible-header collection-item level2data">Verify Broker</a></h6>
	        <h6 class="collection"><a href="#/operator/vryVehicle" class="collapsible-header collection-item level2data">Verify Vehicle</a></h6>
	        <h6 class="collection"><a href="#/operator/trafficOff" class="collapsible-header collection-item level2data">Traffic Off.</a></h6>	        
   		</accordion-group>
   	</accordion>
   	
 
   	
   	<accordion  style="margin: 0px 0px;" close-others="oneAtATime"> 
     <accordion-group>
	   		<accordion-heading>
				<div class="level1 l1"><b>All Images</b></div>
	        </accordion-heading>    
	        <h6 class="collection"><a href="#/operator/allImg" class="collapsible-header collection-item level2data">Images</a></h6>	        
	        <h6 class="collection"><a href="#/operator/imgReader" class="collapsible-header collection-item level2data">Images Reader</a></h6>
	        <!-- 
	        	<h6 class="collection"><a href="#/operator/brkImg" class="collapsible-header collection-item level2data">Broker IMG</a></h6>
	        -->	        
   		</accordion-group>
   	</accordion>
   
   
    <!-------Financial back start	-->
    	
<!--     	<accordion  style="margin: 0px 0px;" close-others="oneAtATime">  -->

<!-- 		<accordion-group is-open="status.open.financial"> -->
<!-- 			<accordion-heading> -->
<!-- 				<div class="level1 l1" ng-class="{'greyborder': status.open.financial, 'row': !status.open.financial}"><b>Financial Back</b></div> -->
<!-- 			</accordion-heading> -->
<!-- 							Voucher -->
<!-- 				<accordion  style="margin: 0px 0px;" close-others="oneAtATime"> -->
<!-- 				<div ng-show="userRights.urVouch">			 -->
<!-- 					<accordion-group is-open="status.open.voucher"> -->
<!-- 						<accordion-heading> -->
<!-- 							<div class="level1 l2" ng-class="{'greyborder': status.open.voucher, 'row': !status.open.voucher}"><b>Voucher</b></div> -->
<!-- 						</accordion-heading> -->
<!-- 							<accordion  style="margin: 0px 0px;" close-others="oneAtATime"> -->
							
<!-- 								<accordion-group is-open="status.open.cashWithdraw"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.cashWithdraw, 'row': !status.open.cashWithdraw}"><b>Cash Withdrawl (CHQ)</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<h6 class="collection"><a href="#/operator/newVoucherBack" class="collapsible-header collection-item level2data"><div class="l5">New Back</div></a></h6> -->
<!-- 										<h6 class="collection"><a href="#/operator/viewVoucher" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6> -->
<!-- 								</accordion-group> -->
								
<!-- 								<accordion-group is-open="status.open.atmVoucher"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.atmVoucher, 'row': !status.open.atmVoucher}"><b>Cash Withdrawl (ATM)</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<h6 class="collection"><a href="#/operator/newAtmVoucherBack" class="collapsible-header collection-item level2data"><div class="l5">New Back</div></a></h6> -->
<!-- 										<h6 class="collection"><a href="#/operator/viewAtmVoucher" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6> -->
<!-- 								</accordion-group> -->
								
<!-- 								<accordion-group is-open="status.open.cashReciept"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.cashReciept, 'row': !status.open.cashReciept}"><b>Cash Reciept</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<h6 class="collection"><a href="#/operator/newCashRecieptBack" class="collapsible-header collection-item level2data"><div class="l5">New Back</div></a></h6> -->
<!-- 										<h6 class="collection"><a href="#/operator/viewCashReciept" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6> -->
<!-- 								</accordion-group> -->
									
<!-- 								<accordion-group is-open="status.open.cashDeposite"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.cashDeposite, 'row': !status.open.cashDeposite}"><b>Cash Deposite</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<h6 class="collection"><a href="#/operator/newCashDepositeBack" class="collapsible-header collection-item level2data"><div class="l5">New Back</div></a></h6> -->
<!-- 										<h6 class="collection"><a href="#/operator/viewCashDeposite" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6> -->
<!-- 								</accordion-group> -->
									 
<!-- 								<accordion-group is-open="status.open.cashPayment"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.cashPayment, 'row': !status.open.cashPayment}"><b>Cash Payment</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<h6 class="collection"><a href="#/operator/newCashPaymentBack" class="collapsible-header collection-item level2data"><div class="l5">New Back</div></a></h6> -->
<!-- 										<h6 class="collection"><a href="#/operator/viewCashPayment" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6> -->
<!-- 								</accordion-group>	 -->
								
<!-- 								<accordion-group is-open="status.open.chqPayVoucher"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.chqPayVoucher, 'row': !status.open.chqPayVoucher}"><b>Cheque Payment</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<h6 class="collection"><a href="#/operator/newChqPayVoucherBack" class="collapsible-header collection-item level2data">New Back</a></h6> -->
<!-- 										<h6 class="collection"><a href="#/operator/viewChqPayVoucher" class="collapsible-header collection-item level2data">View</a></h6> -->
<!-- 								</accordion-group> -->
								
<!-- 								<accordion-group is-open="status.open.chqCancelVoucher"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.chqCancelVoucher, 'row': !status.open.chqPayVoucher}"><b>Cheque Cancel</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<h6 class="collection"><a href="#/operator/newChqCancelVoucherBack" class="collapsible-header collection-item level2data">New Back</a></h6> -->
<!-- 										<h6 class="collection"><a href="#/operator/viewChqCancelVoucher" class="collapsible-header collection-item level2data">View</a></h6> -->
<!-- 								</accordion-group> -->
								
<!-- 								<accordion-group is-open="status.open.telephoneVoucher"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.telephoneVoucher, 'row': !status.open.telephoneVoucher}"><b>Telephone</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<h6 class="collection"><a href="#/operator/newTelephoneVoucherBack" class="collapsible-header collection-item level2data"><div class="l5">New Back</div></a></h6> -->
<!-- 										<h6 class="collection"><a href="#/operator/viewTelephoneVoucher" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6> -->
<!-- 								</accordion-group> -->
								
<!-- 								<accordion-group is-open="status.open.electricityVoucher"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.electricityVoucher, 'row': !status.open.electricityVoucher}"><b>Electricity</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<h6 class="collection"><a href="#/operator/newElectVoucherBack" class="collapsible-header collection-item level2data"><div class="l5">New Back</div></a></h6> -->
<!-- 										<h6 class="collection"><a href="#/operator/viewElectVoucher" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6> -->
<!-- 								</accordion-group> -->
								
<!-- 								<accordion-group is-open="status.open.vehicleVoucher"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.vehicleVoucher, 'row': !status.open.vehicleVoucher}"><b>Vehicle</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<h6 class="collection"><a href="#/operator/newVehVoucherBack" class="collapsible-header collection-item level2data"><div class="l5">New Back</div></a></h6> -->
<!-- 										<h6 class="collection"><a href="#/operator/viewVehVoucher" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6> -->
<!-- 								</accordion-group> -->
								
<!-- 								<accordion-group is-open="status.open.travelVoucher"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.travelVoucher, 'row': !status.open.travelVoucher}"><b>Travel</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<h6 class="collection"><a href="#/operator/newTravVoucherBack" class="collapsible-header collection-item level2data"><div class="l5">New Back</div></a></h6> -->
<!-- 										<h6 class="collection"><a href="#/operator/viewTravVoucher" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6> -->
<!-- 								</accordion-group> -->
								
<!-- 								<accordion-group is-open="status.open.rentVoucher"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.rentVoucher, 'row': !status.open.rentVoucher}"><b>Rent</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<h6 class="collection"><a href="#/operator/newRentVoucherBack" class="collapsible-header collection-item level2data"><div class="l5">New Back</div></a></h6> -->
<!-- 										<h6 class="collection"><a href="#/operator/viewRentVoucher" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6> -->
<!-- 								</accordion-group> -->
								
<!--   								<accordion-group is-open="status.open.lhpvVouch"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.lhpvVouch, 'row': !status.open.lhpvVouch}"><b>Lhpv</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<accordion  style="margin: 0px 0px;" close-others="oneAtATime"> -->
<!-- 											<accordion-group is-open="status.open.lhpvAdv"> -->
<!-- 												<accordion-heading> -->
<!-- 													 <div class="level1 l4" ng-class="{'greyborder': status.open.lhpvAdv, 'row': !status.open.lhpvAdv}"><b>Lhpv Advance</b></div> -->
<!-- 												</accordion-heading> -->
<!-- 													<h6 class="collection"><a href="#/operator/newLhpvAdvBack" class="collapsible-header collection-item level2data"><div class="l5">New Back</div></a></h6> -->
<!--  											  <h6 class="collection"><a href="#/operator/rvrsLhpvAdvBack" class="collapsible-header collection-item level2data"><div class="l5">Revers Back</div></a></h6> -->
<!-- 										</accordion-group> -->
<!-- 											<accordion-group is-open="status.open.lhpvBal"> -->
<!-- 												<accordion-heading> -->
<!-- 													<div class="level1 l4" ng-class="{'greyborder': status.open.lhpvBal, 'row': !status.open.lhpvBal}"><b>Lhpv Balance</b></div> -->
<!-- 												</accordion-heading> -->
<!-- 													<h6 class="collection"><a href="#/operator/newLhpvBalBack" class="collapsible-header collection-item level2data"><div class="l5">New Back</div></a></h6>											 -->
<!-- 											 <h6 class="collection"><a href="#/operator/rvrsLhpvBalBack" class="collapsible-header collection-item level2data"><div class="l5">Revers Back</div></a></h6> -->
<!-- 											 </accordion-group>	 -->
<!-- 											<accordion-group is-open="status.open.lhpvSup"> -->
<!-- 												<accordion-heading> -->
<!-- 													<div class="level1 l4" ng-class="{'greyborder': status.open.lhpvSup, 'row': !status.open.lhpvSup}"><b>Lhpv Supplementary</b></div> -->
<!-- 												</accordion-heading> -->
<!-- 													<h6 class="collection"><a href="#/operator/newLhpvSupBack" class="collapsible-header collection-item level2data"><div class="l5">New Back</div></a></h6> -->
<!-- 													<h6 class="collection"><a href="#/operator/viewLhpvSup" class="collapsible-header collection-item level2data"><div class="l5">View</div></a></h6>											        -->
<!-- 											</accordion-group>	  -->
<!-- 											<accordion-group is-open="status.open.lhpvClose"> -->
<!-- 												<accordion-heading> -->
<!-- 													<div class="level1 l4" ng-class="{'greyborder': status.open.lhpvClose, 'row': !status.open.lhpvClose}"><b>Close Lhpv</b></div> -->
<!-- 												</accordion-heading> -->
<!-- 													<h6 class="collection"><a href="#/operator/closeLhpvBack" class="collapsible-header collection-item level2data"><div class="l5">Closing Lhpv Back</div></a></h6> -->
<!-- 											</accordion-group> -->
<!-- 								</accordion> -->
<!-- 								</accordion-group> -->
								
<!-- 								<accordion-group is-open="status.open.bPromVoucher"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.bPromVoucher, 'row': !status.open.bPromVoucher}"><b>Business Promotion</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<h6 class="collection"><a href="#/operator/newBPVoucherBack" class="collapsible-header collection-item level2data">New Back</a></h6> -->
<!-- 										<h6 class="collection"><a href="#/operator/viewBPVoucher" class="collapsible-header collection-item level2data">View</a></h6> -->
<!-- 								</accordion-group> -->
								
								
								
<!-- 								<accordion-group is-open="status.open.intBrTVCrDr"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.intBrTVCrDr, 'row': !status.open.intBrTVCrDr}"><b>Inter Branch TV C/D</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<h6 class="collection"><a href="#/operator/newIntBrTVCrDrBack" class="collapsible-header collection-item level2data">New Back</a></h6> -->
<!-- 										<h6 class="collection"><a href="#/operator/clrPendingIBTVBack" class="collapsible-header collection-item level2data">Clear Pending IBTV Back</a></h6> -->
<!-- 										<h6 class="collection"><a href="#/operator/viewIntBrTVCrDrBack" class="collapsible-header collection-item level2data">View Back</a></h6> -->
<!-- 								</accordion-group> -->
								
								
<!-- 								<accordion-group is-open="status.open.revVoucher"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.revVoucher, 'row': !status.open.revVoucher}"><b>Voucher Reversal</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<h6 class="collection"><a href="#/operator/newRevVoucherBack" class="collapsible-header collection-item level2data">New Back</a></h6> -->
<!-- 										<h6 class="collection"><a href="#/operator/viewRevVoucher" class="collapsible-header collection-item level2data">View</a></h6> -->
<!-- 								</accordion-group> -->
							
							
								
<!-- 								<accordion-group is-open="status.open.moneyReceipt"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.moneyReceipt, 'row': !status.open.moneyReceipt}"><b>Money Receipt</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<accordion  style="margin: 0px 0px;" close-others="oneAtATime"> -->
<!-- 											<accordion-group is-open="status.open.onAccMR"> -->
<!-- 												<accordion-heading> -->
<!-- 													 <div class="level1 l4" ng-class="{'greyborder': status.open.onAccMR, 'row': !status.open.onAccMR}"><b>On Accounting</b></div> -->
<!-- 												</accordion-heading> -->
<!-- 													<h6 class="collection"><a href="#/operator/newOnAccMRBack" class="collapsible-header collection-item level2data"><div class="l5">New MR Back</div></a></h6> -->
<!-- 													<h6 class="collection"><a href="#/operator/payDetMRBack" class="collapsible-header collection-item level2data"><div class="l5">Payment Detail Back</div></a></h6> -->
<!-- 													<h6 class="collection"><a href="#/operator/reverseMRBack" class="collapsible-header collection-item level2data"><div class="l5">Reverse MR Back</div></a></h6> -->
<!-- 											</accordion-group> -->
<!-- 											<accordion-group is-open="status.open.dirPayMR"> -->
<!-- 												<accordion-heading> -->
<!-- 													<div class="level1 l4" ng-class="{'greyborder': status.open.dirPayMR, 'row': !status.open.lhpvBal}"><b>Direct Payment</b></div> -->
<!-- 												</accordion-heading> -->
<!-- 													<h6 class="collection"><a href="#/operator/dirPayMRBack" class="collapsible-header collection-item level2data"><div class="l5">New DPMR Back</div></a></h6> -->
<!-- 											</accordion-group>	 -->
											
<!-- 											<div ng-show="userRights.urCancelMr">	 -->
<!-- 												<accordion-group is-open="status.open.cancelMR"> -->
<!-- 												<accordion-heading> -->
<!-- 													<div class="level1 l4" ng-class="{'greyborder': status.open.cancelMR, 'row': !status.open.cancelMR}"><b>Cancel MR</b></div> -->
<!-- 												</accordion-heading> -->
<!-- 													<h6 class="collection"><a href="#/operator/cancelMRBack" class="collapsible-header collection-item level2data"><div class="l5">Cancel Back</div></a></h6> -->
<!-- 											</accordion-group> -->
<!-- 											</div> -->
<!-- 										</accordion> -->
<!-- 							</accordion-group>	 -->
									
<!-- 								<accordion-group is-open="status.open.closeVoucher"> -->
<!-- 									<accordion-heading> -->
<!-- 										<div class="level1 l3" ng-class="{'greyborder': status.open.closeVoucher, 'row': !status.open.closeVoucher}"><b>Close Voucher</b></div> -->
<!-- 									</accordion-heading> -->
<!-- 										<h6 class="collection"><a href="#/operator/closeVoucherBack" class="collapsible-header collection-item level2data"><div class="l5">Closing Back</div></a></h6> -->
<!-- 								</accordion-group> -->
								
								
<!-- 					  </accordion> -->
						
<!-- 					</accordion-group> -->
<!-- 				</div> -->
				
					
				
				
					
<!--     	</accordion-group> -->
  
<!-- </accordion> -->
<!--  <!-------Financial back end	--> 
</div>


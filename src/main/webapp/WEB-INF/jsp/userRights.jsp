<div ng-show="operatorLogin || superAdminLogin">

<div class="row">
		<form name="userRightsForm" ng-submit=submitUR(userRightsForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="userId" name ="userN" ng-model="userName"  ng-click="openUserDB()" readonly ng-required="true" >
		       			<label for="code">User Code</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		    
		    	<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urBranch" ng-init="ur.urBranch=false"/>	
		   			<label>Branch</label>
		   		</div>
		   		
		   		<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urEmployee" ng-init="ur.urEmployee=false"/>	
		   			<label>Employee</label>
		   		</div>		
		   		
		   		<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urAssigned" ng-init="ur.urAssigned=false"/>	
		   			<label>Assignment</label>
		   		</div>		
		   			
		   		<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urCustomer" ng-init="ur.urCustomer=false"/>	
		   			<label>Customer</label>
		   		</div>
		   		
		   		<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urContract" ng-init="ur.urContract=false"/>	
		   			<label>Regular Contract</label>
		   		</div>
		   		
		   		<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urDlyContract" ng-init="ur.urDlyContract=false"/>	
		   			<label>Daily Contract</label>
		   		</div>	
		   				
		    </div>
		    
		    <div class="row">
		    
		    	<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urCnmt" ng-init="ur.urCnmt=false"/>	
		   			<label>Cnmt</label>
		   		</div>		
		   		
		   		<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urChallan" ng-init="ur.urChallan=false"/>	
		   			<label>Challan</label>
		   		</div>
		   		
		    	<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urSedr" ng-init="ur.urSedr=false"/>	
		   			<label>Arrival Report</label>
		   		</div>	
		   		
		    	<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urStnSup" ng-init="ur.urStnSup=false"/>	
		   			<label>Stationary Supplier</label>
		   		</div>
		   		
		   		<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urOwner" ng-init="ur.urOwner=false"/>	
		   			<label>Owner</label>
		   		</div>		
		   		
		   		<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urBroker" ng-init="ur.urBroker=false"/>	
		   			<label>Broker</label>
		   		</div>		
		   					
		    </div>
		    
		    
		     <div class="row">
		    	
		    	<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urBill" ng-init="ur.urBill=false"/>	
		   			<label>Bill</label>
		   		</div>	
		   		
		    	<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urMisc" ng-init="ur.urMisc=false"/>	
		   			<label>Miscellaneous</label>
		   		</div>
		   		
		   		<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urVouch" ng-init="ur.urVouch=false"/>	
		   			<label>Voucher</label>
		   		</div>
		   		
		   		
		   		<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urBank" ng-init="ur.urBank=false"/>	
		   			<label>Bank</label>
		   		</div>
		   		
		   		<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urFT" ng-init="ur.urFT=false"/>	
		   			<label>Fund Transfer</label>
		   		</div>
		   		
   		   		<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urChq" ng-init="ur.urChq=false"/>	
		   			<label>Cheque</label>
		   		</div>
		   		
		   </div>
	   		 
   		   <div class="row">
	   		
	   			<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urTel" ng-init="ur.urTel=false"/>	
		   			<label>Telephone</label>
		   		</div>
		   		
   		 		<div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urEle" ng-init="ur.urEle=false"/>	
		   			<label>Electricity</label>
		   		</div>
	   			
		   		 <div class="col s2 input-field">
			   		  <input type="checkbox" ng-model="ur.urVeh" ng-init="ur.urVeh=false"/>	
			   			<label>Vehicle</label>
			   	  </div>
			   	  
			   	  <div class="col s2 input-field">
			   		  <input type="checkbox" ng-model="ur.urAtm" ng-init="ur.urAtm=false"/>	
			   			<label>ATM</label>
			   	  </div>
			   	  
			   	  <div class="col s2 input-field">
			   		  <input type="checkbox" ng-model="ur.urRent" ng-init="ur.urRent=false"/>	
			   			<label>Rent</label>
			   	  </div>
			   	  
			   	  <div class="col s2 input-field">
			   		  <input type="checkbox" ng-model="ur.urVehVen" ng-init="ur.urVehVen=false"/>	
			   			<label>VehVendor</label>
			   	  </div>
			 </div>
			 
			<div class="row">
		   		 <div class="col s2 input-field">
			   		  <input type="checkbox" ng-model="ur.urPanVal" ng-init="ur.urPanVal=false"/>	
			   			<label>Pan Validation</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urEditCnmt" ng-init="ur.urEditCnmt=false" />
			   	 	<label>Edit Cnmt</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urEditChln" ng-init="ur.urEditChln=false" />
			   	 	<label>Edit Chln</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urEnquiry" ng-init="ur.urEnquiry=false" />
			   	 	<label>Enquiry</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urReports" ng-init="ur.urReports=false" />
			   	 	<label>Reports</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urLedger" ng-init="ur.urLedger=false" />
			   	 	<label>Ledger</label>
			   	 </div>
			 </div>
			 
			 <div class="row">
		   		 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urTrial" ng-init="ur.urTrial=false" />
			   	 	<label>Trial</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urCustStationary" ng-init="ur.urCustStationary=false" />
			   	 	<label>Cust Stationary</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urEditCS" ng-init="ur.urEditCS=false" />
			   	 	<label>Edit CS</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urMR" ng-init="ur.urMR=false" />
			   	 	<label>MR</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urCancelMr" ng-init="ur.urCancelMr=false" />
			   	 	<label>Cancel MR</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urBackLhpv" ng-init="ur.urBackLhpv=false" />
			   	 	<label>Back Date LHPV</label>
			   	 </div>
			   	 
			 </div>  	   

			<div class="row">
		   		 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urCnmtBbl" ng-init="ur.urCnmtBbl=false" />
			   	 	<label>CnmtBarbil</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urSedrAlw" ng-init="ur.urSedrAlw=false" />
			   	 	<label>ArRptAlw</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
		   		  <input type="checkbox" ng-model="ur.urRvrsLhpvAdv" ng-init="ur.urRvrsLhpvAdv=false"/>	
		   			<label>RvrsLhpvAdv</label>
		   		</div>
			   	 
			   	  <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urCancelChln" ng-init="ur.urCancelChln=false" />
			   	 	<label>Cancel Challan</label>
			   	 </div>
			   	 
			   	   <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urCancelCnmt" ng-init="ur.urCancelCnmt=false" />
			   	 	<label>Cancel CNMT</label>
			   	 </div>
			   	 
			   	   <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urCancelSedr" ng-init="ur.urCancelSedr=false" />
			   	 	<label>Cancel SEDR</label>
			   	 </div>
			 </div>		
			 
			 <div class="row">
			 
			 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urRvrsLhpvBal" ng-init="ur.urRvrsLhpvBal=false" />
			   	 	<label>RvrsLhpvBal</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urRelBl" ng-init="ur.urRelBl=false" />
			   	 	<label>Reliance Bill Print</label>
			   	 </div>
			 
			 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.alwSrtgDmgAr" ng-init="ur.alwSrtgDmgAr=false" />
			   	 	<label>SedrSrtgDmgAlw</label>
			   	 </div>
			 
			  <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urBillCancel" ng-init="ur.urBillCancel=false" />
			   	 	<label>Cancel Bill</label>
			   	 </div>
			   	 
			 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urEditBill" ng-init="ur.urEditBill=false" />
			   	 	<label>Bill Edit</label>
			   	 </div>
			   	 
			   	  <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urCnList" ng-init="ur.urCnList=false" />
			   	 	<label>CnmtList</label>
			   	 </div>
			 
			 </div>	 
			 
			  <div class="row">
			 
			 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urManualBill" ng-init="ur.urManualBill=false" />
			   	 	<label>Manual Bill</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.reIssue" ng-init="ur.reIssue=false" />
			   	 	<label>ReIssue Stationary</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.allowAr" ng-init="ur.allowAr=false" />
			   	 	<label>Ar Allow Optn</label>
			   	 </div>
			   	 
			   	  <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urHoLhpv" ng-init="ur.urHoLhpv=false" />
			   	 	<label>HO LhpvBal Pay</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urOldLhpvBal" ng-init="ur.urOldLhpvBal=false" />
			   	 	<label>Old Lhpv Bal Option</label>
			   	 </div>
			   	 
			   	  <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urAckValidation" ng-init="ur.urAckValidation=false" />
			   	 	<label>Ack Validation</label>
			   	 </div>
			</div>
			<div class="row">
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urGenrateLbExcel" ng-init="ur.urGenrateLbExcel=false" />
			   	 	<label>LHPV BAL EXCEL</label>
			   	 </div>
			   	
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urGenrateRelExcel" ng-init="ur.urGenrateRelExcel=false" />
			   	 	<label>Reliance EXCEL Upload</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urFundAllocation" ng-init="ur.urFundAllocation=false" />
			   	 	<label>Fund Allocation</label>
			   	 </div>
			   	 
			   	  <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urCosting" ng-init="ur.urCosting=false" />
			   	 	<label>Costing</label>
			   	 </div>
			   	 
			   	  <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urPetroExcel" ng-init="ur.urPetroExcel=false" />
			   	 	<label>Petro Card Excel</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urRentExcel" ng-init="ur.urRentExcel=false" />
			   	 	<label>Rent Excel</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urShrtExcessUpdate" ng-init="ur.urShrtExcessUpdate=false" />
			   	 	<label>Edit Short Excess Report</label>
			   	 </div>
			   	 
			   	  <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urBillPrint" ng-init="ur.urBillPrint=false" />
			   	 	<label>Print</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urLhpvBalCash" ng-init="ur.urLhpvBalCash=false" />
			   	 	<label>LHPV Balance Cash</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urMisngStnry" ng-init="ur.urMisngStnry=false" />
			   	 	<label>Missing Stationary</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urAccValid" ng-init="ur.urAccValid=false" />
			   	 	<label>Edit/Validate A/C details</label>
			   	 </div>
			   	 
			   	 <div class="col s2 input-field">
			   	 	<input type="checkbox" ng-model="ur.urLhpvAlw" ng-init="ur.urLhpvAlw=false" />
			   	 	<label>Allow LHPV</label>
			   	 </div>
			   	 
			 </div>
			 
			 	   	  
   		   <div class="row">
     		 <div class="col s12 center">
       				<input class="validate" type ="submit" value="submit" >
       		 </div>
       		
   		   </div>
   		   
	     </form>
</div>

 <div id="userDB" ng-hide="userDBFlag">
    	<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search....">
	 	  <table>
	 	  	  <tr>
	 	  	  	  <th></th>
	 	  	  	  <th> UserName</th>
	 	  	  </tr>
	 	  
			  <tr ng-repeat="user in userList | filter:filterTextbox">
			 	  <td><input type="radio"  name="user"   value="{{ user }}" ng-model="userCode" ng-click="saveUser(user)"></td>
	              <td>{{user.userName}}</td>
	          </tr>
	      </table> 
    </div>




</div>
<form name ="createDispatchOrderForm" ng-submit="saveBranchStockDispatch(createDispatchOrderForm,brsd,modelCnmt,modelChln,modelSedr)">
<table >
		<tr>
		<td>ID</td>
		<td>{{HBON.hbonId}}</td>
		</tr>
	
		<tr>
			<td>Branch Code</td>
			<td><input type ="text" name="brsDisBranchCode" ng-model="brsd.brsDisBranchCode" required readonly></td>
		</tr>
		
		<tr>
			<td>No of Cnmt</td>
			<td><input type ="text" name="brsDisCnmt" ng-model="brsd.brsDisCnmt" readonly></td>
		</tr>
		
		<tr ng-show="brsd.brsDisCnmt > 0">
		
		<td>
			<div ng-repeat="cnmt in cnmtTextboxList">
				<input type="text" ng-model="modelCnmt[cnmt]"  ng-blur="checkCnmtNo(modelCnmt)" />
			</div>
		</td>
		<!-- <td><input type="button" value="Save CNMT" ng-click="saveNoCnmt(modelCnmt,brsd)"></td> -->
		</tr>
		
		<tr >
			<td>No of Chln</td>
			<td><input type ="text" name="brsDisChln" ng-model="brsd.brsDisChln"   readonly></td>
		</tr>
		
		<tr ng-show="brsd.brsDisChln > 0">
		<td>
		<div ng-repeat="chln in chlnTextboxList">
				<input type="text" ng-model="modelChln[chln]" ng-blur="checkChlnNo(modelChln)"/>
			</div>
			</td>
		<!-- <td><input type="button" value="Save Chln" ng-click="saveNoChln(modelChln,brsd)"></td> -->
		</tr>
		
		<tr>
			<td>No of Sedr</td>
			<td><input type ="text" name="brsDisSedr" ng-model="brsd.brsDisSedr"   readonly></td>
		</tr>
		
		<tr ng-show="brsd.brsDisSedr > 0">
		<td>
		<div ng-repeat="sedr in sedrTextboxList">
				<input type="text" ng-model="modelSedr[sedr]" ng-blur="checkSedrNo(modelSedr)"/>
			</div>
			</td>
		<!-- <td><input type="button" value="Save Sedr" ng-click="saveNoSedr(modelSedr,brsd)"></td> -->
		</tr>
		
		<tr>
			<td>Date</td>
			<td><input type ="date" name="brsDisDt" ng-model="brsd.brsDisDt" required></td>
		</tr>
		
	
		<tr>
			<td><input type="submit" id="submitBSD" value="Submit"></td>
		</tr>
</table>
</form>
<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="dlyContAuthForm" ng-submit=submit(dlyContAuthForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		     		<input class="validate" type ="text" id="dcaCustCodeTempId" name ="dcaCustCodeTempName" ng-model="dcaCustCodeTemp"  ng-click="openCustCodeDB()" readonly ng-required="true" >
		       				<input class="validate" type ="hidden" id="dcaCustCodeId" name ="dcaCustCodeName" ng-model="dca.dcaCustCode" ng-required="true" >
		       			<label for="code">Customer Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="datepicker" type ="date" id="dcaFDtId" name ="dcaFDtName" ng-model="dca.dcaFDt" ng-required="true">
		       			<label for="code">From Date</label>
		       		</div>		
		       		
		       			<div class="col s4 input-field">
		       				<input class="datepicker" type ="date" id="dcaTDtId" name ="dcaTDtName" ng-model="dca.dcaTDt" ng-required="true">
		       			<label for="code">To Date</label>	
		       		</div>
		    </div>
		    
		
		<div class="row">
			<div class="col s1">
				<input type="radio" name="btn" ng-click="selectSt()" />
			</div>
			<div class="input-field col s5">
				<input class="col s12 btn teal white-text" type="button" id="addStId" value="Add Station" ng-click="OpenStList()" disabled="disabled" readonly> 
			</div>
			
			<div class="col s1">
				<input type="radio" name="btn" ng-click="allSt()" />
			</div>
			<div class="input-field col s5">
				<input class="col s12 btn teal white-text" type="button"  id="allStId" value="All Station" ng-click="allowAllSt()" disabled="disabled" readonly> 
			</div>
	
		</div>
		
		<div class="row">
			<div class="col s4 input-field">
	    		<select name="isActiveName" id="isActiveId" ng-model="dca.dcaIsActive" ng-change="setIsActive()" ng-required="true">
					<option value = "YES">YES</option>
					<option value = "NO">NO</option>
				</select>
				<label>Active</label>
			</div>
		</div>
		
		<div ng-if=" finalStList.length > 0">
			<table>
	 	  	  <tr>
	 	  	  	  <th> Station Name</th>
	 	  	 	  <th> Station Code</th>
	 	  	  </tr>
	 	  
			  <tr ng-repeat="finalSt in finalStList">
	              <td>{{finalSt.stnName}}</td>
	              <td>{{finalSt.stnCode}}</td>
	              <td><input class="col s12 btn teal white-text" type="button" value="Remove" ng-click="removeSt($index)"></td>
	          </tr>
	      </table> 
		</div>
		
		<div class="row">
			<div class="col s12 center">
				<input class="btn" type="submit" value="Submit">
			</div>
		</div>
 </form>
</div>

<div id="custCodeDB" ng-hide="custCodeDBFlag">
	 <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Customer Name </th>
 	  	 	  <th> Customer FaCode </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cust in custList | filter:filterTextbox ">
		 	  <td><input type="radio"  name="custName"   class="custCode" value="{{ cust }}"  ng-model="cust1" ng-click="saveCustCode(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
</div>


<div id="stCodeDB" ng-hide="stCodeDBFlag">
	 <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Station Name </th>
 	  	 	  <th> Station Code </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="st in stList | filter:filterTextbox ">
		 	  <td><input type="radio"  name="stName"  class="station"  value="{{ st }}"  ng-model="st1" ng-click="saveStCode(st)"></td>
              <td>{{st.stnName}}</td>
              <td>{{st.stnCode}}</td>
          </tr>
      </table> 
</div>
    
<div id="saveDcaDB" ng-hide="saveDCAFlag"> 

	<table class="table-bordered table-hover table-condensed">
				<tr>
					<td>Customer Code:</td>
					<td>{{dca.dcaCustCode}}</td>
				</tr>
				<tr>
					<td>From Date:</td>
					<td>{{dca.dcaFDt}}</td>
				</tr>
				<tr>
					<td>To Date:</td>
					<td>{{dca.dcaTDt}}</td>
				</tr>
				<tr>
					<td>Activation:</td>
					<td>{{dca.dcaIsActive}}</td>
				</tr>
				<tr>
					<td>Station List:</td>
					<td>
						<table>
							<tr  ng-repeat="fSt in finalStList">
								<td>{{fSt.stnName}}</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<input type="button" value="Save" ng-click="saveDCA()">
			<input type="button" value="Cancel" ng-click="closeDCA()">

</div>    
    
    
</div>

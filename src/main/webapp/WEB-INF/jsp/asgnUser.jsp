<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="assgnForm" ng-submit=assgnSubmit(assgnForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
	     		<div class="col s6 input-field">
	       				<input class="validate" type ="text" id="userId" name ="userName" ng-model="user.userName" ng-click="openUserModel()" readonly ng-required="true" >
	       			<label for="code">Select UserName</label>	
	       		</div>

	     		<div class="col s6 input-field">
	       				<input class="validate" type ="text" id="branchId" name ="branchName" ng-model="branch.branchName" readonly ng-required="true" >
	       			<label for="code">User Branch</label>	
	       		</div>
		    </div>

 			<div class="row">
	   		 	
	   		 	<div class="col s6 input-field">
	       				<input class="validate" type ="text" id="empId" name ="empName" ng-model="emp.empName" ng-click="openEmpModel()" readonly ng-required="true" >
	       			<label for="code">Select Employee</label>	
	       		</div>

	   		 </div>
	   		 
	   		 
	   		  <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" id="saveId" value="submit" >
	       		</div>
	   		 </div>
		  </form>
    </div>
    
    
    <div id ="userModDB" ng-hide="userModDBFlag">
		  <input type="text" name="filterUser" ng-model="filterUser" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>UserName</th>
 	  	 	  <th>Role</th>
 	  	 	  <th>Status</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="user in userList | filter:filterUser">
		 	  <td><input type="radio"  name="user"   value="{{ user }}" ng-model="userCode" ng-click="selectUser(user)"></td>
              <td>{{ user.userName }}</td>
              <td>{{ user.userRole }}</td>
              <td>{{ user.userStatus }}</td>
          </tr>
      </table> 
	</div>
	
	
	<!-- <div id ="brModDB" ng-hide="brModDBFlag">
		  <input type="text" name="filterBranch" ng-model="filterBranch" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Branch Name</th>
 	  	 	  <th>Branch FACode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="brh in branchList | filter:filterBranch">
		 	  <td><input type="radio"  name="brh"   value="{{ brh }}" ng-model="brhCode" ng-click="selectBranch(brh)"></td>
              <td>{{ brh.branchName }}</td>
              <td>{{ brh.branchFaCode }}</td>
          </tr>
      </table> 
	</div> -->
    
    <div id ="empModDB" ng-hide="empModDBFlag">
		  <input type="text" name="filterEmp" ng-model="filterEmp" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Employee Name</th>
 	  	 	  <th>FACode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="emp in empList | filter:filterEmp">
		 	  <td><input type="radio"  name="emp"   value="{{ emp }}" ng-model="empCode" ng-click="selectEmp(emp)"></td>
              <td>{{ emp.empName }}</td>
              <td>{{ emp.empFaCode }}</td>
          </tr>
      </table> 
	</div>
    
</div>    
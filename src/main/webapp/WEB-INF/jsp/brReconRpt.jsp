<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="brReconRptForm" ng-submit="submitBrReconRpt(brReconRptForm)" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
					
				<div class="col s3 input-field">
		       		<input class="validate" type ="date" id="frmDtId" name ="frmDtName" ng-model="frmDt" ng-required="true" >
		       	<label for="code">From Date</label>	
		       	</div>
			       	
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="date" id="toDtId" name ="toDtName" ng-model="toDt" ng-required="true" >
		       	<label for="code">To Date</label>	
		       	</div>
		       		
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="frmBrId" name ="frmBrName" ng-model="frmBr.branchName" ng-click="openFrmBrDB()" readonly>
		       	<label for="code">From Branch</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="toBrId" name ="toBrName" ng-model="toBr.branchName" ng-click="openToBrDB()" readonly>
		       	<label for="code">To Branch</label>	
		       	</div>
		       	
			</div>
			<div class="row">
   			 	<div class="col s12 center">
   			 		<input type="submit" id="submitId" value="Submit">
   			 		<input type="button" id="printCstXlsId" value="PendingXls" ng-click="printCstXls()">
   			 		<input type="button" id="printCrTvXlsId" value="CrTvXls" ng-click="printCrTvXls()">
   			 	</div>
      		</div>
		</form>
	</div>
	
	
	<div id ="frmBrDB" ng-hide="frmBrDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="frmBr in branchList | filter:filterTextbox1">
		 	  <td><input type="radio" name="frmBr" value="{{ frmBr }}" ng-click="saveFrmBr(frmBr)"></td>
              <td>{{frmBr.branchName}}</td>
              <td>{{frmBr.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="toBrDB" ng-hide="toBrDBFlag">
		<input type="text" name="filterTextbox2" ng-model="filterTextbox2" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="toBr in branchList | filter:filterTextbox2">
		 	  <td><input type="radio" name="toBr" value="{{ toBr }}" ng-click="saveToBr(toBr)"></td>
              <td>{{toBr.branchName}}</td>
              <td>{{toBr.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="csToBrDB" ng-hide="csToBrDBFlag">
		<input type="text" name="filterTextbox3" ng-model="filterTextbox3" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="csToBr in branchList | filter:filterTextbox3">
		 	  <td><input type="radio" name="csToBr" value="{{ csToBr }}" ng-click="saveCSToBr(csToBr)"></td>
              <td>{{csToBr.branchName}}</td>
              <td>{{csToBr.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="faMstrDB" ng-hide="faMstrDBFlag">
		<input type="text" name="filterTextbox4" ng-model="filterTextbox4" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> FaCode </th>
 	  	  	  <th> Name </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="faCodeName in faCodeNameList | filter:filterTextbox4">
		 	  <td><input type="radio"  name="faCodeName" value="{{ faCodeName }}" ng-click="saveFaMstr(faCodeName)"></td>
              <td>{{faCodeName.faMfaCode}}</td>
              <td>{{faCodeName.faMfaName}}</td>
          </tr>
          
      </table> 
	</div>
	
	<div id ="csBrDB" ng-hide="csBrDBFlag">
		<input type="text" name="filterTextbox5" ng-model="filterTextbox5" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="csBr in branchList | filter:filterTextbox5">
		 	  <td><input type="radio" name="csBr" value="{{ csBr }}" ng-click="saveCSBr(csBr)"></td>
              <td>{{csBr.branchName}}</td>
              <td>{{csBr.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div class="row" ng-if="cashStmtList.length>0" id="cstId">
		<table class="tblrow" >
       		
       		<caption class="coltag tblrow">Pending(Clear by Date)</caption> 
       		
            <tr class="rowclr">
            	<th class="colclr">S.No</th>
	            <th class="colclr">FrmBr</th>
	            <th class="colclr">PendingBr</th>
	            <th class="colclr">CsCloseDT</th>
	            <th class="colclr">FaCode</th>
	            <th class="colclr">Date</th>
	            <th class="colclr">VType</th>
	            <th class="colclr">D/C</th>
	            <th class="colclr">Amt</th>
	            <!-- <th class="colclr">Type</th> -->
	            <th class="colclr">Narration</th>
	            <!-- <th class="colclr">Save</th> -->
	        </tr>	
	         
	        <tr ng-repeat="cashStmt in cashStmtList | orderBy : '+fromBrName'">
	            <td class="rowcel">{{$index+1}}</td>
	            <td class="rowcel">{{cashStmt.fromBrName}}</td>
	            <td class="rowcel">{{cashStmt.toBrName}}</td>
	            <td class="rowcel">{{cashStmt.toBrCsCloseDt | date : 'dd/MM/yy'}}</td>
	            <td class="rowcel">{{cashStmt.csFaCode}}</td>
	            <!-- <td class="rowcel">
		        	<input type="text" id="csBrhFaCodeId{{$index}}" name="csBrhName" ng-model="cashStmtTemp.csBrhName" readonly="readonly" size="8" ng-click="openCSToBrDB($index)">
		        </td>
		        <td class="rowcel" ng-if="cashStmtTemp.csDrCr === 'C'">
		        	<input type="text" id="csFaCodeId{{$index}}" name="csFaCodeName" ng-model="cashStmtTemp.csFaCode" readonly="readonly" size="6" ng-click="openFaMstrDB($index)" disabled="disabled">
		        </td>
		        <td class="rowcel" ng-if="cashStmtTemp.csDrCr === 'D'">
		        	<input type="text" id="csFaCodeId{{$index}}" name="csFaCodeName" ng-model="cashStmtTemp.csFaCode" readonly="readonly" size="6" ng-click="openFaMstrDB($index)">
		        </td> -->
	            <td class="rowcel">{{cashStmt.csDt | date : 'dd/MM/yy'}}</td>
	            <td class="rowcel">{{cashStmt.csVouchType}}</td>
	            <td class="rowcel">{{cashStmt.csDrCr}}</td>
	            <td class="rowcel">{{cashStmt.csAmt}}</td>
	           <!--  <td class="rowcel">{{cashStmtTemp.csType}}</td> -->
	            <td class="rowcel">{{cashStmt.csDescription}}</td>
	            <!-- <td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="saveCSTemp(cashStmtTemp)" >
						<i class="mdi-content-send white-text"></i>
					</a>
				</td> -->
			</tr>
		</table>
    </div>
    
    <div class="row" ng-if="tempIntBrTvList.length>0" id="crTvId">
   		<table class="tblrow">
		<caption class="coltag tblrow">PENDING IBTV C/D OF</caption>
			<tr class="rowclr">
				<th class="colclr">S.No.</th>
				<th class="colclr">D_BRANCH</th>
				<th class="colclr">C_BRANCH</th>
				
				<th class="colclr">D/C</th>
				<th class="colclr">DATE</th>
				<th class="colclr">AMOUNT</th>
				<th class="colclr">DESCRIPTION</th>
			</tr>
			<tr class="tbl" ng-repeat="tibTv in tempIntBrTvList | orderBy : '+tibDrBrName'">
				<td class="rowcel">{{$index+1}}</td>
				<td class="rowcel">{{tibTv.tibDrBrName}}</td>
				<td class="rowcel">{{tibTv.tibCrBrName}}</td>
				<td class="rowcel">{{tibTv.tibDOrC}}</td>
				<td class="rowcel">{{tibTv.tibDate | date:'dd/MM/yyyy'}}</td>
				<td class="rowcel">{{tibTv.tibAmt}}</td>
				<td class="rowcel">{{tibTv.tibDesc}}</td>
			</tr>
		</table>
		<br>
	</div>
    
   <div id ="cashStmtDB" ng-hide="cashStmtDBFlag">
		<table class="tblrow" >
       		
       		<tr class="rowclr">
       			<th class="colclr">S.No.</th>
	            <th class="colclr">csAmt</th>
	            <th class="colclr">D/C</th>
	            <th class="colclr">Desc</th>
	            <th class="colclr">FaCode</th>
	            <th class="colclr">Save</th>
	        </tr>	
	         
	        <tr ng-repeat="cashStmt in cashStmtList">
	            <td class="rowcel">{{$index+1}}</td>
	            <td class="rowcel">{{cashStmt.csAmt}}</td>
	            <td class="rowcel">{{cashStmt.csDrCr}}</td>
	            <td class="rowcel">{{cashStmt.csDescription}}</td>
	            <td class="rowcel">
		        	<input type="text" id="csFaCodeId{{$index}}" name="csFaCodeName" ng-model="cashStmt.csFaCode" readonly="readonly" size="6" ng-click="openCSBrDB($index)">
		        </td>
	            <td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="saveCS(cashStmt)" >
						<i class="mdi-content-send white-text"></i>
					</a>
				</td>
			</tr>
		</table>
	</div>
	
</div>
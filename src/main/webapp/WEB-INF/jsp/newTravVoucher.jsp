<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="newVoucherForm" ng-submit=voucherSubmit(newVoucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly  >
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.cashStmtStatus.cssCsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div>
			   		 <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code">Voucher Type</label>	
		       		</div>
		       		<div class="col s4 input-field">
				    		<select name="payBy" id="payById" ng-model="vs.payBy" ng-change="selectPayBy()" ng-required="true">
								<option value='C'>By Cash</option>
								<option value='Q'>By Cheque</option>
								<option value='O'>By Online</option>
							</select>
							<label>Payment By</label>
					</div>
		    </div>
		    
		     <div class="row">
		     		<div class="col s4 input-field">
			    		<select name="chequeType" id="chequeTypeId" ng-model="vs.chequeType" ng-change="selectChqType()" disabled="disabled">
							<option value='C'>Computerized</option>
							<option value='M'>Manual</option>
						</select>
						<label>Cheque Type </label>
					</div>
					
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bankCodeId" name ="bankCode" ng-model="vs.bankCode"  ng-click = "openBankCodeDB()" readonly disabled="disabled">
		       			<label for="code">Bank Code</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="chequeNoId" name ="chequeNo" ng-model="vs.chequeLeaves.chqLChqNo" ng-click="openChqDB()" readonly disabled="disabled">
		       			<label for="code">Cheque No</label>	
		       		</div>
		    </div>
		       	
		    <div class="row">
	     		<div class="col s4 input-field">
	       				<input type ="text" id="payToId" name ="payToName" ng-model="vs.payTo" disabled="disabled">
	       			<label for="code">Pay To</label>	
	       		</div>
	       		
				<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="tdsCodeId" name ="tdsCode" ng-model="vs.tdsCode" ng-click="selectTds()" readonly>
		       			<label for="code">TDS Code</label>	
		       	</div>
		      
		        <div class="col s4 input-field">
		       				<input class="validate" type ="number" id="tdsAmtId" name ="tdsAmt" ng-model="vs.tdsAmt"  STEP = "0.01" >
		       			<label for="code">TDS Amount</label>	
		       	</div>
	   		</div>
	   		
	   		
	   		 <div class="row">
		   		 <div class="col s4 input-field">
							<input type ="text" name="empCode" id="empCodeId" ng-model="empCode" ng-click="openEmpDB()" disabled="disabled" readonly ng-required="true">
						<label>Select Employee Code</label>
				  </div>
	   		 </div>
	   			   		
	   		<div class="row">	   		
	   		<div ng-show="travVList.length > 0">
	   			<table class="tblrow">
	   				   <caption class="coltag tblrow">Travel Expense Details</caption>
	   				 <tr class="rowclr">
                        <th class="colclr">Emp Code</th>
                        <th class="colclr">TRAVELLING</th>
                        <th class="colclr">BOARDING</th>
                        <th class="colclr">LODGING</th>    
                        <th class="colclr">CONVEYANCE</th>    
                        <th class="colclr">OTHERS</th>  
                        <th class="colclr">TOTAL EXP</th>   
                        <th class="colclr">ACTION</th>                      
                    </tr>
                     <tr class="tbl" ng-repeat="travV in travVList">
                        <td class="rowcel">{{travV.employee.empFaCode}}</td>
                        <td class="rowcel">
                        	<div ng-repeat="faTrav in travV.ftdList">
                        		<div ng-if="faTrav.ftdTravType == 'TRAVELLING'">
                        			<table>
                        				<tr>
                        					<td>H N</td>
                        					<td>{{faTrav.ftdHName}}</td>
                        				</tr>
                        				<tr>
                        					<td>H R</td>
                        					<td>{{faTrav.ftdHRate}}</td>
                        				</tr>
                        				<tr>
                        					<td>Place</td>
                        					<td>{{faTrav.ftdPlace}}</td>
                        				</tr>
                        				<tr>
                        					<td>Fr Dt</td>
                        					<td>{{faTrav.ftdFrDt}}</td>
                        				</tr>
                        				<tr>
                        					<td>To Dt</td>
                        					<td>{{faTrav.ftdToDt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Amt</td>
                        					<td>{{faTrav.ftdTravAmt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Desc</td>
                        					<td>{{faTrav.ftdDesc}}</td>
                        				</tr>
                        			</table>
                        		</div>
                        	</div>
                        </td>	 
                        <td class="rowcel">
                        	<div ng-repeat="faTrav in travV.ftdList">
                        		<div ng-if="faTrav.ftdTravType == 'BOARDING'">
                        			<table>
                        				<tr>
                        					<td>H N</td>
                        					<td>{{faTrav.ftdHName}}</td>
                        				</tr>
                        				<tr>
                        					<td>H R</td>
                        					<td>{{faTrav.ftdHRate}}</td>
                        				</tr>
                        				<tr>
                        					<td>Place</td>
                        					<td>{{faTrav.ftdPlace}}</td>
                        				</tr>
                        				<tr>
                        					<td>Fr Dt</td>
                        					<td>{{faTrav.ftdFrDt}}</td>
                        				</tr>
                        				<tr>
                        					<td>To Dt</td>
                        					<td>{{faTrav.ftdToDt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Amt</td>
                        					<td>{{faTrav.ftdTravAmt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Desc</td>
                        					<td>{{faTrav.ftdDesc}}</td>
                        				</tr>
                        			</table>
                        		</div>
                        	</div>
                        </td>  
                        <td class="rowcel">
                        	<div ng-repeat="faTrav in travV.ftdList">
                        		<div ng-if="faTrav.ftdTravType == 'LODGING'">
                        			<table>
                        				<tr>
                        					<td>H N</td>
                        					<td>{{faTrav.ftdHName}}</td>
                        				</tr>
                        				<tr>
                        					<td>H R</td>
                        					<td>{{faTrav.ftdHRate}}</td>
                        				</tr>
                        				<tr>
                        					<td>Place</td>
                        					<td>{{faTrav.ftdPlace}}</td>
                        				</tr>
                        				<tr>
                        					<td>Fr Dt</td>
                        					<td>{{faTrav.ftdFrDt}}</td>
                        				</tr>
                        				<tr>
                        					<td>To Dt</td>
                        					<td>{{faTrav.ftdToDt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Amt</td>
                        					<td>{{faTrav.ftdTravAmt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Desc</td>
                        					<td>{{faTrav.ftdDesc}}</td>
                        				</tr>
                        			</table>
                        		</div>
                        	</div>
                        </td>
                        <td class="rowcel">
                        	<div ng-repeat="faTrav in travV.ftdList">
                        		<div ng-if="faTrav.ftdTravType == 'CONVEYANCE'">
                        			<table>
                        				<tr>
                        					<td>H N</td>
                        					<td>{{faTrav.ftdHName}}</td>
                        				</tr>
                        				<tr>
                        					<td>H R</td>
                        					<td>{{faTrav.ftdHRate}}</td>
                        				</tr>
                        				<tr>
                        					<td>Place</td>
                        					<td>{{faTrav.ftdPlace}}</td>
                        				</tr>
                        				<tr>
                        					<td>Fr Dt</td>
                        					<td>{{faTrav.ftdFrDt}}</td>
                        				</tr>
                        				<tr>
                        					<td>To Dt</td>
                        					<td>{{faTrav.ftdToDt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Amt</td>
                        					<td>{{faTrav.ftdTravAmt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Desc</td>
                        					<td>{{faTrav.ftdDesc}}</td>
                        				</tr>
                        			</table>
                        		</div>
                        	</div>
                        </td>
                        <td class="rowcel">
                        	<div ng-repeat="faTrav in travV.ftdList">
                        		<div ng-if="faTrav.ftdTravType == 'OTHER'">
                        			<table>
                        				<tr>
                        					<td>H N</td>
                        					<td>{{faTrav.ftdHName}}</td>
                        				</tr>
                        				<tr>
                        					<td>H R</td>
                        					<td>{{faTrav.ftdHRate}}</td>
                        				</tr>
                        				<tr>
                        					<td>Place</td>
                        					<td>{{faTrav.ftdPlace}}</td>
                        				</tr>
                        				<tr>
                        					<td>Fr Dt</td>
                        					<td>{{faTrav.ftdFrDt}}</td>
                        				</tr>
                        				<tr>
                        					<td>To Dt</td>
                        					<td>{{faTrav.ftdToDt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Amt</td>
                        					<td>{{faTrav.ftdTravAmt}}</td>
                        				</tr>
                        				<tr>
                        					<td>Desc</td>
                        					<td>{{faTrav.ftdDesc}}</td>
                        				</tr>
                        			</table>
                        		</div>
                        	</div>
                        </td>
                        <td class="rowcel">{{travV.totAmt}}</td>		
	   			   		<td class="rowcel">
                        	<input type="button" value="REMOVE" ng-click="removeTravE($index,travV)"/>
                        </td>
                    </tr>
                    <tr class="tbl" >							
							<td class="rowcel" colspan="6">Total Amount</td>
							<td class="rowcel">{{totAmtSum}}</td>
							<td class="rowcel"></td>
						</tr>
	   			</table>
	   		</div>
	   		</div>	   		
	   			   		
	   			   		
	   			   		
	   			   		
	   			   		
	   		 <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="vs.desc" ng-required="true"></textarea>
 					<label>Description</label>
				</div>
    		 </div>	
	   		 
	   		  <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       		<!-- <div class="col s4 input-field">
	       				<input type="button" value="closing voucher" ng-click="closeVoucher()">
	       		</div> -->
	   		 </div>
		  </form>
    </div>
    
    
     <div id ="bankCodeDB" ng-hide="bankCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Bank Code </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bankCode in bankCodeList | filter:filterTextbox">
		 	  <td><input type="radio"  name="bankCode"   value="{{ bankCode }}" ng-model="bkCode" ng-click="saveBankCode(bankCode)"></td>
              <td>{{bankCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	
	<div id ="selChqDB" ng-hide="selChqDBFlag">
		  <input type="text" name="filterTextChq" ng-model="filterTextChq" placeholder="Search...">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Cheque No</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="chq in chqList | filter:filterTextChq">
		 	  <td><input type="radio"  name="chq"   value="{{ chq }}" ng-model="chqCode" ng-click="saveChqNo(chq)"></td>
              <td>{{chq.chqLChqNo}}</td>
              
          </tr>
      </table> 
	</div>
	

	<div id="tdsCodeDB" ng-hide=tdsCodeFlag>
    	<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search by Tds Code ">
	 	  <table>
	 	  	  <tr>
	 	  	  	  <th></th>
	 	  	  	  <th> TDS Code </th>
	 	  	  	  <th> Name </th>
	 	  	  	  <th> Type </th>
	 	  	  </tr>
	 	  
			  <tr ng-repeat="fsMTds in fsMTdsList | filter:filterTextbox">
			 	  <td><input type="radio"  name="fsMTds"   value="{{ fsMTds }}" ng-model="fsMT" ng-click="saveTdsCode(fsMTds)"></td>
	              <td>{{fsMTds.faMfaCode}}</td>
	              <td>{{fsMTds.faMfaName}}</td>
	              <td>{{fsMTds.faMfaType}}</td>
	          </tr>
	      </table> 
    </div>
    
    
    <div id="empCodeDB" ng-hide=empCodeDBFlag>
    	<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search by Employee Code ">
	 	  <table>
	 	  	  <tr>
	 	  	  	  <th></th>
	 	  	  	  <th>Name</th>
	 	  	  	  <th>Code</th>
	 	  	  </tr>
	 	  
			  <tr ng-repeat="emp in empList | filter:filterTextbox">
			 	  <td><input type="radio"  name="emp"   value="{{ emp }}" ng-model="empId" ng-click="saveEmpCode(emp)"></td>
	              <td>{{emp.empName}}</td>
	              <td>{{emp.empFaCode}}</td>
	          </tr>
	      </table> 
    </div>


	<div id="travVoucherDB" ng-hide="travVoucherDBFlag">
	<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="travVoucherForm" ng-submit="submitTRVoucher(travVoucherForm)">
		 <table>
		 	<tr>
		 		<td></td>
		 		<td>EXPENSE Type</td>
		 	</tr>
		 	<tr>
		 		<td><input type="checkbox" id ="travellingId" ng-model="travelling" ng-change="saveTravelling()"></td>
		 		<td><input type ="button" id="travDetId" value="TRAVELLING" ng-disabled="!travelling" ng-click="openTravDet()"></td>
		 	</tr>
		 	<tr>
		 		<td><input type="checkbox" id ="boardingId" ng-model="boarding" ng-change="saveBoarding()"></td>
		 		<td><input type ="button" id="brdDetId" value="BOARDING" ng-disabled="!boarding"" ng-click="openBrdDet()"></td>
		 	</tr>
		 	<tr>
		 		<td><input type="checkbox" id ="lodgingId" ng-model="lodging" ng-change="saveLodging()"></td>
		 		<td><input type ="button" id="lodgDetId" value="LODGING" ng-disabled="!lodging" ng-click="openLodgDet()"></td>
		 	</tr>
		 	<tr>
		 		<td><input type="checkbox" id ="conveyanceId" ng-model="conveyance" ng-change="saveConv()"></td>
		 		<td><input type ="button" id="convDetId" value="CONVEYANCE" ng-disabled="!conveyance" ng-click="openConvDet()"></td>
		 	</tr>
		 	<tr>
		 		<td><input type="checkbox" id ="otherId" ng-model="other" ng-change="saveOther()"></td>
		 		<td><input type ="button" id="othDetId" value="OTHER" ng-disabled="!other" ng-click="openOthDet()"></td>
		 	</tr>
		 	<tr>
		 		<td>
		 			<input type="submit" value="submit"/>
		 		</td>
		 	</tr>
		 </table>
	</form>
	</div>
	
	
	<div id="travDetDB" ng-hide="travDetDBFlag">
		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="trvlDetForm" ng-submit="submitTrvlDet(trvlDetForm)">
			<table>
				<tr>
					<td>Hotel Name</td>
					<td><input type="text" name="hName" id="hNameIdId" ng-model="faTrv1.ftdHName"/></td>
				</tr>
				
				<tr>
					<td>Hotel Rate</td>
					<td><input type="number" name="hRate" id="hRateId" ng-model="faTrv1.ftdHRate"/></td>
				</tr>
				
				<tr>
					<td>Place</td>
					<td><input type="text" name="place" id="placeId" ng-model="faTrv1.ftdPlace"/></td>
				</tr>
				
				<tr>
					<td>From DT</td>
					<td><input type="date" name="frDt" id="frDtId" ng-model="faTrv1.ftdFrDt"/></td>
				</tr>
				
				<tr>
					<td>To DT</td>
					<td><input type="date" name="toDt" id="toDtId" ng-model="faTrv1.ftdToDt"/></td>
				</tr>
				
				<tr>
					<td>Travel Amount</td>
					<td><input type="number" name="travAmt" id="travAmtId" ng-model="faTrv1.ftdTravAmt"/></td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td><i class="mdi-editor-mode-edit prefix"></i>
 						<textarea id="ftdDescId" name ="ftdDesc" ng-model="faTrv1.ftdDesc"></textarea>
 					</td>
				</tr>
				
				<tr>
					<td>
						<input type="submit" value="submit"/>
					</td>
				</tr>
			
			</table>
		</form>
	</div>
	
	
	<div id="brdDetDB" ng-hide="brdDetDBFlag">
		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="brdDetForm" ng-submit="submitBrdDet(brdDetForm)">
			<table>
				<tr>
					<td>Hotel Name</td>
					<td><input type="text" name="hName" id="hNameIdId" ng-model="faTrv2.ftdHName"/></td>
				</tr>
				
				<tr>
					<td>Hotel Rate</td>
					<td><input type="number" name="hRate" id="hRateId" ng-model="faTrv2.ftdHRate"/></td>
				</tr>
				
				<tr>
					<td>Place</td>
					<td><input type="text" name="place" id="placeId" ng-model="faTrv2.ftdPlace"/></td>
				</tr>
				
				<tr>
					<td>From DT</td>
					<td><input type="date" name="frDt" id="frDtId" ng-model="faTrv2.ftdFrDt"/></td>
				</tr>
				
				<tr>
					<td>To DT</td>
					<td><input type="date" name="toDt" id="toDtId" ng-model="faTrv2.ftdToDt"/></td>
				</tr>
				
				<tr>
					<td>Travel Amount</td>
					<td><input type="number" name="travAmt" id="travAmtId" ng-model="faTrv2.ftdTravAmt"/></td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td><i class="mdi-editor-mode-edit prefix"></i>
 						<textarea id="ftdDescId" name ="ftdDesc" ng-model="faTrv2.ftdDesc"></textarea>
 					</td>
				</tr>
				
				<tr>
					<td>
						<input type="submit" value="submit"/>
					</td>
				</tr>
			
			</table>
		</form>
	</div>
	
	
		<div id="lodgDetDB" ng-hide="lodgDetDBFlag">
		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="lodgDetForm" ng-submit="submitLodgDet(lodgDetForm)">
			<table>
				<tr>
					<td>Hotel Name</td>
					<td><input type="text" name="hName" id="hNameIdId" ng-model="faTrv3.ftdHName"/></td>
				</tr>
				
				<tr>
					<td>Hotel Rate</td>
					<td><input type="number" name="hRate" id="hRateId" ng-model="faTrv3.ftdHRate"/></td>
				</tr>
				
				<tr>
					<td>Place</td>
					<td><input type="text" name="place" id="placeId" ng-model="faTrv3.ftdPlace"/></td>
				</tr>
				
				<tr>
					<td>From DT</td>
					<td><input type="date" name="frDt" id="frDtId" ng-model="faTrv3.ftdFrDt"/></td>
				</tr>
				
				<tr>
					<td>To DT</td>
					<td><input type="date" name="toDt" id="toDtId" ng-model="faTrv3.ftdToDt"/></td>
				</tr>
				
				<tr>
					<td>Travel Amount</td>
					<td><input type="number" name="travAmt" id="travAmtId" ng-model="faTrv3.ftdTravAmt"/></td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td><i class="mdi-editor-mode-edit prefix"></i>
 						<textarea id="ftdDescId" name ="ftdDesc" ng-model="faTrv3.ftdDesc"></textarea>
 					</td>
				</tr>
				
				<tr>
					<td>
						<input type="submit" value="submit"/>
					</td>
				</tr>
			
			</table>
		</form>
	</div>
	
	
	<div id="convDetDB" ng-hide="convDetDBFlag">
		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="convDetForm" ng-submit="submitConvDet(convDetForm)">
			<table>
				<tr>
					<td>Hotel Name</td>
					<td><input type="text" name="hName" id="hNameIdId" ng-model="faTrv4.ftdHName"/></td>
				</tr>
				
				<tr>
					<td>Hotel Rate</td>
					<td><input type="number" name="hRate" id="hRateId" ng-model="faTrv4.ftdHRate"/></td>
				</tr>
				
				<tr>
					<td>Place</td>
					<td><input type="text" name="place" id="placeId" ng-model="faTrv4.ftdPlace"/></td>
				</tr>
				
				<tr>
					<td>From DT</td>
					<td><input type="date" name="frDt" id="frDtId" ng-model="faTrv4.ftdFrDt"/></td>
				</tr>
				
				<tr>
					<td>To DT</td>
					<td><input type="date" name="toDt" id="toDtId" ng-model="faTrv4.ftdToDt"/></td>
				</tr>
				
				<tr>
					<td>Travel Amount</td>
					<td><input type="number" name="travAmt" id="travAmtId" ng-model="faTrv4.ftdTravAmt"/></td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td><i class="mdi-editor-mode-edit prefix"></i>
 						<textarea id="ftdDescId" name ="ftdDesc" ng-model="faTrv4.ftdDesc"></textarea>
 					</td>
				</tr>
				
				<tr>
					<td>
						<input type="submit" value="submit"/>
					</td>
				</tr>
			
			</table>
		</form>
	</div>
	
	
	<div id="othDetDB" ng-hide="othDetDBFlag">
		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="othDetForm" ng-submit="submitOthDet(othDetForm)">
			<table>
				<tr>
					<td>Hotel Name</td>
					<td><input type="text" name="hName" id="hNameIdId" ng-model="faTrv5.ftdHName"/></td>
				</tr>
				
				<tr>
					<td>Hotel Rate</td>
					<td><input type="number" name="hRate" id="hRateId" ng-model="faTrv5.ftdHRate"/></td>
				</tr>
				
				<tr>
					<td>Place</td>
					<td><input type="text" name="place" id="placeId" ng-model="faTrv5.ftdPlace"/></td>
				</tr>
				
				<tr>
					<td>From DT</td>
					<td><input type="date" name="frDt" id="frDtId" ng-model="faTrv5.ftdFrDt"/></td>
				</tr>
				
				<tr>
					<td>To DT</td>
					<td><input type="date" name="toDt" id="toDtId" ng-model="faTrv5.ftdToDt"/></td>
				</tr>
				
				<tr>
					<td>Travel Amount</td>
					<td><input type="number" name="travAmt" id="travAmtId" ng-model="faTrv5.ftdTravAmt"/></td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td><i class="mdi-editor-mode-edit prefix"></i>
 						<textarea id="ftdDescId" name ="ftdDesc" ng-model="faTrv5.ftdDesc"></textarea>
 					</td>
				</tr>
				
				<tr>
					<td>
						<input type="submit" value="submit"/>
					</td>
				</tr>
			
			</table>
		</form>
	</div>
	
		<div id="saveVsDB" ng-hide="saveVsFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{vs.branch.branchFaCode}}</td>
				</tr>
				
				<tr>
					<td>Branch Name</td>
					<td>{{vs.branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Date</td>
					<td>{{dateTemp}}</td>
				</tr>
				
				<tr>
					<td>Sheet No</td>
					<td>{{vs.cashStmtStatus.cssCsNo}}</td>
				</tr>
				
				<tr>
					<td>Voucher Type</td>
					<td>{{vs.voucherType}}</td>
				</tr>
				
				<tr>
					<td>Payment By</td>
					<td>{{vs.payBy}}</td>
				</tr>
				
				<tr>
					<td>Cheque Type</td>
					<td>{{vs.chequeType}}</td>
				</tr>
				
				<tr>
					<td>Bank Code</td>
					<td>{{vs.bankCode}}</td>
				</tr>
				
				<tr>
					<td>Cheque No</td>
					<td>{{vs.chequeLeaves.chqLChqNo}}</td>
				</tr>
				
				<tr>
					<td>Pay To</td>
					<td>{{vs.payTo}}</td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td>{{vs.desc}}</td>
				</tr>
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" id="saveId" ng-click="saveVS()"/>
		</div>
	</div>
	
</div>    
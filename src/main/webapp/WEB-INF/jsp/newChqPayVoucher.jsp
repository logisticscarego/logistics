<div ng-show="operatorLogin || superAdminLogin">
<div ng-show="loadingFlag" style=" text-align:center; margin-left: -15px; margin-top: -50px; position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
        <div style="margin-left:20px; margin-top:100px;">
           <svg width='200px' height='200px'  viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-gears">
             <rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect>
                <g transform="translate(-20,-20)">
                      <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#2bbbad ">
                         <animateTransform attributeName="transform" type="rotate" from="90 50 50" to="0 50 50" dur="0.8s" repeatCount="indefinite"></animateTransform>
                       </path>
                 </g>
                 <g transform="translate(20,20) rotate(15 50 50)">
                     <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#0A3F4E ">
                          <animateTransform attributeName="transform" type="rotate" from="0 50 50" to="90 50 50" dur="0.8s" repeatCount="indefinite">
                          </animateTransform>
                      </path>
                  </g>
            </svg>
        </div>
       <span style=" color: #ffffff ;font-size:30px; font-weight:bold; letter-spacing:3px;">Loading...</span>
</div>
	<div class="row">
		<form name="newVoucherForm" ng-submit=voucherSubmit(newVoucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly  >
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.cashStmtStatus.cssCsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div>
			   		 <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code">Voucher Type</label>	
		       		</div>
		       		<div class="col s4 input-field">
				    		<select name="payBy" id="payById" ng-model="vs.payBy" ng-change="selectPayBy()" ng-required="true">
								<option value='Q'>By Cheque</option>
								<option value='O'>By Online</option>
							</select>
							<label>Payment By</label>
					</div>
		    </div>
		    
		     <div class="row">
		     		<div class="col s4 input-field">
			    		<select name="chequeType" id="chequeTypeId" ng-model="vs.chequeType" ng-change="selectChqType()" disabled="disabled">
							<option value='C'>Computerized</option>
							<option value='M'>Manual</option>
						</select>
						<label>Cheque Type </label>
					</div>
					
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bankCodeId" name ="bankCode" ng-model="vs.bankCode"  ng-click = "openBankCodeDB()" readonly>
		       			<label for="code">Bank Code</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="chequeNoId" name ="chequeNo" ng-model="vs.chequeLeaves.chqLChqNo" disabled="disabled" ng-keyup="openChqNoDB($event.keyCode)">
		       			<label for="code">Cheque No (Last three digits)</label>	
		       		</div>
		       		
		    </div>
		       	
		    <div class="row">
	     		
	       		<div class="col s4 input-field">
	       					<input type ="text" id="payToId" name ="payToName" ng-model="vs.payTo" disabled="disabled">
	       				<label for="code">Pay To</label>	
	       		</div>
	       		
				<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="tdsCodeId" name ="tdsCode" ng-model="vs.tdsCode" ng-click="selectTds()" readonly="readonly">
		       			<label for="code">TDS Code</label>	
		       	</div>
		      
		        <div class="col s4 input-field">
		       				<input class="validate" type ="number" id="tdsAmtId" name ="tdsAmt" ng-model="vs.tdsAmt"  STEP = "0.01" >
		       			<label for="code">TDS Amount</label>	
		       	</div>
		       	  
	   		</div>
	   		
	   		<div class="row">
	   			<div class="col s4 input-field">
							<input type ="text" name="faCode" id="faCodeId" ng-model="faCode" ng-keyup="openFACodeDB($event.keyCode)" ng-required="true">
						<label>FA Name(First three characters)/Code (Last three digits)</label>
				  </div>
			</div>	  
	   		
	   		<div class="row">	   			   
		   		<div ng-show="actFaMList.length > 0">
		   			<table class="tblrow">
		   				   <caption class="coltag tblrow">Cheque Payment Voucher Details</caption>
		   				 <tr class="rowclr">
	                        <th class="colclr">S.No.</th>
	                        <th class="colclr">FA CODE</th>
	                        <th class="colclr">AMOUNT</th>
	                        <th class="colclr">ACTION</th>                      
	                     </tr>
	                     <tr class="tbl" ng-repeat="actFaM in actFaMList">
	                     	<td class="rowcel">{{$index + 1}}</td>
	                        <td class="rowcel">{{actFaM.faCode}}</td>
	                        <td class="rowcel">{{actFaM.amt}}</td>
	                        <td class="rowcel">
	                        	<input type="button" value="REMOVE" ng-click="removeCPV($index,actFaM)"/>
	                        </td>
	                 	</tr>
	                 	<tr class="tbl" >							
							<td class="rowcel" colspan="2">Total Amount</td>
							<td class="rowcel">{{amtSum}}</td>
							<td class="rowcel"></td>							
						</tr>
	                 </table>
	             </div>    	       	   
	   		</div>	   
	   			   		
	   		 <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="vs.desc" ng-required="true"></textarea>
 					<label>Description</label>
				</div>
    		 </div>	
	   		 
	   		  <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       		<!-- <div class="col s4 input-field">
	       				<input type="button" value="closing voucher" ng-click="closeVoucher()">
	       		</div> -->
	   		 </div>
		  </form>
    </div>
    
   <div id ="bankCodeDB" ng-hide="bankCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox1" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Bank Code </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bankCode in bankCodeList | filter:filterTextbox1">
		 	  <td><input type="radio"  name="bankCode"   value="{{ bankCode }}" ng-model="bkCode" ng-click="saveBankCode(bankCode)"></td>
              <td>{{bankCode}}</td>
              
          </tr>
      </table> 
	</div> 
	
	<div id ="chqNoDB" ng-hide="chqNoDBFlag" class="noprint">
		  <input type="text" name="filterChqbox" ng-model="filterChqbox" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Cheque No</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="chq in chqList | filter:filterChqbox ">
		 	  <td><input type="radio"  name="chq.chqLChqNo"   value="{{ chq.chqLChqNo }}" ng-model="chqCode" ng-click="saveChqNo(chq)"></td>
              <td>{{chq.chqLChqNo}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id="tdsCodeDB" ng-hide=tdsCodeFlag>
    	<input type="text" name="filterTextbox" ng-model="filterTextbox2" placeholder="Search by Tds Code">
	 	  <table>
	 	  	  <tr>
	 	  	  	  <th></th>
	 	  	  	  <th> TDS Code </th>
	 	  	  	  <th> Name </th>
	 	  	  	  <th> Type </th>
	 	  	  </tr>
	 	  
			  <tr ng-repeat="fsMTds in faTList | filter:filterTextbox2">
			 	  <td><input type="radio"  name="fsMTds"   value="{{ fsMTds }}" ng-model="fsMT" ng-click="saveTdsCode(fsMTds)"></td>
	              <td>{{fsMTds.faMfaCode}}</td>
	              <td>{{fsMTds.faMfaName}}</td>
	              <td>{{fsMTds.faMfaType}}</td>
	          </tr>
	      </table> 
    </div>
	
	
	
	
	<div id ="faCodeDB" ng-hide="faCodeDBFlag">
		  <input type="text" name="filterTextbox3" ng-model="filterTextbox3" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>FA Code</th>
 	  	 	  <th>Name</th>
 	  	 	  <th>Type</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="faMstr in faMList | filter:filterTextbox3">
		 	  <td><input type="radio"  name="faMstr"   value="{{ faMstr }}" ng-model="faMstrId" ng-click="saveFACode(faMstr)"></td>
              <td>{{faMstr.faMfaCode}}</td>
              <td>{{faMstr.faMfaName}}</td>
              <td>{{faMstr.faMfaType}}</td>
          </tr>
      </table> 
	</div> 
	
	
	<div id="chqVoucherDB" ng-hide="chqVoucherDBFlag">
		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="chqVoucherForm" ng-submit="submitCPVoucher(chqVoucherForm)">
		 <div class="row">
		 		<!-- <div class="col s6 input-field">
					<h6>Amount : </h6>
				</div>
			    <div class="col s6 input-field">
					<input class="validate" type ="number" name="amtName" id="amtId" ng-model="amt" ng-required="true">
				</div> -->
				<table>
					<tr>
						<td>AMOUNT</td>
						<td><input class="validate" type ="number" name="amtName" id="amtId" ng-model="amt" ng-required="true" step="0.01"  min="0.01"></td>
					</tr>
					<tr>
						<td>Branch Code</td>
						<td><input class="validate" type ="text" name="brName" id="brId" ng-model="brCode" ng-click="selectBrh()" disabled="disabled" readonly></td>
					</tr>
				</table>
	     </div>
	     <div class="row">
			    <div class="input-field col s12 center">
					<input class="validate" type ="submit" value="submit">
				</div>
	     </div>
	     </form>
	</div>
	
	
	<div id="custBrDB" ng-hide="custBrDBFlag">
		<!-- <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search by Branch FACode"> -->
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Branch FACode</th>
 	  	  </tr>
 	  		
 	  	  <tr ng-repeat="custBrFa in brFaList">
			  <td><input type="radio"  name="custBrFa"   value="{{ custBrFa }}" ng-model="custBrFaModel" ng-click="saveBrFA(custBrFa)"/></td>
	          <td>{{ custBrFa }}</td>
	      </tr>
		
       </table>   
	</div>
	
	
	<div id="saveVsDB" ng-hide="saveVsFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{vs.branch.branchFaCode}}</td>
				</tr>
				
				<tr>
					<td>Branch Name</td>
					<td>{{vs.branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Date</td>
					<td>{{dateTemp}}</td>
				</tr>
				
				<tr>
					<td>Sheet No</td>
					<td>{{vs.cashStmtStatus.cssCsNo}}</td>
				</tr>
				
				<tr>
					<td>Voucher Type</td>
					<td>{{vs.voucherType}}</td>
				</tr>
				
				<tr>
					<td>Payment By</td>
					<td>{{vs.payBy}}</td>
				</tr>
				
				<tr>
					<td>Cheque Type</td>
					<td>{{vs.chequeType}}</td>
				</tr>
				
				<tr>
					<td>Bank Code</td>
					<td>{{vs.bankCode}}</td>
				</tr>
				
				<tr>
					<td>Cheque No</td>
					<td>{{vs.chequeLeaves.chqLChqNo}}</td>
				</tr>
				
				<tr>
					<td>Pay To</td>
					<td>{{vs.payTo}}</td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td>{{vs.desc}}</td>
				</tr>
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" id="saveId" ng-click="saveVS()"/>
		</div>
	</div>
    
 </div>   
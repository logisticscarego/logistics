<div ng-show="operatorLogin || superAdminLogin">

<title>Branch</title>




<!-- Angular.js UI design started -->
		<div class="row">
		<form class="col s12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);"  name="BranchForm" ng-submit="SubmitBranch(BranchForm,branch)">
  	
  	 <div class="row">
      <div class="input-field col s3">
       <input class="validate" type ="text" name ="branchStationName" ng-model="brStationCode" readonly ng-click="openBranchStationCodeDB()" required>
        <input class="validate" type ="hidden" name ="branchStationCode" ng-model="branch.branchStationCode" >
        <label>Station</label>
      </div>
      <div class="input-field col s3">
        <input type ="hidden" ng-model="branch.isNCR" ng-init="branch.isNCR='no'">
        <input id="check" type="checkbox" ng-model="checkIsNCR" ng-change="saveisNCR()">
        <label>IsNCR</label>
      </div>
      <div class="input-field col s3">
       <input class="validate" type ="text" name ="stateGSTName" ng-model="stateGST"  ng-click="openStateGSTCodeDB()" ng-required="true" readonly>
        <label>State Code GST</label>
      </div>
            <div class="input-field col s3">
       <input class="validate" type ="text" name ="stateName" ng-model="stateName"  ng-click="openStateGSTCodeDB()" ng-required="true" readonly>
        <label>State Name</label>
      </div>
      
      <div class="input-field col s3">
        <input type ="hidden" name ="branchCode" ng-model="branch.branchCode">
      </div> 
    </div>
    
    <div class="row">
      <div class="input-field col s3">
        <input id="branchname" class="validate" type ="text" name ="branchName" ng-model="branch.branchName" ng-minlength="3" ng-maxlength="40" ng-required="true"  ng-pattern="/[a-zA-Z]/" >
        <label>Name</label>
      </div>
      <div class="input-field col s3">
        <input class="validate" id="branchAddress" type ="text" name ="branchAdd" ng-model="branch.branchAdd" ng-required="true" ng-minlength="3" ng-maxlength="255">
        <label>Address</label>
      </div>
      <div class="input-field col s3">
        <input id="branchcity" class="validate" type ="text" name ="branchCity" ng-model="branch.branchCity" ng-required="true" ng-minlength="3" ng-maxlength="40">
        <label>City</label>
      </div>
      <div class="input-field col s3">
        <input id="branchstate" class="validate" type ="text" name ="branchState" ng-model="branch.branchState" ng-required="true" ng-minlength="3" ng-maxlength="40">
        <label>State</label>
      </div>
    </div>
    
    <div class="row">
      <div class="input-field col s3">
        <input id="branchpin" class="validate" type ="text"  name ="branchPin" ng-model="branch.branchPin" ng-minlength="6" ng-maxlength="6" ng-required="true" >
        <label>Pin</label>
      </div>
      <div class="input-field col s3">
        <input type ="text" name="branchPhone" id ="branchPhone" ng-model="branch.branchPhone" ng-required="true"  ng-minlength="4" ng-maxlength="15">
        <label>Phone</label>
      </div>
      <div class="input-field col s3">
        <input type ="text" name="branchFax" id ="branchFax" ng-model="branch.branchFax" ng-minlength="8" ng-maxlength="20">
        <label>Fax</label>
      </div>
      <div class="input-field col s3">
        <input type ="email" name="branchEmailId" id ="branchEmailId" ng-model="branch.branchEmailId" ng-required="true" ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" ng-maxlength="40">
        <label>Email</label>
      </div>
     
    </div>
    
    <div class="row">
      
      <div class="input-field col s3">
        <input type ="text" id ="branchWebsite" ng-model="branch.branchWebsite" ng-minlength="3" ng-maxlength="255">
        <label>Website</label>
      </div>
      <div class="input-field col s3">
        	<input type ="date" name ="branchOpenDt" ng-model="branch.branchOpenDt" ng-required="true">
        	<label>Open Date</label>
      </div>
  				<div class="row">
      			 	<div class="col s12 center">
      			 		<input type="submit" value="Submit">
      			 	</div>
      		</div>
  </form>
</div>     



<div id ="branchStationCodeDB" ng-hide="branchStationCodeDBFlag">
 	  <input type="text" name="filterStnCode" ng-model="filterStnCode.stnName" placeholder="Search by Station Code">
 	  <table>
 	 		<tr>
 	 		    <th></th>				
				<th>Station Code</th>
				<th>Station Name</th>
			</tr>
 	  	  
		  <tr ng-repeat="station in stationList | filter:filterStnCode">
		 	  <td><input type="radio"  name="stationName"  class="stationCls"  value="{{ station }}" ng-model="brStationCode" ng-click="saveBranchStationCode(station)"></td>
              <td>{{station.stnCode}}</td>
              <td>{{station.stnName}}</td>
          </tr>
         </table>
         
	</div>

<div id ="stateCodeDB" ng-hide="stateCodeDBFlag">
 	  <input type="text" name="filterstateCode" ng-model="filterStateCode.stateName" placeholder="Search by State Name">
 	  <table>
 	 		<tr>
 	 		    <th></th>				
				<th>State GST Code</th>
				<th>State Name</th>
			</tr>
 	  	  
		  <tr ng-repeat="state in stateList | filter:filterStateCode">
		 	  <td><input type="radio"  name="stateName"  class="stateCls"  value="{{ state }}" ng-model="stateGST" ng-click="saveStateCode(state)"></td>
              <td>{{state.stateGST}}</td>
              <td>{{state.stateName}}</td>
          </tr>
         </table>
         
	</div>
	
	
	<div id ="branchDirectorDB" ng-hide="branchDirectorDBFlag">
 	 <input type="text" name="filterBrDirector" ng-model="filterBrDirector.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp </th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchDirector in employeeList | filter:filterBrDirector">
		 	  <td><input type="radio"  name="empBranchDirectorName" class="empBranchDirectorCls" value="{{ empBranchDirector }}" ng-model="brDirector" ng-click="saveBranchDirector(empBranchDirector)"></td>
              <td>{{ empBranchDirector.empCode }}</td>
              <td>{{ empBranchDirector.empName }}</td>
              <td>{{ empBranchDirector.branchCode }}</td>
              <td>{{ empBranchDirector.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchMarketingHDDB" ng-hide="branchMarketingHDDBFlag">
	<input type="text" name="filterBrMarketingHD" ng-model="filterBrMarketingHD.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp </th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchMarketingHD in employeeList | filter:filterBrMarketingHD">
		 	  <td><input type="radio"  name="empBranchMarketingHDName" class="empBranchMarketingHDCls" value="{{ empBranchMarketingHD }}" ng-model="brMarketingHD" ng-click="saveBranchMarketingHD(empBranchMarketingHD)"></td> 
              <td>{{ empBranchMarketingHD.empCode }}</td>
              <td>{{ empBranchMarketingHD.empName }}</td>
              <td>{{ empBranchMarketingHD.branchCode }}</td>
              <td>{{ empBranchMarketingHD.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchOutStandingHDDB" ng-hide="branchOutStandingHDDBFlag">
	<input type="text" name="filterBrOutStandingHD" ng-model="filterBrOutStandingHD.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp </th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchOutStandingHD in employeeList | filter:filterBrOutStandingHD">
		 	  <td><input type="radio"  name="empBranchOutStandingHDName" class="empBranchOutStandingHDCls" value="{{ empBranchOutStandingHD }}" ng-model="brOutStandingHD" ng-click="saveBranchOutStandingHD(empBranchOutStandingHD)"></td>
              <td>{{ empBranchOutStandingHD.empCode }}</td>
              <td>{{ empBranchOutStandingHD.empName }}</td>
              <td>{{ empBranchOutStandingHD.branchCode }}</td>
              <td>{{ empBranchOutStandingHD.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchMarketingDB" ng-hide="branchMarketingDBFlag">
	<input type="text" name="filterBrMarketing" ng-model="filterBrMarketing.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp </th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchMarketing in employeeList | filter:filterBrMarketing">
		 	  <td><input type="radio"  name="empBranchMarketingName" class="empBranchMarketingCls" value="{{ empBranchMarketing }}" ng-model="brMarketing" ng-click="saveBranchMarketing(empBranchMarketing)"></td>
              <td>{{ empBranchMarketing.empCode }}</td>
              <td>{{ empBranchMarketing.empName }}</td>
              <td>{{ empBranchMarketing.branchCode }}</td>
              <td>{{ empBranchMarketing.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchMngrDB" ng-hide="branchMngrDBFlag">
	<input type="text" name="filterBrMngr" ng-model="filterBrMngr.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp </th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchMngr in employeeList | filter:filterBrMngr">
		 	  <td><input type="radio"  name="empBranchMngrName" class="empBranchMngrCls" value="{{ empBranchMngr }}" ng-model="brMngr" ng-click="saveBranchMngr(empBranchMngr)"></td>
              <td>{{ empBranchMngr.empCode }}</td>
              <td>{{ empBranchMngr.empName }}</td>
              <td>{{ empBranchMngr.branchCode }}</td>
              <td>{{ empBranchMngr.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchCashierDB" ng-hide="branchCashierDBFlag">
	<input type="text" name="filterBrCashier" ng-model="filterBrCashier.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp </th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchCashier in employeeList | filter:filterBrCashier">
		 	  <td><input type="radio"  name="empBranchCashierName"  class="empBranchCashierCls" value="{{ empBranchCashier }}" ng-model="brCashier" ng-click="saveBranchCashier(empBranchCashier)"></td>
              <td>{{ empBranchCashier.empCode }}</td>
              <td>{{ empBranchCashier.empName }}</td>
              <td>{{ empBranchCashier.branchCode }}</td>
              <td>{{ empBranchCashier.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchTrafficDB" ng-hide="branchTrafficDBFlag">
	<input type="text" name="filterBrTraffic" ng-model="filterBrTraffic.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp </th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchTraffic in employeeList | filter:filterBrTraffic">
		 	  <td><input type="radio"  name="empBranchTrafficName" class="empBranchTrafficCls" value="{{ empBranchTraffic }}" ng-model="brTraffic" ng-click="saveBranchTraffic(empBranchTraffic)"></td>
              <td>{{ empBranchTraffic.empCode }}</td>
              <td>{{ empBranchTraffic.empName }}</td>
              <td>{{ empBranchTraffic.branchCode }}</td>
              <td>{{ empBranchTraffic.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchAreaMngrDB" ng-hide="branchAreaMngrDBFlag">
	<input type="text" name="filterBrAreaMngr" ng-model="filterBrAreaMngr.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp </th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchAreaMngr in employeeList | filter:filterBrAreaMngr">
		 	  <td><input type="radio"  name="empBranchAreaMngrName" class="empBranchAreaMngrCls" value="{{ empBranchAreaMngr }}" ng-model="brAreaMngr" ng-click="saveBranchAreaMngr(empBranchAreaMngr)"></td>
              <td>{{ empBranchAreaMngr.empCode }}</td>
              <td>{{ empBranchAreaMngr.empName }}</td>
              <td>{{ empBranchAreaMngr.branchCode }}</td>
              <td>{{ empBranchAreaMngr.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchRegionalMngrDB" ng-hide="branchRegionalMngrDBFlag">
	<input type="text" name="filterBrRegionalMngr" ng-model="filterBrRegionalMngr.empCodeTemp" placeholder="Search by Employee Code Temp">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
				
				<th>Branch Code </th>
				
				<th>Employee Code Temp </th>
				
			</tr>
 	 		  
		  <tr ng-repeat="empBranchRegionalMngr in employeeList | filter:filterBrRegionalMngr">
		 	  <td><input type="radio"  name="empBranchRegionalMngrName" class="empBranchRegionalMngrCls" value="{{ empBranchRegionalMngr }}" ng-model="brRegionalMngr" ng-click="saveBranchRegionalMngr(empBranchRegionalMngr)"></td>
              <td>{{ empBranchRegionalMngr.empCode }}</td>
              <td>{{ empBranchRegionalMngr.empName }}</td>
              <td>{{ empBranchRegionalMngr.branchCode }}</td>
              <td>{{ empBranchRegionalMngr.empCodeTemp }}</td>
          </tr>
         </table>
         
	</div>
	
<div id="EmployeeDB" ng-hide="EmployeeDBFlag" style="margin-top: 40px;">  

<form class="grey-text" name="EmployeeForm" ng-submit="submitEmployee(EmployeeForm,employee)" >
				 <input type ="hidden" name ="userCode" ng-model="employee.userCode">
				 <input type ="hidden" name ="empCode" ng-model="employee.empCode">
			<div class="row" style="margin-top: 40px;">
      				<div class="input-field col s3">
        				<input type="text" name="branchCode" ng-model="employee.branchCode" readonly="true" required autofocus>
        				<label>Branch Code</label>
        				<input type="hidden" ng-model="employee.isInNCR">
        			</div>
        			
      		</div>	
      		
      		<div class="row">
      				<div class="input-field col s4">
        				<input class="validate " id="empFirstName" type ="text" name ="empFirstName" ng-model="employee.empfirstname" ng-required="true" ng-minlength="3" ng-maxlength="40">
        				<label for="empname">Employee's First Name</label>
        			</div>
      				<div class="input-field col s4">
        				<input class="validate " id="empMiddleName" type ="text" name ="empMiddleName" ng-model="employee.empmiddlename" ng-minlength="3" ng-maxlength="40">
        				<label for="empname">Employee's Middle Name</label>
        			</div>
      				<div class="input-field col s4">
        				<input class="validate " id="empLastName" type ="text" name ="empLastName" ng-model="employee.emplastname" ng-required="true" ng-minlength="3" ng-maxlength="40">
        				<label for="empname">Employee's Last Name</label>
        			</div>
        	</div>
        	<div class="row">
        			<div class="input-field col s6">	
        				<input class="validate " id="empDesignation" type ="text" name ="empDesignation" ng-model="employee.empDesignation" ng-required="true" ng-minlength="2" ng-maxlength="40">
        				<label for="empDesignation">Designation</label>
      				</div>
      				<div class="input-field col s6">	
        					<input id="empEduQualification" class="validate" type ="text" name ="empEduQuali" ng-model="employee.empEduQuali" ng-required="true" ng-minlength="2" ng-maxlength="255">
        					<label for="empEduQualification">Qualification</label>
      				</div>
      		</div>
        	<div class="row">
        			<div class="input-field col s4">
        				<select name="empSex" ng-model="employee.empSex" required>
                    		<option value="Male">Male</option>
                    		<option value="Female">Female</option>   
                     	</select>
                     	<label>Emp. Sex</label>
        			</div>
        			<div class="input-field col s4">
        					<input  type ="date" name ="empDOB" ng-model="employee.empDOB" ng-required="true">
        					<label>Date Of Birth</label>
      				</div>      				
      				<div class="input-field col s4">      						
        					<input type ="date" name ="empDOAppointment" ng-model="employee.empDOAppointment" ng-required="true">
        					<label>Date of Appt</label>
      					
      				</div>
      		</div>
      		<div class="row">
      				<div class="input-field col s6">
        				<input id="empFatherFirstName" type ="text" name ="empFatherFirstName" ng-model="employee.fatherFirstName"  ng-required="true" ng-minlength="3" ng-maxlength="40">
        				<label>Father's First Name</label>
        			</div>
      				<div class="input-field col s6">
        				<input id="empFatherLastName" name="empFatherLastName" type ="text" ng-required="true" ng-model="employee.fatherLastName" ng-minlength="3" ng-maxlength="40">
        				<label>Father's Last Name</label>
        			</div>
      		</div>
        	<div class="row">
        			<div class="input-field col s6">
        				<input id="empNomineeFirstName" class="validate " type ="text" name ="empNomineeFirstName" ng-model="employee.nomineeFirstName" ng-required="true" ng-minlength="3" ng-maxlength="40">
        				<label>Nominee's First Name</label>
        			</div>
      				<div class="input-field col s6">
        				<input id="empNomineeLastName" type ="text" name="empNomineeLastName" ng-required="true" ng-model="employee.nomineeLastName" ng-minlength="3" ng-maxlength="40">
        				<label>Nominee's Last Name</label>
        			</div>
      		</div>
      		<div class="row">
      				<div class="input-field col s12">
        				<input class="validate" id="empAddress" type ="text" name ="empAdd" ng-model="employee.empAdd" ng-required="true" ng-minlength="3" ng-maxlength="255">
        				<label for="empAddress">Address</label>
        			</div>
        	</div>
      		<div class="row">
      				<div class="input-field col s4">	
        					<input id="empCity" type ="text" name ="empCity" ng-model="employee.empCity" ng-required="true" ng-minlength="3" ng-maxlength="40">
        					<label for="empCity">City</label>
      				</div>
      				
      				<div class="input-field col s4">	
        					<input id="empState" type ="text" name ="empState" ng-model="employee.empState" ng-required="true" ng-minlength="3" ng-maxlength="40">
        					<label for="empState">State</label>
      				</div>
	      			<div class="input-field col s4">	
        					<input id="empPin" type ="text" name ="empPin" ng-model="employee.empPin" ng-required="true" ng-minlength="6" ng-maxlength="6" ng-blur="chkPresentAddrDB()" >
        					<label for="empPin">Pin</label>
    	  			</div>
      				
      		</div>      		
      		<div class="row">
      				<div class="input-field col s12">
        				<input class="validate " id="empPresentAdd" type ="text" name ="empPresentAdd" ng-model="employee.empPresentAdd" ng-required="true" ng-minlength="3" ng-maxlength="255" >
        				<label for="empPresentAdd">Present Address</label>
        			</div>
      		</div>      		
      		<div class="row">
        			<div class="input-field col s4">	
        					<input class="validate" id="empPresentCity" type ="text" name ="empPresentCity" ng-model="employee.empPresentCity" ng-required="true" ng-minlength="3" ng-maxlength="40">
        					<label for="empPresentCity">Present City</label>
      				</div>
      				
      				<div class="input-field col s4">	
        					<input class="validate" id="empPresentState" type ="text" name ="empPresentState" ng-model="employee.empPresentState" ng-required="true" ng-minlength="3" ng-maxlength="40">
        					<label for="empPresentState">Present State</label>
      				</div>
      				<div class="input-field col s4">	
        					<input class="validate" id="empPresentPin" type ="text" name ="empPresentPin" ng-model="employee.empPresentPin" ng-required="true" ng-minlength="6" ng-maxlength="6" >
        					<label for="empPresentPin">Present Pin</label>
      				</div>      				
      		</div>
      		
      		
      		
      		
      		
      		<div class="row">
      				<div class="input-field col s4">
        				<input class="validate "  id="empBasic"  type ="number" name ="empBasic" ng-model="employee.empBasic" ng-keyup="calculateOnBasic()" ng-required="true"  step="0.01" min="0.00">
        				<label>Basic</label>
        			</div>
        			<div class="input-field col s4">	
        				<input class="validate" type ="text" name ="empHRA" ng-model="employee.empHRA" readonly >
        				<label>HRA</label>
      				</div>
      				<div class="input-field col s4">	
        				<input class="validate" id="empOtherAllowance" type ="number" name ="empOtherAllowance" ng-model="employee.empOtherAllowance" ng-init="employee.empOtherAllowance=0.00" ng-keyup="calculateOnOthers(employee.empOtherAllowance)"   ng-required="true"  step="0.01" min="0.00" >
        				<label>Other Allowance</label>
      				</div>

      		</div>
      		<div class="row">
      			<div class="input-field col s6">	
        				<input class="validate" type ="text" name ="empPF" ng-model="employee.empPF" readonly >
        				<label>PF</label>
      			</div>
      			<div class="input-field col s6">	
        				<input class="validate" id="empPFNo" type ="text" name ="empPFNo" ng-model="employee.empPFNo" ng-required="true" ng-minlength="8" ng-maxlength="18">
        				<label>PF Number</label>	
      			</div>
      		</div>
      		
      		<div class="row">
      				<div class="input-field col s4">
        				<input class="validate "  type ="text" id="empESI" name ="empESI" ng-model="employee.empESI" readonly>
        				<label for="empESI">ESI</label>
        			</div>
        			<div class="input-field col s4">	
        					<input class="validate" id="empESINo" type ="text" name ="empESINo" ng-model="employee.empESINo" ng-minlength="3" ng-maxlength="40">
        					<label for="empESINo">ESI Number</label>
      				</div>
      				
      				<div class="input-field col s4">	
        					<input class="validate" id="empESIDispensary" type ="text" name ="empESIDispensary" ng-model="employee.empESIDispensary" ng-minlength="3" ng-maxlength="40">
        					<label for="empESIDispensary">ESI Dispensary</label>
      				</div>
      		</div>
      		<div class="row">
      			<div class="input-field col s6">
        				<input id="empGross" type ="text" name ="empGross" ng-model="employee.empGross" readonly>
        				<label>Gross Salary</label>
        		</div>
      			<div class="input-field col s6">	
        				<input id="netSalary" type ="text" name ="netSalary" ng-model="employee.netSalary" readonly >	
        				<label>Net Salary</label>
      			</div>
      		</div>
      		
      		<div class="row">
      				<div class="input-field col s4">
        				<input class="validate" id="empBankBranch" type ="text" name ="empBankBranch" ng-model="employee.empBankBranch" ng-minlength="3" ng-maxlength="40">
        				<label for="empBankBranch">Bank Branch</label>
        			</div>
        			<div class="input-field col s4">	
        					<input class="validate" id="empBankIFS" type ="text" name ="empBankIFS" ng-model="employee.empBankIFS"  ng-minlength="3" ng-maxlength="40">
        					<label for="empBankIFS">IFSC</label>
      				</div>
      				<div class="input-field col s4">
        				<input class="validate" id="empPanNo" type ="text" name ="empPanNo" ng-model="employee.empPanNo" ng-required="true" ng-minlength="10" ng-maxlength="10" ng-pattern="/^[a-zA-Z0-9]*$/">
        				<label for="empPanNo">PAN No.</label>
        			</div>
      		</div>
      		
      		<div class="row">
      				<div class="input-field col s6">
        				<input class="validate" id="empBankAcNo" type ="number" name ="empBankAcNo" ng-model="employee.empBankAcNo" ng-required="true" ng-minlength="10" ng-maxlength="40" min="0">
        				<label for="empBankAcNo">Bank A/c number</label>
        			</div>
        			<div class="input-field col s6">	
        					<input class="validate" id="empBankName" type ="text" name ="empBankName" ng-model="employee.empBankName" ng-minlength="3" ng-maxlength="40">
        					<label for="empBankName">Bank Name</label>
      				</div>
      		</div>
      		
      		<div class="row">
      				
        			<div class="input-field col s6">	
        					<input class="validate" id="empLoanBal" type ="number" name ="empLoanBal" ng-model="employee.empLoanBal" ng-required="true"  step="0.01" min="0.00">
        					<label for="empLoanBal">Loan Balance</label>
      				</div>
      				
      				<div class="input-field col s6">	
        					<input class="validate" id="empLoanPayment" type ="number" name ="empLoanPayment" ng-model="employee.empLoanPayment" ng-required="true"  step="0.01" min="0.00">
        					<label for="empLoanPayment">Loan Payment</label>
      				</div>
      		</div>
      		
      		<div class="row">
      			<div class="input-field col s6">
        				<input class="validate" id="empLicenseNo" type ="text" name ="empLicenseNo" ng-model="employee.empLicenseNo" ng-required="true" ng-minlength="3" ng-maxlength="40">
        				<label for="empLicenseNo">Licence No.</label>
        		</div>
        		<div class="input-field col s6">	
        					<input class="validate" type ="date" name ="empLicenseExp" ng-model="employee.empLicenseExp" ng-required="true">
        					<label for="empLicenseExp">Licence Exp.</label>
      			</div>
      		</div>
      		
      		<div class="row">
      				<div class="input-field col s6">	
        					<input class="validate" id="empMailId" type ="email" name ="empMailId" ng-model="employee.empMailId" ng-required="true" ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" ng-maxlength="40">
        					<label for="empMailId">Mail ID</label>
      				</div>
      				
      				<div class="input-field col s6">	
        					<input class="validate" id="empPhNo" type ="text" name ="empPhNo" ng-model="employee.empPhNo" ng-minlength="4" ng-maxlength="15" min="0">
        					<label for="empPhNo">Phone</label>
      				</div>
      		</div>
      		<div class="row">		
      				<div class="input-field col s6">	
        					<input class="validate" id="empTelephoneAmt" type ="number" name ="empTelephoneAmt" ng-model="employee.empTelephoneAmt" step="0.01" min="0.00">
        					<label for="empTelephoneAmt">Telephone Amt.</label>
      				</div>
      				
      				<div class="input-field col s6">	
        				<input class="validate" id="empMobAmt" type ="number" name ="empMobAmt" ng-model="employee.empMobAmt" step="0.01" min="0.00">
        				<label for="empMobAmt">Mobile Amt.</label>	
      				</div>
      		</div>
      		
			
			<div class="row">
				<div class="col s12 center">
      			 		<input class="btn waves-effect waves-light" type="submit" value="Submit">
      			</div>
      		</div>
			
		</form>  
		

<div id="presentAddrDB" ng-hide="presentAddrDBFlag">
    	<div class="row">
    	<div class="col s12 center"  style="margin: 20px 0px;"><span class="white-text">Is present address same as correspondence address?</span></div>
    	<div class="col s6 center"><a class="btn white-text" ng-click="yesPresentAddr()">Yes</a></div>
   	    <div class="col s6 center"><a class="btn white-text" ng-click="YesNoAddr()">No</a></div>
   	    </div>
</div>

 <div id="viewEmpDetailsDB" ng-hide="viewEmpDetailsFlag">
    
   	<div class="row">
	
	<div ng-hide = "show" class="col s12 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
				<h4>Here's the details of Employee <span class="teal-text text-lighten-2">{{employee.empName}}</span>:</h4>
		<table class="table-bordered table-hover table-condensed">
	            
	            <tr>
	                <td>Branch Code:</td>
	                <td>{{employee.branchCode}}</td>
	            </tr>
	            
	            <tr>
	                <td>Name:</td>
	                <td>{{employee.empName}}</td>
	            </tr>
	            
	            <tr>
	                <td>Address:</td>
	                <td>{{employee.empAdd}}</td>
	            </tr>
	            <tr>
	                <td>City:</td>
	                <td>{{employee.empCity}}</td>
	            </tr>
	            <tr>
	                <td>State:</td>
	                <td>{{employee.empState}}</td>
	            </tr>
	            <tr>
	                <td>Pin:</td>
	                <td>{{employee.empPin}}</td>
	            </tr>
	            <tr>
	                <td>Date Of Birth:</td>
	                <td>{{employee.empDOB}}</td>
	            </tr>
	            <tr>
	                <td>Date Of Appointment:</td>
	                <td>{{employee.empDOAppointment}}</td>
	            </tr>
	            <tr>
	                <td>Designation:</td>
	                <td>{{employee.empDesignation}}</td>
	            </tr>
	            <tr>
	                <td>Basic:</td>
	                <td>{{employee.empBasic}}</td>
	                
	            </tr>
	            <tr>
	                <td>HRA:</td>
	                <td>{{employee.empHRA}}</td>
	            </tr>
	            <tr>
	                <td>Other Allowance:</td>
	                <td>{{employee.empOtherAllowance}}</td>
	            </tr>
	            <tr>
	                <td>PF No:</td>
	                <td>{{employee.empPFNo}}</td>
	            </tr>
	            <tr>
	                <td>ESI No:</td>
	                <td>{{employee.empESINo}}</td>
	            </tr>
	            <tr>
	                <td>ESI Dispensary:</td>
	                <td>{{employee.empESIDispensary}}</td>
	            </tr>
	            <tr>
	                <td>Nominee:</td>
	                <td>{{employee.empNominee}}</td>
	            </tr>
	            <tr>
	                <td>Educational Qualification:</td>
	                <td>{{employee.empEduQuali}}</td>
	            </tr>
	            <tr>
	                <td>PF:</td>
	                <td>{{employee.empPF}}</td>
	            </tr>
	            <tr>
	                <td>ESI:</td>
	                <td>{{employee.empESI}}</td>
	            </tr>
	            <tr>
	                <td>Loan Balance:</td>
	                <td>{{employee.empLoanBal}}</td>
	            </tr>
	            
	            <tr>
	                <td>Loan Payment:</td>
	                <td>{{employee.empLoanPayment}}</td>
	            </tr>
	            
	            <tr>
	                <td>Present Address:</td>
	                <td>{{employee.empPresentAdd}}</td>
	            </tr>
	            
	            <tr>
	                <td>City:</td>
	                <td>{{employee.empPresentCity}}</td>
	            </tr>
	            
	            <tr>
	                <td>State:</td>
	                <td>{{employee.empPresentState}}</td>
	            </tr>
	            
	            <tr>
	                <td>Present Pin:</td>
	                <td>{{employee.empPresentPin}}</td>
	            </tr>
	            
	            <tr>
	                <td>Sex:</td>
	                <td>{{employee.empSex}}</td>
	            </tr>
	            
	            <tr>
	                <td>Father Name:</td>
	                <td>{{employee.empFatherName}}</td>
	            </tr>
	            
	            <tr>
	                <td>License No:</td>
	                <td>{{employee.empLicenseNo}}</td>
	            </tr>
	            
	            <tr>
	                <td>License Exp:</td>
	                <td>{{employee.empLicenseExp}}</td>
	            </tr>
	            
	             <tr>
	                <td>Telephone Amount:</td>
	                <td>{{employee.empTelephoneAmt}}</td>
	            </tr>
	            
	             <tr>
	                <td>Mobile Amount:</td>
	                <td>{{employee.empMobAmt}}</td>
	            </tr>
	            
	             <tr>
	                <td>Bank Account No:</td>
	                <td>{{employee.empBankAcNo}}</td>
	            </tr>
	            
	             <tr>
	                <td>Pan No:</td>
	                <td>{{employee.empPanNo}}</td>
	            </tr>
	            
	             <tr>
	                <td>Mail Id:</td>
	                <td>{{employee.empMailId}}</td>
	            </tr>
	            
	             <tr>
	                <td>Phone No:</td>
	                <td>{{employee.empPhNo}}</td>
	            </tr>
	            
	             <tr>
	                <td>Bank IFSC:</td>
	                <td>{{employee.empBankIFS}}</td>
	            </tr>
	            
	             <tr>
	                <td>Bank Name:</td>
	                <td>{{employee.empBankName}}</td>
	            </tr>
	            
	             <tr>
	                <td>Bank Branch:</td>
	                <td>{{employee.empBankBranch}}</td>
	            </tr>
</table>

<input type="button" value="Save" ng-click="saveEmployee(employee)">
<input type="button" value="Cancel" ng-click="closeViewEmpDetailsDB()">
		
		
</div>
</div>
</div>

<div id="viewBranchDetailsDB" ng-hide="viewBranchDetailsFlag">
	<div class="row">
	<div class="col s12 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
		<h4>Here's the details of <span class="teal-text text-lighten-2">{{branch.branchName}}</span> branch:</h4>
		<table class="table-hover table-bordered table-condensed">
				
				<tr>
	                <td>Name:</td>
	                <td>{{branch.branchName}}</td>
	            </tr>
	            
	            <tr>
	                <td>IsNCR:</td>
	                <td>{{branch.isNCR}}</td>
	            </tr>
	            
	            <tr>
	                <td>Address:</td>
	                <td>{{branch.branchAdd}}</td>
	            </tr>
	            <tr>
	                <td>City:</td>
	                <td>{{branch.branchCity}}</td>
	            </tr>
	            <tr>
	                <td>State:</td>
	                <td>{{branch.branchState}}</td>
	            </tr>
	            <tr>
	                <td>Pin:</td>
	                <td>{{branch.branchPin}}</td>
	            </tr>
	            <tr>
	                <td>Open Date:</td>
	                <td>{{branch.branchOpenDt}}</td>
	            </tr>
	            <tr>
	                <td>Station Code:</td>
	                <td>{{branch.branchStationCode}}</td>
	            </tr>
	            <tr>
	                <td>State Code:</td>
	                <td>{{branch.branchStateGST}}</td>
	            </tr>
	         <!--    <tr>
	                <td>Director:</td>
	                <td>{{branch.branchDirector}}</td>
	                
	            </tr>
	            <tr>
	                <td>Marketing HD:</td>
	                <td>{{branch.branchMarketingHD}}</td>
	            </tr>
	            <tr>
	                <td>Outstanding HD:</td>
	                <td>{{branch.branchOutStandingHD}}</td>
	            </tr>
	            <tr>
	                <td>Marketing:</td>
	                <td>{{branch.branchMarketing}}</td>
	            </tr>
	            <tr>
	                <td>Manager:</td>
	                <td>{{branch.branchMngr}}</td>
	            </tr>
	            <tr>
	                <td>Cashier:</td>
	                <td>{{branch.branchCashier}}</td>
	            </tr>
	            <tr>
	                <td>Traffic:</td>
	                <td>{{branch.branchTraffic}}</td>
	            </tr>
	            <tr>
	                <td>Area Manager:</td>
	                <td>{{branch.branchAreaMngr}}</td>
	            </tr>
	            <tr>
	                <td>Regional Manager:</td>
	                <td>{{branch.branchRegionalMngr}}</td>
	            </tr> -->
	            <tr>
	                <td>Phone:</td>
	                <td>{{branch.branchPhone}}</td>
	            </tr>
	            <tr>
	                <td>Fax:</td>
	                <td>{{branch.branchFax}}</td>
	            </tr>
	            
	            <tr>
	                <td>Email Id:</td>
	                <td>{{branch.branchEmailId}}</td>
	            </tr>
	            
	            <tr>
	                <td>Website:</td>
	                <td>{{branch.branchWebsite}}</td>
	            </tr>
	          
	   </table>
	   
<input type="button" value="Save" id="saveId" ng-click="saveBranch(branch)">
<input type="button" value="Cancel" ng-click="closeViewBranchDetailsDB()">
		
</div>	

</div>

</div>
 
</div>		

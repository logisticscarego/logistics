<div ng-show="operatorLogin || superAdminLogin">
<title>Add State</title>
<div class="row">
	<div class="col s2 hide-on-med-and-down"> &nbsp; </div>
		<form class="col s8 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="StateForm" ng-submit="Submit(StateForm,state,addstateLryPrefix1,addstateLryPrefix2,addstateLryPrefix3)">
			<div class="row">
      			<div class="input-field col s6">
        			<input class="validate" id="stateName" type ="text" name ="stateName" ng-model="state.stateName" ng-required="true" ng-minlength="3" ng-maxlength="40">
        			<label for="StateName">State Name</label>	
      			</div>
      			<div class="row">
      			 	<div class="input-field col s6">
      					<input class="validate" id="std" type ="text" name ="stateSTD" ng-model="state.stateSTD" ng-minlength="2" ng-maxlength="5">
      					<label for="std">State STD</label>
      			 	</div>
      			 </div>
      			</div>
      			 
      			 <div class="row">
      					
      				<div class="input-field col s4">
      					<input class="validate" type ="text" id="addstateLryPrefix1" name ="addstateLryPrefix1" ng-model="addstateLryPrefix1" ng-required="true" ng-minlength="2" >
      					<label>State Lorry Prefix</label>
      			 	</div>
      			 	<div class="input-field col s4">
      					<input class="validate" type ="text" id="addstateLryPrefix2" name ="addstateLryPrefix2" ng-model="addstateLryPrefix2" ng-minlength="2">
      			 	</div>
      			 	<div class="input-field col s4">
      					<input class="validate" type ="text" name ="addstateLryPrefix3" id="addstateLryPrefix3" ng-model="addstateLryPrefix3" ng-minlength="2" >
      			 	</div>
      	        </div>
      			<div class="row">
      			 	<div class="input-field col s12 center">
      			 		<input class="btn waves-effect waves-light" type="submit" value="Submit">
      			 	</div>
      			</div>
			</form>
			
		<div class="col s2">&nbsp;</div>
</div>

<div id="viewStateDB" ng-hide="viewStateFlag">

<div class="row">
	
	<div ng-hide = "show" class="col s12 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
				<h4>Here's the details of State <span class="teal-text text-lighten-2">{{state.stateName}}</span>:</h4>
		<table class="table-bordered table-hover table-condensed">
	            
	            <tr>
	                <td>Name:</td>
	                <td>{{state.stateName}}</td>
	            </tr>
	            
	            <tr>
	                <td>STD:</td>
	                <td>{{state.stateSTD}}</td>
	            </tr>
	            
	            <tr>
	                <td>Lorry Prefix:</td>
	                <td>{{state.stateLryPrefix}}</td>
	            </tr>
	            
	     </table>
	      
<input type="button" value="Save" ng-click="saveState(state)">
<input type="button" value="Cancel" ng-click="closeStateDB()">
	      
	  </div>
	</div>         
</div>



<!-- <form name="StateForm" ng-submit="Submit(StateForm,state,addstateLryPrefix1,addstateLryPrefix2,addstateLryPrefix3)">
<table>
     
     <tr>
		<td>State Name: *</td> 
		<td><input type ="text" name ="stateName" ng-model="state.stateName" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
	</tr>
	
	 <tr>
		<td>State Lorry Prefix: *</td> 
			<td><input type ="text" name ="addstateLryPrefix1" ng-model="addstateLryPrefix1" ng-required="true" ng-maxlength="3" >
			/<input type ="text" name ="addstateLryPrefix2"  ng-model="addstateLryPrefix2" ng-maxlength="3">
			/<input type ="text" name ="addstateLryPrefix3"  ng-model="addstateLryPrefix3" ng-maxlength="3" ></td>
	</tr>  
	
	<tr>
		<td>State STD: *</td> 
		<td><input type ="number" name ="stateSTD" ng-model="state.stateSTD" ng-required="true" ng-minlength="2" ng-maxlength="5" min="0"></td>
	</tr>
	
	
	<tr>
		<td><input type="submit" value="Submit"></td>
	</tr> 
	
	
</table>

</form> -->

</body>
</html>
</div>
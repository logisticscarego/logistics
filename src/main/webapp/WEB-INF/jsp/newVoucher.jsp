<style type="text/css">
            .printable {display: none;}
            @media print {
                .noprint, .menuwrapper, .ui-dialog, footer, #toast-container {display: none !important;}
                .printable {display: block;
                		width: 10in !important;
                		position: fixed;
                		top: 0px;
                		left: 0px;
                		z-index: 999999;
                		border: 1px solid black;
                		}
            }
</style>
<div ng-show="operatorLogin || superAdminLogin">
	<div class="row noprint">
		<form name="newVoucherForm" ng-submit=voucherSubmit(newVoucherForm) class="col s12 card" style="align: center; padding-top:40px;background-color: #F2FAEF;"
		 ng-class = "{'form-error': newVoucherForm.$invalid && isFormSubmitted}" novalidate = "novalidate">
			<div class="row">
		     		<div class="col s2 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code" style="color:black;">Branch Code</label>	
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.branchFaCode.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Branch code is required.</span>
							</div>	
		       		</div>

		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code" style="color:black;">Branch Name</label>
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.branchName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Branch name is required.</span>
							</div>		
		       		</div>
		       		
		       			<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly ng-required = "true" >
		       			<label for="code" style="color:black;">Date</label>
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.dateTemp.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Date is required.</span>
							</div>		
		       		</div>
		       		<div class="col s1 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.cashStmtStatus.cssCsNo" readonly ng-required="true" >
		       			<label for="code" style="color:black;">Sheet No</label>	
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.sheetNo.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Sheet No. is required.</span>
							</div>
		       		</div>
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code" style="color:black;">Voucher Type</label>
		       				<div class = "text-left errorMargin" ng-messages = "newVoucherForm.voucherType.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Voucher type is required.</span>
							</div>
		       		</div>
		    </div>
		    <div class="row">
			      	<div class="col s3 input-field">
			    		<select name="chequeType" id="chequeTypeId" ng-model="vs.chequeType" ng-init="vs.chequeType = 'C'" ng-required="true"  ng-change="selectChqType()">
							<option value='C'>Computerized</option>
							<option value='M'>Manual</option>
						</select>
						<label style="color:black;">Cheque Type </label>
						<div class = "text-left errorMargin" ng-messages = "newVoucherForm.chequeType.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Cheque type is required.</span>
							</div>
					</div>
					<div class="col s2 input-field">
		       				<input class="validate" type ="text" id="bankCodeId" name ="bankCode" ng-model="vs.bankCode"  ng-click = " openBankCodeDB()" readonly ng-required="true" >
		       			<label for="code" style="color:black;">Bank Code</label>	
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.bankCode.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Bank code is required.</span>
							</div>
		       		</div>
		       		<div class="col s2 input-field">
		       				<input class="validate" type ="text" id="chequeNoId" name ="chequeNo" ng-model="vs.chequeLeaves.chqLChqNo" readonly disabled="disabled" ng-click="openChqNoDB()">
		       			<label for="code" style="color:black;">Cheque No</label>	
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.chequeNo.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Cheque is required.</span>
							</div>
		       		</div>
		       		<div class="col s2 input-field">
		       				<input class="validate" type ="number" id="amountId" name ="amount" ng-model="vs.amount" step = "0.01"  ng-blur="checkAmount()" ng-required="true" >
		       			<label for="code" style="color:black;">Amount</label>	
		       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.amount.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Amount is required.</span>
							</div>
		       		</div>
		       		<div class="col s3 input-field">
	       				<input type ="text" id="payToId" name ="payToName" ng-model="vs.payTo"  ng-required="true" 
	       					ng-minlength = "2" ng-maxlength = "200">
	       			<label for="code" style="color:black;">Pay To</label>
	       			<div class = "text-left errorMargin" ng-messages = "newVoucherForm.payToName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Pay to is required.</span>
								<span ng-message = "minlength">*  Pay to should be greater than 1.</span>
								<span ng-message = "maxlength">*  Pay to should be less than 201.</span>
							</div>	
	       		</div>
		    </div>
	   	
	   		 <div class="row">
				<div class="input-field col s12">
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="vs.desc" ng-required="true"
 						ng-minlength = "2" ng-maxlength = "200"></textarea>
 					<label style="color:black;">Description</label>
 					<div class = "text-left errorMargin" ng-messages = "newVoucherForm.desc.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Description is required.</span>
								<span ng-message = "minlength">*  Description should be greater than 1.</span>
								<span ng-message = "maxlength">*  Description should be less than 201.</span>
							</div>
				</div>
    		 </div>	
	   		 
	   		  <div class="row">
	     		<div class="col s4 input-field">
	       				<input class="validate" type ="submit" value="submit"  id= "saveId">
	       		</div>
	   		 </div>
		  </form>
    </div>
    
    <div id ="bankCodeDB" ng-hide="bankCodeDBFlag" class="noprint">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox.bankCode" placeholder="Search by Bank Code ">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Bank Code </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bnkCode in bankCodeList | filter:filterTextbox.bankCode ">
		 	  <td><input type="radio"  name="bnkCode"   value="{{ bnkCode }}" ng-model="bnkCde" ng-click="saveBankCode(bnkCode)"></td>
              <td>{{bnkCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	
	<div id ="chqNoDB" ng-hide="chqNoDBFlag" class="noprint">
		  <input type="text" name="filterChqbox" ng-model="filterChqbox" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Cheque No</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="chq in chqList | filter:filterChqbox ">
		 	  <td><input type="radio"  name="chq.chqLChqNo"   value="{{ chq.chqLChqNo }}" ng-model="chqCode" ng-click="saveChqNo(chq)"></td>
              <td>{{chq.chqLChqNo}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id="saveVsDB" ng-hide="saveVsFlag" class="noprint"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{vs.branch.branchFaCode}}</td>
				</tr>
				
				<tr>
					<td>Branch Name</td>
					<td>{{vs.branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Date</td>
					<td>{{dateTemp}}</td>
				</tr>
				
				<tr>
					<td>Sheet No</td>
					<td>{{vs.cashStmtStatus.cssCsNo}}</td>
				</tr>
				
				<tr>
					<td>Voucher Type</td>
					<td>{{vs.voucherType}}</td>
				</tr>
				
				<tr>
					<td>Cheque Type</td>
					<td>{{vs.chequeType}}</td>
				</tr>
				
				<tr>
					<td>Bank Code</td>
					<td>{{vs.bankCode}}</td>
				</tr>
				
				<tr>
					<td>Cheque No</td>
					<td>{{vs.chequeLeaves.chqLChqNo}}</td>
				</tr>
				
				<tr>
					<td>Amount</td>
					<td>{{vs.amount}}</td>
				</tr>
				
				<tr>
					<td>Pay To</td>
					<td>{{vs.payTo}}</td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td>{{vs.desc}}</td>
				</tr>
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" ng-click="saveVS()"/>
		</div>
	</div>
		
	<div id="printVsDB" ng-hide="printVsFlag" class="noprint">
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">{{PRINT_STMT}}</h5></div>
		</div>
		<div>
			<div class="col s12 center">
				<a class="btn white-text" ng-click="cancelPrint()">Cancel</a>
				<a class="btn white-text" ng-click="printVs()">Yes</a>
			</div>
		</div>
	</div>
	
	
	
	<div class="printable"> 
		<div class="col s12 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<div class="container" style="width: 100% !important;">
				<div class="row">
					<div class="col s8">
						<div class="row">
							<div class="col s12">{{cmpnyName}}<br/>
												Voucher Type: {{vs.voucherType}}<br/>
												Cheque Type: {{vs.chequeType}}<br/>
												<br/>
												Pay to: {{vs.payTo}}</div>
						</div>
					</div>
					<div class="col s4">
						<div class="row">
							<div class="col s12">Branch Code:{{vs.branch.branchFaCode}}<br/>
												Sheet No: {{vs.cashStmtStatus.cssCsNo}}<br/>
												Date: {{dateTemp}}<br/>
												Voucher No: {{vs.vhNo}}<br/>
												Cheque No: {{vs.chequeLeaves.chqLChqNo}}</div>
						</div>						
					</div>
				</div>
				<table style="width: 100% !important;">
					<tr style="border-top: 1px solid black; border-bottom: 1px solid black;">
						<!-- <td style="width: 1in;">BCD</td> -->
						<td style="width: 1in;">BRH NAME</td>
						<td style="width: 1in;">BANK CODE</td>
						<td style="width: 1in;">BANK NAME</td>
						<td style="width: 1in;">TVNO</td>
						<td style="width: 1.5in;">DEBIT AMT</td>
						<td style="width: 1.5in;">CREDIT AMT</td>
						<td style="width: 3in;">DESCRIPTION</td>
					</tr>
					<tr style="border-bottom: 1px solid black;">
						<!-- <td style="width: 1in;">{{vs.branch.branchFaCode}}</td> -->
						<td style="width: 1in;">{{vs.branch.branchName}}</td>
						<td style="width: 1in;">{{vs.bankCode}}</td>
						<td style="width: 1in;">{{vs.bnkName}}</td>
						<td style="width: 1in;">{{vs.tvNo}}</td>
						<td style="width: 1.5in;"> </td>
						<td style="width: 1.5in; text-align: center;">{{vs.amount}}</td>
						<td style="width: 3in;">{{vs.desc}}</td>
					</tr>
				</table>
				<div class="row">
					<div class="col s12">Net Cash Withdrawal Amount {{vs.amount}}</div>
				</div>
				<div class="row">
					<div class="col s12">Total Amount </div>
				</div>
				<div class="row">
					<div class="col s12">Note: Cashier has confirmed that he has obtained signature of passing authority & Payee on this payment by putting his signature on the same. </div>
				</div>
				<div class="row">
					<div class="col s3">Passed by<br/></>Code No</div><div class="col s3">Cash by<br/></>Code No</div><div class="col s3">Vouchered by<br/></>Code No</div><div class="col s3">Payees Signature</div>
				</div>
			</div>
		</div>
	</div>
</div>

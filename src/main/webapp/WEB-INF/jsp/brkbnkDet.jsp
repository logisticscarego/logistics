<div ng-show="operatorLogin || superAdminLogin">
	<div class="container">
	<div class="row">
	<form class="card" name="BankDetBrkForm" ng-submit="addBankBrkDet(BankDetBrkForm)" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
		
		<div class="col s4 input-field">
		       	<input class="validate" type ="text" id="brokerCodeId" name ="brokerCode" ng-model="broker.brkCode"  ng-click = "openBrokerCodeDB()" readonly ng-required="true" >
		       	<label for="code">Broker Code</label>	
		 </div>
		 <div class="input-field col s6">
		<input type="text" name="brkAcntHldrName" ng-model="broker.brkAcntHldrName" required><br>
		<label>Account Holder Name</label>
		</div>
		<div class="input-field col s6">
		<input type="text" name="accntNo" ng-model="broker.brkAccntNo" ng-required="true"><br>
		<label>Account Number</label>
		</div>
		<div class="input-field col s6">
		<input type="text" name="ifsc" ng-model="broker.brkIfsc" ng-pattern="/^[A-Z]{4}0[A-Z0-9]{6}$/" ng-required="true">
		<label>IFSC</label>
		</div>
		<div class="input-field col s6">
		<input type="text" name="micr" ng-model="broker.brkMicr"  >
		<label>MICR</label>
		</div>
		
		<div class="input-field col s6" >
			<input type="text" name="bnkBranch" ng-model="broker.brkBnkBranch" ng-required = "true" >
			<label>Bank Name</label>
		</div>
		
		
		<div class="row" > <!-- ng-show="uploadCCFlag" -->
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" file-model="ccImage">
						<label>Choose File for Cancelled cheque</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadCCImage(ccImage)">Upload</a>
					</div>
				</div>
		
		<div class="col s12 center">
			<input type="submit" value="Add Bank Details"/>
		</div>
	</form>
	</div>
	</div>
	
	  <div id ="brokerCodeDB" ng-hide="brokerCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox.brokerCode" placeholder="Search by Broker Code ">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Broker Code </th>
 	  	  	  <th> Broker Name </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="brkrCode in brokerCodeList | filter:filterTextbox.brokerCode ">
		 	  <td><input type="radio"  name="brkrCode"   value="{{ brkrCode}}" ng-model="brkCode " ng-click="saveBrokerCode(brkrCode.brkCode)"></td>
              <td>{{brkrCode.brkCode}}</td> 
              <td>{{brkrCode.brkName}}</td>
          </tr>
      </table> 
	</div>
	
	
</div>
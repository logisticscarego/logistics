<div ng-show="operatorLogin || superAdminLogin">
	<div class="noprint">
		<form name="cancelMrForm" ng-submit=cancelMrSubmit(cancelMrForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: #F2FAEF;"
			ng-class = "{'form-error': cancelMrForm.$invalid && isFormSubmitted}" novalidate = "novalidate">
			 
			<div class="row">
					<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="brhId" name ="brhName" ng-model="branch.branchName" ng-required="true" ng-click="openBrhDB()" readonly>
		       			<label for="code" style = "color:black;">Branch</label>
		       			<div class = "text-left errorMargin" ng-messages = "cancelMrForm.brhName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Branch is required.</span>
							</div>	
		       		</div>
		       		<div class="col s3 input-field">
				    		<select name="mrTypeName" id="mrTypeId" ng-model="mrType" ng-init="mrType = 'O'" ng-required="true">
								<option value='O'>On Account</option>
							</select>
							<label style = "color:black;">MR Type</label>
							<div class = "text-left errorMargin" ng-messages = "cancelMrForm.mrType.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  MR type is required.</span>
							</div>	
					</div>
		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="mrId" name ="mrName" ng-model="mrNo" ng-required="true" >
		       			<label for="code" style = "color:black;">Mr No</label>
		       				<div class = "text-left errorMargin" ng-messages = "cancelMrForm.mrName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  MR is required.</span>
							</div>
		       		</div>
		       		<div class="col s3 input-field">
	       				<input class="validate" type ="submit" value="submit" id="submitId" disabled="disabled" >
	       		</div>
		    </div>
		</form>    
	</div>
	
	<div id ="openBrhId" ng-hide="openBrhFlag">
		  <input type="text" name="filterBrh" ng-model="filterBrh" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Branch Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
		  <tr ng-repeat="brh in brhList | filter:filterBrh">
		 	  <td><input type="radio"  name="brh"   value="{{ brh }}" ng-model="brhCode" ng-click="saveBranch(brh)"></td>
              <td>{{brh.branchName}}</td>
              <td>{{brh.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
</div>	
<div ng-show="operatorLogin || superAdminLogin">
	
	<div class="row">
		<div class="col s3 hide-on-med-and-down"> &nbsp; </div>
			<form name="vehVenForm" ng-submit="vehVenSubmit()" class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
				<div class="input-field col s6">
					<input type="text" name="vehVenName" ng-model="vehVenTemp.rcNo" ng-click="openVehVenDB()" required readonly />
	   				<label>Enter RC No</label>
	 			</div>
	      		<div class="input-field col s6 center">
	      			<input class="btn waves-effect waves-light" type="submit" value="Submit">
	      		</div>
			</form>
		<div class="col s3"> &nbsp; </div>
	</div>

		
	<div class="row" ng-show="vehVenMstrFormFlag">
		<form name="vehVenMstrForm" ng-submit=updateVehVenMstr(vehVenMstrForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			
			<div class="row">
				<div class="col s6 input-field">
					<input class="validate" type="text" id="ownerId" name="ownerNm" ng-model="owner.ownName" ng-keyup="openOwnerDB()" ng-click="openOwnerDB()" readonly ng-required="true"> 
					<label for="code">Select Owner</label>
				</div>

				<div class="col s6 input-field">
					<input class="validate" type="text" id="brokerId" name="brokerNm" ng-model="broker.brkName" ng-keyup="openBrokerDB()" ng-click="openBrokerDB()" readonly ng-required="true"> 
					<label for="code">Select Broker</label>
				</div>
				
			</div>
			
			<div class="row">
		     		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvTypeId" name ="vvTypeName" ng-model="vehVenMstr.vvType" ng-keyup="openVehicleTypeDB()" ng-click="openVehicleTypeDB()" readonly="readonly">
		       			<label for="code">Vehcle Type</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="vvRcIssueDtId" name ="vvRcIssueDtName" ng-model="vehVenMstr.vvRcIssueDt" >
		       		<label for="code">RC Issue Date</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="vvRcValidDtId" name ="vvRcValidDtName" ng-model="vehVenMstr.vvRcValidDt" >
		       		<label for="code">RC Valid Date</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvDriverNameId" name ="vvDriverName" ng-model="vehVenMstr.vvDriverName" ng-required="true" >
		       			<label for="code">Driver Name</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     	
		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvDriverDLNoId" name ="vvDriverDLNoName" ng-model="vehVenMstr.vvDriverDLNo" ng-required="true" >
		       			<label for="code">Driver DL No.</label>
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="vvDriverDLIssueDtId" name ="vvDriverDLIssueDtName" ng-model="vehVenMstr.vvDriverDLIssueDt" >
		       		<label for="code">Driver DL Issue Date</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="vvDriverDLValidDtId" name ="vvDriverDLValidDtName" ng-model="vehVenMstr.vvDriverDLValidDt" >
		       		<label for="code">Driver DL Valid Date</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvDriverMobNoId" name ="vvDriverMobNoName" ng-model="vehVenMstr.vvDriverMobNo" ng-minlength="10" ng-maxlength="10">
		       			<label for="code">Driver Mob No.</label>
		       		</div>
	
			</div>
		    
		    <div class="row">
		     	
		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvPerNoId" name ="vvPerNoName" ng-model="vehVenMstr.vvPerNo" >
		       			<label for="code">Permit No.</label>
		       		</div>
		       		
					<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="vvPerIssueDtId" name ="vvPerIssueDtName" ng-model="vehVenMstr.vvPerIssueDt" >
		       		<label for="code">Permit Issue Date</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="vvPerValidDtId" name ="vvPerValidDtName" ng-model="vehVenMstr.vvPerValidDt" >
		       		<label for="code">Permit Valid Date</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvPerStateId" name ="vvPerStateName" ng-model="vehVenMstr.vvPerState" readonly="readonly" ng-keyup="openStateDB()" ng-click="openStateDB()">
		       			<label for="code">Permit State</label>
		       		</div>
		    </div>
		    
		    <div class="row">
		     	
		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvFitNoId" name ="vvFitNoName" ng-model="vehVenMstr.vvFitNo" >
		       			<label for="code">Fit Doc No.</label>
		       		</div>
		       		
					<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="vvFitIssueDtId" name ="vvFitIssueDtName" ng-model="vehVenMstr.vvFitIssueDt" >
		       		<label for="code">Fit Doc Issue Date</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="vvFitValidDtId" name ="vvFitValidDtName" ng-model="vehVenMstr.vvFitValidDt" >
		       		<label for="code">Fit Doc Valid Date</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvFitStateId" name ="vvFitStateName" ng-model="vehVenMstr.vvFitState" readonly="readonly" ng-keyup="openFitStateDB()" ng-click="openFitStateDB()">
		       			<label for="code">Fit Doc State</label>
		       		</div>
		    </div>
		    
		    <div class="row">
		     	
		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvFinanceBankId" name ="vvFinanceBankName" ng-model="vehVenMstr.vvFinanceBankName" >
		       			<label for="code">Finance Bank Name</label>
		       		</div>
		       		
					<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvPolicyNoId" name ="vvPolicyNoName" ng-model="vehVenMstr.vvPolicyNo" >
		       		<label for="code">Policy No.</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvPolicyCompId" name ="vvPolicyCompName" ng-model="vehVenMstr.vvPolicyComp" >
		       		<label for="code">Policy Company</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvTransitPassNoId" name ="vvTransitPassNoName" ng-model="vehVenMstr.vvTransitPassNo" >
		       			<label for="code">Transit Pass No.</label>
		       		</div>
		    </div>
		    	
			<div class="row">
      		 	<div class="col s12 center">
      		 		<input type="submit" id="updateVehVenMstrId" value="Submit">
      		 	</div>
      		</div>
	       	 
		  </form>
    </div>
	
	<div id ="vehVenDB" ng-hide="vehVenDBFlag">
		<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> RC No </th>
 	  	  	  <th> Owner </th>
 	  	  	  <th> Broker </th>
 	  	  	  <th> Driver </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="vehVenTemp in vehVendorList | filter:filterTextbox">
		 	  <td><input type="radio"  name="vehVenTemp" value="{{ vehVenTemp }}" ng-focus="saveVehVen(vehVenTemp)"></td>
              <td>{{vehVenTemp.rcNo}}</td>
              <td>{{vehVenTemp.ownName}}</td>
              <td>{{vehVenTemp.brkName}}</td>
              <td>{{vehVenTemp.driName}}</td>
          </tr>
          
          <tr>
          	<td colspan="3">
	          	<div class="col s12 center">
	          		<input type="button" value="Submit" ng-click="closeVehVenDb()">
	          	</div>
          	</td>
          </tr>
      	  
      </table> 
	</div>
	
	<div id ="ownerDB" ng-hide="ownerDBFlag">
		<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> Code </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="owner in ownerList | filter:filterTextbox">
		 	  <td><input type="radio"  name="owner" value="{{ owner }}" ng-focus="saveOwner(owner)"></td>
              <td>{{owner.ownName}}</td>
              <td>{{owner.ownCode}}</td>
          </tr>
          
          <tr>
          	<td colspan="3">
	          	<div class="col s12 center">
	          		<input type="button" value="Submit" ng-click="closeOwnDb()">
	          	</div>
          	</td>
          </tr>
      	  
      </table> 
	</div>
	
	<div id ="brokerDB" ng-hide="brokerDBFlag">
		<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> Code </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="broker in brokerList | filter:filterTextbox">
		 	  <td><input type="radio"  name="broker" value="{{ broker }}" ng-focus="saveBroker(broker)"></td>
              <td>{{broker.brkName}}</td>
              <td>{{broker.brkCode}}</td>
          </tr>
          
          <tr>
          	<td colspan="3">
	          	<div class="col s12 center">
	          		<input type="button" value="Submit" ng-click="closeBrkDb()">
	          	</div>
          	</td>
          </tr>
      	  
      </table> 
	</div>
	
	<div id ="vehTypeDB" ng-hide = "vehTypeDBFlag">
		<input type="text" name="filterVehicleType" ng-model="filterVehicleType" placeholder="Search">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				<th>Vehicle Type</th>
				<th>Service Type</th>
			</tr>
 	  
		  <tr ng-repeat="vt in vtList  | filter:filterVehicleType">
		 	  <td><input type="radio"  name="vtCode" id="vtCode" class="vtCode"  value="{{ vt }}" ng-model="vtCode" ng-click="saveVehicleType(vt)"></td>
              <td>{{ vt.vtCode }}</td>
              <td>{{ vt.vtVehicleType }}</td>
              <td>{{ vt.vtServiceType }}</td>
          </tr>
          
          <tr>
          	<td colspan="4">
	          	<div class="col s12 center">
	          		<input type="button" value="Submit" ng-click="closeVehicleTypeDb()">
	          	</div>
          	</td>
          </tr>
          
         </table>       
	</div>
	
	<div id ="stateDB" ng-hide = "stateDBFlag">
		<input type="text" name="filterVehicleType" ng-model="filterVehicleType" placeholder="Search">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>State</th>
			</tr>
 	  
		  <tr ng-repeat="state in stateList | filter:filterVehicleType">
		 	  <td><input type="radio"  name="stateName" id="stateId" ng-model="vtCode" ng-click="saveState(state)"></td>
              <td>{{ state }}</td>
              
          </tr>
          
          <tr>
          	<td colspan="2">
	          	<div class="col s12 center">
	          		<input type="button" value="Submit" ng-click="closeStateDb()">
	          	</div>
          	</td>
          </tr>
          
         </table>       
	</div>
	
	<div id ="fitStateDB" ng-hide = "fitStateDBFlag">
		<input type="text" name="filterVehicleType" ng-model="filterVehicleType" placeholder="Search">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>State</th>
			</tr>
 	  
		  <tr ng-repeat="state in stateList | filter:filterVehicleType">
		 	  <td><input type="radio"  name="fitStateName" id="fitStateId" ng-model="vtCode" ng-click="saveFitState(state)"></td>
              <td>{{ state }}</td>
              
          </tr>
          
          <tr>
          	<td colspan="2">
	          	<div class="col s12 center">
	          		<input type="button" value="Submit" ng-click="closeFitStateDb()">
	          	</div>
          	</td>
          </tr>
          
         </table>       
	</div>
	
</div>

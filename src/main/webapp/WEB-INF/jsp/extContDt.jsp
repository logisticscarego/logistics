<div ng-show="operatorLogin || superAdminLogin" >
<div class="row">
	<div class="col s3 hide-on-med-and-down"> &nbsp; </div>
	<form class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="contForm" ng-submit="viewContract(contCode,contForm)">
	
			<div class="input-field col s6">
   				<input type="text" id="displayDetails" name="ContCode" ng-model="contCode" ng-click="openCodesDB()" ng-required="true" readonly/>
   				<label>Enter Contract Code </label>
 			</div>
			
		 	<div class="input-field col s6 center">
		 		<input class="btn waves-effect waves-light" type="submit" value="Submit">
		 	</div>
			
	</form>
</div>



	<div id ="contCodeDB" ng-hide="contCodeDBFlag">
		<input type="text" name="filterContCode" ng-model="filterContCode" placeholder="Search....">
	 	  
	 	  <table>
	 	 		<tr>
					<th></th>
					
					<th>Contract Code</th>				
					
				</tr>
	 	 		  
			  <tr ng-repeat="contFa in contFaList | filter:filterContCode">
			 	  <td><input type="radio"  name="contFa" value="{{ contFa }}" ng-model="contFaMod" ng-click="saveContCode(contFa)"></td> 
	              <td>{{ contFa }}</td>
	          </tr>
	         </table>
         
	</div>


<div class="row" ng-show="showCont">
<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
	<h4>Here's the details of Contract <span class="teal-text text-lighten-2">{{ contCode }}</span>:</h4>
		<table class="table-hover table-bordered table-bordered" ng-show="showCont">
				<tr>
	                <td>Contract Code:</td>
	                <td>{{contract.contFaCode}}</td>
	            </tr>
	            
	             <tr>
	                <td>Customer Name:</td>
	                <td>{{contract.custName}}</td>
	            </tr>
	            
	            <tr>
	                <td>Customer Code:</td>
	                <td>{{contract.custFaCode}}</td>
	            </tr>
	            
	             <tr>
	                <td>Branch Name:</td>
	                <td>{{contract.contBranch}}</td>
	            </tr>
	            
	            <tr>
	                <td>From Station:</td>
	                <td>{{contract.contFrStn}}</td>
	            </tr>
	             
	             <tr>
	                <td>Contract Type:</td>
	                <td>{{contract.contType}}</td>
	            </tr>
	            
	             <tr>
	                <td>Contract Value:</td>
	                <td>{{contract.contValue}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract Weight:</td>
	                <td>{{contract.contWeight}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract DDL:</td>
	                <td>{{contract.contDDL}}</td>
	            </tr>
	            
	            <tr>
	                <td>Insaured By:</td>
	                <td>{{contract.contInsBy}}</td>
	            </tr>
	            
	             <tr>
	                <td>Remarks:</td>
	                <td>{{contract.contRemarks}}</td>
	            </tr>
	            
	             <tr>
	                <td>From Date:</td>
	                <td>{{contract.contFrDt | date : format : timezone}}</td>
	            </tr>
	            
	            <tr>
	                <td>To Date:</td>
	                <td>
		                <input type="date" id="frDtId" name="frDtName" ng-model="contract.contToDt"/>
		                <!-- {{contract.contToDt | date : format : timezone}} -->
	                </td>
	            </tr>

	          </table>
	          
	          <input type="button" value="UPDATE" id="saveId" ng-click="submit()"/>
		</div>
	</div>

</div>
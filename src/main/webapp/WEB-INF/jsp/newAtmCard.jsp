<div ng-show="operatorLogin || superAdminLogin">
		
	<!--Assign bank  -->
	<div class="row">
		<form name="newAtmCardForm" ng-submit=saveNewAtmCard(newAtmCardForm) class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
				<div class="col s4 input-field">
					<input class="validate" type="text" id="selectBnkId" name="bnkName" ng-model="bankMstr.bnkName" ng-click="openBankDB()" readonly ng-required="true"> 
					<label for="code">Bank</label>
				</div>

				<div class="col s4 input-field">
					<input class="validate" type="text" id="selectBranchId"	name="branchName" ng-model="branch.branchName" ng-click="openBranchDB()" readonly ng-required="true"> 
					<label for="code">Branch</label>
				</div>
				
				<div class="col s4 input-field">
					<input class="validate" type="text" id="selectEmployeeId"	name="employeeName" ng-model="employee.empName" ng-click="openEmployeeDB()" readonly ng-required="true"> 
					<label for="code">Employee</label>
				</div>
				
			</div>
			
			<div class="row">
				
				<div class="col s8 input-field">
					<input class="validate" type="text" id="atmCardNoId" name="atmCardNoName" ng-model="atmCard.atmCardNo" ng-minlength="16" ng-maxlength="16"  ng-required="true"> 
					<label for="code">Atm No.</label>
				</div>
				
				<div class="col s4 input-field">
					<input class="validate" type="date" id="atmCardExpireDtId" name="atmCardExpireDtName" ng-model="atmCard.atmCardExpireDt" ng-required="true">
					<label for="code">Expire Date</label>
				</div>
				
			</div>
			
			<div class="row">
      			 	<div class="col s12 center">
      			 		<input type="submit" value="Submit">
      			 	</div>
      		</div>
			
		</form>
		
	</div>
	
	<div id ="bankDB" ng-hide="bankDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Bank Name </th>
 	  	  	  <th> Bank FaCode </th>
 	  	  	  <th> Bank A/c No. </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bankMstr in bankMstrList | filter:filterTextbox">
		 	  <td><input type="radio"  name="bankMstr" value="{{ bankMstr }}" ng-model="bnkMstr" ng-click="saveBank(bankMstr, $index)"></td>
              <td>{{bankMstr.bnkName}}</td>
              <td>{{bankMstr.bnkFaCode}}</td>
              <td>{{bankMstr.bnkAcNo}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="branchDB" ng-hide="branchDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in branchList | filter:filterTextbox">
		 	  <td><input type="radio"  name="branch" value="{{ branch }}" ng-model="brnch" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id ="employeeDB" ng-hide="employeeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Employee Name </th>
 	  	  	  <th> Employee Branch </th>
 	  	  	  <th> Employee FaCode </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="employee in employeeList | filter:filterTextbox">
		 	  <td><input type="radio"  name="employee" value="{{ employee }}" ng-model="empl" ng-click="saveEmployee(employee)"></td>
              <td>{{employee.empName}}</td>
              <td>{{employee.prBranch.branchName}}</td>
              <td>{{employee.empFaCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	<!-- submit -->
	
	<div id="saveNewAtmCardDB" ng-hide="saveNewAtmCardDBFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				
				<tr>
					<td>Bank Name</td>
					<td>{{bankMstr.bnkName}}</td>
				</tr>
				
				<tr>
					<td>Branch</td>
					<td>{{branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Employee</td>
					<td>{{employee.empName}}</td>
				</tr>
				
				<tr>
					<td>Atm No.</td>
					<td>{{atmCard.atmCardNo}}</td>
				</tr>
				
				<tr>
					<td>Expire Date</td>
					<td>{{atmCard.atmCardExpireDt  | date:'dd/MM/yyyy'}}</td>
				</tr>
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" ng-click="saveAtmCard()"/>
		</div>
	</div>
	
</div>

<div ng-show="operatorLogin || superAdminLogin">
<title>Arrival Report Allowance</title>

<div class="row">
	<div class="col s3 hide-on-med-and-down">&nbsp;</div>
	<div class="col s6 card"
		style="align: center; margin-top: 40px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
		
		<div class="input-field col s6">
			<input type="text" id="brhCodeId" name="brhCodeName" ng-model="actBrhName"
				ng-required="true" readonly ng-click="openBrhDB()" /> <label>Select Branch</label>
		</div>
		
		<div class="input-field col s6">
			<input type="text" id="arrRepCode" name="arCode" ng-model="arCode"
				ng-required="true" readonly ng-click="openArCodeDB()" /> <label>Select Arrival Report</label>
		</div>

		<!-- <div class="input-field col s12 center">
			<input class="btn waves-effect waves-light" type="submit"
				value="Submit" ng-click="submit()">
		</div> -->
		
</div>

</div>


	<div id ="brhDB" ng-hide="brhDBFlag">
		  <input type="text" name="filterBrhbox" ng-model="filterBrhbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Branch Name</th>
 	  	  	  <th>FaCode</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="brh in brhList | filter:filterBrhbox">
		 	  <td><input type="radio"  name="brh"   value="{{ brh }}" ng-model="brhCode" ng-click="saveBranch(brh)"></td>
              <td>{{brh.branchName}}</td>
              <td>{{brh.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="arCodeDB" ng-hide="arCodeDBFlag">
		  <input type="text" name="filterArbox" ng-model="filterArbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>SEDR CODE</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="arCode in arCodeList | filter:filterArbox">
		 	  <td><input type="radio"  name="arCode"   value="{{ arCode }}" ng-model="arCodeM" ng-click="saveSedr(arCode)"></td>
              <td>{{arCode}}</td>
          </tr>
      </table> 
	</div>
<!-- <div ng-hide="showAR">
	<div class="container">
	<div class="row">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
	<div ng-hide = "show" class="col s8 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
				<h4>Here's the details of Arrival Report <span class="teal-text text-lighten-2">{{arrivalReport.arCode}}</span>:</h4>
		<table class="table-bordered table-hover table-condensed">
	            <tr>
	                <td>Arrival Report Type:</td>
	                <td><input type="text" id="arRepType" name="arRepType" ng-model="arrivalReport.arRepType" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Arrival Report Code:</td>
	                <td><input type="text" id="arCode" name="arCode" ng-model="arrivalReport.arCode" readonly></td>
	            </tr>
	            
	             <tr>
	                <td>Challan Code:</td>
	                <td><input type="text" id="archlnCodeId" name="archlnCodeName" ng-model="arrivalReport.archlnCode" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Arrival Report Date:</td>
	                <td><input type="date" id="arDt" name="arDt" ng-model="arrivalReport.arDt" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Received Weight:</td>
	                <td><input type="number" id="arRcvWt" name="arRcvWt" ng-model="arrivalReport.arRcvWt" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Package:</td>
	                <td><input type="number" id="arPkg" name="arPkg" ng-model="arrivalReport.arPkg" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Extra Km:</td>
	                <td><input type="number" id="arExtKmId" name="arExtKmName" ng-model="arrivalReport.arExtKm" step="0.00001" min="0.00000"></td>
	            </tr>
	            
	            <tr>
	                <td>Over Height:</td>
	                <td><input type="number" id="arOvrHgtId" name="arOvtHgtName" ng-model="arrivalReport.arOvrHgt" step="0.00001" min="0.00000"></td>
	            </tr>
	            
	            <tr>
	                <td>Weight Shortage:</td>
	                <td><input type="number" id="arWtShrtgId" name="arWtShrtgName" ng-model="arrivalReport.arWtShrtg" step="0.00001" min="0.00000" ng-keyup="calClaim()"></td>
	            </tr>
	            
	            <tr>
	                <td>Driver(Claim):</td>
	                <td><input type="number" id="arDrRcvrWtId" name="arDrRcvrWtName" ng-model="arrivalReport.arDrRcvrWt" step="0.00001" min="0.00000" ng-keyup="calClaim()"></td>
	            </tr>
	            
	            <tr>
	                <td>Penalty:</td>
	                <td><input type="number" id="arPenaltyId" name="arPenaltyName" ng-model="arrivalReport.arPenalty" step="0.00001" min="0.00000"></td>
	            </tr>
	            
	            <tr>
	                <td>Others(Claim):</td>
	                <td><input type="number" id="arOther" name="arOther" ng-model="arrivalReport.arOther" step="0.00001" min="0.00000" ng-keyup="calClaim()"></td>
	            </tr>
	            
	            <tr>
	                <td>Claim:</td>
	                <td><input type="number" id="arClaim" name="arClaim" ng-model="arrivalReport.arClaim" step="0.00001" min="0.00000"></td>
	            </tr>
	            
	            <tr>
	                <td>Unloading:</td>
	                <td><input type="number" id="arUnloading" name="arUnloading" ng-model="arrivalReport.arUnloading" step="0.00001" min="0.00000"></td>
	            </tr>
	            
	             <tr>
	                <td>Detention:</td>
	                <td><input type="number" id="arDetention" name="arDetention" ng-model="arrivalReport.arDetention" step="0.00001" min="0.00000"></td>
	            </tr>
	            
	            <tr>
	                <td>Late Delivery Charge:</td>
	                <td><input type="number" id="arLateDelivery" name="arLateDelivery" ng-model="arrivalReport.arLateDelivery" step="0.00001" min="0.00000" ng-keyup="calClaim()"></td>
	            </tr>
	            
	            <tr>
	                <td>Late Ack. Charge:</td>
	                <td><input type="number" id="arLateAck" name="arLateAck" ng-model="arrivalReport.arLateAck" step="0.00001" min="0.00000" ng-keyup="calClaim()"></td>
	            </tr>
	            
	            <tr>
	                <td>Reporting Date:</td>
	                <td><input type="date" id="arRepDt" name="arRepDt" ng-model="arrivalReport.arRepDt" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Reporting Time:</td>
	                <td><input type="time" id="arRepTime" name="arRepTime" ng-model="arrivalReport.arRepTime" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>HO Allowance:</td>
	                <td>
	                <select name="arIsHOAlw" ng-model="arrivalReport.arIsHOAlw" required>
						<option value='true'>Yes</option>
						<option value='false'>No</option>
					</select> 
				<input type="text" id="" name="" ng-model="arrivalReport.arIsHDAlw" ></td>
	            </tr>
	         
		</table>
		
		<input type="button" value="UPDATE" id="saveId" ng-click="editARSubmit(arrivalReport)"/>
</div>
</div>
</div>
</div>-->

<div ng-hide="showAR">
	<div class="container" style="width: 100%;">
	<div class="row">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
	<div ng-hide = "show" class="col s12 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
		<h5>Here's the details of Arrival Report <span class="teal-text text-lighten-2">{{arrivalReport.arCode}}</span>:</h5>
				
		<form name="arAlwForm" ng-submit="editARSubmit(arrivalReport)" class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
					
					<div class="col s3 input-field">
		       				<input type="text" id="arRepType" name="arRepType" ng-model="arrivalReport.arRepType" >
		       				<label for="code">Arrival Report Type</label>	
		       		</div>
		       		<div class="col s3 input-field">
						<input type="text" id="arCode" name="arCode" ng-model="arrivalReport.arCode" >
						<label for="code">Arrival Report Code</label>	
					</div>
					<div class="col s3 input-field">
						<input type="text" id="archlnCodeId" name="archlnCodeName" ng-model="arrivalReport.archlnCode" >
						<label for="code">Challan Code</label>	
					</div>
					<div class="col s3 input-field">
						<input type="date" id="arDt" name="arDt" ng-model="arrivalReport.arDt" >
						<label for="code">Arrival Report Date</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arRcvWt" name="arRcvWt" ng-model="arrivalReport.arRcvWt" >
						<label for="code">Received Weight</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arPkg" name="arPkg" ng-model="arrivalReport.arPkg" >
						<label for="code">Package</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arExtKmId" name="arExtKmName" ng-model="arrivalReport.arExtKm" ng-keyup="chngExKm()" ng-blur="chkValExKm()" step="0.00001" min="0.00000" required="required">
						<label for="code">Extra Km</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arRemExtKmId" name="arRemExtKmName" ng-model="arrivalReport.arRemExtKm" ng-keyup="chngRemExKm()"  ng-blur="chkValExKm()" step="0.00001" min="0.00000" required="required">
						<label for="code">Rem. Extra Km</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arOvrHgtId" name="arOvtHgtName" ng-model="arrivalReport.arOvrHgt" ng-keyup="chngOH()" ng-blur="chkValOH()" step="0.00001" min="0.00000" required="required">
						<label for="code">Over Height</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arRemOvrHgtId" name="arRemOvtHgtName" ng-model="arrivalReport.arRemOvrHgt" ng-keyup="chngRemOH()" ng-blur="chkValOH()" step="0.00001" min="0.00000" required="required">
						<label for="code">Rem. Over Height</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arWtShrtgId" name="arWtShrtgName" ng-model="arrivalReport.arWtShrtg" ng-keyup="chngWtShrtg()||calClaim()" ng-blur="chkValWtShrtg()" step="0.00001" min="0.00000"  required="required">
						<label for="code">Weight Shortage</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arRemWtShrtgId" name="arWtShrtgName" ng-model="arrivalReport.arRemWtShrtg" ng-keyup="chngRemWtShrtg()|| calClaim()" ng-blur="chkValWtShrtg()"   step="0.00001" min="0.00000"  required="required">
						<label for="code">Rem. Weight Shortage</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arUnloading" name="arUnloading" ng-model="arrivalReport.arUnloading" step="0.00001" ng-keyup="chngUnloading()" ng-blur="chkValUnloading()"  min="0.00000" required="required">
						<label for="code">Unloading</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arRemUnloading" name="arRemUnloading" ng-model="arrivalReport.arRemUnloading" ng-keyup="chngRemUnloading()" ng-blur="chkValUnloading()"  step="0.00001" min="0.00000" required="required">
						<label for="code">Rem. Unloading</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arDetention" name="arDetention" ng-model="arrivalReport.arDetention" ng-keyup="chngDetention()" ng-blur="chkValDetention()"   step="0.00001" min="0.00000" required="required">
						<label for="code">Detention</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arRemDetention" name="arRemDetention" ng-model="arrivalReport.arRemDetention" ng-keyup="chngRemDetention()" ng-blur="chkValDetention()"   step="0.00001" min="0.00000" required="required">
						<label for="code">Rem. Detention</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arLateDelivery" name="arLateDelivery" ng-model="arrivalReport.arLateDelivery" ng-keyup="chngLateDelivery()||calClaim()" ng-blur="chkValLateDelivery()"   step="0.00001" min="0.00000"  required="required">
						<label for="code">Late Delivery Charge:</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arRemLateDelivery" name="arRemLateDelivery" ng-model="arrivalReport.arRemLateDelivery" ng-keyup="chngRemLateDelivery()||calClaim()" ng-blur="chkValLateDelivery()"  step="0.00001" min="0.00000"  required="required">
						<label for="code">Rem. Late Delivery Charge:</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arLateAck" name="arLateAck" ng-model="arrivalReport.arLateAck" ng-keyup="chngLateAck()||calClaim()" step="0.00001" min="0.00000" ng-blur="chkValLateAck()" required="required">
						<label for="code">Late Ack. Charge:</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arRemLateAck" name="arRemLateAck" ng-model="arrivalReport.arRemLateAck" ng-keyup="chngRemLateAck()||calClaim()" ng-blur="chkValLateAck()" step="0.00001" min="0.00000"  required="required">
						<label for="code">Rem. Late Ack. Charge:</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arDrRcvrWtId" name="arDrRcvrWtName" ng-model="arrivalReport.arDrRcvrWt" step="0.00001" min="0.00000" ng-keyup="chngDrRcvrWt()||calClaim()" ng-blur="chkValDrRcvrWt()" required="required">
						<label for="code">Driver(Claim)</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arRemDrRcvrWtId" name="arRemDrRcvrWtName" ng-model="arrivalReport.arRemDrRcvrWt" step="0.00001" min="0.00000" ng-keyup="chngRemDrRcvrWt()||calClaim()" ng-blur="chkValDrRcvrWt()" required="required">
						<label for="code">Rem. Driver(Claim)</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arPenaltyId" name="arPenaltyName" ng-model="arrivalReport.arPenalty" step="0.00001" min="0.00000" required="required">
						<label for="code">Penalty</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arOther" name="arOther" ng-model="arrivalReport.arOther" step="0.00001" min="0.00000" ng-keyup="calClaim()" required="required">
						<label for="code">Others(Claim)</label>	
					</div>
					<div class="col s3 input-field">
						<input type="number" id="arClaim" name="arClaim" ng-model="arrivalReport.arClaim" ng-keyup="chngClaim()" ng-blur="chkValClaim()" step="0.00001" min="0.00000" readonly="readonly">
						<label for="code">Claim</label>	
					</div>
					<div class="col s3 input-field">
						<input type="date" id="arRepDt" name="arRepDt" ng-model="arrivalReport.arRepDt" >
						<label for="code">Reporting Date:</label>	
					</div>
					<div class="col s3 input-field">
						<input type="time" id="arRepTime" name="arRepTime" ng-model="arrivalReport.arRepTime" >
						<label for="code">Reporting Time:</label>	
					</div>
					<div class="col s3 input-field">
						<select name="arIsHOAlw" ng-model="arrivalReport.arIsHOAlw" required>
							<option value='true'>Yes</option>
							<option value='false'>No</option>
						</select>
						<label for="code">HO Allowance:</label>	
					</div>
					
					<div class="col s3 input-field">
						<input type="date" id="arIssueDt" name="arIssueDt" ng-model="arrivalReport.arIssueDt" >
						<label for="code">AR Issue Date:</label>	
					</div>
					
					<div class="col s3 input-field">
						<input type="date" id="chlnDt" name="chlnDt" ng-model="chlnDt">
						<label for="code">Challan Date:</label>	
					</div>
					<div class="col s3 input-field">
						<input type="text" id="lorryNO" name="lorryNO" ng-model="lorryNO">
						<label for="code">Lorry No.:</label>	
					</div>
					
					<div class="input-field col s3">
				<select name="arSrtgDmgType" ng-model="arrivalReport.arSrtgDmg" required>
				<!-- 	ng-blur="fillArSrtgDmgType()" -->
					<option value="OK">OK</option>
					<option value="SHORTAGE">SHORTAGE</option>
					<option value="DAMAGE">DAMAGE</option>
				</select> <label for="arSrtgDmgType">Shortage/Damage Report</label>
			</div>
					
					
				<div class="row">
	     			<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       </div>
	 	</form>
		
		

</div>
</div>
</div>
</div>
</div>
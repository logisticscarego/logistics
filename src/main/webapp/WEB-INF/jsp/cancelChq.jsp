<div ng-show="operatorLogin || superAdminLogin">
	
	<div class="row">
		<form name="cancelChqForm" ng-submit="cancelChq(cancelChqForm)" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			
			<div class="input-field col s3">
				<input type="text" name="bankName" ng-model="bankMstr.bnkName" ng-click="openBankDB()" required readonly/>
   				<label>Bank</label>
 			</div>
			
			<div class="input-field col s3">
				<input type="text" name="cancelChqNoName" ng-model="chqLeave.chqLChqNo" ng-click="openChqLeaveListDB()" required readonly/>
   				<label>Cheque No</label>
 			</div>
 			
 			<div class="input-field col s3">
				<input type="date" name="cancelChqDtName" ng-model="chqLeave.chqLChqCancelDt" required/>
   				<label>Cancel Date</label>
 			</div>
 			
      		<div class="input-field col s3 center">
      			<input class="btn waves-effect waves-light" type="submit" value="Cancel">
      		</div>
		</form>
		<div class="col s3"> &nbsp; </div>
	</div>
	
	<div id="cancelChqAlertDB" ng-hide="cancelChqAlertDBFlag" >
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">You are going to cancel Cheque {{chqLeave.chqLChqNo}}</h5></div>
		</div>
		<div>
			<div class="col s12 center">
				<a class="btn white-text" ng-click="cancel()">Cancel</a>
				<a class="btn white-text" ng-click="yes()">Yes</a>
			</div>
		</div>
	</div>
	
	<div id ="bankMstrDB" ng-hide="bankMstrDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> FaCode </th>
 	  	  	  <th> A/c No </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bankMstr in bankMstrList | filter:filterTextbox ">
		 	  <td><input type="radio"  name="bankMstr" value="{{ bankMstr }}" ng-model="bnkMstr" ng-click="saveBank(bankMstr)"></td>
              <td>{{bankMstr.bnkName}}</td>
              <td>{{bankMstr.bnkFaCode}}</td>
              <td>{{bankMstr.bnkAcNo}}</td>
          </tr>
      </table>  
	</div>
	
	<div id ="chqLeaveListDB" ng-hide="chqLeaveListDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Chq No </th>
 	  	  	  <th> Type </th>
 	  	  	  <th> PType </th>
 	  	  	  <th> MaxLimit </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="chqLeave in chqLeaveList | filter:filterTextbox ">
		 	  <td><input type="radio"  name="chqLeave" value="{{ chqLeave }}" ng-model="cLeave" ng-click="saveChqLeave(chqLeave)"></td>
              <td>{{chqLeave.chqLChqNo}}</td>
              <td>{{chqLeave.chqLChqType}}</td>
              <td>{{chqLeave.chqLCType}}</td>
              <td>{{chqLeave.chqLChqMaxLt}}</td>
              
          </tr>
      </table>  
	</div>
		
</div>

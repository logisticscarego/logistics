<h3>Data Integration</h3>
<div>
	

<div class="row">
	<div  class="input-field col s3">
		<input type="text" name="stateName" id="stateNameId" ng-model="stateName" ng-click="getState()" readonly>
		<label>State Name</label>
	</div>
	<div  class="input-field col s3">
		<input type="text" name="districtName" id="districtNameId" ng-model="distName" ng-click="getState()" readonly>
		<label>District Name</label>
	</div>
	<div  class="input-field col s3">
		<input type="text" name="cityName" id="cityNameId" ng-model="cityName" ng-click="getState()" readonly>
		<label>City Name</label>
	</div>
	<div  class="input-field col s3">
		<input type="text" name="stnName" id="stnNameId" ng-model="stnName" ng-click="getState()" readonly>
		<label>Station Name</label>
	</div>
	<div  class="input-field col s3">
		<input type="text" name="pinCode" id="pinCodeId" ng-model="pinCode" ng-keyup="getStnByPin()" >
		<label>PIN Code</label>
	</div>
	
	</div>

	<div id="stateDB" ng-hide="stateDBFlag">

		<input type="text" name="filterStateCode"
			ng-model="filterStateCode.stateName" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>State GstCode</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="state in stateList | filter:filterStateCode">
				<td><input type="radio" name="stateCode" id="stateId"
					value="{{ state.stateCode }}" ng-model="stateCode"
					ng-click="saveStateCode(state)"></td>
				<td>{{ state.stateGST }}</td>
				<td>{{ state.stateName }}</td>
			</tr>
		</table>

	</div>
	
	
		<div id="distDB" ng-hide="distDBFlag">

		<input type="text" name="filterDistCode"
			ng-model="filterDistCode" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="dist in distList | filter:filterDistCode">
				<td><input type="radio" name="distName" id="distId"
					value="{{ dist }}" ng-model="distName"
					ng-click="saveDist(dist)"></td>
				<td>{{ dist }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
	
	
	
		<div id="cityDB" ng-hide="cityDBFlag">

		<input type="text" name="filterCityCode"
			ng-model="filterCityCode" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>City Name</th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="city in cityList | filter:filterCityCode">
				<td><input type="radio" name="cityCode" id="cityId"
					value="{{ city }}" ng-model="cityName"
					ng-click="saveCity(city)"></td>
				<td>{{ city }}</td>
				<td>{{ distName }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
	
	
	<div id="stnDB" ng-hide="stnDBFlag">

		<input type="text" name="filterStnCode"
			ng-model="filterStnCode.stnName" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>Pin Code</th>
				<th>Station Name</th>
				<th>City Name</th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="stn in stnList | filter:filterStnCode">
				<td><input type="radio" name="stnCode" id="stnId"
					value="{{ stn }}" ng-model="stnName"
					ng-click="saveStn(stn)"></td>
				<td>{{ stn.stnPin }}</td>
				<td>{{ stn.stnName }}</td>
				<td>{{ stn.stnCity }}</td>
				<td>{{ stn.stnDistrict }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
	
	
	
	<input type="button" value="OwnerView" ng-click="getOwnerView()">

	<input type="button" value="BrokerView" ng-click="getBrokerView()">
	
	<div class="row">
		     		<div class="input-field col s12 center">
		       				<input class="validate" type ="button" value="Download OwnerList"  ng-click="downloadOwn()">
		       		</div>
		     		<div class="input-field col s12 center">
		       				<input class="validate" type ="button" value="Download BrokerList"  ng-click="downloadBrk()">
		       		</div>
		      </div>
	
	
	<div id="exportable"  class="noprint">
	<div id ="ownOpenDB" ng-hide="ownOpenFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th>Owner Code</th>
 	  	  	  <th>Owner Name</th>
 	  	  	  <th>Phone No 1</th>
 	  		 <th>Phone No 2</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="own in ownerList | filter:filterTextbox">
              <td>{{own.ownCode}}</td>
              <td>{{own.ownName}}</td>
              <td>{{own.ownPhNoList[0]}}</td>
              <td>{{own.ownPhNoList[1]}}</td>
              
          </tr>
      </table> 
	</div>
	</div>

	<div id="exportableB"  class="noprint">
	<div id ="brkOpenDB" ng-hide="brkOpenFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th>Broker Code</th>
 	  	  	  <th>Broker Name</th>
 	  	  	  <th>Phone No 1</th>
 	  		 <th>Phone No 2</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="brk in brokerList | filter:filterTextbox">
              <td>{{brk.brkCode}}</td>
              <td>{{brk.brkName}}</td>
              <td>{{brk.brkPhNoList[0]}}</td>
              <td>{{brk.brkPhNoList[1]}}</td>
              
          </tr>
      </table> 
	</div>
	
	
</div>
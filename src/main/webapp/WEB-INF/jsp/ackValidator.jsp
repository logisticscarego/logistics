<div ng-show="operatorLogin || superAdminLogin">

	<style>
	
	.tbl{text-align:center; 
		  color: #B5DAD4;
		
		border:1px solid rgba(255, 255, 255, 0.47);
		-moz-border-right: 1px solid rgba(255, 255, 255, 0.47);
		-webkit-border-right: 1px solid rgba(255, 255, 255, 0.47);}
		
		.rowcelwhite
		{
		text-align: center;
		color: #fff;
		border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:12px;
	    padding: 0px;
	    
	    }
	    .rowcelheader
	    {
		text-align: center;
	    color: #26A69A;
	    border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:12px;
	    font-weight:bold;
	    padding:3px;
	    
	    }
	    
		.align{
	    text-align:center;
	    font-size:20px;
	    }
	    .cursor
	    {
	    cursor:pointer;
	    }
		:hover.cursor
		{
		background-color:rgba(73, 156, 145, 0.25);
		}

#table {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#table td, #table th {
    border: 1px solid #ddd;
    padding: 8px;
}


#table th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: gray;
    color: white;
}
		
	</style>

	<!-- Loading div -->
	<div ng-show="lodingFlag" style=" position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
		<div style="margin-left:500px; margin-top:100px;">
			<img src="resources/img/loading.gif"  id="img" width="388px" height="104px"/>
		</div>
	</div>

	<div class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);" ng-hide="getAckValidationForm">
		<form  ng-submit="getDataFromAckValidator(fromDate,toDate,remarkType)">
			
			<div class="row">
							
		       	
		       	<div class="col s4 input-field">
		       		<input class="validate" type ="date" id="upToDtId" name ="upToDtName" ng-model="fromDate" required="required">
		       	<label for="code">From Date</label>	
		       	</div>
		       	
		       		<div class="col s4 input-field">
		       		<input class="validate" type ="date" id="upToDtId" name ="upToDtName" ng-model="toDate" required="required">
		       	<label for="code">TO Date</label>	
		       	</div>
		       	
		       		<div class="col s4 input-field">
		       		<select ng-model="remarkType" required="required"> 
		       		<option ng-value="all">ALL</option>
		       			<option ng-value="qualified">QUALIFIED</option>
		       			<option ng-value="allnonqualified">ALL NON QUALIFIED</option>
		      
		       		</select>
		       	<label for="code">Select Remark</label>	
		       	</div>
		       	
		       <div class="col s4 input-field">
		       		<input class="validate" type ="submit" value="Submit">
		      
		       	</div>
		       	
		    </div>

		</form>
		
		
	</div>
	
	<div class="row"  ng-hide="AckValidatorDBFlag">
<table  id="table">
<thead>
<tr><th>BCode</th><th>Cnmt Code</th><th>Pdf Name</th><th>Scan Dt</th><th>Check</th><th>Remarks</th><th>Reason</th></tr>
</thead>
<tbody>
<tr ng-repeat="ackValidation in ackValidationData">
<td>{{ackValidation.bcode}}</td>
<td>{{ackValidation.cnmtCode}}</td>
<td ng-click="openCnmtPdf(ackValidation.cnmtCode)"><a href="" style="color: yellow;">{{ackValidation.cnmtPdfName}}</a></td>
<td>{{ackValidation.scandate}}</td><td>{{ackValidation.check}}</td><td>{{ackValidation.remarks}}</td>
<td>{{ackValidation.reason}}</td>
</tr>
</tbody>
</table>
</div>




	<div class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);" ng-hide="ackFormData">
		<form  ng-submit="doAckValidation(cnmt.cnmtCode,cnmt.cnmtDt,arrivalReport,challan,consignor,consignee,lryArTime,lrarrivalDate,uploadingTime,unloadingAmt,lrUnloadingdate,detention,othAmt,days,recWeight,recPkg,remarks,damOrShrtg,weightOrPkg,claimPkg,claimwght,gdClaimAmt,reason)">
			
			<div class="row">

		       	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="cnmt.cnmtCode" readonly="readonly">
		       	<label for="code">Cnmt Code</label>	
		       	</div>
		       	
		       		<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="date"  ng-model="cnmt.cnmtDt" readonly="readonly">
		       	<label for="code">Date</label>	
		       	</div>
		       	
		       	    		<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="cnmt.bCode" readonly="readonly">
		       	<label for="code">Branch</label>	
		       	</div>
		       	
		    </div>
		    
		    		<div class="row">

		       	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="consignor" readonly="readonly">
		       	<label for="code">Cngr</label>	
		       	</div>
		       	
		       		<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="consignee" readonly="readonly">
		       	<label for="code">Cnge</label>	
		       	</div>
		       	
		       	    		<div class="col s4 input-field">
		       		<input class="validate" type ="time"  ng-model="lryArTime" required="required" >
		       	<label for="code">Lorry Arrival Time</label>	
		       	</div>
		       	
		       	
		    </div>
		    
		        		<div class="row">

		       	<div class="col s4 input-field">
		       		<input class="validate" type ="date"  ng-model="lrarrivalDate" ng-blur="lrArrivalDt(lrarrivalDate)" required="required">
		       	<label for="code">Lorry Arrival Date</label>	
		       	</div>
		       	
		       	    		<div class="col s4 input-field">
		       		<input class="validate" type ="time"  ng-model="uploadingTime" required="required">
		       	<label for="code">Unloading Time</label>	
		       	</div>
		       	
		       	     	    		<div class="col s4 input-field">
		       		<input class="validate" type ="number"  ng-model="unloadingAmt" required="required">
		       	<label for="code">Unloading Amt</label>	
		       	</div>
	
		       	
		    </div>
		        		<div class="row">
		       	<div class="col s4 input-field">
		       		<input class="validate" type ="date"  ng-model="lrUnloadingdate" ng-blur="lrUnloadingDt(lrUnloadingdate)" required="required">
		       	<label for="code">Lorry Unloading Date</label>	
		       	</div>
		       	
		       		<div class="col s4 input-field">
		       		      		<input class="validate" type ="number"  ng-model="detention" required="required">
		       	<label for="code">Detention</label>	
		       		
		       	</div>
		       	
		   		<div class="col s4 input-field">
		       		      		<input class="validate" type ="number"  ng-model="othAmt" required="required">
		       	<label for="code">Others Amt</label>	
		       		
		       	</div> 	
		    </div>
		    
		          		<div class="row">

		       	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="number"  ng-model="days" readonly="readonly" >
		       	<label for="code">Days</label>	
		       	</div> 

		    </div>
		    
		          		<div class="row">

		       	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="arrivalReport.arCode" readonly="readonly" >
		       	<label for="code">SEDR NO</label>	
		       	</div> 
		       	 	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="date"  ng-model="arrivalReport.arDt" readonly="readonly" >
		       	<label for="code">AR DATE</label>	
		       	</div> 
		       		 	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="branchName" readonly="readonly" >
		       	<label for="code">AR BCD</label>	
		       	</div>
		    </div>
		    
		    <div class="row">

		       	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="challan.chlnLryNo" readonly="readonly" >
		       	<label for="code">LORRY NO</label>	
		       	</div> 
		       	 	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="challan.chlnCode" readonly="readonly" >
		       	<label for="code">CHLN NO</label>	
		       	</div> 
		       		 	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="challan.chlnRemBal" readonly="readonly" >
		       	<label for="code">FINAL</label>	
		       	</div>
		    </div>
		    
		    	    <div class="row">

		       	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="arrivalReport.arClaim" readonly="readonly" >
		       	<label for="code">CLAIM</label>	
		       	</div> 
		       	 	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="arrivalReport.arOther" readonly="readonly" >
		       	<label for="code">OTHER</label>	
		       	</div> 
		       		 	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="arrivalReport.arDesc" readonly="readonly" >
		       	<label for="code">REMARKS</label>	
		       	</div>
		    </div>
		    
		       	    <div class="row">

		       	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="arrivalReport.arRemUnloading" readonly="readonly" >
		       	<label for="code">Unloading</label>	
		       	</div> 
		       	 	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="arrivalReport.arRemDetention" readonly="readonly" >
		       	<label for="code">Detention</label>	
		       	</div> 
		       		 	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="challan.chlnLoadingAmt" readonly="readonly" >
		       	<label for="code">Loading</label>	
		       	</div>
		    </div>
		    
		       	    <div class="row">

		       	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="challan.chlnExtra" readonly="readonly" >
		       	<label for="code">Extra</label>	
		       	</div> 
		       	 	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="challan.chlnTimeAllow" readonly="readonly" >
		       	<label for="code">Time Allowed</label>	
		       	</div> 
		       		 	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="timeTaken" readonly="readonly" >
		       	<label for="code">Time Taken</label>	
		       	</div>
		    </div>
		    
		       	    <div class="row">

		       	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="date"  ng-model="arrivalReport.arRepDt" readonly="readonly" >
		       	<label for="code">Report Dt</label>	
		       	</div> 
		       	 	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="challan.chlnNoOfPkg" readonly="readonly" >
		       	<label for="code">Pkg.</label>	
		       	</div> 
		       		 	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="challan.chlnTotalWt" readonly="readonly" >
		       	<label for="code">Wt.</label>	
		       	</div>
		    </div>
		    
		      <div class="row">

		       	<div class="col s4 input-field">
		       		<input class="validate" type ="number"   ng-model="recWeight" ng-blur="calculateWeightDiff(recWeight)" required="required" >
		       	<label for="code">Rec. Weight</label>	
		       	</div> 
		       	 	<div class="col s4 input-field">
		       		<input  class="validate" type ="number"   ng-model="recPkg" ng-blur="calculatePkgDiff(recPkg)" required="required" >
		       	<label for="code">Rec. Pkg.</label>	
		       	</div> 
		       		 	<div class="col s4 input-field" >
		       		<select class="validate" ng-model="remarks" ng-change="remarksSel(remarks)" required="required">
		       		<option value="ok">OK</option>
		       		<option value="QUALIFIED">QUALIFIED</option>
		       		</select>
		       	<label for="code">Remarks</label>	
		       	</div>
		     
		    </div>


            <div class="row" ng-hide="dspwDiv">
		       	<div class="col s4 input-field">
		       		<select class="validate" ng-model="damOrShrtg">
		       		<option value="Damage">Damage</option>
		       		<option value="Shortage">Shortage</option>
		       		</select>
		       	<label for="code">Damage/Shortage(D/S)</label>	
		       	</div> 
		       	 	<div class="col s4 input-field">
		       		<select class="validate" ng-model="weightOrPkg" ng-change="packORWeight(weightOrPkg)">
		       		<option value="Package">Package</option>
		       		<option value="Weight">Weight</option>
		       		</select>
		       	<label for="code">Claim Calculation on weight/PKG</label>	
		       	</div> 
		       		 	<div class="col s4 input-field" ng-if="pw===true">
		       	<input style="background-color: gray;"  class="validate" type ="text"  ng-model="claimPkg" readonly="readonly" >
		       	<label for="code">Enter Claim Package</label>	
		        	</div>
		        		 	<div class="col s4 input-field" ng-if="pw===false">
		       	<input style="background-color: gray;"  class="validate" type ="text"  ng-model="claimwght" readonly="readonly" >
		       	<label for="code">Enter Claim Weight</label>	
		        	</div>
		    </div>


      <div class="row" ng-hide="VOGDiv">

		       	<div class="col s4 input-field">
		       		<input style="background-color: gray;" class="validate" type ="text"  ng-model="gdClaimAmt" readonly="readonly" >
		       	<label for="code">Value of Good Claim Amt</label>	
		       	</div> 
		    
		    </div>
		    
		    <div class="row">

		       	<div class="col s4 input-field">
		       		<textarea rows="4" cols="10" ng-model="reason" class="validate" required="required"></textarea>
		       	<label for="code">Reason</label>	
		       	</div> 
		    
		    </div>
		    
		    	<div class="row">
				<div class="col s12 center">
					<input class="btn" type="submit" value="Submit">
				</div>
			</div>

		</form>
		
		
	</div>


</div>
	
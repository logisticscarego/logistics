<!-- 	<table>
		<tr>
			<td>Enter Stationary supplier code</td>
			<td><input type="text"  id= "stSupCode" name="stSupCode" ng-model="stSupplierCode" ng-click="openSupplierCodeDB()" required readonly></td>
		</tr>
	    <tr>
			<td><input type="button" value="Submit" ng-click="submitSupplierCode(stSupplierCode)"></td>
		</tr>
	</table> -->
	<div ng-show="operatorLogin || superAdminLogin">
<div class="row">
	<div class="col s3 hide-on-med-and-down"> &nbsp; </div>
		<div class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="input-field col s6">
				<input type="text" id="stSupName" name="stSupName" ng-model="stSupplierName" ng-click="openSupplierCodeDB(stSupplierName,stSupplierCode)" required readonly>
   				<input type="hidden"  name="stSupCode" ng-model="stSupplierCode">
   				<label>Enter Stationary supplier code</label>
 			</div>
      		<div class="input-field col s6 center">
      			<input type="button" value="Submit" ng-click="submitSupplierCode(stSupplierCode)">
      		</div>

     		
<div id="showSupplierDetails" ng-hide="showSupplierDetailsFlag">
	<h4>Here's the details of Stationary Supplier <span class="teal-text text-lighten-2">{{stsupplier.stSupName}}</span></h4>
	<form name="StSupplierForm" ng-submit="updateStSupplier(StSupplierForm)">
	<table class="table-hover table-bordered table-condensed">
		<tr>
			<td>Name</td>
			<td><input type ="text" name ="stSupName" ng-model="stsupplier.stSupName" value={{stsupplier.stSupName}}  ng-required="true"></td>
		</tr>
		
		<tr>
			<td>Code</td>
			<td><input type ="text" name ="stSupCode" ng-model="stsupplier.stSupCode" value={{stsupplier.stSupCode}} readonly required></td>
		</tr>
		
		<tr>
			<td>Address</td>
			<td><input type ="text" id="supAdd" name ="stSupAdd" ng-model="stsupplier.stSupAdd" value={{stsupplier.stSupAdd}} ng-minlength="3" ng-maxlength="255" ng-required="true"></td>
		</tr>
		
		<tr>
			<td>City</td>
			<td><input type ="text" id="supCity" name ="stSupCity" ng-model="stsupplier.stSupCity" value={{stsupplier.stSupCity}} ng-minlength="3" ng-maxlength="40" ng-required="true"></td>
		</tr>
		
		<tr>
			<td>State</td>
			<td><input type ="text" name ="stSupStateName" ng-model="stSupStateName" value={{stSupStateName}} readonly required ng-click="openStateCodeDB()"></td> 
			<td><input type ="hidden" name ="stSupState" ng-model="stsupplier.stSupState" ></td>
		</tr>
		
		<tr>
			<td>Pin</td>
			<td><input type ="text" id="supPin" name ="stSupPin" ng-model="stsupplier.stSupPin" value={{stsupplier.stSupPin}} ng-minlength="6" ng-maxlength="6" ng-required="true"></td>
		</tr>
		
		<tr>
			<td>Pan no.</td>
			<td><input type ="text" id="supPanNo" name ="stSupPanNo" ng-model="stsupplier.stSupPanNo" value={{stsupplier.stSupPanNo}} ng-required="true" ng-minlength="10" ng-maxlength="10" ng-pattern="/^[a-zA-Z0-9]*$/"></td>
		</tr>
		
		<tr>
			<td>TDS Code</td>
			<td><input type ="text" id="supTdsCode" name ="stSupTdsCode" ng-model="stsupplier.stSupTdsCode" value={{stsupplier.stSupTdsCode}} ng-minlength="3" ng-maxlength="40" ng-required="true"></td>
		</tr>
		
		<tr>
			<td>Group</td>
			<td><input type ="text" id="supGroup" name ="stSupGroup" ng-model="stsupplier.stSupGroup" value={{stsupplier.stSupGroup}} ng-minlength="3" ng-maxlength="40" ng-required="true"></td>
		</tr>
		
		<tr>
			<td>Sub-Group</td>
			<td><input type ="text" id="supSubGroup" name ="stSupSubGroup" ng-model="stsupplier.stSupSubGroup" value={{stsupplier.stSupSubGroup}} ng-minlength="3" ng-maxlength="40" ng-required="true"></td>
		</tr>
		 
		<tr>
			<td>CreationTS</td>
			<td><input type ="text" name ="creationTS" ng-model="time" value={{time}} required readonly></td>
		</tr>
	</table>
		<input type="submit" value="Submit!">
	</form>
</div>
</div>
<div class="col s3"> &nbsp; </div>
</div>	
	
	
<div id ="supplierCodeDB" ng-hide="supplierCodeDBFlag">
 	  <input type="text" name="filterCode" ng-model="filterCode.supplierCode " placeholder="Search">
 	  <table>
		  <tr ng-repeat="supplier in supplierCodeList | filter:filterCode">
		 	  <td><input type="radio"  name="supplierName"  class="supplierCls"  value="{{ supplier }}" ng-model="supplierCode" ng-click="saveSupplierCode(supplier)"></td>
              <td>{{ supplier.stSupCode }}</td>
              <td>{{ supplier.stSupName }}</td>
          </tr>
      </table>
         
</div>	

<div id ="stateCodeDB" ng-hide="stateCodeDBFlag"> 
 	   	<input type="text" name="filterCode" ng-model="filterCode.stateCode" placeholder="Search">
		 <table>
		  <tr ng-repeat="state in stateList  | filter:filterCode">
		 	  <td><input type="radio"  name="stateName" class="statecls"  value="{{ state }}" ng-model="stateModel" ng-click="saveStateCode(state)"></td>
              <td>{{ state.stateCode }}</td>
              <td>{{ state.stateName }}</td>
          </tr>
         </table>
</div>
</div>

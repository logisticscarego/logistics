
<title>Display Customer Representative</title>

<div class="row">
	<div class="col s3 hide-on-med-and-down">&nbsp;</div>
	<form class="col s6 card"
		style="align: center; margin-top: 40px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
		ng-submit="SubmitCR(crCode)">

		<div class="input-field col s6">
			<input type="text" id="custrepCode" name="crCodeTemp" ng-model="crCodeTemp"
				required readonly ng-click="OpenCustomerRepCodeDB()" /> 
			<input class="validate" type ="hidden" name ="crCode" ng-model="crCode" >	
				<label>Enter
				customer representative code </label>
		</div>

		<div class="input-field col s6 center">
			<input class="btn waves-effect waves-light" type="submit"
				value="Submit" />
		</div>

	</form>
	<div class="col s3">&nbsp;</div>
</div>

<div id="customerRepCodeDB" ng-hide="CustomerRepCodeDBFlag">
	<input type="text" name="filterTextbox" ng-model="filterTextbox"
		placeholder="Search by Customer Representative Code">
	<table>
		<th></th>
		<th>CustomerRep Code</th>
		<th>CustomerRep Name</th>
		<th>CustomerRep FaCode</th>
		
		<tr ng-repeat="cr in custRepCodeList | filter:filterTextbox">
			<td><input type="radio" name="crCode" value="{{ cr }}"
				ng-model="$parent.crCode" ng-change="saveCustomerRepCode(cr)"></td>
			<td>{{ cr.crCode }}</td>
			<td>{{ cr.crName }}</td>
			<td>{{ cr.crFaCode }}</td>
		</tr>
	</table>
</div>


<div class="row">
	<div class="col s3 hidden-xs hidden-sm">&nbsp;</div>
	<div ng-hide="show" class="col s6 card"
		style="align: center; margin-top: 40px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
		<h3>
			Here's the detail of <span class="teal-text text-lighten-2">{{crCode}}</span>:
		</h3>
		<table class="table-hover table-bordered table-condensed">
			<tr>
				<td>Customer Id:</td>
				<td>{{customer.custId}}</td>
			</tr>
			<tr>
				<td>Customer Reference No:</td>
				<td>{{customerRepresentative.custRefNo}}</td>
			</tr>
			<tr>
				<td>Customer Code</td>
				<td>{{customer.custCode}}</td>
			</tr>
			<tr>
				<td>Customer Name:</td>
				<td>{{customer.custName}}</td>
			</tr>
			<tr>
				<td>Customer Address:</td>
				<td>{{customer.custAdd}}</td>
			</tr>
			<tr>
				<td>Customer City:</td>
				<td>{{customer.custCity}}</td>
			</tr>
			<tr>
				<td>Customer State:</td>
				<td>{{customer.custState}}</td>
			</tr>
			<tr>
				<td>Customer Pin:</td>
				<td>{{customer.custPin}}</td>
			</tr>
			<tr>
				<td>Customer TDS Circle:</td>
				<td>{{customer.custTdsCircle}}</td>
			</tr>
			<tr>
				<td>Customer TDS City:</td>
				<td>{{customer.custTdsCity}}</td>
			</tr>
			<tr>
				<td>Customer TIN NO:</td>
				<td>{{customer.custTinNo}}</td>
			</tr>
			<tr>
				<td>Customer PAN No.:</td>
				<td>{{customer.custPanNo}}</td>
			</tr>
			<tr>
				<td>Customer Status:</td>
				<td>{{customer.custStatus}}</td>
			</tr>
			<tr>
				<td>Customer Daily Contract Allowed:</td>
				<td>{{customer.custIsDailyContAllow}}</td>
			</tr>
			<tr>
				<td>Customer Representative Code:</td>
				<td>{{customerRepresentative.crCode}}</td>
			</tr>
			<tr>
				<td>Customer Representative Name:</td>
				<td>{{customerRepresentative.crName}}</td>
			</tr>
			<tr>
				<td>Customer Representative Designation:</td>
				<td>{{customerRepresentative.crDesignation}}</td>
			</tr>
			<tr>
				<td>Customer Representative MobileNo:</td>
				<td>{{customerRepresentative.crMobileNo}}</td>
			</tr>
			<tr>
				<td>Customer Representative EmailId:</td>
				<td>{{customerRepresentative.crEmailId}}</td>
			</tr>

			<tr>
				<td>Customer Representative CreationTS:</td>
				<td>{{customerRepresentative.creationTS}}</td>
			</tr>

			<tr>
				<td>Customer CreationTS:</td>
				<td>{{customer.creationTS}}</td>
			</tr>


		</table>
	</div>
</div>
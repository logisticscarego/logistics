<div ng-show="operatorLogin || superAdminLogin">
<form name="StationarySupplierForm" ng-submit="submitSupplier(StationarySupplierForm,stsupplier)">
<div class="container">
	<div class="card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
	<div class="input-field col s4">
		<input type="text" id="supName" name="stSupName" ng-model="stsupplier.stSupName" ng-minlength="3" ng-maxlength="40" ng-required="true">
		<label>Stationary Supplier Name</label>
	</div>
	<div class="input-field col s4">
		<input type="text" id="supAdd" name="stSupAdd" ng-model="stsupplier.stSupAdd" ng-minlength="3" ng-maxlength="255" ng-required="true">
		<label>Stationary Supplier Address</label>
	</div>
	<div class="input-field col s4">
		<input type="text" id="supCity" name="stSupCity" ng-model="stsupplier.stSupCity" ng-minlength="3" ng-maxlength="40" ng-required="true">
		<label>Stationary Supplier City</label>
	</div>
	<div class="input-field col s4">
		<input type="text" name="stSupStateName" ng-model="stSupStateName" required readonly ng-click="openStateCodeDB()">
		<input type="hidden" name="stSupState" ng-model="stsupplier.stSupState">
		<label>Stationary Supplier State</label>
	</div>
	<div class="input-field col s4">
		<input type="text" id="supPin" name="stSupPin" ng-model="stsupplier.stSupPin" ng-minlength="6" ng-maxlength="6" ng-required="true">
		<label>Stationary Supplier Pin</label>
	</div>
	<div class="input-field col s4">
		<input type="text" id="supPanNo" name="stSupPanNo" ng-model="stsupplier.stSupPanNo" ng-minlength="10" ng-maxlength="10" ng-pattern="/^[a-zA-Z0-9]*$/">
		<label>Stationary Supplier Pan no.</label>
	</div>
	<div class="input-field col s4">
		<input type="text" id="supTdsCode" name="stSupTdsCode" ng-model="stsupplier.stSupTdsCode" ng-minlength="3" ng-maxlength="40">
		<label>Stationary Supplier Tds Code</label>
	</div>
	<div class="input-field col s4">
		<input type="text" id="supGroup" name="stSupGroup" ng-model="stsupplier.stSupGroup" ng-minlength="3" ng-maxlength="40">
		<label>Stationary Supplier Group</label>
	</div>
	<div class="input-field col s4">
		<input type="text" id="supSubGroup" name="stSupSubGroup" ng-model="stsupplier.stSupSubGroup" ng-minlength="3" ng-maxlength="40">
		<label>Supplier Supplier Sub-Group</label>
	</div>
	<div class="col s12 center">
		<input type="submit" value="Submit">
	</div>
	</div>
</div>
</form>

<div id ="stateCodeDB" ng-hide="stateCodeDBFlag"> 
 	    <input type="text" name="filterCode" ng-model="filterCode" placeholder="Search">
		 <table>
		  <tr ng-repeat="state in stateList  | filter:filterCode">
		 	  <td><input type="radio"  name="stateName" class="statecls"  value="{{ state }}" ng-model="stateModel" ng-click="saveStateCode(state)"></td>
              <td>{{ state.stateCode }}</td>
              <td>{{ state.stateName }}</td>
          </tr>
         </table>
</div>

<div id="viewStSupplierDB" ng-hide="viewStSupplierFlag">

<div class="row">
	
	<div ng-hide = "show" class="col s12 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
				<h4>Here's the details of Stationary Supplier <span class="teal-text text-lighten-2">{{stsupplier.stSupName}}</span>:</h4>
		<table class="table-bordered table-hover table-condensed">
	            
	    <tr>
			<td>Name</td>
			<td>{{stsupplier.stSupName}}</td>
		</tr>
		
		<tr>
			<td>Address</td>
			<td>{{stsupplier.stSupAdd}}</td>
		</tr>
		
		<tr>
			<td>City</td>
			<td>{{stsupplier.stSupCity}}</td>
		</tr>
		
		<tr>
			<td>State</td>
			<td>{{stsupplier.stSupState}}</td>
		</tr>
		
		<tr>
			<td>Pin</td>
			<td>{{stsupplier.stSupPin}}</td>
		</tr>
		
		<tr>
			<td>Pan no.</td>
			<td>{{stsupplier.stSupPanNo}}</td>
		</tr>
		
		<tr>
			<td>TDS Code</td>
			<td>{{stsupplier.stSupTdsCode}}</td>
		</tr>
		
		<tr>
			<td>Group</td>
			<td>{{stsupplier.stSupGroup}}</td>
		</tr>
		
		<tr>
			<td>Sub-Group</td>
			<td>{{stsupplier.stSupSubGroup}}</td>
		</tr>
		
  </table>
	      
<input type="button" value="Save" ng-click="saveStSupplier(stsupplier)">
<input type="button" value="Cancel" ng-click="closeStSupplierDB()">
	      
	  </div>
  </div>         
</div>

  </div> 
   
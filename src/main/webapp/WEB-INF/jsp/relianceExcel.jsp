
<div ng-show="operatorLogin || superAdminLogin">

<div ng-show="loadingFlag" style=" text-align:center; margin-left: -15px; margin-top: -50px; position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
        <div style="margin-left:20px; margin-top:100px;">
           <svg width='200px' height='200px'  viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-gears">
             <rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect>
                <g transform="translate(-20,-20)">
                      <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#2bbbad ">
                         <animateTransform attributeName="transform" type="rotate" from="90 50 50" to="0 50 50" dur="0.8s" repeatCount="indefinite"></animateTransform>
                       </path>
                 </g>
                 <g transform="translate(20,20) rotate(15 50 50)">
                     <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#0A3F4E ">
                          <animateTransform attributeName="transform" type="rotate" from="0 50 50" to="90 50 50" dur="0.8s" repeatCount="indefinite">
                          </animateTransform>
                      </path>
                  </g>
            </svg>
        </div>
       <span style=" color: #ffffff ;font-size:30px; font-weight:bold; letter-spacing:3px;">Loading...</span>
</div>

<div class="row">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
	<div class="col s8 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<form name="ledgerForm"  
			ng-class = "{'form-error': ledgerForm.$invalid && isFormSubmitted}" novalidate = "novalidate">
			<div class="row">
	     		<div class="col s4 input-field">
 	 				<input type="text" class="validate" id="cnId" name="cnName" ng-model="cnNo" ng-required/>	
 	 			</div>
 	 			<div class="col s3 input-field">
 	 				<input type="button" id="subAllId" value="Submit" ng-click="submitAll()" disabled="disabled"/>
			</div>
			</div>
	</div>
	</form>
</div>	
<div class="col s12 center" ng-hide="excelBtn">
			<form method="post" action="getRelCnXLSX" enctype="multipart/form-data">
				<input type="submit" id="printXlsId" value="Print XLS" disabled="disabled" ng-click="disableButton()">
		  	</form>
		</div>




<div class="col s12" id="exportable">
	<table class="table-condensed" ng-show="cnDetList.length > 0">	
		<tr>
			<th style="border:1px solid #000000;">Truck Number</th>
		    <th style="border:1px solid #000000;">LR No.</th>
		    <th style="border:1px solid #000000;">LR Date</th>
		    <th style="border:1px solid #000000;">Delivered Qty</th>
		    <th style="border:1px solid #000000;">Delivery Date</th>
		    <th style="border:1px solid #000000;">Rate</th>
		    <th style="border:1px solid #000000;">Freight Value</th>
		    <th style="border:1px solid #000000;">Manual claim</th>
		</tr>
		
		<tr ng-repeat="cnDet in cnDetList">
			<td style="border:1px solid #000000;">{{ cnDet.lryNo }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.cnmtCode }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.cnmtDt}}</td>
			<td style="border:1px solid #000000;">{{ cnDet.pkg}}</td>
			<td style="border:1px solid #000000;">{{ cnDet.dlvryDt }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.rate }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.cnmtFreight }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.claim }}</td>
		</tr>
		
		
	</table>
	
	

</div>
</div>

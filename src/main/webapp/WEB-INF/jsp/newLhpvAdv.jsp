<div ng-show="operatorLogin || superAdminLogin">

	<!-- Loading div -->
	<div ng-show="lodingFlag" style=" position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
		<div style="margin-left:500px; margin-top:100px;">
			<img src="resources/img/loading.gif"  id="img" width="388px" height="104px"/>
		</div>
	</div>

	<div class="row">
		<form name="voucherForm" ng-submit=voucherSubmit(voucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp" readonly>
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		    	<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.lhpvStatus.lsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div>
			   		 <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code">Voucher Type</label>	
		       		</div>
		       		<div class="col s4 input-field">
				    		<select name="payBy" id="payById"  ng-model="vs.payBy" ng-change="selectPayBy()" ng-init="vs.payBy = 'C'" ng-required="true">
								<option value='C'>BY CASH</option>
								<option value='Q'>BY CHEQUE</option>
								<option value='R'>BY RTGS</option>
								<option value='P'>BY PETRO CARD</option>
							</select>
							<label>Payment By</label>
					</div>
		    </div>
		    
		     <div class="row">
	     		<div class="input-field col s4 center" ng-show="vs.payBy == 'R'">
	       				<select name="payToStf" id="payToId" ng-model="vs.payToStf" ng-change="selectPayTo()" ng-init="vs.payToStf = 'B'" >
								<option value='S'>TO STAFF</option>
								<option value='B'>TO OWNBRK</option>
								<option ng-show="vs.branch.branchFaCode=='0100001'" value='L'>TO LORY</option>
							</select>
							<label>Payment To</label>
	       		</div>
	       		
	       		<div class="col s4 input-field"  ng-show="stafCodeFlag">
		       				<input class="validate" type ="text" id="stafCodeId" name ="stafCodeName" ng-model="vs.stafCode" ng-keyup="stfName()" ng-required="stfCodeRqr" ng-minlength="7" ng-maxlength="7">
		       			<label for="code">Staff Code</label>	
		       		</div>
	       		
	       		<div class="input-field col s4 center" ng-show="vs.payBy == 'P'">
	       				<select name="payToPetro" id="payToPetroId" ng-model="vs.payToPetro" ng-change="selectPayTo()" ng-init="vs.payToPetro = 'H'" ng-required="cardCodeRqr" >
								<option value='HP'>HINDUSTAN PETROLEUM</option>
								<option value='BP'>BHARAT PETROLEUM</option>
								<option value='IO'>INDIAN OIL</option>
							</select>
							<label>Payment To</label>
	       		</div>
	       		
	       		<div class="col s4 input-field"  ng-show="cardCodeFlag">
		       				<input class="validate" type ="text" id="cardCodeId" name ="cardCodeName" ng-model="vs.cardCode" ng-click="getCardNo()" ng-required="cardCodeRqr" readonly >
		       			<label for="code">Card FACODE</label>
		       		</div>
	       		
	       		
	       		<div class="col s4 input-field"  ng-show="loryCodeFlag">
		       				<input class="validate" type ="text" id="loryCodeId" name ="loryCodeName" ng-model="vs.loryCode" ng-click="lryNumber()" ng-required="loryCodeRqr"  readonly>
		       			<label for="code">LORY NO.</label>	
		       		</div>
	       		
	      </div>
		    
		    <div class="row">
		     		<div class="col s4 input-field">
			    		<select name="chequeType" id="chequeTypeId" ng-model="vs.chequeType" ng-change="selectChqType()" disabled="disabled">
							<option value='C'>Computerized</option>
							<option value='M'>Manual</option>
						</select>
						<label>Cheque Type </label>
					</div>
					
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bankCodeId" name ="bankCode" ng-model="vs.bankCode"  ng-click = "openBankCodeDB()" readonly disabled="disabled">
		       			<label for="code">Bank Code</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="chequeNoId" name ="chequeNo" ng-model="vs.chequeLeaves.chqLChqNo" ng-click="openChqNoDB()" readonly disabled="disabled" ng-required="chqNoRqr">
		       			<label for="code">Cheque No</label>	
		       		</div>
		    </div>
		    
		    <!-- <div class="row">
		    	<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="brkOwnId" name ="brkOwnName" ng-model="vs.brkOwnFaCode" ng-click="openBrkOwnDB()" readonly ng-required="true">
		       			<label for="code">Broker/Owner</label>	
		       			<label for="code">Pan Card Holder</label>
		       	</div>
		       	
		       	<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="challanId" name ="challanName" ng-model="challan" ng-click="openChlnDB()" readonly ng-required="true">
		       			<label for="code">Select Challan Code</label>	
		       	</div>
		    </div> -->
		    
		    <div class="row">
		    	<div class="col s4 input-field">
		       			<input class="validate" type ="text" id="brkOwnId" name ="brkOwnName" ng-model="vs.brkOwnFaCode" ng-click="notifyMsg()" readonly ng-required="true">
		       			<!-- <label for="code">Broker/Owner</label> -->	
		       			<label for="code">Pan Card Holder</label>
		       	</div>
		       	
		       	<div class="col s4 input-field">
		       			<input class="validate" type ="text" id="challanId" name ="challanName" ng-model="challan" ng-required="true">
		       			<label for="code">Enter Challan Code</label>	
		       	</div>
		       	
		       	<div class="col s4 input-field" ng-show="vs.branch.branchFaCode=='0100001' || dateTemp<='2018-12-05'">
	       				<input class="col s12 btn teal white-text" id="verfChln" type ="button" value="Verify Challan" ng-click="verifyChallan()" required readonly >
	       		</div>
		    </div>
		    
		    <div class="row"> 		
				 <div ng-show="lhpvAdvList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">LHPV ADVANCE DETAILS</caption>
						<tr class="rowclr">
							<th class="colclr">Chln No.</th>
							<th class="colclr">Lry Adv</th>
							<th class="colclr">Cash Dis</th>
							<th class="colclr">Munsiana</th>
							<th class="colclr">TDS</th>
							<th class="colclr">Payment Amt</th>
							<th class="colclr">Recovery Amt</th>
							<th class="colclr">Final Total Amt</th>
							<th class="colclr">Description</th>
							<th class="colclr">Action</th>
						</tr>
						<tr class="tbl" ng-repeat="lhpvadv in lhpvAdvList">
							<td class="rowcel">{{lhpvadv.challan.chlnCode}}</td>
							<td class="rowcel">{{lhpvadv.laLryAdvP}}</td>
							<td class="rowcel">{{lhpvadv.laCashDiscR}}</td>
							<td class="rowcel">{{lhpvadv.laMunsR}}</td>
							<td class="rowcel">{{lhpvadv.laTdsR}}</td>
							<td class="rowcel">{{lhpvadv.laTotPayAmt}}</td>
							<td class="rowcel">{{lhpvadv.laTotRcvrAmt}}</td>
							<td class="rowcel">{{lhpvadv.laFinalTot}}</td>
							<td class="rowcel">{{lhpvadv.laDesc}}</td>
							<td class="rowcel"><input type="button" value="remove" ng-click="removeLhpvAdv($index,lhpvadv)" /></td>
						</tr>
						<tr class="tbl" >							
							<td class="rowcel" colspan="7">Total Amount</td>
							<td class="rowcel">{{laFinalTotSum}}</td>
							<td class="rowcel"></td>
							<td class="rowcel"></td>							
						</tr>						
					</table>
				</div>
			</div>	

	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	      </div>		
	     
	</form>
	</div>   
	
	
	
	<div id="lhpvAdvDB" ng-hide="lhpvAdvDBFlag">
	<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="lhpvAdvForm" ng-submit="submitLhpvAdv(lhpvAdvForm,lhpvAdv)">
		 <div class="row">
			    <div class="col s4 input-field">
					<input class="validate" type ="number" name="lryAdvName" id="lryAdvId" ng-model="lhpvAdv.laLryAdvP" ng-keyUp="chngLryAdv()" ng-required="true">
					<label>Lorry Advance</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="csDisName" id="csDisId" ng-model="lhpvAdv.laCashDiscR" ng-keyup="chngLryCD()" ng-required="true" />
					<label>Cash Discount</label>
				</div>
				
				 <div class="col s4 input-field">
					<input type ="number" name="munsName" id="munsId" ng-model="lhpvAdv.laMunsR" ng-keyup="chngLryMU()" ng-required="true">
					<label>Munsiana</label>
				</div>
	     </div>
	     
	     
	     <div class="row">
			    <div class="col s4 input-field">
					<input type ="number" name="tdsAmt" id="tdsAmtId" ng-model="lhpvAdv.laTdsR" ng-keyup="chngTDS()" step="0.01"	min="0.00" readonly="readonly"/>
					<label>TDS Amount</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="payAmt" id="payAmtId" ng-model="lhpvAdv.laTotPayAmt" readonly/>
					<label>Payment Amount</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="tdsAmt" id="tdsAmtId" ng-model="lhpvAdv.laTotRcvrAmt" readonly/>
					<label>Recovery Amount</label>
				</div>
			
	     </div>
	     
	     
	     <div class="row">
	     		<div class="col s4 input-field">
					<input type ="number" name="finalTotAmt" id="finalTotAmtId" ng-model="lhpvAdv.laFinalTot" readonly/>
					<label>Final Total Amount</label>
				</div>
				<div class="col s4 input-field" ng-show="tdsPerFlag">
			    		<select name="tdsPerName" id="tdsPerId" ng-model="tdsPer" ng-init="tdsPer = 1" ng-change="changeTdsPer()" >
							<option value=1>1%</option>
							<option value=2>2%</option>
						</select>
						<label>Tds Percent</label>
				</div>
	     </div>
	     
	    <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="lhpvAdv.laDesc"></textarea>
 					<label>Description</label>
				</div>
         </div>	
	     
	     
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       </div>		
	     
	</form>
	</div>
	
	
	
	<div id ="bankCodeDB" ng-hide="bankCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Bank Code</th>
 	  	  	  <th>Bank Name</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bnk in bnkList | filter:filterTextbox">
		 	  <td><input type="radio"  name="bnk"   value="{{ bnk }}" ng-model="bkCode" ng-click="saveBankCode(bnk.bnkFaCode)"></td>
              <td>{{bnk.bnkFaCode}}</td>
              <td>{{bnk.bnkName}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id ="chqNoDB" ng-hide="chqNoDBFlag" class="noprint">
		  <input type="text" name="filterChqbox" ng-model="filterChqbox" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Cheque No</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="chq in chqList | filter:filterChqbox ">
		 	  <td><input type="radio"  name="chq.chqLChqNo"   value="{{ chq.chqLChqNo }}" ng-model="chqCode" ng-click="saveChqNo(chq)"></td>
              <td>{{chq.chqLChqNo}}</td>
              
          </tr>
      </table> 
	</div>
	
	
	<div id ="brkOwnDB" ng-hide="brkOwnDBFlag" class="noprint">
		  <input type="text" name="filterBObox" ng-model="filterBObox" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Name</th>
 	  	  	  <th>FaCode</th>
 	  	  	  <th>Code</th>
 	  	  	  <th>Pan No</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="actBO in actBOList | filter:filterBObox ">
		 	  <td><input type="radio"  name="actBO"   value="{{ actBO }}" ng-model="actBOCode" ng-click="saveBOCode(actBO.faCode)"></td>
              <td>{{actBO.name}}</td>
              <td>{{actBO.faCode}}</td>
              <td>{{actBO.code}}</td>
              <td>{{actBO.panNo}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="chlnDB" ng-hide="chlnDBFlag" class="noprint">
		  <input type="text" name="filterChlnbox" ng-model="filterChlnbox" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Challan Code</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="chln in chlnList | filter:filterChlnbox ">
		 	  <td><input type="radio"  name="chln"   value="{{ chln }}" ng-model="chlnCode" ng-click="saveChln(chln)"></td>
              <td>{{chln}}</td>
          </tr>
      </table> 
	</div>
	
	
	
	<div id="saveVsDB" ng-hide="saveVsFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{vs.branch.branchFaCode}}</td>
				</tr>
				
				<tr>
					<td>Branch Name</td>
					<td>{{vs.branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Date</td>
					<td>{{dateTemp}}</td>
				</tr>
				
				<tr>
					<td>Sheet No</td>
					<td>{{vs.lhpvStatus.lsNo}}</td>
				</tr>
				
				<tr>
					<td>Voucher Type</td>
					<td>{{vs.voucherType}}</td>
				</tr>
				
				<tr>
					<td>Payment By</td>
					<td>{{vs.payBy}}</td>
				</tr>
				
				<tr>
					<td>Cheque Type</td>
					<td>{{vs.chequeType}}</td>
				</tr>
				
				<tr>
					<td>Bank Code</td>
					<td>{{vs.bankCode}}</td>
				</tr>
				
				<tr>
					<td>Cheque No</td>
					<td>{{vs.chequeLeaves.chqLChqNo}}</td>
				</tr>
				
				<tr>
					<td>Pay To</td>
					<td>{{vs.payToStf}}</td>
				</tr>
				
				<tr>
					<td>Staff Code</td>
					<td>{{vs.stafCode}}</td>
				</tr>
				
				<tr>
					<td>Lorry Code</td>
					<td>{{vs.loryCode}}</td>
				</tr>
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" id="saveId" ng-click="saveVS()"/>
		</div>
	</div>
	
	<div id ="lryCodeDB" ng-hide="lryCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Lorry no.</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="lry in lryList | filter:filterTextbox">
		 	  <td><input type="radio"  name="lry"   value="{{ lry }}" ng-model="lryNo" ng-click="saveLryNo(lry)"></td>
              <td>{{lry}}</td>
              
          </tr>
      </table> 
	</div>
	
	
	<div id ="petroCardDB" ng-hide="petroCardDBFlag" class="noprint">
		  <input type="text" name="filterPetrobox" ng-model="filterPetrobox" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Card Type</th>
 	  	  	  <th>Card No.</th>
 	  	  	  <th>FA Code</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="petro in petroList | filter:filterBObox ">
		 	  <td><input type="radio"  name="petro"   value="{{ petro }}" ng-model="petroFa" ng-click="savePetroCode(petro.cardFaCode)"></td>
              <td>{{petro.cardType}}</td>
              <td>{{petro.cardNo}}</td>
              <td>{{petro.cardFaCode}}</td>
          </tr>
      </table> 
	</div>
	
   
</div>

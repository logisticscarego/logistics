<div ng-show="operatorLogin || superAdminLogin">
	<div class="row noprint">
		<form name="newRentForm" ng-submit=submit(newRentForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchId" name ="branchName" ng-model="branch.branchName" ng-click="openBranchDB()" readonly ng-required="true" >
		       			<label for="code">Branch</label>	
		       		</div>
		       		
		       	    <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentMLandLordNameId" name ="rentMLandLordName" ng-model="rent.rentMLandLordName" ng-required="true" >
		       			<label for="code">Land Lord Name</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       			<input class="col s12 btn teal white-text" type ="button" name="rentAddress" id="rentAddressId" ng-click="openAddDB()" value="Address">
		       			<label for="code">Land Lord Address</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentMRentPMId" name ="rentMRentPMName" ng-model="rent.rentMRentPM" ng-required="true" >
		       			<label for="code">Rent Per Month</label>	
		       		</div>
		       		
		       	    <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentMAdvId" name ="rentMAdvName" ng-model="rent.rentMAdv" ng-required="true" >
		       			<label for="code">Advance</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentMSecDepId" name ="rentMSecDepName" ng-model="rent.rentMSecDep" ng-required="true" >
		       			<label for="code">Security Deposite</label>	
		       		</div>
		    </div>
			
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="rentMAgreementFromId" name ="rentMAgreementFromName" ng-model="rent.rentMAgreementFrom" ng-required="true" >
		       			<label for="code">Agreement From Date</label>	
		       		</div>
		       		
		       	    <div class="col s4 input-field">
		       				<input class="validate" type ="date" id="rentMAgreementToId" name ="rentMAgreementToName" ng-model="rent.rentMAgreementTo" ng-required="true" >
		       			<label for="code">Agreement To Date</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
				    		<select name="telType" id="telTypeId" ng-model="rent.rentMFor" ng-init="rent.rentMFor = 'office'" ng-change="selectEmpType()" ng-required="true">
								<option value="office">Office</option>
								<option value="staff">Staff</option>
							</select>
							<label>Rent For</label>
					</div>
			</div>
			
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentMPanNoId" name ="rentMPanNoName" ng-model="rent.rentMPanNo" >
		       			<label for="code">Pan No</label>	
		       		</div>
		       		
		       	    <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentMBankNameId" name ="rentMBankName" ng-model="rent.rentMBankName" >
		       			<label for="code">Bank Name</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentBankBranchId" name ="rentBankName" ng-model="rent.rentBankBranch" >
		       			<label for="code">Bank Branch</label>	
		       		</div>
		    
		    </div>
		    
		    <div class="row">
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentMIFSCId" name ="rentMIFSCName" ng-model="rent.rentMIFSC" >
		       			<label for="code">IFSC Code</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentAcNoId" name ="rentAcNoName" ng-model="rent.rentAcNo" >
		       			<label for="code">A/C No</label>	
		       		</div>
		    
		     		<!-- <div class="col s4 input-field">
		       				<input class="col s12 btn teal white-text" type ="button"  id="employeeId" name ="employeeName" ng-model="employee.empName" ng-click="openEmployeeDB()" disabled="disabled" value="{{employee.empName}}" readonly ng-required="true" >
		       			<label for="code">Employee</label>	
		       		</div> -->
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text"  id="employeeId" name ="employeeName" ng-model="employee.empName" ng-click="openEmployeeDB()" disabled="disabled" readonly >
		       			<label for="code">Employee</label>	
		       		</div>
		       	    
		    </div>
		    
		    <div class="row">
      			 	<div class="col s12 center">
      			 		<input type="submit" value="Submit">
      			 	</div>
      		</div>
		</form>
    </div>
    
    <div id ="branchDB" ng-hide="branchDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in branchList | filter:filterTextbox">
		 	  <td><input type="radio"  name="branch" value="{{ branch }}" ng-model="brnch" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id ="employeeDB" ng-hide="employeeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Employee Name </th>
 	  	  	  <th> Employee Code</th>
 	  	  	  <th> Employee FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="employee in employeeList | filter:filterTextbox">
		 	  <td><input type="radio"  name="employee" value="{{ employee }}" ng-model="emp" ng-click="saveEmployee(employee)"></td>
              <td>{{employee.empName}}</td>
              <td>{{employee.empCodeTemp}}</td>
              <td>{{employee.empFaCode}}</td>
              
              
          </tr>
      </table> 
	</div>
	
	<div id="addDB" ng-hide = "addDBFlag">
		<form name = "addForm" ng-submit="saveAdd(addForm)">
			<table>
				<tr>
					<td>Address *</td>
					<td><input type ="text" id="addressId" name ="address" ng-model="add.completeAdd" ng-required="true" ng-minlength="3" ng-minlength="255"></td>
				</tr>
				
				<tr>
					<td>Address City *</td>
					<td><input type ="text" id="addCityId" name ="addCity" ng-model="add.addCity" ng-required="true" ng-minlength="3" ng-minlength="15"></td>
				</tr>
				
				<tr>
					<td>Address State *</td>
					<td><input type ="text" id="addStateId" name ="addState" ng-model="add.addState" ng-required="true" ng-minlength="3" ng-minlength="40"></td>
				</tr>
				
				<tr>
					<td>Address Pin *</td>
					<td><input type ="text" id="addPinId" name ="addPin" ng-model="add.addPin" ng-required="true" ng-minlength="6" ng-maxlength="6"></td>
				</tr>
				
				<tr>
					<td><input type="submit"  value="Submit" ></td>
				</tr>
			</table>
		</form>
	</div>
    
    	<div id="saveRentDB" ng-hide="saveRentDBFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<tr>
					<td>Branch Name</td>
					<td>{{branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Land Lord Name</td>
					<td>{{rent.rentMLandLordName}}</td>
				</tr>
				
				<tr>
					<td>Rent Per Month</td>
					<td>{{rent.rentMRentPM}}</td>
				</tr>
				
				<tr>
					<td>Advance</td>
					<td>{{rent.rentMAdv}}</td>
				</tr>
				
				<tr>
					<td>Security Deposit</td>
					<td>{{rent.rentMSecDep}}</td>
				</tr>
				
				<tr>
					<td>Agreement From Date</td>
					<td>{{rent.rentMAgreementFrom}}</td>
				</tr>
				
				<tr>
					<td>Agreement To Date</td>
					<td>{{rent.rentMAgreementTo}}</td>
				</tr>
				
				<tr>
					<td>Rent For</td>
					<td>{{rent.rentMFor}}</td>
				</tr>
				
				<tr>
					<td>Pan No</td>
					<td>{{rent.rentMPanNo}}</td>
				</tr>
				
				<tr>
					<td>Bank Name</td>
					<td>{{rent.rentMBankName}}</td>
				</tr>
				
				<tr>
					<td>Bank Branch</td>
					<td>{{rent.rentBankBranch}}</td>
				</tr>
				
				<tr>
					<td>IFSC Code</td>
					<td>{{rent.rentMIFSC}}</td>
				</tr>
				
				<tr>
					<td>A/C No.</td>
					<td>{{rent.rentAcNo}}</td>
				</tr>
				
				<tr>
					<td>Employee Name</td>
					<td>{{employee.empName}}</td>
				</tr>
				
				<tr>
					<td colspan="2" class="center"><h5>Address</h5></td>
				</tr>
				
				<tr>
					<td>Address</td>
					<td>{{add.completeAdd}}</td>
				</tr>
				
				<tr>
					<td>City</td>
					<td>{{add.addCity}}</td>
				</tr>
				
				<tr>
					<td>State</td>
					<td>{{add.addState}}</td>
				</tr>
				
				<tr>
					<td>Pin</td>
					<td>{{add.addPin}}</td>
				</tr>
								
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" ng-click="saveRent()"/>
		</div>
	</div>

</div>

<div ng-show="superAdminLogin">
	<div class="container">
	<div class="row">
	<form class="card" name="UserForm" ng-submit="addNewUser(UserForm,user)" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
		<div class="input-field col s6">
		<input type="text" name="username" ng-model="user.userName" ng-required="true">
		<label>UserName</label> 
		</div>
		<div class="input-field col s6">
		<input type="text" name="password" ng-model="user.password" ng-required="true"><br><br>
		<label>Password</label>
		</div>
		<div class="input-field col s6">
		<select name="userRole" ng-model="user.userRole" ng-options="roleOption as roleOption for roleOption in roleOptions" ng-change="selectUserRole(user.userRole)" required></select>
		<label>User Role</label>
		</div>
		<div class="input-field col s6">
		<select name="userStatus" ng-model="user.userStatus" ng-options="statusOption as statusOption for statusOption in statusOptions" required></select>
		<label>User Status</label>
		</div>
		<div class="input-field col s12" ng-show="user.userRole == 'Admin' || user.userRole == 'Operator'">
			<input type="text" name="userBranchName" ng-model="userBranchName" ng-click="displayBranch()" readonly>
			<label>User Branch Name</label> 
		</div>
		 <div class="input-field col s12" ng-show="enableUserCode">
			<input type="text" name="userCode" ng-model="user.userCodeTemp" ng-click="displayUserCode()" readonly>
			<label>User Code</label>
		</div> 
		<div class="col s12 center">
			<input type="submit" value="Add New User"/>
		</div>
	</form>
	</div>
	</div>
	<div id ="displayBranchList" ng-hide = "displayBranchFlag">
	 <input type="text" name="filterBranch" ng-model="filterBranch" placeholder="Search">
	    <table>
	          <tr>	
	          		<th></th>                
	                <th>Branch Name</th>
	                <th>Branch Code</th>
	                <th>Branch Pin</th>
	          </tr>
	      	  <tr ng-repeat="branch in branchList | filter:filterBranch">
	                 <td><input type="radio" name="branchCode" id="branchCode" class="branchCode"  value="{{ branch.branchCode }}" ng-click="selectBranch(branch)"></td>
		             <td>{{ branch.branchName }}</td>
		             <td>{{ branch.branchCode }}</td>
		             <td>{{ branch.branchPin }}</td>
	          </tr>
	    </table>       
	</div>
	
	<div id ="displayUserCodeList" ng-hide = "displayUserCodeFlag">
	 <input type="text" name="filterEmp" ng-model="filterEmp" placeholder="Search">
	    <table>
	          <tr>	
	          		<th></th>                
	                <th>Employee Name</th>
	                <th>Employee FaCode</th>
	          </tr>
	      	  <tr ng-repeat="employee in finalEmpList | filter:filterEmp">
	                 <td><input type="radio" name="employeeCode" id="employeeCode" class="employeeCode"  value="{{ employee.employeeCode }}"  ng-click="selectUserCode(employee)"></td>
		             <td>{{ employee.empName }}</td>
		             <td>{{ employee.empFaCode }}</td>
	          </tr>
	    </table>       
	</div>
	
</div>


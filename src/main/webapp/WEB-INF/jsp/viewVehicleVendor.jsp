<div ng-show="operatorLogin || superAdminLogin">
<title>View Branch</title>
<div class="row">
	<div class="col s3 hide-on-med-and-down"> &nbsp; </div>
		<form name="vehVenForm" ng-submit="vehVenSubmit()" class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<div class="input-field col s6">
				<input type="text" name="vehVenName" ng-model="vehVenTemp.rcNo"  required  />
   				<label>Enter RC No</label>
 			</div>
      		<div class="input-field col s6 center">
      			<input class="btn waves-effect waves-light" type="submit" value="Submit">
      		</div>
		</form>
		<div class="col s3"> &nbsp; </div>
</div>



	<div class="row" ng-show = "vehVenDetailFlag" >
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
	<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
		<h4>Here's the details of <span class="teal-text text-lighten-2">{{vehVendor.vvRcNo}}</span></h4>
		<table class="table-hover table-bordered table-condensed">
				
				
				<tr>
	                <td>Owner:</td>
	                <td>{{vehVendor.owner.ownName}}</td>
	            </tr>
	            <tr>
	                <td>Owner Code:</td>
	                <td>{{vehVendor.owner.ownCode}}</td>
	            </tr>
	            <tr>
	                <td>Owner PAN No.:</td>
	                <td>{{vehVendor.owner.ownPanNo}}</td>
	            </tr>
	            <tr>
	                <td>Vehicle Type Code:</td>
	                <td>{{vehVendor.vvType}}</td>
	            </tr>
	            
	            <tr>
	                <td>Vehicle Type:</td>
	                <td>{{vt}}</td>
	            </tr>
	            </table>
	            	                <div ng-show = "transferButtonFlag"><input class="btn waves-effect waves-light" type="button" value="Transfer" ng-click="newOwnerDetail()"></div>
		</div>	
		
		<div class="row" ng-show = "transferFlag">
		<form class="col s12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" name ="vehicleOwnerForm" ng-submit="saveVehicleOwner(vehicleOwnerForm,vv,own,add)">
		
		<div  class="col s12 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
      			<div class="input-field col s3">
        			<input id="ownPanNoId" type ="text" name ="ownPanNoName" ng-pattern="/^[A-Z]{3}[C,P,H,F,A,T,B,L,J,G][A-Z]\d{4}[A-Z]$/" ng-model="own.ownPanNo" ng-change="ownerDetailDBFlag='false'"  required>
        			<label>Pan No.</label>	
      			</div>
      			
      				<div class="col s3 center" ng-hide="ownerDetailDBFlag">
      			 		<input type="button" value="VerifyPanNo" ng-click="verifyPan(own.ownPanNo)">
      			 	</div>
</div>


  <div ng-show="ownerDetailDBFlag">	
  <div class="row">
  
  <div class="input-field col s3">
        			<input id="vvTransferId" type ="text" name ="vvTransferName"  ng-model="vv.vvTransferId"  required>
        			<label>Transfer Id</label>	
      			</div>
  
  <div class="input-field col s3">
        			<input id="vvTransferDtId" type ="date" name ="vvRcIssueDtName"  ng-model="vv.vvTransferDt"  required>
        			<label>Transfer Date.</label>	
      			</div>
      			
      			</div>
      			
      			<div class="row">
      			<div class="input-field col s3">
        			<input id="vvPerNoId" type ="text" name ="vvPerNoName"  ng-model="vv.vvPerNo"  >
        			<label>Permit No.:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="vvPerIssueDtId" type ="date" name ="vvPerIssueDtName"  ng-model="vv.vvPerIssueDt" ng-blur="perIssueDt()" >
        			<label>Permit Issue Date</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="vvPerValidDtId" type ="date" name ="vvPerValidDtName"  ng-model="vv.vvPerValidDt" ng-blur="perValidDt()" >
        			<label>Permit Valid Date</label>	
      			</div>
      			</div>
      			<div class="row">
      			<div class="input-field col s3">
        			<input id="vvPolicyNoId" type ="text" name ="vvPolicyNoName"  ng-model="vv.vvPolicyNo"  >
        			<label>Insurance No.:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="policyIssueDateId" type ="date" name ="policyIssueDateName"  ng-model="vv.vvPolicyIssueDt" ng-blur="policyIssueDt()" >
        			<label>Insurance Issue Date</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="policyValidDateId" type ="date" name ="policyValidDateName"  ng-model="vv.vvPolicyValidDt" ng-blur="policyValidDt()" >
        			<label>Insurance Valid Date</label>	
      			</div>
      			</div>
  		
		<div class="row">
      			<div class="input-field col s3">
        			<input id="ownPanNameId" type ="text" name ="ownPanName"  ng-model="own.ownPanName"  ng-readonly="" required>
        			<label>Name on PAN:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="ownFatherNameId" type ="text" name ="ownFatherName"  ng-model="own.ownFather"  >
        			<label>Father Name</label>	
      			</div>
      			</div>
      			
      			<div class="row">
      			<div class="input-field col s3">
        			<input id="ownNameId" type ="text" name ="ownName"  ng-model="own.ownName"  required>
        			<label>Name On RC/Owner Name</label>
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="phNoId" type ="text" name ="phNoName"  ng-model="phNo"  pattern="[0-9]{10}" required>
        			<label>Mobile No.:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="ownEmailId" type ="text" name ="ownEmailIdName"  ng-model="own.ownEmailId" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$">
        			<label>Email Address:</label>	
      			</div>
      			</div>
      			
      			<div class="row">
      			<div class="input-field col s3">
        			<input id="completeAddId" type ="text" name ="completeAddName"  ng-model="add.completeAdd" required >
        			<label>Building No./Plot No.:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="addPinId" type ="text" name ="addPinName"  ng-model="add.addPin" ng-keyup="getStnByPin()" minlength="6" maxlength="6"  required>
        			<label>Pin Code:</label>
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="addStateId" type ="text" name ="addStateName"  ng-model="add.addState" ng-click="getState()" readonly required>
        			<label>State</label>	
      			</div>
      			</div>
      			<div class="row">
      			<div class="input-field col s3">
        			<input id="districtId" type ="text" name ="districtName"  ng-model="add.addDist" ng-click="getState()" readonly required >
        			<label>District:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="addCityId" type ="text" name ="addCityName"  ng-model="add.addCity" ng-click="getState()" readonly required >
        			<label>City/Town:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="locationId" type ="text" name ="locationName"  ng-model="add.addPost" ng-click="getState()" readonly reauired >
        			<label>Post Office:</label>
      			</div>
      			</div>
      			<div class="row">
      			<div class="input-field col s3">
        			<input id="ownAccntNoId" type ="text" name ="ownAccntNoName"  ng-model="own.ownAccntNo" >
        			<label>Account No:</label>	
      			</div>
      			
      			
      			<div class="input-field col s3">
        			<input id="accountHldrNameId" type ="text" name ="accountHldrName"  ng-model="own.ownAcntHldrName"  >
        			<label>Account Holder Name</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="ownIfscId" type ="text" name ="ownIfscName"  ng-model="own.ownIfsc" pattern="^[A-Z]{4}0[A-Z0-9]{6}$"  >
        			<label>IFSC Code</label>
      			</div>
      			<div class="input-field col s3">
        			<input id="ownBnkBranchId" type ="text" name ="ownBnkBranchName"  ng-model="own.ownBnkBranch"  >
        			<label>Bank Name</label>
      			</div>
      			</div>
      			
      		<div class="row">
      		
      			<div class="col s3 center">
      			 		<input type="button" value="Upload Documents" id="upldDocId" ng-click="openUploadDB()">
      			 	</div>
      			 	
      				<div class="col s3 center">
      			 		<input type="submit" value="Save" id="saveId">
      			 	</div>
      		</div>
      		</div>
      		

</form>
	
		</div>
	
	</div>
	
	<div class="row" id="docUploadDBId" ng-show="docUpldFlag">
<form class="col s12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" name ="docUploadForm" ng-submit="uploadImage(docUploadForm)">
      			
      			<!-- <div class="row" >
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" name="" file-model="tcImage" ng-required="tcRqrFlag">
						<label>Choose File for TC</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadTCImage(tcImage)">Upload</a>
					</div>
				</div> -->
      			
      			<div class="row" ng-show="uploadRCFlag">
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" name="" file-model="rcImage" ng-required="rcRqrFlag">
						<label>Choose File for RC</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadRCImage(rcImage)">Upload</a>
					</div>
				</div>
      			
      			<div class="row" ng-show="uploadPANFlag">
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" file-model="panImage">
						<label>Choose File for PAN card:</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadPanImage(panImage)">Upload</a>
					</div>
				</div>
				
				<div class="row" ng-show="uploadPSFlag">
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" file-model="psImage">
						<label>Choose File for Permit Slip</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadPSImage(psImage)">Upload</a>
					</div>
				</div>
				
				<div class="row" ng-show="uploadInsrncFlag">
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" file-model="insrncImage">
						<label>Choose File for Insurance Doc</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadInsrncImage(insrncImage)">Upload</a>
					</div>
				</div>
				
				<div class="row" ng-show="uploadCCFlag">
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" file-model="ccImage">
						<label>Choose File for Cancelled cheque</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadCCImage(ccImage)">Upload</a>
					</div>
				</div>
				
				<div class="row" ng-show="uploadDecFlag">
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" file-model="decImage">
						<label>Choose File for Declaration</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadDecImage(decImage)">Upload</a>
					</div>
				</div>
				<div class="row">
      				<div class="col s3 center">
      			 		<input type="submit" value="Submit">
      			 	</div>
      		</div>
</form>
</div>
	
	
	
	
	
	<div id ="existOwnerId" ng-hide = "existOwnerDB">
	
	<input type="text" name="filterExistOwner" ng-model="filterExistOwner" placeholder="Search">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Owner Code</th>
				<th>Owner Name</th>
				<th>Owner Pan Name</th>
			</tr>
 	  
		  <tr ng-repeat="own in ownList  | filter:filterExistOwner">
		 	  <td><input type="radio"  name="ownerCode" id="ownerCode" class="ownerCode"  value="{{ own }}" ng-model="ownCode" ng-click="editExistOwner(own)"></td>
              <td>{{ own.ownCode }}</td>
              <td>{{ own.ownName }}</td>
              <td>{{ own.ownPanName}}</td>
          </tr>
         </table>       
	</div>
	
	
	<div id="stateDB" ng-hide="stateDBFlag">

		<input type="text" name="filterStateCode"
			ng-model="filterStateCode.stateName" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>State GstCode</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="state in stateList | filter:filterStateCode">
				<td><input type="radio" name="stateCode" id="stateId"
					value="{{ state.stateCode }}" ng-model="stateCode"
					ng-click="saveStateCode(state)"></td>
				<td>{{ state.stateGST }}</td>
				<td>{{ state.stateName }}</td>
			</tr>
		</table>

	</div>
	
	
		<div id="distDB" ng-hide="distDBFlag">

		<input type="text" name="filterDistCode"
			ng-model="filterDistCode" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="dist in distList | filter:filterDistCode">
				<td><input type="radio" name="distName" id="distId"
					value="{{ dist }}" ng-model="distName"
					ng-click="saveDist(dist)"></td>
				<td>{{ dist }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
	
	
	
		<div id="cityDB" ng-hide="cityDBFlag">

		<input type="text" name="filterCityCode"
			ng-model="filterCityCode" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>City Name</th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="city in cityList | filter:filterCityCode">
				<td><input type="radio" name="cityCode" id="cityId"
					value="{{ city }}" ng-model="cityName"
					ng-click="saveCity(city)"></td>
				<td>{{ city }}</td>
				<td>{{ distName }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
	
	
	<div id="stnDB" ng-hide="stnDBFlag">

		<input type="text" name="filterStnCode"
			ng-model="filterStnName" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>Pin Code</th>
				<th>Station Name</th>
				<th>City Name</th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="stn in stnList | filter:filterStnName">
				<td><input type="radio" name="stnCode" id="stnId"
					value="{{ stn }}" ng-model="stnName"
					ng-click="saveStn(stn)"></td>
				<td>{{ stn.pinCode }}</td>
				<td>{{ stn.stationName }}</td>
				<td>{{ stn.city }}</td>
				<td>{{ stn.district }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>	
	
	
</div>
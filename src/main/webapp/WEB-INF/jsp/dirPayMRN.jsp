<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="dpmrForm" ng-submit=dpmrSubmit(dpmrForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: #F2FAEF;"
			ng-class = "{'form-error': dpmrForm.$invalid && isFormSubmitted}" novalidate = "novalidate">
			<div class="row">
			
					<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="csDtId" name ="csDtName" ng-model="csDt" ng-blur="chngDt()" readonly ng-required="true" >
		       			<label for="code" style="color:black;">CS Date</label>
		       			<div class = "text-left errorMargin" ng-messages = "dpmrForm.csDtName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  CS date is required.</span>
							</div>		
		       		</div>
		       		
					<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="custId" name ="custName" ng-model="customer.custName" ng-click="selectCust()" ng-required="true" readonly>
		       			<label for="code" style="color:black;">Customer</label>
		       			<div class = "text-left errorMargin" ng-messages = "dpmrForm.custName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Customer is required.</span>
							</div>	
		       		</div>
					
		       		<div class="col s3 input-field">
		       			<select name="mrByName" id="mrById" ng-model="dpmr.mrPayBy" ng-init="dpmr.mrPayBy = 'C'" ng-change="chngMRPay()" ng-required="true">
								<option value='C'>CASH</option>
								<option value='Q'>CHEQUE</option>
								<option value='R'>RTGS</option>
						</select>
						<label style="color:black;">Money Receipt By</label>
						<div class = "text-left errorMargin" ng-messages = "dpmrForm.mrByName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Money receipt by is required.</span>
							</div>
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="number" id="netAmtId" name ="netAmtName" ng-model="dpmr.mrNetAmt" step="0.001" min="0.000" ng-required="true">
		       			<label for="code" style="color:black;">Net Amount</label>
	       				<div class = "text-left errorMargin" ng-messages = "dpmrForm.netAmtName.$error" ng-if = "isFormSubmitted">
							<span ng-message = "required">*  Net amount is required.</span>
						</div>
		       		</div>
		     		
		    </div>
		    
		  
		  		<div class="row">
		  		
		  		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="bnkId" name ="bnkName" ng-model="bank.bnkName" ng-click="selectBank()" disabled="disabled" readonly
		       					ng-required = "!isCashRequired">
		       			<label for="code" style="color:black;">Bank Name</label>
		       			<div class = "text-left errorMargin" ng-messages = "dpmrForm.bnkName.$error" ng-if = "isFormSubmitted">
							<span ng-message = "required">*  Bank is required.</span>
						</div>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="custBnkId" name ="custBnkName" ng-model="dpmr.mrCustBankName" disabled="disabled"
		       				ng-required = "!isCashRequired" ng-minlenght = "3" ng-maxlength = 100>
		       			<label for="code" style="color:black;">Customer Bank</label>
		       			<div class = "text-left errorMargin" ng-messages = "dpmrForm.custBnkName.$error" ng-if = "isFormSubmitted">
							<span ng-message = "required">*  Customer bank is required.</span>
							<span ng-message = "minlength">*  Customer bank length should be greater than 2.</span>
							<span ng-message = "maxlength">*  Customer bank length should be less than 100.</span>
						</div>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="chqId" name ="chqName" ng-model="dpmr.mrChqNo" disabled="disabled"
		       				ng-required = "chequeRequired" ng-minlength = "5">
		       			<label for="code" style="color:black;">Cheque no.</label>
		       			<div class = "text-left errorMargin" ng-messages = "dpmrForm.chqName.$error" ng-if = "isFormSubmitted">
							<span ng-message = "required">*  Cheque no. is required.</span>
							<span ng-message = "minlength">*  Cheque no. length should be greater than 5.</span>
						</div>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="refId" name ="refName" ng-model="dpmr.mrRtgsRefNo" disabled="disabled"
		       				 ng-required = "rtgsRequired">
		       			<label for="code" style="color:black;">RTGS Ref No</label>
		       			<div class = "text-left errorMargin" ng-messages = "dpmrForm.refName.$error" ng-if = "isFormSubmitted">
							<span ng-message = "required">*  RTGS ref. no. is required.</span>
						</div>	
		       		</div>
		       			  
		    </div>
		    
		    <div class="row">
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="billId" name ="billName" ng-model="bill.blBillNo" ng-click="selectBill()" readonly
		       				>
		       			<label for="code" style="color:black;">Select Bill No. *</label>
		       			<div class = "text-left errorMargin" ng-if = "isFormSubmitted && subPdmrList.length == 0">
							<span>*  Bill is required.</span>
						</div>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="number" id="onAccAmtId" name ="onAccAmtName" ng-model="onAcc" ng-init="onAcc = 0" step="0.001" min="0.000" ng-required="true">
		       			<label for="code" style="color:black;">On Account MR Amount</label>
		       			<div class = "text-left errorMargin" ng-messages = "dpmrForm.onAccAmtName.$error" ng-if = "isFormSubmitted">
							<span ng-message = "required">*  On account MR amount is required.</span>
						</div>	
		       		</div>
		    </div>
		    
		    <div class="row">
		    	<div class="input-field col s12">
				 <div ng-show="subPdmrList.length > 0">
					<table>
					<caption class="coltag tblrow">Detail Of Money Receipt</caption>
						<tr class="rowclr">
							<th class="colclr">Bill No.</th>
							<th class="colclr">Bill Date</th>
							<th class="colclr">Freight</th>
							<th class="colclr">Service Tax</th>
							<th class="colclr">TDS</th>
							<th class="colclr">Excess</th>
							<th class="colclr">Deduction</th>
							<th class="colclr">Net Payment</th>
							<th class="colclr">Action</th>
						</tr>
						<tr ng-repeat="subPmr in subPdmrList">
							<td class="rowcel">{{subPmr.bill.blBillNo}}</td>
							<td class="rowcel">{{subPmr.bill.blBillDt}}</td>
							<td class="rowcel">{{subPmr.mrFreight}}</td>
							<td class="rowcel">{{subPmr.mrSrvTaxAmt}}</td>
							<td class="rowcel">{{subPmr.mrTdsAmt}}</td>
							<td class="rowcel">{{subPmr.mrAccessAmt}}</td>
							<td class="rowcel">{{subPmr.mrDedAmt}}</td>
							<td class="rowcel">{{subPmr.mrNetAmt}}</td>
							<td class="rowcel"><input type="button" value="remove" ng-click="removePDMR($index)" /></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>TOTAL</td>
							<td class="rowcel">{{totRecAmt}}</td>
						</tr>
					</table>
				</div>
				</div>
    		 </div>	
		    
		     <div class="row">
				<div class="input-field col s12">
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="dpmr.mrDesc" ng-required = "true"
 					 ng-minlength = "2" ng-maxlength = 200></textarea>
 					<label style="color:black;">Description</label>
 					<div class = "text-left errorMargin" ng-messages = "dpmrForm.desc.$error" ng-if = "isFormSubmitted">
							<span ng-message = "required">*  Description is required.</span>
							<span ng-message = "minlength">*  Description should be greater than 1.</span>
							<span ng-message = "maxlength">*  Description should be less than 200.</span>
						</div>	
				</div>
         	</div>	
         
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" id="saveId" value="submit" >
	       		</div>
	      </div>		
	     
	</form>
	</div> 
	
	
	<div id ="selCustId" ng-hide="selCustFlag">
		  <input type="text" name="filterCust" ng-model="filterCust" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Customer Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cust in custList | filter:filterCust">
		 	  <td><input type="radio"  name="cust"   value="{{ cust }}" ng-model="custCode" ng-click="saveCustomer(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="selBankId" ng-hide="selBankFlag">
		  <input type="text" name="filterBank" ng-model="filterBank" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Bank Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="bnk in bankList | filter:filterBank">
		 	  <td><input type="radio"  name="bnk"   value="{{ bnk }}" ng-model="bnkCode" ng-click="saveBank(bnk)"></td>
              <td>{{bnk.bnkName}}</td>
              <td>{{bnk.bnkFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="selBillId" ng-hide="selBillFlag">
		  <input type="text" name="filterBill" ng-model="filterBill" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Bill No</th>
 	  	  	  <th>Bill Date</th>
 	  	  	  <th>Final Amount</th>
 	  	  	  <th>Pending Amount</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="bill in blList | filter:filterBill">
		 	  <td><input type="radio"  name="bill"   value="{{ bill }}" ng-model="billCode" ng-click="saveBill(bill)"></td>
              <td>{{bill.blBillNo}}</td>
              <td>{{bill.blBillDt}}</td>
              <td>{{bill.blFinalTot | number : 2}}</td>
              <td>{{bill.blRemAmt | number : 2}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="dedDetId" ng-hide="dedDetFlag">
 	  <form class="col s12 card" style="align: center; padding-top:40px;  background-color: #F2FAEF;" name="dedDetForm" ng-submit="submitDedDet(dedDetForm)"
 	  	ng-class = "{'form-error': dedDetForm.$invalid && dedDetFormFormSubmitted" novalidate = "novalidate">
		 <div class="row">
		 		
		 		<div class="col s4 input-field">
		       		<input class="validate" type ="text" id="cnmtId" name ="cnmtName" ng-model="cnmtCode" ng-click="selectCnmt()" readonly ng-required = "true">
		       			<label for="code" style="color:black;">Select Cnmt No</label>	
		       			<div class = "text-left errorMargin" ng-messages = "dedDetForm.cnmtName.$error" ng-if = "dedDetFormFormSubmitted">
								<span ng-message = "required">*  CNMT is required.</span>
							</div>	
		       	</div>
		       	
		 		<div class="col s4 input-field">
					<select name="dedTypeName" id="dedTypeId" ng-model="dedType" ng-init="dedType = 'FREIGHT RATE'" ng-required="true">
							<option value='FREIGHT RATE'>FREIGHT RATE</option>
							<option value='FREIGHT WEIGHT'>FREIGHT WEIGHT</option>
							<option value='LATE DELIVERY'>LATE DELIVERY</option>
							<option value='COOLIE LOADING'>COOLIE LOADING</option>
							<option value='COOLIE UNLOADING'>COOLIE UNLOADING</option>			
							<option value='DETENTION'>DETENTION</option>
							<option value='CLAIM SHORTAGE'>CLAIM SHORTAGE</option>
							<option value='CLAIM DAMAGE'>CLAIM DAMAGE</option>
							<option value='NON PLACEMENT'>NON PLACEMENT</option>
							<option value='OTHERS'>OTHERS</option>
					</select>
					<label style="color:black;">Deduction Type</label>
					<div class = "text-left errorMargin" ng-messages = "dedDetForm.dedTypeName.$error" ng-if = "dedDetFormFormSubmitted">
								<span ng-message = "required">*  Deduction type is required.</span>
							</div>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="dedName" id="dedId" ng-model="dedAmt" step="0.001" min="0.000" ng-required="true"/>
					<label style="color:black;">Deduction Amount</label>
					<div class = "text-left errorMargin" ng-messages = "dedDetForm.dedName.$error" ng-if = "dedDetFormFormSubmitted">
								<span ng-message = "required">*  Deduction is required.</span>
							</div>	
				</div>
				
	     </div>
	  	     
	     <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	      </div>		
	    </form> 
	</div>
	
	
	<div id ="selCnmtId" ng-hide="selCnmtFlag">
		  <input type="text" name="filterCnmt" ng-model="filterCnmt" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>CNMT NO</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cnmt in cnmtList | filter:filterCnmt">
		 	  <td><input type="radio"  name="cnmt"   value="{{ cnmt }}" ng-model="cnmtCode" ng-click="saveCnmt(cnmt)"></td>
              <td>{{cnmt}}</td>
          </tr>
       </table>
	</div>
	
	
	<div id="billInfoId" ng-hide="billInfoFlag">
	<form class="col s12 card" style="align: center; padding-top:40px;  background-color: #F2FAEF;" name="billInfoForm" ng-submit="submitBillInfo(billInfoForm)"
	ng-class = "{'form-error': billInfoForm.$invalid && billInfoFormSubmitted}" novalidate = "novalidate">
		 <div class="row">
		 		<div class="col s4 input-field">
					<input class="validate" type ="number" name="frtAmtName" id="frtAmtId" ng-model="subPdmr.mrFreight" step="0.001" min="0.000" ng-required="true" ng-readonly='customer.custEditBillAmt'>
					<label style="color:black;">Freight Amount</label>
					<div class = "text-left errorMargin" ng-messages = "billInfoForm.frtAmtName.$error" ng-if = "billInfoFormSubmitted">
								<span ng-message = "required">*  Freight is required.</span>
							</div>	
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="tdsName" id="tdsId" ng-model="subPdmr.mrTdsAmt" step="0.001" min="0.000"/>
					<label style="color:black;">TDS</label>
				</div>
				
			    <div class="col s4 input-field">
					<input class="validate" type ="number" name="recAmtName" id="recAmtId" ng-model="subPdmr.mrNetAmt" step="0.001" min="0.000" ng-required="true">
					<label style="color:black;">Received Amount</label>
					<div class = "text-left errorMargin" ng-messages = "billInfoForm.recAmtName.$error" ng-if = "billInfoFormSubmitted">
								<span ng-message = "required">*  Receive amount is required.</span>
							</div>	
				</div>
			
	     </div>
	     
	     <div class="row">
	     		<div class="col s4 input-field">
					<input type ="number" name="dedName" id="dedId" ng-model="subPdmr.mrDedAmt" step="0.001" min="0.000">
					<label style="color:black;">Deduction Amount</label>
				</div>
				
				 <div class="col s4 input-field">
					<input type ="button" name="dedBtn" id="dedBtnId" value="Deduction Detail" ng-click="addDedDetail()">
				</div>
				
	     		<div class="col s4 input-field">
					<input type ="number" name="excName" id="excId" ng-model="subPdmr.mrAccessAmt" step="0.001" min="0.000"/>
					<label style="color:black;">Excess</label>
				</div>
	     </div>
	    
	     
	     <!-- <div class="row">
		      <div class="col s4 input-field">
					<input type ="number" name="stName" id="stId" ng-model="subPdmr.mrSrvTaxAmt" step="0.001" min="0.000" ng-requierd="true">
					<label>Service Tax</label>
			  </div>
	     </div> -->
	     
	    <div class="row">
	    	<div class="input-field col s12">
			 <div ng-show="dedDetailList.length > 0">
				<table class="tblrow">
				<caption class="coltag tblrow">Deduction Detail</caption>
					<tr class="rowclr">
						<th class="colclr">Cnmt No.</th>
						<th class="colclr">Dedution Type</th>
						<th class="colclr">Deduction Amount</th>
						<th class="colclr">Action</th>
					</tr>
					<tr class="tbl" ng-repeat="dedDetail in dedDetailList">
						<td class="rowcel">{{dedDetail.cnmtNo}}</td>
						<td class="rowcel">{{dedDetail.dedType}}</td>
						<td class="rowcel">{{dedDetail.dedAmt}}</td>
						<td class="rowcel"><input type="button" value="remove" ng-click="removeDed($index)" /></td>
					</tr>
				</table>
			</div>
			</div>
   		</div>	 
	     
	    
	     
	    <!-- <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="dedRsn" ng-model="subPdmr.mrDedRsn"></textarea>
 					<label>Deduction Reason</label>
				</div>
         </div>	 -->
	     
	     
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       </div>		
	     
	</form>
	</div>
	
	<div id="duplicateDB" ng-hide="duplicateFlag">
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">You already generate a MR of {{ customer.custName }} with {{dpmr.mrNetAmt}}</h5></div>
		</div>
		<div>
			<div class="col s12 center">
				<a class="btn white-text" ng-click="cancelMR()">Cancel</a>
				<a class="btn white-text" ng-click="saveMR()">Yes</a>
			</div>
		</div>
	</div>
	
</div>	
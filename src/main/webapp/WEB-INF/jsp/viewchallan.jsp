<div ng-show="operatorLogin || superAdminLogin">
		<title>View Challan</title>
	
<div class="row">
	<div class="col s3 hide-on-med-and-down"> &nbsp; </div>
		<div class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
	
			<div class="input-field col s6">
   				<input type="text" id="challanCode" name="chlnCode" ng-model="chlnCode" required />
   				<label>Enter Challan Code:</label>
 			</div>
			
      			 	<div class="input-field col s6 center">
      			 		<input class="btn waves-effect waves-light" type="submit" value="Submit" ng-click="ViewChallan(chlnCode)">
      			 	</div>
			

		<div id="showChallanDetails" ng-hide="showChallanDetailsFlag">
		<h4> Here's the review and details of Challan <span class="teal-text text-lighten-2">{{challan.chlnId}}</span>:</h4>
		 <form name="ChallanForm" ng-submit="updateChallan(ChallanForm)">
		<table class="table-hover table-bordered table-condensed">
	            <tr>
	                <td>Challan Id:</td>
	                <td><input type ="text" id="chlnId" name ="chlnId" ng-model="challan.chlnId" value={{challan.chlnId}} readonly></td>
	            </tr>
	            <tr>
	                <td> Challan Branch Code:</td>
	                <td><input type ="text" name ="branchCode"  ng-model="challan.branchCode" value={{challan.branchCode}} required readonly></td>
	            </tr> 
	            <tr>
	                <td>Lorry No:</td>
	                <td><input type ="text" name ="chlnLryNo" id="chlnLryNo" ng-model="challan.chlnLryNo" value={{challan.chlnLryNo}} readonly></td>
	            </tr> 
			     <tr>
	                <td> Challan From Station:</td>
	                <td><input type ="text" name ="chlnFromStn" id="chlnFromStn" ng-model="challan.chlnFromStn" value={{challan.chlnFromStn}} required readonly></td>
	            </tr>
	            <tr>
	                <td>Challan To Station:</td>
	                <td><input type ="text" name ="chlnToStn" id="chlnToStn" ng-model="challan.chlnToStn" value={{challan.chlnToStn}}  required readonly></td>
	            </tr> 
	            <tr>
	                <td>Employee Code:</td>
	                <td><input type ="text" name ="chlnEmpCode" id="chlnEmpCode" ng-model="challan.chlnEmpCode" value={{challan.chlnEmpCode}} required readonly></td>
	            </tr> 
	            <tr>
	                <td>No. Of Package:</td>
	                <td><input type ="text" name ="chlnNoOfPkg" ng-model="challan.chlnNoOfPkg" value={{challan.chlnNoOfPkg}} ng-minlength="1" ng-maxlength="7" ng-required="true" readonly></td>
	            </tr> 
	            <tr>
	                <td>Lorry Rate:</td>
	                <td><input class="col s6" type ="text" name ="chlnLryRate" ng-model="challan.chlnLryRate" value={{challan.chlnLryRate}} step="0.01" min="0.00" ng-required="true" readonly>
	                <select class="col s6" name="chlnLryRate1" id="chlnLryRate1" ng-model="chlnLryRatePer" ng-init="chlnLryRatePer = 'Kg'" required>
						<option value="Ton">Ton</option>
						<option value="Kg">Kg</option>
					</select></td>
	            </tr> 
	            <tr>
	                <td>Charge Weight:</td>
	                <td><input class="col s6" type ="text" name ="chlnChgWt" id="chlnChgWt" ng-model="challan.chlnChgWt" value={{challan.chlnChgWt}} step="0.01" min="0.00" ng-required="true" readonly>
	                <select class="col s6" name="chlnChgWt1" id="chlnChgWt1" ng-model="chlnChgWtPer" ng-init="chlnChgWtPer = 'Kg'" required>
						<option value="Ton">Ton</option>
						<option value="Kg">Kg</option>
					</select></td>
	            </tr>
	            <tr>
	                <td>Challan Freight:</td>
	                <td><input type ="text" name ="chlnFreight" id="chlnFreight" ng-model="challan.chlnFreight" value={{challan.chlnFreight}} step="0.01" min="0.00" ng-required="true" readonly></td>
	            </tr>
	            <tr>
	                <td>Loading Amount:</td>
	                <td><input type ="text" name ="chlnLoadingAmt" id="chlnLoadingAmt" ng-model="challan.chlnLoadingAmt" value={{challan.chlnLoadingAmt}} step="0.01" min="0.00" ng-minlength="1" ng-maxlength="9" ng-required="true" readonly></td>
	            </tr>
	            <tr>
	                <td> Challan Extra:</td>
	                <td><input type ="text" name ="chlnExtra" id="chlnExtra" ng-model="challan.chlnExtra" value={{challan.chlnExtra}} step="0.01" min="0.00" ng-minlength="1" ng-maxlength="7" ng-required="true" readonly></td>
	            </tr> 
	              <tr>
	                <td>Statistical Charge:</td>
	                <td><input type ="text" name ="chlnStatisticalChg"  ng-model="challan.chlnStatisticalChg" value={{challan.chlnStatisticalChg}} step="0.01" min="0.00" ng-minlength="1" ng-maxlength="7" ng-required="true" readonly></td>
	            </tr>
	            
	             <tr>
	                <td>Tds Amount:</td>
	                <td><input type ="text" name ="chlnTdsAmt" id="chlnTdsAmt" ng-model="challan.chlnTdsAmt" value={{challan.chlnTdsAmt}} step="0.01" min="0.00" ng-minlength="1" ng-maxlength="7" ng-required="true" readonly></td>
	            </tr>
	            <tr>
	                <td>Total Freight:</td>
	                <td><input type ="text" name ="chlnTotalFreight" id="chlnTotalFreight" ng-model="challan.chlnTotalFreight" value={{challan.chlnTotalFreight}} step="0.01" min="0.00" readonly></td>
	            </tr>
	            <tr>
	                <td>Challan Advance:</td>
	                <td><input type ="text" name ="chlnAdvance" id="chlnAdvance" ng-model="challan.chlnAdvance" value={{challan.chlnAdvance}} step="0.01" min="0.00" ng-minlength="1" ng-maxlength="7" ng-required="true" readonly></td>
	            </tr>
				<tr>
	                <td>Challan Balance:</td>
	                <td><input type ="text" name ="chlnBalance" id="chlnBalance" ng-model="challan.chlnBalance" value={{challan.chlnBalance}} step="0.01" min="0.00" readonly></td>
	            </tr>
	            <tr>
	                <td>Challan Pay At:</td>
	                <td><input type ="text" name ="chlnPayAt" id="chlnPayAt" ng-model="challan.chlnPayAt" value={{challan.chlnPayAt}} required readonly></td>
	            </tr>
	             <tr>
	                <td>Time Allow:</td>
	                <td><input type ="text" name ="chlnTimeAllow" id="chlnTimeAllow" ng-model="challan.chlnTimeAllow" value={{challan.chlnTimeAllow}} ng-minlength="1" ng-maxlength="2" readonly></td>
	            </tr>
	             <tr>
	                <td>Challan Date:</td>
	                <td>{{challan.chlnDt | date : format : timezone}}<!-- <input type ="date" name ="chlnDt" ng-model="challan.chlnDt" value={{challan.chlnDt}}> --></td>
	            </tr>
	             <tr>
	                <td>BR Rate:</td>
	                <td><input type ="text" name ="chlnBrRate" ng-model="challan.chlnBrRate" value={{challan.chlnBrRate}} step="0.01" min="0.00" ng-minlength="1" ng-maxlength="7" ng-required="true" readonly></td>
	            </tr>
	             <tr>
	                <td>Challan Total Weight:</td>
	                <td><input class="col s6" type ="text" name ="chlnTotalWt" id="chlnTotalWt" ng-model="challan.chlnTotalWt" value={{challan.chlnTotalWt}} step="0.01" min="0.00" ng-minlength="1" ng-maxlength="7" ng-required="true" readonly>
	                <select class="col s6" name="chlnTotalWt1" id="chlnTotalWt1" ng-model="chlnTotalWtPer" ng-init="chlnTotalWtPer = 'Kg'" required>
						<option value="Ton">Ton</option>
						<option value="Kg">Kg</option>
					</select></td>
	            </tr>
	             <tr>
	                <td>Weight Slip:</td>
	                <td><input class="col s6" type="text" name ="chlnWtSlip" id="chlnWtSlip" ng-model="challan.chlnWtSlip" value={{challan.chlnWtSlip}} readonly>
	                <select class="col s6" name ="chlnWtSlip" id="chlnWtSlip" ng-model="challan.chlnWtSlip" required>
						<option value = "Yes">Yes</option>
						<option value = "No">No</option>
					</select></td>
	            </tr>
	             <tr>
	                <td>Lorry Reporting Date:</td>
	                <td>{{challan.chlnLryRepDT | date : format : timezone}}<!-- <input type ="date" name ="chlnLryRepDT" ng-model="challan.chlnLryRepDT" value={{challan.chlnLryRepDT}}> --></td>
	            </tr>
	             <tr>
	                <td>Vehicle type:</td>
	                <td><input type ="text" name ="chlnVehicleType" id="chlnVehicleType" ng-model="challan.chlnVehicleType" value={{challan.chlnVehicleType}} required readonly></td>
	            </tr>
		        <tr>
	                <td>Challan RR No:</td>
	                <td><input type ="text" name ="chlnRrNo"  ng-model="challan.chlnRrNo" value={{challan.chlnRrNo}} disabled="disabled" readonly></td>
	            </tr>
	             <tr>
	                <td>Challan Train No:</td>
	                <td><input type ="text" name ="chlnTrainNo" ng-model="challan.chlnTrainNo" value={{challan.chlnTrainNo}} disabled="disabled" readonly></td>
	            </tr>
	             <tr>
	                <td>Lorry Load Time:</td>
	                <td><input type ="text" name ="chlnLryLoadTime" ng-model="challan.chlnLryLoadTime" value={{challan.chlnLryLoadTime}} required readonly></td>
	            </tr>
	        	  <tr>
	                <td>Lorry Reporting Time:</td>
	                <td><input type ="text" name ="chlnLryRptTime" ng-model="challan.chlnLryRptTime" value={{challan.chlnLryRptTime}} readonly></td>
	            </tr>
	            <tr><td><h4>Challan Detail</h4></td></tr>
	               <tr>
	                <td>Challan Detail Id:</td>
	                <td><input type ="text" name ="chdId" ng-model="challanDetail.chdId" value={{challanDetail.chdId}} readonly></td>
	            </tr>
	      		  <tr>
	                <td>Broker Code:</td>
	                 <td><input type ="text" name ="chdBrCode" ng-model="challanDetail.chdBrCode" value={{challanDetail.chdBrCode}} readonly required></td>
	            </tr>
	           <tr>
	                <td>Owner Code:</td>
	                 <td><input type ="text" name ="chdOwnCode" ng-model="challanDetail.chdOwnCode" value={{challanDetail.chdOwnCode}}  readonly required></td>
	            </tr>
	            <tr>
	                <td>Broker Mobile No:</td>
	                 <td><input type ="text" name ="chdBrMobNo" ng-model="challanDetail.chdBrMobNo" value={{challanDetail.chdBrMobNo}}  required></td>
	            </tr>
	             <tr>
	                <td>Owner Mobile No:</td>
	                 <td><input type ="text" name ="chdOwnMobNo" ng-model="challanDetail.chdOwnMobNo" value={{challanDetail.chdOwnMobNo}}  required></td>
	            </tr>
	            <tr>
	                <td>Driver Name:</td>
	                 <td><input type ="text" name ="chdDvrName" ng-model="challanDetail.chdDvrName" value={{challanDetail.chdDvrName}} ng-required="true"></td>
	            </tr>
	           <tr>
	                <td>Driver Mobile No.:</td>
	                 <td><input type ="text" name ="chdDvrMobNo" ng-model="challanDetail.chdDvrMobNo" value={{challanDetail.chdDvrMobNo}} ng-maxlength="10" ng-minlength="10" ng-required="true"></td>
	            </tr>
	            <tr>
	                <td>RC No</td>
	                 <td><input type ="text" name ="chdRcNo" ng-model="challanDetail.chdRcNo" value={{challanDetail.chdRcNo}} ng-minlength="3" ng-maxlength="20" ng-required="true"></td>
	            </tr>
	              <tr>
	                <td>RC Issue Date:</td>
	                 <td>{{challanDetail.chdRcIssueDt | date : format : timezone}}<!-- <input type ="date" name ="chdRcIssueDt" ng-model="challanDetail.chdRcIssueDt" value={{challanDetail.chdRcIssueDt}}> --></td>
	            </tr>
	           <tr>
	                <td>RC Valid Date:</td>
	                 <td>{{challanDetail.chdRcValidDt | date : format : timezone}}<!-- <input type ="date" name ="chdRcValidDt" ng-model="challanDetail.chdRcValidDt" value={{challanDetail.chdRcValidDt}}></td> -->
	            </tr>
	            <tr>
	                <td>DL No:</td>
	                 <td><input type ="text" name ="chdDlNo" ng-model="challanDetail.chdDlNo" value={{challanDetail.chdDlNo}} ng-minlength="3" ng-maxlength="20" ng-required="true" readonly></td>
	            </tr>
	      	    <tr>
	                <td>DL Issue Date:</td>
	                 <td>{{challanDetail.chdDlIssueDt | date : format : timezone}}<!-- <input type ="date" name ="chdDlIssueDt" ng-model="challanDetail.chdDlIssueDt" value={{challanDetail.chdDlIssueDt}}> --></td>
	            </tr>
	          
	           <tr>
	                <td>DL Valid Date:</td>
	                 <td>{{challanDetail.chdDlValidDt | date : format : timezone}}<!-- <input type ="date" name ="chdDlValidDt" ng-model="challanDetail.chdDlValidDt" value={{challanDetail.chdDlValidDt}}> --></td>
	            </tr>
	             <tr>
	                <td>Per NO:</td>
	                 <td><input type ="text" name ="chdPerNo" ng-model="challanDetail.chdPerNo" value={{challanDetail.chdPerNo}} ng-minlength="3" ng-maxlength="20" ng-required="true" readonly></td>
	            </tr>
	             <tr>
	                <td>Per Issue Date:</td>
	                 <td>{{challanDetail.chdPerIssueDt | date : format : timezone}}<!-- <input type ="date" name ="chdPerIssueDt" ng-model="challanDetail.chdPerIssueDt" value={{challanDetail.chdPerIssueDt}}> --></td>
	            </tr>
	            <tr>
	                <td>Per Valid Date:</td>
	                 <td>{{challanDetail.chdPerValidDt | date : format : timezone}}<!-- <input type ="date" name ="chdPerValidDt" ng-model="challanDetail.chdPerValidDt" value={{challanDetail.cchdPerValidDt}}> --></td>
	            </tr>
	            <tr>
	                <td>Per State:</td>
	                 <td><input type ="text" name ="chdPerState" ng-model="challanDetail.chdPerState" value={{challanDetail.chdPerState}} readonly required></td>
	            </tr>
	             <tr>
	                <td>Fit Doc No:</td>
	                 <td><input type ="text" name ="chdFitDocNo" ng-model="challanDetail.chdFitDocNo" value={{challanDetail.chdFitDocNo}} ng-minlength="3" ng-maxlength="20" ng-required="true" readonly></td>
	            </tr>
	             <tr>
	                <td>Fit Doc Issue Date:</td>
	                 <td>{{challanDetail.chdFitDocIssueDt | date : format : timezone}}<!-- <input type ="date" name ="chdFitDocIssueDt" ng-model="challanDetail.chdFitDocIssueDt" value={{challanDetail.chdFitDocIssueDt}}> --></td>
	            </tr>
	   	         <tr>
	                <td>Fit Doc Valid Date:</td>
	                 <td>{{challanDetail.chdFitDocValidDt | date : format : timezone}}<!-- <input type ="date" name ="chdFitDocValidDt" ng-model="challanDetail.chdFitDocValidDt" value={{challanDetail.chdFitDocValidDt}}> --></td>
	            </tr>
			       <tr>
	                <td>Fit Doc Place:</td>
	                 <td><input type ="text" name ="chdFitDocPlace" ng-model="challanDetail.chdFitDocPlace" value={{challanDetail.chdFitDocPlace}} ng-required="true" readonly></td>
	             </tr>
	             <tr>
	                <td>Bank Finance:</td>
	                 <td><input type ="text" name ="chdBankFinance" ng-model="challanDetail.chdBankFinance" value={{challanDetail.chdBankFinance}} ng-required="true" readonly></td>
	              </tr>
	           <tr>
	                <td>Policy No:</td>
	                 <td><input type ="text" name ="chdPolicyNo" ng-model="challanDetail.chdPolicyNo" value={{challanDetail.chdPolicyNo}} ng-minlength="3" ng-maxlength="20" ng-required="true" readonly></td>
	            </tr>
	   		     <tr>
	                <td>Policy Com:</td>
	                 <td><input type ="text" name ="chdPolicyCom" ng-model="challanDetail.chdPolicyCom" value={{challanDetail.chdPolicyCom}} ng-required="true" readonly></td>
	            </tr>
	               <tr>
	                <td>Transit Pass No:</td>
	                 <td><input type ="text" name ="chdTransitPassNo" ng-model="challanDetail.chdTransitPassNo" value={{challanDetail.chdTransitPassNo}} ng-required="true" readonly></td>
	            </tr>
	             <tr>
	                <td>Pass Hdr Type:</td>
	                 <td><input class="col s6" type ="text" name ="chdPanHdrType" ng-model="challanDetail.chdPanHdrType" value={{challanDetail.chdPanHdrType}}>
	                 <select class="col s6" name ="chdPanHdrType" id="chdPanHdrType" ng-model="challanDetail.chdPanHdrType" required>
					    <option value = "Broker">Broker</option>
					    <option value = "Owner">Owner</option>
				</select></td>
	   	            </tr>
	            <tr>
	                <td>Pan No:</td>
	                 <td><input type ="text" name ="chdPanNo" ng-model="challanDetail.chdPanNo" value={{challanDetail.chdPanNo}} ng-minlength="10" ng-maxlength="10" ng-pattern="/[a-zA-Z0-9]/" ng-required="true" readonly></td>
	              </tr>
	              <tr>
	                <td>Pan Hdr Name:</td>
	                 <td><input type ="text" name ="chdPanHdrName" ng-model="challanDetail.chdPanHdrName" value={{challanDetail.chdPanHdrName}} ng-required="true" readonly></td>
	              </tr>
	    		 <tr>
	                <td>Pan Issue St:</td>
	                 <td><input type ="text" name ="chdPanIssueSt" ng-model="challanDetail.chdPanIssueSt" value={{challanDetail.chdPanIssueSt}} required readonly></td>
	              </tr>
		         <tr>
	                <td>Challan Detail CreationTS:</td>
	                 <td><input type ="text" name ="chdId" ng-model="challanDetail.creationTS" value={{challanDetail.creationTS}} readonly></td>
	            </tr>
	             <tr>
	                <td>Challan CreationTS:</td>
	                 <td><input type ="text" name ="creationTS" ng-model="challan.creationTS" value={{challan.creationTS}} readonly></td>
	            </tr> 
	 		</table>
			 <div class="row">
      			<div class="input-field col s9">
      				<input class="teal white-text btn" type="file" file-model="challanImage"/>
      				<label>Choose File</label>
      			</div>
      
      		 	<div class="col s3">
       				<a class="btn waves-effect waves-light white-text col s12" type="button" ng-click="uploadChallanImage()">Upload</a>
     		 	</div> 
      		</div>
     			<input type="submit" value="Submit!">
		</form>
		</div>
	</div>
	<div class="col s3"> &nbsp; </div>
</div>


<div id ="challanDB" ng-hide="ChallanDBFlag">
  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search Challan Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Challan Code</th>
			</tr>
		  <tr ng-repeat="chlnCode in challanList | filter:filterTextbox">
		 	  <td><input type="radio"  name="chlnCode" id="cnmtId" value="{{ chlnCode }}" ng-model="$parent.chlnCode" ng-click="saveChallanCode(chlnCode)"></td>
              <td>{{ chlnCode }}</td>
          </tr>
         </table>
     </div>  
     
     
      <div id ="branchCodeDB" ng-hide="BranchCodeDBFlag">
	  <input type="text" name="filterBranchCode" ng-model="filterBranchCode.branchCode" placeholder="Search by Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Branch Code</th>
				
				<th>Branch Name</th>
			
				<th>Branch Pin</th>
			</tr>
		  <tr ng-repeat="branch in branchList | filter:filterBranchCode">
		 	  <td><input type="radio"  name="branchCode" id="branchId" value="{{ branch.branchCode }}" ng-model="branchCode" ng-click="saveBranchCode(branch)"></td>
              <td>{{ branch.branchCode }}</td>
              <td>{{ branch.branchName }}</td>
              <td>{{ branch.branchPin }}</td>
          </tr>
         </table>
     </div>  
     
     
     
     <div id ="challanFromStationDB" ng-hide = "ChallanFromStationDBFlag">
     <input type="text" name="filterChallanFromStation" ng-model="filterChallanFromStation.stnCode" placeholder="Search by Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	  
		  
		  <tr ng-repeat="station in stationList | filter:filterChallanFromStation">
		 	  <td><input type="radio"  name="stnName" id="stnName" class="stnName"  value="{{ station }}" ng-model="stnName"  ng-click="saveFrmStnCode(station)"></td>
              <td>{{ station.stnCode }}</td>
              <td>{{ station.stnName }}</td>
              <td>{{ station.stnDistrict }}</td>
          </tr>
         </table>
   	</div>
   	
   	
   	<div id ="challanToStationDB" ng-hide = "ChallanToStationDBFlag">
	<input type="text" name="filterChallanToStation" ng-model="filterChallanToStation.stnCode" placeholder="Search by Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	  
		  
		  <tr ng-repeat="station in stationList | filter:filterChallanToStation">
		 	  <td><input type="radio"  name="stnName" id="stnName" class="stnName"  value="{{ station}}" ng-model="stnName1" ng-click="saveToStnCode(station)"></td>
              <td>{{ station.stnCode }}</td>
              <td>{{ station.stnName }}</td>
              <td>{{ station.stnDistrict }}</td>
          </tr>
         </table>
   	</div>
		
		
		<div id ="employeeCodeDB" ng-hide="EmployeeCodeDBFlag">
	<input type="text" name="filterEmployeeCode" ng-model="filterEmployeeCode.empCode" placeholder="Search by Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Employee Code</th>
			
				<th>Employee Name</th>
			</tr>
 	 		  
		  <tr ng-repeat="employee in employeeList | filter:filterEmployeeCode">
		 	  <td><input type="radio"  name="empCode" id="empId"  value="{{ employee.empCode }}" ng-model="empCode" ng-click="saveEmployeeCode(employee)"></td>
              <td>{{ employee.empCode }}</td>
              <td>{{ employee.empName }}</td>
          </tr>
         </table>
   	</div>
   	
   
   	
   	 <div id ="payAtDB" ng-hide="PayAtDBFlag">
   	 <input type="text" name="filterPay" ng-model="filterPay.branchCode" placeholder="Search by Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Branch Code</th>
				
				<th>Branch Name</th>
			
				<th>Branch Pin</th>
			</tr>
		  <tr ng-repeat="branch in branchList | filter:filterPay">
		 	  <td><input type="radio"  name="branchCode" id="branchId" value="{{ branch.branchCode }}" ng-model="branchCode" ng-click="savePayAt(branch)"></td>
              <td>{{ branch.branchCode }}</td>
              <td>{{ branch.branchName }}</td>
              <td>{{ branch.branchPin }}</td>
          </tr>
         </table>
     </div>
     
     
     
     <div id ="vehicleTypeDB" ng-hide = "VehicleTypeDBFlag">
		<input type="text" name="filterVehicleType" ng-model="filterVehicleType.vtCode" placeholder="Search by Code">
	
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type</th>
				<th>Vehicle Type Code</th>
			</tr>
		  <tr ng-repeat="vt in vtList | filter:filterVehicleType">
		 	  <td><input type="radio"  name="vtCode" id="vtId"  value="{{ vt }}" ng-model="vtCode" ng-click="saveVehicleType(vt)"></td>
              <td>{{ vt.vtVehicleType }}</td>
              <td>{{ vt.vtCode }}</td>
          </tr>
         </table>  
         </div>   
         
         
     <div id ="chdBrCodeDB" ng-hide = "ChdBrCodeDBFlag">
         <input type="text" name="filterChdBrCode" ng-model="filterChdBrCode.brkCode" placeholder="Search by Code">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Broker Code</th>
				<th>Broker PP No.</th>
			    <th>Broker Pan Name</th>
			</tr>
		  <tr ng-repeat="brk in brkList | filter:filterChdBrCode">
		 	  <td><input type="radio"  name="brkCode" id="brkId"  value="{{ brk }}" ng-model="brkCode" ng-click="saveBrokerCode(brk)"></td>
              <td>{{  brk.brkCode }}</td>
              <td>{{  brk.brkPPNo }}</td>
              <td>{{  brk.brkPanName }}</td>
          </tr>
         </table>  
       </div> 
         
          
          
      <div id ="chdOwnCodeDB" ng-hide = "ChdOwnCodeDBFlag">
      		<input type="text" name="filterChdOwnCode" ng-model="filterChdOwnCode.ownCode" placeholder="Search by Code">
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Owner Code</th>
				<th>Owner PP No.</th>
			    <th>Owner Pan Name</th>
			</tr>
		  <tr ng-repeat="own in ownList | filter:filterChdOwnCode">
		 	  <td><input type="radio"  name="ownCode" id="ownId"  value="{{ own }}" ng-model="ownCode" ng-click="saveOwnerCode(own)"></td>
              <td>{{  own.ownCode }}</td>
              <td>{{  own.ownPPNo }}</td>
              <td>{{  own.ownPanName }}</td>
          </tr>
         </table>  
     </div> 
     
     
     <div id ="chdPerStateCodeDB" ng-hide="ChdPerStateCodeDBFlag"> 
	   <input type="text" name="filterChdPerStateCode" ng-model="filterChdPerStateCode.stateCode" placeholder="Search by Code">
 	   	<table>
 	 		<tr>
				<th></th>
				<th>State Code</th>
			</tr>
		  <tr ng-repeat="state in stList | filter:filterChdPerStateCode">
		 	  <td><input type="radio"  name="stateCode" id="stateId" value="{{ state.stateCode }}" ng-model="stateCode" ng-click="savePerStateCode(state)"></td>
              <td>{{ state.stateCode }}</td>
         </tr>
         </table>
	  	 </div> 
	  	 
	  	 
	  	  <div id ="chdPanIssueStDB" ng-hide="ChdPanIssueStDBFlag"> 
          <input type="text" name="filterChdPanIssueSt" ng-model="ChdPanIssueSt.stateCode" placeholder="Search by Code">
 	   	<table>
 	 		<tr>
				<th></th>
				<th>State Code</th>
			</tr>
		  <tr ng-repeat="state in stList | filter:ChdPanIssueSt">
		 	  <td><input type="radio"  name="stateCode" id="stateId" value="{{ state.stateCode }}" ng-model="stateCode" ng-click="savePanIssueStateCode(state)"></td>
              <td>{{ state.stateCode }}</td>
         </tr>
         </table>
	  	 </div>   
	  	 </div>
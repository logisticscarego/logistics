<div ng-show="operatorLogin || superAdminLogin">
<!-- Loading div -->
	<div ng-show="lodingFlag" style=" position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
		<div style="margin-left:500px; margin-top:100px;">
			<img src="resources/img/loading.gif"  id="img" width="388px" height="104px"/>
		</div>
	</div>
		<div class="row">
		<form name="voucherForm" ng-submit=voucherSubmit(voucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly  >
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		    	<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.lhpvStatus.lsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div>
			   		 <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code">Voucher Type</label>	
		       		</div>
		       		<div class="col s4 input-field">
				    		<select name="payBy" id="payById" ng-model="vs.payBy" ng-change="selectPayBy()" ng-init="vs.payBy = 'C'" ng-required="true">
								<option value='C'>BY CASH</option>
								<option value='Q'>BY CHEQUE</option>
								<option value='R'>BY RTGS</option>
							</select>
							<label>Payment By</label>
					</div>
		    </div>
		    
		    <div class="row">
		     		<div class="col s4 input-field">
			    		<select name="chequeType" id="chequeTypeId" ng-model="vs.chequeType" ng-change="selectChqType()" disabled="disabled">
							<option value='C'>Computerized</option>
							<option value='M'>Manual</option>
						</select>
						<label>Cheque Type </label>
					</div>
					
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bankCodeId" name ="bankCode" ng-model="vs.bankCode"  disabled="disabled">
		       			<label for="code">Bank Code</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="chequeNoId" name ="chequeNo" ng-model="vs.chequeLeaves.chqLChqNo" disabled="disabled">
		       			<label for="code">Cheque No</label>	
		       		</div>
		    </div>
		    
		    <!-- <div class="row">
		    	<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="brkOwnId" name ="brkOwnName" ng-model="vs.brkOwnFaCode" ng-click="openBrkOwnDB()" readonly ng-required="true">
		       			<label for="code">Broker/Owner</label>	
		       			<label for="code">Pan Card Holderr</label>
		       	</div>
		       	
		       	<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="challanId" name ="challanName" ng-model="challan" ng-click="openChlnDB()" readonly ng-required="true">
		       			<label for="code">Select Challan Code</label>	
		       	</div>
		    </div> -->
		    
		    <div class="row">
		    	<div class="col s4 input-field">
		       			<input class="validate" type ="text" id="brkOwnId" name ="brkOwnName" ng-model="vs.brkOwnFaCode" ng-click="notifyMsg()" readonly ng-required="true">
		       			<label for="code">Pan Card Holder</label>
		       	</div>
		       	
		       	<div class="col s4 input-field">
		       			<input class="validate" type ="text" id="challanId" name ="challanName" ng-model="challan" ng-required="true">
		       			<label for="code">Enter Challan Code</label>	
		       	</div>
		       	
		       	<div class="col s4 input-field">
	       				<input class="col s12 btn teal white-text" id="verfChln" type ="button" value="Verify Challan" ng-click="verifyChallan()" required readonly >
	       		</div>
		    </div>
		  
		  
		  <div class="row"> 		
				 <div ng-show="lhpvBalList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">LHPV BALANCE DETAILS</caption>
						<tr class="rowclr">
							<th class="colclr">Chln No.</th>
							<th class="colclr">Lry Bal</th>
							<th class="colclr">Cash Dis</th>
							<th class="colclr">Muns</th>
							<th class="colclr">TDS</th>
							<th class="colclr">Wt Sh</th>
							<th class="colclr">Dr(Claim)</th>
							<th class="colclr">Late Del</th>
							<th class="colclr">Late Ack.</th>
							<th class="colclr">Ex Km</th>
							<th class="colclr">Ovr Hgt</th>
							<th class="colclr">Penalty</th>
							<th class="colclr">Oth</th>
							<th class="colclr">Det</th>
							<th class="colclr">Unld</th>
							<!-- <th class="colclr">Pay Amt</th>
							<th class="colclr">Recvr Amt</th> -->
							<th class="colclr">Final Amt</th>
							<th class="colclr">Desc</th>
							<th class="colclr">Action</th>
						</tr>
						<tr class="tbl" ng-repeat="lhpvbal in lhpvBalList">
							<td class="rowcel">{{lhpvbal.challan.chlnCode}}</td>
							<td class="rowcel">{{lhpvbal.lbLryBalP}}</td>
							<td class="rowcel">{{lhpvbal.lbCashDiscR}}</td>
							<td class="rowcel">{{lhpvbal.lbMunsR}}</td>
							<td class="rowcel">{{lhpvbal.lbTdsR}}</td>
							<td class="rowcel">{{lhpvbal.lbWtShrtgCR}}</td>
							<td class="rowcel">{{lhpvbal.lbDrRcvrWtCR}}</td>
							<td class="rowcel">{{lhpvbal.lbLateDelCR}}</td>
							<td class="rowcel">{{lhpvbal.lbLateAckCR}}</td>
							<td class="rowcel">{{lhpvbal.lbOthExtKmP}}</td>
							<td class="rowcel">{{lhpvbal.lbOthOvrHgtP}}</td>
							<td class="rowcel">{{lhpvbal.lbOthPnltyP}}</td>
							<td class="rowcel">{{lhpvbal.lbOthMiscP}}</td>
							<td class="rowcel">{{lhpvbal.lbUnpDetP}}</td>
							<td class="rowcel">{{lhpvbal.lbUnLoadingP}}</td>
							<!-- <td class="rowcel">{{lhpvbal.lbTotPayAmt}}</td>
							<td class="rowcel">{{lhpvbal.lbTotRcvrAmt}}</td> -->
							<td class="rowcel">{{lhpvbal.lbFinalTot}}</td>
							<td class="rowcel">{{lhpvbal.lbDesc}}</td>
							 <td class="rowcel"><input type="button" value="Remove" ng-click="removeLhpvBal($index,lhpvbal)" /></td>
						</tr>
						<tr class="tbl" >							
							<td class="rowcel" colspan="15">Total Amount</td>
							<td class="rowcel">{{lbFinalTotSum}}</td>
							<td class="rowcel"></td>
							<td class="rowcel"></td>							
						</tr>
					</table>
				</div>
			</div>	
			
			  
		   
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	      </div>		
	     
	</form>
	</div>   
	
	
	
	<!-- <div id ="bankCodeDB" ng-hide="bankCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Bank Code</th>
 	  	  	  <th>Bank Name</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bnk in bnkList | filter:filterTextbox">
		 	  <td><input type="radio"  name="bnk"   value="{{ bnk }}" ng-model="bkCode" ng-click="saveBankCode(bnk.bnkFaCode)"></td>
              <td>{{bnk.bnkFaCode}}</td>
              <td>{{bnk.bnkName}}</td>
              
          </tr>
      </table> 
	</div> -->
	
	
	
	<!-- <div id ="chlnDB" ng-hide="chlnDBFlag" class="noprint">
		  <input type="text" name="filterChlnbox" ng-model="filterChlnbox" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Challan Code</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="chln in chlnList | filter:filterChlnbox ">
		 	  <td><input type="radio"  name="chln"   value="{{ chln }}" ng-model="chlnCode" ng-click="saveChln(chln)"></td>
              <td>{{chln}}</td>
          </tr>
      </table> 
	</div> -->
	
	
	
	<div id="lhpvBalDB" ng-hide="lhpvBalDBFlag">
	<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="lhpvBalForm" ng-submit="submitLhpvBal(lhpvBalForm,lhpvBal)">
		 <div class="row">
			    <div class="col s4 input-field">
					<input class="validate" type ="number" name="lryAdvName" id="lryAdvId" ng-model="lhpvBal.lbLryBalP" ng-required="true" >
					<label>Lorry Balance</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="csDisName" id="csDisId" ng-model="lhpvBal.lbCashDiscR" ng-requires="true"/>
					<label>Cash Discount</label>
				</div>
				
				 <div class="col s4 input-field">
					<input type ="number" name="munsName" id="munsId" ng-model="lhpvBal.lbMunsR" ng-required="true" >
					<label>Munsiana</label>
				</div>
	     </div>
	     
	     <div class="row">
			    <div class="col s4 input-field">
					<input type ="number" name="tdsAmt" id="tdsAmtId" ng-model="lhpvBal.lbTdsR" />
					<label>TDS Amount</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="wtSrtg" id="wtSrtgId" ng-model="lhpvBal.lbWtShrtgCR"  required="required"  />
					<label>Wt Shortage</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="lbDrRcvrWtCRName" id="lbDrRcvrWtCRId" ng-model="lhpvBal.lbDrRcvrWtCR" required="required" />
					<label>Driver(Claim)</label>
				</div>

	     </div>
	     
	     
	     <div class="row">
	     
	    		<div class="col s4 input-field">
					<input type ="number" name="ltDelName" id="ltDelId" ng-model="lhpvBal.lbLateDelCR" />
					<label>Late Delivery</label>
				</div>
			    <div class="col s4 input-field">
					<input type ="number" name="ltAckName" id="ltAckId" ng-model="lhpvBal.lbLateAckCR" />
					<label>Late Ack.</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="exKmName" id="exKmId" ng-model="lhpvBal.lbOthExtKmP" />
					<label>Extra KM</label>
				</div>
			
	     </div>
	     
	      <div class="row">
	      		<div class="col s4 input-field">
					<input type ="number" name="ovrHgtName" id="ovrHgtId" ng-model="lhpvBal.lbOthOvrHgtP"  required="required" />
					<label>Over Height</label>
				</div>
	      
			    <div class="col s4 input-field">
					<input type ="number" name="penName" id="penId" ng-model="lhpvBal.lbOthPnltyP" />
					<label>Penalty</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="othName" id="othId" ng-model="lhpvBal.lbOthMiscP" />
					<label>Others Misc</label>
				</div>
			
	     </div>
	     
	     <div class="row">
	     
	     		<div class="col s4 input-field">
					<input type ="number" name="detName" id="detId" ng-model="lhpvBal.lbUnpDetP"  required="required"/>
					<label>Detention</label>
				</div>
				
	     		<div class="col s4 input-field">
					<input type ="number" name="unLdgName" id="unLdgId" ng-model="lhpvBal.lbUnLoadingP" required="required"/>
					<label>Unloading</label>
				</div>
				
	     		<div class="col s4 input-field">
					<input type ="number" name="payAmt" id="payAmtId" ng-model="lhpvBal.lbTotPayAmt" />
					<label>Payment Amount</label>
				</div>
			
	     </div>
	     
	     <div class="row">
	     	<div class="col s4 input-field">
					<input type ="number" name="tdsAmt" id="tdsAmtId" ng-model="lhpvBal.lbTotRcvrAmt" />
					<label>Recovery Amount</label>
			</div>
	     
	     	<div class="col s4 input-field">
					<input type ="number" name="finalTotAmt" id="finalTotAmtId" ng-model="lhpvBal.lbFinalTot" />
					<label>Final Total Amount</label>
			</div>
			
			<div class="col s4 input-field" ng-show="tdsPerFlag">
		    		<!-- <select name="tdsPerName" id="tdsPerId" ng-model="tdsPer" ng-init="tdsPer = 1" ng-change="changeTdsPer()" >
					</select> -->
						<input type ="number" name="tdsPerName" id="tdsPerId" ng-model="tdsPer" />
					<label>Tds Percent</label>
			</div>
	     </div>
	     
	    <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="lhpvBal.lbDesc"></textarea>
 					<label>Description</label>
				</div>
         </div>	
	     
	     
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       </div>		
	     
	</form>
	</div>
	
	
	
	<div id="saveVsDB" ng-hide="saveVsFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{vs.branch.branchFaCode}}</td>
				</tr>
				
				<tr>
					<td>Branch Name</td>
					<td>{{vs.branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Date</td>
					<td>{{dateTemp}}</td>
				</tr>
				
				<tr>
					<td>Sheet No</td>
					<td>{{vs.lhpvStatus.lsNo}}</td>
				</tr>
				
				<tr>
					<td>Voucher Type</td>
					<td>{{vs.voucherType}}</td>
				</tr>
				
				<tr>
					<td>Payment By</td>
					<td>{{vs.payBy}}</td>
				</tr>
				
				<tr>
					<td>Cheque Type</td>
					<td>{{vs.chequeType}}</td>
				</tr>
				
				<tr>
					<td>Bank Code</td>
					<td>{{vs.bankCode}}</td>
				</tr>
				
				<tr>
					<td>Cheque No</td>
					<td>{{vs.chequeLeaves.chqLChqNo}}</td>
				</tr>
				
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" id="saveId" ng-click="saveVS()"/>
		</div>
	</div>
	
	
	
	
</div>
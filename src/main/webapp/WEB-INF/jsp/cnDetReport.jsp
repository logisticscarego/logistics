
<div ng-show="adminLogin || superAdminLogin">

<div ng-show="loadingFlag" style=" text-align:center; margin-left: -15px; margin-top: -50px; position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
        <div style="margin-left:20px; margin-top:100px;">
           <svg width='200px' height='200px'  viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-gears">
             <rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect>
                <g transform="translate(-20,-20)">
                      <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#2bbbad ">
                         <animateTransform attributeName="transform" type="rotate" from="90 50 50" to="0 50 50" dur="0.8s" repeatCount="indefinite"></animateTransform>
                       </path>
                 </g>
                 <g transform="translate(20,20) rotate(15 50 50)">
                     <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#0A3F4E ">
                          <animateTransform attributeName="transform" type="rotate" from="0 50 50" to="90 50 50" dur="0.8s" repeatCount="indefinite">
                          </animateTransform>
                      </path>
                  </g>
            </svg>
        </div>
       <span style=" color: #ffffff ;font-size:30px; font-weight:bold; letter-spacing:3px;">Loading...</span>
</div>

<div class="row">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
	<div class="col s8 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
		<table>
			<tr>
				<td></td>
				<td>From Date</td>
				<td>To Date</td>
				<td></td>
			</tr>
			
			<tr>	
				<td></td>
 	 			<td>
 	 				<input type="date" class="validate" id="stDtId" name="stDtName" ng-model="strtDt"/>	
 	 			</td>	
 	 			<td>
 	 				<input type="date" class="validate" id="eDtId" name="toDtName" ng-model="endDt"/>	
 	 			</td>	
 	 			<td></td>	
			</tr>
			
		
 	 		<tr>
 	 			<td>
 	 				<input type="radio" name="btn" ng-click="getCmpRep()"/>
 	 			</td>
 	 			<td>
 	 				<input type="text" id="compId" name="compName" ng-model="comp" disabled="disabled"/>
 	 			</td>	
 	 			<td>
					<input type="button" id="subAllId" value="Submit" ng-click="submitAll()" disabled="disabled"/>
				</td>	
				<td>
					<input type="button" id="allCnDetId" value="DOWNLOAD" ng-click="downloadExl()" disabled="disabled"/>
				</td>
			</tr>
			
			<tr>
 	 			<td>
 	 				<input type="radio" name="btn" ng-click="getCnRepCust()"/>
 	 			</td>
 	 			<td>
 	 				<input type="text" name="custName" id="custId"
					ng-model="customerName" placeholder="Select Customer" ng-click="openCustDB()" 
					disabled="disabled" readonly>	
 	 			</td>
 	 			<td>
					<input type="button" id="subCustId" value="Submit" ng-click="submitCust()" disabled="disabled"/>
				</td>			
				<td>
					<input type="button" id="allCnDetCustId" value="DOWNLOAD" ng-click="downloadExl()" disabled="disabled"/>
				</td>
			</tr>
			
			<tr>
 	 			<td>
 	 				<input type="radio" name="btn" ng-click="getCnRepBrh()"/>
 	 			</td>
 	 			<td>
 	 				<input type="text" name="brhName" id="brhId"
					ng-model="branchName" placeholder="Select Branch" ng-click="openBrhDB()" 
					disabled="disabled" readonly>	
					
 	 			</td>		
 	 			<td>
					<input type="button" id="subBrhId" value="Submit" ng-click="submitBrh()" disabled="disabled"/>
				</td>
				<td>
					<input type="button" id="allCnDetBrhId" value="DOWNLOAD" ng-click="downloadExl()" disabled="disabled"/>
				</td>
			</tr>
			
			
			
		</table>
	</div>
</div>	


<div id ="custDB" ng-hide="custDBFlag">
	  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search....">
	  <table>
	  	  <tr>
	  	  	  <th></th>
	  	  	  <th> Name </th>
	  	  	  <th> FaCode </th>
	  	  </tr>
	  
	  <tr ng-repeat="cust in custList | filter:filterTextbox">
	 	  <td><input type="radio"  name="cust" value="{{ cust }}" ng-model="custCode" ng-click="saveCustomer(cust)"></td>
             <td>{{cust.custName}}</td>
             <td>{{cust.custFaCode}}</td>
         </tr>
     </table> 
</div>


<div id ="brhDB" ng-hide="brhDBFlag">
	  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search....">
	  <table>
	  	  <tr>
	  	  	  <th></th>
	  	  	  <th> Name </th>
	  	  	  <th> FaCode </th>
	  	  </tr>
	  
	  <tr ng-repeat="brh in brhList | filter:filterTextbox">
	 	  <td><input type="radio"  name="brh" value="{{ brh }}" ng-model="brhCode" ng-click="saveBranch(brh)"></td>
             <td>{{brh.branchName}}</td>
             <td>{{brh.branchFaCode}}</td>
         </tr>
     </table> 
</div>



<div class="col s12" id="exportable">
	<table class="table-condensed" ng-show="cnDetList.length > 0">	
		<tr>
			<th style="border:1px solid #000000;">Cnmt No</th>
		    <th style="border:1px solid #000000;">Cnmt Date</th>
		    <th style="border:1px solid #000000;">From Station</th>
		    <th style="border:1px solid #000000;">To Station</th>
		    <th style="border:1px solid #000000;">Consignor</th>
		    <th style="border:1px solid #000000;">Consignee</th>
		    <th style="border:1px solid #000000;">Billing Party</th>
		    <th style="border:1px solid #000000;">No Of Package</th>
		    <th style="border:1px solid #000000;">Actual WT</th>
		    <th style="border:1px solid #000000;">Wt Chg For Billing</th>  
		    <th style="border:1px solid #000000;">Per MT Rate</th>
		    <th style="border:1px solid #000000;">Freight(CNMT)</th>
		    <th style="border:1px solid #000000;">Challan No</th>
		    <th style="border:1px solid #000000;">Challan Date</th>
		    <th style="border:1px solid #000000;">Per MT Cost</th>
		    <th style="border:1px solid #000000;">Lorry Hire</th>
		    <th style="border:1px solid #000000;">Advance</th>
		    <th style="border:1px solid #000000;">Balance</th>
		    <th style="border:1px solid #000000;">Any Other Charges</th>
		    <th style="border:1px solid #000000;">Total Hire Freight</th>
		    <th style="border:1px solid #000000;">Broker Name</th>
		    <th style="border:1px solid #000000;">Broker Contact No</th>
		    <th style="border:1px solid #000000;">Owner Name</th>
		    <th style="border:1px solid #000000;">Owner Contact No.</th>
		    <th style="border:1px solid #000000;">Owner Address</th>
		    <th style="border:1px solid #000000;">Owner Pan No</th>
		    <th style="border:1px solid #000000;">Driver Name</th>
		    <th style="border:1px solid #000000;">Driver Contact No</th>
		    <th style="border:1px solid #000000;">Invoice No</th>
		    <th style="border:1px solid #000000;">Value Of Goods</th>
		    <th style="border:1px solid #000000;">Transit Time</th>
		    <th style="border:1px solid #000000;">Expected Dt Of Delivery</th> 
		    <th style="border:1px solid #000000;">Bill No</th> 
		    <th style="border:1px solid #000000;">Bill Amt</th>
		</tr>
		
		<tr ng-repeat="cnDet in cnDetList">
		
			<td style="border:1px solid #000000;">{{ cnDet.cnmtNo }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.cnmtDt | date : format : timezone}}</td>
			<td style="border:1px solid #000000;">{{ cnDet.frStn }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.toStn }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.consignor }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.consignee }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.blngParty }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.noOfPckg }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.actWt }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.wtChrg }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.prMtRt }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.cnmtFreight }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.chlnNo }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.chlnDt | date : format : timezone}}</td>
			<td style="border:1px solid #000000;">{{ cnDet.prMtCost }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.lryHire }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.adv }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.bal }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.anyOthChrg }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.totHireChrg }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.brkName }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.brkCntList[0] }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.ownName }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.ownCntList[0] }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.ownAdd }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.ownPan }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.driverName }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.drvCntNo }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.invList[0].invoiceNo }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.vog }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.tnsTime }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.expDtOfDly | date : format : timezone}}</td>
			<td style="border:1px solid #000000;">{{ cnDet.billNo }}</td>
			<td style="border:1px solid #000000;">{{ cnDet.blAmt }}</td>
		</tr>
		
		
		
		<!-- <tr ng-repeat="cc in ccList">
			<td>
				<div ng-repeat="cnmt in cnmtList">
					<div ng-if="cnmt.cnmtId == cc.cnmtId">
						{{ cnmt.cnmtCode }}
					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="cnmt in cnmtList">
					<div ng-if="cnmt.cnmtId == cc.cnmtId">
						{{ cnmt.cnmtDt | date : format : timezone }}
					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="cnmt in cnmtList">
					<div ng-if="cnmt.cnmtId == cc.cnmtId">
						<div ng-repeat="cust in custList">
							<div ng-if="cust.custCode == cnmt.cnmtConsignor">
								{{ cust.custName }}
							</div>
						</div>
					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="cnmt in cnmtList">
					<div ng-if="cnmt.cnmtId == cc.cnmtId">
						<div ng-repeat="cust in custList">
							<div ng-if="cust.custCode == cnmt.cnmtConsignee">
								{{ cust.custName }}
							</div>
						</div>
					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="cnmt in cnmtList">
					<div ng-if="cnmt.cnmtId == cc.cnmtId">
						{{ cnmt.cnmtActualWt / 1000 }}
					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="cnmt in cnmtList">
					<div ng-if="cnmt.cnmtId == cc.cnmtId">
						{{ cnmt.cnmtGuaranteeWt / 1000 }}
					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="cnmt in cnmtList">
					<div ng-if="cnmt.cnmtId == cc.cnmtId">
						{{ cnmt.cnmtNoOfPkg }}
					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="cnmt in cnmtList">
					<div ng-if="cnmt.cnmtId == cc.cnmtId">
						<div ng-repeat="cnmtInv in cnmt.cnmtInvoiceNo">
							{{ cnmtInv.invoiceNo }}
						</div>
 					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="cnmt in cnmtList">
					<div ng-if="cnmt.cnmtId == cc.cnmtId">
							{{ cnmt.cnmtVOG }}
 					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="cnmt in cnmtList">
					<div ng-if="cnmt.cnmtId == cc.cnmtId">
						<div ng-repeat="stnt in stntList">
							<div ng-if="stnt.stnCode == cnmt.cnmtFromSt">
								{{ stnt.stnName }}
							</div>
						</div>
					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="cnmt in cnmtList">
					<div ng-if="cnmt.cnmtId == cc.cnmtId">
						<div ng-repeat="stnt in stntList">
							<div ng-if="stnt.stnCode == cnmt.cnmtToSt">
								{{ stnt.stnName }}
							</div>
						</div>
					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="chln in chlnList">
					<div ng-if="chln.chlnId == cc.chlnId">
							{{ chln.chlnTimeAllow }}
 					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="cnmt in cnmtList">
					<div ng-if="cnmt.cnmtId == cc.cnmtId">
							{{ cnmt.cnmtDtOfDly | date : format : timezone}}
 					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="chln in chlnList">
					<div ng-if="chln.chlnId == cc.chlnId">
							{{ chln.chlnCode}}
 					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="chln in chlnList">
					<div ng-if="chln.chlnId == cc.chlnId">
							{{ chln.chlnDt |  date : format : timezone }}
 					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="chln in chlnList">
					<div ng-if="chln.chlnId == cc.chlnId">
							{{ chln.chlnFreight }}
 					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="chln in chlnList">
					<div ng-if="chln.chlnId == cc.chlnId">
							{{ chln.chlnFreight - chln.chlnTotalFreight}}
 					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="chln in chlnList">
					<div ng-if="chln.chlnId == cc.chlnId">
							{{ chln.chlnTotalFreight }}
 					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="chln in chlnList">
					<div ng-if="chln.chlnId == cc.chlnId">
							{{ chln.chlnAdvance }}
 					</div>
				</div>
			</td>
			
			<td>
				<div ng-repeat="chln in chlnList">
					<div ng-if="chln.chlnId == cc.chlnId">
							{{ chln.chlnBalance }}
 					</div>
				</div>
			</td>
			
		</tr> -->
	</table>
	
	
	<!-- <div class="row">
		<div class="col s12 center">
			<input type="submit" value="Submit" ng-click="submit()" />
		</div>
		
		<div class="col s12 center">
			<input type="button" value="download excel" ng-click="downloadExl()" />
		</div>
	</div> -->

</div>
</div>

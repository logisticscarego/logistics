<div ng-show="operatorLogin || superAdminLogin">

<div class="row">
		<form class="col s12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);"  name="brhWiseForm" ng-submit="SubmitBranch(brhWiseForm)">
  	
		  	 <div class="row">
			      <div class="input-field col s3">
			       <input class="validate" type ="text" name ="branch" ng-model="branch.branchName" readonly ng-click="openBranchDB()">
			        <label>Select Branch</label>
			      </div>
		    </div>
		    
		    <div class="row">
		    	<div class="input-field col s3">
				        <input type ="text"  name="brDirector"  ng-model="temp.branchDirector" readonly="true">
				        <label>Director</label>
				        <input type ="hidden" ng-model="branch.branchDirector">
				 </div>
				 <div class="col s3">
				       <div class="input-field col s6">	
				        	<input  type="radio"  name ="branchDirector" ng-model="clrBtnBranchDirectorNE" ng-click="branchDirectorRadio()">
							<label>New</label>
				  		</div>
				     	<div class="input-field col s6">	
				        	<input  type="radio"  name ="branchDirector" ng-model="clrBtnBranchDirectorEE" ng-click="openBranchDirectorDB()">
							<label>Existing</label>
						</div>
				 </div>  
		   
		    
		        <div class="input-field col s3">
			        <input type ="text" name="brMarketingHD" ng-model="temp.branchMarketingHD" readonly="true">
			        <label>Marketing HD</label>
			        <input type ="hidden" ng-model="branch.branchMarketingHD">
			    </div>
			    <div class="col s3">
			      	<div class="input-field col s6">	
			        	<input  type="radio"  name ="branchMarketingHD" ng-model="clrBtnbranchMarketingHDNE" ng-click="branchMarketingHDRadio()">
						<label>New</label>
					</div>
			        <div class="input-field col s6">	
			        	<input  type="radio" name ="branchMarketingHD" ng-model="clrBtnbranchMarketingHDEE" ng-click="openBranchMarketingHDDB()">
						<label>Existing</label>
					</div>
			    </div>
			     
			 </div>
			 
			 <div class="row">
			 	<div class="input-field col s3">
			        <input type ="text" name="brOutStandingHD" ng-model="temp.branchOutStandingHD" readonly="true">
			       	<label>Outstanding HD</label>
			       	<input type ="hidden" ng-model="branch.branchOutStandingHD">
			    </div>
			    <div class="col s3">
			      	<div class="input-field col s6">	
			        	<input  type="radio"  name ="branchOutStandingHD" ng-model="clrBtnBranchOutStandingHDNE" ng-click="branchOutStandingHDRadio()">
						<label>New</label>
					</div>
			        <div class="input-field col s6">	
			        	<input  type="radio" name ="branchOutStandingHD" ng-model="clrBtnBranchOutStandingHDEE" ng-click="openBranchOutStandingHDDB()" >
						<label>Existing</label>
					</div>
			     </div>
			 
			 	  <div class="input-field col s3">
				        <input type ="text" name="brMarketing"  ng-model="temp.branchMarketing" readonly="true">
				        <label>Marketing</label>
				        <input type ="hidden"  ng-model="branch.branchMarketing">
			      </div>
			      
			      <div class="col s3">
			      	<div class="input-field col s6">	
			        	<input  type="radio"  name ="branchMarketing" ng-model="clrBtnBranchMarketingNE" ng-click="branchMarketingRadio()">
						<label>New</label>
					</div>
			        <div class="input-field col s6">	
			        	<input  type="radio" name ="branchMarketing"  ng-model="clrBtnBranchMarketingEE" ng-click="openBranchMarketingDB()">
						<label>Existing</label>
					</div>
			      </div>
			 
			 </div>
			 
			 
			 <div class="row">
			 	<div class="input-field col s3">
			        <input class="validate" type ="text" name="brMngr" ng-model="temp.branchMngr" readonly="true">
			        <label for="branchMngr">Manager</label>
			        <input type ="hidden" ng-model="branch.branchMngr">
			    </div>
			    <div class="col s3">
			      	<div class="input-field col s6">	
			        	<input type="radio"  name ="branchMngr" ng-model="clrBtnBranchMngrNE" ng-click="branchMngrRadio()">
						<label>New</label>
					</div>
			     
			        <div class="input-field col s6">	
			        	<input type="radio" name ="branchMngr" ng-model="clrBtnBranchMngrEE" ng-click="openBranchMngrDB()">
						<label>Existing</label>
					</div>
			    </div>
			    
				<div class="input-field col s3">
				        <input type ="text" name="brCashier" ng-model="temp.branchCashier" readonly="true">
				        <label>Cashier</label>
				        <input type ="hidden"  ng-model="branch.branchCashier">
				</div>
				<div class="col s3">
				   	<div class="input-field col s6">	
				        	<input type="radio"  name ="branchCashier" ng-model="clrBtnBranchCashierNE" ng-click="branchCashierRadio()">
							<label>New</label>
					</div>
					<div class="input-field col s6">	
					  	<input type="radio" name ="branchCashier" ng-model="clrBtnBranchCashierEE" ng-click="openBranchCashierDB()">
								<label>Existing</label>
					</div>
				</div>
			    
			 </div>
			 
			 
			 <div class="row">
			 	<div class="input-field col s3">
			        <input type ="text" name="brTraffic"  ng-model="temp.branchTraffic" readonly="true">
			        <label>Traffic</label>
			        <input type ="hidden"  ng-model="branch.branchTraffic">
			    </div>
			    <div class="col s3">
			      	<div class="input-field col s6">	
			        	<input type="radio"  name ="branchTraffic" ng-model="clrBtnBranchTrafficNE" ng-click="branchTrafficRadio()">
						<label>New</label>
					</div>
			        <div class="input-field col s6">	
			        	<input type="radio" name ="branchTraffic" ng-model="clrBtnBranchTrafficEE" ng-click="openBranchTrafficDB()" >
						<label>Existing</label>
					</div>
			    </div>
			 
			 	<div class="input-field col s3">
				        <input type ="text" name="brAreaMngr" ng-model="temp.branchAreaMngr" readonly="true">
				        <label>Area Manager</label>
				        <input type ="hidden" ng-model="branch.branchAreaMngr">
				 </div>
				 <div class="col s3">
				        
				     	<div class="input-field col s6">	
				        	<input type="radio"  name ="branchAreaMngr" ng-model="clrBtnBranchAreaMngrNE" ng-click="branchAreaMngrRadio()">
							<label>New</label>
						</div>
				     
				        <div class="input-field col s6">	
				        	<input type="radio" name ="branchAreaMngr" ng-model="clrBtnBranchAreaMngrEE" ng-click="openBranchAreaMngrDB()" >
							<label>Existing</label>
						</div>
				 </div>
			 
			 </div>
			 
			  <div class="row">
				      <div class="input-field col s3">
					        <input type ="text" name="brRegionalMngr" ng-model="temp.branchRegionalMngr" readonly="true">
					        <label>Regional Manager</label>
					        <input type ="hidden"  ng-model="branch.branchRegionalMngr">
				      </div>  
				      <div class="col s3">
					      <div class="input-field col s6">	
					        	<input type="radio"  name ="branchRegionalMngr" ng-model="clrBtnBranchRegionalMngrNE" ng-click="branchRegionalMngrRadio()">
								<label>New</label>
					       </div>
					     
					       <div class="input-field col s6">	
					        	<input type="radio" name ="branchRegionalMngr" ng-model="clrBtnBranchRegionalMngrEE" ng-click="openBranchRegionalMngrDB()" >
								<label>Existing</label>
					       </div>
					   </div>    
			   </div>	  
			 
			   <div class="row">
      			 	<div class="col s12 center">
      		 	 		<input type="submit" value="Submit">
      		   </div>
			 
		</form>
		
</div>

<div id="openBrDB" ng-hide="openBrDBFlag">
		<input type="text" name="filterBrhName" ng-model="filterBrhName" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Branch Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="branch in branchList | filter:filterBrhName">
					<td><input type="radio" name="branch" value="{{ branch }}" ng-model="branchCode"
							ng-click="saveBranch(branch)"></td>
					<td>{{ branch.branchName }}</td>
					<td>{{ branch.branchFaCode }}</td>
				</tr>
			</table>
</div>


<div id="newEmpFrDr" ng-hide="newEmpFrDrFlag">
		<input type="text"  ng-model="filterNewDr" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="newEmp in newEmpList | filter:filterNewDr">
					<td><input type="radio" name="newEmp" value="{{ newEmp }}" ng-model="newEmpName"
							ng-click="saveNewDr(newEmp)"></td>
					<td>{{ newEmp.empName }}</td>
					<td>{{ newEmp.empFaCode }}</td>
				</tr>
			</table>
</div>


<div id="existEmpFrDr" ng-hide="existEmpFrDrFlag">
		<input type="text"  ng-model="filterExsDr" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="existEmp in existEmpList | filter:filterExsDr">
					<td><input type="radio" name="existEmp" value="{{ existEmp }}" ng-model="existEmpName"
							ng-click="saveExsDr(existEmp)"></td>
					<td>{{ existEmp.empName }}</td>
					<td>{{ existEmp.empFaCode }}</td>
				</tr>
			</table>
</div>


<div id="newEmpFrBmh" ng-hide="newEmpFrBmhFlag">
		<input type="text"  ng-model="filterNewBmh" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="newEmp in newEmpList | filter:filterNewBmh">
					<td><input type="radio" name="newEmp" value="{{ newEmp }}" ng-model="newEmpName"
							ng-click="saveNewBmh(newEmp)"></td>
					<td>{{ newEmp.empName }}</td>
					<td>{{ newEmp.empFaCode }}</td>
				</tr>
			</table>
</div>


<div id="existEmpFrBmh" ng-hide="existEmpFrBmhFlag">
		<input type="text"  ng-model="filterExsBmh" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="existEmp in existEmpList | filter:filterExsBmh">
					<td><input type="radio" name="existEmp" value="{{ existEmp }}" ng-model="existEmpName"
							ng-click="saveExsBmh(existEmp)"></td>
					<td>{{ existEmp.empName }}</td>
					<td>{{ existEmp.empFaCode }}</td>
				</tr>
			</table>
</div>


<div id="newEmpFrOsh" ng-hide="newEmpFrOshFlag">
		<input type="text"  ng-model="filterNewOsh" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="newEmp in newEmpList | filter:filterNewOsh">
					<td><input type="radio" name="newEmp" value="{{ newEmp }}" ng-model="newEmpName"
							ng-click="saveNewOsh(newEmp)"></td>
					<td>{{ newEmp.empName }}</td>
					<td>{{ newEmp.empFaCode }}</td>
				</tr>
			</table>
</div>


<div id="existEmpFrOsh" ng-hide="existEmpFrOshFlag">
		<input type="text"  ng-model="filterExsOsh" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="existEmp in existEmpList | filter:filterExsOsh">
					<td><input type="radio" name="existEmp" value="{{ existEmp }}" ng-model="existEmpName"
							ng-click="saveExsOsh(existEmp)"></td>
					<td>{{ existEmp.empName }}</td>
					<td>{{ existEmp.empFaCode }}</td>
				</tr>
			</table>
</div>


<div id="newEmpFrBmk" ng-hide="newEmpFrBmkFlag">
		<input type="text"  ng-model="filterNewBmk" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="newEmp in newEmpList | filter:filterNewBmk">
					<td><input type="radio" name="newEmp" value="{{ newEmp }}" ng-model="newEmpName"
							ng-click="saveNewBmk(newEmp)"></td>
					<td>{{ newEmp.empName }}</td>
					<td>{{ newEmp.empFaCode }}</td>
				</tr>
			</table>
</div>


<div id="existEmpFrBmk" ng-hide="existEmpFrBmkFlag">
		<input type="text"  ng-model="filterExsBmk" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="existEmp in existEmpList | filter:filterExsBmk">
					<td><input type="radio" name="existEmp" value="{{ existEmp }}" ng-model="existEmpName"
							ng-click="saveExsBmk(existEmp)"></td>
					<td>{{ existEmp.empName }}</td>
					<td>{{ existEmp.empFaCode }}</td>
				</tr>
			</table>
</div>

<div id="newEmpFrBmg" ng-hide="newEmpFrBmgFlag">
		<input type="text"  ng-model="filterNewBmg" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="newEmp in newEmpList | filter:filterNewBmg">
					<td><input type="radio" name="newEmp" value="{{ newEmp }}" ng-model="newEmpName"
							ng-click="saveNewBmg(newEmp)"></td>
					<td>{{ newEmp.empName }}</td>
					<td>{{ newEmp.empFaCode }}</td>
				</tr>
			</table>
</div>


<div id="existEmpFrBmg" ng-hide="existEmpFrBmgFlag">
		<input type="text"  ng-model="filterExsBmg" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="existEmp in existEmpList | filter:filterExsBmg">
					<td><input type="radio" name="existEmp" value="{{ existEmp }}" ng-model="existEmpName"
							ng-click="saveExsBmg(existEmp)"></td>
					<td>{{ existEmp.empName }}</td>
					<td>{{ existEmp.empFaCode }}</td>
				</tr>
			</table>
</div>


<div id="newEmpFrCsh" ng-hide="newEmpFrCshFlag">
		<input type="text"  ng-model="filterNewCsh" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="newEmp in newEmpList | filter:filterNewCsh">
					<td><input type="radio" name="newEmp" value="{{ newEmp }}" ng-model="newEmpName"
							ng-click="saveNewCsh(newEmp)"></td>
					<td>{{ newEmp.empName }}</td>
					<td>{{ newEmp.empFaCode }}</td>
				</tr>
			</table>
</div>


<div id="existEmpFrCsh" ng-hide="existEmpFrCshFlag">
		<input type="text"  ng-model="filterExsCsh" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="existEmp in existEmpList | filter:filterExsCsh">
					<td><input type="radio" name="existEmp" value="{{ existEmp }}" ng-model="existEmpName"
							ng-click="saveExsCsh(existEmp)"></td>
					<td>{{ existEmp.empName }}</td>
					<td>{{ existEmp.empFaCode }}</td>
				</tr>
			</table>
</div>

<div id="newEmpFrTrf" ng-hide="newEmpFrTrfFlag">
		<input type="text"  ng-model="filterNewTrf" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="newEmp in newEmpList | filter:filterNewTrf">
					<td><input type="radio" name="newEmp" value="{{ newEmp }}" ng-model="newEmpName"
							ng-click="saveNewTrf(newEmp)"></td>
					<td>{{ newEmp.empName }}</td>
					<td>{{ newEmp.empFaCode }}</td>
				</tr>
			</table>
</div>


<div id="existEmpFrTrf" ng-hide="existEmpFrTrfFlag">
		<input type="text"  ng-model="filterExsTrf" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="existEmp in existEmpList | filter:filterExsTrf">
					<td><input type="radio" name="existEmp" value="{{ existEmp }}" ng-model="existEmpName"
							ng-click="saveExsTrf(existEmp)"></td>
					<td>{{ existEmp.empName }}</td>
					<td>{{ existEmp.empFaCode }}</td>
				</tr>
			</table>
</div>

<div id="newEmpFrAmg" ng-hide="newEmpFrAmgFlag">
		<input type="text"  ng-model="filterNewAmg" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="newEmp in newEmpList | filter:filterNewAmg">
					<td><input type="radio" name="newEmp" value="{{ newEmp }}" ng-model="newEmpName"
							ng-click="saveNewAmg(newEmp)"></td>
					<td>{{ newEmp.empName }}</td>
					<td>{{ newEmp.empFaCode }}</td>
				</tr>
			</table>
</div>


<div id="existEmpFrAmg" ng-hide="existEmpFrAmgFlag">
		<input type="text"  ng-model="filterExsAmg" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="existEmp in existEmpList | filter:filterExsAmg">
					<td><input type="radio" name="existEmp" value="{{ existEmp }}" ng-model="existEmpName"
							ng-click="saveExsAmg(existEmp)"></td>
					<td>{{ existEmp.empName }}</td>
					<td>{{ existEmp.empFaCode }}</td>
				</tr>
			</table>
</div>


<div id="newEmpFrRmg" ng-hide="newEmpFrRmgFlag">
		<input type="text"  ng-model="filterNewRmg" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="newEmp in newEmpList | filter:filterNewRmg">
					<td><input type="radio" name="newEmp" value="{{ newEmp }}" ng-model="newEmpName"
							ng-click="saveNewRmg(newEmp)"></td>
					<td>{{ newEmp.empName }}</td>
					<td>{{ newEmp.empFaCode }}</td>
				</tr>
			</table>
</div>

<div id="existEmpFrRmg" ng-hide="existEmpFrRmgFlag">
		<input type="text"  ng-model="filterExsRmg" placeholder="Search...">
			<table>
				<tr>
					<th></th>
					<th>Employee Name</th>
					<th>FACode</th>
				</tr>
				<tr	ng-repeat="existEmp in existEmpList | filter:filterExsRmg">
					<td><input type="radio" name="existEmp" value="{{ existEmp }}" ng-model="existEmpName"
							ng-click="saveExsRmg(existEmp)"></td>
					<td>{{ existEmp.empName }}</td>
					<td>{{ existEmp.empFaCode }}</td>
				</tr>
			</table>
</div>


<div id="viewBranchDetailsDB" ng-hide="viewBranchDetailsFlag">
	<div class="row">
	<div class="col s12 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
		<h4>Here's the details of <span class="teal-text text-lighten-2">{{branch.branchName}}</span> branch:</h4>
		<table class="table-hover table-bordered table-condensed">
				
				<tr>
	                <td>Name:</td>
	                <td>{{branch.branchName}}</td>
	            </tr>
	            
	            <tr>
	                <td>Director:</td>
	                <td>{{temp.branchDirector}}</td>
	                
	            </tr>
	            <tr>
	                <td>Marketing HD:</td>
	                <td>{{temp.branchMarketingHD}}</td>
	            </tr>
	            <tr>
	                <td>Outstanding HD:</td>
	                <td>{{temp.branchOutStandingHD}}</td>
	            </tr>
	            <tr>
	                <td>Marketing:</td>
	                <td>{{temp.branchMarketing}}</td>
	            </tr>
	            <tr>
	                <td>Manager:</td>
	                <td>{{temp.branchMngr}}</td>
	            </tr>
	            <tr>
	                <td>Cashier:</td>
	                <td>{{temp.branchCashier}}</td>
	            </tr>
	            <tr>
	                <td>Traffic:</td>
	                <td>{{temp.branchTraffic}}</td>
	            </tr>
	            <tr>
	                <td>Area Manager:</td>
	                <td>{{temp.branchAreaMngr}}</td>
	            </tr>
	            <tr>
	                <td>Regional Manager:</td>
	                <td>{{temp.branchRegionalMngr}}</td>
	            </tr>
	           
	   </table>
	   
<input type="button" value="Save" id="saveId" ng-click="saveBrWise(branch)">
<input type="button" value="Cancel" ng-click="closeViewBranchDetailsDB()">
		
</div>	

</div>

</div>


</div>
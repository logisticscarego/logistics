<div ng-show="operatorLogin || superAdminLogin">
	<style>
	
	.tbl{text-align:center; 
		  color: #B5DAD4;
		
		border:1px solid rgba(255, 255, 255, 0.47);
		-moz-border-right: 1px solid rgba(255, 255, 255, 0.47);
		-webkit-border-right: 1px solid rgba(255, 255, 255, 0.47);}
		
		.rowcelwhite
		{
		text-align: center;
		color: #fff;
		border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    padding: 0px;
	    
	    }
	    .rowcelheader
	    {
		text-align: center;
	    color: #B5DAD4;
	    border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    font-weight:bold;
	    padding:3px;
	    
	    }
	    
		.align{
	    text-align:center;
	    font-size:20px;
	    }
	    .cursor
	    {
	    cursor:pointer;
	    }
		:hover.cursor
		{
		background-color:rgba(73, 156, 145, 0.25);
		}
		
	</style>
	<div class="row">
		<form name="countForm" ng-submit="submitCount()" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
					       	
		       	<div class="col s3 input-field">
		    		<input type="text" id="branchCode" name="branchCode" ng-model="branchCode" ng-click="openBranchDB()" readonly="readonly">
					<label>Select Branch</label>
				</div>
		       	
		       	<div class="col s3 input-field">		       		
		       		<input type="date" id="date" name="date" ng-model="date" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
		       		<label for="code">Date</label>	
		       	</div>
		       	   	
		       	<div class="col s3 input-field">
	       			<input class="col s12 btn teal white-text" type ="submit" id="bnkAddId" name ="bnkAdd" value="Submit" >
	       		</div>
			
			</div>
		</form>
		
	</div>
	
	<div id ="branchDB" ng-hide="branchDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="branch in branchList | filter:filterTextbox1">
		 	  <td><input type="radio" name="branch" value="{{ branch }}" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table> 
	</div>	
	
	<!-- LhpvAdvList -->
	<div ng-show="countFlag">
		<table class="table">
 	    	<caption class="coltag tblrow">App Entry Detail</caption> 
	 	    <tr>	 	    	
 	  	  	  	<th class="rowcelheader">CNMT</th>
 	  	  	  	<th class="rowcelheader">Challan</th>
 	  	  	  	<th class="rowcelheader">Owner</th>
 	  	  	  	<th class="rowcelheader">Broker</th>
 	  	  	  	<th class="rowcelheader">Vehicle</th>	  	  
 	  	  </tr>
 	  	  <tr>		 	  
              <td class="rowcelwhite">{{ count.cnmtCount }}</td>
              <td class="rowcelwhite">{{ count.chlnCount }}</td>
              <td class="rowcelwhite">{{ count.ownCount }}</td>
              <td class="rowcelwhite">{{ count.brkCount }}</td>
              <td class="rowcelwhite">{{ count.vhCount }}</td>              
          </tr>
      </table> 
	</div>	

</div>
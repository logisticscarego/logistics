<div ng-show="operatorLogin || superAdminLogin">

	<!-- Loading div -->
	<div ng-show="lodingFlag" style=" position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
		<div style="margin-left:500px; margin-top:100px;">
			<img src="resources/img/loading.gif"  id="img" width="388px" height="104px"/>
		</div>
	</div>

	<div class="row">
		<form name="voucherForm" ng-submit=voucherSubmit(voucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<!-- <div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly  >
		       			<label for="code">Date</label>	
		       		</div> -->
		    </div>
		    
		    
		    
		    
		    <div class="row">
		    	<!-- <div class="col s3 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.lhpvStatus.lsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div> -->
			   		 <div class="col s3 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code">Voucher Type</label>	
		       		</div>
		       		<div class="col s3 input-field">
			    		<select name="detailType" id="detailTypeId" ng-model="detailType" ng-change="selectDetailType()" ng-required="true">
							<option value='bank'>BANK</option>
							<option value='card'>CARD</option>
						</select>
						<label>BANK/CARD </label>
					</div>
					<div class="col s3 input-field" ng-hide="cardButtonDB">
	       				<input class="col s12 btn teal white-text" id="cardDetailId" type ="button" value="Card Detail" ng-click="addCardDetail()"  readonly >
	       		</div>
		       		
		       		<div class="col s3 input-field" ng-hide="bankButtonDB">
			    		<select name="acntHldrName" id="acntHldrId" ng-model="vs.acntHldr" ng-change="selectAcntHldr()" ng-required="cardDetailDB">
							<option value='Owner'>Owner</option>
							<option value='Broker'>Broker</option>
						</select>
						<label>Pay To</label>
					</div>
		       		
		       		<!-- 	<div class="col s3 input-field" ng-hide="bankButtonDB">
	       				<input class="col s12 btn teal white-text" id="bankDetailId" type ="button" value="Bank Detail" ng-click="addBankDetail()"  readonly >
	       		</div> -->
		       		
		    </div>
		    
		    <div class="row" ng-hide="bankDetailDB">
		     		
		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="actBankNameId" name ="actBankName" ng-model="vs.actBankName"  readonly ng-required="cardDetailDB">
		       			<label for="code">Bank Name</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="actIfscCodeId" name ="actIfscCodeName" ng-model="vs.actIfscCode"  readonly ng-required="cardDetailDB" >
		       			<label for="code">IFSC Code</label>	
		       		</div>

		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="actAccountNoId" name ="actAccountNoName" ng-model="vs.actAccountNo" readonly ng-required="cardDetailDB" >
		       			<label for="code">A/C No</label>	
		       		</div>
		       		
		       			<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="actPayeeNameId" name ="actPayeeName" ng-model="vs.actPayeeName" readonly ng-required="cardDetailDB" >
		       			<label for="code">Payee Name</label>	
		       		</div>
		    </div>
		    


    <div class="row" ng-hide="cardDetailDB">
		     		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="actCardNameId" name ="actCardName" ng-model="vs.actBankName"  readonly ng-required="bankDetailDB">
		       			<label for="code">Card Name</label>	
		       		</div>
		       		

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="actCardNoId" name ="actCardNoName" ng-model="vs.actAccountNo" readonly ng-required="bankDetailDB" >
		       			<label for="code">Card No</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="actCardFaCodeId" name ="actCardFaCodeName" ng-model="vs.cardCode" readonly ng-required="bankDetailDB" >
		       			<label for="code">Card FaCode</label>	
		       		</div>
		       		
		    </div>



		    
		     <div class="row">
		    	<div class="col s4 input-field">
		       			<input class="validate" type ="text" id="brkOwnId" name ="brkOwnName" ng-model="vs.brkOwnFaCode" ng-click="notifyMsg()" readonly ng-required="true">
		       			<label for="code">Pan Card Holder</label>
		       	</div>
		       	
		       	<div class="col s4 input-field">
		       			<input class="validate" type ="text" id="challanId" name ="challanName" ng-model="challan" ng-required="true">
		       			<label for="code">Enter Challan Code</label>	
		       	</div>
		       	
		       	<div class="col s4 input-field">
	       				<input class="col s12 btn teal white-text" id="verfChln" type ="button" value="Verify Challan" ng-click="verifyChallan()" required readonly >
	       		</div>
		    </div>
		   
		  
		  
		    
	     
	
	  <div class="row"> 		
				 <div ng-show="lhpvBalList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">LHPV BALANCE DETAILS</caption>
						<tr class="rowclr">
							<th class="colclr">Chln No.</th>
							<th class="colclr">Lry Bal</th>
							<th class="colclr">Cash Dis</th>
							<th class="colclr">Muns</th>
							<th class="colclr">TDS</th>
							<th class="colclr">Wt Sh</th>
							<th class="colclr">Dr(Claim)</th>
							<th class="colclr">Late Del</th>
							<th class="colclr">Late Ack.</th>
							<th class="colclr">Ex Km</th>
							<th class="colclr">Ovr Hgt</th>
							<th class="colclr">Penalty</th>
							<th class="colclr">Oth</th>
							<th class="colclr">Det</th>
							<th class="colclr">Unld</th>
							<!-- <th class="colclr">Pay Amt</th>
							<th class="colclr">Recvr Amt</th> -->
							<th class="colclr">Final Amt</th>
							<th class="colclr">Desc</th>
							<th class="colclr">Action</th>
						</tr>
						<tr class="tbl" ng-repeat="lhpvbal in lhpvBalList">
							<td class="rowcel">{{lhpvbal.challan.chlnCode}}</td>
							<td class="rowcel">{{lhpvbal.lbLryBalP}}</td>
							<td class="rowcel">{{lhpvbal.lbCashDiscR}}</td>
							<td class="rowcel">{{lhpvbal.lbMunsR}}</td>
							<td class="rowcel">{{lhpvbal.lbTdsR}}</td>
							<td class="rowcel">{{lhpvbal.lbWtShrtgCR}}</td>
							<td class="rowcel">{{lhpvbal.lbDrRcvrWtCR}}</td>
							<td class="rowcel">{{lhpvbal.lbLateDelCR}}</td>
							<td class="rowcel">{{lhpvbal.lbLateAckCR}}</td>
							<td class="rowcel">{{lhpvbal.lbOthExtKmP}}</td>
							<td class="rowcel">{{lhpvbal.lbOthOvrHgtP}}</td>
							<td class="rowcel">{{lhpvbal.lbOthPnltyP}}</td>
							<td class="rowcel">{{lhpvbal.lbOthMiscP}}</td>
							<td class="rowcel">{{lhpvbal.lbUnpDetP}}</td>
							<td class="rowcel">{{lhpvbal.lbUnLoadingP}}</td>
							<!-- <td class="rowcel">{{lhpvbal.lbTotPayAmt}}</td>
							<td class="rowcel">{{lhpvbal.lbTotRcvrAmt}}</td> -->
							<td class="rowcel">{{lhpvbal.lbFinalTot}}</td>
							<td class="rowcel">{{lhpvbal.lbDesc}}</td>
							 <td class="rowcel"><input type="button" value="Remove" ng-click="removeLhpvBal($index,lhpvbal)" /></td>
						</tr>
						<tr class="tbl" >							
							<td class="rowcel" colspan="15">Total Amount</td>
							<td class="rowcel">{{lbFinalTotSum}}</td>
							<td class="rowcel"></td>
							<td class="rowcel"></td>							
						</tr>
					</table>
				</div>
			</div>	
		
	 <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       </div>		
	     
	</form>
	</div>
	
	
	
	<div id="lhpvBalBankDB" ng-hide="lhpvBalBankDBFlag">
	<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="lhpvBankForm" ng-submit="submitBankDetail(lhpvBankForm)">
		 <div class="row">
			    <div class="col s6 input-field">
					<input class="validate" type ="text" name="bankName" id="bankNameId" ng-model="bankName" ng-required="true">
					<label>Bank Name</label>
				</div>
				
			     <div class="col s6 input-field">
					<input class="validate" type ="text" name="payeeName" id="payeeNameId" ng-model="payeeName" ng-required="true">
					<label>Payee Name</label>
				</div>
				
	     </div>
	     
	     
	     <div class="row">
	     
	    		 <div class="col s6 input-field">
					<input class="validate" type ="password" name="ifscCodeName" id="ifscCodeId" ng-model="ifscCode" ng-required="true">
					<label>IFSC Code</label>
				</div>
				
				<div class="col s6 input-field">
					<input class="validate" type ="text"  name="ifscVCodeName" id="ifscVCodeId" ng-model="ifscVCode"  ng-required="true"  >
					<label>Verify IFSC Code</label>
				</div>	
			
	     </div>
	     
	      <div class="row">
	      		 <div class="col s6 input-field">
					<input class="validate" type ="password" name="accountNoName" id="accountNoId" ng-model="accountNo" ng-required="true">
					<label>Account NO</label>
				</div>
				
				<div class="col s6 input-field">
					<input class="validate" type ="text"  name="accountVNoName" id="accountVNoId" ng-model="accountVNo"  ng-required="true"  >
					<label>Verify Account NO</label>
				</div>
	     </div>
	     
	          <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	      </div>		
	     
		</form>
	</div>
	     
	
	
	
	
	<div id="lhpvBalCardDB" ng-hide="lhpvBalCardDBFlag">
	<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="lhpvCardForm" ng-submit="submitCardDetail(lhpvCardForm)">
		 <div class="row">
<!-- 			    <div class="col s12 input-field"> -->
<!-- 					<input class="validate" type ="text" name="cardName" id="cardNameId" ng-model="cardName" ng-required="true"> -->
<!-- 					<label>Card Name</label> -->
<!-- 				</div> -->
				<div class="col s12 input-field">
<!-- 			    		<select name="cardName" id="cardNameId" ng-model="cardName" ng-change="selectCardName()" ng-required="true"> -->
						<select name="cardName" id="cardNameId" ng-model="cardName" ng-change="selectCardName()" ng-required="true">
							<option value='HP'>HP</option>
							<option value='BP'>BP</option>
							<option value='IO'>IO</option>
						</select>
						<label>Card Name </label>
					</div>
	     </div>
	     
	     
	     <div class="row">
	     	<div class="col s12 input-field">
					<input class="validate" type ="text" name="cardNoName" id="cardNoId" ng-model="cardNo" ng-click="getCardNo()" readonly ng-required="true">
					<label>Card NO.</label>
				</div>
	     </div>
	     
	      <div class="row">
	      		 
				
				<div class="col s12 input-field">
					<input class="validate" type ="text"  name="cardVNoName" id="cardVNoId" ng-model="cardVNo"  ng-required="true"  >
					<label>card FaCode</label>
				</div>
	     </div>
	     
	          <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	      </div>		
	     
		</form>
	</div>
	
	
	
	
	
		<div id="lhpvBalDB" ng-hide="lhpvBalDBFlag">
	<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="lhpvBalForm" ng-submit="submitLhpvBal(lhpvBalForm,lhpvBal)">
		 <div class="row">
			    <div class="col s4 input-field">
					<input class="validate" type ="number" name="lryAdvName" id="lryAdvId" ng-model="lhpvBal.lbLryBalP" ng-required="true"  step="0.01" min="0.00" readonly/>
					<label>Lorry Balance</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="csDisName" id="csDisId" ng-model="lhpvBal.lbCashDiscR"  ng-required="true" step="0.01" min="0.00" readonly/>
					<label>Cash Discount</label>
				</div>
				
				 <div class="col s4 input-field">
					<input type ="number" name="munsName" id="munsId" ng-model="lhpvBal.lbMunsR"  ng-required="true" step="0.01" min="0.00" readonly/>
					<label>Munsiana</label>
				</div>
	     </div>
	     
	     <div class="row">
			    <div class="col s4 input-field">
					<input type ="number" name="tdsAmt" id="tdsAmtId" ng-model="lhpvBal.lbTdsR" ng-keyup="chngTDS()" step="0.01" min="0.00" readonly="readonly"/>
					<label>TDS Amount</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="wtSrtg" id="wtSrtgId" ng-model="lhpvBal.lbWtShrtgCR"  step="0.01" min="0.00" required="required" readonly />
					<label>Wt Shortage</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="lbDrRcvrWtCRName" id="lbDrRcvrWtCRId" ng-model="lhpvBal.lbDrRcvrWtCR" ng-keyup="chngDR()" step="0.01" min="0.00" required="required" readonly/>
					<label>Driver(Claim)</label>
				</div>

	     </div>
	     
	     
	     <div class="row">
	     
	    		<div class="col s4 input-field">
					<input type ="number" name="ltDelName" id="ltDelId" ng-model="lhpvBal.lbLateDelCR" step="0.01" min="0.00" readonly/>
					<label>Late Delivery</label>
				</div>
			    <div class="col s4 input-field">
					<input type ="number" name="ltAckName" id="ltAckId" ng-model="lhpvBal.lbLateAckCR" step="0.01" min="0.00" readonly/>
					<label>Late Ack.</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="exKmName" id="exKmId" ng-model="lhpvBal.lbOthExtKmP"  step="0.01" min="0.00" required="required" readonly/>
					<label>Extra KM</label>
				</div>
			
	     </div>
	     
	      <div class="row">
	      		<div class="col s4 input-field">
					<input type ="number" name="ovrHgtName" id="ovrHgtId" ng-model="lhpvBal.lbOthOvrHgtP"  step="0.01" min="0.00" required="required" readonly/>
					<label>Over Height</label>
				</div>
	      
			    <div class="col s4 input-field">
					<input type ="number" name="penName" id="penId" ng-model="lhpvBal.lbOthPnltyP" step="0.01" min="0.00" readonly/>
					<label>Penalty</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="othName" id="othId" ng-model="lhpvBal.lbOthMiscP" step="0.01" min="0.00" readonly/>
					<label>Others Misc</label>
				</div>
			
	     </div>
	     
	     <div class="row">
	     
	     		<div class="col s4 input-field">
					<input type ="number" name="detName" id="detId" ng-model="lhpvBal.lbUnpDetP"  step="0.01" min="0.00" required="required" readonly/>
					<label>Detention</label>
				</div>
				
	     		<div class="col s4 input-field">
					<input type ="number" name="unLdgName" id="unLdgId" ng-model="lhpvBal.lbUnLoadingP"  step="0.01" min="0.00" required="required" readonly/>
					<label>Unloading</label>
				</div>
				
	     		<div class="col s4 input-field">
					<input type ="number" name="payAmt" id="payAmtId" ng-model="lhpvBal.lbTotPayAmt" step="0.01" min="0.00" readonly/>
					<label>Payment Amount</label>
				</div>
			
	     </div>
	     
	     <div class="row">
	     	<div class="col s4 input-field">
					<input type ="number" name="rcvrAmt" id="rcvrAmtId" ng-model="lhpvBal.lbTotRcvrAmt" step="0.01" min="0.00" readonly/>
					<label>Recovery Amount</label>
			</div>
	     
	     	<div class="col s4 input-field">
					<input type ="number" name="finalTotAmt" id="finalTotAmtId" ng-model="lhpvBal.lbFinalTot" step="0.01"  readonly/>
					<label>Final Total Amount</label>
			</div>
			
			<div class="col s4 input-field" ng-show="tdsPerFlag">
		    		<select name="tdsPerName" id="tdsPerId" ng-model="tdsPer" ng-init="tdsPer = 1" ng-change="changeTdsPer()" >
						<option value=1>1%</option>
						<option value=2>2%</option>
					</select>
					<label>Tds Percent</label>
			</div>
	     </div>
	     
<!-- 	    <div class="row"> -->
<!-- 				<div class="input-field col s12"> -->
<!-- 					<i class="mdi-editor-mode-edit prefix"></i> -->
<!--  					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="lhpvBal.lbDesc"></textarea> -->
<!--  					<label>Description</label> -->
<!-- 				</div> -->
<!--          </div>	 -->
	     
	     
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       </div>		
	     
	</form>
	</div>
	
	
	
	
	
	
	<div id="saveVsDB" ng-hide="saveVsFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{vs.branch.branchFaCode}}</td>
				</tr>
				
				<tr>
					<td>Branch Name</td>
					<td>{{vs.branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Date</td>
					<td>{{dateTemp}}</td>
				</tr>
				
				<tr>
					<td>Sheet No</td>
					<td>{{vs.lhpvStatus.lsNo}}</td>
				</tr>
				
				<tr>
					<td>Voucher Type</td>
					<td>{{vs.voucherType}}</td>
				</tr>
				
				<tr>
					<td>Bank Name</td>
					<td>{{vs.actBankName}}</td>
				</tr>
				
				<tr>
					<td>IFSC Code</td>
					<td>{{vs.actIfscCode}}</td>
				</tr>
				
				<tr>
					<td>Account No</td>
					<td>{{vs.actAccountNo}}</td>
				</tr>
				
				<tr>
					<td>Payee Name</td>
					<td>{{vs.actPayeeName}}</td>
				</tr>
				
				
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" id="saveId" ng-click="saveVS()"/>
		</div>
	</div>
	
	<div id ="petroCardDB" ng-hide="petroCardDBFlag" class="noprint">
		  <input type="text" name="filterPetrobox" ng-model="filterPetrobox" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Card Type</th>
 	  	  	  <th>Card No.</th>
 	  	  	  <th>FA Code</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="petro in petroList | filter:filterBObox ">
		 	  <td><input type="radio"  name="petro"   value="{{ petro }}" ng-model="petroFa" ng-click="savePetroCode(petro)"></td>
              <td>{{petro.cardType}}</td>
              <td>{{petro.cardNo}}</td>
              <td>{{petro.cardFaCode}}</td>
          </tr>
      </table> 
	</div>
   
</div>

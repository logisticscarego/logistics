<title>Rent Details</title>
<div ng-show="operatorLogin || superAdminLogin">
	<div class="row noprint">
		<form name="newRentForm" ng-submit=submit(newRentForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchId" name ="branchName" ng-model="branch2.branchName" ng-click="openBranchDB2()" readonly ng-required="true" >
		       			<label for="code">Branch</label>	
		       		</div>
		       		
		       	    <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="rentMLandLordNameId" name ="rentMLandLordName" ng-model="bankMstr.bnkName"  ng-click="openBankMstr()" ng-required="true" readOnly >
		       			<label for="code">Bank Name</label>	
		       		</div>
		      </div>
		 </form>
	</div>
	    <div id ="branchDB2" ng-hide="branchDBFlag2">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in branchList | filter:filterTextbox">
		 	  <td><input type="radio"  name="branch" value="{{ branch }}" ng-model="brnch" ng-click="saveBranch2(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
              
          </tr>
      </table> 
	</div>
	
 		    <div id ="bankDB" ng-hide="bankDBFlag">
		  <input type="text" name="filterTextbox2" ng-model="filterTextbox2" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Id </th>
 	  	  	  <th>Bank Name</th>
 	  	 	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="bankMstr in bankMstrList | filter:filterTextbox2">
		 	  <td><input type="radio"  name="bankMstr" value="{{ bankMstr }}" ng-model="bnkMstr" ng-click="saveBankMstr(bankMstr)"></td>
              <td>{{selBranchId}}</td>
              <td>{{bankMstr.bnkName}}</td>
              <td>{{bankMstr.bnkFaCode}}</td>
          </tr>
      </table> 
	</div> 
	
	<div ng-if="myRent.length>0">
	<table style="border-collapse: collapse;">
	<tr><th>RentMstr Id</th><th>LandLord Name</th><th>Rent Per Month</th><th>TDS</th><th>Description</th><th>Submit</th></tr>
	<tr ng-repeat="mrt in myRent">
	<td><input style="width:100px" type="text" ng-model="mrt.rmId" readonly="readonly"></td>
	<td><input type="text" ng-model="mrt.landLord" readonly="readonly"></td>
	<td><input style="width:150px" type="number" ng-model="mrt.rentPM"></td>
	<td><input style="width:150px" type="number" ng-model="tdsAmt"></td>
	<td><input style="width:150px" type="text" ng-model="desc" required></td>
	<td><input type="submit" value="Submit" ng-click="submitRentDetails(mrt,tdsAmt,$index,desc)"></td>

	</tr>
	</table>
	</div>
	
</div>		 
<div ng-show="operatorLogin || superAdminLogin">
<div ng-show="loadingFlag" style=" text-align:center; margin-left: -15px; margin-top: -50px; position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
        <div style="margin-left:20px; margin-top:100px;">
           <svg width='200px' height='200px'  viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-gears">
             <rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect>
                <g transform="translate(-20,-20)">
                      <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#2bbbad ">
                         <animateTransform attributeName="transform" type="rotate" from="90 50 50" to="0 50 50" dur="0.8s" repeatCount="indefinite"></animateTransform>
                       </path>
                 </g>
                 <g transform="translate(20,20) rotate(15 50 50)">
                     <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#0A3F4E ">
                          <animateTransform attributeName="transform" type="rotate" from="0 50 50" to="90 50 50" dur="0.8s" repeatCount="indefinite">
                          </animateTransform>
                      </path>
                  </g>
            </svg>
        </div>
       <span style=" color: #ffffff ;font-size:30px; font-weight:bold; letter-spacing:3px;">Loading...</span>
</div>

	<div class="row">
		<form name="btbFTForm" ng-submit=FundTraSubmit(btbFTForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: #F2FAEF;"
			ng-class = "{'form-error': btbFTForm.$invalid && isFormSubmitted}" novalidate = "novalidate">
			<div class="row">
		     		<div class="col s2 input-field">
		       				<input class="validate" type ="text" id="brCodeId" name ="brCodeName" ng-model="btbFServ.frBrCode"  readonly ng-required="true" >
		       			<label for="code" style="color:black;">Branch Code</label>	
		       			<div class = "text-left errorMargin" ng-messages = "btbFTForm.brCodeName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Branch date is required.</span>
							</div>		
		       		</div>

					<div class="col s3 input-field">
			    		<select name="chequeType" id="chequeTypeId" ng-model="btbFServ.btbChqType" ng-init="btbFServ.btbChqType = 'C'">
							<option value='C'>Computerized</option>
							<option value='M'>Manual</option>
						</select>
						<label style="color:black;">Cheque Type </label>
						<div class = "text-left errorMargin" ng-messages = "btbFTForm.chequeType.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Cheque is required.</span>
							</div>	
					</div>
					
		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="bkId" name ="bkName" ng-model="btbFServ.frBkName" ng-click="openBankDB()" readonly ng-required="true" >
		       			<label for="code" style="color:black;">Bank Name</label>
		       				<div class = "text-left errorMargin" ng-messages = "btbFTForm.bkName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Bank is required.</span>
							</div>	
		       		</div>
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="fundDtId" name ="fundDtName" ng-model="btbFServ.fundDt" ng-blur="checkCsIsClose(btbFServ.fundDt)" ng-required="true" >
		       			<label for="code" style="color:black;">Date</label>	
		       			<div class = "text-left errorMargin" ng-messages = "btbFTForm.fundDtName.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Date is required.</span>
							</div>	
		       		</div>
					
		    </div>
		    
		    <div class="row">

		       		<div class="col s4 input-field">
		       				<input class="validate" type ="button" id="fTransId" value="FUND TRANSFER" ng-click="openFTrans()">
		       		</div>
		    </div>
		    <div class="row">
				<div ng-show="toBrhList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">Fund Transfer Details</caption>
						<tr class="rowclr" ng-show="isBankLabel">
							<th class="colclr">From Branch</th>
							<th class="colclr">From Bank</th>
							<th class="colclr">From Bank Acc No.</th>
							<th class="colclr">Chq No</th>
							<th class="colclr">To Branch</th>
							<th class="colclr">To Bank</th>
							<th class="colclr">To Bank Acc No.</th>
							<th class="colclr">Amount</th>
							<th class="colclr">Description</th>
							<th class="colclr">Action</th>
						</tr>
						<tr class="rowclr" ng-show="isEmployeLabel">
							<th class="colclr">From Branch</th>
							<th class="colclr">From Bank</th>
							<th class="colclr">From Bank Acc No.</th>
							<th class="colclr">Chq No</th>
							<th class="colclr">To Branch</th>
							<th class="colclr">To Employee</th>
							<th class="colclr">To EmployeeFcode</th>
							<th class="colclr">Amount</th>
							<th class="colclr">Description</th>
							<th class="colclr">Action</th>
						</tr>
						<tr class="rowclr" ng-show="isPetroLabel">
							<th class="colclr">From Branch</th>
							<th class="colclr">From Bank</th>
							<th class="colclr">From Bank Acc No.</th>
							<th class="colclr">Chq No</th>
							<th class="colclr">To Branch</th>
							<th class="colclr">To Petro</th>
							<th class="colclr">To PetroFcode</th>
							<th class="colclr">Amount</th>
							<th class="colclr">Description</th>
							<th class="colclr">Action</th>
						</tr>
						<tr class="tbl" ng-repeat="toBrh in toBrhList">
							<td class="rowcel">{{ currentBranch }}</td>
							<td class="rowcel">{{ btbFServ.frBkName }}</td>
							<td class="rowcel">{{ btbFServ.frBkAccNo }}</td>
							<td class="rowcel">{{ toBrh.frChqNo }}</td>
							<td class="rowcel">{{ toBrh.toBrName }}</td>
							<td class="rowcel">{{ toBrh.toBkName }}</td>
							<td class="rowcel">{{toBrh.toBkCode}}</td>
							<td class="rowcel">{{ toBrh.toAmt }}</td>
							<td class="rowcel">{{ toBrh.toDesc }}</td>
							<td class="rowcel"><input type="button" value="remove" ng-click="removeFT($index)" /></td>
						</tr>
					</table>
				</div>
		    </div>
		   <div class="row">
		   </div>
			<div class="row">
				<div class="input-field col s12">
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="btbFServDesc" ng-model="btbFServ.desc" ng-required="true"
 						ng-minlength = "2" ng-maxlength = "200"></textarea>
 					<label style="color:black;">Description</label>
 					<div class = "text-left errorMargin" ng-messages = "btbFTForm.btbFServDesc.$error" ng-if = "isFormSubmitted">
								<span ng-message = "required">*  Description is required.</span>
								<span ng-message = "minlength">*  Description should be greater than 1.</span>
								<span ng-message = "maxlength">*  Description should be less than 200.</span>
							</div>	
				</div>
    		 </div>	
    		 
	   		  <div class="row">
		     		<div class="col s12 center">
		       				<input class="validate" type ="submit" id="saveId" value="submit" >
		       		</div>
	   		 </div>
		  </form>
    </div>



 <div id="bankDB" ng-hide="bankDBFlag">
	<input type="text" name="filterBank"
		ng-model="filterBank" placeholder="Search....">
	<table>
		<tr>
			<th></th>
			<th>Bank Name</th>
			<th>FaCode</th>
			<th>Acc. No</th>
			<th>IFSC Code</th>
		</tr>
		<tr ng-repeat="bank in frBankList | filter:filterBank">
			<td><input type="radio" name="bankName" id="bankId"
				 value="{{ bank }}" ng-model="bankCode"
				ng-click="selectBank(bank)"></td>
			<td>{{ bank.bnkName }}</td>
			<td>{{ bank.bnkFaCode }}</td>
			<td>{{ bank.bnkAcNo }}</td>
			<td>{{ bank.bnkIfscCode }}</td>
		</tr>
	</table>
</div>

<div id="toEmpDB" ng-hide="toEmpDBFlag">
<input type="text" name="fileterEmployee" ng-model="toEmpFilter" placeholder="Search....">
<table>
    <tr>
       <th></th>
       <th>Employee Name</th>
       <th>Employee Facode</th>
       </tr>
      <tr ng-repeat="emp in toEmpList | filter:toEmpFilter" >
      <td><input type="radio" name="empName" id="empId" value="{{ emp }}" ng-modal="empCode" ng-click="selectToEmp(emp)"></td>
      <td>{{emp.empName}}</td>
      <td>{{emp.empFaCode}}</td>
      </tr>
</table>

</div>


<div id="toPetroDB" ng-hide="toPetroDBFlag">
<input type="text" name="fileterPetro" ng-model="toPetroFilter" placeholder="Search....">
<table>
    <tr>
       <th></th>
       <th>Branch</th>
       <th>Card Type</th>
       <th>Card No.</th>
       <th>Card FaCode</th>
       </tr>
      <tr ng-repeat="petro in toPetroList | filter:toPetroFilter" >
      <td><input type="radio" name="petroName" id="petroId" value="{{ petro }}" ng-modal="petroCode" ng-click="selectToPetro(petro)"></td>
      <td>{{petro.branch}}</td>
      <td>{{petro.cardType}}</td>
      <td>{{petro.cardNo}}</td>
      <td>{{petro.cardFaCode}}</td>
      </tr>
</table>

</div>


<div id="toBnkDB" ng-hide="toBnkDBFlag">
	<input type="text" name="filterBank"
		ng-model="toBankFilter" placeholder="Search....">
	<table>
		<tr>
			<th></th>
			<th>Bank Name</th>
			<th>FaCode</th>
			<th>Acc. No</th>
			<th>IFSC Code</th>
		</tr>
		<tr ng-repeat="bank in toBnkList | filter:toBankFilter">
			<td><input type="radio" name="bankName" id="bankId"
				 value="{{ bank }}" ng-model="bankCode"
				ng-click="selectToBank(bank)"></td>
			<td>{{ bank.bnkName }}</td>
			<td>{{ bank.bnkFaCode }}</td>
			<td>{{ bank.bnkAcNo }}</td>
			<td>{{ bank.bnkIfscCode }}</td>
		</tr>
	</table>
</div>


<div id="branchDB" ng-hide="branchDBFlag">
	<input type="text" name="filterBranch"
		ng-model="filterBranch" placeholder="Search....">
	<table>
		<tr>
			<th></th>
			<th>Branch Name</th>
			<th>FaCode</th>
		</tr>
		<tr ng-repeat="brh in brList | filter:filterBranch">
			<td><input type="radio" name="brName" id="brId"
				 value="{{ brh }}" ng-model="brhCode"
				ng-click="selectBrh(brh)"></td>
			<td>{{ brh.branchName }}</td>
			<td>{{ brh.branchFaCode }}</td>
		</tr>
	</table>
</div>


<div id="fTransDB" ng-hide="fTransDBFlag">
	<form name = "FundTranForm" ng-submit="saveToBrDet(FundTranForm)" 
		ng-class = "{'form-error': FundTranForm.$invalid && isFundFormSubmitted" novalidate = "novalidate">
	<table style="background-color: #F2FAEF!important;">
		<tr>
			<td style="color:black;">Cheque No :</td>
			<td>
				<input class="validate" type ="text" id="toChqId" name ="toChqName" ng-model="toBrh.frChqNo" ng-click="openChqDB()" readonly>
		    </td>
		</tr>
		<tr>
			<td style="color:black;">Branch : </td>
			<td>
				<input class="validate" type ="text" id="toBrhId" name ="toBrhName" ng-model="toBrh.toBrName" ng-click="openBranchDB()" readonly ng-required="true" >
				<div class = "text-left errorMargin" ng-messages = "FundTranForm.toBrhName.$error" ng-if = "isFundFormSubmitted">
								<span ng-message = "required">*  Branch is required.</span>
							</div>
		    </td>
		</tr>
		
		<tr ng-show="radioShow">
				<td ng-show="bnkRadioBut" style="color:black;">Bank :
					<input class="validate" type="radio" id="bankId"  name="isSRName"  ng-model="changeRadioBank"  ng-show="bnkRadioBut" ng-click="bank()" > 
					</td>
					
				<td ng-show="empRadioBut" style="color:black;">Employee :
					<input class="validate" type="radio" id="employeeId" name="isSRName" ng-model="changeRadioEmp" ng-show="empRadioBut" ng-click="employee()"> 
					</td>
					
				<td ng-show="petroRadioBut" style="color:black;">Petro :
					<input class="validate" type="radio" id="petroId" name="isSRName" ng-model="changeRadioPetro" ng-show="petroRadioBut" ng-click="petroCard()"> 
					</td>
					
					<td>
						<div class = "text-left errorMargin" ng-if = "isFundFormSubmitted && bnkRadioBut && empRadioBut && petroRadioBut">
								<span>*  Bank/Employee is required.</span>
							</div>
					</td>
					
				</tr>
	 <tr>
			<td ng-show="isBankLabel" style="color:black;">Bank : </td>
			<td ng-show="isEmployeLabel" style="color:black;">Employee : </td>
			<td ng-show="isPetroLabel" style="color:black;">PetroCard : </td>
			<td>
				<input class="validate" type ="text" id="toBnkId" name ="toBnkName" ng-model="toBrh.toBkName" ng-click="openToBankDB()" ng-show="isBankText" readonly ng-required="true">
				<div class = "text-left errorMargin" ng-messages = "FundTranForm.toBnkName.$error" ng-if = "isFundFormSubmitted && isBankText">
								<span ng-message = "required">*  Bank/Employee is required.</span>
							</div>
		    </td>
		</tr>
		
		<tr>
			<td style="color:black;">Amount * : </td>
			<td>
				<input class="validate" type ="number" id="toAmtId" name ="toAmtName" ng-model="toBrh.toAmt" ng-required="true" step="0.00001"  min="0.00001">
				<div class = "text-left errorMargin" ng-messages = "FundTranForm.toAmtName.$error" ng-if = "isFundFormSubmitted">
								<span ng-message = "required">*  Amount is required.</span>
							</div>
		    </td>
		</tr>
		
		<tr>
			<td style="color:black;">Description : </td>
			<td>
				<textarea id="textarea" class="materialize-textarea"  rows="3" name ="descName" ng-model="toBrh.toDesc" ng-required="true"></textarea>
				<div class = "text-left errorMargin" ng-messages = "FundTranForm.descName.$error" ng-if = "isFundFormSubmitted">
								<span ng-message = "required">*  Description is required.</span>
							</div>
		    </td>
		</tr>
		
		<tr>
			<td>       </td>
			<td>
				<input class="validate" type ="submit" id="toBrSaveId" value="SUBMIT">
		    </td>
		</tr>
	</table>
	</form>
</div>

	<div id ="chequeDB" ng-hide="chequeDBFlag" class="noprint">
		  <input type="text" name="filterChqbox" ng-model="filterChqbox" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Cheque No</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="chq in chqList | filter:filterChqbox ">
		 	  <td><input type="radio"  name="chq.chqLChqNo"   value="{{ chq.chqLChqNo }}" ng-model="chqCode" ng-click="saveChqNo(chq)"></td>
              <td>{{chq.chqLChqNo}}</td>
              
          </tr>
      </table> 
	</div>
</div>

<div ng-show="operatorLogin || superAdminLogin">
	<title>View Owner</title>
	<div class="row">
		<div class="col s3 hide-on-med-and-down">&nbsp;</div>
		<form class="col s6 card"
			style="align: center; margin-top: 40px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
			ng-submit="ownerView(ownCode)">
			<div class="input-field col s6">
				<!--    				<input type="text" id="ownercode" name="ownCode" ng-model="ownCode" ng-click="openOwnerCodeDB()" readonly/> -->
				<input type="text" id="ownercode" name="ownCode" ng-model="ownCode" />
				<label>Enter Owner Code </label>
			</div>
			<div class="col s6 center">
				<input class="btn" type="submit" value="Submit">
			</div>
		</form>
		<div class="col s3">&nbsp;</div>
	</div>




	<div class="row" ng-hide="ownerDetails">
		<div class="col s3 hidden-xs hidden-sm">&nbsp;</div>
		<div class="col s6 card"
			style="align: center; margin-top: 0px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<form name="ViewOwnerForm" ng-submit="EditOwnerSubmit(ViewOwnerForm)">
				<h5>
					Here's the detail of <span class="teal-text text-lighten-2">{{owner.ownName}}</span>:
				</h5>
				<table class="table-hover table-bordered table-condensed">
					<tr>
						<td>Owner Code:</td>
						<td>{{owner.ownCode}}</td>
					</tr>

					<tr>
						<td>Name:</td>
						<td>{{ owner.ownName}}<input type="text" name="ownName"
							id="OwnerName" ng-model="owner.ownName"
							value="{{ owner.ownName}}"></td>
					</tr>

					<tr>
						<td>Father Name:</td>
						<td>{{ owner.ownFather}}<input type="text" name="ownFather"
							id="ownFather" ng-model="owner.ownFather"
							value="{{ owner.ownFather}}"></td>
					</tr>

					<tr>
						<td>Pan No.:</td>
						<td>{{ owner.ownPanNo}}<input type="text" name="ownPanNo"
							ng-model="owner.ownPanNo"
							ng-pattern="/^[A-Z]{3}[C,P,H,F,A,T,B,L,J,G][A-Z]\d{4}[A-Z]$/"
							value="{{ owner.ownPanNo}}"></td>
					</tr>

					<tr>
						<td>Pan Name:</td>
						<td>{{ owner.ownPanName}}<input type="text" name="ownPanName"
							ng-model="owner.ownPanName" value="{{ owner.ownPanName}}"></td>
					</tr>

					<tr>
						<td>Phone No.:</td>
						<td>{{ owner.ownPhNoList}}<input type="text" name="ownPhNo"
							ng-model="owner.ownPhNoList[0]" required></td>
					</tr>

					<tr>
						<td>Email Id:</td>
						<td>{{ owner.ownEmailId}}<input type="email"
							name="ownEmailId" ng-model="owner.ownEmailId"
							value="{{ owner.ownEmailId}}"></td>
					</tr>

					<td><h5>Bank Detail</h5></td>
					<tr>
						<td>Account Holder Name:</td>
						<td>{{ owner.ownAcntHldrName}}</td>
					</tr>

					<tr>
						<td>Bank Name:</td>
						<td>{{ owner.ownBnkBranch}}</td>
					</tr>
					<tr>
						<td>IFSC Code:</td>
						<td>{{ owner.ownIfsc}}</td>
					</tr>
					<tr>
						<td>Account Number:</td>
						<td>{{ owner.ownAccntNo}}</td>
					</tr>


				</table>
				<table class="table-hover table-bordered table-condensed">
					<td><h5>View Image</h5></td>
					<tr>
						<td><input type="button" value="PAN Image"
							ng-click="showPanImage(owner)"></td>
						<td><input type="button" value="DEC Image"
							ng-click="showDecImage(owner)"></td>
						<td><input type="button" value="BANK Image"
							ng-click="showBankImage(owner)"></td>
					</tr>
				</table>

				<table class="table-hover table-bordered table-condensed" ng-if="currentBranch=='Gurgaon (H.O)'">
					<td><h5>Upload Image</h5></td>
					<tr>
						<td>

							<form class="col s12 card"
								style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
								name="ownPANUpldForm">

								<div class="input-field col s9">
									<input type="file" class="teal white-text btn" name="ownPanImageName"
										file-model="ownPanImage"> <label>Choose PAN
										File for Upload</label>
								</div>
								<div class="col s3">
									<a class="btn waves-effect waves-light white-text col s12"
										type="button" ng-click="uploadOwnPanImage(ownPanImage)">Upload
										PAN</a>
								</div>

							</form>
						</td>
					</tr>

					<tr>
						<td>

							<form class="col s12 card"
								style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
								name="ownDecUpldForm">

								<div class="input-field col s9">
									<input type="file" class="teal white-text btn" name="ownDecImageName"
										file-model="ownDecImage"> <label>Choose DEC
										File for Upload</label>
								</div>
								<div class="col s3">
									<a class="btn waves-effect waves-light white-text col s12"
										type="button" ng-click="uploadOwnDecImage(ownDecImage)">Upload Dec.</a>
								</div>

							</form>
						</td>
					</tr>
				</table>


				<table class="table-hover table-bordered table-condensed">
					<tr>
						<td><h5>Current Address</h5></td>
					</tr>

					 <tr>
						<td>Address</td>
						<td>{{ add.completeAdd}}<input id="completeAddId" type ="text" name ="completeAddName"  ng-model="add.completeAdd"  ></td>
					</tr>

					<tr>
						<td>Pin</td>
						<td>{{add.addPin}}<input id="addPinId" type ="text" name ="addPinName"  ng-model="add.addPin" ng-keyup="getStnByPin()" minlength="6" maxlength="6"  >
        			</td>
					</tr> 
					<tr>
						<td>State</td>
						<td>{{add.addState}}<input id="addStateId" type ="text" name ="addStateName"  ng-model="add.addState" ng-click="getState()" readonly >
        			</td>
					</tr>
					
					<tr>
						<td>District</td>
						<td>{{add.addDist}}<input id="districtId" type ="text" name ="districtName"  ng-model="add.addDist" ng-click="getState()" readonly  >
        			</td>
					</tr>
					
					<tr>
						<td>City</td>
						<td>{{add.addCity}}<input id="addCityId" type ="text" name ="addCityName"  ng-model="add.addCity" ng-click="getState()" readonly  >
        			</td>
					</tr>
					
					<tr>
						<td>Location</td>
						<td>{{add.addPost}}<input id="locationId" type ="text" name ="locationName"  ng-model="add.addPost" ng-click="getState()" readonly  >
        			</td>
					</tr>
					
									
				</table>
				<div ng-if="currentBranch=='Gurgaon (H.O)'">
					<input type="submit" value="Update" ng-submit="submit">
				</div>
			</form>
		</div>
	</div>

	<div id="viewOwnerDB" ng-show="viewOwnerFlag">
		<table class="table-hover table-bordered table-bordered">
			<tr>
				<td>Owner Code:</td>
				<td>{{owner.ownCode}}</td>
			</tr>

			<tr>
				<td>Name</td>
				<td>{{owner.ownName}}</td>
			</tr>

			<tr>
				<td>Email-ID</td>
				<td>{{owner.ownEmailId}}</td>
			</tr>

			<tr>
				<td>Pan Name</td>
				<td>{{owner.ownPanName}}</td>
			</tr>


		</table>

		<table class="table-hover table-bordered table-bordered">
			<h4>Current Address</h4>
			<tr>
				<td>Address</td>
				<td>{{add.completeAdd}}</td>
			</tr>

			<tr>
				<td>City</td>
				<td>{{add.addCity}}</td>
			</tr>

			<tr>
				<td>State</td>
				<td>{{add.addState}}</td>
			</tr>

			<tr>
				<td>Pin</td>
				<td>{{add.addPin}}</td>
			</tr>
		</table>


		<input type="button" value="Cancel" ng-click="back()"> <input
			type="button" value="Save" ng-click="saveOwner()">

	</div>
	
	<div id="stateDB" ng-hide="stateDBFlag">

		<input type="text" name="filterStateCode"
			ng-model="filterStateCode.stateName" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>State GstCode</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="state in stateList | filter:filterStateCode">
				<td><input type="radio" name="stateCode" id="stateId"
					value="{{ state.stateCode }}" ng-model="stateCode"
					ng-click="saveStateCode(state)"></td>
				<td>{{ state.stateGST }}</td>
				<td>{{ state.stateName }}</td>
			</tr>
		</table>

	</div>
	
	
		<div id="distDB" ng-hide="distDBFlag">

		<input type="text" name="filterDistCode"
			ng-model="filterDistCode" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="dist in distList | filter:filterDistCode">
				<td><input type="radio" name="distName" id="distId"
					value="{{ dist }}" ng-model="distName"
					ng-click="saveDist(dist)"></td>
				<td>{{ dist }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
	
	
	
		<div id="cityDB" ng-hide="cityDBFlag">

		<input type="text" name="filterCityCode"
			ng-model="filterCityCode" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>City Name</th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="city in cityList | filter:filterCityCode">
				<td><input type="radio" name="cityCode" id="cityId"
					value="{{ city }}" ng-model="cityName"
					ng-click="saveCity(city)"></td>
				<td>{{ city }}</td>
				<td>{{ distName }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
	
	
	<div id="stnDB" ng-hide="stnDBFlag">

		<input type="text" name="filterStnCode"
			ng-model="filterStnName" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>Pin Code</th>
				<th>Station Name</th>
				<th>City Name</th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="stn in stnList | filter:filterStnName">
				<td><input type="radio" name="stnCode" id="stnId"
					value="{{ stn }}" ng-model="stnName"
					ng-click="saveStn(stn)"></td>
				<td>{{ stn.pinCode }}</td>
				<td>{{ stn.stationName }}</td>
				<td>{{ stn.city }}</td>
				<td>{{ stn.district }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
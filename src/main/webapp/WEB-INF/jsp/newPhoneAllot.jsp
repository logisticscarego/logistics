<div ng-show="operatorLogin || superAdminLogin">
	<div class="row noprint">
		<form name="newPhoneAllotForm" ng-submit=submit(newPhoneAllotForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchId" name ="branchName" ng-model="tpms.branch.branchFaCode" ng-click="openBranchDB()" readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>
		       		
		       	    <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="empId" name ="empName" ng-model="tpms.employee.empFaCode" ng-click="openEmpDB()" readonly ng-required="true" >
		       			<label for="code">Employee Code</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
				    		<select name="telType" id="telTypeId" ng-model="tpms.telephoneMstr.tmType" ng-required="true">
								<option value="tele">Telephone</option>
								<option value="mobile">Mobile</option>
							</select>
							<label>Type</label>
					</div>
		    </div>
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="phNoId" name ="phNo" ng-model="tpms.telephoneMstr.tmPhNo"  ng-required="true" >
		       			<label for="code">Phone No</label>	
		       		</div>
			   		 <div class="col s4 input-field">
				    		<select name="LocType" id="LocTypeId" ng-model="tpms.telephoneMstr.tmLocType" ng-required="true">
								<option value="office">Office</option>
								<option value="godown">Godown</option>
								<option value="residence">Residence</option>
							</select>
							<label>Location</label>
					</div>
			      	<div class="col s4 input-field">
			    		<select name="payMod" id="payModId" ng-model="tpms.telephoneMstr.tmPayMod" ng-required="true"">
							<option value="prepaid">Prepaid</option>
							<option value="postpaid">Postpaid</option>
						</select>
						<label>Payment Mode</label>
					</div>
		    </div>
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="spNameId" name ="spName" ng-model="tpms.telephoneMstr.tmSPName" ng-required="true" >
		       			<label for="code">Service Provider</label>	
		       		</div>
		  
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="instDtId" name ="instDt" ng-model="tpms.telephoneMstr.tmInstDt">
		       			<label for="code">Initiate Date</label>	
		       		</div>
		  
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="discDtId" name ="discDt" ng-model="tpms.telephoneMstr.tmDiscDt">
		       			<label for="code">Disconnect Date</label>	
		       		</div>
		    </div>
		    
	      	 <div class="row">
	     		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="advDepDtId" name ="advDepDt" ng-model="tpms.telephoneMstr.tmAdvDepDt">
		       			<label for="code">Advance Deposit Date</label>	
		       	</div>
		       	<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="advDepAmtId" name ="advDepAmt" ng-model="tpms.telephoneMstr.tmAdvDepAmt">
		       			<label for="code">Advance Deposit Amount</label>	
		       	</div>
		       	<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="planId" name ="plan" ng-model="tpms.telephoneMstr.tmPlan">
		       			<label for="code">Plan</label>	
		       	</div>
	   		 </div>
	   	
	   		 <div class="row">
				<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="refDtId" name ="refDt" ng-model="tpms.telephoneMstr.tmRefDt">
		       			<label for="code">Refund Date</label>	
		       	</div>
		       	<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="refAmtId" name ="refAmt" ng-model="tpms.telephoneMstr.tmRefAmt">
		       			<label for="code">Refund Amount</label>	
		       	</div>
		       	<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="limitAmtId" name ="limitAmt" ng-model="tpms.telephoneMstr.tmLimitAmt">
		       			<label for="code">Limit Of Amount</label>	
		       	</div>
    		 </div>	
    		 
    		 <div class="row">
				<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="accNoId" name ="accNo" ng-model="tpms.telephoneMstr.tmAccNo">
		       			<label for="code">Account No.</label>	
		       	</div>
		       	<div class="col s4 input-field">
		       				<select name="billDay" ng-model="tpms.telephoneMstr.tmBillDay" ng-options="billDay as billDay for billDay in billDays" required></select>
						<label>Bill Day</label>
		       	</div>
		       	<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="dueDayId" name ="dueDay" ng-model="tpms.telephoneMstr.tmDueDay">
		       			<label for="code">Due Day</label>	
		       	</div>
		       	
    		 </div>	
    		 
    		 <div class="row">
				<div class="col s4 input-field">
		       			<input type ="button" name="empAdd" id="empAddId" ng-model="billEmpAdd" ng-click="empAdd()" value="Employee Address">
		       			<label for="code">Billing Address</label>	
		       	</div>
		       	<div class="col s4 input-field">
		       			<input type ="button" name="newAdd" id="newAddId" ng-model="billNewAdd" ng-click="newAdd()" value="New Address">
		       	</div>    	
    		 </div>	
	   		 
	   		 <div class="row" ng-show="showEmpAdd">
				<table>
					<thead>
						<tr>
							<th>Address</th>	
							<th>city</th>	
							<th>state</th>	
							<th>pin</th>	
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{{billEmpAdd.completeAdd}}</td>
							<td>{{billEmpAdd.addCity}}</td>
							<td>{{billEmpAdd.addState}}</td>
							<td>{{billEmpAdd.addPin}}</td>
						</tr>
					</tbody>		
				</table>
			</div>
			
			<div class="row" ng-show="showNewAdd">
				<table>
					<thead>
						<tr>
							<th>Address</th>	
							<th>city</th>	
							<th>state</th>	
							<th>pin</th>	
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{{billNewAdd.completeAdd}}</td>
							<td>{{billNewAdd.addCity}}</td>
							<td>{{billNewAdd.addState}}</td>
							<td>{{billNewAdd.addPin}}</td>
						</tr>
					</tbody>		
				</table>
			</div>
	   		 
	   		 
	   		  <div class="row">
	     		<div class="col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	   		 </div>
		  </form>
    </div>
    
    <div id ="branchCodeDB" ng-hide="branchCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Branch Name</th>
 	  	 	  <th>Branch FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in branchList | filter:filterTextbox">
		 	  <td><input type="radio"  name="branch"   value="{{ branch }}" ng-model="brCode" ng-click="saveBrCode(branch)"></td>
              <td>{{ branch.branchName }}</td>
              <td>{{ branch.branchFaCode }}</td>
          </tr>
      </table> 
	</div>

	<div id ="empCodeDB" ng-hide="empCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Employee Name</th>
 	  	 	  <th>Employee FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="emp in empList | filter:filterTextbox">
		 	  <td><input type="radio"  name="emp"   value="{{ emp }}" ng-model="empFaCode" ng-click="saveEmpCode(emp)"></td>
              <td>{{ emp.empName }}</td>
              <td>{{ emp.empFaCode }}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id="newAddDB" ng-hide="newAddDBFlag">
			<form name ="AddressForm" ng-submit="submitNewAdd(AddressForm)">
			<table>
					<tr>
						<td>Address</td>
						<td><input type ="text" name="completeAddName" id="completeAddId" ng-model="billNewAdd.completeAdd" ng-required="true" ng-minlength="3" ng-minlength="255"></td>
					</tr>
					<tr>
						<td>State</td>
						<td><input type ="text" name="addStateName" id="addStateId" ng-model="billNewAdd.addState" ng-required="true" ng-minlength="3" ng-minlength="15"></td>
					</tr>
					<tr>
						<td>City</td>
						<td><input type ="text" name="addCityName" id="addCityId" ng-model="billNewAdd.addCity" ng-required="true" ng-minlength="3" ng-minlength="40"></td>
					</tr>
				
					<tr>
						<td>Pin No</td>
						<td><input type ="number" name="addPinName" id="addPinId" ng-model="billNewAdd.addPin"  ng-required="true" ng-minlength="6" ng-maxlength="6"></td>
					</tr>	
					<tr><td colspan="2"><input type ="submit" value="Submit" ></td></tr>	
			</table>	
			</form>
	</div>
	
	
	
		<div id="saveTpmsDB" ng-hide="saveTpmsFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{tpms.branch.branchFaCode}}</td>
				</tr>
				
				<tr>
					<td>Employee Code</td>
					<td>{{tpms.employee.empFaCode}}</td>
				</tr>
				
				<tr>
					<td>Type</td>
					<td>{{tpms.telephoneMstr.tmType}}</td>
				</tr>
				
				<tr>
					<td>Phone No</td>
					<td>{{tpms.telephoneMstr.tmPhNo}}</td>
				</tr>
				
				<tr>
					<td>Location</td>
					<td>{{tpms.telephoneMstr.tmLocType}}</td>
				</tr>
				
				<tr>
					<td>Payment Mode</td>
					<td>{{tpms.telephoneMstr.tmPayMod}}</td>
				</tr>
				
				<tr>
					<td>Service Provider</td>
					<td>{{tpms.telephoneMstr.tmSPName}}</td>
				</tr>
				
				<tr>
					<td>Initiate Date</td>
					<td>{{tpms.telephoneMstr.tmInstDt}}</td>
				</tr>
				
				<tr>
					<td>Disconnect Date</td>
					<td>{{tpms.telephoneMstr.tmDiscDt}}</td>
				</tr>
				
				<tr>
					<td>Advance Deposit Date</td>
					<td>{{tpms.telephoneMstr.tmAdvDepDt}}</td>
				</tr>
				
				<tr>
					<td>Advance Deposite Amount</td>
					<td>{{tpms.telephoneMstr.tmAdvDepAmt}}</td>
				</tr>
				
				<tr>
					<td>Plan</td>
					<td>{{tpms.telephoneMstr.tmPlan}}</td>
				</tr>
				
				<tr>
					<td>Refund Date</td>
					<td>{{tpms.telephoneMstr.tmRefDt}}</td>
				</tr>
				
				<tr>
					<td>Refund Amount</td>
					<td>{{tpms.telephoneMstr.tmRefAmt}}</td>
				</tr>
				
				<tr>
					<td>Limit Of Amount</td>
					<td>{{tpms.telephoneMstr.tmLimitAmt}}</td>
				</tr>
				
				<tr>
					<td>Accont No.</td>
					<td>{{tpms.telephoneMstr.tmAccNo}}</td>
				</tr>
				
				<tr>
					<td>Bill Day</td>
					<td>{{tpms.telephoneMstr.tmBillDay}}</td>
				</tr>
				
				<tr>
					<td>Due Day</td>
					<td>{{tpms.telephoneMstr.tmDueDay}}</td>
				</tr>
				
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" ng-click="saveTpms()"/>
		</div>
	</div>

</div>

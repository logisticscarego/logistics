<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<form ng-submit="EditContract(ContCode)">
				<table>
					<tr>
						<td>Enter Contract Code </td>
						<td><input type="text" name="ContCode" ng-model="ContCode"/></td>
					</tr>
					<tr>
						<td><input type="submit" value="Submit"/></td>
					</tr>
				</table>
			</form>
			
	<div ng-hide = "editDlyCont" >	
		
			<form ng-submit="EditDailyContractSubmit(dailyContract)">
			<div align="center">
	   
			
				<table border="0">
				 
			 <tr>
				<td>Branch Code</td>
				<td><input type ="text" id="branchCode" name ="branchCode" ng-model="dailyContract.branchCode" value={dailyContract.branchCode}}></td>
			</tr>
			
			<tr>
				<td>Daily Contract BLPM Code</td>
				<td><input type ="text" id="dlyContBLPMCode" name ="dlyContBLPMCode" ng-model="dailyContract.dlyContBLPMCode" value={{dailyContract.dlyContBLPMCode}}></td>
			</tr>
			
			<tr>
				<td>Daily Contract Consignor Code</td>
				<td><input type ="text" id="dlyContCngrCode" name ="dlyContCngrCode" ng-model="dailyContract.dlyContCngrCode" value={{dailyContract.dlyContCngrCode}}></td>
			</tr>
			
			<tr>
				<td>Daily Contract CR Name</td>
				<td><input type ="text" id="dlyContCrName" name ="dlyContCrName" ng-model="dailyContract.dlyContCrName" value={{dailyContract.dlyContCrName}}></td>
			</tr>
			
			<tr>
				<td>Daily Contract From Station</td>
				<td><input type ="text" id="dlyContFromStation" name ="dlyContFromStation" ng-model="dailyContract.dlyContFromStation" value={{dailyContract.dlyContFromStation}}></td>
			</tr>
			
			<tr>
				<td>Daily Contract To Station</td>
				<td><input type ="text" id="dlyContToStation" name ="dlyContToStation" ng-model="dailyContract.dlyContToStation" value={{dailyContract.dlyContToStation}}></td>
			</tr>
			
			<tr>
				<td>Daily Contract Rate</td>
				<td><input type ="text" id="dlyContRate" name ="dlyContRate" ng-model="dailyContract.dlyContRate" value={{dailyContract.dlyContRate}}></td>
			</tr>
			
			<tr>
				<td>Daily Contract Vehicle Type</td>
				<td><input type ="text" id="dlyContVehicleType" name ="dlyContVehicleType" ng-model="dailyContract.dlyContVehicleType" value={{dailyContract.dlyContVehicleType}}></td>
			</tr>
			
			<tr>
				<td>Daily Contract Product Type</td>
				<td><input type ="text" id="dlyContProductType" name ="dlyContProductType" ng-model="dailyContract.dlyContProductType" value={{dailyContract.dlyContProductType}}></td>
			</tr>
			
			<tr>
				<td>Daily Contract From Weight</td>
				<td><input type ="text" id=" dlyContFromWt" name ="dlyContFromWt" ng-model="dailyContract.dlyContFromWt" value={{dailyContract.dlyContFromWt}}></td>
			</tr>
			
			<tr>
				<td>Daily Contract To Weight</td>
				<td><input type ="text" id="dlyContToWt" name ="dlyContToWt" ng-model="dailyContract.dlyContToWt" value={{dailyContract.dlyContToWt}}></td>
			</tr>
			
			<tr>
				<td>Daily Contract Transit Day</td>
				<td><input type ="text" id="dlyContTransitDay" name ="dlyContTransitDay" ng-model="dailyContract.dlyContTransitDay" value={{dailyContract.dlyContTransitDay}}></td>
			</tr>
			
			<tr>
				<td>Daily Contract Statistical Charge</td>
				<td><input type ="text" id="dlyContStatisticalCharge" name ="dlyContStatisticalCharge" ng-model="dailyContract.dlyContStatisticalCharge" value={{dailyContract.dlyContStatisticalCharge}}></td>
			</tr>
			
			<tr>
				<td>Daily Contract Load</td>
				<td><input type ="text" id="dlyContLoad" name ="dlyContLoad" ng-model="dailyContract.dlyContLoad" value={{dailyContract.dlyContLoad}></td>
			 </tr>
			    
			  <tr>
			      <td>Daily Contract UnLoad</td>
				  <td><input type ="text" id="dlyContUnLoad" name ="dlyContUnLoad" ng-model="dailyContract.dlyContUnLoad" value={{dailyContract.dlyContUnLoad}}></td>
			   </tr>
			   
			   <tr>
					<td>Daily Contract Penalty</td>
					<td><input type ="text" id="dlyContPenalty" name ="dlyContPenalty" ng-model="dailyContract.dlyContPenalty" value={{dailyContract.dlyContPenalty}}></td>
			    </tr>
			    
			    <tr>
					<td>Daily Contract Bonus</td>
					<td><input type ="text" id="dlyContBonus" name ="dlyContBonus" ng-model="dailyContract.dlyContBonus" value={{dailyContract.dlyContBonus}}></td>
			    </tr>
			    
			    <tr>
					<td>Daily Contract Detention</td>
					<td><input type ="text" id="dlyContDetention" name ="dlyContDetention" ng-model="dailyContract.dlyContDetention" value={{dailyContract.dlyContDetention}}></td>
			    </tr>
			    
			    <tr>
					<td>Daily Contract Type</td>
					<td><input type ="text" id="dlyContType" name ="dlyContType" ng-model="dailyContract.dlyContType" value={{dailyContract.dlyContType}}></td>
				</tr>
	             
			 	<tr>
					<td>Daily Contract Additional Rate</td>
					<td><input type ="text" id="dlyContAdditionalRate" name ="dlyContAdditionalRate" ng-model="dailyContract.dlyContAdditionalRate" value={{dailyContract.dlyContAdditionalRate}} ></td>
			    </tr>
			     
			    <tr>
					<td>Daily Contract Proportionate</td>
					<td><input type="text" id="dlyContProportionate" name ="dlyContProportionate" ng-model="dailyContract.dlyContProportionate" value={{dailyContract.dlyContProportionate}}>
					</td>
				</tr>
				
				<tr>
					<td>Daily Contract DC</td>
					<td><input type="text" id="dlyContDc" name ="dlyContDc" ng-model="dailyContract.dlyContDc" value={{dailyContract.dlyContDc}}></td>
			   </tr>
			   
			   <tr>
					<td>Daily Contract Cost Grade</td>
					<td><input type="text" id="dlyContCostGrade" name ="dlyContCostGrade" ng-model="dailyContract.dlyContCostGrade" value={{dailyContract.dlyContCostGrade}}></td>
				</tr>
				
					
				<tr>
					<td>Daily Contract DDL</td>
					<td><input type ="text" id="dlyContDdl" name ="dlyContDdl" ng-model="dailyContract.dlyContDdl" value={{dailyContract.dlyContDdl}}></td>
				</tr>
				
				<tr>
					<td>Daily Contract RBKM ID</td>
					<td><input type ="text" id="dlyContRbkmId" name ="dlyContRbkmId" ng-model="dailyContract.dlyContRbkmId" value={{dailyContract.dlyContRbkmId}}></td>
				</tr>
				
				<tr>
					<td>Daily Contract PBD</td>
					<td><input type ="text" id="dlyContPbd" name ="dlyContPbd" ng-model="dailyContract.dlyContPbd" value={{dailyContract.dlyContPbd}}></td>
				</tr>
				
				<tr>
					<td>Daily Contract Remark</td>
					<td><input type ="text" id="dlyContRemark" name ="dlyContRemark" ng-model="dailyContract.dlyContRemark" value={{dailyContract.dlyContRemark}}></td>
				</tr>
			   
				
	            <tr>
					<td align ="center"><input type="submit" value="Submit!" ></td>
				</tr>
	       </table>
	      </div>
	   </form>
	</div>
	
			<div ng-hide = "editRegCont" >	
		
			<form ng-submit="EditRegularContractSubmit(regularContract)">
			<div align="center">
	   
			
				<table border="0">
				 
			 <tr>
				<td>Branch Code</td>
				<td><input type ="text" id="branchCode" name ="branchCode" ng-model="regularContract.branchCode" value={{regularContract.branchCode}}></td>
			</tr>
			
			<tr>
				<td>Regular Contract BLPM Code</td>
				<td><input type ="text" id="regContBLPMCode" name ="regContBLPMCode" ng-model="regularContract.regContBLPMCode" value={{regularContract.regContBLPMCode}}></td>
			</tr>
			
			<tr>
				<td>Regular Contract Consignor Code</td>
				<td><input type ="text" id="regContCngrCode" name ="regContCngrCode" ng-model="regularContract.regContCngrCode" value={{regularContract.regContCngrCode}}></td>
			</tr>
			
			<tr>
				<td>Regular Contract CR Name</td>
				<td><input type ="text" id="regContCrName" name ="regContCrName" ng-model="regularContract.regContCrName" value={{regularContract.regContCrName}}></td>
			</tr>
			
			<tr>
				<td>Regular Contract From Station</td>
				<td><input type ="text" id="regContFromStation" name ="regContFromStation" ng-model="regularContract.regContFromStation" value={{regularContract.regContFromStation}}></td>
			</tr>
			
			<tr>
				<td>Regular Contract To Station</td>
				<td><input type ="text" id="regContToStation" name ="regContToStation" ng-model="regularContract.regContToStation" value={{regularContract.regContToStation}}></td>
			</tr>
			
			<tr>
				<td>Regular Contract Rate</td>
				<td><input type ="text" id="regContRate" name ="regContRate" ng-model="regularContract.regContRate" value={{regularContract.regContRate}}></td>
			</tr>
			
			<tr>
				<td>Regular Contract Vehicle Type</td>
				<td><input type ="text" id="regContVehicleType" name ="regContVehicleType" ng-model="regularContract.regContVehicleType" value={{regularContract.regContVehicleType}}></td>
			</tr>
			
			<tr>
				<td>Regular Contract From Date</td>
				<td><input type ="text" id="regContFromDt"  name ="regContFromDt" ng-model="regularContract.regContFromDt" value={{regularContract.regContFromDt}}></td>
			</tr>
			
			<tr>
				<td>Regular Contract To Date</td>
				<td><input type ="text" id="regContToDt"  name ="regContToDt" ng-model="regularContract.regContToDt" value={{regularContract.regContToDt}}></td>
			</tr>
			
			<tr>
				<td>Regular Contract Product Type</td>
				<td><input type ="text" id="regContProductType" name ="regContProductType" ng-model="regularContract.regContProductType" value={{regularContract.regContProductType}}></td>
			</tr>
			
			<tr>
				<td>Regular Contract From Weight</td>
				<td><input type ="text" id=" regContFromWt" name ="regContFromWt" ng-model="regularContract.regContFromWt" value={{regularContract.regContFromWt}}></td>
			</tr>
			
			<tr>
				<td>Regular Contract To Weight</td>
				<td><input type ="text" id="regContToWt" name ="regContToWt" ng-model="regularContract.regContToWt" value={{regularContract.regContToWt}}></td>
			</tr>
			
			<tr>
				<td>Regular Contract Transit Day</td>
				<td><input type ="text" id="regContTransitDay" name ="regContTransitDay" ng-model="regularContract.regContTransitDay" value={{regularContract.regContTransitDay}}></td>
			</tr>
			
			<tr>
				<td>Regular Contract Statistical Charge</td>
				<td><input type ="text" id="regContStatisticalCharge" name ="regContStatisticalCharge" ng-model="regularContract.regContStatisticalCharge" value={{regularContract.regContStatisticalCharge}}></td>
			</tr>
			
			<tr>
				<td>Regular Contract Load</td>
				<td><input type ="text" id="regContLoad" name ="regContLoad" ng-model="regularContract.regContLoad" value={{regularContract.regContLoad}}></td>
			 </tr>
			    
			  <tr>
			      <td>Regular Contract UnLoad</td>
				  <td><input type ="text" id="regContUnLoad" name ="regContUnLoad" ng-model="regularContract.regContUnLoad" value={{regularContract.regContUnLoad}}></td>
			   </tr>
			   
			   <tr>
					<td>Regular Contract Penalty</td>
					<td><input type ="text" id="regContPenalty" name ="regContPenalty" ng-model="regularContract.regContPenalty" value={{regularContract.regContPenalty}}></td>
			    </tr>
			    
			    <tr>
					<td>Regular Contract Bonus</td>
					<td><input type ="text" id="regContBonus" name ="regContBonus" ng-model="regularContract.regContBonus" value={{regularContract.regContBonus}}></td>
			    </tr>
			    
			    <tr>
					<td>Regular Contract Detention</td>
					<td><input type ="text" id="regContDetention" name ="regContDetention" ng-model="regularContract.regContDetention" value={{regularContract.regContDetention}}></td>
			    </tr>
			    
			    <tr>
					<td>Regular Contract Type</td>
					<td><input type ="text" id="regContType" name ="regContType" ng-model="regularContract.regContType" value={{regularContract.regContType}}></td>
				</tr>
	             
			 	<tr>
					<td>Regular Contract Additional Rate</td>
					<td><input type ="text" id="regContAdditionalRate" name ="regContAdditionalRate" ng-model="regularContract.regContAdditionalRate" value={{regularContract.regContAdditionalRate}} ></td>
			    </tr>
			   
			   <tr>
					<td>Regular Contract Value</td>
					<td><input type ="text" id="regContValue" name ="regContValue" ng-model="regularContract.regContValue" value={{regularContract.regContValue}}></td>
			    </tr>	
			    
			    <tr>
					<td>RegularContract Proportionate</td>
					<td><input type="text" id="regContProportionate" name ="regContProportionate" ng-model="regularContract.regContProportionate" value={{regularContract.regContProportionate}}>
					</td>
				</tr>
				
				<tr>
					<td>Regular Contract DC</td>
					<td><input type="text" id="regContDc" name ="regContDc" ng-model="regularContract.regContDc" value={{regularContract.regContDc}}></td>
			   </tr>
			   
			   <tr>
					<td>Regular Contract Cost Grade</td>
					<td><input type="text" id="regContCostGrade" name ="regContCostGrade" ng-model="regularContract.regContCostGrade" value={{regularContract.regContCostGrade}}></td>
				</tr>
				
				<tr>
					<td>Regular Contract Weight</td>
					<td><input type ="text" id="regContWt" name ="regContWt" ng-model="regularContract.regContWt" value={{regularContract.regContWt}}></td>
				</tr>
				
				<tr>
					<td>Regular Contract DDL</td>
					<td><input type ="text" id="regContDdl" name ="regContDdl" ng-model="regularContract.regContDdl" value={{regularContract.regContDdl}}></td>
				</tr>
				
				<tr>
					<td>Regular Contract Renew</td>
					<td><input type ="text" id="regContRenew" name ="regContRenew" ng-model="regularContract.regContRenew" value={{regularContract.regContRenew}}></td>
				</tr>
				
				<tr>
					<td>Regular Contract Renew Date</td>
					<td><input type ="text" id="regContRenewDt"  name ="regContRenewDt" ng-model="regularContract.regContRenewDt" value={{regularContract.regContRenewDt}}></td>
				</tr>
				
				<tr>
					<td>Regular Contract Insured By</td>
					<td><input type ="text" id="regContInsuredBy" name ="regContInsuredBy" ng-model="regularContract.regContInsuredBy" value={{regularContract.regContInsuredBy}}></td>
				</tr>
				
				<tr>
					<td>Regular Contract Insure Company</td>
					<td><input type ="text" id="regContInsureComp" name ="regContInsureComp" ng-model="regularContract.regContInsureComp" value={{regularContract.regContInsureComp}}></td>
				</tr>
				
				<tr>
					<td>Regular Contract Insure Unit Number</td>
					<td><input type ="text" id="regContInsureUnitNo" name ="regContInsureUnitNo" ng-model="regularContract.regContInsureUnitNo" value={{regularContract.regContInsureUnitNo}}></td>
				</tr>
				
				<tr>
					<td>Regular Contract Insure Policy Number</td>
					<td><input type ="text" id="regContInsurePolicyNo" name ="regContInsurePolicyNo" ng-model="regularContract.regContInsurePolicyNo" value={{regularContract.regContInsurePolicyNo}}></td>
				</tr>
				
				<tr>
					<td>Regular Contract Remark</td>
					<td><input type ="text" id="regContRemark" name ="regContRemark" ng-model="regularContract.regContRemark" value={{regularContract.regContRemark}}></td>
				</tr>
			   
				
	            <tr>
					<td align ="center"><input type="submit" value="Submit!" ></td>
				</tr>
	       </table>
	      </div>
	   </form>
	</div>

</body>
</html>
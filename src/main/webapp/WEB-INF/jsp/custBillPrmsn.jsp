<div ng-show="adminLogin || superAdminLogin" >
<div class="row">
	<div class="col s3 hide-on-med-and-down"> &nbsp; </div>
	<form class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="custForm" ng-submit="viewCustomer(custForm)">
	
			<div class="input-field col s6">
   				<input type="text" id="custId" name="custName" ng-model="customer" ng-click="openCustDB()" ng-required="true" readonly/>
   				<label>Select Customer </label>
 			</div>
			
		 	<div class="input-field col s6 center">
		 		<input class="btn waves-effect waves-light" type="submit" value="Submit">
		 	</div>
			
	</form>
	
	
	
</div>


	<div id ="setCustId" ng-hide="setCustFlag">
		  <input type="text" name="filterCust" ng-model="filterCust" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Customer Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cust in custList | filter:filterCust">
		 	  <td><input type="radio"  name="cust"   value="{{ cust }}" ng-model="custCode" ng-click="saveCustomer(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
	</div>


	<div class="row" ng-show="showCust">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
	<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
		<h4>Here's the Billing Detail Of <span class="teal-text text-lighten-2">{{ customer }}</span>:</h4>
			<table class="table-hover table-bordered table-bordered">
				<tr>
		            <td>Customer Name:</td>
	                <td>{{actCust.custName}}</td>
	            </tr>
	            
	            <tr>
	                <td>Bill Permission From Date:</td>
	                <td>
	                	<input type="date" ng-model="actCust.custBillFrDt" required/>
	                </td>
	            </tr>
	            
	            <tr>
	                <td>Bill Permission To Date:</td>
	                <td>
	                	<input type="date" ng-model="actCust.custBillToDt" required/>
	                </td>
	            </tr>
	            
	            <tr>
	                <td>Bill Allow:</td>
	                <td>
	                	<select name="billPerName" id="billPerId" ng-model="actCust.custBillPer" required>
							<option value='true'>YES</option>
							<option value='false'>NO</option>
						</select>
	                </td>
	            </tr>
	            
	            <tr>
	            	<td></td>
	            	<td>
	            		<input type="button" value="SUBMIT" ng-click="saveCustBill(actCust)"/>
	            	</td>
	            </tr>

	          </table>
		</div>
	</div>
	
</div>
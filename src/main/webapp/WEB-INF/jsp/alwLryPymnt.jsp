<title>Allow LHPV</title>
<div ng-show="operatorLogin || superAdminLogin">
	<div class="row" >
		<form ng-submit="allowFrLhpv()" class="col s12 card"
			style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			
			<div class="input-field col s4">
				<input type="text" name="chlnNoName" id="chlnNoId" ng-model="chlnNo" required> 
				<label>Challan No.</label>
			</div>
			
			<div class="input-field col s4">
					<select ng-model="alwType"
						ng-init="alwType='OB'" required="required">
						<option value="AFA">Advance Fund Allocation</option>
						<option value="APC">Advance Petro Card</option>
						<option value="OB">60 Days Old Balance</option>
					</select> <label>Allow For</label>
				</div>
			
			<div class="row">
				<div class="input-field col s12 center">
					<input type="submit" value="Allow">
				</div>
			</div>

		</form>
	</div>
</div>
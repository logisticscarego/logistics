<style type="text/css">
.printable {display: none;}
            @media print {
                .noprint, .menuwrapper, .ui-dialog, footer, #toast-container {display: none !important;}
                .printable {display: block;
                		width: 10in !important;
                		position: fixed;
                		top: 0px;
                		left: 0px;
                		z-index: 999999;
                		}
                		
                		body {
						    width: 10in;
						    height: 210in;
						     
						  }
            }
            @PAGE {
				  size:A4 landscape;
				margin:0.5cm 0.5cm 0.5cm 0.5cm;
			      }
}
</style>

<div ng-show="operatorLogin || superAdminLogin">

<div ng-show="loadingFlag" style=" text-align:center; margin-left: -15px; margin-top: -50px; position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
        <div style="margin-left:20px; margin-top:100px;">
           <svg width='200px' height='200px'  viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-gears">
             <rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect>
                <g transform="translate(-20,-20)">
                      <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#2bbbad ">
                         <animateTransform attributeName="transform" type="rotate" from="90 50 50" to="0 50 50" dur="0.8s" repeatCount="indefinite"></animateTransform>
                       </path>
                 </g>
                 <g transform="translate(20,20) rotate(15 50 50)">
                     <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#0A3F4E ">
                          <animateTransform attributeName="transform" type="rotate" from="0 50 50" to="90 50 50" dur="0.8s" repeatCount="indefinite">
                          </animateTransform>
                      </path>
                  </g>
            </svg>
        </div>
       <span style=" color: #ffffff ;font-size:30px; font-weight:bold; letter-spacing:3px;">Loading...</span>
</div>

	<div class="noprint">
		<form name="billForm" ng-submit=billSubmit(billForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
					<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="brhId" name ="brhName" ng-model="branch" ng-required="true" readonly>
		       			<label for="code">Billing Branch</label>	
		       		</div>
		       		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="billDtId" name ="billDtName" ng-model="bill.blBillDt" ng-blur="checkBlDt(bill.blBillDt)" ng-required="true" >
		       			<label for="code">Bill Date</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="csDtId" name ="csDtName" ng-model="csDt" readonly ng-required="true" >
		       			<label for="code">CS Date</label>	
		       		</div>
		    </div>
		    
		   <div class="row">
		   			<div class="col s4 input-field">
		       			<select name="billTypeName" id="billTypeId" ng-model="bill.blType" ng-init="bill.blType = 'N'" ng-change="chngBlType()" ng-required="true">
								<option value='N'>NORMAL BILL</option>
								<option value='S'>SUPPLEMENTARY BILL</option>
						</select>
						<label>Bill Type</label>
		       		</div>
		       		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="custId" name ="custName" ng-model="customer.custName" ng-click="selectCust()" ng-required="true" readonly>
		       			<label for="code">Customer Name</label>	
		       		</div>

					<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="cnmtId" name ="cnmtName" ng-model="cnmtCode" ng-click="selectCnmt()" ng-required="true" readonly>
		       			<label for="code">Select Cnmt No</label>	
		       		</div>
		    </div>
		    
		    <div ng-show="billDetSerList.length > 0">
		    	<div class="row">
					<table class="tblrow">
					<caption class="coltag tblrow">Bill For {{customer.custName}}</caption>
						<tr class="rowclr">
							<th class="colclr">S.No.</th>
							<th class="colclr">Cn No</th>
							<th class="colclr">Cn Dt.</th>
							<th class="colclr">Lry No</th>
							<th class="colclr">Fr Stn</th>
							<th class="colclr">To Stn</th>
							<th class="colclr">Weight MT</th>
							<th class="colclr">Rate</th>
							<th class="colclr">Freight</th>
							<th class="colclr">Detention</th>
							<th class="colclr">Bonus</th>
							<th class="colclr">Ldng</th>
							<th class="colclr">UnLdng</th>
							<th class="colclr">Other</th>
							<th class="colclr">Total</th>
							<th class="colclr"></th>
						</tr>
						<tr class="tbl" ng-repeat="blDetSer in billDetSerList">
							<td class="rowcel">{{$index+1}}</td>
							<td class="rowcel">{{blDetSer.cnmt.cnmtCode}}</td>
							<td class="rowcel">{{blDetSer.cnmt.cnmtDt | date : format : timezone}}</td>
							<td class="rowcel">{{blDetSer.billDetail.bdLryNo}}</td>
							<td class="rowcel">{{blDetSer.frStn}}</td>
							<td class="rowcel">{{blDetSer.toStn}}</td>
							<td class="rowcel">
								<div ng-show="blDetSer.billDetail.bdBlBase == 'chargeWt'"> 
									{{blDetSer.billDetail.bdChgWt / 1000}}
								</div>
								<div ng-show="blDetSer.billDetail.bdBlBase == 'actualWt'"> 
									{{blDetSer.billDetail.bdActWt / 1000}}
								</div>
								<div ng-show="blDetSer.billDetail.bdBlBase =='receiveWt'"> 
									{{blDetSer.billDetail.bdRecWt / 1000}}
								</div>
							</td>
							<td class="rowcel">{{blDetSer.billDetail.bdRate * 1000 | number:2}}</td>
							<td class="rowcel">{{blDetSer.billDetail.bdFreight}}</td>
							<td class="rowcel">{{blDetSer.billDetail.bdDetAmt}}</td>
							<td class="rowcel">{{blDetSer.billDetail.bdBonusAmt}}</td>
							<td class="rowcel">{{blDetSer.billDetail.bdLoadAmt}}</td>
							<td class="rowcel">{{blDetSer.billDetail.bdUnloadAmt}}</td>
							<td class="rowcel">{{blDetSer.billDetail.bdOthChgAmt}}</td>
							<td class="rowcel">{{blDetSer.billDetail.bdTotAmt}}</td>
							<td class="rowcel">
				            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="removeBillDet($index)" >
									<i class="mdi-action-delete white-text"></i>
								</a>
							</td>
						</tr>
					</table>
				</div>
		    </div>
		    
		    <div ng-show="billDetSerList.length > 0">
		    	<div class="row">
		    		
		    	</div>
		    </div>
		    
		    
		    <div class="row">
		   				       		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="subTotId" name ="subTotName" ng-model="bill.blSubTot" ng-required="true" ng-readonly="readFlag">
		       			<label for="code">Sub Total</label>	
		       		</div>

					<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="txbSerId" name ="txbSerName" ng-model="bill.blTaxableSerTax" ng-required="true" readonly>
		       			<label for="code">Taxable Service Tax</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="serId" name ="serName" ng-model="bill.blSerTax" ng-required="true" readonly>
		       			<label for="code">Service Tax</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		   				       		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="sbcId" name ="sbcName" ng-model="bill.blSwachBhCess" ng-required="true" readonly>
		       			<label for="code">Swachh Bharat Cess</label>	
		       		</div>

					<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="kcId" name ="kkcName" ng-model="bill.blKisanKalCess" ng-required="true" readonly>
		       			<label for="code">Kisan kalyan Cess</label>	
		       		</div>
		       		
					<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="fTotId" name ="fTotName" ng-model="bill.blFinalTot" ng-required="true" ng-readonly="readFlag">
		       			<label for="code">Final Total</label>	
		       		</div>
		       		
		    </div>
		    
		    <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="bill.blDesc" ng-required="true"></textarea>
 					<label>Description</label>
				</div>
    		 </div>	
		    
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	      </div>		
	     
	</form>
	</div>  
	
	
	<div id ="selCustId" ng-hide="selCustFlag" class="noprint">
		  <input type="text" name="filterCust" ng-model="filterCust" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Customer Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cust in custList | filter:filterCust">
		 	  <td><input type="radio"  name="cust"   value="{{ cust }}" ng-model="custCode" ng-click="saveCustomer(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="fromToDataDB" ng-hide="fromToDateDBFlag" class="noprint">
		<div class="row" style="width: 100%;">
			<form name="CnmtFromToDate" ng-submit="takeCnmtFromToDate(CnmtFromToDate)" class="col s12 card"	style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
				<div class="input-field col s3" style="width: 38%;">
					<input type="date" name="cnmtFromDt" id="cnmtFromDt" ng-model="dateRange.cnmtFromDt" ng-required="true">
					<label>From Date</label>
				</div>
				<div class="input-field col s3" style="width: 38%;">
					<input type="date" name="cnmtToDt" id="cnmtToDt" ng-model="dateRange.cnmtToDt"	ng-required="true">
					<label>To Date</label>
				</div>
				<div class="input-field col s13" style="width: 20%;">
					<input type="submit" value="Submit">
				</div>	
			</form>
		</div>
	</div>
	
	
	<div id ="selCnmtId" ng-hide="selCnmtFlag">
		  <input type="text" name="filterCnmt" ng-model="filterCnmt" placeholder="Search....">
 	  <table ng-show="bill.blType == 'N'">
 	  		<tr>
 	  			<th>
 	  				<input type="checkbox" id="selectAllCnmtChkNor" ng-model="selectAllCnmt"> 
 	  				<span id="selectAllCnmtNor">Select All</span>
 	  			</th>
 	  			<th>
 	  				<input type="button" ng-click="seUnseAllCnmt('normal')" value="Submit"> 	  				
 	  			</th>
 	  		</tr>
 	  	  <tr>
 	  	  	  <th>S.No.</th>
 	  	  	  <th></th>
 	  	  	  <th>CNMT NO</th> 	  	  	  
 	  	  </tr>
 	  
		  <tr ng-repeat="cnmt in cnmtList | filter:filterCnmt">
		  		<td>{{$index+1}}</td>
		 	  <td><input type="radio"  name="cnmt"   value="{{ cnmt }}" ng-model="cnmtCode" ng-click="saveCnmt(cnmt)"></td>
              <td>{{cnmt}}</td>              
          </tr>
      </table> 
      
      <table ng-show="bill.blType == 'S'">
      		<tr>
 	  			<th colspan="1">
 	  				<input type="checkbox" id="selectAllCnmtChkSup" ng-model="selectAllCnmt" ng-checked="selectAllCnmt" ng-click="seUnseAllCnmt('sup')"> 
 	  				<span id="selectAllCnmtSup">Select All</span>
 	  			</th>
 	  			<th>
 	  				<input type="button" ng-click="seUnseAllCnmt('normal')" value="Submit"> 	  				
 	  			</th>
 	  		</tr>
 	  	  <tr>
 	  	  	<th>S.No.</th>
 	  	  	  <th></th>
 	  	  	  <th>CNMT NO</th>
 	  	  </tr>
 	  	  
 	  	<!--   <tr>
 	  	  <td><input type="radio"  name="zeroCnmt"   value="{{zeroCnmt}}" ng-model="cnmtCode" ng-click="saveCnmt(zeroCnmt)" ng-show="hillFlag"></td>
 	  	  <td>{{zeroCnmt}}</td>
 	  	  </tr>
 	  -->
		  <tr ng-repeat="cnmt2 in cnmtList2 | filter:filterCnmt">
		  		<td>{{$index+1}}</td>
		 	  <td><input type="radio"  name="cnmt2"   value="{{ cnmt2 }}" ng-model="cnmtCode" ng-click="saveCnmt(cnmt2)"></td>
              <td>{{cnmt2}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id="cnmtBillId" ng-hide="cnmtBillFlag" class="noprint">
	<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="cnmtBillForm" ng-submit="saveCnBillForm(cnmtBillForm)">
		 <div class="row">
			    <div class="col s4 input-field">
					<input class="validate" type ="date" name="cnmtDt" id="cnmtDtId" ng-model="actCnmt.cnmtDt" ng-required="true" readonly>
					<label>Cnmt Date</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="text" name="frStnName" id="frStnId" ng-model="frStn" ng-required="true" readonly />
					<label>From Station</label>
				</div>
				
				 <div class="col s4 input-field">
					<input type ="text" name="toStnName" id="toStnId" ng-model="toStn" ng-requierd="true" readonly>
					<label>To Station</label>
				</div>
	     </div>
	     
	     <div class="row">
	     		
				<div class="col s4 input-field">
					<input class="validate" type ="number" name="actWtName" id="actWtId" ng-model="billDet.bdActWt" ng-keyup="calBillTot()" step="0.001" min="0.000" ng-required="true" readonly>
					<label>Actual Wt(kg)</label>
				</div>
				
				<div class="col s4 input-field">
					<input class="validate" type ="number" name="recWtName" id="recWtId" ng-model="billDet.bdRecWt" ng-keyup="calBillTot()" step="0.001" min="0.000"  readonly>
					<label>Receiving Wt(kg)</label>
				</div>
				
			    <div class="col s4 input-field">
					<input class="validate" type ="number" name="wtName" id="wtId" ng-model="billDet.bdChgWt" ng-keyup="calBillTot()" step="0.001" min="0.000" ng-required="true" readonly>
					<label>Charge Wt(kg)</label>
				</div>
		
	     </div>
	     
	     <div class="row">
	     		
	     		<div class="col s4 input-field">
					<input type ="text" name="lryName" id="lryId" ng-model="billDet.bdLryNo" readonly />
					<label>Lorry No</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="rateName" id="rateId" ng-model="billDet.bdRate" ng-keyup="calBillTot()" step="0.000001" min="0.000000" ng-requierd="true" readonly>
					<label>Rate per kg</label>
				</div>
				
	     		<div class="col s4 input-field">
					<input type ="number" name="frgName" id="frgId" ng-model="billDet.bdFreight" ng-keyup="calBillTot()" ng-required="true" step="0.001" min="0.000" ng-readonly="bill.blType == 'N'" />
					<label>Freight</label>
				</div>
				
	     </div>
	     
	     
	     <div class="row">
	     
	     		<div class="col s4 input-field">
					<input class="validate" type ="number" name="ldName" id="ldId" ng-model="billDet.bdLoadAmt" ng-keyup="calBillTot()" ng-init="billDet.bdLoadAmt = 0" ng-required="true" >
					<label>Loading</label>
				</div>
			
				 <div class="col s4 input-field">
					<input class="validate" type ="number" name="unldName" id="unldId" ng-model="billDet.bdUnloadAmt" ng-keyup="calBillTot()" ng-init="billDet.bdUnloadAmt = 0" ng-required="true" >
					<label>UnLoading</label>
				</div>
				
	     		<div class="col s4 input-field">
					<input class="validate" type ="number" name="bnsName" id="bnsId" ng-model="billDet.bdBonusAmt" ng-keyup="calBillTot()" ng-init="billDet.bdBonusAmt = 0" ng-required="true">
					<label>Bonus</label>
				</div>
		
	     </div>
	     
	     <div class="row">
	     
	     		 <div class="col s4 input-field">
					<input class="validate" type ="number" name="detName" id="detId" ng-model="billDet.bdDetAmt" ng-keyup="calBillTot()" ng-init="billDet.bdDetAmt = 0" ng-required="true" >
					<label>Detention</label>
				</div>
				
				<div class="col s4 input-field">
					<input class="validate" type ="number" name="totName" id="totId" ng-model="billDet.bdTotAmt" ng-init="billDet.bdTotAmt = 0" ng-required="true" readonly step="0.001" min="0.000">
					<label>Total Amt</label>
				</div>
				
				<div class="col s4 input-field">
					<select name="supBlRsnName" id="supBlRsnId" ng-model="billDet.bdSupBlRsn" disabled="disabled">
							<option value='RATE DIFFERENCE'>RATE DIFFERENCE</option>
							<option value='WEIGHT DIFFERENCE'>WEIGHT DIFFERENCE</option>
							<option value='LOADING'>LOADING</option>
							<option value='UNLOADING'>UNLOADING</option>
							<option value='DETENTION'>DETENTION</option>
							<option value='OTHERS'>OTHERS</option>
					</select>
					<label>Supplementary Bill Reason</label>
				</div>  
				
				<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="esnId" name ="esnNumber"  ng-model="billDet.bdEntryShNum"   ng-init="billDet.bdEntryShNum ='0'" ng-required="true" ng-show="adaniCustomer">
		       			<label for="code" ng-show="adaniCustomer">Entery sh no</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="shId" name ="shNumber" ng-model="billDet.bdShipmentNum"  ng-init="billDet.bdShipmentNum ='0'" ng-required="true" ng-show="adaniCustomer" >
		       			<label for="code" ng-show="adaniCustomer">Shipment no</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="obdId" name ="obdNumber" ng-model="billDet.bdObdNum"  ng-init="billDet.bdObdNum ='0'" ng-required="true" ng-show="raigardhJindal" >
		       			<label for="code" ng-show="raigardhJindal">Obd no</label>	
		       		</div>
		       		
		       	     		
		      <!--  		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="soId" name ="salesONumber" ng-model="billDet.bdsalesOdrNum"  ng-init="billDet.bdsalesOdrNum ='0'" ng-required="true" ng-show="raigardhMsp" >
		       			<label for="code" ng-show="raigardhMsp">Sales Order no</label>	
		       		</div>-->
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="PoId" name ="poNumber" ng-model="billDet.bdPoNum"  ng-init="billDet.bdPoNum ='0'" ng-required="true" ng-show="hillLimited">
		       			<label for="code" ng-show="hillLimited">Po no</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="TpId" name ="tpNumber" ng-model="actCnmt.cnmtTpNo"  ng-show="bonaiBarbil" readonly>
		       			<label for="code" ng-show="bonaiBarbil">Tp no</label>	
		       		</div>
		        </div>
	     
	    
	    <div class="row"> 
	     		<div class="col s4 input-field">
					<input type ="button" name="disChlnName" id="disChlnId" value="Display Challan" ng-click="displayChln()">
				</div>
				
				<div class="col s4 input-field">
					<input type ="button" name="othChgName" id="othChgId" value="Other Charges" ng-click="otChg()">
				</div>
				
	     		<div class="input-field col s4 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	    </div>
	     <div class="row">
	     	<div class="col s12 input-field" ng-show="billDet.bdOthChgList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">OTHER CHARGES</caption>
						<tr>
							<th class="colclr">Type</th>
							<th class="colclr">Value</th>
							<th class="colclr">Action</th>
						</tr>
						<tr ng-repeat="otChg in billDet.bdOthChgList">
							<td class="rowcel">{{otChg.otChgType}}</td>
							<td class="rowcel">{{otChg.otChgValue}}</td>
							<td class="rowcel">
								<input type="button" name="rmvName" id="rmvId" value="REMOVE" ng-click="removeOth($index)"/>
							</td>
						</tr>
					</table>
				</div>
	     </div>

	</form>
	<table class="tblrow">
					<caption class="coltag tblrow">CHALLAN DETAIL</caption>
						<tr>
							<th class="colclr">Chln</th>
							<th class="colclr">Ttl Frt</th>
							<th class="colclr">Ext Amt</th>
							<th class="colclr">Ext KM</th>
							<th class="colclr">Unldng</th>
							<th class="colclr">Ovr Ht</th>
							<th class="colclr">Pnlty</th>
							<th class="colclr">Detn</th>
							<th class="colclr">Desc</th>
						</tr>
						<tr ng-repeat="act in actChlnList">
							<td class="rowcel">{{act.chlnCode}}</td>
							<td class="rowcel">{{act.chlnTotalFreight}}</td>
							<td class="rowcel">{{act.chlnExtra}}</td>
							<td class="rowcel">{{act.extKm}}</td>
							<td class="rowcel">{{act.unldng}}</td>
							<td class="rowcel">{{act.ovrHgt}}</td>
							<td class="rowcel">{{act.pen}}</td>
							<td class="rowcel">{{act.det}}</td>
							<td class="rowcel">{{act.arDesc}}</td>
							</td>
						</tr>
		</table>
	</div>
	
	
	<div id="otChgDB" ng-hide="otChgFlag" class="noprint">
		<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="OtherChargeForm" ng-submit="saveOtChg(OtherChargeForm,ot)">
			 <div class="row">
				    <div class="col s6 input-field">
						<select name="otChgTypeName" id="otChgTypeId" ng-model="ot.otChgType" ng-options="otChgType as otChgType for otChgType in otChgTypeList" required></select>
						<label>Type</label>
					</div>
					
					<div class="col s6 input-field">
						<input type="number" name="otChgValueName" id="otChgValueId" ng-model="ot.otChgValue" required>
						<label>Value</label>
					</div>
		     </div>
		     
		     <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       </div>	
	     </form>
	</div>
	
	<div id ="disChlnCodeDB" ng-hide="disChlnCodeFlag">
		  <input type="text" name="filterChln" ng-model="filterChln" placeholder="Search....">
		  <table>
		  	  <tr>
		  	  	  <th></th>
		  	  	  <th>Challan No</th>
		  	  </tr>
		  
		  	<tr ng-repeat="chln in actChlnList | filter:filterChln">
		 	  <td><input type="radio"  name="chln"   value="{{ chln }}" ng-model="chlnCode" ng-click="selectChln(chln)"></td>
	             <td>{{chln.chlnCode}}</td>
	         </tr>
	     </table> 
	</div>
	
	
	<div id="disSelChlnDB" ng-hide="disSelChlnFlag" class="noprint">
		<h4> Here's the details of Challan <span class="teal-text text-lighten-2">{{selChln.chlnCode}}</span>:</h4>
			<table class="table-hover table-bordered table-condensed">
	            <tr>
	                <td>Challan No:</td>
	                <td>{{selChln.chlnCode}}</td>
	            </tr> 
	            <tr>
	                <td>Challan Date:</td>
	                <td>{{selChln.chlnDt | date : format : timezone}}</td>
	            </tr> 
			    <tr>
	                <td>From Station:</td>
	                <td>{{selChln.chlnFromStn}}</td>
	            </tr>
	            <tr>
	                <td>To Station:</td>
	                <td>{{selChln.chlnToStn}}</td>
	            </tr> 
	            <tr>
	                <td>Lorry No:</td>
	                <td>{{selChln.chlnLryNo}}</td>
	            </tr> 
			    <tr>
	                <td>Lorry Rate:</td>
	                <td>{{selChln.chlnLryRate}}</td>
	            </tr>
	            <tr>
	                <td>Charge Wt:</td>
	                <td>{{selChln.chlnChgWt}}</td>
	            </tr> 
	            <tr>
	                <td>No Of Package:</td>
	                <td>{{selChln.chlnNoOfPkg}}</td>
	            </tr> 
			    <tr>
	                <td>Total Wt:</td>
	                <td>{{selChln.chlnTotalWt}}</td>
	            </tr>
	            <tr>
	                <td>Freight:</td>
	                <td>{{selChln.chlnFreight}}</td>
	            </tr> 
	            <tr>
	                <td>Total Freight:</td>
	                <td>{{selChln.chlnTotalFreight}}</td>
	            </tr> 
	            <tr>
	                <td>Advance:</td>
	                <td>{{selChln.chlnAdvance}}</td>
	            </tr> 
			    <tr>
	                <td>Balance:</td>
	                <td>{{selChln.chlnBalance}}</td>
	            </tr>
	            <tr>
	                <td>Loading Amt:</td>
	                <td>{{selChln.chlnLoadingAmt}}</td>
	            </tr> 
	            <tr>
	                <td>Statistical Charge:</td>
	                <td>{{selChln.chlnStatisticalChg}}</td>
	            </tr>
			    <tr>
	                <td>Extra Amt:</td>
	                <td>{{selChln.chlnExtra}}</td>
	            </tr>
	            <tr>
	                <td>Pay At:</td>
	                <td>{{selChln.chlnPayAt}}</td>
	            </tr> 
			    <tr>
	                <td>Time Allow:</td>
	                <td>{{selChln.chlnTimeAllow}}</td>
	            </tr>
	            <tr>
	                <td>Broker Rate:</td>
	                <td>{{selChln.chlnBrRate}}</td>
	            </tr>
	            <tr>
	                <td>Wt Slip:</td>
	                <td>{{selChln.chlnWtSlip}}</td>
	            </tr> 
			    <tr>
	                <td>Vehicle Type:</td>
	                <td>{{selChln.chlnVehicleType}}</td>
	            </tr>
	            <tr>
	                <td>Tds Amt:</td>
	                <td>{{selChln.chlnTdsAmt}}</td>
	            </tr>
	            <tr>
	                <td>Train No:</td>
	                <td>{{selChln.chlnTrainNo}}</td>
	            </tr> 
			    <tr>
	                <td>Lorry Load Time:</td>
	                <td>{{selChln.chlnLryLoadTime}}</td>
	            </tr>
	            <tr>
	                <td>Lorry Rep Dt:</td>
	                <td>{{selChln.chlnLryRepDT | date : format : timezone}}</td>
	            </tr>
	            <tr>
	                <td>Lorry Rep Time:</td>
	                <td>{{selChln.chlnLryRptTime}}</td>
	            </tr> 
			    <tr>
	                <td>Owner:</td>
	                <td>{{selChln.ownName}}</td>
	            </tr>
	            <tr>
	                <td>Broker:</td>
	                <td>{{selChln.brkName}}</td>
	            </tr>
	            <tr>
	                <td>Receiving Pkg:</td>
	                <td>{{selChln.recPkg}}</td>
	            </tr>
	            <tr>
	                <td>Unloading:</td>
	                <td>{{selChln.unldng}}</td>
	            </tr>
	            <tr>
	                <td>Detention:</td>
	                <td>{{selChln.det}}</td>
	            </tr>
	            <tr>
	                <td>Others:</td>
	                <td>{{selChln.oth}}</td>
	            </tr>
	            <tr>
	                <td>Late Delivery:</td>
	                <td>{{selChln.ltDel}}</td>
	            </tr>
	            <tr>
	                <td>Late Ack:</td>
	                <td>{{selChln.ltAck}}</td>
	            </tr>
	            <tr>
	                <td>Extra Km:</td>
	                <td>{{selChln.extKm}}</td>
	            </tr>
	            <tr>
	                <td>Over Height:</td>
	                <td>{{selChln.ovrHgt}}</td>
	            </tr>
	            <tr>
	                <td>Penalty:</td>
	                <td>{{selChln.pen}}</td>
	            </tr>
	            <tr>
	                <td>Wt Shortage:</td>
	                <td>{{selChln.wtSrt}}</td>
	            </tr>
	            <tr>
	                <td>Driver Recovery:</td>
	                <td>{{selChln.drRcvr}}</td>
	            </tr>
	            <tr ng-if="selChln.chlnArId > 0">
	                <td>Download Cnmt Image:</td>
	                <td><input type="button" value="DOWNLOAD" ng-click="getCnImage()"></td>
	            </tr>
	            <!-- <tr ng-if="selChln.chlnArId > 0">
	            	<td></td>
	            	<td><img ng-src="data:application/pdf;base64,{{image}}"></td>
	            </tr> -->
	        </table>  
	</div>
	
	
	
	<div id="finalSubId" ng-hide="finalSubmit" class="noprint"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch</td>
					<td>{{branch}}</td>
				</tr>
				
				<tr>
					<td>Bill Date</td>
					<td>{{bill.blBillDt | date : format : timezone}}</td>
				</tr>
				
				<tr>
					<td>Cs Date</td>
					<td>{{csDt | date : format : timezone}}</td>
				</tr>
				
				<tr>
					<td>Bill Type</td>
					<td>{{bill.blType}}</td>
				</tr>
				
				<tr>
					<td>Customer</td>
					<td>{{customer.custName}}</td>
				</tr>
				
				<tr>
					<td>SubTotal</td>
					<td>{{bill.blsubTot}}</td>
				</tr>
				
				<tr>
					<td>Taxable Service Tax</td>
					<td>{{bill.blTaxableSerTax}}</td>
				</tr>
				
				<tr>
					<td>Service Tax</td>
					<td>{{bill.blSerTax}}</td>
				</tr>
				
				<tr>
					<td>Swachh Bharat Cess</td>
					<td>{{bill.blSwachBhCess}}</td>
				</tr>
				
				<tr>
					<td>Kisan kalyan Cess</td>
					<td>{{bill.blKisanKalCess}}</td>
				</tr>
				
				<tr>
					<td>Final Total</td>
					<td>{{bill.blFinalTot}}</td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td>{{bill.blDesc}}</td>
				</tr>
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" id="saveId" ng-click="saveBill()"/>
		</div>
	</div>
	
	<div id="billGenId" ng-hide="billGenFlag" class="noprint">
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">{{PRINT_STMT}} the Bill {{genBillNo}}</h5></div>
		</div>
		<div>
			<div class="col s12 center">
				<a class="btn white-text" ng-click="cancelPrint()">Cancel</a>
				<a class="btn white-text" ng-click="printVs()">Yes</a>
			</div>
		</div>
	</div>
	
	
	
	<div class="printable"> 
			 <div class="divTableb" ng-repeat="bill in blList">
			 
			 	<div ng-show="bill.relBillFormat">
						<div class="headRowlld">
			                <div class="latterhead"><img src="resources/img/latterhead.png" height="185px" /></div>
			            </div>
			          
			           <div class="divRowl">
			                <div class="latterhead1">FREIGHT INVOICE</span></div>
			             </div>
			          
			          <div class="divRowlld">
			                <div class="latterhead2">Invoice No {{bill.bill.blBillNo}}</span></div>
			                <div class="latterhead2" style="text-align:right;">Date: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}}</span></div>
			             </div>
			          
			            <div class="CSSTableGeneratorhd">
								<table>
									<tr> 
										<td>1. Name & Address of Goods Transport Agency</td>
										<td>CARE GO LOGISTICS PVT LTD. {{selBrh.branchAdd}}</td>
									</tr>
			
			                         <tr> 
										<td>2. Service tax Registration No for payment of Service Tax & PAN No.</td>
										<td><span>AAGCC0032KSD001</span><span> & AAGCC0032K</span></td>
									</tr>
									
									<tr> 
										<td>3. Name and Address of Person Liable For service Tax</td>
										<td>{{bill.cust.custName}}, {{bill.cust.custAdd}}, {{bill.cust.custCity}}-{{bill.cust.custPin}}</td>
									</tr>
			
			                         <tr> 
										<td>4. Service Category</td>
										<td>TRANSPORT OF GOODS ROAD SERVICE</td>
									</tr>
									
									<tr> 
										<td>5. Details of Transportation</td>
										<td>As per Annexure attached</td>
									</tr>
			
			                         <tr> 
										<td><div class="csstd2">i. Supply Invoice No. & Date</div></td>
										<td>As per Annexure attached</td>
									</tr>
									
									<tr> 
										<td><div class="csstd2">ii. Consignment Note No. & Date</div></td>
										<td>As per Annexure attached</td>
									</tr>
			
			                         <tr> 
										<td><div class="csstd2">iii. Weight of Consignment</div></td>
										<td>As per Annexure attached</td>
									</tr>
									<tr> 
										<td><div class="csstd2">iv. Description of Goods</div></td>
										<td>As per Annexure attached</td>
									</tr>
			
			                         <tr> 
										<td><div class="csstd2">v. Place of Origin</div></td>
										<td>As per Annexure attached</td>
									</tr>
									
									<tr> 
										<td><div class="csstd2">vi. Vehicle Number</div></td>
										<td>As per Annexure attached</td>
									</tr>
			
			                         <tr> 
										<td><div class="csstd2">vii. Freight rate / unit of measure</div></td>
										<td>As per Annexure attached</td>
									</tr>
									
									<tr> 
										<td><div class="csstd2">viii. Name & Addresh of Goods Transport Agency></td>
										<td>As per Annexure attached</td>
									</tr>
			
			                         <tr> 
										<td>
										<div class="csstd2">ix. Value of Total Freight</div>
										<div class="csstd1">(including handling delivery charge)</div>
										</td>
										<td>{{bill.bill.blSubTot}}</td>
									</tr>
									
									<tr> 
										<td>
										<div class="csstd2">x. Taxable Service Amount</div>
										<div class="csstd1">(30% of above value)</div>
										</td>
										<td>{{bill.bill.blTaxableSerTax}}</td>
									</tr>
			
			                         <tr> 
										<td>
									   	<div class="csstd2">xii. Service tax Payable</div>
										<div class="csstd1">a) Service Tax @ 14%</div>
										 <div class="csstd1">SB Cess@ 0.5% on TSA</div>
										</td>
										
										<td>
										<div>{{bill.bill.blSerTax}}</div>
										 <div>{{bill.bill.blSwachBhCess}}</div>
										 </td>
									</tr>
									
									<tr> 
										<td><div  class="csstd2">xii. Tatal Invoice Value</div></td>
										<td>{{bill.bill.blFinalTot}}</td>
									</tr>
			
			                         <tr> 
										<td><div  class="csstd2">xii. Tatal Invoice Value in words</div></td>
										<td>{{bill.bilAmt}}</td>
									</tr>
			
								</table>
							</div>
			       
			               
			               <div class="divRowlld">
			                <div class="latterhead3">Declaration</span></div>
			             </div>
			       
			             <div>
								<table>
									<tr  class="csstd"> 
										<td>1. 70% abatement availed in term of Natification no. 8/2015-ST dated 01.03.2015</td>
									</tr>
									<tr  class="csstd"> 
										<td>2. we have not taken Cenvat credit of duthy paid on input or capital goods or Cenvat credit of service tax of input service, used for providing these services</td>
									</tr>
								</table>
							</div>
			       
			            <div class="divRowlld">
			                <div class="latterhead4">Enclosure:</span></div>
			             </div> 
			             
			             <div class="divRowlld">
			                <div class="latterhead5">1. Annexure 2. Original LR's</span></div>
			             </div>      
			            
			             <div class="divRow1ld">
			                <div class="latterhead6">care go logistics pvt. ltd.</span></div>
			             </div> 
			             
			             <div class="divRowlld">
			                <div class="latterhead7">Regd. office - Flat no. 402, Baroda House Apartment, Sector-10, Dwarka, New Delhi-110075, Cin No.-U60231DL2015PTC279265</div>
			            <div class="latterhead7">Corporate Office - Sco-26, 1st Floor, Sec.-15, Part-II, Gurgaon-122001, Ph 0124-4205305</div>
			             </div>
			 	</div>
			 
				 <div ng-show="bill.hndlBillFormat">
				 		<!-- <style type="text/css">
					 	   @PAGE {
							  size:A4 landscape;
							margin:0.5cm 0.5cm 0.5cm 0.5cm;
						      }
				 	   </style> -->
						   <div class="headRow1">
                <div class="divCel1"><img src="resources/img/logo.png" width="357" height="106" /></div>
                <div  class="divCel1">
                          <div style="font-size: 16px; font-weight: bold; font-family: Arial;">Care Go Logistics Pvt. Ltd.</div>
                          <div style="font-size: 12px">Billing Office: {{ selBrh.branchAdd }}</div>
                          <div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div> 
                          <div style="font-size: 12px">Email: {{ selBrh.branchEmailId }}</div>
			 </div>
             </div>
            <div class="divRow1">
                  <div class="divCel01 boldbl">
                  	<div ng-show="bill.bill.blType == 'N'">FRIEGHT BILLS</div>
                  	<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
                  </div>
                <div class="divCel02 boldbl">Co CIN No. U60231DL2015PTC279265</div>
                <div class="divCel03 boldbl">Service Tax Regidtration No.: AAGCC0032KSD001</div>
            </div>
            
            
            <div class="divRow1">
                  <div class="divCel04 boldbl">
                  <div class="divCel04 boldbl" style="border:0px; text-align:left;">To, M/s </div>
                
                  <div class="divCel04 boldbl" style="border:0px; margin-top:-15px; margin-left:100px; text-align:justify;">{{bill.cust.custName}}
                  <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}} {{bill.cust.custPin}}</div>
                  
                  </div>
               <div class="divCel066 boldbl">
                   <div class="divCel06 boldbl">
                      <div style="margin-top:2px; text-align:center;">BILL NO.: {{bill.bill.blBillNo}}</div><br><div>DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}}</div>
                   </div>
               </div>
            </div>
            
            <div class="divRow1">
                    <div class="divCel25 boldbl">We hereby Submit our Bill for the Goods Transported as Under:</div>
	                <div class="divCel26 boldbl">OUR PAN NO: AAGCC0032K</div>
	                <div class="divCel26 boldbl">No TDS To Be Deducted w.e.f 01/10/2009</div>
            </div>
            
            
            <div class="divRow1">
               
			<div class="CSSTableGeneratorss">
				<table>
				 <tr>
				   <td class="latterh latterbold" width="450px"colspan="4">CONSIGNMENT DETAILS</td>
				   <td class="latterh latterbold" rowspan="2">Invoice</td>
				   <td class="latterh latterbold" rowspan="2">Truck No.</td>
				   <td class="latterh latterbold" rowspan="2">Product</td>
				   <td class="latterh latterbold" rowspan="2">Weight MT</td>
				   <td class="latterh latterbold" rowspan="2">Rate</td>
				   <td class="latterh latterbold" rowspan="2">Freight(A)</td>
				   <td class="latterh latterbold" rowspan="2">Detention(B)</td>
				   <td class="latterh latterbold"  rowspan="2">Bonus(C)</td>
				   <td class="latterh latterbold" rowspan="2">Ldng/Unldng(D)</td>
				   <td class="latterh latterbold" rowspan="2">Total(A+B+C+D)</td>
				 </tr>
				 <tr>
				   <td class="latterh latterbold">Date</td>
				   <td class="latterh latterbold">LR No.</td>
				   <td class="latterh latterbold">From Station</td>
				   <td class="latterh latterbold">To Station</td>
				 </tr>
				 <tr ng-repeat="bilDet in bill.blDetList">
				   <td class="latterh">{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
				   <td class="latterh">{{bilDet.cnmt.cnmtCode}}</td>
				   <td class="latterh">{{bilDet.frmStn}}</td>
				   <td class="latterh">{{bilDet.toStn}}</td>
				   <td class="latterh">{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}</td>
				   <td class="latterh">{{bilDet.truckNo}}</td>
				   <td class="latterh">{{bilDet.cnmt.cnmtProductType}}</td>
				   <td class="latterh">
				   		<div ng-show="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 2}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 2}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 2}}</div>
				   </td>
				   <td class="latterh">{{bilDet.billDet.bdRate * 1000 | number : 2}}</td>
				   <td class="latterh">{{bilDet.billDet.bdFreight | number : 2}}</td>
				   <td class="latterh">{{bilDet.billDet.bdDetAmt | number : 2}}</td>
				   <td class="latterh">{{bilDet.billDet.bdBonusAmt | number : 2}}</td>
				   <td class="latterh">{{bilDet.billDet.bdLoadAmt | number : 2}}/{{bilDet.billDet.bdUnloadAmt | number : 2}}</td>
				   <td class="latterh">{{bilDet.billDet.bdTotAmt | number : 2}}</td>
				 </tr>
				
				</table>
				              
			</div>
      </div>
       
       
       
         <div class="divRow1">
                  <div class="divCel16">
                     <div class="divCel17 bold2bl">IT IS HEREBY DECLARE THAT WE ARE NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES /CAPITAL GOODS</div>
                     <div class="divCel17 bold2bl">WE HAVE NOT AVAILED ANY BENEFIT UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008</div>
                   </div>
               
               
             
                
                 <div class="divCel15 bold2bl" style="text-align:center;">
                     <div class="divCel14 bold1bl paddingbl" >SUB TOTAL</div>
                     <div class="divCel14 bold1bl paddingbl" >{{bill.bill.blSubTot}}</div> 
                  </div>
               
            </div>
       
        
         <div class="divRow1">
                     <div class="divCel18 bold1bl">Bank Name</div>
                     <div class="divCel00 bold1bl">A/c No.</div>
                     <div class="divCel18 bold1bl">IFSC CODE</div>
                     <div class="divCel05 bold1bl">Branch</div>
                     <div class="divCel19 bold2bl">Taxable Service tax= 30%</div>
                     <div class="divCel14 bold2bl" style="text-align:right; padding-right:5px;">{{bill.bill.blTaxableSerTax}}</div>
                     <div class="divCel14 bold2bl">&nbsp</div>
             </div>
             
             <div class="divRow1">
                     <div class="divCel18 bold1bl padding1bl">Kotak Mahindra Bank</div>
                     <div class="divCel00 bold1bl padding1bl">3011623336</div>
                     <div class="divCel18 bold1bl padding1bl">KKBK0000299</div>
                     <div class="divCel05 bold1bl paddingbl">61, Old Judicial Complex,<br> Sector-15, Gurgaon 122001</div>
                     <div class="divCel19 bold2bl paddingbl">Service Tax= 14%<br> Swachh Bharat Cess= 0.5%</div>
                     <div class="divCel14 bold2bl paddingbl" style="text-align:right;"><span>{{bill.bill.blSerTax}}</span><br><span>{{bill.bill.blSwachBhCess}}</span></div>
                     <div class="divCel14 bold2bl padding1bl">&nbsp</div>
             </div>
       
         <div class="divRow1">
                     <div class="divCel20 bold2bl paddingbl">
                        Only By A/c Payee Cheque / DD in Favour Of <span class="bold1">Care Go Logistics Pvt. Ltd.</span>, or Electronic Fund transfer to <span class="bold1">above Bank A/C</span>
                        <br>You are hereby requested to mail us Payment Detail on mail Id <span class="bold1">{{ selBrh.branchEmailId }}</span>
                     </div>
                     <div class="divCel20 bold1bl padding2bl">Pay {{bill.bilAmt}}</div>
                     <div class="divCel14 bold1bl padding2bl" style="text-align:right;">Total</div>
                     <div class="divCel14 bold1bl padding2bl">{{bill.bill.blFinalTot}}</div>
             </div>
           
             <div class="divRow1">
                <div class="divCel28 bold2bl" style="text-align:center;">
                     <div class="divCel31 bold1bl">Other Bank Details</div>
                     <div class="divCel27 bold1bl">Bank Name</div>
                     <div class="divCel27 bold1">A/c No.</div>
                     <div class="divCel27 bold1bl">IFSC CODE</div>
                     <div class="divCel29 bold1bl">Branch</div>
                     <div class="divCel27 bold1bl padding1bl">HDFC BANK</div>
                     <div class="divCel27 bold1bl padding1bl">13810330000037</div>
                     <div class="divCel27 bold1bl padding1bl">HDFC0001381</div>
                     <div class="divCel29 bold1bl paddingbl">472-11, OLD RAILWAY ROAD, DHOBI GHAT, NEAR KABIR BHAWAN, GURGAON</div>
                     <div class="divCel27 bold1bl padding1bl">ICICI BANK</div>
                     <div class="divCel27 bold1bl padding1bl">103105001831</div>
                     <div class="divCel27 bold1bl padding1bl">ICIC0001031</div>
                     <div class="divCel29 bold1bl paddingbl">SCO - 59, 60, OLD JUDICIAL COMPLEX, SECTOR - 15, GURGAON</div>
                     <div class="divCel27 bold1bl padding1bl">STATE BANK OF INDIA</div>
                     <div class="divCel27 bold1bl padding1bl">35334334071</div>
                     <div class="divCel27 bold1bl padding1bl">SBIN0060414</div>
                     <div class="divCel29 bold1bl paddingbl">SCO - 5,6,7 SECTOR - 15, <br>PART - 2, GURGAON</div>
                 </div>
                 <div class="divCel30" style="height:180px;">
                   <div class="divCel32 bold1bl" style="text-align:right; margin-top:10px; margin-left:-10px;">For Care Go Logistic Pvt. Ltd</div>
                    <div class="divCel32 bold1bl" style="text-align:left; margin-top:125px; margin-left:10px;">Cheked By</div>
                    <div class="divCel32 bold1bl" style="text-align:right; margin-top:-18px; margin-left:-10px;">Cheked By</div>
                 </div> 
             </div>
             
            <div class="divRow1">
                     <div class="divCel21 bold1bl padding3bl"><span>Corporate Office: SCO-26, 1st Floor, Sec. 15, Part-II, Gurgaon-122001,</span> &nbsp  <span>Ph.No.: 0124-4205305,</span> &nbsp <span>Email ID: corporate@tcgppl.com</span></div>
             </div> 
				 </div>
				 
				 <div ng-show="bill.simBillFormat">
				 
				 		<!-- <style type="text/css">
					 	   @PAGE {
							  size:A4 landscape;
							margin:0.5cm 0.5cm 0.5cm 0.5cm;
						      }
				 	   </style> -->
					<div class="headRowb">
		                <div class="divCell1b"><img src="resources/img/logo.png" width="357" height="106" /></div>
		                <div  class="divCell1b">
		                          <div style="font-size: 16px; font-weight: bold; font-family: Arial;">Care Go Logistics Pvt. Ltd.</div>
		                          <div style="font-size: 12px">Billing Office: {{ selBrh.branchAdd }}</div>
		                          <div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div> 
		                          <div style="font-size: 12px">Email: {{ selBrh.branchEmailId }}</div>
					 </div>
		            </div>
		            <div class="divRowb">
		                  <div class="divCell01b boldb">
		                  	<div ng-show="bill.bill.blType == 'N'">FRIEGHT BILLS</div>
		                  	<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
		                  </div>
		                <div class="divCell02b boldb">Co CIN No. U60231DL2015PTC279265</div>
		                <div class="divCell03b boldb">Service Tax Regidtration No.: AAGCC0032KSD001</div>
		            </div>
		            
		            
		            <div class="divRowb">
		                  <div class="divCell04b boldb">
		                  <div class="divCell04b boldb" style="border:0px; text-align:left;">To, M/s </div>
		                
		                  <div class="divCell04b boldb" style="border:0px; margin-top:-15px; margin-left:100px; text-align:justify;">{{bill.cust.custName}}
		                  <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}} {{bill.cust.custPin}}</div>
		                  
		                  </div>
		               <div class="divCell066b boldb">
		                   <div class="divCell06b boldb">
		                      <div style="margin-top:2px; text-align:center;">BILL NO.: {{bill.bill.blBillNo}}</div><br><div>DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}}</div>
		                   </div>
		               </div>
		            </div>
		            
		            <div class="divRowb">
		                    <div class="divCell25b boldb">We hereby Submit our Bill for the Goods Transported as Under:</div>
			                <div class="divCell26b boldb">OUR PAN NO: AAGCC0032K</div>
			                <div class="divCell26b boldb">No TDS To Be Deducted w.e.f 01/10/2009</div>
		            </div>
		            
		             <div class="divRowb">
		                  <div class="divCell10b bold2b" style="text-align:center;">
		                     <div class="divCell11b bold2b">CONSIGNMENT DETAILS</div>
		                     <div class="divCell12b bold1b">Date</div>
		                     <div class="divCell12b bold1b">LR No.</div>
		                     <div class="divCell12b bold1b">From Station</div>
		                     <div class="divCell12b bold1b">To Station</div>
		                     <div ng-repeat="bilDet in bill.blDetList">
		                     	<div class="divCell12b bold2b">{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</div>
			                     <div class="divCell12b bold2b">{{bilDet.cnmt.cnmtCode}}</div>
			                     <div class="divCell12b bold2b">{{bilDet.frmStn}}</div>
			                     <div class="divCell12b bold2b">{{bilDet.toStn}}</div> 
		                     </div>
		                  </div>
		               
		               
		                <div class="divCell14b bold1b paddingb">Weight MT</div>
		                <div class="divCell13b bold1b paddingb">Rate</div>
		                <div class="divCell14b bold1b paddingb">Freight(A)</div>
		                <div class="divCell14b bold1b paddingb">Detention(B)</div>
		                <div class="divCell14b bold1b paddingb">Bonus(C)</div>
		                <div class="divCell14b bold1b paddingb">Ldng/Unldng(D)</div>
		                <div class="divCell14b bold1b paddingb">Others(E)</div>
		                <div class="divCell14b bold1b paddingb">Total(A+B+C+D+E)</div>
		                
		                 <div class="divCell15b bold2b" style="text-align:center;" ng-repeat="bilDet in bill.blDetList">
		                     <div class="divCell14b bold2b" >
		                     	<div ng-show="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 2}}</div>
		                     	<div ng-show="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 2}}</div>
		                     	<div ng-show="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 2}}</div>
		                     </div>
		                     <div class="divCell13b bold2b" >{{bilDet.billDet.bdRate * 1000 | number : 2}}</div>
		                     <div class="divCell14b bold2b">{{bilDet.billDet.bdFreight | number : 2}}</div>
		                     <div class="divCell14b bold2b" >{{bilDet.billDet.bdDetAmt | number : 2}}</div>
		                     <div class="divCell14b bold2b" >{{bilDet.billDet.bdBonusAmt | number : 2}}</div>
		                     <div class="divCell14b bold2b" >{{bilDet.billDet.bdLoadAmt | number : 2}}/{{bilDet.billDet.bdUnloadAmt | number : 2}}</div>
		                     <div class="divCell14b bold2b" >{{bilDet.billDet.bdOthChgAmt | number : 2}}</div> 
		                     <div class="divCell14b bold2b" >{{bilDet.billDet.bdTotAmt | number : 2}}</div> 
		                  </div>
		               
		            </div>
		       
		       
		       
		         <div class="divRowb">
		                  <div class="divCell16b">
		                     <div class="divCell17b bold2b">IT IS HEREBY DECLARE THAT WE ARE NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES /CAPITAL GOODS</div>
		                     <div class="divCell17b bold2b">WE HAVE NOT AVAILED ANY BENEFIT UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008</div>
		                   </div>
		               
		               
		             
		                
		                 <div class="divCell15b bold2b" style="text-align:center;">
		                     <div class="divCell14b bold1b paddingb" >SUB TOTAL</div>
		                     <div class="divCell14b bold1b paddingb" >{{bill.bill.blSubTot}}</div> 
		                  </div>
		               
		            </div>
		       
		        
		         <div class="divRowb">
		                     <div class="divCell18b bold1b">Bank Name</div>
		                     <div class="divCell12b bold1b">A/c No.</div>
		                     <div class="divCell18b bold1b">IFSC CODE</div>
		                     <div class="divCell05b bold1b">Branch</div>
		                     <div class="divCell19b bold2b">Taxable Service tax= 30%</div>
		                     <div class="divCell14b bold2b" style="text-align:right; padding-right:5px;">{{bill.bill.blTaxableSerTax}}</div>
		                     <div class="divCell14b bold2b">&nbsp</div>
		             </div>
		             
		             <div class="divRowb">
		                     <div class="divCell18b bold1b padding1b">Kotak Mahindra Bank</div>
		                     <div class="divCell12b bold1b padding1b">3011623336</div>
		                     <div class="divCell18b bold1b padding1b">KKBK0000299</div>
		                     <div class="divCell05b bold1b paddingb">61, Old Judicial Complex,<br> Sector-15, Gurgaon 122001</div>
		                     <div class="divCell19b bold2b paddingb">Service Tax= 14%<br> Swachh Bharat Cess= 0.5%</div>
		                     <div class="divCell14b bold2b paddingb" style="text-align:right;"><span>{{bill.bill.blSerTax}}</span><br><span>{{bill.bill.blSwachBhCess}}</span></div>
		                     <div class="divCell14b bold2b padding1b">&nbsp</div>
		             </div>
		       
		         <div class="divRowb">
		                     <div class="divCell20b bold2b paddingb">
		                        Only By A/c Payee Cheque / DD in Favour Of <span class="bold1">Care Go Logistics Pvt. Ltd.</span>, or Electronic Fund transfer to <span class="bold1">above Bank A/C</span>
		                        <br>You are hereby requested to mail us Payment Detail on mail Id <span class="bold1">{{ selBrh.branchEmailId }}</span>
		                     </div>
		                     <div class="divCell20b bold1b padding2b">Pay {{bill.bilAmt}}</div>
		                     <div class="divCell14b bold1b padding2b" style="text-align:right;">Total</div>
		                     <div class="divCell14b bold1b padding2b">{{bill.bill.blFinalTot}}</div>
		             </div>
		           
		             <div class="divRowb">
		                <div class="divCell28b bold2b" style="text-align:center;">
		                     <div class="divCell31b bold1b">Other Bank Details</div>
		                     <div class="divCell27b bold1b">Bank Name</div>
		                     <div class="divCell27b bold1b">A/c No.</div>
		                     <div class="divCell27b bold1b">IFSC CODE</div>
		                     <div class="divCell29b bold1b">Branch</div>
		                     <div class="divCell27b bold1b padding1b">HDFC BANK</div>
		                     <div class="divCell27b bold1b padding1b">13810330000037</div>
		                     <div class="divCell27b bold1b padding1b">HDFC0001381</div>
		                     <div class="divCell29b bold1b paddingb">472-11, OLD RAILWAY ROAD, DHOBI GHAT, NEAR KABIR BHAWAN, GURGAON</div>
		                     <div class="divCell27b bold1b padding1b">ICICI BANK</div>
		                     <div class="divCell27b bold1b padding1b">103105001831</div>
		                     <div class="divCell27b bold1b padding1b">ICIC0001031</div>
		                     <div class="divCell29b bold1b paddingb">SCO - 59, 60, OLD JUDICIAL COMPLEX, SECTOR - 15, GURGAON</div>
		                     <div class="divCell27b bold1b padding1b">STATE BANK OF INDIA</div>
		                     <div class="divCell27b bold1b padding1b">35334334071</div>
		                     <div class="divCell27b bold1b padding1b">SBIN0060414</div>
		                     <div class="divCell29b bold1b paddingb">SCO - 5,6,7 SECTOR - 15, <br>PART - 2, GURGAON</div>
		                 </div>
		                 <div class="divCell30b" style="height:180px;">
		                   <div class="divCell32b bold1" style="text-align:right; margin-top:10px; margin-left:-10px;">For Care Go Logistic Pvt. Ltd</div>
		                    <div class="divCell32b bold1" style="text-align:left; margin-top:125px; margin-left:10px;">Cheked By</div>
		                    <div class="divCell32b bold1b" style="text-align:right; margin-top:-18px; margin-left:-10px;">Cheked By</div>
		                 </div> 
		             </div>
		             
		            <div class="divRowb">
		                     <div class="divCell21b bold1b padding3b"><span>Corporate Office: SCO-26, 1st Floor, Sec. 15, Part-II, Gurgaon-122001,</span> &nbsp  <span>Ph.No.: 0124-4205305,</span> &nbsp <span>Email ID: corporate@tcgppl.com</span></div>
		             </div> 
				 </div>
			         
     		 </div>
			
	
	</div>
</div>	
<title>Stationary Order</title>


<div class="row">
			<div class="col s3 hide-on-med-and-down"> &nbsp; </div>	
			<form class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="StationaryOrderForm" ng-submit="submitStationaryOrder(StationaryOrderForm,st,chCnSe)">
				<div class="row">
      				<div class="input-field col s6">
        				<input type ="date" name="stOdrDt" ng-model="st.stOdrDt" required>
						<label>Stationary Order Date*</label>
      				</div>
      			   <div class="input-field col s6">
        				<input class="validate" type ="text" name="stSupCode" ng-model="st.stSupCode" ng-click="openStationarySupplierDB()" readonly required>
        				<label>Supplier Code</label>	
      			   </div>
      			</div>
      			 
      			 <div class="row">
      			 	<div class="input-field col s4">
      					<input class="validate" type ="number" name="stOdrCnmt" ng-model="st.stOdrCnmt" min="1" ng-minlength="1" ng-maxlength="7">
      					<label>Number Of Books For CNMT</label>
      				</div>	
      			 
      			 	<div class="input-field col s4">
      					<input class="validate" type ="number" name="stOdrChln" ng-model="st.stOdrChln" min="1" ng-minlength="1" ng-maxlength="7" >
      					<label>Number Of Books For Challan</label>
      			 	</div>
      			 
      			 	<div class="input-field col s4">	
      			 		<input class="validate" type ="number" name="stOdrSedr" ng-model="st.stOdrSedr" min="1" ng-minlength="1" ng-maxlength="7" >
      			 		<label>Number Of Books For SEDR</label>
      			 	</div>	
      			</div>
      			
      			<div class="row">
      			 	<div class="input-field col s12 center">
      			 		<input class="btn waves-effect waves-light" type="submit" value="Submit">
      			 	</div>
      			</div>
			</form>
			<div class="col s3"> &nbsp; </div>
		
</div>


<!-- 	<form name="StationaryOrderForm" ng-submit="submitStationaryOrder(StationaryOrderForm,so,chCnSe)">
	
	<table>
			
			<tr>
				<td>Stationary Order Date</td>
				<td><input type ="date" name="stOdrDt" ng-model="so.stOdrDt" required></td>
			</tr>
			
			<tr>
				<td>Number of Books for CNMT</td>
				<td><input type ="number" name="stOdrCnmt" ng-model="so.stOdrCnmt" min="1" ng-minlength="1" ng-minlength="7"></td>
			</tr>
			
			<tr>
				<td>Number of Books for Challan</td>
				<td><input type ="number" name="stOdrChln" ng-model="so.stOdrChln" min="1" ng-minlength="1" ng-minlength="7"></td>
			</tr>
			
			<tr>
				<td>Number of Books for SEDR</td>
				<td><input type ="number" name="stOdrSedr" ng-model="so.stOdrSedr" min="1" ng-minlength="1" ng-minlength="7"></td>
			</tr>
			
			<tr>
				<td>Supplier Code</td>
				<td><input type ="text" name="stOdrSupCode" ng-model="so.stOdrSupCode" ng-click="OpenStationarySupplierDB()" readonly required></td>
			</tr>
			
			 <tr>
				<td><input type="submit" value="Submit"></td>
			</tr>
	
	</table>
	</form> -->
	
	 <div id="cnmt" ng-hide="show">
				
				<table>
				
					<tr>
						<td>Start Number</td>
						<td><input type ="text" name="chCnSeStartNo" ng-model="chCnSe.chCnSeStartNo"></td>
					</tr>
					
					<tr>
						<td>End Number</td>
						<td><input type ="text" name="chCnSeEndNo" ng-model="chCnSe.chCnSeEndNo"></td>
					</tr>
					
					<tr>
						<td>Number Of Books</td>
						<td><input type ="text" name="chCnSeNob" ng-model="chCnSe.chCnSeNob"></td>
					</tr>
					
					<tr>
						<td>Status</td>
						<td><input type ="text" name="chCnSeStatus" ng-model="chCnSe.chCnSeStatus"></td>
					</tr>
					
				</table>
	</div>
	
	
	<div id ="StationarySupplierDB" ng-hide = "StationarySupplierFlag">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Supplier Code</th>
			</tr>
 	 
		  
		  <tr ng-repeat="supplier in supplierCodeList">
		 	  <td><input type="radio"  name="supplier" id="supplier"  value="{{ supplier }}" ng-model="stnCode3" ng-click="saveSupplierCode(supplier)"></td>
             <td>{{ supplier.stSupCode }}</td>
              
          </tr>
         </table>
         
	</div>


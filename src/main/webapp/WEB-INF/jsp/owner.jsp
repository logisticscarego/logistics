
<div class="row">
<div class="col s12 center">
        			<h6> Vehicle Detail</h6>	
      			</div></br>
<form class="col s12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" name ="vehicleOwnerForm" ng-submit="saveVehicleOwner(vehicleOwnerForm,vv,own,add)">
		
      			
		<div class="row">
      			<div class="input-field col s3">
        			<input id="vvRcNoId" type ="text" name ="vvRcNoName" ng-change="vehicleDBFlag='false'"  ng-model="vv.vvRcNo" required >
        			<label>RC No./Lorry No.:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="vvEngineNoId" type ="text" name ="vvEngineNoName" ng-change="vehicleDBFlag='false'"  ng-model="vv.vvEngineNo" required >
        			<label>Engine No.</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="vvChassisNoId" type ="text" name ="vvChassisNoName" ng-change="vehicleDBFlag='false'"  ng-model="vv.vvChassisNo" required >
        			<label>Chasis No.</label>	
      			</div>
      			
      			<div class="col s3 center" ng-hide="vehicleDBFlag">
      			 		<input type="button" value="Verify Vehicle" ng-click="verifyVehicle()">
      			 	</div>
      			
      			</div>
      		<div ng-show="vehicleDBFlag">	
      			<div class="row">
      			<div class="input-field col s3">
        			<input id="vvTypeId" type ="text" name ="vvTypeName"  ng-model="vtName" ng-click="openVehicleTypeDB()" readonly required >
        			<label>Vehicle Type:</label>
        			
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="vvRcIssueDtId" type ="date" name ="vvRcIssueDtName"  ng-model="vv.vvRcIssueDt" ng-blur="rcIssueDt()" required>
        			<label>Registration Date.</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="vvRcValidDtId" type ="date" name ="vvRcValidDtName"  ng-model="vv.vvRcValidDt" ng-blur="rcValidDt()">
        			<label>Registration Valid Date.</label>	
      			</div>
      			</div>
      			
      			<div class="row">
      			<div class="input-field col s3">
        			<input id="vvPerNoId" type ="text" name ="vvPerNoName"  ng-model="vv.vvPerNo"  >
        			<label>Permit No.:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="vvPerIssueDtId" type ="date" name ="vvPerIssueDtName"  ng-model="vv.vvPerIssueDt" ng-blur="perIssueDt()" >
        			<label>Permit Issue Date</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="vvPerValidDtId" type ="date" name ="vvPerValidDtName"  ng-model="vv.vvPerValidDt" ng-blur="perValidDt()" >
        			<label>Permit Valid Date</label>	
      			</div>
      			</div>
      			<div class="row">
      			<div class="input-field col s3">
        			<input id="vvPolicyNoId" type ="text" name ="vvPolicyNoName"  ng-model="vv.vvPolicyNo"  >
        			<label>Insurance No.:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="policyIssueDateId" type ="date" name ="policyIssueDateName"  ng-model="vv.vvPolicyIssueDt" ng-blur="policyIssueDt()" >
        			<label>Insurance Issue Date</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="policyValidDateId" type ="date" name ="policyValidDateName"  ng-model="vv.vvPolicyValidDt" ng-blur="policyValidDt()" >
        			<label>Insurance Valid Date</label>	
      			</div>
      			</div>
      			<div class="row">
      			
      			<div class="input-field col s3">
        			<input id="vvHypothecatedId" type ="text" name ="vvHypothecatedName"  ng-model="vv.vvHypoTo" ng-click="openHypoDB()" readonly  >
        			<label>Hypothecated To</label>	
        		
      			</div>
      			<div class="input-field col s3">
        			<input id="vvModelNoId" type ="text" name ="vvModelNoName"  ng-model="vv.vvModelNo"  >
        			<label>Model No.</label>	
      			</div>
      			<div class="input-field col s3">
        			<input id="vvModelId" type ="number" name ="vvModelName"  ng-model="vv.vvModel"  >
        			<label>Model Year.</label>	
      			</div>
      			</div>
      			
      			<div class="col s12 center">
        			<h6> Owner Detail</h6>	
      			</div>
      			
 <div class="row">
		<div class="row">
      			<div class="input-field col s3">
        			<input id="ownPanNoId" type ="text" name ="ownPanNoName" ng-pattern="/^[A-Z]{3}[C,P,H,F,A,T,B,L,J,G][A-Z]\d{4}[A-Z]$/" ng-model="own.ownPanNo" ng-change="ownerDetailDBFlag='false'"  required>
        			<label>Pan No.</label>	
      			</div>
      			
      				<div class="col s3 center" ng-hide="ownerDetailDBFlag">
      			 		<input type="button" value="VerifyPanNo" ng-click="verifyPan(own.ownPanNo)">
      			 	</div>
      		</div>
</div>
      			
      <div ng-show="ownerDetailDBFlag">			
		<div class="row">
		
				<div class="input-field col s3">
        			<input id="ownBcodeNameId" type ="text" name ="ownBcodeName"  ng-model="own.bCode" ng-click="getBranch()" readonly required>
        			<label>Branch</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="ownPanNameId" type ="text" name ="ownPanName"  ng-model="own.ownPanName"  ng-readonly="" required>
        			<label>Name on PAN:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="ownFatherNameId" type ="text" name ="ownFatherName"  ng-model="own.ownFather"  >
        			<label>Father Name</label>	
      			</div>
      			</div>
      			
      			<div class="row">
      			<div class="input-field col s3">
        			<input id="ownNameId" type ="text" name ="ownName"  ng-model="own.ownName"  required>
        			<label>Name On RC/Owner Name</label>
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="phNoId" type ="text" name ="phNoName"  ng-model="phNo"  pattern="[0-9]{10}" required>
        			<label>Mobile No.:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="ownEmailId" type ="text" name ="ownEmailIdName"  ng-model="own.ownEmailId" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$">
        			<label>Email Address:</label>	
      			</div>
      			</div>
      			
      			<div class="row">
      			<div class="input-field col s3">
        			<input id="completeAddId" type ="text" name ="completeAddName"  ng-model="add.completeAdd" required >
        			<label>Building No./Plot No.:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="addPinId" type ="text" name ="addPinName"  ng-model="add.addPin" ng-keyup="getStnByPin()" minlength="6" maxlength="6"  required>
        			<label>Pin Code:</label>
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="addStateId" type ="text" name ="addStateName"  ng-model="add.addState" ng-click="getState()" readonly required>
        			<label>State</label>	
      			</div>
      			</div>
      			<div class="row">
      			<div class="input-field col s3">
        			<input id="districtId" type ="text" name ="districtName"  ng-model="add.addDist" ng-click="getState()" readonly required >
        			<label>District:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="addCityId" type ="text" name ="addCityName"  ng-model="add.addCity" ng-click="getState()" readonly required >
        			<label>City/Town:</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="locationId" type ="text" name ="locationName"  ng-model="add.addPost" ng-click="getState()" readonly reauired >
        			<label>Post Office:</label>
      			</div>
      			</div>
      			<div class="row">
      			<div class="input-field col s3">
        			<input id="ownAccntNoId" type ="text" name ="ownAccntNoName"  ng-model="own.ownAccntNo" >
        			<label>Account No:</label>	
      			</div>
      			
      			
      			<div class="input-field col s3">
        			<input id="accountHldrNameId" type ="text" name ="accountHldrName"  ng-model="own.ownAcntHldrName"  >
        			<label>Account Holder Name</label>	
      			</div>
      			
      			<div class="input-field col s3">
        			<input id="ownIfscId" type ="text" name ="ownIfscName"  ng-model="own.ownIfsc" pattern="^[A-Z]{4}0[A-Z0-9]{6}$"  >
        			<label>IFSC Code</label>
      			</div>
      			<div class="input-field col s3">
        			<input id="ownBnkBranchId" type ="text" name ="ownBnkBranchName"  ng-model="own.ownBnkBranch"  >
        			<label>Bank Name</label>
      			</div>
      			</div>
      			
      		<div class="row">
      		
      			<div class="col s3 center">
      			 		<input type="button" value="Upload Documents" id="upldDocId" ng-click="openUploadDB()">
      			 	</div>
      			 	
      				<div class="col s3 center">
      			 		<input type="submit" value="Save" id="saveId">
      			 	</div>
      		</div>
      		</div>
      		</div>
	</form>		
        		
</div>    			
      	<div class="row" id="docUploadDBId" ng-show="docUpldFlag">
<form class="col s12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" name ="docUploadForm" ng-submit="uploadImage(docUploadForm)">
      			
      			<div class="row" ng-show="uploadRCFlag">
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" name="" file-model="rcImage" ng-required="rcRqrFlag">
						<label>Choose File for RC</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadRCImage(rcImage)">Upload</a>
					</div>
				</div>
      			
      			<div class="row" ng-show="uploadPANFlag">
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" file-model="panImage">
						<label>Choose File for PAN card:</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadPanImage(panImage)">Upload</a>
					</div>
				</div>
				
				<div class="row" ng-show="uploadPSFlag">
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" file-model="psImage">
						<label>Choose File for Permit Slip</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadPSImage(psImage)">Upload</a>
					</div>
				</div>
				
				<div class="row" ng-show="uploadInsrncFlag">
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" file-model="insrncImage">
						<label>Choose File for Insurance Doc</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadInsrncImage(insrncImage)">Upload</a>
					</div>
				</div>
				
				<div class="row" ng-show="uploadCCFlag">
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" file-model="ccImage">
						<label>Choose File for Cancelled cheque</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadCCImage(ccImage)">Upload</a>
					</div>
				</div>
				
				<div class="row" ng-show="uploadDecFlag">
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" file-model="decImage">
						<label>Choose File for Declaration</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadDecImage(decImage)">Upload</a>
					</div>
				</div>
				<div class="row">
      				<div class="col s3 center">
      			 		<input type="submit" value="Submit">
      			 	</div>
      		</div>
</form>
</div>
<div id ="existOwnerId" ng-hide = "existOwnerDB">
	
	<input type="text" name="filterExistOwner" ng-model="filterExistOwner" placeholder="Search">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Owner Code</th>
				<th>Owner Name</th>
				<th>Owner Pan Name</th>
			</tr>
 	  
		  <tr ng-repeat="own in ownList  | filter:filterExistOwner">
		 	  <td><input type="radio"  name="ownerCode" id="ownerCode" class="ownerCode"  value="{{ own }}" ng-model="ownCode" ng-click="editExistOwner(own)"></td>
              <td>{{ own.ownCode }}</td>
              <td>{{ own.ownName }}</td>
              <td>{{ own.ownPanName}}</td>
          </tr>
         </table>       
	</div>



<div id ="VehicleTypeDB" ng-hide = "VehicleTypeFlag">
	
	<input type="text" name="filterVehicleType" ng-model="filterVehicleType" placeholder="Search">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				<th>Vehicle Type</th>
				<th>Service Type</th>
			</tr>
 	  
		  <tr ng-repeat="vt in vtList  | filter:filterVehicleType">
		 	  <td><input type="radio"  name="vtCode" id="vtCode" class="vtCode"  value="{{ vt }}" ng-model="vtCode" ng-click="saveVehicleType(vt)"></td>
              <td>{{ vt.vtCode }}</td>
              <td>{{ vt.vtVehicleType }}</td>
              <td>{{vt.vtServiceType}}</td>
          </tr>
         </table>       
	</div>

<div id ="hypoDB" ng-hide = "hypoFlag">
	
	<input type="text" name="filterHypo" ng-model="filterHypo" placeholder="Search">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Name</th>
			</tr>
 	  
		  <tr ng-repeat="hp in hypoList  | filter:filterHypo">
		 	  <td><input type="radio"  name="hpCode" id="hpCode" class="hpCode"  value="{{ hp }}" ng-model="hp" ng-click="saveHypo(hp)"></td>
              <td>{{ hp }}</td>
          </tr>
         </table>       
	</div>





	<div id="stateDB" ng-hide="stateDBFlag">

		<input type="text" name="filterStateCode"
			ng-model="filterStateCode.stateName" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>State GstCode</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="state in stateList | filter:filterStateCode">
				<td><input type="radio" name="stateCode" id="stateId"
					value="{{ state.stateCode }}" ng-model="stateCode"
					ng-click="saveStateCode(state)"></td>
				<td>{{ state.stateGST }}</td>
				<td>{{ state.stateName }}</td>
			</tr>
		</table>

	</div>
	
	
		<div id="distDB" ng-hide="distDBFlag">

		<input type="text" name="filterDistCode"
			ng-model="filterDistCode" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="dist in distList | filter:filterDistCode">
				<td><input type="radio" name="distName" id="distId"
					value="{{ dist }}" ng-model="distName"
					ng-click="saveDist(dist)"></td>
				<td>{{ dist }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
	
	
	
		<div id="cityDB" ng-hide="cityDBFlag">

		<input type="text" name="filterCityCode"
			ng-model="filterCityCode" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>City Name</th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="city in cityList | filter:filterCityCode">
				<td><input type="radio" name="cityCode" id="cityId"
					value="{{ city }}" ng-model="cityName"
					ng-click="saveCity(city)"></td>
				<td>{{ city }}</td>
				<td>{{ distName }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>
	
	
	<div id="stnDB" ng-hide="stnDBFlag">

		<input type="text" name="filterStnCode"
			ng-model="filterStnName" placeholder="Search by Name">

		<table>
			<tr>
				<th></th>
				<th>Pin Code</th>
				<th>Station Name</th>
				<th>City Name</th>
				<th>District Name</th>
				<th>State Name</th>
			</tr>
		</table>
		<table>

			<tr ng-repeat="stn in stnList | filter:filterStnName">
				<td><input type="radio" name="stnCode" id="stnId"
					value="{{ stn }}" ng-model="stnName"
					ng-click="saveStn(stn)"></td>
				<td>{{ stn.pinCode }}</td>
				<td>{{ stn.stationName }}</td>
				<td>{{ stn.city }}</td>
				<td>{{ stn.district }}</td>
				<td>{{ stateName }}</td>
			</tr>
		</table>

	</div>

<div id="branchCodeDB" ng-hide="BranchCodeDBFlag">

		<input type="text" name="filterBranchCode" ng-model="filterBranchCode"
			placeholder="Search by Code">

		<table>
			<tr>
				<th></th>

				<th>Branch FaCode</th>

				<th>Branch Name</th>

				<th>Branch Pin</th>
			</tr>
			<tr ng-repeat="branch in branchList | filter:filterBranchCode">
				<td><input type="radio" name="branchCode" id="branchId"
					value="{{ branch.branchCode }}" ng-model="branchCodeTemp"
					ng-click="saveBranchCode(branch)"></td>
				<td>{{ branch.branchFaCode }}</td>
				<td>{{ branch.branchName }}</td>
				<td>{{ branch.branchPin }}</td>
			</tr>
		</table>
	</div>


</div>
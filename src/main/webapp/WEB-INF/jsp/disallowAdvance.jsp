<div ng-show="adminLogin || superAdminLogin">

<title>Disallow Advance></title>
<div class="row">
   <div class="input-field col s6">
      <select name="advncType"  ng-model="advncType" required ng-change="checkAdvanceType(advncType)">
                    <option value="customer">Customer</option>
                    <option value="consignor">Consignor</option>  
                    <option value="consignee">Consignee</option>  
                    <option value="branch">Branch</option>  
                    <option value="fromStn">From station</option>
                    <option value="toStn">To station</option>   
     </select>
     <label for="advncType">Select Advance Type</label>
</div>
</div>

<div class="container" ng-hide="viewCustRightsFlag">
 <input type="text" name="filterCustRights" ng-model="filterCustRights.crhCustCode" placeholder="Search">
	<table class="table-hover table-bordered table-condensed">
 	 		<tr>
 	 		
 	 			<th></th>	
 	 		    	 		    		
				<th>Customer Right Code</th>
				
				<th>Customer valid Date</th>
							
				<th>Creation TS</th>				
			</tr>
			
			<tr ng-repeat="cust in custRightList | filter:filterCustRights">
               	<td><input type="checkbox" value="{{ cust.crhId }}" ng-checked="selectCust.indexOf(cust.crhId) > -1" ng-click="toggleSelectCust(cust.crhId)" /></td>
                <td>{{ cust.crhCustCode }}</td>
               	<td>{{ cust.crhAdvValidDt }}</td>
              	<td>{{ cust.creationTS }}</td>
            </tr>
	</table>
	<table>
	   <tr>
          <td class="center"><input type="button" value="Submit" ng-click="submitCustIsAllowNo()"></td>
       </tr>
	</table>
</div>	

<div class="container" ng-hide="viewBranchRightsFlag">
 <input type="text" name="filterBrRights" ng-model="filterBrRights.brhBranchCode" placeholder="Search">
	<table class="table-hover table-bordered table-condensed">
 	 		<tr>
 	 		
 	 			<th></th>	
 	 		    	 		    		
				<th>Branch Right Code</th>
				
				<th>Valid Date</th>
							
				<th>Creation TS</th>				
			</tr>
			
			<tr ng-repeat="br in branchRightList | filter:filterBrRights">
               	<td><input type="checkbox" value="{{br.brhId }}" ng-checked="selectBranch.indexOf(br.brhId) > -1" ng-click="toggleSelectBranch(br.brhId)" /></td>
                <td>{{ br.brhBranchCode }}</td>
               	<td>{{ br.brhAdvValidDt }}</td>
              	<td>{{ br.creationTS }}</td>
            </tr>
	</table>
	<table>
	   <tr>
          <td class="center"><input type="button" value="Submit" ng-click="submitBranchIsAllowNo()"></td>
       </tr>
	</table>
</div>	

<div class="container" ng-hide="viewStationRightsFlag" >
 <input type="text" name="filterStnRights" ng-model="filterStnRights.strhStCode" placeholder="Search">
	<table class="table-hover table-bordered table-condensed">
 	 		<tr>
 	 		
 	 			<th></th>	
 	 		    	 		    		
				<th>Station Right Code</th>
				
				<th>Valid Date</th>
							
				<th>Creation TS</th>				
			</tr>
			
			<tr ng-repeat="stn in stnRightList | filter:filterStnRights">
               	<td><input type="checkbox" value="{{stn.strhId }}" ng-checked="selectStation.indexOf(stn.strhId) > -1" ng-click="toggleSelectStation(stn.strhId)" /></td>
                <td>{{ stn.strhStCode }}</td>
               	<td>{{ stn.strhAdvValidDt }}</td>
              	<td>{{ stn.creationTS }}</td>
            </tr>
	</table>
	<table>
	   <tr>
          <td class="center"><input type="button" value="Submit" ng-click="submitStationIsAllowNo()"></td>
       </tr>
	</table>
</div>	
</div>

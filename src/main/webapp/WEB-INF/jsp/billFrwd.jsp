<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="billFrwdForm" ng-submit=billFrwdSubmit(billFrwdForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
					<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="custId" name ="custName" ng-model="customer.custName" ng-click="selectCust()" ng-required="true" readonly>
		       			<label for="code">Customer Name</label>	
		       		</div>
		       		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="billId" name ="billName" ng-model="bill.blBillNo" ng-required="true" ng-click="selBillNo()" readonly>
		       			<label for="code">Bill No</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="totAmtId" name ="totAmtName" ng-model="totAmt" ng-required="true" readonly>
		       			<label for="code">Bill Forwarding Amt</label>	
		       		</div>
		    </div>
		    
		    <div ng-show="billFrwd.billList.length > 0">
		    	<div class="row">
		    		<table class="tblrow">
					<caption class="coltag tblrow">Bill Forwarding</caption>
						<tr class="rowclr">
							<th class="colclr">Bill No</th>
							<th class="colclr">Bill Date</th>
							<th class="colclr">Amount</th>
							<th class="colclr">Action</th>
						</tr>
						<tr class="tbl" ng-repeat="bil in billFrwd.billList">
							<td class="rowcel">{{bil.blBillNo}}</td>
							<td class="rowcel">{{bil.blBillDt | date : format : timezone}}</td>
							<td class="rowcel">{{bil.blFinalTot}}</td>
							<td class="rowcel">
				            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="removeBill($index)" >
									<i class="mdi-action-delete white-text"></i>
								</a>
							</td>
						</tr>
					</table>	
		    	</div>
		    </div>
		    
		    <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" id="saveId" value="submit" >
	       		</div>
	        </div>
		    
		</form>
	</div>
	
	
	<div id ="selCustId" ng-hide="selCustFlag">
		  <input type="text" name="filterCust" ng-model="filterCust" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Customer Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cust in custList | filter:filterCust">
		 	  <td><input type="radio"  name="cust"   value="{{ cust }}" ng-model="custCode" ng-click="saveCustomer(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="selBillId" ng-hide="selBillFlag">
		  <input type="text" name="filterBill" ng-model="filterBill" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Bill No</th>
 	  	  	  <th>Amount</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="blng in blList | filter:filterBill">
		 	  <td><input type="radio"  name="blng"   value="{{ blng }}" ng-model="blngCode" ng-click="saveBill(blng)"></td>
              <td>{{blng.blBillNo}}</td>
              <td>{{blng.blFinalTot}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id="disBfNoId" ng-hide="displayBfNoFlag">
		<table>
			<tr>
				<td>Bill Forwarding No :</td>
				<td>{{bfNo}}</td>
			</tr>
		</table>
	</div>
	
</div>		    
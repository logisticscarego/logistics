<div ng-show="operatorLogin || superAdminLogin">
	<title>Cancel Challan</title>
	
	<div class="row">
	<form name="chlnDtailForm" ng-submit="cancelChallan()" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);" >
	
	<div class="row">
				<div class="input-field col s4">
					<input type="text" name="chlnCode" ng-model="chlnCode" ng-required=true>
					 <label>Challan No.</label>
				</div>
				<div class="input-field col s4">
					<input type="text" name="lorryNo" ng-model="lorryNo" ng-required=true>
					<label>Lorry No.</label>
				</div>
				<div class="input-field col s4">
					<input type="date" name="chlnDt" ng-model="chlnDt" ng-required=true>
					<label>Date</label>
				</div>
			</div>
	
	 <div class="row">
				<div class="col s12 center"> 
					<input class="btn" type="submit" value="Cancel">
				</div>
			 </div>
	</form>
	
	</div>
</div>
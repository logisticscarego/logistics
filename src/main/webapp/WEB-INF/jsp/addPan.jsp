<div ng-show="operatorLogin || superAdminLogin">
	
	<div class="row">
		<form name="panForm" ng-submit=panFormSubmit(panForm) class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			
			<div class="row">

				<div class="input-field col s5">
					<input type="text" name="ownerName" id="ownerId" ng-model="owner.ownName" ng-keyup="getOwnNameCodeId()" disabled="disabled"	> 
					<label>Select Owner</label>
				</div>
				
				<div class="col s1">
					<input type="radio" id="ownerRBId" name="btn" ng-click="ownerRB()" />
				</div>
				
				<div class="input-field col s5">
					<input type="text" name="brokerName" id="brokerId" ng-model="broker.brkName" ng-keyup="getBrkNameCodeId()" disabled="disabled" > 
					<label>Select Broker</label>
				</div>
				
				<div class="col s1">
					<input type="radio" id="brokerRBId" name="btn" ng-click="brokerRB()" />
				</div>
			
			</div>
			
			<div class="row" >
				<div class="col s3 input-field">
					<input class="validate" type="text" id="panNameId" name="panName" ng-model="panService.panName" ng-minlength="3" ng-maxlength="40" ng-required="true"> 
					<label for="code">Pan Name</label>
				</div>

				<div class="col s3 input-field">
					<input class="validate" type="text" id="panNoId" name="panNoName" ng-model="panService.panNo" ng-minlength="10" ng-maxlength="10" ng-required="true" ng-pattern="/^[A-Z]{3}[C,P,H,F,A,T,B,L,J,G][A-Z]\d{4}[A-Z]$/" > 
					<label for="code">Pan No.</label>
				</div>
				
				<div class="col s3 input-field">
					<input class="validate" type="date" id="panDOBId" name="panDOBName" ng-model="panService.panDOB" > 
					<label for="code">Pan DOB</label>
				</div>
				
				<div class="col s3 input-field">
					<input class="validate" type="Date" id="panDtId" name="panDtName" ng-model="panService.panDt" > 
					<label for="code">Pan Date</label>
				</div>
				
			</div>
			
			<div class="row">
				<div class="col s3 input-field">
					<input class="teal white-text btn" type="file" file-model="panImg" accept="application/pdf" > 
				</div>
				
				<div class="col s3 input-field">
					<a class="btn waves-effect waves-light white-text col s12" type="button" ng-click="uploadPanImg()">Upload Pan Card Image</a> 
				</div>
				
				<div class="col s3 input-field">
					<input class="teal white-text btn" type="file" file-model="decImg" accept="application/pdf"> 
				</div>
				
				<div class="col s3 input-field">
					<a class="btn waves-effect waves-light white-text col s12" type="button" ng-click="uploadDecImg()">Upload Dec Image</a> 
				</div>
			</div>
			
			<div class="row">
      			 <div class="col s12 center">
      			 	<input type="submit" id="panFormBtnId" value="Submit">
      			 </div>
      		</div>
			
		</form>
	</div>
	
	<div id ="ownerDB" ng-hide="ownerDBFlag">
		<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> PAN No. </th>
 	  	  	  <th> Code </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="owner in ownerList | filter:filterTextbox">
		 	  <td><input type="radio"  name="owner" value="{{ owner }}" ng-focus="saveOwner(owner)"></td>
              <td>{{owner.ownName}}</td>
              <td>{{owner.ownPanNo}}</td>
              <td>{{owner.ownCode}}</td>
          </tr>
          
          <tr>
          	<td colspan="3">
	          	<div class="col s12 center">
	          		<input type="button" value="Submit" ng-click="closeOwnDb()">
	          	</div>
          	</td>
          </tr>
      	  
      </table> 
	</div>
	
	<div id ="brokerDB" ng-hide="brokerDBFlag">
		<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> PAN No. </th>
 	  	  	  <th> Code </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="broker in brokerList | filter:filterTextbox">
		 	  <td><input type="radio"  name="broker" value="{{ broker }}" ng-focus="saveBroker(broker)"></td>
              <td>{{broker.brkName}}</td>
              <td>{{broker.brkPanNo}}</td>
              <td>{{broker.brkCode}}</td>
          </tr>
          
          <tr>
          	<td colspan="3">
	          	<div class="col s12 center">
	          		<input type="button" value="Submit" ng-click="closeBrkDb()">
	          	</div>
          	</td>
          </tr>
      	  
      </table> 
	</div>
		    
</div>

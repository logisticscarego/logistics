<div ng-show="adminLogin || superAdminLogin">

<title>Display Employee</title>
<div class="container">
		<div class="row">
			<div class="input-field col s6">
			<input class="prefix" type="radio" id= "empModalRadio" name="empRadio" ng-model="empModalRadio" ng-click="enableEmpCodeTextbox()">
			<input type ="text" name="empTextList" id="empTextList" ng-model="empCodeModalList" disabled="disabled" ng-click="OpenEmpCodeDB()" readonly>
			</div>	
			<div class="col s6"><input type ="text" id="creationTSId" name="creationTSName" ng-model="creationTSModel" disabled="disabled" ng-click="OpenCreationTSDB()" readonly></div>
		</div>
			
		<div class="row">
			<div class="col s2"><input type="radio" id="empAutoRadio" name="empRadio" ng-model="empAutoRadio" ng-click="enableAutoEmpCodeTextBox()"></div>
			<div class="col s8"><input type = "text" name="empAutoTextList" id="empAutoTextList"  ng-model="empCodeAutoList" disabled="disabled" ng-keyup="getEmpCodeList()"></div>
			<div class="col s2"><input class="col s12" type ="button" id="okId" value = "OK" ng-click="empDataByEmpCodeTemp(empCodeAutoList)"></div>
		</div> 
		
		<div class="row">
			<div class="col s12"><input type ="button" id="bkToList" value = "Back To list" ng-click="backToList()"></div>
		</div>
</div>

<div class="container" id="employeeTableDB" ng-hide="employeeTableDBFlag">
 <input type="text" name="filterEmp" ng-model="filterEmp.empCodeTemp" placeholder="Search by Employee Code Temp">
	<table class="table-hover table-bordered table-condensed">
 	 		<tr>
 	 		
 	 			<th></th>	
 	 		    	 		    		
				<th>Employee Code</th>
				
				<th>Employee Name</th>
				
				<th>Employee Code Temp</th>
			
				<th>Operator Id</th>
				
				<th>Employee Designation</th>
				
				<th>Creation TS</th>				
			</tr>
			
			<tr ng-repeat="employ in employeeList | filter:filterEmp">
               	<td><input type="checkbox" value="{{ employ.empCode }}" ng-checked="selection.indexOf(employ.empCode) > -1" ng-click="toggleSelection(employ.empCode)" /></td>
                <td>{{ employ.empCode }}</td>
               	<td>{{ employ.empName }}</td>
               	<td>{{ employ.empCodeTemp }}</td>
               	<td>{{ employ.userCode }}</td>
              	<td>{{ employ.empDesignation }}</td>
              	<td>{{ employ.creationTS }}</td>
            </tr>
	</table>
	<table>
	   <tr>
          <td class="center"><input type="button" value="Submit" ng-click="verifyEmployee()"></td>
       </tr>
	</table>
</div>	
	
<div id ="empCodeDB" ng-hide = "empCodeDBFlag">
        <input type="text" name="filterEmpCode" ng-model="filterEmpCode.empCodeTemp" placeholder="Search by employee code temp">
 	   <table>
 	   		<tr>
 	   			<th></th>
 	   			
 	   			<th>Employee Code</th>
 	   			
 	   			<th>Employee Name</th>
 	   			
 	   			<th>Employee Code Temp</th>
 	   		</tr>
 	   		<tr ng-repeat="emp in empCodeList | filter:filterEmpCode">
		 	  	<td><input type="radio"  name="empName"  class="empCls"  value="{{ emp }}" ng-model="empCode" ng-click="saveEmployeeCode(emp)"></td>
              	<td>{{ emp.empCode }}</td>
              	<td>{{ emp.empName }}</td>
              	<td>{{ emp.empCodeTemp }}</td>
          	</tr>
         </table>
 	     
</div>	

<div id ="creationTSDB" ng-hide = "creationTSDBFlag">
        <input type="text" name="filterCreationTS" ng-model="filterCreationTS" placeholder="Search">
 	   <table>
 	   		<tr ng-repeat="emp in empOldList | filter:filterCreationTS">
		 	  	<td><input type="radio"  name="empName"  class="empCls"  value="{{ emp.creationTS }}" ng-model="$parent.creationTS" ng-click="saveCreationTS(emp.creationTS)"></td>
              	<td>{{ emp.creationTS | date : 'medium'}}</td>
 	        </tr>
         </table>
 	     
</div>	
	
<div id="employeeDetailsDB" ng-hide = "editEmployeeDetailsDBFlag" >		
	<form name="EmployeeForm" ng-submit="editEmployeeSubmit(EmployeeForm,employee)">
			<div class="container">
	            <!-- <h3>Here's the detail of your Employee:</h3> -->
	            <div class="row">
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" name ="branchCode" ng-model="employee.branchCode" value={{employee.branchCode}} required readonly ng-click="openBranchCodeDB()" >
	                <label>Branch Code:</label>
	                <input type="hidden" ng-model="employee.isInNCR" > 
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empName" name ="empName" ng-model="employee.empName" value={{employee.empName}} ng-required="true" ng-minlength="7" ng-maxlength="122">
	                <label>Employee Name:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" name ="empCode" ng-model="employee.empCode" value={{employee.empCode}} readonly required>
	                <label>Employee Code:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text"  id="empAdd" name="empAdd" ng-model="employee.empAdd" value={{employee.empAdd}} ng-required="true" ng-minlength="3" ng-maxlength="255">
	                <label>Employee Address:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empCity" name ="empCity" ng-model="employee.empCity" value={{employee.empCity}} ng-required="true" ng-minlength="3" ng-maxlength="40">
	                <label>Employee City:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empState" name ="empState" ng-model="employee.empState" value={{employee.empState}} ng-required="true" ng-minlength="3" ng-maxlength="40">
	                <label>Employee State:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empPin" name ="empPin" ng-model="employee.empPin" value={{employee.empPin}} ng-required="true" ng-minlength="6" ng-maxlength="6" ng-blur="chkPresentAddrDB()">
	                <label>Employee Pin:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="date" name ="empDOB" ng-model="employee.empDOB" value={{employee.empDOB}} ng-required="true">
	                <label>Employee Date Of Birth:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="date" name ="empDOAppointment" ng-model="employee.empDOAppointment" value={{employee.empDOAppointment}} ng-required="true">
	                <label>Employee Date Of Appointment:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empDesignation" name ="empDesignation" ng-model="employee.empDesignation" value={{employee.empDesignation}}  ng-keyup="getEmpDesigList(employee)"  ng-blur="fillEmpDesigVal()" ng-required="true" ng-minlength="2" ng-maxlength="40">
	                <label>Employee Designation:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="number" id="empBasic" name ="empBasic" ng-model="employee.empBasic" value={{employee.empBasic}} ng-keyup="calculateOnBasic()" ng-required="true" step="0.01" min="0.00" >
	                <label>Employee Basic:</label>	                
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" name ="empHRA" ng-model="employee.empHRA" value={{employee.empHRA}} readonly>
	                <label>Employee HRA:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="number" id="empOtherAllowance" name ="empOtherAllowance" ng-model="employee.empOtherAllowance" value={{employee.empOtherAllowance}} ng-keyup="calculateOnOthers(employee.empOtherAllowance)" ng-required="true" step="0.01" min="0.00">
	                <label>Employee Other Allowance:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	            	<input type ="text"  name ="empGross" ng-model="employee.empGross" value={{employee.empGross}} readonly>
        				<label>Gross Salary</label>
        		</div>
        		<div class="input-field col s12 m4 l3">
	            	<input type ="text"  name ="netSalary" ng-model="employee.netSalary" value={{employee.netSalary}} readonly>
        				<label>Net Salary</label>
        		</div>			
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empPFNo" name ="empPFNo" ng-model="employee.empPFNo" value={{employee.empPFNo}} ng-required="true" ng-minlength="8" ng-maxlength="18">
	                <label>Employee PF No:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empESINo" name ="empESINo" ng-model="employee.empESINo" value={{employee.empESINo}} ng-minlength="3" ng-maxlength="40">
	                <label>Employee ESI No:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empESIDispensary" name ="empESIDispensary" ng-model="employee.empESIDispensary" value={{employee.empESIDispensary}} ng-minlength="3" ng-maxlength="40">
	                <label>Employee ESI Dispensary:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empNominee" name ="empNominee" ng-model="employee.empNominee" value={{employee.empNominee}} ng-required="true" ng-minlength="7" ng-maxlength="81">
	                <label>Employee Nominee:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empEduQuali" name ="empEduQuali" ng-model="employee.empEduQuali" value={{employee.empEduQuali}} ng-required="true" ng-minlength="2" ng-maxlength="255">
	                <label>Employee Educational Qualification:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" name ="empPF" ng-model="employee.empPF" value={{employee.empPF}} readonly>
	                <label>Employee PF:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empESI" name ="empESI" ng-model="employee.empESI" value={{employee.empESI}} readonly>
	                <label>Employee ESI:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="number" id="empLoanBal" name ="empLoanBal" ng-model="employee.empLoanBal" value={{employee.empLoanBal}}  ng-required="true" step="0.01" min="0.00">
	                <label>Employee Loan Balance:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="number" id="empLoanPayment" name ="empLoanPayment" ng-model="employee.empLoanPayment" value={{employee.empLoanPayment}}  ng-required="true" step="0.01" min="0.00">
	                <label>Employee Loan Payment:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empPresentAdd" name ="empPresentAdd" ng-model="employee.empPresentAdd" value={{employee.empPresentAdd}} ng-required="true" ng-minlength="3" ng-maxlength="255">
	                <label>Employee Present Address:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empPresentCity" name ="empPresentCity" ng-model="employee.empPresentCity" value={{employee.empPresentCity}} ng-required="true" ng-minlength="3" ng-maxlength="40">
	                <label>Employee Present City:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empPresentState" name ="empPresentState" ng-model="employee.empPresentState" value={{employee.empPresentState}} ng-required="true" ng-minlength="3" ng-maxlength="40">
	                <label>Employee Present State:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empPresentPin" name ="empPresentPin" ng-model="employee.empPresentPin" value={{employee.empPresentPin}} ng-required="true" ng-minlength="6" ng-maxlength="6">
	                <label>Employee Present Pin:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <select name="empSex" ng-model="employee.empSex" value={{employee.empSex}} required>
                    	<option value="Male">Male</option>
                    	<option value="Female">Female</option>   
                     </select>
        				<label for="empsex">Employee Sex</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empFatherName" name ="empFatherName" ng-model="employee.empFatherName" value={{employee.empFatherName}} ng-required="true" ng-minlength="7" ng-maxlength="81">
	                <label>Employee Father Name:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empLicenseNo" name ="empLicenseNo" ng-model="employee.empLicenseNo" value={{employee.empLicenseNo}} ng-required="true" ng-minlength="3" ng-maxlength="40">
	                <label>Employee License No:</label>
	            </div>
	            <div class="input-field col s12 m4 l3">
	                <input type ="date" name ="empLicenseExp" ng-model="employee.empLicenseExp" value={{employee.empLicenseExp}} ng-required="true">
	                <label>Employee License Exp:</label>
	            </div>
	             <div class="input-field col s12 m4 l3">
	                <input type ="number" id="empTelephoneAmt" name ="empTelephoneAmt" ng-model="employee.empTelephoneAmt" value={{employee.empTelephoneAmt}} step="0.01" min="0.00">
	                <label>Employee Telephone Amount:</label>
	            </div>
	             <div class="input-field col s12 m4 l3">
	                <input type ="number" id="empMobAmt" name ="empMobAmt" ng-model="employee.empMobAmt" value={{employee.empMobAmt}} step="0.01" min="0.00">
	                <label>Employee Mobile Amount:</label>
	            </div>
	             <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empBankAcNo" name ="empBankAcNo" ng-model="employee.empBankAcNo" value={{employee.empBankAcNo}} ng-required="true" ng-minlength="10" ng-maxlength="40" min="0">
	                <label>Employee Bank Account No:</label>
	            </div>
	             <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empPanNo" name ="empPanNo" ng-model="employee.empPanNo" value={{employee.empPanNo}} ng-required="true" ng-minlength="10" ng-maxlength="10" ng-pattern="/^[a-zA-Z0-9]*$/">
	                <label>Employee Pan No:</label>
	            </div>
	             <div class="input-field col s12 m4 l3">
	                <input type ="email" id="empMailId" name ="empMailId" ng-model="employee.empMailId" value={{employee.empMailId}} ng-required="true" ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" ng-maxlength="40">
	                <label>Employee Mail Id:</label>
	            </div>
	             <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empPhNo" name ="empPhNo" ng-model="employee.empPhNo" value={{employee.empPhNo}} ng-minlength="4" ng-maxlength="15" min="0">
	                <label>Employee Phone No:</label>
	            </div>
	             <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empBankIFS" name ="empBankIFS" ng-model="employee.empBankIFS" value={{employee.empBankIFS}} ng-minlength="3" ng-maxlength="40">
	                <label>Employee Bank IFSC:</label>
	            </div>
	             <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empBankName" name ="empBankName" ng-model="employee.empBankName" value={{employee.empBankName}} ng-minlength="3" ng-maxlength="40">
	                <label>Employee Bank Name:</label>
	            </div>
	             <div class="input-field col s12 m4 l3">
	                <input type ="text" id="empBankBranch" name ="empBankBranch" ng-model="employee.empBankBranch" value={{employee.empBankBranch}} ng-minlength="3" ng-maxlength="40">
	                <label>Employee Bank Branch:</label>
	            </div>
        		 <div class="input-field col s12 m4 l3">
	                <select name="isTerminate" ng-model="employee.isTerminate"  value={{employee.isTerminate}} required>
                    	<option value="true">True</option>
                    	<option value="false">False</option>   
                    </select>
        			<label for="isTerminate">Employee Termination:</label>
	            </div>
        		<div class="input-field col s12 m4 l3">
        			<input type="submit" value="Update"/>
				</div>

			</div>                                                                                                                                                                                                                      
		</div>
	</form>
</div>

<div id ="branchCodeDB" ng-hide="branchCodeDBFlag">
	
	<input type="text" name="filterTextbox" ng-model="filterTextbox.branchCodeTemp" placeholder="Search by Branch Code Temp">
 	  
 	  <table>
 	 		<tr>	
 	 		    <th></th>
 	 		    		
				<th>Branch Code</th>
				
				<th>Branch Name</th>
			
				<th>Branch Code Temp</th>
				
				<th>Branch Pin</th>
				
				<!-- <th>Branch IsNCR</th> -->
			</tr>
		  
		  <tr ng-repeat="branch in branchList | filter:filterTextbox">
		 	  <td><input type="radio"  name="branchName" class="branchCls"  value="{{ branch.branchCode }}" ng-model="$parent.branchCode" ng-click="saveBranchCode(branch.branchCode,branch.isNCR)"></td>
              <td>{{ branch.branchCode }}</td>
              <td>{{ branch.branchName }}</td>
              <td>{{ branch.branchCodeTemp }}</td>
              <td>{{ branch.branchPin }}</td>
              <!-- <td>{{ branch.isNCR }}</td> -->
          </tr>
		
		  <tr>
			  <td colspan="3"><input type="button" value="Create Branch Code"  ng-click="openBranchPage()"></td>
		  </tr>
          
         </table>
  </div>
  
  <div id="presentAddrDB" ng-hide="presentAddrDBFlag">
    	<div class="row">
    		<div class="col s12 center" style="margin: 20px 0px;">
    			<span class="white-text">Is present address same as correspondence address?</span></div>
    		<div class="col s6 center"><a class="white-text" ng-click="yesPresentAddr()">Yes</a></div>
   	    	<div class="col s6 center"><a class="white-text" ng-click="YesNoAddr()">No</a></div>
   	    </div>
    </div>
    
    <div id="viewEmpDetailsDB" ng-hide="viewEmpDetailsFlag">
    
   	<div class="row">
	
	<div ng-hide = "show" class="col s12 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
				<h4>Here's the details of Employee <span class="teal-text text-lighten-2">{{employee.empName}}</span>:</h4>
		<table class="table-bordered table-hover table-condensed">
	            
	            <tr>
	                <td>Branch Code:</td>
	                <td>{{employee.branchCode}}</td>
	            </tr>
	            
	            <tr>
	                <td>Name:</td>
	                <td>{{employee.empName}}</td>
	            </tr>
	            
	            <tr>
	                <td>Address:</td>
	                <td>{{employee.empAdd}}</td>
	            </tr>
	            <tr>
	                <td>City:</td>
	                <td>{{employee.empCity}}</td>
	            </tr>
	            <tr>
	                <td>State:</td>
	                <td>{{employee.empState}}</td>
	            </tr>
	            <tr>
	                <td>Pin:</td>
	                <td>{{employee.empPin}}</td>
	            </tr>
	            <tr>
	                <td>Date Of Birth:</td>
	                <td>{{employee.empDOB}}</td>
	            </tr>
	            <tr>
	                <td>Date Of Appointment:</td>
	                <td>{{employee.empDOAppointment}}</td>
	            </tr>
	            <tr>
	                <td>Designation:</td>
	                <td>{{employee.empDesignation}}</td>
	            </tr>
	            <tr>
	                <td>Basic:</td>
	                <td>{{employee.empBasic}}</td>
	                
	            </tr>
	            <tr>
	                <td>HRA:</td>
	                <td>{{employee.empHRA}}</td>
	            </tr>
	            <tr>
	                <td>Other Allowance:</td>
	                <td>{{employee.empOtherAllowance}}</td>
	            </tr>
	            <tr>
	                <td>PF No:</td>
	                <td>{{employee.empPFNo}}</td>
	            </tr>
	            <tr>
	                <td>ESI No:</td>
	                <td>{{employee.empESINo}}</td>
	            </tr>
	            <tr>
	                <td>ESI Dispensary:</td>
	                <td>{{employee.empESIDispensary}}</td>
	            </tr>
	            <tr>
	                <td>Nominee:</td>
	                <td>{{employee.empNominee}}</td>
	            </tr>
	            <tr>
	                <td>Educational Qualification:</td>
	                <td>{{employee.empEduQuali}}</td>
	            </tr>
	            <tr>
	                <td>PF:</td>
	                <td>{{employee.empPF}}</td>
	            </tr>
	            <tr>
	                <td>ESI:</td>
	                <td>{{employee.empESI}}</td>
	            </tr>
	            <tr>
	                <td>Loan Balance:</td>
	                <td>{{employee.empLoanBal}}</td>
	            </tr>
	            
	            <tr>
	                <td>Loan Payment:</td>
	                <td>{{employee.empLoanPayment}}</td>
	            </tr>
	            
	            <tr>
	                <td>Present Address:</td>
	                <td>{{employee.empPresentAdd}}</td>
	            </tr>
	            
	            <tr>
	                <td>City:</td>
	                <td>{{employee.empPresentCity}}</td>
	            </tr>
	            
	            <tr>
	                <td>State:</td>
	                <td>{{employee.empPresentState}}</td>
	            </tr>
	            
	            <tr>
	                <td>Present Pin:</td>
	                <td>{{employee.empPresentPin}}</td>
	            </tr>
	            
	            <tr>
	                <td>Sex:</td>
	                <td>{{employee.empSex}}</td>
	            </tr>
	            
	            <tr>
	                <td>Father Name:</td>
	                <td>{{employee.empFatherName}}</td>
	            </tr>
	            
	            <tr>
	                <td>License No:</td>
	                <td>{{employee.empLicenseNo}}</td>
	            </tr>
	            
	            <tr>
	                <td>License Exp:</td>
	                <td>{{employee.empLicenseExp}}</td>
	            </tr>
	            
	             <tr>
	                <td>Telephone Amount:</td>
	                <td>{{employee.empTelephoneAmt}}</td>
	            </tr>
	            
	             <tr>
	                <td>Mobile Amount:</td>
	                <td>{{employee.empMobAmt}}</td>
	            </tr>
	            
	             <tr>
	                <td>Bank Account No:</td>
	                <td>{{employee.empBankAcNo}}</td>
	            </tr>
	            
	             <tr>
	                <td>Pan No:</td>
	                <td>{{employee.empPanNo}}</td>
	            </tr>
	            
	             <tr>
	                <td>Mail Id:</td>
	                <td>{{employee.empMailId}}</td>
	            </tr>
	            
	             <tr>
	                <td>Phone No:</td>
	                <td>{{employee.empPhNo}}</td>
	            </tr>
	            
	             <tr>
	                <td>Bank IFSC:</td>
	                <td>{{employee.empBankIFS}}</td>
	            </tr>
	            
	             <tr>
	                <td>Bank Name:</td>
	                <td>{{employee.empBankName}}</td>
	            </tr>
	            
	             <tr>
	                <td>Bank Branch:</td>
	                <td>{{employee.empBankBranch}}</td>
	            </tr>
	            
	            <tr>
	                <td>Employee IsTerminate:</td>
	                <td>{{employee.isTerminate}}</td>
	            </tr>
</table>

<input type="button" value="Save" ng-click="saveEdittedEmployee(employee)">
<input type="button" value="Cancel" ng-click="closeViewEmpDetailsDB()">
		
		
</div>
</div>
</div>

<div  ng-hide="viewEmpOldDetailsFlag">
<div class="row">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
	<div ng-hide = "show" class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
		<h4>Modification Details <span class="teal-text text-lighten-2"></span>:</h4>
		<table class="table-bordered table-hover table-condensed">
	                        
	            <tr>
	                <td>Name:</td>
	                <td>{{empOld.empName}}</td>
	            </tr>
	            <tr>
	                <td>Code:</td>
	                <td>{{empOld.empCode}}</td>
	            </tr>
	            <tr>
	                <td>Branch Code:</td>
	                <td>{{empOld.branchCode}}</td>
	            </tr>
	            <tr>
	                <td>Address:</td>
	                <td>{{empOld.empAdd}}</td>
	            </tr>
	            <tr>
	                <td>City:</td>
	                <td>{{empOld.empCity}}</td>
	            </tr>
	            <tr>
	                <td>State:</td>
	                <td>{{empOld.empState}}</td>
	            </tr>
	            <tr>
	                <td>Pin:</td>
	                <td>{{empOld.empPin}}</td>
	            </tr>
	            <tr>
	                <td>Date Of Birth:</td>
	                <td>{{empOld.empDOB}}</td>
	            </tr>
	            <tr>
	                <td>Date Of Appointment:</td>
	                <td>{{empOld.empDOAppointment}}</td>
	            </tr>
	            <tr>
	                <td>Designation:</td>
	                <td>{{empOld.empDesignation}}</td>
	            </tr>
	            <tr>
	                <td>Basic:</td>
	                <td>{{empOld.empBasic}}</td>
	                
	            </tr>
	            <tr>
	                <td>HRA:</td>
	                <td>{{empOld.empHRA}}</td>
	            </tr>
	            <tr>
	                <td>Other Allowance:</td>
	                <td>{{empOld.empOtherAllowance}}</td>
	            </tr>
	            <tr>
	                <td>PF No:</td>
	                <td>{{empOld.empPFNo}}</td>
	            </tr>
	            <tr>
	                <td>ESI No:</td>
	                <td>{{empOld.empESINo}}</td>
	            </tr>
	            <tr>
	                <td>ESI Dispensary:</td>
	                <td>{{empOld.empESIDispensary}}</td>
	            </tr>
	            <tr>
	                <td>Nominee:</td>
	                <td>{{empOld.empNominee}}</td>
	            </tr>
	            <tr>
	                <td>Educational Qualification:</td>
	                <td>{{empOld.empEduQuali}}</td>
	            </tr>
	            <tr>
	                <td>PF:</td>
	                <td>{{empOld.empPF}}</td>
	            </tr>
	            <tr>
	                <td>ESI:</td>
	                <td>{{empOld.empESI}}</td>
	            </tr>
	            <tr>
	                <td>Loan Balance:</td>
	                <td>{{empOld.empLoanBal}}</td>
	            </tr>
	            
	            <tr>
	                <td>Loan Payment:</td>
	                <td>{{empOld.empLoanPayment}}</td>
	            </tr>
	            
	            <tr>
	                <td>Present Address:</td>
	                <td>{{empOld.empPresentAdd}}</td>
	            </tr>
	            
	            <tr>
	                <td>City:</td>
	                <td>{{empOld.empPresentCity}}</td>
	            </tr>
	            
	            <tr>
	                <td>State:</td>
	                <td>{{empOld.empPresentState}}</td>
	            </tr>
	            
	            <tr>
	                <td>Present Pin:</td>
	                <td>{{empOld.empPresentPin}}</td>
	            </tr>
	            
	            <tr>
	                <td>Sex:</td>
	                <td>{{empOld.empSex}}</td>
	            </tr>
	            
	            <tr>
	                <td>Father Name:</td>
	                <td>{{empOld.empFatherName}}</td>
	            </tr>
	            
	            <tr>
	                <td>License No:</td>
	                <td>{{empOld.empLicenseNo}}</td>
	            </tr>
	            
	            <tr>
	                <td>License Exp:</td>
	                <td>{{empOld.empLicenseExp}}</td>
	            </tr>
	            
	             <tr>
	                <td>Telephone Amount:</td>
	                <td>{{empOld.empTelephoneAmt}}</td>
	            </tr>
	            
	             <tr>
	                <td>Mobile Amount:</td>
	                <td>{{empOld.empMobAmt}}</td>
	            </tr>
	            
	             <tr>
	                <td>Bank Account No:</td>
	                <td>{{empOld.empBankAcNo}}</td>
	            </tr>
	            
	             <tr>
	                <td>Pan No:</td>
	                <td>{{empOld.empPanNo}}</td>
	            </tr>
	            
	             <tr>
	                <td>Mail Id:</td>
	                <td>{{empOld.empMailId}}</td>
	            </tr>
	            
	             <tr>
	                <td>Phone No:</td>
	                <td>{{empOld.empPhNo}}</td>
	            </tr>
	            
	             <tr>
	                <td>Bank IFSC:</td>
	                <td>{{empOld.empBankIFS}}</td>
	            </tr>
	            
	             <tr>
	                <td>Bank Name:</td>
	                <td>{{empOld.empBankName}}</td>
	            </tr>
	            
	             <tr>
	                <td>Bank Branch:</td>
	                <td>{{empOld.empBankBranch}}</td>
	            </tr>
	            
	             <tr>
	                <td>IsTerminate:</td>
	                <td>{{empOld.isTerminate}}</td>
	            </tr>
	                 
	            <tr>
	                <td>CreationTS:</td>
	                <td>{{empOld.creationTS}}</td>
	            </tr>                                                                                                                                                                                                            
	           
</table>
		
</div>		
</div>
</div>
</div>
    
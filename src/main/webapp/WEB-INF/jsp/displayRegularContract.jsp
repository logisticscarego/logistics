<div ng-show="adminLogin || superAdminLogin">

<title>Display Regular Contract</title>
<div class="container">
		<div class="row">
			<div class="col s2"><input type="radio" name="regContList" ng-model="regContByModal" ng-click="enableModalTextBox()"></div>
			<div class="col s5"><input type = "text" id="regContByMId" name="regContByM" ng-model="regContByM" disabled="disabled" ng-click="OpenRegContCodeDB()" readonly></div>
			<div class="col s5"><input type="text" id="oldRegContId" ng-model="oldRegCont" ng-click="openOldContractDB()" disabled="disabled" readonly></div>	
		</div>
		<div class="row">
			<div class="col s2"><input type="radio" name="regContList" ng-model="regContByAuto" ng-click="enableAutoTextBox()"></div>
			<div class="col s8"><input type = "text" id="regContByAId" name="regContByA" ng-model="regContByA" disabled="disabled" ng-keyup="getContCodeList()"></div>
			<div class="col s2"><input class="col s12" type ="button" id="ok" value = "OK" ng-click="getContractList()" disabled="disabled"></div>
		</div>
		<div class="row">
			<div class="col s2"><input type ="button" value = "Back To list" ng-click="backToList()"></div>
		</div>
</div>
		
		<div id ="regContCodeDB" ng-hide = "regContCodeFlag">
 	   <input type="text" name="filterRegContCode" ng-model="filterRegContCode" placeholder="Search by Contract Code">	
 	   	  <table>
		  <tr ng-repeat="regContCodes in regContCodeList  | filter:filterRegContCode">
		 	  <td><input type="radio"  name="contCodesName"  value="{{ regContCodes }}" ng-model="contCodes" ng-click="saveContCode(regContCodes)"></td>
              <td>{{ regContCodes}}</td>
          </tr>
         </table>       
	</div>
	
		<div ng-hide = "showContractDetails" class="container">
		<form  name="EditRegularContractForm" ng-submit="EditRegularContractSubmit(EditRegularContractForm,regCnt,metricType,tnc,pbdP,pbdB,pbdD)">
			<div class="row">
      			<div class="input-field col s3">
        			<input type ="hidden"  name ="branchCode" ng-model="regCnt.branchCode" ng-click="OpenBranchCodeDB()" readonly>
        			<!-- <label>Branch Code</label> -->
      			</div>
      			<div class="input-field col s3">
      				<input type ="text" id="regContBLPMCode" name ="regContBLPMCode" ng-model="regCnt.regContBLPMCode" ng-click="OpenregContBLPMCodeDB()" readonly >
      				<label>BLPM Code</label>
      			</div>
      			<div class="input-field col s3">
        			<input type ="text" id="regContCngrCode" name ="regContCngrCode" ng-model="regCnt.regContCngrCode" ng-click="OpenregContCngrCodeDB()" readonly>
        			<label>Consignor Code</label>
      			</div>
      			<div class="input-field col s3">
        			<input type ="text" id="regContCrName" name ="regContCrName" ng-model="regCnt.regContCrName" ng-click="OpenregContCrNameDB()"readonly >
        			<label>CR Name</label>
      			</div>
    		</div>
    		
    		
    	<div class="row">
      		<div class="input-field col s3">
      				<select name="metricType" id="metricType" ng-model="metricType"  ng-blur="sendMetricType(metricType)">
						<option value="Ton">Ton</option>
						<option value="Kg">Kg</option>
					</select>
					<label>Metric Type</label>
			</div>
    		
    		<div class="input-field col s3">
      			<input type="date" id="regContFromDt" class="datepicker" name ="regContFromDt" ng-model="regCnt.regContFromDt" >
      			<label>From Date</label>
      		</div>
      		
      		<div class="input-field col s3">
      			<input type="date" id="regContToDt" class="datepicker" name ="regContToDt" ng-model="regCnt.regContToDt" >
      			<label>To Date</label>
      		</div>
          </div>
       <!--    
          <div class="row">
      		 	<div class="col s3">
      				<div class="input-field col s8">
        				<input type="radio" id="regContProportionate" name ="regContProportionate" ng-model="regCnt.regContProportionate" value ="P" ng-click="clickP()">
						<label>Proportionate</label>
					</div>
					<div class="input-field col s4">
						<input type ="radio" id="regContProportionate1" name ="regContProportionate" ng-model="regCnt.regContProportionate" value="F" ng-click="clickF()">
        				<label>Fixed</label>
        			</div>
      			</div>
      			<div class="input-field col s3">
      				<input type ="number" id="regContAdditionalRate" name ="regContAdditionalRate" ng-model="regCnt.regContAdditionalRate" min="1"  disabled="disabled" ng-blur="setFflagTrue(regCnt)">        			
        			<label>Additional Rate</label>
      			</div>
      	</div> -->
      	
      	<div class="row">
    	
    		<!-- 	<div class="col s3">
      	  		<div class=" input-field col s3"> 
      				<input class="with-gap" type="radio" id="regContType" name ="regContType" ng-model="regCnt.regContType" value="W" ng-click="clickW(metricType,regCnt.regContProportionate)">
      				<label for="regContType">W</label>
      			</div>
      			<div class="input-field col s3">
					<input class="with-gap" type="radio" id="regContType1" name ="regContType" ng-model="regCnt.regContType" value="Q" ng-click="clickQ(metricType,regCnt.regContProportionate)">
					<label for="regContType1">Q</label>
				</div>
      			<div class="input-field col s3">
					<input class="with-gap" type="radio" id="regContType2" name ="regContType" ng-model="regCnt.regContType" value="K" ng-click="clickK(metricType,regCnt.regContProportionate)">
      				<label for="regContType2">K</label>
      			</div>
      		</div> -->
    	
      		<div class="input-field col s3">
      			<input type ="text" id="regContFromStation" name ="regContFromStation" ng-model="regCnt.regContFromStation" ng-click="OpenregContFromStationDB()"readonly>
      			<label>From Station</label>	
      			<input class="col s12 btn teal white-text" type ="button"  id="addStation" name ="addStation" ng-model="addStation" ng-click="openAddNewStnDB()">
      		</div>
      		
      		<div class="input-field col s3" ng-show="onWQClick">
        		<input type ="button" id="ToStations" name ="ToStations" ng-model="ToStations" ng-click="contToStnDB(regCnt,metricType)">
        		<label>To Station</label>
      		</div>
      		
      		<div class="input-field col s3" ng-show="onKClick">
      			<input type ="text" id="regContToStation" name ="regContToStation" ng-model="regCnt.regContToStation" ng-click="OpenregContToStationDB()" readonly ng-blur="setToStnFlag(regCnt)">
      			<label>To Station</label>
      		</div>
      		
      		<div class="col s3">
      				<input class="col s12 btn teal white-text" type ="button" value="Add RBKM" id="regContRbkmId" name ="regContRbkmId" ng-model="regCnt.regContRbkmId" ng-click="OpenregContRbkmIdDB(metricType)" disabled="disabled">
      		</div>
          </div>
              
               	<div class="row" ng-show="newCtsFlagW" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);overflow:auto;" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in ctsList">
					<td>{{regCnt.regContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		          	<td>{{cts.ctsRate}}</td>
		            
		            <td><input type ="button" ng-click="removeNewCTS(cts)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		<input type ="button" ng-show="ctsList.length > 1" ng-click="removeAllCTS()" value="Remove All CTS">
		</div>
		</div>
		
		<div class="row" ng-show="ctsFlagW" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in oldCtsList">
					<td>{{regCnt.regContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		          	<td>{{cts.ctsRate}}</td>
		            
		            <td><input type ="button" ng-click="removeCTS(cts)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
		
		   	<div class="row" ng-show="ctsFlagQ" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Product Type</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in oldCtsList">
					<td>{{regCnt.regContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		            <td>{{cts.ctsProductType}}</td>
		          	<td>{{cts.ctsRate}}</td>
		            
		            <td><input type ="button" ng-click="removeCTS(cts)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
   		
   		   	<div class="row" ng-show="newCtsFlagQ" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Product Type</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in ctsList">
					<td>{{regCnt.regContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		            <td>{{cts.ctsProductType}}</td>
		          	<td>{{cts.ctsRate}}</td>
		            
		            <td><input type ="button" ng-click="removeNewCTS(cts)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		<input type ="button" ng-show="ctsList.length > 1" ng-click="removeAllCTS()" value="Remove All CTS">
		</div>
		</div>
              
              
      <div class="row">
      		<!-- <div class="input-field col s3">
      			<input type ="number" id="regContFromWt" name ="regContFromWt" ng-model="regCnt.regContFromWt"  step="0.01" min="0.01" required >
      			<label>From Weight</label>
      	    </div>
      	        
      		<div class="input-field col s3">
       			<input type ="number" id="regContToWt" name ="regContToWt" ng-model="regCnt.regContToWt"  step="0.01"  required min="0.01" >
      			<label>To Weight</label>
      		</div> -->
      		
      		<div class="input-field col s3">
				<input class="prefix" type="checkbox" id ="checkLoad" ng-model="checkLoad" ng-change="saveTncLoad()">
				<input class="validate" type ="number" id="regContLoad" name ="regContLoad" ng-model="regCnt.regContLoad" ng-disabled="!checkLoad"  step="0.01" min="0.01"  ng-blur="setLoadFlagTrue(regCnt)">				<label>Load</label>
				<input type ="hidden" ng-model="tnc.tncLoading" id="tncLoading" ng-init="tnc.tncLoading='no'" >
      	    </div>
      		<div class="input-field col s3">
				
				<input class="prefix" type="checkbox" id ="checkUnLoad" ng-model="checkUnLoad" ng-change="saveTncUnload()">
				<input class="validate" type ="number" id="regContUnLoad" name ="regContUnLoad" ng-model="regCnt.regContUnLoad" ng-disabled="!checkUnLoad"  step="0.01" min="0.01"  ng-blur="setUnLoadFlagTrue(regCnt)">
				<label>UnLoad</label>
				<input type ="hidden" ng-model="tnc.tncUnLoading" id="tncUnLoading"  ng-init="tnc.tncUnLoading='no'">
      			
      		</div>
      	</div>
      	
      	 	<div class="row">
      		<!-- <div class="input-field col s3">
				<input type ="number" id ="regContRate" name ="regContRate" ng-model="regCnt.regContRate"  step="0.01"  min="0.01"  required>
			 	<label> <i class="fa fa-inr"></i> Rate</label>
			 </div>
      		<div class="input-field col s3">
      			
				<select name="regContRate1" id="regContRate1" ng-model="regContRatePer" required>
					<option value="Per Ton">Per Ton</option>
					<option value="Per Kg">Per Kg</option>
				</select>
				<label>Per Ton / KG</label>
			</div> -->
      		<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkTransitDay" ng-model="checkTransitDay" ng-change="saveTncTransitDay()">
      				<input class="validate" type ="number" id="regContTransitDay" name ="regContTransitDay" ng-model="regCnt.regContTransitDay" ng-disabled="!checkTransitDay"  min="1" ng-blur="setTransitFlagTrue(regCnt)" >
					<label for="regContTransitDay">Transit Day</label>
					<input type ="hidden" ng-model="tnc.tncTransitDay" id="tncTransitDay" ng-init="tnc.tncTransitDay='no'" >
        	</div>
      		<div class="input-field col s3">	
      				<input class="prefix" type="checkbox" id ="checkStatisticalCharge" ng-model="checkStatisticalCharge" ng-change="saveTncStatisticalCharge()">
      				<input class="validate" type ="number" id="regContStatisticalCharge" name ="regContStatisticalCharge" ng-model="regCnt.regContStatisticalCharge" ng-disabled="!checkStatisticalCharge"  step="0.01" min="0.01"  ng-blur="setSCFlagTrue(regCnt)">
					<label for="regContStatisticalCharge">Statistical Charge</label>
					<input type ="hidden" ng-model="tnc.tncStatisticalCharge" id="tncStatisticalCharge" ng-init="tnc.tncStatisticalCharge='no'" >
      		</div>
      			
      		<!-- <div class="input-field col s3">
      			<a class="btn-floating suffix waves-effect teal" type="button" id="openVehicleTypeDB" ng-model="savVehicletype" ng-click="OpenregContVehicleTypeDB1()" ><i class="mdi-content-add white-text"></i></a>
      			<input type ="text" id="regContVehicleType" name ="regContVehicleType" ng-model="regCnt.regContVehicleType" ng-click="OpenregContVehicleTypeDB()" readonly required>
      			<label for="regContVehicleType">Vehicle Type</label>	
      	    </div> -->
          </div>
          
<div class="row"  ng-show="rbkmFlag" style="margin-bottom: 50px; margin-top: -100px;">
    <div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >
		<table>
		    <thead>
		    	<tr><td><h5>RBKM</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>State Code</th>
		            <th>From Km</th>
		            <th>To Km</th>
		            <th>Vehicle Type</th>
		            <th>Rate</th>
		            <th>Action</th>   
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="rbkm in rbkmList">
		            
		            <td>{{rbkm.rbkmFromStation}}</td>
		            <td>{{rbkm.rbkmToStation}}</td>
		            <td>{{rbkm.rbkmStateCode}}</td>
		            <td>{{rbkm.rbkmFromKm}}</td>
		            <td>{{rbkm.rbkmToKm}}</td>
		            <td>{{rbkm.rbkmVehicleType}}</td>
		            <td>{{rbkm.rbkmRate}}</td>
		            <td><input type="button" class="btn" ng-click="removeRbkm(rbkm)" value="Remove"></td>
		            <!-- <button class="btn" ng-click="removeRbkm(rbkm)">Remove</button> -->
		        </tr>
		    </tbody>
		</table>
		<input type="button" ng-show="rbkmList.length > 1" ng-click="removeAllRbkm()" value="Remove All RBKM">
   </div>
</div>
         
         <div class="row"  ng-show="newRbkmFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		    	<tr><td><h5>RBKM</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>State Code</th>
		            <th>From Km</th>
		            <th>To Km</th>
		            <th>Vehicle Type</th>
		            <th>Rate</th>
		            <th>Action</th>   
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="rbkm in rateByKmList">
		            <td>{{rbkm.rbkmFromStation}}</td>
		            <td>{{rbkm.rbkmToStation}}</td>
		            <td>{{rbkm.rbkmStateCode}}</td>
		            <td>{{rbkm.rbkmFromKm}}</td>
		            <td>{{rbkm.rbkmToKm}}</td>
		            <td>{{rbkm.rbkmVehicleType}}</td>
		            <td>{{rbkm.rbkmRate}}</td>
		            <td><input type ="button" ng-click="removeNewRbkm(rbkm)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		<input type ="button" ng-show="rateByKmList.length > 1" ng-click="removeAllRbkm()" value="Remove All RBKM">
		</div>
	</div> 
          
          
          <div class="row">
      			<div class="input-field col s3">
        			<input class="validate" type ="text" id="regContInsuredBy" name ="regContInsuredBy" ng-model="regCnt.regContInsuredBy" value="{{regCnt.regContInsuredBy}}">
        			<label for="regContInsuredBy">Insured By</label>
      			</div>
      			<div class="input-field col s3">
      				<input class="validate" type ="text" id="regContInsureComp" name ="regContInsureComp" ng-model="regCnt.regContInsureComp" value="{{regCnt.regContInsureComp}}">
      				<label for="regContInsureComp">Insured Company</label>
      			</div>	
      			<div class="input-field col s3">
      				<input class="validate" type ="text" id="regContInsureUnitNo" name ="regContInsureUnitNo" ng-model="regCnt.regContInsureUnitNo" value="{{regCnt.regContInsureUnitNo}}">
        			<label for="regContInsureUnitNo">Insured Unit Number</label>
        			
      			</div>
      			<div class="input-field col s3">
        			<input class="validate" type ="text" id="regContInsurePolicyNo" name ="regContInsurePolicyNo" ng-model="regCnt.regContInsurePolicyNo" value="{{regCnt.regContInsurePolicyNo}}">
        			<label for="regContInsurePolicyNo">Insured Policy Number</label>
      			</div>
      			
    		</div>
      		
      		<div class="row">
      			<div class="input-field col s3">
      				
        			<select name="regContDc" id="regContDc" ng-model="regCnt.regContDc" ng-init="regCnt.regContDc">
					<option value="01">One bill</option>
					<option value="02">Two bill</option>
					<option value="10">Direct Payment through CNMT</option>
					<option value="20">Twice Payment</option>
					<option value="11">Partially through bill Partially through CNMT</option>
					</select>
					<label>DC</label>
      			</div>
      			<div class="input-field col s3">
					
      				<select name="regContCostGrade" id="regContCostGrade" ng-model="regCnt.regContCostGrade" ng-init="regCnt.regContCostGrade">
					<option value="A">Heavy Material</option>
					<option value="B">Natural Rubber</option>
					<option value="C">General Goods</option>
					<option value="D">Light Goods</option>
					</select>
					<label>Cost Grade</label>
      			</div>
      			<div class="input-field col s3">
        			
        			<select name="regContDdl" id="regContDdl" ng-model="regCnt.regContDdl" ng-init="regCnt.regContDdl">
					<option value="Yes">Yes</option>
					<option value="No">No</option>
					</select>
					<label>DDL</label>
      			</div>
      			
      			<div class="input-field col s3">
			   		<input class="prefix" type="checkbox" id ="checkPenalty" ng-model="checkPenalty" ng-change="saveTncPenalty()">
			   		<input class="btn teal white-text nopadding" type ="button" value="Penalty" id="regContPenalty"  ng-disabled="!checkPenalty" ng-click="OpenPenaltyDB()">
			   		<label>Add</label>
			   		<input type ="hidden" ng-model="tnc.tncPenalty" id="tncPenalty" ng-init="tnc.tncPenalty='no'">
      			</div>
      			
      		</div>	
      		
      		<div class="row">
      			<div class="input-field col s3">
      				
      				<select name="regContRenew" id="regContRenew" ng-model="regCnt.regContRenew" ng-init="regCnt.regContRenew">
					<option value="F">Fresh</option>
					<option value="R">Routine</option>
					<option value="D">Diesel Hike</option>
					</select>
					<label>Renew</label>
      			</div>
      			<div class="input-field col s3">
      				<input type ="date" id="regContRenewDt" name ="regContRenewDt" ng-model="regCnt.regContRenewDt" value="{{regCnt.regContRenewDt}}">
      				<label>Renew Date</label>
      			</div>
      			<div class="input-field col s3">
        			<input type ="number" id="regContWt" name ="regContWt" ng-model="regCnt.regContWt"  step="0.01" min="0.01" value="{{regCnt.regContWt}}">
        			<label>Weight</label>
      			</div>
      			
      		<!-- 	<div class="input-field col s1.5">
      				<select name="weight" id="weight" ng-model="weight" required ng-change="changeWt(weight,regCnt)">
						<option value="Ton">Ton</option>
						<option value="Kg">Kg</option>
					</select>
					<label>Ton/KG</label>
				</div> -->
      			
      			<div class="input-field col s3">
			    	<input class="prefix" type="checkbox" id ="checkBonus" ng-model="checkBonus" ng-change="saveTncBonus()">
			    	<input class="btn teal white-text nopadding" type ="button" id="regContBonus" value="Bonus" ng-disabled="!checkBonus" ng-click="OpenBonusDB()">
			    	<label>Add</label>
			    	<input type ="hidden" ng-model="tnc.tncBonus" id="tncBonus" ng-init="tnc.tncBonus='no'">
        		</div>
      		</div>	
      		
      		<div class="row">
      			<!-- <div class="col s3">
      				<div class="input-field col s8">
        				<input type="radio" id="regContProportionate" name ="regContProportionate" ng-model="regCnt.regContProportionate" value ="P" ng-click="clickP()">
						<label>Proportionate</label>
					</div>
					<div class="input-field col s4">
						<input type ="radio" id="regContProportionate1" name ="regContProportionate" ng-model="regCnt.regContProportionate" value="F" ng-click="clickF()">
        				<label>Fixed</label>
        			</div>
      			</div> -->
      			<!-- <div class="input-field col s3">
      				<input type ="number" id="regContAdditionalRate" name ="regContAdditionalRate" ng-model="regCnt.regContAdditionalRate" min="1"  disabled="disabled" ng-blur="setFflagTrue(regCnt)">        			
        			<label>Additional Rate</label>
      			</div> -->
      			<div class="input-field col s3">
        			<input class="validate" type ="number" id="regContValue" name ="regContValue" ng-model="regCnt.regContValue"  step="0.01" min="0.01" value="{{regCnt.regContValue}}">
        			<label>Value</label>
      			</div>
      			<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkDetention" ng-model="checkDetention" ng-change="saveTncDetention()">
					<input class="btn teal white-text nopadding" type ="button" id="regContDetention" value="Detention" ng-disabled="!checkDetention" ng-click="OpenDetentionDB()">
					<label>Add</label>
					<input type ="hidden" ng-model="tnc.tncDetention" id="tncDetention" ng-init="tnc.tncDetention='no'">
      			</div>
    		</div>
    		<div class="row"  ng-show="pbdFlag" style="margin-bottom: 50px; margin-top: -100px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >
	<table>
		    <thead>
		    	<tr><td><h5>PBD</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Start Date</th>
		            <th>End Date</th>
		            <th>From Day</th>
		            <th>To Day</th>
		            <th>Vehicle Type</th>
		            <th>P/B/D</th>
		            <th>Hour N Day</th>
		            <th>Amount</th>
		            <th>Action</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="pbd in pbdList">
		            <td>{{pbd.pbdFromStn}}</td>
		            <td>{{pbdP.pbdToStn}}</td>
		            <td>{{pbd.pbdStartDt}}</td>
		            <td>{{pbd.pbdEndDt}}</td>
		            <td>{{pbd.pbdFromDay}}</td>
		            <td>{{pbd.pbdToDay}}</td>
		            <td>{{pbd.pbdVehicleType}}</td>
		            <td>{{pbd.pbdPenBonDet}}</td>
		            <td>{{pbd.pbdHourNDay}}</td>
		            <td>{{pbd.pbdAmt}}</td>
		            <td><input type="button" class="btn" ng-click="removePbd(pbd)" value="Remove"></td>
		        </tr>
		    </tbody>
	</table>
		<input type="button" ng-show="pbdList.length > 1" ng-click="removeAllPbd()" value="Remove All PBD">
          </div>
          </div>
    		
    <div class="row" ng-show="newPbdFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		    	<tr><td><h5>PBD</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Start Date</th>
		            <th>End Date</th>
		            <th>From Dlay</th>
		            <th>To Dlay</th>
		            <th>Vehicle Type</th>
		            <th>P/B/D</th>
		            <th>Hour N Day</th>
		            <th>Amount</th>
		            <th>Action</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="pbd in newPbdList">
		            
		            <td>{{pbd.pbdFromStn}}</td>
		            <td>{{pbd.pbdToStn}}</td>
		            <td>{{pbd.pbdStartDt}}</td>
		            <td>{{pbd.pbdEndDt}}</td>
		            <td>{{pbd.pbdFromDay}}</td>
		            <td>{{pbd.pbdToDay}}</td>
		            <td>{{pbd.pbdVehicleType}}</td>
		            <td>{{pbd.pbdPenBonDet}}</td>
		            <td>{{pbd.pbdHourNDay}}</td>
		            <td>{{pbd.pbdAmt}}</td>
		            <td><input type ="button" ng-click="removeNewPbd(pbd)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		<input type ="button" ng-show="newPbdList.length > 1" ng-click="removeAllPbd()" value="Remove All PBD">
		</div>
		</div>
		
    		
    		<div class="row">
      			<div class="input-field col s12">
      				<i class="mdi-editor-mode-edit prefix"></i>
        			<textarea id="textarea1" class="materialize-textarea"  rows="3" cols="92"  name ="regContRemark" ng-model="regCnt.regContRemark" value="{{regCnt.regContRemark}}"></textarea>
        			<label>Remark</label>
      			</div>
      		</div>	
      			   
			 <div class="input-field col s12 m4 l3">
					<div><input type="submit" value="Submit!"></div>
			</div>
	           <!--  <div class="input-field col s12 m12 l12">
					<input type="button" value="Submit!" ng-click="EditRegularContractSubmit(regCnt,metricType,tnc,pbdP,pbdB,pbdD)">
				</div> -->
		</form>		
		</div>
	
	
	<div id="regContRbkmIdDB" ng-hide="RbkmIdFlag">	
	<form name="RbkmForm" ng-submit="saveRbkmId(RbkmForm,rbkm)"> 
	
		<table class="noborder" >
			<tr>
				<td>From Station *</td>
				<td><input type ="text" id="rbkmFromStationTemp" name ="rbkmFromStationTemp" ng-model="rbkm.rbkmFromStationTemp" ng-click="OpenrbkmFromStationDB()" required readonly></td>
				<td><input type ="hidden" id="rbkmFromStation" name ="rbkmFromStation" ng-model="rbkm.rbkmFromStation" ></td>
			</tr>
			
			<tr>
				<td>To Station *</td>
				<td><input type ="text"  id="rbkmToStationTemp" name ="rbkmToStationTemp" ng-model="rbkm.rbkmToStationTemp" ng-click="OpenrbkmToStationDB()" required readonly></td>
			<td><input type ="hidden"  id="rbkmToStation" name ="rbkmToStation" ng-model="rbkm.rbkmToStation" ></td>
			</tr>
			
			<tr>
				<td>Start Date *</td>
				<td><input type ="date"  id="rbkmStartDate" name ="rbkmStartDate" ng-model="rbkm.rbkmStartDate" required></td>
			</tr>
			
			<tr>
				<td>End Date *</td>
				<td><input type ="date"  id="rbkmEndDate" name ="rbkmEndDate" ng-model="rbkm.rbkmEndDate" required></td>
			</tr>
			
			<tr>
				<td>State Code *</td>
				<td><input type ="text"  id="rbkmStateCodeTemp" name ="rbkmStateCodeTemp" ng-model="rbkm.rbkmStateCodeTemp" ng-click="OpenrbkmStateCodeDB()" readonly required></td>
				<td><input type ="hidden"  id="rbkmStateCode" name ="rbkmStateCode" ng-model="rbkm.rbkmStateCode"></td>
			</tr>
			
			<tr>
				<td>From Kilometer *</td>
				<td><input type ="number"  id="rbkmFromKm" name ="rbkmFromKm" ng-model="rbkm.rbkmFromKm" min="1" ng-minlength="1" ng-maxlength="7" required></td>
			</tr>
			
			<tr>
				<td>To Kilometer *</td>
				<td><input type ="number"  id="rbkmToKm" name ="rbkmToKm" ng-model="rbkm.rbkmToKm" min="1" ng-minlength="1" ng-maxlength="7" required></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text"  id="rbkmVehicleType" name ="rbkmVehicleType" ng-model="rbkm.rbkmVehicleType" ng-click="OpenRbkmVehicleTypeDB()" required readonly></td>
			</tr>
			
			<tr>
				<td>Rbkm Rate *</td>
				<td><input type ="number"  id="rbkmRate" name ="rbkmRate" ng-model="rbkm.rbkmRate" step="0.01" min="0.01"  required></td>
			</tr>
			
		 	<tr>
				<td><input type="submit" id="submit3" value="Submit" ></td>
			</tr>
		</table>
	</form>
	</div>	
	
	<div id="AddProductTypeDB" ng-hide = "AddProductTypeFlag">
	<form name="ProductTypeDB1Form" ng-submit="saveproducttype(ProductTypeDB1Form,pt)">
		<table>
			<tr>
				<td>Product Name</td>
				<td><input type ="text" id="ptName" name ="ptName" ng-model="pt.ptName" required></td>
			</tr>
			
		 	<tr>
				<td><input type="submit"  value="Submit" ></td>
			</tr>
		</table>
		</form>
	</div>	
	
	<div id="addVehicleTypeDB1" ng-hide = "VehicleTypeFlag1">
	<form name="VehicleForm" ng-submit="savevehicletype(VehicleForm,vt)">
		<table class="noborder">
			<tr>
				<td>Service Type *</td>
				<td><input type ="text" id="vtServiceType" name ="vtServiceType" ng-model="vt.vtServiceType" ng-minlength="1" ng-maxlength="30" required></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="vtVehicleType" name ="vtVehicleType" ng-model="vt.vtVehicleType" ng-minlength="1" ng-maxlength="30" required></td>
			</tr>
			
			<tr>
				<td>Code *</td>
				<td><input type ="text" id="vtCode" name ="vtCode" ng-model="vt.vtCode"  required></td>
			</tr>
			
			<tr>
				<td>Load Limit *</td>
				<td><input type ="number" id="vtLoadLimit" name ="vtLoadLimit" ng-model="vt.vtLoadLimit" ng-minlength="1" ng-maxlength="7" min ="1"  required></td>
			</tr>
			
			<tr>
				<td>Guarantee Weight *</td>
				<td><input type ="number" id="vtGuaranteeWt" name ="vtGuaranteeWt" ng-model="vt.vtGuaranteeWt" step="0.01" min="0.01" required></td>
			</tr>
			
		 	<tr>
				<td><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>
	
		<div id="PenaltyDB" ng-hide="PenaltyFlag">
	<form name="PenaltyForm" ng-submit="savePbdForPenalty(PenaltyForm,pbdP)"> 
		<table>
		
			<tr>
				<td>From Station *</td>
				<td><input type ="text" id="pbdFromStnTempP" name ="pbdFromStnTempP" ng-model="pbdP.pbdFromStnTemp" ng-click="OpenPenaltyFromStationDB()" readonly required></td>
				<td><input type ="hidden" id="pbdFromStnP" name ="pbdFromStnP" ng-model="pbdP.pbdFromStn"  ></td>
			</tr>
		
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="pbdToStnTempP" name ="pbdToStnTempP" ng-model="pbdP.pbdToStnTemp" ng-click="OpenPenaltyToStationDB()" readonly required></td>
				<td><input type ="hidden" id="pbdToStnP" name ="pbdToStnP" ng-model="pbdP.pbdToStn"></td>
			
			</tr>
			
			<tr>
				<td>Start Date *</td>
				<td><input type ="date" id="pbdStartDtP" name ="pbdStartDtP" ng-model="pbdP.pbdStartDt" required></td>
			</tr>
			
			<tr>
				<td>End Date *</td>
				<td><input type ="date" id="pbdEndDtP" name ="pbdEndDtP" ng-model="pbdP.pbdEndDt" required></td>
			</tr>
			
			<tr>
				<td>From Day*</td>
				<td><input type ="number" id="pbdFromDayP" name ="pbdFromDayP" ng-model="pbdP.pbdFromDay" required min="1" ng-minlength="1" ng-maxlength="7"></td>
			</tr>
			
			<tr>
				<td>To Day *</td>
				<td><input type ="number" id="pbdToDayP" name ="pbdToDayP" ng-model="pbdP.pbdToDay" required min="1" ng-minlength="1" ng-maxlength="7"></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="pbdVehicleTypeP" name ="pbdVehicleTypeP" ng-model="pbdP.pbdVehicleType" ng-click="OpenPenaltyVehicleTypeDB()" readonly required></td>
			</tr>
			
			<tr>
				<td> Hour Or Day *</td>
				<td>
				<select name="pbdHourNDayP" id="pbdHourNDayP" ng-model="pbdP.pbdHourNDayP" required>
				<option value="Hour">Hour</option>
				<option value="Day">Day</option>
				</select></td>
			</tr>
			
			<!-- <tr>
				<td>P/B/D</td>
				<td><input type="text" id="pbdPenBonDetP" name ="pbdPenBonDetP" ng-model="pbdP.pbdPenBonDet"  readonly></td>
			</tr> -->
			
			<tr>
				<td>Amount *</td>
				<td><input type ="number" id="pbdAmtP" name ="pbdAmtP" ng-model="pbdP.pbdAmt" required step="0.01" min="0.01"></td>
			</tr>
			
			<tr>
				<td><input type="submit" value="Submit" id="savePenalty"  ></td>
			</tr>
		</table>
		</form>
	</div>	
	
	<div id="BonusDB" ng-hide="BonusFlag">
	<form name="BonusForm" ng-submit="savePbdForBonus(BonusForm,pbdB)">
		<table>
		
			<tr>
				<td>From Station *</td>
				<td><input type ="text" id="pbdFromStnTempB" name ="pbdFromStnTempB" ng-model="pbdB.pbdFromStnTemp" ng-click="OpenBonusFromStationDB()" readonly required></td>
				<td><input type ="hidden" id="pbdFromStnB" name ="pbdFromStnB" ng-model="pbdB.pbdFromStn" ></td>
			</tr>
		
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="pbdToStnTempB" name ="pbdToStnTempB" ng-model="pbdB.pbdToStnTemp" ng-click="OpenBonusToStationDB()" readonly required></td>
				<td><input type ="hidden" id="pbdToStnB" name ="pbdToStnB" ng-model="pbdB.pbdToStn"></td>
			</tr>
			
			<tr>
				<td>Start Date *</td>
				<td><input type ="date" id="pbdStartDtB" name ="pbdStartDtB" ng-model="pbdB.pbdStartDt" required ></td>
			</tr>
			
			<tr>
				<td>End Date *</td>
				<td><input type ="date" id="pbdEndDtB" name ="pbdEndDtB" ng-model="pbdB.pbdEndDt" required></td>
			</tr>
			
			<tr>
				<td>From Day *</td>
				<td><input type ="text" id="pbdFromDayB" name ="pbdFromDayB" ng-model="pbdB.pbdFromDay" min="1" required></td>
			</tr>
			
			<tr>
				<td>To Day *</td>
				<td><input type ="text" id="pbdToDayB" name ="pbdToDayB" ng-model="pbdB.pbdToDay" min="1" required></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="pbdVehicleTypeB" name ="pbdVehicleTypeB" ng-model="pbdB.pbdVehicleType" ng-click="OpenBonusVehicleTypeDB()" readonly required></td>
			</tr>
			
			<tr>
				<td> Hour Or Day *</td>
				<td>
				<select name="pbdHourNDayB" id="pbdHourNDayB" ng-model="pbdB.pbdHourNDay" required>
				<option value="Hour">Hour</option>
				<option value="Day">Day</option>
				</select></td>
			</tr>
			
			<!-- <tr>
				<td>P/B/D</td>
				<td><input type="text" id="pbdPenBonDetB" name ="pbdPenBonDetB" ng-model="pbdB.pbdPenBonDet"  readonly></td>
			</tr> -->
			
			<tr>
				<td>Amount *</td>
				<td><input type ="text" id="pbdAmtB" name ="pbdAmtB" ng-model="pbdB.pbdAmt" step="0.01" min="0.01" required></td>
			</tr>
			
		 	<tr>
				<td><input type="submit" value="Submit"  id="saveBonus" ></td>
			</tr>
		</table>
		</form>
	</div>	
	
	<div id="DetentionDB" ng-hide="DetentionFlag">
	<form name="DetentionForm"  ng-submit="savePbdForDetention(DetentionForm,pbdD)">
		<table>
		
			<tr>
				<td>From Station *</td>
				<td><input type ="text" id="pbdFromStnTempD" name ="pbdFromStnTempD" ng-model="pbdD.pbdFromStnTemp" ng-click="OpenDetentionFromStationDB()" readonly required></td>
				<td><input type ="hidden" id="pbdToStnD" name ="pbdToStnD" ng-model="pbdD.pbdToStn" ></td>
			</tr>
		
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="pbdToStnTempD" name ="pbdToStnTempD" ng-model="pbdD.pbdToStnTemp" ng-click="OpenDetentionToStationDB()" readonly required></td>
				<td><input type ="hidden" id="pbdToStnD" name ="pbdToStnD" ng-model="pbdD.pbdToStn"></td>
			</tr>
			
			<tr>
				<td>Start Date *</td>
				<td><input type ="date" id="pbdStartDtD" name ="pbdStartDtD" ng-model="pbdD.pbdStartDt" required></td>
			</tr>
			
			
			<tr>
				<td>End Date *</td>
				<td><input type ="date" id="pbdEndDtD" name ="pbdEndDtD" ng-model="pbdD.pbdEndDt" required></td>
			</tr>
			
			<tr>
				<td>From Day *</td>
				<td><input type ="number" id="pbdFromDayD" name ="pbdFromDayD" ng-model="pbdD.pbdFromDay" required min="1"></td>
			</tr>
			
			<tr>
				<td>To Day *</td>
				<td><input type ="number" id="pbdToDayD" name ="pbdToDayD" ng-model="pbdD.pbdToDay" required min="1"></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="pbdVehicleTypeD" name ="pbdVehicleTypeD" ng-model="pbdD.pbdVehicleType" ng-click="OpenDetentionVehicleTypeDB()" readonly required></td>
			</tr>
			
			<tr>
				<td> Hour Or Day *</td>
				<td>
				<select name="pbdHourNDayD" id="pbdHourNDayD" ng-model="pbdD.pbdHourNDay" required>
				<option value="Hour">Hour</option>
				<option value="Day">Day</option>
				</select></td>
			</tr>
			
			<!-- <tr>
				<td>P/B/D</td>
				<td><input type="text" id="pbdPenBonDetD" name ="pbdPenBonDetD" ng-model="pbdD.pbdPenBonDet" readonly> </td>
			</tr> -->
			
			<tr>
				<td>Amount *</td>
				<td><input type ="number" id="pbdAmtD" name ="pbdAmtD" ng-model="pbdD.pbdAmt" required step="0.01" min="0.01"></td>
			</tr>
			
		 	<tr>
				<td><input type="submit" value="Submit" id="saveDetention" ></td>
			</tr>
		</table>
		</form>
	</div>
	
	<div id="contToStationW" ng-hide = "contToStationFlagW">
	<form name="ContToStationFormW" ng-submit="saveContToStnW(ContToStationFormW,cts)">
		<table>
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="ctsToStnTemp" name ="ctsToStnTemp" ng-model="cts.ctsToStnTemp" ng-click="openToStationsDB()" readonly required>
				<input type ="hidden" id="ctsToStn" name ="ctsToStn" ng-model="cts.ctsToStn"></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="ctsVehicleType" name ="ctsVehicleType" ng-model="cts.ctsVehicleType"  required ng-click="openCtsVehicleType()" readonly>
				<a class="btn-floating suffix waves-effect teal" type="button" id="openVehicleTypeDB" ng-model="savVehicletype" ng-click="OpenAddVehicleTypeDB()" ><i class="mdi-content-add white-text"></i></a></td>
			</tr>
			
			<tr>
			<!-- 	<td>Product Type*</td> -->
				<td><input type ="hidden" id="ctsProductType" name ="ctsProductType" ng-model="cts.ctsProductType" readonly></td>
			</tr>
			
			<tr>
				<td>From Weight *</td>
				<td><input type ="number" id="ctsFromWt" name ="ctsFromWt" ng-model="cts.ctsFromWt"  required></td>
			</tr>
			
			<tr>
				<td>To Weight *</td>
				<td><input type ="number" id="ctsToWt" name ="ctsToWt" ng-model="cts.ctsToWt"  required></td>
			</tr>
			
			<tr>
				<td>Rate *</td>
				<td><input type ="number" id="ctsRate" name ="ctsRate" ng-model="cts.ctsRate"  required></td>
			</tr>
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>
	
	
	<div id="contToStationQ" ng-hide = "contToStationFlagQ">
	<form name="ContToStationFormQ" ng-submit="saveContToStnQ(ContToStationFormQ,cts)">
		<table>
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="ctsToStnTemp" name ="ctsToStnTemp" ng-model="cts.ctsToStnTemp" ng-click="openToStationsDB()" readonly required></td>
				<td><input type ="hidden" id="ctsToStn" name ="ctsToStn" ng-model="cts.ctsToStn"></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td class="input-field"><a class="btn-floating suffix waves-effect teal" id="openVehicleTypeDB" ng-model="savVehicletype" ng-click="OpenAddVehicleTypeDB()" ><i class="mdi-content-add white-text"></i></a>
				<input type ="text" id="ctsVehicleType" name ="ctsVehicleType" ng-model="cts.ctsVehicleType"  required ng-click="openCtsVehicleType()" readonly></td>
			</tr>
			
			<tr>
				<td>Product Type*</td>
				<td class="input-field"><a class="btn-floating suffix waves-effect teal" id="openProductTypeDB" ng-model="savProducttype" ng-click="OpenProductTypeDB1()"><i class="mdi-content-add white-text" style="font-size: 24px; margin-left: -5px;"></i></a>
				<input type ="text" id="ctsProductType" name ="ctsProductType" ng-model="cts.ctsProductType" ng-click="openProductTypeDB()" readonly required></td>
			</tr>
			
			<tr>
				<td>From Weight *</td>
				<td><input type ="number" id="ctsFromWt" name ="ctsFromWt" ng-model="cts.ctsFromWt"  required></td>
			</tr>
			
			<tr>
				<td>To Weight *</td>
				<td><input type ="number" id="ctsToWt" name ="ctsToWt" ng-model="cts.ctsToWt"  required></td>
			</tr>
			
			<tr>
				<td>Rate *</td>
				<td><input type ="number" id="ctsRate" name ="ctsRate" ng-model="cts.ctsRate"  required></td>
			</tr>
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>	
	
		<div id="addStation" ng-hide="addStationFlag">
	<form name="StationForm" ng-submit="saveNewStation(StationForm,station)">
		<table>
			<tr>
				<td>Station Name*</td>
				<td><input id="stnName" type ="text" name ="stnName" ng-model="station.stnName" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
			</tr>
			
			<tr>
				<td>Station District*</td>
				<td><input class="validate" id="district" type ="text" name ="stnDistrict" ng-model="station.stnDistrict" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
			</tr>
			
			<tr>
				<td>State Code *</td>
				<td><input class="validate" id="code" type ="text" name ="stateCode"  ng-model="station.stateCode" readonly ng-click="openStateCodeDB()" required></td>
			</tr>
			<tr>
				<td>Pin</td>
				<td><input class="validate"  type ="text" id="stnPin" name ="stnPin" ng-model="station.stnPin" ng-minlength="6" ng-maxlength="6" ng-required="true" ></td>
			</tr>
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>	
		
		
	<div ng-hide="isViewNo" class="container">
	<input type="text" name="filterTextbox" ng-model="filterTextbox.regContCode" placeholder="Search by Contract Code">
 	  <table class="table-hover table-bordered table-condensed">
 	 	<tr>
				<th></th>
				<th>Contract Code</th>
				<th>Customer Code</th>
				<th>User Code</th>
				<th>Time Stamp</th>
		</tr>
		  <tr ng-repeat="rc in ContractList  | filter:filterTextbox">
		 	  <td><input id="{{ rc.regContCode }}" type="checkbox" value="{{ rc.regContCode }}"  ng-checked="selection.indexOf(rc.regContCode) > -1" ng-click="toggleSelection(rc.regContCode)" /></td>
			      <td>{{ rc.regContCode }}</td>
	              <td>{{ rc.regContBLPMCode }}</td>
	              <td>{{ rc.userCode }}</td>
	              <td>{{ rc.creationTS }}</td>
          </tr>
         </table>
		   	<table>
		   		<tr>
					<td class="center"><input type="button" value="Submit" ng-click="verifyContracts()"></td>
				</tr>
			</table>		
		</div>
		
		<div id ="branchCodeDB" ng-hide = "branchCodeFlag">
 	 
 	 <input type="text" name="filterBranch" ng-model="filterBranch.branchCode" placeholder="Search by Branch Code"> 
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Branch Code</th>
				
				<th>Branch Name</th>
			
				<th>Branch Pin</th>
			</tr>
 	  
		  
		  <tr ng-repeat="branch in branchList | filter:filterBranch">
		 	  <td><input type="radio"  name="branchCodeName" class="branchCode"  value="{{ branch.branchCode }}" ng-model="branchCode" ng-click="savBranchCode(branch)"></td>
              <td>{{ branch.branchCode }}</td>
              <td>{{ branch.branchName }}</td>
              <td>{{ branch.branchPin }}</td>
          </tr>
         </table>
	</div>
	
	<div id ="regContBLPMCodeDB" ng-hide = "blpmCodeFlag">
	
	<input type="text" name="filterBlpm" ng-model="filterBlpm.custCode" placeholder="Search by Customer Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Customer Code</th>
				
				<th>Customer Name</th>
			
				<th>Customer Pin</th>
				
				<th>Daily Contract Allowed</th>
			</tr>
 	  
		  
		  <tr ng-repeat="blpmCode in customerList | filter:filterBlpm">
		 	  <td><input type="radio"  name="blpmCodeName"  class="custCode"  value="{{ blpmCode }}" ng-model="custCode1" ng-click="savBLPMCode(blpmCode)"></td>
              <td>{{ blpmCode.custCode }}</td>
              <td>{{ blpmCode.custName }}</td>
              <td>{{ blpmCode.custPin }}</td>
              <td>{{ blpmCode.custIsDailyContAllow }}</td>
          </tr>
         </table>
         
	</div>
	
	 <div id ="regContCngrCodeDB" ng-hide = "CngrCodeFlag">
	 
	 <input type="text" name="filterCngr" ng-model="filterCngr.custCode" placeholder="Search by Customer Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Customer Code</th>
				
				<th>Customer Name</th>
			
				<th>Customer Pin</th>
				
				<th>Daily Contract Allowed</th>
			</tr>
 	  
		  
		  <tr ng-repeat="cngrCode in customerList | filter:filterCngr">
		 	  <td><input type="radio"  name="cngrCodeName"  class="custCode"  value="{{ cngrCode }}" ng-model="custCode" ng-click="savCngrCode(cngrCode)"></td>
              <td>{{ cngrCode.custCode }}</td>
              <td>{{ cngrCode.custName }}</td>
              <td>{{ cngrCode.custPin }}</td>
              <td>{{ cngrCode.custIsDailyContAllow }}</td>
          </tr>
      </table>   
	</div>
	
	<div id ="regContCrNameDB" ng-hide = "CrNameFlag">
	
	<input type="text" name="filterCr" ng-model="filterCr.custRepCode" placeholder="Search by CR Code">
 	  
 	   	  <table>
 	 		<tr>
 	 			<th></th>
 	 			
 	 			<th>CR Code</th>
 	 			
				<th>Customer Name</th>
				
				<th>CR Name</th>
			
				<th>CR Designation</th>
			</tr>
 	 
		  
		  <tr ng-repeat="cr in crList | filter:filterCr">
		 	  <td><input type="radio"  name="cr" class="custRepCode"  value="{{ cr}}" ng-model="custRepCode" ng-click="savCrName(cr)"></td>
             <td>{{ cr.custRepCode }}</td>
              <td>{{ cr.custName }}</td>
              <td>{{ cr.custRepName }}</td>
              <td>{{ cr.custRepDesig }}</td>
          </tr>
         </table>
	</div>
	
	<div id ="regContFromStationDB" ng-hide = "FromStationFlag">
 	 
 	 <input type="text" name="filterFStn" ng-model="filterFStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	  
		  
		  <tr ng-repeat="fromStation in stationList  | filter:filterFStn">
		 	  <td><input type="radio"  name="fromStationName"  class="stnName"  value="{{ fromStation }}" ng-model="stnName"  ng-click="savFrmStnCode(fromStation)"></td>
               <td>{{ fromStation.stnCode }}</td>
              <td>{{ fromStation.stnName }}</td>
              <td>{{ fromStation.stnDistrict }}</td>
          </tr>
         </table>
	</div>
	
	<div id ="regContToStationDB" ng-hide = "ToStationFlag">
	
	 <input type="text" name="filterTStn" ng-model="filterTStn" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	  
		  
		  <tr ng-repeat="toStation in stationList | filter:filterTStn">
		 	  <td><input type="radio"  name="toStationName"  class="stnName"  value="{{ toStation }}" ng-model="stnName1" ng-click="savToStnCode(toStation)"></td>
               <td>{{ toStation.stnCode }}</td>
              <td>{{ toStation.stnName }}</td>
              <td>{{ toStation.stnDistrict }}</td>
          </tr>
         </table>
	</div>
	
	<!-- <div id ="regContVehicleTypeDB" ng-hide = "VehicleTypeFlag">
	
	<input type="text" name="filterVehicleType" ng-model="filterVehicleType.vtCode" placeholder="Search by Vehicle Type Code">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
				
				<th>Service Type</th>
			</tr>
 	  
		  
		  <tr ng-repeat="VT in vtList | filter:filterVehicleType">
		 	  <td><input type="radio"  name="VTName" class="vtCode"  value="{{ VT }}" ng-model="vtCode" ng-click="savVehicleType(VT)"></td>
              <td>{{ VT.vtCode }}</td>
              <td>{{ VT.vtVehicleType }}</td>
              <td>{{VT.vtServiceType}}</td>
          </tr>
         </table>       
	</div> -->
	
	
	
	<div id ="rbkmFromStationDB" ng-hide = "rbkmFromStationFlag">
	
	<input type="text" name="filterRbkmTStn" ng-model="filterRbkmTStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th> 
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="rbkmFromstation in stationList | filter:filterRbkmTStn">
		 	  <td><input type="radio"  name="rbkmFromstationName"  class="station"  value="{{rbkmFromstation}}" ng-model="stnCode2" ng-click="savRbkmFromStnCode(rbkmFromstation)"></td>
              	<td>{{ rbkmFromstation.stnCode }}</td>
              	<td>{{ rbkmFromstation.stnName }}</td>
              	<td>{{ rbkmFromstation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="rbkmToStationDB" ng-hide = "rbkmToStationFlag">
	
	<input type="text" name="filterRbkmTStn" ng-model="filterRbkmTStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="rbkmTostation in stationList | filter:filterRbkmTStn">
		 	  <td><input type="radio"  name="rbkmTostationName"  value="{{rbkmTostation }}" ng-model="stnCode3" ng-click="saveRbkmToStnCode(rbkmTostation)"></td>
          		<td>{{ rbkmTostation.stnCode }}</td>
              	<td>{{ rbkmTostation.stnName }}</td>
              	<td>{{ rbkmTostation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="rbkmStateCodeDB" ng-hide = "rbkmStateCodeFlag">
	
	<input type="text" name="filterRbkmState" ng-model="filterRbkmState.stateCode" placeholder="Search by State Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>State Code</th>
				
				<th>State Name</th>
			
			</tr>
 	 
		  
		  <tr ng-repeat="state in listState | filter:filterRbkmState">
		 	  <td><input type="radio"  name="stateName"  value="{{ state }}" ng-model="stateCode" ng-click="saveStateCode(state)"></td>
               <td>{{ state.stateCode }}</td>
              <td>{{ state.stateName }}</td>
          </tr>
       </table>
         
	</div>
	
	<div id ="rbkmVehicleTypeDB" ng-hide = "rbkmVehicleTypeFlag">
 	 
 	 <input type="text" name="filterRbkmVehicle" ng-model="filterRbkmVehicle.vtCode" placeholder="Search by Vehicle Type Code">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
				
				<th>Service Type</th>
			</tr>
 	  
		  
		 <tr ng-repeat="rbkmVType in vtList | filter:filterRbkmVehicle">
		 	  <td><input type="radio"  name="rbkmVTypeName"  value="{{ rbkmVType }}" ng-model="vtCode" ng-click="savRbkmVehicleType(rbkmVType)"></td>
              <td>{{ rbkmVType.vtCode }}</td>
              <td>{{ rbkmVType.vtVehicleType }}</td>
              <td>{{rbkmVType.vtServiceType}}</td>
              
          </tr>
         </table>       
	</div>
	
	<div id ="productTypeDB" ng-hide="ProductTypeFlag"> 
	
	<input type="text" name="filterProduct" ng-model="filterProduct" placeholder="Search by Product Name">
 	   	<table>
 	 		<tr>
				<th></th>
				<th>Product Name</th>
			</tr>
 	  
		  
		  <tr ng-repeat="pt in ptList  | filter:filterProduct">
		 	  <td><input type="radio"  name="ptName" class="ptName"  value="{{ pt }}" ng-model="ptName" ng-click="savProductName(pt)"></td>
              <td>{{ pt }}</td>
          </tr>
         </table>
   </div>
   
   <div id ="PenaltyFromStationDB" ng-hide = "PenaltyFromStationFlag">
 	
 	<input type="text" name="filterPenaltyFStn" ng-model="filterPenaltyFStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th> 
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="penaltyFromstation in stationList  | filter:filterPenaltyFStn">
		 	  <td><input type="radio"  name="penaltyFromstationName" class="station"  value="{{ penaltyFromstation }}" ng-model="stnCode2" ng-click="savPenaltyFromStn(penaltyFromstation)"></td>
              <td>{{ penaltyFromstation.stnCode }}</td>
              <td>{{ penaltyFromstation.stnName }}</td>
              <td>{{ penaltyFromstation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="PenaltyToStationDB" ng-hide = "PenaltyToStationFlag">
	
	<input type="text" name="filterPenaltyTStn" ng-model="filterPenaltyTStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Station Code</th>
				
				<th>Station Name</th>
				
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="penaltyTostation in stationList | filter:filterPenaltyTStn">
		 	  <td><input type="radio"  name="penaltyTostationName"  value="{{ penaltyTostation }}" ng-model="stnCode3" ng-click="savPenaltyToStn(penaltyTostation)"></td>
              <td>{{ penaltyTostation.stnCode }}</td>
              <td>{{ penaltyTostation.stnName }}</td>
              <td>{{ penaltyTostation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	

	<div id ="PenaltyVehicleTypeDB" ng-hide = "PenaltyVehicleTypeFlag">
 	 
 	 <input type="text" name="filterPenaltyVehicle" ng-model="filterPenaltyVehicle.vtCode" placeholder="Search by Vehicle Code"> 
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
			
				<th>Service Type</th>
			</tr>
 	  
		  
		 <tr ng-repeat="VtPenalty in vtList | filter:filterPenaltyVehicle">
		 	  <td><input type="radio"  name="VtPenaltyName" class="vtCode"  value="{{ VtPenalty }}" ng-model="vtCode" ng-click="savPenaltyVehicleType(VtPenalty)"></td>
              <td>{{ VtPenalty.vtCode }}</td>
              <td>{{ VtPenalty.vtVehicleType }}</td>
               <td>{{VtPenalty.vtServiceType}}</td>
             
          </tr>
         </table>       
	</div>
	
		<div id ="BonusFromStationDB" ng-hide = "BonusFromStationFlag">
		
		<input type="text" name="filterBonusFStn" ng-model="filterBonusFStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th> 
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="bonusFromstation in stationList | filter:filterBonusFStn">
		 	  <td><input type="radio"  name="bonusFromstationName" class="station"  value="{{ bonusFromstation }}" ng-model="stnCode2" ng-click="savBonusFromStn(bonusFromstation)"></td>
              <td>{{ bonusFromstation.stnCode }}</td>
              <td>{{ bonusFromstation.stnName }}</td>
              <td>{{ bonusFromstation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="BonusToStationDB" ng-hide = "BonusToStationFlag">
	
	<input type="text" name="filterBonusTStn" ng-model="filterBonusTStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="bonusTostation in stationList | filter:filterBonusTStn">
		 	  <td><input type="radio"  name="bonusTostationName"  value="{{ bonusTostation }}" ng-model="stnCode3" ng-click="savBonusToStn(bonusTostation)"></td>
              <td>{{ bonusTostation.stnCode }}</td>
              <td>{{ bonusTostation.stnName }}</td>
              <td>{{ bonusTostation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="BonusVehicleTypeDB" ng-hide = "BonusVehicleTypeFlag">
	
	<input type="text" name="filterTextbox" ng-model="filterTextbox.vtCode" placeholder="Search by Vehicle Code">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
				
				<th>Service Type</th>
			</tr>
 	  
		  
		 <tr ng-repeat="vtBonus in vtList  | filter:filterTextbox">
		 	  <td><input type="radio"  name="vtBonusName"  class="vtCode"  value="{{ vt.vtCode }}" ng-model="vtCode" ng-click="savBonusVehicleType(vtBonus)"></td>
              <td>{{ vtBonus.vtCode }}</td>
              <td>{{ vtBonus.vtVehicleType }}</td>
               <td>{{ vtBonus.vtServiceType }}</td>
              
          </tr>
         </table>       
	</div>
	
			<div id ="DetentionFromStationDB" ng-hide = "DetentionFromStationFlag">
 	  	<input type="text" name="filterDetentionFStn" ng-model="filterDetentionFStn.stnCode" placeholder="Search by Station Code">
 	  <table>
 	 		<tr>
				<th></th> 
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="detentionFromstation in stationList | filter:filterDetentionFStn">
		 	  <td><input type="radio"  name="detentionFromstationName"  class="station"  value="{{ detentionFromstation }}" ng-model="stnCode2" ng-click="savDetentionFromStn(detentionFromstation)"></td>
             <td>{{ detentionFromstation.stnCode }}</td>
              <td>{{ detentionFromstation.stnName }}</td>
              <td>{{ detentionFromstation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="DetentionToStationDB" ng-hide = "DetentionToStationFlag">
	
	<input type="text" name="filterDetentionTStn" ng-model="filterDetentionTStn.stnCode" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="detentionTostation in stationList | filter:filterDetentionTStn">
		 	  <td><input type="radio"  name="detentionTostationName"   value="{{ detentionTostation }}" ng-model="stnCode3" ng-click="savDetentionToStn(detentionTostation)"></td>
             <td>{{ detentionTostation.stnCode }}</td>
              <td>{{ detentionTostation.stnName }}</td>
              <td>{{ detentionTostation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="DetentionVehicleTypeDB" ng-hide = "DetentionVehicleTypeFlag">
 	  
 	  <input type="text" name="filterDetentionVehicle" ng-model="filterDetentionVehicle.vtCode" placeholder="Search by Vehicle Code">
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
				
				<th>Service Type</th>
			</tr>
 	  
		  
		 <tr ng-repeat="vtDetention in vtList | filter:filterDetentionVehicle">
		 	  <td><input type="radio"  name="vtDetentionName"  class="vtCode"  value="{{ vtDetention }}" ng-model="vtCode" ng-click="savDetentionVehicleType(vtDetention)"></td>
               <td>{{ vtDetention.vtCode }}</td>
              <td>{{ vtDetention.vtVehicleType }}</td>
              <td>{{ vtDetention.vtServiceType }}</td>
          </tr>
         </table>       
	</div>
	
	<div id ="ToStationsDB" ng-hide ="ToStationsDBFlag">
 	  	<input type="text" name="filterToStns" ng-model="filterToStns.stnCode" placeholder="Search by Station Code">
 	  <table>
 	 		<tr>
				<th></th> 
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="toStations in stationList | filter:filterToStns">
		 	  <td><input type="radio"  name="toStations"  class="station"  value="{{ toStations }}" ng-model="toStns" ng-click="saveToStations(toStations)"></td>
             <td>{{ toStations.stnCode }}</td>
              <td>{{ toStations.stnName }}</td>
              <td>{{ toStations.stnDistrict }}</td>
          </tr>
         </table>
	</div>
	
	<div id ="ctsVehicleTypeDB" ng-hide = "VehicleTypeFlag">
	
	<input type="text" name="filterVehicleType" ng-model="filterVehicleType.vtCode" placeholder="Search by Vehicle Type Code">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
				
				<th>Service Type</th>
			</tr>
 	  
		  
		  <tr ng-repeat="VT in vtList | filter:filterVehicleType">
		 	  <td><input type="radio"  name="VTName" class="vtCode"  value="{{ VT }}" ng-model="vtCode" ng-click="savVehicleType(VT)"></td>
              <td>{{ VT.vtCode }}</td>
              <td>{{ VT.vtVehicleType }}</td>
              <td>{{VT.vtServiceType}}</td>
          </tr>
         </table>       
	</div>
	
	<div id ="StateCodeDB" ng-hide="StateCodeFlag">
	<input type="text" name="filterState" ng-model="filterState.stateCode" placeholder="Search by State Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>State Code</th>
			</tr>
 	 	<tr ng-repeat="statecode in listState  | filter:filterState">
		 	  <td><input type="radio"  name="statecode"  value="{{ statecode }}" ng-model="stateCode1" ng-click="savStateCode(statecode)"></td>
               <td>{{ statecode.stateCode }}</td>
          </tr>
         </table> 
	</div>
	
	
	<div id ="OldRegularContractDB" ng-hide ="OldRegularContractFlag">
	
	<input type="text" name="filterOldRegularContract" ng-model="filterOldRegularContract.creationTS" placeholder="Search by Time">
 	   	  <table>
 	 		<tr>
				<th></th>
			
				<th>Creation Time</th>
			</tr>
		 <tr ng-repeat="oldRC in regCntOld  | filter:filterOldRegularContract">
		 	  <td><input type="radio"  name="oldRCName" class="oldRCClass"  value="{{ oldRC }}" ng-model="oldRCTime" ng-click="saveOldRC(oldRC)"></td>
              <td>{{ oldRC.creationTS | date : 'medium' }}</td>
          </tr>
         </table>       
	</div>
	
	
	<div id="regContDB" ng-hide = "regContFlag">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div> 
	<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
	<table class="table-hover table-bordered table-bordered">
		<tr>
			<td><h3>Regular Contract Details</h3></td>
		</tr>
		<tr>
			<td>Branch Code</td>
			<td>{{regCnt.branchCode}}</td>
		</tr>
		
		<tr>
			<td>BLPM Code</td>
			<td>{{regCnt.regContBLPMCode}}</td>
		</tr>
		
		<tr>
			<td>Consignor Code</td>
			<td>{{regCnt.regContCngrCode}}</td>
		</tr>
		
		<tr>
			<td>CR Name</td>
			<td>{{regCnt.regContCrName}}</td>
		</tr>
		
		<tr>
			<td>From Station</td>
			<td>{{regCnt.regContFromStation}}</td>
		</tr>
		
		<tr>
			<td>To Station</td>
			<td>{{regCnt.regContToStation}}</td>
		</tr>
		
		<tr>
			<td>Start Date</td>
			<td>{{regCnt.regContFromDt}}</td>
		</tr>
		
		<tr>
			<td>End Date</td>
			<td>{{regCnt.regContToDt}}</td>
		</tr>
		
		<tr>
		<td>From Weight</td>
			<td>{{regCnt.regContFromWt}}</td>
		</tr>
		
		<tr>
			<td>To Weight</td>
			<td>{{regCnt.regContToWt}}</td>
		</tr>
		
		<tr>
			<td>Rate</td>
			<td>{{regCnt.regContRate}}</td>
		</tr>
		
		<tr>
			<td>DC</td>
			<td>{{regCnt.regContDc}}</td>
		</tr>
		
		<tr>
			<td>Cost Grade</td>
			<td>{{regCnt.regContCostGrade}}</td>
		</tr>
		
		<tr>
			<td>DDL</td>
			<td>{{regCnt.regContDdl}}</td>
		</tr>
		
		<tr>
			<td>Additional Rate</td>
			<td>{{regCnt.regContAdditionalRate}}</td>
		</tr>
		
		<tr>
			<td>Product Type</td>
			<td>{{regCnt.regContProductType}}</td>
		</tr>
		
		<tr>
			<td>Vehicle Type</td>
			<td>{{regCnt.regContVehicleType}}</td>
		</tr>
		
		<tr>
			<td>Load</td>
			<td>{{regCnt.regContLoad}}</td>
		</tr>
		
		<tr>
			<td>Unload</td>
			<td>{{regCnt.regContUnLoad}}</td>
		</tr>
		
		<tr>
			<td>Transit Day</td>
			<td>{{regCnt.regContTransitDay}}</td>
		</tr>
		
		<tr>
			<td>Statistical Charge</td>
			<td>{{regCnt.regContStatisticalCharge}}</td>
		</tr>
		
		<tr>
			<td>Value</td>
			<td>{{regCnt.regContValue}}</td>
		</tr>
		
		<tr>
			<td>Renew</td>
			<td>{{regCnt.regContRenew}}</td>
		</tr>
		
		<tr>
			<td>Renew Date</td>
			<td>{{regCnt.regContRenewDt}}</td>
		</tr>
		
		<tr>
			<td>Insure By</td>
			<td>{{regCnt.regContInsuredBy}}</td>
		</tr>
		
		<tr>
			<td>Insure Company</td>
			<td>{{regCnt.regContInsureComp}}</td>
		</tr>
		
		<tr>
			<td>Insure Unit Number</td>
			<td>{{regCnt.regContInsureUnitNo}}</td>
		</tr>
		
		<tr>
			<td>Remarks</td>
			<td>{{regCnt.regContRemark}}</td>
		</tr>
	</table>
	
		<div class="row"  ng-show="rbkmFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		    	 <tr><td><h5>RBKM</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>State Code</th>
		            <th>From Km</th>
		            <th>To Km</th>
		            <th>Vehicle Type</th>
		            <th>Rate</th>   
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="rbkm in rbkmList">
		            <td>{{rbkm.rbkmFromStation}}</td>
		            <td>{{rbkm.rbkmToStation}}</td>
		            <td>{{rbkm.rbkmStateCode}}</td>
		            <td>{{rbkm.rbkmFromKm}}</td>
		            <td>{{rbkm.rbkmToKm}}</td>
		            <td>{{rbkm.rbkmVehicleType}}</td>
		            <td>{{rbkm.rbkmRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
	</div>
		
	
	
	
			<div class="row" ng-show="pbdFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		    	 <tr><td><h5>PBD</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Start Date</th>
		            <th>End Date</th>
		            <th>From Dlay</th>
		            <th>To Dlay</th>
		            <th>Vehicle Type</th>
		            <th>P/B/D</th>
		            <th>Hour N Day</th>
		            <th>Amount</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="pbd in pbdList">
		            
		            <td>{{pbd.pbdFromStn}}</td>
		            <td>{{pbd.pbdToStn}}</td>
		            <td>{{pbd.pbdStartDt}}</td>
		            <td>{{pbd.pbdEndDt}}</td>
		            <td>{{pbd.pbdFromDay}}</td>
		            <td>{{pbd.pbdToDay}}</td>
		            <td>{{pbd.pbdVehicleType}}</td>
		            <td>{{pbd.pbdPenBonDet}}</td>
		            <td>{{pbd.pbdHourNDay}}</td>
		            <td>{{pbd.pbdAmt}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
	
			<div class="row" ng-show="newPbdFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		    	 <tr><td><h5>PBD</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Start Date</th>
		            <th>End Date</th>
		            <th>From Dlay</th>
		            <th>To Dlay</th>
		            <th>Vehicle Type</th>
		            <th>P/B/D</th>
		            <th>Hour N Day</th>
		            <th>Amount</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="pbd in newPbdList">
		            
		            <td>{{pbd.pbdFromStn}}</td>
		            <td>{{pbd.pbdToStn}}</td>
		            <td>{{pbd.pbdStartDt}}</td>
		            <td>{{pbd.pbdEndDt}}</td>
		            <td>{{pbd.pbdFromDay}}</td>
		            <td>{{pbd.pbdToDay}}</td>
		            <td>{{pbd.pbdVehicleType}}</td>
		            <td>{{pbd.pbdPenBonDet}}</td>
		            <td>{{pbd.pbdHourNDay}}</td>
		            <td>{{pbd.pbdAmt}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
		<div class="row"  ng-show="newRbkmFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		     <tr><td><h5>RBKM</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>State Code</th>
		            <th>From Km</th>
		            <th>To Km</th>
		            <th>Vehicle Type</th>
		            <th>Rate</th>   
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="rbkm in rateByKmList">
		            <td>{{rbkm.rbkmFromStation}}</td>
		            <td>{{rbkm.rbkmToStation}}</td>
		            <td>{{rbkm.rbkmStateCode}}</td>
		            <td>{{rbkm.rbkmFromKm}}</td>
		            <td>{{rbkm.rbkmToKm}}</td>
		            <td>{{rbkm.rbkmVehicleType}}</td>
		            <td>{{rbkm.rbkmRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
	</div>
	
	         
               	<div class="row" ng-show="newCtsFlagW" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);overflow:auto;" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in ctsList">
					<td>{{regCnt.regContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		          	<td>{{cts.ctsRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
		<div class="row" ng-show="ctsFlagW" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in oldCtsList">
					<td>{{regCnt.regContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		          	<td>{{cts.ctsRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
		
		   	<div class="row" ng-show="ctsFlagQ" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Product Type</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in oldCtsList">
					<td>{{regCnt.regContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		            <td>{{cts.ctsProductType}}</td>
		          	<td>{{cts.ctsRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
   		
   		   	<div class="row" ng-show="newCtsFlagQ" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Product Type</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in ctsList">
					<td>{{regCnt.regContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		            <td>{{cts.ctsProductType}}</td>
		          	<td>{{cts.ctsRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
              
		
		<input type="button" value="Cancel" ng-click="back()">
		<input type="button" value="Save" ng-click="saveContract(regCnt,tnc,metricType)"> 
		
	</div>
	</div>
	
			<div class="row" ng-hide = "oldregCont">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
	<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
		<h4>Here's the details of Daily Contract <span class="teal-text text-lighten-2">{{oldCont.regContCode}}</span>:</h4>
		<table class="table-hover table-bordered table-bordered">
				<tr>
	                <td>Contract Code:</td>
	                <td>{{oldCont.regContCode}}</td>
	            </tr>
	            <tr>
	                <td>Contract BLPM Code:</td>
	                <td>{{oldCont.regContBLPMCode}}</td>
	            </tr>
	            <tr>
	                <td>Branch Code:</td>
	                <td>{{oldCont.branchCode}}</td>
	            </tr>
	            <tr>
	                <td>Consignor Code:</td>
	                <td>{{oldCont.regContCngrCode}}</td>
	            </tr>
	            <tr>
	                <td>CR Name:</td>
	                <td>{{oldCont.regContCrName}}</td>
	            </tr>
	            <tr>
	                <td>From Station:</td>
	                <td>{{oldCont.regContFromStation}}</td>
	            </tr>
	            <tr>
	                <td>To Station:</td>
	                <td>{{oldCont.regContToStation}}</td>
	            </tr>
	            <tr>
	                <td>Contract Rate:</td>
	                <td>{{oldCont.regContRate}}</td>
	            </tr>
	            <tr>
	                <td>Vehicle Type:</td>
	                <td>{{oldCont.regContVehicleType}}</td>
	            </tr>
	            <tr>
	                <td>From Date:</td>
	                <td>{{oldCont.regContFromDt}}</td>
	            </tr>
	            <tr>
	                <td>To Date:</td>
	                <td>{{oldCont.regContToDt}}</td>
	            </tr>
	            <tr>
	                <td>Contract Type:</td>
	                <td>{{oldCont.regContType}}</td>
	            </tr>
	            <tr>
	                <td>From Weight:</td>
	                <td>{{oldCont.regContFromWt}}</td>
	            </tr>
	            <tr>
	                <td>To Weight:</td>
	                <td>{{oldCont.regContToWt}}</td>
	            </tr>
	            <tr>
	                <td>Product Type:</td>
	                <td>{{oldCont.regContProductType}}</td>
	            </tr>
	            <tr>
	                <td>Transit Day:</td>
	                <td>{{oldCont.regContTransitDay}}</td>
	            </tr>
	            <tr>
	                <td>Statistical Charge:</td>
	                <td>{{oldCont.regContStatisticalCharge}}</td>
	            </tr>
	            <tr>
	                <td>Contract Load:</td>
	                <td>{{oldCont.regContLoad}}</td>
	            </tr>
	            <tr>
	                <td>Contract UnLoad:</td>
	                <td>{{oldCont.regContUnLoad}}</td>
	            </tr>
	            
	            <tr>
	                <td> Proportionate:</td>
	                <td>{{oldCont.regContProportionate}}</td>
	            </tr>
	            
	            <tr>
	                <td>Additional Rate:</td>
	                <td>{{oldCont.regContAdditionalRate}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract Value:</td>
	                <td>{{oldCont.regContValue}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract DC:</td>
	                <td>{{oldCont.regContDc}}</td>
	            </tr>
	            
	            <tr>
	                <td>Cost Grade:</td>
	                <td>{{oldCont.regContCostGrade}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract Weight:</td>
	                <td>{{oldCont.regContWt}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract DDL:</td>
	                <td>{{oldCont.regContDdl}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract Renew:</td>
	                <td>{{oldCont.regContRenew}}</td>
	            </tr>
	            
	            <tr>
	                <td>Contract Renew Date:</td>
	                <td>{{oldCont.regContRenewDt}}</td>
	            </tr>
	            
	            <tr>
	                <td>Insured By:</td>
	                <td>{{oldCont.regContInsuredBy}}</td>
	            </tr>
	            
	            <tr>
	                <td>Insure Company:</td>
	                <td>{{oldCont.regContInsureComp}}</td>
	            </tr>
	            
	            <tr>
	                <td>Insure Unit Number:</td>
	                <td>{{oldCont.regContInsureUnitNo}}</td>
	            </tr>
	            
	            <tr>
	                <td>Insure Policy Number:</td>
	                <td>{{oldCont.regContInsurePolicyNo}}</td>
	            </tr>
	            
	            <tr>
	                <td>Creation Time</td>
	                <td>{{oldCont.creationTS}}</td>
	            </tr>
	            
	            <tr>
	                <td>Remarks:</td>
	                <td>{{oldCont.regContRemark}}</td>
	            </tr>   
	         </table>
		</div>
	</div>
	
	</div>
<div ng-show="operatorLogin || superAdminLogin">
	<title>All Images </title>
		<div class="row">			
			<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="ImgForm" ng-submit="SubmitImg(ImgForm)">	
				<div class="row">      			
      				<div class="input-field col s3">
        				<input class="validate" id="branchCode" type ="text" name ="branchCode" ng-click="OpenBranchDataDB()" ng-model="branchCode" ng-required="true" readonly="readonly">
        				<label for="BrokerName">Branch</label>	
      				</div>
	      			<div class="input-field col s3">
		        		<select name="imgType" id="imgType" ng-model="imgType" ng-required="true">
		                    <option value="CNMT">CNMT</option>
		                </select>
		                <label>Type</label>	
	      			</div>
	      			<div class="input-field col s3">
	      				<input class="validate" id="code" type ="text" name ="code" ng-model="code">
		                <label>CNMT No.</label>	
	      			</div>
	      			<div class="col s2">
      			 		<input type="submit" value="Submit" id="imgSubmit">
      			 	</div>
      			</div>      			
      		</form>
 		</div>
 		
 		
 		<!-- Brnach Dialog Box -->

		<div id ="branchCodeDB" ng-hide = "branchCodeFlag">
			<input type="text" name="filterBranchCode" ng-model="filterBranchCode" placeholder="Search" />
	 		<table>
 	 			<tr>
					<th></th>
					<th>Branch FaCode</th>
					<th>Branch Name</th>
				</tr>
				<tr ng-repeat="branch in branchList | filter:filterBranchCode">
			  		<td><input type="radio"  class="branchCode"  value="{{ branch.branchCode }}" ng-click="savBranchCode(branch)"></td>
        	   		<td>{{ branch.branchFaCode }}</td>
             		<td>{{ branch.branchName }}</td>
          		</tr>
         	</table>
		</div>
		
		
		<!-- notValidPanDecList -->
	<div class="row" ng-hide="detailTableFlag">
		<table class="tblrow" >
       		<caption class="coltag tblrow">{{imgType}} Detail</caption> 
            <tr class="rowclr">
            	<th class="colclr">S.No</th>
            	<th class="colclr">{{imgType}}</th>
            	<th class="colclr">Imgage</th>
            	<th></th>
            	<th>Action</th>
	        </tr>	
	        <tr ng-repeat="obj in list">
	        	<td class="rowcel">{{$index+1}}</td>
	        	<td class="rowcel">{{obj.code}}</td>
	        	<td class="rowcel">{{obj.isAvailable}}</td>
	        	<td class="rowcel">
	        		<input type="file" id="image{{$index}}" class="teal white-text btn" name="image{{$index}}"  accept="application/pdf" />
	        	</td>
	        	<td class="rowcel">
	        		<input type="submit" id="submit{{$index}}" value="UPLOAD" ng-click="uploadFile(obj.imgId, $index)">
	        	</td>
	        </tr>
    	</table>
	</div>
	
</div>
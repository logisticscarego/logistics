<div ng-show="operatorLogin || superAdminLogin">
<title>Product Type</title>

<!-- <form name="ProductTypeForm" ng-click="Submit(ProductTypeForm,producttype)"> -->
<div class="row">
	<div class="col s3 hide-on-med-and-down"> &nbsp; </div>
		<form class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="ProductTypeForm" ng-submit="Submit(ProductTypeForm,producttype)">
	
			<div class="input-field col s6">
   				<input type ="text" id="ptName" name ="ptName" ng-model="producttype.ptName"  ng-required="true">
   				<label>Product Name</label>
 			</div>
			
      			 	<div class="input-field col s6 center">
      			 		<input type="submit" value="Submit">
      			 	</div>
			
		</form>
		<div class="col s3"> &nbsp; </div>
</div>	

<div id="viewPTDB" ng-hide="viewPTDetailsFlag">

<div class="row">
	
	<div ng-hide = "show" class="col s12 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
				<h4>Here's the details of Product Type <span class="teal-text text-lighten-2">{{producttype.ptName}}</span>:</h4>
		<table class="table-bordered table-hover table-condensed">
	            
	            <tr>
	                <td>Product name:</td>
	                <td>{{producttype.ptName}}</td>
	            </tr>
	            
	      </table>
	      
<input type="button" value="Save" ng-click="saveProductType(producttype)">
<input type="button" value="Cancel" ng-click="closePTDetailsDB()">
	      
	  </div>
 </div>         
</div>
</div>


<!-- 
<form name="ProductTypeForm" ng-click="Submit(ProductTypeForm,producttype)">
<table>
	<tr>
		<td>Product Name:</td> 
		<td><input type ="text" name ="ptName" ng-model="producttype.ptName" ng-minlength="3" ng-maxlength="40" ng-required="true"></td>
	</tr>
	
	<tr>
		<td><input type="submit" value="Submit" ></td>
	</tr>
	
</table>
</form>

 -->
<div ng-show="adminLogin || superAdminLogin">

<title>Display Cnmt</title>
<div class="container">
	<div class="row">
		<div class="col s2">
			<input type="radio" name="biltyList" ng-model="biltyByModal"
				ng-click="enableModalTextBox()">
		</div>
		<div class="col s10">
			<input type="text" id="biltyCodeByMId" name="biltyByM"
				ng-model="biltyCodeByM" disabled="disabled"
				ng-click="OpenBiltyCodeDB()">
		</div>
	</div>

	<div class="row">
		<div class="col s2">
			<input type="radio" name="biltyList" ng-model="biltyByAuto"
				ng-click="enableAutoTextBox()">
		</div>
		<div class="col s8">
			<input type="text" id="biltyCodeByAId" name="biltyByA"
				ng-model="biltyCodeByA" disabled="disabled"
				ng-keyup="getBiltyCodeList()">
		</div>
		<div class="col s2">
			<input class="col s12" type="button" id="ok" value="OK"
				disabled="disabled" ng-click="getBiltyList()">
		</div>
	</div>

	<div class="row">
		<div class="col s2">
			<input id="bkToList" type="button" value="Back To list"
				ng-click="backToList()">
		</div>
	</div>
</div>

<div id="biltyCodeDB" ng-hide="biltyCodeFlag">
	<input type="text" name="filterBiltyCode" ng-model="filterBiltyCode"
		placeholder="Search by Bilty Code">
	<table>
		<tr ng-repeat="biltyCode in cnmtCodeList | filter:filterBiltyCode">
			<td><input type="radio" name="biltyCodeName"
				value="{{ biltyCode }}" ng-model="contCodes"
				ng-click="saveBiltyCode(biltyCode)"></td>
			<td>{{ biltyCode}}</td>
		</tr>
	</table>
</div>

<div ng-hide="isViewNo" id="CnmtTableDB" class="container">
	<input type="text" name="filterCnmt" ng-model="filterCnmt.cnmtCode"
		placeholder="Search by Bilty Code">

	<table class="table-hover table-bordered table-condensed">
		<tr>
			<th></th>
			<th>Bilty Code</th>
			<th>Contract Code</th>
			<th>Customer Code</th>
			<th>User Code</th>
			<th>Time Stamp</th>
		</tr>

		<tr ng-repeat="bilty in BiltyList | filter:filterCnmt">
			<td><input id="{{ bilty.cnmtCode }}" type="checkbox"
				value="{{ bilty.cnmtCode }}"
				ng-checked="selection.indexOf(bilty.cnmtCode) > -1"
				ng-click="toggleSelection(bilty.cnmtCode)" /></td>
			<td>{{ bilty.cnmtCode }}</td>
			<td>{{ bilty.contractCode }}</td>
			<td>{{ bilty.custCode }}</td>
			<td>{{ bilty.userCode }}</td>
			<td>{{ bilty.creationTS }}</td>
		</tr>



	</table>
	<table>
		<tr>
			<td class="center"><div class="col s12 center">
					<input type="submit" value="Submit" ng-click="verifyBilty()">
				</div>
				</div></td>
		</tr>
	</table>
</div>

<div ng-hide="showCnmt">
	<div class="container">
		<div class="row">
			<div class="input-field col s12 m4 l3">
				<input type="text" name="cnmtCode" ng-model="cnmt.cnmtCode"
					value="{{cnmt.cnmtCode }}" readonly><label>CNMT
					Code</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" name="custCode" id="custCode"
					ng-model="cnmt.custCode" value="{{cnmt.custCode }}"><label>Customer
					Code</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" ng-model="cnmt.branchCode"
					value="{{ cnmt.branchCode}}"><label>Branch Code</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" name="cnmtDt" ng-model="cnmt.cnmtDt"
					value="{{cnmt.cnmtDt }}"><label>CNMT date</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" ng-model="cnmt.cnmtConsignor"
					value="{{ cnmt.cnmtConsignor}}"><label>CNMT
					Consignor</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" ng-model="cnmt.cnmtConsignee"
					value="{{ cnmt.cnmtConsignee}}"><label>CNMT
					Consignee</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" ng-model="cnmt.contractCode"
					value="{{cnmt.contractCode }}"><label>Contract Code</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" ng-model="cnmt.cnmtDC" value="{{cnmt.cnmtDC }}"><label>CNMT
					DC</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" name="cnmtNoOfPkg" ng-model="cnmt.cnmtNoOfPkg"
					value="{{cnmt.cnmtNoOfPkg }}"><label>CNMT No. of
					Packages</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="number" name="cnmtActualWt"
					ng-model="cnmt.cnmtActualWt" value="{{cnmt.cnmtActualWt }}"><label>CNMT
					Actual Weight</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="number" name="cnmtGuaranteeWt"
					ng-model="cnmt.cnmtGuaranteeWt" value="{{ cnmt.cnmtGuaranteeWt}}"><label>CNMT
					Guarantee Weight</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" name="cnmtDDL" ng-model="cnmt.cnmtDDL"
					value="{{ cnmt.cnmtDDL}}"><label>CNMT Door-to-Door
					Delivery</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" name="cnmtPayAt" id="cnmtPayAt"
					ng-model="cnmt.cnmtPayAt" value="{{cnmt.cnmtPayAt}}"><label>CNMT
					Pay At</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" name="cnmtBillAt" id="cnmtBillAt"
					ng-model="cnmt.cnmtBillAt" value="{{cnmt.cnmtBillAt}}"><label>CNMT
					Bill At</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" name="cnmtRate" ng-model="cnmt.cnmtRate"
					value="{{cnmt.cnmtRate}}"><label>CNMT Rate</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" name="cnmtFreight" ng-model="cnmt.cnmtFreight"
					value="{{ cnmt.cnmtFreight}}"><label>CNMT Freight</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" name="cnmtVOG" ng-model="cnmt.cnmtVOG"
					value="{{cnmt.cnmtVOG }}"><label>CNMT Value of Good
					(Rs.)</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" name="cnmtExtraExp" ng-model="cnmt.cnmtExtraExp"
					value="{{cnmt.cnmtExtraExp }}"><label> CNMT Extra
					Expenses (Rs.)</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" name="cnmtCostGrade"
					ng-model="cnmt.cnmtCostGrade" value="{{ cnmt.cnmtCostGrade}}"><label>CNMT
					Cost Grade</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" name="cnmtDtOfDly" ng-model="cnmt.cnmtDtOfDly"
					value="{{ cnmt.cnmtDtOfDly}}"><label>CNMT text of
					Delivery</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" name="cnmtEmpCode" ng-model="cnmt.cnmtEmpCode"
					value="{{ cnmt.cnmtEmpCode}}"><label>CNMT Employee
					Code</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" name="cnmtKm" ng-model="cnmt.cnmtKm"
					value="{{cnmt.cnmtKm }}"><label>CNMT Kilometer</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" name="cnmtVehicleType"
					ng-model="cnmt.cnmtVehicleType" value="{{cnmt.cnmtVehicleType }}"><label>CNMT
					Vehicle Type</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" name="cnmtState" ng-model="cnmt.cnmtState"
					value="{{cnmt.cnmtState}}"><label>CNMT State</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" name="cnmtInvoiceNo"
					ng-model="cnmt.cnmtInvoiceNo" value="{{cnmt.cnmtInvoiceNo}}"><label>CNMT
					Invoice Number</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<select name="cnmtBillPermission" ng-model="cnmt.cnmtBillPermission"
					required>
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
				<label for="cnmtBillPermission">CNMT Bill Permission</label>
			</div>

		</div>
		<div class="row">
			<div class="col s12 center">
				<input type="button" value="Submit" ng-click="EditBiltySubmit(cnmt)">
			</div>
		</div>
	</div>

	<form method="post" action="downloadCnmt" enctype="multipart/form-data">
		<table>
			<tr>
				<td><input type="hidden" id="submitImageId"
					name="submitImageName" value="{{ cnmt.cnmtCode }}" size="50" /></td>
			</tr>
			<tr>
				<td><input type="submit" id="submitImage"
					value="download Image" disabled="disabled" /></td>
			</tr>
		</table>
	</form>


	<form method="post" action="downloadConfCnmt"
		enctype="multipart/form-data">
		<table>
			<tr>
				<td><input type="hidden" id="submitConfImageId"
					name="submitConfImageName" value="{{ cnmt.cnmtCode }}" size="50" /></td>
			</tr>
			<tr>
				<td><input type="submit" id="submitConfImage"
					value="download Final Image" disabled="disabled" /></td>
			</tr>
		</table>
	</form>

</div>
</div>
<style type="text/css">
            .printable {display: none;}
            @media print {
                .noprint, .menuwrapper, .ui-dialog, footer, #toast-container {display: none !important;}
                .printable {display: block;
                		width: 11in !important;
                		position: fixed;
                		top: 0px;
                		left: 0px;
                		z-index: 999999;
                		}
                		
                		body {
						    width: 11in;
						    height: 210in;
						     
						  }
            }
            @PAGE {
				size: A4;
				margin:0.5cm 0.4cm 0.5cm 0.5cm;
			}

}

 
   .divTable
    {
        display:  table;
        width:auto;
        border:1px solid #000;
        border-spacing:0px;/*cellspacing:poor IE support for  this*/
       /* border-collapse:separate;*/
    }

    .divRow
    {
       display:table-row;
	   border:10px solid #0CC;
       width:auto;
	   
    }

     .divCell04
    {
        float:left;/*fix for  buggy browsers*/
        display:table-column;
        width:620px;
        background-color:#ccc;
		border: 1px solid #000; text-align:center;
	
    }
   
.divCell06
   {
       float:left;/*fix for  buggy browsers*/
       display:table-column;
       width:436px;
       background-color:#ccc;
        text-align:center;
         border: 1px solid #000;
    
   }
   
   .divCell066
   {
       float:left;/*fix for  buggy browsers*/
       display:table-column;
       width:41.2%;
       background-color:#ccc;
        border: 0px solid #000; text-align:center;
    
   }


.divCell21
    {
        float:left;/*fix for  buggy browsers*/
        display:table-column;
        width:100.1%;
        height:auto;
        background-color:#ccc;
		border: 1px solid #000;
	
    }

   
   .divCell22 {
	float: left; /*fix for  buggy browsers*/
	display: table-column;
	width: 351px;
	height: auto;;
	background-color: #ccc; 
	text-align:center;
	border: 0px solid black;
    }
   

.divCell1
    {
        float:left;/*fix for  buggy browsers*/
        display:table-column;
        width:50%;
        background-color:#ccc;
		border: 0px solid #000;
		height:106px;
    }


.divCell2
    {
        float:left;/*fix for  buggy browsers*/
        display:table-column;
        width:50%;
        background-color:#ccc;
		border: 0px solid #000;
		height:106px;
    }

.CSSTableGenerator {
	padding: 0px;
	width: 100%;
	border: 0px solid #000000;
	border-top-left-radius: 0px;
}

.CSSTableGenerator table {
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
	height: 100%;
	margin: 0px;
	padding: 0px;
	
}

.CSSTableGenerator td {
	vertical-align: middle;
	border: 1px solid #000000;
	text-align: center;
	padding: 3px;
	font-size: 10px;
	font-family: Arial;
	font-weight: normal;
	color: #000000;
}

.CSSTableGenerator tr:first-child td {
	background-color: #ff7f00;
	border: 0px solid #000000;
	text-align: center;
	border-width: 1px 1px 1px 1px;
	font-size: 14px;
	font-family: Arial;
	font-weight: bold;
	color: #ffffff;
}



.bold{font-weight:bold; font-size:12px;}
.bold1{font-weight:bold; font-size:16px; font-family: 'Corbel';}
.bold2{font-size:13px;}
.padding{padding:8px;}
.padding1{padding:15px;}
.padding2{padding:22px;}
.padding3{padding:10px; text-align:center;}
.hr {width:130px;
    margin-left: auto;
    margin-right: auto;
    background-color:#FF0066;
    color:#FF0066;
    margin-top:-10px;
    border: 1px solid black ;
   }



    .divbdr{border-right:0px solid #000; border-top:1px solid #000; border-left:1px solid #000;}
	.divbdr1{border-right:1px solid #000; border-top:1px solid #000; border-left:0px solid #000;}


</style>



<div ng-show="operatorLogin || superAdminLogin">
<div class="noprint">
<div ng-show="loadingFlag" style=" text-align:center; margin-left: -15px; margin-top: -50px; position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
        <div style="margin-left:20px; margin-top:100px;">
           <svg width='200px' height='200px'  viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-gears">
             <rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect>
                <g transform="translate(-20,-20)">
                      <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#2bbbad ">
                         <animateTransform attributeName="transform" type="rotate" from="90 50 50" to="0 50 50" dur="0.8s" repeatCount="indefinite"></animateTransform>
                       </path>
                 </g>
                 <g transform="translate(20,20) rotate(15 50 50)">
                     <path d="M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z" fill="#0A3F4E ">
                          <animateTransform attributeName="transform" type="rotate" from="0 50 50" to="90 50 50" dur="0.8s" repeatCount="indefinite">
                          </animateTransform>
                      </path>
                  </g>
            </svg>
        </div>
       <span style=" color: #ffffff ;font-size:30px; font-weight:bold; letter-spacing:3px;">Loading...</span>
</div>
</div>
	<div class="noprint">
	<div class="row">
	<div class="col s1 hide-on-med-and-down">&nbsp;</div>
	<div class="col s10 card"
		style="align: center; margin-top: 40px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">

		<div class="input-field col s6">
			<input type="text" id="bfNoId" name="bfNoName" ng-model="bfNo" required/> 
			<label>BillForwarding No.</label>
		</div>
		
		<div class="input-field col s4">
			<input type="date" id="bfDtId" name="bfDt" ng-model="bfDt" /> 
			<label>BillForwarding Date change</label>
		</div>

		<div class="input-field col s6 center">
			<input class="btn waves-effect waves-light" type="submit" value="Submit" ng-click="getBFPrint()">
		</div>
	</div>
	
	
	
	</div>
</div>
	
	
	<div id="printVsDB" ng-hide="printVsFlag" class="noprint">
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">{{PRINT_STMT}}</h5></div>
		</div>
		<div>
			<div class="col s12 center">
				<a class="btn white-text" ng-click="cancelPrint()">Cancel</a>
				<a class="btn white-text" ng-click="printVs()">Yes</a>
			</div>
		</div>
	</div>
	
	
	<div class="printable"> 
             <div class="headRow">
                <div class="divCell1 divbdr"><img src="resources/img/logo.png" width="357" height="106" /></div>
                <div  class="divCell1 divbdr1">
                          <div style="font-size: 16px; font-weight: bold; font-family: Arial;">Care Go Logistics Pvt. Ltd.</div>
                          <div style="font-size: 12px">Billing Office: S-405, Greater Kailash, Part-II,New Delhi-110048</div>
<!--                           <div style="font-size: 12px">Phone:{{ bfMap.brhPhNo }}</div>  -->
                          <div style="font-size: 12px">Email: gurgaon@carego.co</div></div>
             </div>
          
            <div class="divRow">
                  <div class="divCell04 bold">
                  <div class="divCell04 bold" style="border:0px; margin-left:10px; text-align:left;">To, M/s </div>
                
                  <div class="divCell04 bold" style="border:0px; margin-top:-15px; margin-left:100px; text-align:justify;">{{bfMap.custName}}({{bfMap.bf.bfCustId}})
                  <br>{{bfMap.custAdd}},<br>{{bfMap.custCity}} {{bfMap.custPin}}</div>
                  
                  </div>
               <div class="divCell066 bold">
                   <div class="divCell06 bold">
                      <div style="margin-top:2px; text-align:middle; margin-left:20px;"> BF. NO.: {{bfMap.bf.bfNo}}</div><br>
                      <div style="text-align:middle;">DATE: {{bfDt | date:'dd/MM/yyyy'}}</div>
                   </div>
               </div>
            </div>
            
            
             <div class="divRow">
               <div  style="border:1px solid black;">
                  <div class="bold2" style="border:0px; margin-left:10px;">Dear Sir,</div>
                  <div class="bold2" style="border:0px; margin-left:10px;">
                   We are  please to enclose our bills for transportation of goods in your account as per details given below. We request you to kindly process the same and release payment at the earliest.
                 </div>
               </div>  
            </div>
            
       
            <div class="CSSTableGenerator">
					<table>
						<tr> 
							<td width="50px">S.NO.</td>
							<td width="300px">BILL NO.</td>
							<td width="300px">DATE</td>
							<td width="300px">AMOUNT (Rs.)</td>
						</tr>

						<tr ng-repeat="bill in bfMap.blList">
							<td>{{ $index+1 }}</td>
							<td>{{ bill.blNo }}</td>
							<td>{{ bill.blDt | date:'dd/MM/yyyy' }}</td>
							<td>{{ bill.blAmt | number:2 }}</td>
						</tr>
						
						<tr>
							<td colspan="3"><span style="margin-left: 400px; font-size: 16px; font-weight: bold;"> NET TOTAL</span></td>
							<td style="font-size: 14px; font-weight: bold;">{{ bfMap.bf.bfTotAmt | number:2}}</td>
						</tr>
					</table>
				</div>
       
            <div class="divRow">
                    <div class="divCell21">
                        <div class="divCell22">
                          <div style="text-align:justify; margin-left:30px; margin-top:20px;">
                           <div><p>Thanking You,</p></div>
                           <div><p>Your Faithfully</p></div>
                           <div class="bold1"><p style="margin-top:30px;">For Care Go Logistics Pvt. Ltd.</p></div>
                           <div><p style="margin-top:30px;">Authorised Signatory</p></div>
                          </div>
                        </div>
                        
                        <div class="divCell22">
                           <div class="bold1" style="margin-left:-130px;">Bills have Been Verified by</div>
                               <div style="text-align:right; margin-right:260px;">
                                  <div>Name :</div>
                                  <div>Signature :</div>
                               </div>
                        </div>
                        
                        <div class="divCell22">
                               <div class="bold1" style="margin-right:150px; margin-top:20px;"><p>Receiving Detail :-</p>  <hr class="hr"></div>
                                <div style="text-align:right; margin-right:200px;">
                                  <div>Person Name :</div>
                                  <div>Date :</div>
                                  <div>time :</div>
                                  <div style="margin-left:30px;">Company Stamp</div>
                               </div>
                        </div>
                    </div>
             </div> 
	</div>

</div>	
       
           
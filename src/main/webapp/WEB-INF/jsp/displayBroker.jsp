<div ng-show="adminLogin || superAdminLogin">

<title>Display Broker</title>
		<div class="container">
			<div class="row">
				<div class="col s2"><input type="radio" name="brokerCodeList" ng-model="brkCodeByModal" ng-click="enableModalTextBox()"></div>
				<div class="col s10"><input type = "text" id="brkCodeByMId" name="brkCodeByM" ng-model="brkCodeByM" disabled="disabled" ng-click="OpenBrkCodeDB()" readonly></div>
			</div>
			<div class="row">
				<div class="col s2"><input type="radio" name="brokerCodeList" ng-model="brkCodeByAuto" ng-click="enableAutoTextBox()"></div>
				<div class="col s8"><input type = "text" id="brkCodeByAId" name="brkCodeByA" ng-model="brkCodeByA" disabled="disabled" ng-keyup="getBrkCodeList()" ></div>
				<div class="col s2"><input class="col s12" type ="button" id="ok" value = "OK" ng-click="getBrokerList()" disabled="disabled"></div>
			</div>
			<div class="row">
				<div class="col s12"><input type ="button" value = "Back To list" ng-click="backToList()"></div>
			</div>
		</div>
		
	<div id ="BrokerCodeDB" ng-hide = "BrokerCodeFlag">
 	  	 <input type="text" name="filterBrkCode" ng-model="filterBrkCode" placeholder="Search by Broker Code"> 	  	
 	   	  <table>
		  <tr ng-repeat="brokerCodes in brokerCodeList | filter:filterBrkCode">
		 	  <td><input type="radio"  name="BrokerName"  value="{{ brokerCodes }}" ng-model="brkCodes" ng-click="saveBrkCode(brokerCodes)"></td>
              <td>{{ brokerCodes }}</td>
          </tr>
         </table>       
	</div>
	
	<div class="container" ng-hide="isViewNo">
	 <input type="text" name="filterTextbox" ng-model="filterTextbox.brkCode" placeholder="Search by Broker Code">	
 	  <table class="table-hover table-bordered table-condensed">
 	 		<tr>
				<th></th>
				
				<th>Broker Code</th>
				
				<th>Broker  Name</th>
			
				<th>User Code</th>
				
				<th>Time Stamp</th>
			</tr>
 	  
		  <tr ng-repeat="broker in BrokerList | filter:filterTextbox">
		 	  <td><input id="{{ broker.brkCode }}" type="checkbox" value="{{ broker.brkCode }}"  ng-checked="selection.indexOf(broker.brkCode) > -1" ng-click="toggleSelection(broker.brkCode)" /></td>
			      <td>{{ broker.brkCode}}</td>
	              <td>{{ broker.brkName }}</td>
	              <td>{{ broker.userCode }}</td>
	              <td>{{ broker.creationTS }}</td>
          </tr>
         </table>
		<table>
         	 <tr>
				<td class="center"><input type="submit" value="Submit" ng-click="verifyBroker()"></td>
			</tr>
			
         </table>
		
		</div>
		
		<div ng-hide = "showBrokerDetails" >		
		<div class="container">
		<div class="row">
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="branchCode" ng-model="broker.branchCode" value="{{ broker.branchCode }}">
				    <label>Branch Code</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name =brkCode ng-model="broker.brkCode" value="{{ broker.brkCode }}" readonly>
					<label>Broker Code</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="brkName" ng-model="broker.brkName" value="{{ broker.brkName }}">
					<label>Broker Name</label> 
			</div>
					
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="brkComStbDt" ng-model="broker.brkComStbDt" value="{{ broker.brkComStbDt }}">
					<label>Broker Com Establishing Date</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="brkPanDOB" ng-model="broker.brkPanDOB" value="{{ broker.brkPanDOB }}">
					<label>Broker Pan DOB</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="brkPanDt" ng-model="broker.brkPanDt" value="{{ broker.brkPanDt }}">
					<label>Broker Pan Date</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="brkPanName" ng-model="broker.brkPanName" value="{{ broker.brkPanName }}">
					<label>Broker Pan Name</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="brkVehicleType" ng-model="broker.brkVehicleType" value="{{ broker.brkVehicleType }}">
					<label>Broker Vehicle Type</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="brkPPNo" ng-model="broker.brkPPNo" value="{{ broker.brkPPNo }}">
					<label>Broker PPNo</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="brkVoterId" ng-model="broker.brkVoterId" value="{{ broker.brkVoterId }}">
					<label>Broker Voter Id</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="brkSrvTaxNo" ng-model="broker.brkSrvTaxNo" value="{{ broker.brkSrvTaxNo }}">
					<label>Broker Service Tax No</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="brkFirmRegNo" ng-model="broker.brkFirmRegNo" value="{{ broker.brkFirmRegNo }}">
					<label>Broker Firm Reg No</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="brkRegPlace" ng-model="broker.brkRegPlace" value="{{ broker.brkRegPlace }}">
					<label>Broker Reg Place</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
                	<input type = "text" ng-model="broker.brkFirmType" value="{{ broker.brkFirmType }}">
            	    <label>Broker Firm Type</label>
            </div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="brkCIN" ng-model="broker.brkCIN" value="{{ broker.brkCIN }}">
					<label>Broker CIN</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="brkBsnCard" ng-model="broker.brkBsnCard" value="{{ broker.brkBsnCard }}">
					<label>Broker Bsn Card</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="brkUnion" ng-model="broker.brkUnion" value="{{ broker.brkUnion }}">
					<label>Broker Union</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="brkEmailId" ng-model="broker.brkEmailId" value="{{ broker.brkEmailId }}">
					<label>Broker Email Id</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="brkActiveDt" ng-model="broker.brkActiveDt" value="{{ broker.brkActiveDt }}">
					<label>Broker Active Date</label> 
			</div>
			</div>
			<div class="row">
			<div class="input-field col s12 center">
					<input type="button" value="Submit" ng-click="EditBrokerSubmit(broker)">
			</div>
	        </div>
	 </div>
	          
	</div>
	</div>
<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="mrForm" ng-submit=mrSubmit(mrForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
			
					<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="csDtId" name ="csDtName" ng-model="csDt" ng-blur="chngDt()" readonly ng-required="true" >
		       			<label for="code">CS Date</label>	
		       		</div>
		       		
					<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="custId" name ="custName" ng-model="customer.custName" ng-click="selectCust()" ng-required="true" readonly>
		       			<label for="code">Customer Name</label>	
		       		</div>
					
					<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bnkId" name ="bnkName" ng-model="bank.bnkName" ng-click="selectBank()" disabled="disabled" readonly>
		       			<label for="code">Bank Name</label>	
		       		</div>
		     		
		    </div>
		    
		  
		    <div class="row">
		   			<div class="col s4 input-field">
		       			<select name="mrByName" id="mrById" ng-model="mr.mrPayBy" ng-init="mr.mrPayBy = 'C'" ng-change="chngMRPay()" ng-required="true">
								<option value='C'>CASH</option>
								<option value='Q'>CHEQUE</option>
								<option value='R'>RTGS</option>
						</select>
						<label>Money Receipt By</label>
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="chqId" name ="chqName" ng-model="mr.mrChqNo" disabled="disabled">
		       			<label for="code">Cheque No</label>	
		       		</div>
		       			  
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="custBnkId" name ="custBnkName" ng-model="mr.mrCustBankName" disabled="disabled">
		       			<label for="code">Customer Bank</label>	
		       		</div>
		       			       		
		    </div>
		    
		    <div class="row">
		    
		    		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="refId" name ="refName" ng-model="mr.mrRtgsRefNo" disabled="disabled">
		       			<label for="code">RTGS Ref No</label>	
		       		</div>
		       		
		    		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="netAmtId" name ="netAmtName" ng-model="mr.mrNetAmt" step="0.001" min="0.000" ng-required="true">
		       			<label for="code">Net Amount</label>	
		       		</div>
		    </div>
		    
		    
		    <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="mr.mrDesc" ng-required="true"></textarea>
 					<label>Description</label>
				</div>
    		 </div>	
		    
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" id="saveId" value="submit" >
	       		</div>
	      </div>		
	     
	</form>
	</div>  
	
	
	<div id ="selCustId" ng-hide="selCustFlag">
		  <input type="text" name="filterCust" ng-model="filterCust" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Customer Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cust in custList | filter:filterCust">
		 	  <td><input type="radio"  name="cust"   value="{{ cust }}" ng-model="custCode" ng-click="saveCustomer(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="selBankId" ng-hide="selBankFlag">
		  <input type="text" name="filterBank" ng-model="filterBank" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Bank Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="bnk in bankList | filter:filterBank">
		 	  <td><input type="radio"  name="bnk"   value="{{ bnk }}" ng-model="bnkCode" ng-click="saveBank(bnk)"></td>
              <td>{{bnk.bnkName}}</td>
              <td>{{bnk.bnkFaCode}}</td>
          </tr>
      </table> 
	</div>
	
		<div id="duplicateDB" ng-hide="duplicateFlag">
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">You already generate a MR of {{ customer.custName }} with {{mr.mrNetAmt}}</h5></div>
		</div>
		<div>
			<div class="col s12 center">
				<a class="btn white-text" ng-click="cancelMR()">Cancel</a>
				<a class="btn white-text" ng-click="saveMR()">Yes</a>
			</div>
		</div>
	</div>
	
</div>	
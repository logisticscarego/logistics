<div ng-show="operatorLogin || superAdminLogin">
<title>Petro Card Advance</title>
<div class="row">

<form ng-submit="savePetroAdv(petro)" class="col s12 card"
style="align: center; padding-top: 40px;
 background-color: rgba(125, 125, 125, 0.3);">
 
  
			
	<div class="row">
			<div class="input-field col s3">
				 <input type="text" name="cnmtNoName" id="cnmtNoId" ng-model="petro.cnmtNo" ng-blur="checkCnmtCode(petro.cnmtNo)" required>
				 <label>CNMT No.</label>
			</div>
			
			<div class="input-field col s3">
				 <input type="text" name="challanNoName" id="challanNoId" ng-model="petro.challanNo" ng-blur="checkChlnCode(petro.challanNo)" required>
				 <label>Challan No.</label>
			</div>
			
			<div class="input-field col s3">
				 <input type="text" name="loryNoName" id="loryNoId" ng-model="petro.loryNo" ng-blur="openVehicleDB()" required="required">
				 <label>Lorry No.</label>
			</div>
			
			<div class="input-field col s3">
        			<input class="validate" id="emailid" type ="email" name ="emailId" ng-model="petro.email"  ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" required >
        			<label for="emailid">Email ID</label>
        				
      			</div>
			
		</div>
			
	<div class="row">	
	
			<div class="input-field col s4">
				 <input type="date" ng-model="petro.chlnDt" required="required">
				 <label>Challan Date</label>
			</div>
			
			<div class="input-field col s4">
				<input type="text"  ng-model="fromStCode" ng-keyup="getStationFrom($event.keyCode)" required="required">
				<label>From Station</label>
			</div>
			
			<div class="input-field col s4">
				<input class="validate" type ="text"  ng-model="toStCode" ng-keyup="getStationTo($event.keyCode)" required="required">
       		 	<label>To Station</label>
			</div>
			
			
	</div>
	
	<div class="row">


					<div class="input-field col s4">
						<input type="text" ng-model="petro.ownCode"  readonly>
						<label>Owner</label>
					</div>

					<div class="input-field col s4">
						<input type="text" ng-model="petro.brkCode"  ng-change="getBrokerByName(petro.brkCode)"
							ng-readonly="brkReadonlyFlag"> <label>Broker</label>
					</div>

				</div>
				
				<div class="row">

					<div class="input-field col s4">
						<input type="number" ng-model="petro.freight" required="required">
						<label>Freight</label>
					</div>
					
					<div class="input-field col s4">
						<input type="text" ng-model="petro.rate" required="required">
						<label>Rate</label>
					</div>
					
					<div class="input-field col s4">
						<input type="number" ng-model="petro.advance" required="required">
						<label>Advance</label>
					</div>
					
					</div>
	
	 <div class="row">
		<div class="input-field col s3">
			<select ng-model="petro.cardType" ng-init="petro.cardType='HP'" ng-change="changeCardType()" required="required">
			     <option value="HP">HINDUSTAN PETROLIUM</option>
			     <option value="BP">BHARAT PETROLIUM</option>
			     <option value="IO">INDIAN OIL</option>
			</select>
			<label>Card Type</label>
			</div>
			<div class="input-field col s3" >
				<input class="validate" type ="text"  ng-model="petro.cardNo" ng-click="getCardNo()"  readonly required>
       		 	<label>Card No</label>
			</div>
			
			<div class="input-field col s3">
				<input class="validate" type ="number"  ng-model="petro.tdsAmt"  required>
       		 	<label>Tds Amt</label>
			</div>
			
			<div class="input-field col s3">
				<input class="validate" type ="number"  ng-model="petro.amount"  required>
       		 	<label>Trf Amount</label>
			</div>
			</div>
	
			<div class="row">
			<div class="input-field col s12 center">
				<input type="submit" value="Submit">
			</div>
		</div>

</form>
</div>

<div id="fromStationDB" ng-hide="fromStationDBFlag">
	<input type="text" name="filterFromStation"
		ng-model="filterFromStation" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>

			<th>Station Code</th>

			<th>Station Name</th>

			<th>Station District</th>
		</tr>


		<tr ng-repeat="station in stationList | filter:filterFromStation">
			<td><input type="radio" name="stnName" id="stnName"
				class="stnName" value="{{ station }}" ng-model="frmStationCode"
				ng-click="saveFrmStnCode(station)"></td>
			<td>{{ station.stnCode }}</td>
			<td>{{ station.stnName }}</td>
			<td>{{ station.stnDistrict }}</td>
		</tr>
	</table>
</div>


<div id="toStationDB" ng-hide="toStationDBFlag">
	<input type="text" name="filterToStation"
		ng-model="filterToStation" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>

			<th>Station Code</th>

			<th>Station Name</th>

			<th>Station District</th>
		</tr>


		<tr ng-repeat="station in stationList | filter:filterToStation">
			<td><input type="radio" name="stnName" id="stnName"
				class="stnName" value="{{ station }}" ng-model="toStationCode"
				ng-click="saveToStnCode(station)"></td>
			<td>{{ station.stnCode }}</td>
			<td>{{ station.stnName }}</td>
			<td>{{ station.stnDistrict }}</td>
		</tr>
	</table>
</div>

<div id="brokerDB" ng-hide="brokerDBFlag">
		<input type="text" name="filterBroker" ng-model="filterBroker"
			placeholder="Search by Code">
		<table>
			<tr>
				<th></th>

				<th>Broker Code</th>

				<th>Broker Name</th>

				<th>Broker PAN No.</th>
			</tr>


			<tr ng-repeat="brk in brkList | filter:filterBroker">
				<td><input type="radio" name="brokerName" id="brokerName"
					class="brokerName" value="{{ brk }}" ng-model="brokerCode"
					ng-click="saveBrkCode(brk)"></td>
				<td>{{ brk.brkCode }}</td>
				<td>{{ brk.brkName }}</td>
				<td>{{ brk.brkPanNo }}</td>
			</tr>
		</table>
	</div>

<div id="vehDB" ng-hide="vehDBFlag">

		<input type="text" name="filterVehCode" ng-model="filterVehCode"
			placeholder="Search....">

		<table>
			<tr>
				<th></th>

				<th>Vehilce No</th>
			</tr>
			<tr ng-repeat="vehicle in vehList | filter:filterVehCode">
				<td><input type="radio" name="vehicleName" id="vehicleId"
					value="{{ vehicle }}" ng-model="vehicleCode"
					ng-click="selectVeh(vehicle)"></td>
				<td>{{ vehicle }}</td>
			</tr>
		</table>
	</div>

<div id ="petroCardDB" ng-hide="petroCardDBFlag" class="noprint">
		  <input type="text" name="filterPetrobox" ng-model="filterPetrobox" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Card Type</th>
 	  	  	  <th>Card No.</th>
 	  	  	  <th>FA Code</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="petros in petroList | filter:filterBObox ">
		 	  <td><input type="radio"  name="petros"   value="{{ petros }}" ng-model="petros" ng-click="savePetroCode(petros)"></td>
              <td>{{petros.cardType}}</td>
              <td>{{petros.cardNo}}</td>
              <td>{{petros.cardFaCode}}</td>
          </tr>
      </table> 
	</div>
	
<div id ="detailsDB" ng-hide="detailsDBFlag">
<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
   <tr>
  <td>Card Type</td><td>{{petro.cardType}}</td>
 </tr>
 	  	  <tr>
<td>Card No.</td><td>{{petro.cardNo}}</td>
</tr>
<tr>
<td>Amount</td><td>{{petro.amount}}</td>
</tr>
<tr>
<td>CNMT No.</td><td>{{petro.cnmtNo}}</td>
</tr>
<tr>
<td>Challan No.</td><td>{{petro.challanNo}}</td>
</tr>
<tr>
<td>Challan Date</td><td>{{petro.chlnDt}}</td>
</tr>
<tr>
<td>Lorry No.</td><td>{{petro.loryNo}}</td>
</tr>
<tr>
<td>From</td><td>{{petro.fromStn}}</td>
</tr>
<tr>
<td>To</td><td>{{petro.toStn}}</td>
</tr>
<tr>
<td>Email Id</td><td>{{petro.email}}</td>
</tr>
<tr>
<td>Owner Code</td><td>{{petro.ownCode}}</td>
</tr>
<tr>
<td>Broker Code</td><td>{{petro.brkCode}}</td>
</tr>
<tr>
<td>Rate</td><td>{{petro.rate}}</td>
</tr>
<tr>
<td>Freight</td><td>{{petro.freight}}</td>
</tr>
<tr>
<td>Advance</td><td>{{petro.advance}}</td>
</tr>
<!-- <tr> -->
<!-- <td>Branch Email Id</td><td>{{fund.beneficiaryEmailId}}</td> -->
<!-- </tr> -->
			</table>
			<input type="button" value="Save" id="saveId" ng-click="saveFA()" disabled="disabled"/>
			<input type="button" value="Cancel" ng-click="back()"/>
			
		</div>
	</div>
</div>
<div ng-show="superAdminLogin">
	
	<div class="container">
	<div class="row">
	<form class="card" name="UserForm" ng-submit="changePassword(UserForm,user)" ng-init="fetchUserList()" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
		<div class="input-field col s6">
	    <input type="text" name="userid" ng-model="user.userId" ng-click="openUsersDB()"  ng-required="true" readonly="readonly">
		<label>UserId</label> 
		</div>
		<div class="input-field col s6">
		<input type="text" name="password" ng-model="user.password" ng-required="true"><br>
		<label>New Password</label>
		</div> 
		
		<div class="col s12 center">
		<input type="submit" value="submit"/>
		</div>
	</form>
	
	</div>
	
	<div id ="usersDB" ng-hide="usersDBFlag" class="noprint">
		  <input type="text" name="filterUsers" ng-model="filterUsers" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>UserId</th>
 	  	  	  <th>UserName</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="user in userList | filter:filterUsers ">
		 	 <td><input type="radio"  name="userId"   value="{{user.userId}}" ng-model="userId" ng-click="selectedUser(user)"></td>
              <td>{{user.userId}}</td>
              <td>{{user.userName}}</td>
              
          </tr>
      </table> 
	</div>
	</div>
</div>


package com.mylogistics;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.rowset.serial.SerialBlob;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.mylogistics.services.ConstantsValues;


public class Test {
	public static void main(String []arg) {
		
		List<String> l1=new ArrayList<>();
		HashMap<Integer, String> mapa=new HashMap<>();
		mapa.put(10, "cnmt2_chln111");
		mapa.put(11, "cnmt2_chln11");
		mapa.put(15, "cnmt2_chln15");
		HashMap<Integer, String> map=new HashMap<>();
		map.put(10, "cnmt2_chln10");
		map.put(11, "cnmt2_chln11");
		map.put(12, "cnmt2_chln12");
		map.put(13, "cnmt2_chln13");
		map.put(14, "cnmt2_chln14");
		map.putAll(mapa);
		String s=map.get(10);
		System.out.println("14="+s);
		System.out.println("14="+map);
		l1.add("1");
		l1.add("2");
		l1.add("3");
		l1.add("cnmt2_chln25");
		for(int i=0;i<l1.size();i++) {
			if(l1.get(i).equalsIgnoreCase("2"))
				System.out.println("2 is in l1");
		}
		List<String> l2=l1;
		
		List<String> l3=new ArrayList<>();
		
		l2.add("a");
		l2.add("b");
		l2.add("c");
		
		l3.add("A");
		l3.add("B");
		l3.add("C");
		
		
		l2=l3;
		
		System.out.println(l1);
		System.out.println(l2);
		System.out.println(l3);
		
		String cnmtImgPath="/TestPath";
		try{
			Path path = Paths.get(cnmtImgPath);
			if(! Files.exists(path))
				Files.createDirectories(path);
		}catch(Exception e){
			System.out.println("Error in creating direcotry : "+e);
		}
		
		
		Blob blob;
		File file = new File(cnmtImgPath+"/t.pdf");
		System.out.println("Is file exist " + file.exists());
		if (file.exists()) {
			System.out.println("I am in success");
			
			try {
			  byte[] pdfdata=new byte[(int) file.length()]; 
			  pdfdata=Files.readAllBytes(Paths.get(file.getAbsolutePath()));
			  
				blob=new SerialBlob(pdfdata);
				System.out.println("blob"+blob);
			} catch ( Exception e) {
				e.printStackTrace();
			}
			

		}
		
		
		 PdfReader reader;

	        try {

	        	System.out.println("file.getAbsolutePath()"+file.getAbsolutePath());
	            reader = new PdfReader(file.getAbsolutePath());

	            System.out.println("hi");
	            for(int page=1;page<7;page++) {
		            String textFromPage = PdfTextExtractor.getTextFromPage(reader, page);

		            //System.out.println(textFromPage);
	            	
	            }

	            System.out.println("Bye");
	            reader.close();

	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		// initialize dividend and divisor
		/*int dividend = 3;
		int divisor = 4;
		// initialize quotient
		int quotient = 0;
		//loop till the divisor does not become smaller than dividend
		while(dividend >= divisor)
		{
		   dividend = dividend - divisor;
		   quotient++;
		}
		// print results
		System.out.println("Quotient is "+quotient);
		// result of last subtraction
		System.out.println("Remainder is "+dividend);
		
	
		java.util.Date cd= Calendar.getInstance().getTime();
		System.out.println("date="+ cd);
	java.util.Date d = new java.util.Date();
	Long dt = d.getTime();
	System.out.println("date="+ d);
	Date date = new Date(dt);

	Date date2 = Date.valueOf("2018-07-04");
	Long dif = (date.getTime() - date2.getTime())
			/ (24 * 60 * 60 * 1000);
	System.out.println("date="+ dif);
	
	
	int a=123;
	int b=23565;
	int c=2;
	
	
	
	String ownCode="own"+a;
	String ownFaCode="0"+b;
	
	
	//for(;;)
	
	System.out.println(ownCode+" "+ownFaCode);*/
	        
	        String str=new String("s");
	        String str2=new String("s");
	        String y="s";
		if(str!=str2) {
			System.out.println("True==");
		}
	    if(y.equalsIgnoreCase(str) ) {
			System.out.println("True");
		}else {
			System.out.println("Fasle");
		}
		
		System.out.println("main before return");
		Test t= new Test();
		int i=t.test();
		System.out.println("Main after return"+i);
	}
	
	@SuppressWarnings("finally")
	int test() {
		
		try {
			System.out.println("try block");
			return 1;
		}catch(Exception e) {
			System.out.println("catch block");
		}finally {
			System.out.println("Final block");
			return 2;
		}
		/*System.out.println("After try");
		return 0;*/
	}
	
}
